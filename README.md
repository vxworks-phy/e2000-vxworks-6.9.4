# 飞腾E2000开发板描述文档

## 1.简介
FT-E2000Q 是面向嵌入式应用的高性能处理器，集成2个FTC664核和2个FTC310核。    
主要技术特征如下：   
  
- 兼容ARM v8 64 位指令系统，兼容32 位指令
- 支持单精度、双精度浮点运算指令
- 每个FTC664核包含1MB私有L2 Cache
- 由两个FTC310核组成的Cluster内含256KB共享的L2 Cache
- 集成1个DDR4/LPDDR4-2400控制器，支持72bit数据
- 集成6 Lanes PCIE3.0 接口（1X4+2X1/1X2+4X1/6X1）
- 集成4个1000M以太网控制器
- 集成3个USB2.0(OTG)和2个USB3.0接口（兼容2.0）
- 集成1路I2S接口，支持高清音频播放;2路DisplayPort1.4接口
- 集成2路SATA3.0控制器
- 集成2个SD控制器
- 集成常用低速接口：WDT，DMAC，QSPI，Nand，SPI_M，UART，I2C，CAN，
  GPIO，LocalBus，Timer等等
详细技术指标与功能特征可以参考《飞腾腾珑E2000Q系列处理器数据手册》。

E2000D 1个cluster有2个cpu，共两核。  
主要技术特征如下：  
   
- 兼容ARM v8 64 位指令系统，兼容32 位指令
- 支持单精度、双精度浮点运算指令
- L1有32KB，L2有256KB
- 集成1个DDR4/LPDDR4-2400控制器，支持36bit数据
- 集成4 Lanes PCIE3.0 接口（4X1）
- 集成4个1000M以太网控制器
- 集成3路USB2.0(OTG)和2路USB3.0（兼容2.0）
- 集成1路I2S接口，支持高清音频播放;1路DisplayPort1.4接口
- 集成2路SATA3.0控制器
- 集成2个SD控制器
- 集成常用低速接口：WDT，DMAC，QSPI，Nand，SPI_M，UART，I2C，CAN，
   GPIO，LocalBus，Timer等等
详细技术指标与功能特征可以参考《飞腾腾珑E2000D系列处理器数据手册》。

E2000S 1个cluster有1个cpu，单核结构。  
主要技术特征如下： 
 
- 兼容ARM v8 64 位指令系统，兼容32 位指令
- 支持单精度、双精度浮点运算指令
- L1有32KB，L2有256KB
- 集成1个DDR4/LPDDR4-2133控制器，支持36bit数据
- 集成2 Lanes PCIE3.0 接口（2X1）
- 集成3个1000M以太网控制器
- 集成1路USB2.0(Device)和2路USB2.0(OTG)
- 集成2路DisplayPort1.4接口
- 集成1路JPEG Encoder模块
- 集成1个SD控制器
- 集成常用低速接口：WDT，PWM，QSPI，SPI_M，UART，I2C，I3C，PMBUS，
   SMBUS，GPIO，oneWire，ADC，Timer等等
详细技术指标与功能特征可以参考《飞腾腾珑E2000S系列处理器数据手册》。

## 2.VxWorks编译环境搭建
### 2.1. 更新源码  
把本项目的源码按对应位置复制、替换到VxWorks6.9开发环境。(WIND_HOME代表安装根目录)  
这些源码分两部分：BSP和库  
BSP的路径是：<WIND_HOME>\vxworks-6.9\target\config\<BSP-source>  
库的根路径是：<WIND_HOME>\vxworks-6.9\target\{src,h}\  
如果只更新BSP，可以直接用Workbench编译环境进行开发。  
如果库的源文件有任何更新，则先编译库，再用Workbench开发。  
   
### 2.2. 命令行编译库
库的运行模式有两种，SMP（Symmetrical Multi-Processing）和UP（Uni-Processing），简称多核和单核。  
开发者首先确定用多核，还是单核模式，再做具体的步骤，选择其中的一种模式进行编译。  
打开CMD命令窗口，设置开发环境： 
 
    cd <WIND_HOME>  
    wrenv.exe -p vxworks-6.9  
    cd <WIND_HOME>\vxworks-6.9\target\src    
(1)编译SMP库： 
 
    make CPU=ARMARCH7 TOOL=diab VXBUILD=SMP  
命令结束后，即完成<WIND_HOME>\vxworks-6.9\target\lib_smp\库目录下的文件更新。   
(2)编译UP库：  

    make CPU=ARMARCH7 TOOL=diab   
命令结束后，即完成<WIND_HOME>\vxworks-6.9\target\lib\库目录下的文件更新。   
   
补充说明：  
(1)清理整个库用rclean目标，比如：
    
    make CPU=ARMARCH7 TOOL=diab rclean  //删除整个UP库    
    make CPU=ARMARCH7 TOOL=diab         //重新编译整个UP库    
    make CPU=ARMARCH7 TOOL=diab VXBUILD=SMP rclean  //删除整个SMP库    
    make CPU=ARMARCH7 TOOL=diab VXBUILD=SMP        //重新编译整个SMP库 
   

### 2.3.图形化编译库
以上命令行编译库的方法，可以用Workbench工具的VxWorks Source Build(Kernel Library) Project菜单命令完全替代，简称VSB工程。     
只是VSB工程不会修改系统默认库 <WIND_HOME>\vxworks-6.9\target\lib或lib_smp，而是在VSB工程目录下保存新的库文件。新建VxWorks Image Project（简称VIP工程）时，要基于此VSB。    
推荐使用图形化编译。
   
## 3.VxWorks运行环境搭建
参考板使用uboot启动vxWorks操作系统。用bin格式的镜像，即默认文件名为vxWorks.bin启动操作系统镜像的方法不止一种，有网络、USB盘、SATA硬盘等方式加载，用户选择方便的一种即可。    


### 3.1. 通过tftp网络加载镜像
Workbench集成开发环境编译后，默认生成 vxWorks 镜像文件，是ELF格式的。可以进一步指定生成vxWorks.bin文件镜像，这里称作bin格式。 一般地，这两种格式，uboot都是可以加载运行的。     
加载的过程和方法都是一样的，区别仅仅是内存基地址不同。    

(1)对于bin格式，取基地址 0x80100000    
(2)对于ELF格式，取基地址 0x90100000    
	
uboot命令行里，0x前缀可以省略。
	
#### 3.1.1 PC端设置 TFTP 服务器
PC机和参考板通过网口相连。在PC机上启动TFTP服务器，以windows系统为例，直接双击<workbench安装目录>\vxworks-6.9\host\x86-win32\bin\Tftpd32.exe      
只需要设置 Current Directory 指向vxWorks.bin(或vxWorks)文件所在的路径即可，无其他设置。
        
#### 3.1.2 参考板设置uboot环境变量
参考板和PC机通过TTL电平的3线UART串口连接。在PC机上打开终端软件(比如超级终端、TeraTerm等)，连接此串口，波特率设置为 115200，其他无需设置。上电，在终端软件中停止uboot的自动启动，在uboot提示符中，输入如下命令:   
 
     setenv ipaddr 192.168.3.119    
     setenv serverip 192.168.3.118    
     saveenv

#### 3.1.3 下载和启动操作系统镜像

(1)bin格式镜像：         
在uboot里执行命令:    

    tftpboot  0x80100000  vxWorks.bin    
    go 0x80100000    
即可启动vxWorks系统     
	
(2)ELF格式镜像：    
在uboot里执行命令:    

    tftpboot  0x90100000  vxWorks    
    bootelf 0x90100000    
即可启动vxWorks系统     
	
（以下命令相同，仅以bin格式为例。用ELF格式时，只需要替换基地址和文件名即可，其他部分相同。）    
	   
### 3.2. 通过文件系统加载镜像
#### 3.2.1. USB盘加载
把vxWorks.bin文件提前拷贝到FAT32格式的USB盘中，再把此盘插入参考板的USB口，上电，执行uboot命令:

       usb start
       fatload usb 0 0x80100000 vxWorks.bin
       go 0x80100000
       
#### 3.2.2. FAT32硬盘加载   
把vxWorks.bin文件提前拷贝到FAT32格式的硬盘中，再把此盘插入参考板的SATA口，上电，执行uboot命令:

       fatload scsi 0:1 0x80100000 vxWorks.bin
       go 0x80100000

#### 3.2.3. EXT4硬盘加载   
把vxWorks.bin文件提前拷贝到ext4格式的硬盘中，再把此盘插入参考板的SATA口，上电，执行uboot命令:

       ext4load scsi 0:1 0x80100000 vxWorks.bin  
       go 0x80100000
       

### 3.3. 设置板载MMC卡自动启动，避免人工干预

此项设置是把vxworks镜像保存到板载MMC卡里，通过设置环境变量，使得板卡上电后自动运行操作系统。
比较方便的操作是：利用U盘里的vxWorks.bin文件，先手工启动起来，在vxWorks提示符下，把文件拷贝
到MMC卡。尤其是第一次用MMC时，还需要格式化该卡。演示操作如下：    
   
    第一步在UBOOT提示符E2000#下操作：
    E2000# usb start
    E2000# fatload usb 0 0x80100000 vxWorks.bin
	E2000# go 0x80100000    启动vxWorks
	
	第二步在vxWorks提示符->下操作：
	-> devs
	  3 /mmc0:0         确信能发现mmc设备
	  
	-> ls "/mmc0:0"     如果成功，就不需要格式化。否则需要先格式化
	
	-> dosFsVolFormat("/mmc0:0")    进行格式化...
    -> dosFsShow("/mmc0:0")         显示分区信息。
	
    -> cp "/bd0/vxWorks.bin", "/mmc0:0/vxWorks.bin"  把U盘的镜像文件，拷贝到MMC卡里
	
	第三步重启动板子，再次回到E2000#提示符：
    E2000# setenv bootcmd "fatload mmc 0 0x80100000 vxWorks.bin; go 0x80100000"
    E2000# saveenv
	
然后就可以下电、上电启动，自动引导系统，启动到vxWorks shell提示符。

在以上步骤中，如果MMC卡已经格式化为FAT分区，拷贝文件到卡上时，也可以在UBOOT下操作，只是拷贝
文件比较繁琐，需要把文件先拷贝到内存，再用fatwrite命令写入MMC卡。而且UBOOT提示符里，数字无论
是否有0x前缀，都是16进制数据，这个需要注意（fatwrite最后参数用16进制数据）。演示操作如下：   
   
   第一步在UBOOT提示符E2000#下把文件拷贝到MMC卡：
   
    E2000# usb start
    E2000# fatload usb 0 0x80100000 vxWorks.bin
    3845168 bytes read in 103 ms (35.6 MiB/s)  
	这里显示文件大小是10进制3845168字节，换算成16进制数：0x3AAC30
	
    E2000# fatwrite mmc 0 0x80100000 vxWorks.bin 0x3AAC30
    3845168 bytes written in 536 ms (6.8 MiB/s)

    E2000# fatls mmc 0
    3845168   vxWorks.bin  显示写入成功

  第二步设置环境变量：
  
    E2000# setenv bootcmd "fatload mmc 0 0x80100000 vxWorks.bin; go 0x80100000"
    E2000# saveenv
	
然后就可以下电、上电启动，自动引导系统，启动到vxWorks shell提示符。

### 3.4. 串口配置
四个 PrimeCell UART 串口，默认配置为

    Baud Rate   : 115200
    Data        : 8 bit
    Parity      : None
    Stop        : 1 bit
    Flow Control: None

组件配置：

    #define DRV_SIO_PRIMECELL
    #define INCLUDE_SIO
    #define INCLUDE_SIO_UTILS

在shell下可以列出四个串口设备文件名：

     -> devs
      drv name
       1 /tyCo/0
       1 /tyCo/1
       1 /tyCo/2
       1 /tyCo/3

应用程序可以使用文件系统接口控制访问这些设备。  
例如 ioctl() 可以设置/获取串口波特率。read()/write()可以收发数据。  
在本平台上，/tyCo/1作为系统的控制台，不建议修改配置。其他串口可以任意使用。

MIO复合设备可以做串口，也可以做I2C接口。解复用为串口时，由于驱动都是PRIMECELL串口设备，
则创建相同的设备名，编号按顺序递增。需要NUM_TTY参数值符合硬件总个数即可。


### 3.5. 网络和MAC地址
网口是集成的 SOC XGMACs 10/100/1000/10000  MAC.   
网口驱动组件：

    #define DRV_VXBEND_FTGEM
    #define INCLUDE_GENERICPHY
	
    由于芯片集成四个网口，而最终产品不一定全用，需要在config.h里，根据实际硬件重新配置如下几个宏定义：
    # define CONFIG_FT_GEM0
    # undef CONFIG_FT_GEM1
    # undef CONFIG_FT_GEM2
    # undef CONFIG_FT_GEM3
   需要的网口，用 #define； 不需要的，用 #undef。
   由于网口的MDIO总线访问管脚存在多功能复用，一般的，这是由固件自动解复用，到vxWorks系统运行时，已经可用。

如果需要ifconfig, ping 命令，则需要添加组件：

    #define INCLUDE_IFCONFIG
    #define INCLUDE_PING


网口名字：gem+数字序号

网口MAC地址的获取，优先从寄存器里取值（通常是固件对网口初始化的设置而遗留下来）。  
如果发现该值为全00或全FF，认为无效，会设置网口驱动软件默认值00:00:3e:02:02:38。  
所有网口的MAC地址不能重复，否则会导致网络不通。

vxWorks的ifconfig命令，不带参数时，可以查看网口信息。  
配置IP地址时，可以用如下格式:  

     -> ifconfig "gem0 192.168.100.100 up"

网口应用编程，遵循socket标准。可以参考vxWorks安装自带目录com.windriver.ide.doc.wr_net_stack_6.9里的手册。


### 3.6. CPU 个数

宏 VX_SMP_NUM_CPUS 的值是SMP启动的核的个数。    
E2000Q默认取最大值4，E2000D默认取最大值2，E2000S只能取1。      
可以在Workbench组件窗口中，修改此宏的值，选择启动的核数。

### 3.7. CPU ID映射

对于UP版本，只在一个核上运行操作系统，不涉及CPU ID映射。
只有SMP版本时，才需要映射。在sysLib.c里，使用数组cpuIndexMap[]进行物理ID号和逻辑ID号的转换。
物理ID号取自MPIDR（Multiprocessor Affinity Register）寄存器的低24位。 cpuIndexMap[0] 必须
为操作系统启动核（主核）的MPIDR，各个从核的物理ID依次填入cpuIndexMap[]数组后续项。该数组索引
就是逻辑ID。上层函数usrSmpInit唤醒每个逻辑核，就对应cpuIndexMap[]数组所配的每个实际物理核。

### 3.8. 块设备文件系统 

块设备 SD/USB/SATA 等设备，一般需要加载文件系统。  
VxWorks 支持两种文件系统 dosFs 和 HRFS. 配置dosFs时，可能需要以下常用组件:

    #define INCLUDE_DOSFS
    #define INCLUDE_DOSFS_MAIN
    #define INCLUDE_DOSFS_CHKDSK
    #define INCLUDE_DOSFS_FMT
    #define INCLUDE_DOSFS_FAT
    #define INCLUDE_DOSFS_SHOW
    #define INCLUDE_DOSFS_DIR_VFAT
    #define INCLUDE_DOSFS_DIR_FIXED
    #define INCLUDE_FS_MONITOR
    #define INCLUDE_FS_EVENT_UTIL
    #define INCLUDE_ERF
    #define INCLUDE_XBD
    #define INCLUDE_XBD_BLKDEV
    #define INCLUDE_XBD_TRANS
    #define INCLUDE_DEVICE_MANAGER
    #define INCLUDE_XBD_BLK_DEV
    #define INCLUDE_XBD_PART_LIB
    #define INCLUDE_DISK_UTIL

块设备分区命令：

    xbdCreatePartition("NameOfTheBlockDevice", nPart, size1, size2, size3)

举例1：只分1个分区，占整个设备，命令如下： 

    xbdCreatePartition("/mmc0:0",1,0,0,0)

举例2：分配4个分区，第2，3，4分区容量分别占用10%,15%,20%，剩余容量全部给第1分区，命令如下：

    xbdCreatePartition("/mmc0:0",4,10,15,20)

重要！ 分区命令会删除块设备上所有数据，要慎重使用！提前备份重要数据！建议只对出厂设备第一次使用时才分区。

格式化命令:

    dosFsVolFormat ("NameOfTheBlockDevice", 0x20, 0);    /@ FAT32 format @/

或者

    dosFsVolFormat ("NameOfTheBlockDevice", 0x10, 0);    /@ FAT16 format @/


格式化后，可以用文件系统的相关命令，比如

    copy ("vxWorks", "NameOfTheBlockDevice/vxWorks");
    pwd
    cd("/ata0:2")
    ls
    ll 
    rm "/ata0:2/test.bin"
    mv "/sd0/dir1","/sd0/dir2"
    rename "oldfile","newfile"

如果copy命令省略第2个参数，则用标准输出。这可以在shell里显示文本文件内容，比如

    copy "/ata0:2/hello.txt"

devs 命令可以查看vxWorks系统添加的设备列表。比如:

    /tyCo/0
    host:
    /ata0:1
    /bd0
   
   
### 3.9. I2C驱动组件配置及使用方法

基本组件配置：

    #define DRV_FTI2C
    #define INCLUDE_I2C_BUS


如需访问I2C总线上的设备，则需要添加I2C设备驱动。驱动需遵循VxWorks I2C总线驱动架构的标准。    
I2C总线驱动架构可以参考vxWorks安装自带目录docs下vxbus_device_driver_developers_guide.pdf手册中的第9章I2C Drivers。    
如果访问I2C的RTC设备，比如DS1339器件，则添加组件   

    #define INCLUDE_TIMER_RTC
    #define DRV_I2C_RTC

并且在hwconf.c的I2C设备列表中添加DS1339设备。注意struct i2cDevInputs的deviceAddr用7位I2C设备地址，由硬件决定具体值。   

访问RTC设备的用户接口主要有：   

    STATUS vxbRtcGet (struct tm * rtcTime); /*读取RTC时间。*/
    STATUS vxbRtcSet (struct tm * rtcTime); /*设置RTC时间。*/
    （系统中如果存在多个RTC设备，软件会自动选用最合适的一个）。   
  
如果访问I2C的EEPROM设备，则添加组件   

    #define DRV_I2C_EEPROM
    #define INCLUDE_EEPROMDRV

用devs命令，可以查看到EEPROM设备文件名称。用文件系统接口open/read/write/ioctl/close等访问设备。    
详细用法可以参考target\src\hwif\util\vxbEeprom.c的文件头注释。    

总之，I2C总线驱动，给设备驱动提供统一接口 vxbI2CDevRead()，vxbI2cDevWrite()，vxbI2cDevXfer()。应用程序访问设备时，要根据具体器件的设备驱动，灵活处理。    


### 3.10. GPIO组件配置及使用方法

基本组件配置：

    #define DRV_FTGPIO

可以在hwconf.c中的gpioXModeTable表中配置GPIO各个pin的默认工作模式（NOT USE/ IN / OUT /INTERRUPT）。    
例如，将GPIO0的pin0设成IN，pin1设成OUT，pin2设成INTERRUPT：

    LOCAL UINT32 gpio0ModeTable[] = {
    /*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/       
            1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                               2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
    };

访问GPIO的用户接口主要有：

    gpioModeSet():设置各个pin的工作模式
    gpioInput()：输出各个pin的值
    gpioOutput()：写入各个pin的值
    gpioISRSet()：中断模式下设置用户的回调函数

使用方法可参考FT_GPIO_DEBUG宏下面的用户接的例程


### 3.11. SD/eMMC组件配置及使用方法

基本组件配置：

    #define DRV_FTSD
    #define DRV_SDSTORAGE_CARD
    #define DRV_MMCSTORAGE_CARD
    #define INCLUDE_SD_BUS

根据硬件实际情况配置config.h文件中的以下宏：     

    #define CONFIG_FT_SD0    /*板子上存在sd/mmc-0设备时，define该宏，否则undef该宏*/
    #define CONFIG_FT_SD1    /*板子上存在sd/mmc-1设备时，define该宏，否则undef该宏*/

SD/eMMC控制器使用默认配置不通时，可以考虑存在相位偏差问题，此时可以通过打开以下宏，
 调整sd clk线上输出延时来解决，延时的具体值根据板子实际情况决定。

    #define CONFIG_FT_SD0_CLKPIN_DELAY
    #define CONFIG_FT_SD1_CLKPIN_DELAY 
   
SD/eMMC支持FAT32文件系统 , 需要包含dosFs文件系统组件。   
dosFs文件系统相关组件参考上面的”3.8.块设备文件系统“一节

第一次使用DosFs文件系统时，需要格式化DOS分区：    
在shell里执行：

    dosFsVolFormat ("/mmc0:0", 0x20, 0);    /* eMMC 格式化 */

或者

    dosFsVolFormat ("/sd0:1", 0x20, 0);    /* SD 格式化 */

格式化后，可以用文件系统的相关命令，比如 :

     -> cd "/mmc0:0"
     -> fd=open("test.txt",0x202,0777)
     -> write(fd, "hello world\r\n",13)
     -> close (fd)
     -> copy "test.txt"
   
如果使能INCLUDE_DOSFS_CACHE，那么DOSFS_BLOCKS_PER_DATA_DIR_CACHE_GROUP宏应小于等于SD_MAX_BD_NUM。 

### 3.12. CANFD组件配置及使用方法

基本组件配置：

    #define DRV_FTCAN


由于E2000支持CANFD和CAN接口，应用时先确定器件需要什么模式。    
在hwconf.c中配置CANFD控制器的工作模式（CAN/CANFD）以及波特率。    
例如，将CANFD控制器0配置为CANFD模式，波特率设置为1MHz(仲裁段)+5MHz(数据段)，    
具体需要修改ftCanDev0Resources[] 数组里的具体值。    
  
访问CANFD的用户接口主要有：  
 
    ftCanSetBitrate()：设置波特率，可以运行时修改波特率。
    ftCanSend()：发包函数接口，在参数中设置帧格式的各类参数。
    ftCanRecvCallback()：收包时，挂接回调函数。如果不设置，则默认在shell里显示接收信息。
    ftCanSetIDFilter()：设置CAN ID过滤。


### 3.13. SPI组件配置及使用方法

基本组件配置：

    #define DRV_FTSPI
    #define INCLUDE_SPI_BUS

如需访问SPI总线上的设备，则需要添加SPI设备驱动。驱动需遵循VxWorks SPI总线驱动架构的标准。    
SPI总线驱动架构可以参考vxWorks安装自带目录docs下vxbus_device_driver_developers_guide.pdf手册中的第17章SPI Drivers。

如果访问SPI的FLASH设备，比如gd25lq32es，需要添加如下代码：

① 添加组件

    #define DRV_SPIFLASH_SP25

② 在config.h中添加gd25lq32es的SPI_FLASH_DEVICE_NAME、SPI_FLASH_SIZE等参数    
③ 在hwconf.c的SPI设备列表spi2DevTbl中添加gd25lq32es器件    
④ 在vxbSp25SpiFlash.c的设备列表spiFlashList中添加gd25lq32es器件    
   列表中的flash名称需与config.h中定义的SPI_FLASH_DEVICE_NAME一致(需要去掉spiFlash_前缀)；    
   列表中的manuId, devId, extId需与芯片读出的id一致（调试时可打开debug宏SPI_FLASH_DBG，打印读出的flash相关id）；    
⑤ 可以通过TFFS文件系统访问SPIFLASH，具体操作参考下面的"3.15. TFFS组件配置及使用方法"一节      

SPI总线驱动，给设备驱动提供接口vxbSpiTransfer()，vxbSpiIoctl()。应用程序访问设备时，要根据具体器件的设备驱动，灵活处理。


### 3.14. QSPI组件配置及使用方法

基本组件配置：

    #define DRV_FTQSPI
    #define INCLUDE_SPI_BUS

如需访问SPI总线上的设备，则需要添加SPI设备驱动。驱动需遵循VxWorks SPI总线驱动架构的标准。    
SPI总线驱动架构可以参考vxWorks安装自带目录docs下vxbus_device_driver_developers_guide.pdf手册中的第17章SPI Drivers。    

如果访问QSPI的FLASH设备，比如gd25lq32es，需要添加如下代码：    
① 添加组件

    #define DRV_SPIFLASH_SP25

② 在config.h中添加gd25lq32es的QSPI_FLASH_DEVICE_NAME、QSPI_FLASH_SIZE等参数    
③ 在hwconf.c的SPI设备列表qspiDevTbl中添加gd25lq32es器件    
④ 在vxbSp25SpiFlash.c的设备列表spiFlashList中添加gd25lq32es器件    
   列表中的flash名称需与config.h中定义的QSPI_FLASH_DEVICE_NAME一致(需要去掉spiFlash_前缀)；    
   列表中的manuId, devId, extId需与芯片读出的id一致（调试时可打开debug宏SPI_FLASH_DBG，读出flash的相关id）；    
⑤ 可以通过TFFS文件系统访问SPIFLASH，具体操作参考下面的"3.15. TFFS组件配置及使用方法"一节   

QSPI总线驱动，给设备驱动提供接口vxbSpiTransfer()，vxbSpiIoctl()。应用程序访问设备时，要根据具体器件的设备驱动，灵活处理。


### 3.15. TFFS组件配置及使用方法

若要支持TFFS文件系统，需要包含TFFS及dosFs文件系统组件。    
dosFs文件系统相关组件参考上面的”3.8.块设备文件系统“一节；    
TFFS文件系统相关组件如下：    

    #define INCLUDE_TFFS
    #define INCLUDE_TFFS_MOUNT
    #define INCLUDE_TFFS_SHOW
    #define INCLUDE_TFFS_STUB_VXBFLASH

第一次使用TFFS文件系统时，需要格式化FLASH，并且格式化DOS分区：    
在shell里执行：    

     -> sysTffsFormat 0              对FLASH格式化
     -> usrTffsConfig 0,0,"/tffs0"   创建设备
     -> devs                         显示设备/tffs0
     -> dosFsVolFormat("/tffs0",0,0) 分区格式化为DOS

第二次上电以后，无需格式化，只需要创建设备就可以了，在shell里执行创建设备后，就可以用了：    

    -> usrTffsConfig 0,0,"/tffs0"   

使用方法举例：    

    -> cd "/tffs0"
    -> fd=open("test.txt",0x202,0777)
    -> write(fd, "hello world\r\n",13)
    -> close (fd)
    -> copy "test.txt"


### 3.16. GDMA组件配置及使用方法

基本组件配置：

    #define DRV_FTGDMA

访问GDMA，应遵循VxWorks的DMA驱动架构。    
可以参考vxWorks安装自带目录docs下vxbus_device_driver_developers_guide.pdf手册中的    
第3.6.7节Direct Memory Access (DMA) 及 第8章Direct Memory Access Drivers。    

GDMA控制器支持内存与内存之间的数据搬移，支持直接模式及链表模式。    
使用方法可参考GDMA驱动的例程：    

    testGDmaMemCpy()   /*直接模式的例程*/
    testGDmaChain()    /*链表模式的例程*/


### 3.17. PCIE总线

基本组件配置：

    #define DRV_PCIBUS_FT

    #define INCLUDE_PCI_BUS
    #define INCLUDE_PCI_BUS_AUTOCONF
    #define INCLUDE_PCI_BUS_SHOW
    #define INCLUDE_PCI_OLD_CONFIG_ROUTINES


该组件支持PCI/PCIE总线访问。初始化不做autoConfig总线扫描，而是直接利用固件扫描的结果。    
中断只支持四个legacy interrupt，不支持动态MSI中断。    
PCIE总线设备是运行时动态加载驱动的，不需要hwconf.c里配置hcfDeviceList[]设备列表。     
设备驱动程序通常使用VendorID,ProductID和硬件匹配。    
常用的调试命令有：    

    pciConfigTopoShow
    pciHeaderShow busNo,deviceNo,funcNo
    pciDeviceShow busNo
    pciFindDeviceShow vendorId,deviceId,index


### 3.18. SATA硬盘

基本组件配置：   

    #define INCLUDE_DRV_STORAGE_AHCI

在 shell中，可以用devs查询硬盘设备名字，例如：

     -> devs
      drv name
       3 /ata0:1
       4 /ata0:2
       4 /ata0:3

     -> ls "/ata0:1"
      /ata0:1/uImage-2004

实际显示的设备名字，与硬盘分区有关，而且当前只支持硬盘的DOSFS文件系统。具体参看块设备描述。


### 3.19. USB总线和设备

USB总线组件配置：

    #define INCLUDE_USB_XHCI_HCD_INIT


USB设备种类很多，此处举几个例子：    

USB存储设备组件：    

    #define INCLUDE_USB_GEN2_STORAGE_INIT
    USB存储设备的文件系统，和SATA一样只支持DOSFS文件系统。

USB键盘组件：

    #define INCLUDE_USB_GEN2_KEYBOARD_INIT
 
    如果USB键盘用作shell输入设备，则继续添加组件：
    #define INCLUDE_USB_GEN2_KEYBOARD_SHELL_ATTACH

USB鼠标组件：

    #define INCLUDE_USB_GEN2_MOUSE_INIT


### 3.20. 外设通用定时器
E2000外部设备扩展了38个通用定时器，可以一次性/周期性定时，可以做风扇转速计，还可以做计数器捕获外部事件。    
基本组件配置：

    #define DRV_TIMER_FT_GPT

具体使用时，在hwconf.c里，配置资源和工作模式，使得和硬件一致。    
接口函数有：  
  
一次性定时： 
   
    ftGptClkOnceConnect
    ftGptClkOnceTimeSet
    ftGptClkOnceDisable
    ftGptClkOnceEnable

周期性定时：

    ftGptClkConnect
    ftGptClkDisable
    ftGptClkEnable
    ftGptClkRateGet
    ftGptClkRateSet

转速计：

    ftGptTachoOverConnect
    ftGptTachoUnderConnect
    ftGptTachoOverLimitSet
    ftGptTachoUnderLimitSet
    ftGptTachoDisable
    ftGptTachoEnable
    ftGptTachoRPM

捕获事件：

    ftGptCaptureConnect
    ftGptCaptureCnt


### 3.21. PWM脉冲宽度调制

基本组件配置：

    #define DRV_FTPWM

E2000有8组PWM资源，进行波形脉宽调制。具体配置需要在hwconf.c中，根据实际硬件进行配置。    
系统启动后，创建对应的设备名字，devs可以查看，样例：    

     -> devs            
      11 /pwm/0              
      11 /pwm/1              
      11 /pwm/7  

通过文件系统标准接口，访问控制PWM设备。参数设置主要通过ioctl调用，传入结构体PWM_USER_CONFIG信息。

### 3.22. 显示器PC CONSOLE

基本组件配置：

    #define DRV_FTE2000_DC
    #define INCLUDE_PC_CONSOLE

USB键盘鼠标配置：
	
    #define INCLUDE_USB_XHCI_HCD_INIT
    #define INCLUDE_USB_GEN2_KEYBOARD_INIT
    #define INCLUDE_USB_GEN2_MOUSE_INIT

该组件支持显示器上PC Console使用。显示接口驱动库libFtE2kdcdrv.a文件存在于BSP的lib文件夹下。
BSP的Makefile通过LIB_EXTRA引用此库：
ifneq ($(findstring  bootrom,$(MAKECMDGOALS)),bootrom)
LIB_EXTRA = lib/libFtE2kdcdrv.a
endif	


# 4. 参考资源

ARM Architecture Reference Manual

Wind River Workbench User's Guide

VxWorks Kernel Programmer's Guide

VxWorks Architecture Supplement    

飞腾腾珑E2000系列处理器软件编程手册    

飞腾腾珑E2000Q系列处理器数据手册    

飞腾腾珑E2000D系列处理器数据手册  
  
飞腾腾珑E2000S系列处理器数据手册


# 5. 已知问题列表

## 5.1. Workbench WDB调试连接SMP内核问题
现象：调试连接失败    
PC机上workbench工具在建立连接过程中，默认会检查PC机上程序映像文件，和目标板上正在运行的
文件是否一致。这通过检查文件checksum实现。由于SMP版本映像文件上电启动时，动态修改了代码段内容，
这会导致检查失败，WDB报错退出。（UP版本映像文件无此问题）

建议：为了使用WDB，可以在WDB连接选项中禁用此校验。只要用户保证PC机上WDB使用的映像文件，和目标板上
正在运行的程序，是同一个文件，则不影响正常调试。
      具体选项为： Target Server Options >> Kernel image >> Bypass checksum comparison 打上勾选中。
	  如下图红圈所示：
![markdown picture][SMP_BypassCheckSum]
	
## 5.2. Workbench WDB连接IP地址问题
现象：系统启动后，用ifconfig "网口 IP地址" 命令修改了IP地址后，WDB连接失败。

建议：由于WDB组件初始化时就要获取IP地址，因此不要启动后再修改WDB用的IP地址。    
在组件配置窗口里，修改 DEFAULT_BOOT_LINE 字符串里的IP地址，然后编译镜像。

[SMP_BypassCheckSum]:data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAnAAAAMXCAYAAAC6lWj1AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAGtRSURBVHhe7d3/8y1XXe/5/cvUzL9A1fw21i1L0RKP52PqXJEqjcAFrsQEAY8J2SbhoyEBzzjg9UbxlMn1fC4E9cOM4FCAfBxrMPox6r2EwxfReEpmFMWPegG5cDJAgBATEkgCCUlIsqbfq3t1r7V6re7Ve/fuz157P99Vj5zTvbpXr+7ee/cr3fvzObMzZ84oAAAA5EMHOIqiKIqiKCqPIsBRFEVRFEVlVgQ4iqIoiqKozIoAR1EURVEUlVkR4CiKoiiKojIrAhxFURRFUVRmtXCA+8IXvqDuuOMO9cd//Mfqfe97nzo6OlJPPvlk1UpRFEVRFEWtqhYKcH/zN3+jDg4OdGi799571Re/+EX1j//4j+qP/uiP1N13310tRVEURVEURa2iBgc4CWvvete71MWLF9Xjjz+uHnroIXXffffp+Z/5zGfUn/7pn+o2iqIoiqIoajWlA9yrBwQ4eVz6gQ98QD322GM6wAkJcPJI9ZOf/KT6+Mc/rn7/939fPfLII9Ua1Kh1cV+dmp1S+2RkiqIoagNrNpslWbeSMUkGipW0jTluHeB+6tVpAe5b3/qWesc73qH++Z//Wd95e/TRR/W8r3zlK+qzn/2s+qd/+if1V3/1VzrAyePU/rqo9k/JiQgEkvO7anZqv1hilXVe7Vovhtnu+Wr+GhcBjqIoitrgSgk5YwahsUoC2v/wP/5PwRDX1bZoDQpwcqft7W9/u35E+sADD6gvfelL6nOf+5y655571Ic//GH11re+VV24cEGdO3dO/dZv/Zb68pe/XK0ZqzLA7e4GwtqqA5z0X7wAnMxWzDu1dsmoOkYZZEuKoiiKWrZyDXBSoaC2ivAmNegRqvzAwtve9jZ11113qQ9+8IPq1ltvVTfffLP+YQYJcDfeeKP6vd/7vTrAyU+odpcJJ+WfTnhaaYAr77zlEYoIcBRFUdT2VM4BTsoObKsKb1I6wMl/Uurhhx/Wd9kksMldOHls+hu/8Rs6sP3FX/yFOnv2rPqVX/kV9Yu/+Ivq137t13Sg6/7VIlY40Y8GraDSCnDu404T9s7v2sHPC2axEJgUDr3Hq7PdYo4paTul9vfLu3j2eLrbpML7UZbftu+OQcasj5M/FmuZ5HFSFEVR1PqVXKv6KmWZ4ywT3FYV3qQGBTgp+Q7c+9//fn0X7l/+5V/U3/3d3+nA9pa3vEWHt9tuu0299KUv1eHtla98Zc+vFXHvLl3cP9UEECdkVUGkzh7Nenod00GxzqlTp+qQ4oY7q3oDXBmKnHVlnTocVaGpGbj1vbS+tvB+BLepy7sD5wS4ZcZJURRFbXpJeEixTkWAS6vBAU5+ee/v/M7v1D9x+vd///f6J1Nf/epXq2uvvVYHuF/+5V9WV111lbrsssvUP/zDP1RrhsoLJ9W0Dhx2yNKhpLyDZNPBxQo053clnBShRa8nfUXCSl+AC7b7YcsPYma6o61rP6Jjsrcrk1aAW2qcFEVRFLV+JdfFvkpZ5rjKhDf50/772FUFuFdXk/0lP5jwq7/6q/o7cH/7t3+r/vIv/1I/PpXvxr3oRS9Sv/mbv6kfqf7sz/6suvTSSwcGuKJ0QCnm2T/Y0Bm4TCjxgtv5op/oOn6w8SoajMw6SwS42JhGDXAp46QoiqKo9aucA1wosK0qxA2+Ayf1zne+U//wwp133qnOnz+v/uRP/kTfeZNHqL/+67+ufuEXfkFdeeWV6rnPfa7+FSPx8sJJVRf1o9SZFU4kiLjLnS8CXpNpTulHp+ZRoZluP45sqtyGF2aKQFSuU26v9WjSGU8sGPW1xfbDbzuv9vVK3jGyA1y1zmLjpCiKoqj1q1wDnAS0WFDralu0Fgpw8itEXve61+kfVJDHp+9973v1d+P29/fVTTfdpK655hol34OTu3DdFQ5wZr5zd6m6M6eDXcFZx9y1c0JOQlCRsGP1WX9XTFcZjpr2JjB2B6Oe0JSwH/72yrBZzJPjoZfxx9L0lz5OiqIoilq/aq5n3datZExdAU3axhz3QgFOSn6Z7yte8Qr12te+Vn8nTh6hyl25G264QV1++eXq2c9+tv5+HEVRFEVRFDVuLRzgpD796U/rEPdjP/Zj+idOT58+rX7kR35Efcd3fIf6mZ/5mWopiqIoiqIoasxaKsBJyb+FKv9slvzU6fOe9zz1ghe8QD9OfeaZZ6olKIqiKIqiqDFr6QBHURRFURRFTVsEOIqiKIqiqMyKAEdRFEVRFJVZEeAoiqIoiqIyKwIcRVEURVFUZkWAoyiKoiiKyqwIcBRFURRFUZkVAY6iKIqiKCqzIsBRFEVRFEWtccm/jfDkt59R33jsKfXgw99W//q1JwlwFEVRFEVR61ZPP/2MeuLJZ9QjjzahzUaAoyiKoiiKWoOSO20S2h59/Gn18DefUg881A5uhg5wP/zDP1ytSlEURVEURU1Z337qGfWtJ55W3/zW0/Vj0vuKkCZC4U1wB46iKIqiKGrikrttEtwee/xp9WgR3CS8PfTNp9T9Xy+CmyhCGgGOoiiKoihqTUp+IMEEN2Huukl4I8BRFEVRFEWtUT0ld9yeKEKbhLcqwMl33b760LcLZXgjwFEURVEURa1BPf200t9xq4NbRR6ZSnAzBgc4foiBoiiKoihq3DI/VSqPS/UjU+NbT6uvfePb6oGHyztvCwc47sBRFEVRFEWNV+YnS014MwHum996Sn3tkTK8EeAoiqIoiqLWoJ5+RqnH5a6bF96EhLcHi/AmP7BAgKMoiqIoilqDqu+6GU54e7oOb5MEuNlsliSXCo1d5Fir2o8x+6IoiqKoTa9nCvJdNwlvoQAn4U0em44e4G688cZyBAm1zhf3vrHF2vvWW8cKjXnR/Vh0PYqiKIra9pJ/s/TxJ8vgFgpw5gcWxg5wkt1mN9xwQzWM/lrni33f2GLtfeutW3WNd5F9WWQdiqIoitr2Mo9MfSbASXj7ehXexg5wkt1GDXCm3WZXrM2fb9jV1R6bb1dXm192X6H1Qu3CL3++vazfJtXVZmpIm5n2mQrNt/9uyl5mkXapvnaKoiiKyqH0I9Nvh8ObcMJbDgEu1ObPM9OheaZSlpFKWcYvs0zXsqE2f56ZNvP8dlN9y/jzzLS/nF1d7X6bmQ7NMzV0Wiq2jJkXazflT1MURVFUDiW/201+ytR/bGqTR6cS3rIJcFKm3WfKn5ZKmbfoMl1llvfXSZm3yDL+tFTKMn51LeO3hZbtW6ZvWqpvmb5piqIoisqtnqq+72YEw1tB/nmsrAJcqM2fl7KMlD/PTIeY8qdTK9ZPiCl/WqpvGTMdYsqfDlXXMn5baNm+ZfqmpfqW8aelzLxQG0VRFEWtc8n33ezwFgtwDz/6lP4nsghwVYWW8atvmVi7Pz+2nF2xZcz8UHtonl/LLuO3hZbtW6ZvWqpvGX/ar752iqIoilqXKsNbf4D7xmNleNvaAGemQ/PsSlnGrli7Pz+0XMoyUmZ+qD1lXmiZUIWW65pnz192WqpvmaHTFEVRFLVuJT+s8OS3TXjrDnDyu95MeMsuwEmZ9hC73S97OZtdfe1Ssfmm7HVtfvUtE5pnKqXNZldoXqzsPmLrhZYRftnzQ8uYeTa7/Hldy4TaKIqiKGrdSsKb/ILevgAnP7Qg33tbdYD7ygNPqC/f/4T6UmFwgJuquMiPUxxHiqIoihpW8pOm8mtCJLz1BTj9QwuPPjVugKvCmgS1u//1cfX5ex9Xn/uKiwC34cVxpCiKoqj0kvBm7rylBLhHHivC25IB7v7Cvz74pLrnq0+oL95XBrZQaLOtTYAzQcNGLV8cS4qiKIpKqzq8JQY4+d6bDm8LBDgJbvJYVELb3UVokzttXxBVeMsmwFEURVEURR1XOeEtIcDJ994eMeEtNcAVwU2+32Yej35RSHgjwFEURVEURQ2rVnjrCXDm0WlqgPvqQ9+u77Z9WVQ/iECAoyiKoiiKWqBMeBsS4L7xWHn3rS/AyWPSe+W7bQ88USLAURRFURRFLVd2eEsNcI8+Xv7C3liAk+AmP5Qgwe0rwoQ3AhxFURRFUdRypcPbU014Swlw8uhUwlsowJngZn5nGwGOoiiKoihqxJLwJv88lh3e+gNc+VOnfoB76Jvld9zML9+dNMBddtll1S5RFEVRFEVtbsXCW1+Ak586NeFNyONS+T1uctfN/tcTCHAURVEURVEjlw5vAwOcPDo1d9/krpv8Prf6X084zgAn/6EoiqIoitrkeqoIbkMDnNx9e1SHt6f199z8fwKLAEdRFEVRFLWieurpMrwNDXD6H6p/9Kn6F/ES4CiKoiiKoiYoO7wNCXDyD9XrX8Yr4Y0AR1EURVEUNU09/XT5vbchAc48Nn3oG0/V/xTWWga4tfohhov76tTslNq/WE2va53fVbNT+2rdh9lZKz/W59VuDueSSq9c3p8URVFFmZ84HRLgzO96K3+328YGuItq/9RMzWaWZUMNAW66IsBRQ4sAR1FUJhULbyIU4OSRqfwrC980vyakCG8bH+B2z1eTRZ3fLUKcPWNTa7IA1z7G61v+WMcKcMd5DHI6/quobd9/iqJyrCK7tb73ZvMDnPk1Id/81lM6wMndt/KfxdqiAKeDzTZ82hPgAuWPlQCXf237/lMUlWNJeDO/MiTEDnDyU6byfTcT4Mwv6d2yAFdOn3Ku2HIRbx6xlm3t5S7unyoDkX5Es1usZSq0vuQne/1ymXocXeFK99/014y9Chv7xbpVW9d+aJ0BLjBuf9+8aX0M6nVkvrfNenv+WPzjVezH+WY/9T7KMamm6/2ytu9uu9Ic0Pa+tCo01mos1jF1w/2i/RYVPY/mNVmtFzxm1Zic8xcaS2TbXrXPm6lQn1L+GHfVbuz9oKdS+wmPr7Ufzhi9Pirl8fTWk/7913Bn39IWf0/FjxtFUdTi9bSEt4QAZ/5heglvdoAz/yj9uge4G2+8cczvwBUf1s4VpPoAr+eZi0XxVydkWfOdC0R8ff3hb67aRV+nTp2qLw5uuLOrWH/XusjpUGNvq9gH06ceh9l22WaapPSj4p4LZmsM/sXPnm5dGE1Zx0ZXoO/QfpixmeDm7FfPNp1zI/1FzmGrwmONH9NF+y2mo+exXLYdJLz1ZZmkffS37VX0vPX36YzROeb2sgP7aVW57/HXS2QsXnu9/87+9vVdtgfPf/S4URRFLV5FbivDW0eAk/Amj0z1nTcvwD1i3X1b9wB35syZZX6NiPvh3vr+m/4wl4uDq/zAty5M9oe5/feu9a3lzu9KP0V/+gIoY7IveO0K/5+/NR5dVj8yjvriWlVonqlYm3/Rcqari50zBinvAhrs217G34+OaX88Uv68rnPQKm+srW17x3ThfssKn8fAsqFjZs/rHEt4201FztvgPiPnZamxFRXad2e9UB8d7f7YBr0Wpc1MR44bRVHUgiU/tFCHt0iAM993k/DWDnDNd9+2LsCZD+V6OvgB35S5UyYX4vrC3XuBMGUuBsU29TLVtDw6jK2j+7ZCghNWOi42oXF0jS3W5myvKH9aV3lMmwubd4yDfVtjbe1Hx3Rr+/6yRXWeA7+8sbb66zmm0fL67TyP/hiKCm3Lntc5lkB/wSqXq8/bAn0G3w/Lji24vqxnzkuoD3ue124f696+O85/XWX/BDmKopYp80MLXQHO/IoQE978AGd+dUguAU4/Qh0vwMnne/u7O3b7+V0vwOjv/1gf3s7FuHt9faEr1jV3/cx0ffHzy7vg6LE624pdbPxxVBed6IXVX/682peO9L4123C2X7Tte/2X6/vHuOzb2Udnv/z96Jh2jnX7XJbl74tsrjkHboXGam9b2s30Ev12nsfQfvjbKpdJe52G+rMqet4W6FPOh/9+WGZsusr146+X6lhYnXQeT+c109e3tEfOf+y46f7tdSiKovqr/t5bJMDJ990kvMUCnH58WoS3nAKcZLdRA5yZ537vRf4Pu9S5rJRzgSiqa/2qrZ7X++Ffba/q61RxIXQvRpGLjZRcmKr19N2C1pfgvXLG3exPeXEMbV82YZYvWDtar+MFjnpZ+3i19qNj2jrWzrYNM4auc+CVO9aeY7pwv13nsWxr9eVsqxiDf/46xtI+/m7Fzlu8z8gYzX75DYP78avr9VL1URzDcLts3tp/PRb/9WbW89ftPv/B46b7t9ehKIrqriKrtcObCIS3WICTu29bFuAoKtPy7uJtb6WGQIqiqPUr+d6b/FNZoQAn33fzw1sowMl33yS8EeAoau1KQkr7rlH0UftWFQGOoqg8S773JuEtFOBMeEsJcObuGwGOotaxvMeQhDdTBDiKovKsIqcFA5wd3voCnHz3jQBHURRFURQ1QZlHp36Ak580fcIKb30BTsIbAY6iKIqiKGrFpcNbIMDJnbcnvv10coB7tPpnswhwFEVRFEVRKy4d3uzHp0+V4W1ogDP/7ikBjqIoiqIoaoVV332rApwd3oYEOPMvL2Qf4Bb/t1ApiqIoiqJWX054K+jwtmCAM/9wPXfgKIqiKIqiVlh2gJPvvMkv6V00wG3CHTi5+UaAoyiKoihqbUvCmwlwJrwtFOCq8LYxj1AJcBRFURRFrWPFwtsiAU7ffduk78AR4CiKoiiKWscyAU7CGwGuQYCjKIqiKGotq777VoW3ZQKchDcCHEVRFEVR1AqryG2t8EaAaxDgKIqiKIpau9LhTQe45vEpAa5BgKMoiqIoaq1K7r6Z8LZsgHucAEdRFEVRFLX6su++cQcujABHURRFUdTaVP2DCwS4YHAzCHAURVEURa1NjRrgqvBGgKMoiqIoiqKOvQhwFEVRFEVRmVUd4A4ODgAAAJAB7sBRFEVRFEVlVnWAe+ihhwAAAJABAhwAAEBmCHAAAACZIcABAABkhgAHAACQGQIcAABAZghwAAAAmdEB7rLLLgs2AgAAYP1IduMOHAAAQEbOnDlDgAMAAMiJDnDyn1AjAAAA1o/+DhwBDgAAIB8EOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyk3mAO1LnTs7U/DDUNqLDuZqdPKeOQm2rcHROnZydVOeOAm0AAGDrTRrgDuczNZtZlg5FBDgAALB9dIB7zZndYOPYJMCdPHfkTM/mh84yw+Qa4PxxT7QfAABgI+gAd931lwcbx+YHuKNzJwlwwWkAAIA4HeBeecULg41jcwNcILToR4fNI1Y30ByqudVW9uP1YdavQ2FoHTP/pDp3rghmrbb2elpHgNNBtF52rg7rttD2vb5PnnT2WW9H74fpZ8hYq+WsscbHBgAAcqUD3EsufU6wMdVdd90VZS/X/R24IozNrWm56+WEGD+4CCvA6eXt741VwaeetsNeFXpM0HO+c1a22eFRjzsW4JywZevavhc8/elWgEsda9lPPdbo2Eqh8wUAAKYRujanGuWHGFIH4T9CdUNaKXjHKPoIswo+83Y/Zd+mn0ZzF8wPV9V0aFvR7YsqYDn9FTq3PzTALTrWyNgAAMCxyjvA2eFEhxar3Q4x0QBVBp/yMWQgQHWGrkVDUUw1FtNv5zpeYPOnRwtwhjc2pw0AAExtc+7AecGjvBNnhxg78Byqc/6drCoA2su468j2E0JRa70q/MTCWLHdc96y5bpd27eXC0ynBri+sUbHBgAAjlN2Ac59pGhCiqjCR9V2sgg7zve3qoDmrueHkjLQ1O3OOv5ysVBU0MHSrFfM934wwOfsl/1TtdHtW4+Kq36d6eQAV3C20R5rdGwAAODYZBXgMIHgI1QAALBOCHBbTe7G2XcxyzuQ7vcMAQDAuiHAbTvvMS3hDQCA9UeAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAMBau/3227dO6DjYxglw17ww2JiKAJfmpptuAgBgY4WufUICzd133701JgtwL7mUADcFeXFTFEVR1CYWAa4xWYC7/NLnBhtTEeDSmAAXagMAIFdSBLjGZAHuzPWXBxtTEeDSEOAAAJtIigDXmC7A8UMMkyDAAQA2kRQBrkGA2zAEOADAJpIiwDUIcBtmoQB3OFeznT11FGprOVJ7OzM1m83U/DDUnuJQzWdzdRhsCxljmwCAnEkdX4A7UKdnp9VBsO14bFCAay7yxjZe7GMB7mhvR83mh6352oAA19lPsmEBbpxt5u9wPlM7e0fBNgDYdFLjBDgJY01WOH0QWsZHgFtYWoDbUXtH1fTRntqZWdNbIhjg5Fh0BbQBAW6cEGEHuDJ4d4Xt4w0u/eNbHX/b3mscALaI1PIBrgxvTWi7oM6eSAlmywY42U5qWEyzuQGusI13LEIBTo5DZwAhwHVYpwBXkHPF3UgAW0hq6QB34aw6ceKsuhBq60SAW9hyAc5cCCU4zKywUk3X/Md6bnsTImLzq8d9dVvTX2x+9xgC4+65s9gOcO3j0t5m1Xek3eyfHM9mnWqcejzN/Dps6PnWvjjT0r/83RtHIES2t7nIuZS24hgcNmPV45QwVE2HA2JkfLF9ThpbMY49PzC72ynH0rFtZ10A2A5S49+B85Xt5rP3xNkL1nw7wMWWC7WddaZnCwXIts0NcPoia6bLC2soODkXbn1Bt0NG6MIu8+1AZC7axd/90GLE5veOITBuZ7/a2gFOtmFvu9ymfVdHhyQnbET2r1q2GW/RNrfChD12f5+daXtMbv8hrW0ueC7rfTTBzdzJip4f4Y+vY587xuasL8skHW9/28I+dgCwPaSWD3CFg9PlNaB1R02C1wl19oKZtu+a2QGubzk/0PnLjGPDApycECMeQjS58LbuZFjLBdur+fU2GmV4qIJCK2BF5veNIXgB79YKcBJO7G2EtmnP69w/P0yVgncXVxzgBp3LVkjqm7aFxxe+o5o4tuTjHdq2zIuNFQA2l9QoAa5y4eyJ8jP39EE5rw52rjKMWQGuazlpC95hI8B16LqwJV5Y7T6C7R3zHeX22kHOm983htC4e/TegQtt057Xs39OmNKhzJ+utrUWAc4cxxEDXNc+p44t+XiHjo13PgFgS0iNGeBK5R0zHayi4Ut4AS62HAGubfQApy+EdjAoOBfTsr1Z51Dt6WX9+RIwqgtqcTHfq+db24zN7x1DYNw6MMT2Mxbg/LBi91luI77f1v7pv1vj9cJHeVeqORb2OJ02vQ3z99C5cfUGuN7jGDoGXdM2b3td+9wxtmbekOMd6M8JjACwPaSWDnDyQwzmjptmPw61wlzVfnDafmxq/71rObvtQJ3Vd/C8ACfjMNuN/b3qO2aLA5woL57NLVDvwqgvloE2Z75/8W3m2z8tGJvfPYbYBTy2n6EAV27b6UNCSL29oi//S/U9++eHKbPcThE87HBhP2Z022SfA8vZY7D0BzjRdRylrSuw+dMud3xd+xwZm3M8hx3v1rGRc8dPoQLYQlJj3IGrH51WnLtiOkCF2uwA17Wc39asU29X7tDFQpv9d9NfxAYFOIhQgNMBIRKOcAy8u3jpJCDGgyYAbDKpMQLcpiDAbZhggCvoOzncuTkGErr8u4H2HcV07p1IANguUgS4BgFuw8QCHI6R94iUEAYAw0kR4BqTBbiXXPrCYGMqAlwaAhwAYBNJEeAa3IHbMAQ4AMAmkiLANQhwG8YEOIqiKIratCLANSYLcC+59LnBxlQEuDTy4gYAYFOFrn2CANc2SoC7+sXPCTamIsABAIAYCTTbJnQcbOPcgbviqmBjKgIcAABAulECHN+BAwAAmA4BDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAS4joR8zBgAAmymUBQwCXEbkZIZ+4R8AANgsBLgNQoADAGA7EOA2CAEOAIDtQIDbIAQ4AAC2AwFugxDgAADYDgS4DUKAAwBgOxDgoo7U3s5MzQ9DbSM6nKvZzp46CrUNNFqAOzitZifOqgt6+oI6e2KmTh94ywAAgGOzMQHucD5Ts5ll6VBEgCPAAQCwnjYqwO3sHTnTs/mhs8wwBDgCHAAA62ljA9zR3g4BblEEOAAAJvW+970vOF+E2jY0wAXC19Ge2rEesbrB7FDNrbayH68Ps34dCkPrmPk7am+vCGattvZ6mglwehvFukdm2WH8AHdweqZOnL1QTR+o08W26iDmhLSyzRlTK8C5yzSBzmvX6/WtI9y2ZpwAAGyXT33qU+rlL3+5euc739lqk3nSJsvY8zcqwJkwoDl3tYowNrem5a7XbK4O9XQZqNyQJawAp5e3g1UV0uppO+xVAc0EPSeUlW12eNTjXlGAu3D2hJqdPiini8B24sSJOig14a4MUna4kjY/wM1mp9VB1a7DXz0daO9dp9wmoQ0AgFIoxMXCm9jQO3AFJ6SV9GNVCVda1RZ9hFmFsnm7n7Jv00+j3H4o3FXToW1Ftz+cH+DuvnBWnahC08HpE+rshSI41XfIZLpYxrkTV3HmmbtpVrszr689MC+0TQAAtpwd4rrCm9jcAGcHqerxZ92up9MC3KwIX627Yp2ha40CnA5NgeB2UAQ7E6AIcAAArA0T4rrCm9iOO3BeSCrvxJm7av5jzUO1p/uxHotWAdBepv0o1O4vEuBa65mQWI1t5EeoQh6jyqNT8yjVTEe/G1cFLT/A1Y9iqz78R6ihsBZfx9/mgTrL41QAADQJbl3hTWxUgHMfadqPPaugVLXtFGGrvgMnqoDmrmcFOD1dhq+63VnHXy4W4Ao6WJr1ivnyww4rDHDlY1QrLOnp6vGpWUbuiNVjKtrOBu7AnbaXsb7b1hHg4usUqnEF2wAAQKeNCXCIBLhjEQp1AABgLAS4DUKAAwBgOxDgNggBDgCA7UCA2yDrE+AAAMAqEeA2CAEOAIDtQIDbIAQ4AAC2AwFugxDgAADYDgS4DSInEwAAbIdQFjAIcAAAAJkhwAEAAGSGAAcAAJAZAhwAAEBmCHAAAACZIcABAABkhgAHBNx0000Atkzos0DceeedWyl0LLA+CHBAgHyYUxS1PdUX4D7+8Y9vlXUOcHbo3nSh/TcIcECAvHEoitqe6rpYEuDWy7Z8PhPggAUQ4Chqu4oA5yLAHX8R4IAFEOAoaruKAOciwB1/EeCABRDgKGq7igDnIsAdfxHggAUQ4Chqu4oA5yLAHX8R4DCtw7ma7eypo1Cb6GtfE/EPiPNqdzZTp/YvVtNVXdxXp4r5M+2UcprP71bzA22tivQv5fSzWyxZlbPtmdqtG0LV0b9TqctR1GbU6gPcHerdt+yr2y6E2hZxQd22f4t69x2htuUR4I6/NiLAHc6bi5NjfhhcHsdoYwPcRbV/Sl53u2p31w82Zdipg5MErVP7xRp6Qu3Wfy9KhzArfNXV1X9RTp92Bba9SP91pS5HUZtV4wU4CWq3qFuMd99hzd+UAHek9nbkc6IxPwwttxoEuFJmd+AO1Xw2V4fBtimUL9opX6jZ2fg7cJKRvGDTClcSgmJ32iRwdd+Fa/Xf258d2BbpP1ypy1HUJtQoAe6Od+vQ5oSqYt7+bReKv29agNtRe0fV9NGe2plZ04MNu7YS4EoEuEEIcL0IcEWVd7GCjzKjd8iaavUvj0iL/veL+eb/du32i/unmmnpv/sZarv/SBHgqJzrYx/7WPW3doXalg9w5Z23eKDa4ABXkCdlO3tH1jJDEOBCtR0BTqf/0K1c86KQ9Yq2OjhU01rxItzzQ4Xdbl6U7rxwCJFlqv6cdav26Dir9Q6bdt0mYaeadt8YofGZNp83buf4xY6PJzpu4fdf6DiWbnvK+XH372hvx+qr2Zfg/CX+r3BQgKu+g+Y+xgwEOH+5SAUDor2e7qe5y3axCHDNvveHLgIcten18MMPqyuvvFLdfvvt1ZymZJ60yTJ2LR3g5O7b/m3qQqhNqwLcbeVdOvfxqmlvHr2Wd+262rwAd+E2tS/tTp+LWy7A+Z/7XdedHef6oq8DPZ/dBLjSBgS44sUwtwKDDj1mmfKF4r54yheNH/Lc8GC/cMyLzf97SPWCNN/Nc16EXeM0L+Sq3QQ3px972dj4fGW/TsDrPT6+/nHb29bfV/SCWLw9tP2O/XOOg6VzfvxDoMugACdVhSxN3y1zH2Ne1CGr+9GmqWCA877/Vi/jhbmixf1OXKAIcNQ2VCjExcKb1DQBzgpYOnCZO3L+3Tk7nJXruYHOW0Y/uh3z7t4yj1AXue5Yn/My3fPZTYArbcYduEL4zoz3ohDyQqoDRGCefqGZfhrlizHQnyMUPtwXYXic/nod053jM8tXQvvq7EPf/jSC4045ll3tsfNj7ZdR7p8cB5l2j2l8/uIGBzin3O+lyfJ9jzXtavVfPUK1t2iWuSjB0Ou7b3z94y8rdTmKWteyQ1xXeJOaJsD5Ia2a1gGsucPm3GmL9lsFuHfLuu9Wd7Tal9Mf4JrPZ+ezN/S533vdSb8WCQJcKf8Ap5O6FWCcuzGRgDA0dNT6XmR+8JLlq+nOcfrrdUx3js8TfSOZvhPeNF3jXuRYOvMSz09LuV47sMXmD7dMgHPa9R2yyHfeWnfPymr3X36nzu2zWq/rDlxq/8njoKj8yoS4rvAmtXSAawU0X0+Ai4W/ngB3S9FHcydvPIPuwNmCn+H28oHP/eC8OAJcKf8A571YyjtFHQFO92HPK5dp+vDb5bFfV382P3jJ8tV05zj99bqmu8bnK5d17s71BShf77iHHEu/PbT9jv0rwuOe15dermP+NI9Qza/eKDlt9qNVy6CApasMZs76prxt1G0EOIrSJcGtK7xJLR/gPq4u3LavWo8yiwAW/ilUK8BVj0ntH0i4493mrprfdoe6zf8OXPX9t3oZ+/Fs7O/VdmIWDnALXXe8eT2f3QS4Uv4Brjrx5uK1U1zsO+/ACf3iMOsULxL/hxicdnf9+lGivXxNxme/6OwXedc4/fV6pjvG1ybrNss24UtEjo+ja9wFeWPWfRdj9I9lZ3vK+XHbnd8JaP0ewOD8ng+BLtvyAUFRVFljBDjNfxwa/T1wdoArVCHMrGeHObfNBDsrwOnpMujp9mMNcGL4dce5tvZ8dhPgSpkFuBXx7jIBBDiK2q4aLcBtiO4Ad7wIcKUtDHCS/u3/Gyj/T8G53YutR4CjqO0qApyLAHf8RYAL8R7REd7gI8BR1HYVAc5FgDv+IsABCyDAUdR2FQHORYA7/iLAAQsgwFHUdhUBzkWAO/4iwAELIMBR1HYVAc5FgDv+IsABCyDAUdR2FQHOte4BbluE9t8gwAEBoTcSgM0W+iwQEma2UehYYH0Q4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMjMKAHuNQQ4AACAyYwS4K6+ngAHAAAwlVEC3CuvIcABAABMZZQA95Irrgo2piLAAQAApBslwF3+4suDjakIcAAAAOlGCXBXv/i5wcZUBDgAAIB0owS46654TrAxFQEOAAAg3SgB7sw1Lww2piLAAQAApBsnwJ3hhxgAAACmMlKA49eIAAAATIUABwAAkBkCHAAAQGYIcAAAAJkhwAEAAGSGAAcAAJAZAhwAAEBmCHAAAACZIcABAABkhgAHAACQGQIcAABAZkYJcK8hwAEAAExmlAB39fUEOAAAgKmMEuBeeQ0BDgAAYCqjBLiXXHFVsDEVAQ4AACDdKAHu8hdfHmxMRYADAABIN0qAu/rFzw02piLAAQAApBslwF13xXOCjakIcAAAAOlGCXBnrnlhsDEVAQ4AACDdOAHuDD/EAAAAMJWRAhy/RgQAAGAqBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzIwT4K6/PNiYigAHAACQbpQA9xoCHAAAwGTGuQPHI1QAAIDJEOAAAAAyM06A4xEqAADAZAhwAAAAmSHAAQAAZIYABwAAkBkCHAAAQGYIcAAAAJkZJ8Dxa0QAAAAmQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMjM2gQ4AAAApAtlqlSjBDgAAABMhwAHAACQGQIcAABAZnSAu/ynd4ONAAAAWD9lgLv0ucFGAAAArB8d4K5+MQEOAAAgFwQ4AACAzBDgAAAAMkOAAwAAyIwOcNv1a0QO1Xw2V4ehtqNz6uTspDp3FGgDAABYE5MGuMP5TM0PrXk6MM3UbH7oLLdaBDgAAJC3YwxwEqS8QDeJjgAHAACQgWMKcGV4O3nuqLXM6hHgAABA3o4hwB2pcydj4a0MdrNKs0y5zvywaj95Th3pZU+qc+fmgeW7+uoIcPoRqmmr+j+sHvMWdPg8jGzPPA62lzVtzliqMet9CLXHjg0AAEBplAB31113RdnLSYDTIcUJL0YVmOrvn5nQ1vx95gSvKvSY788531/r6kvaUgOcNVYT3JztmWWL/ufWPull3X6aQFftS30MusZaCh1XAACQN3OdX8RoAS4032ceoeog54c4686Wrbwb1Q414eBTTXf2NSTA2f33TT+kjs6dtLZX9SNjCe2rHww93IUDAGBzZRng6rtQ9k+fhoJObYEAF+1rBQFOr2eFLruflAAXHSsAANhEmQY4IQHIDnHltB3SDucmTA0McJ19SdvIAc4LYeWdOLsfeyxVeK2X99vtsQIAgE2UcYATZXipw0x1J8s8RvRDT3vdWIArRPuS9UYOcCaUVds6WQSwpp+CM5ZiHf+HGKJjBQAAmyirAIcKj00BANhqowS4l1xxVbAxFQGui9yds+/4yZ07fkgBAIBtNk6Au/SFwcZUBLge3iNSwhsAANttlAD3ykufE2xMRYADAABIN0qAu+4KAhwAAMBURglwZ66/PNiYigAHAACQbpwAd2Y32JiKAAcAAJBupADHrxEBAACYCgEOAAAgMwQ4AACAzBDgAAAAMpNdgPvoRz+qXv/616sXvOAF6pJLLtF/yrTMDy0PAACwabIJcA8++KC6+eab1bXXXqs+8IEPqPvuu089+eST+k+ZlvnS/sADDwTXDzraUzuzHbWn/0H5jn+kfjIyBjOeFEdqb4d/wL6Xc57FhMdtoddYOb742NbhtQoAOE7ZBLg3vvGN6s1vfrN65plnVKhk/q233qqXa6/fXLBrO3vqKPMAd7S3o2bzw2AbrOPjBbhJj9sxBrjD+Uzt7PHPrgHAJsoiwMnj0WuuuaaKat0ly7Ufp/ZdEMVxBDh/XMMC3PFeoFOO6aokbFuCk4T0QNvxHbdpA1zZT/rrCQCQjywC3Bve8AZ1/vz5KqJ1lywn34lz+0gJGwS4YVKO6ar0b1uOTax9ewJc4XDOXVoA2EBZBLhLL71Ufwcupe6//371/Oc/3+sjckHUj7fMhdC/KMp088g1fsE3fbvLO9vS2/Hb3OX1I109rwhwe8VF18yPXHwlhNTL6HF746jvPnnbae1jsb3DZnx6bHLRr6bD+x0aezE/uJ8iZWzVfjt3zdztlGOJbNsh27OCsHWe28fNXs9evlmu3I9yH+zjoR/F9u27te1y7NY2U49Xq93rJ3icqjbZRvAYAQBylkWAO3HiRBXP+kt+sEF+OtXto7wgmgtcfZGLXlzl7/adMHNBNdM207d1QdUByEwX7XPrAuq3Of1WF2IT2vT47HG43DtJgXFU/TkXdGf71fbMBd4EN2f7dn82f+z9+xkamx9a3HAXOwf+tn2yrrUtbz+678B17If8vR6fN57YOtHXWMc6Vd/O8XLa7X66jpO/LABgU3AHLnRR1BdLuYC6whf9UN/tefpuTd2X2WboYutfjIcFOGccTuAw7OX87fVN2wLbK6TtZyE0Nnte5zkIb7sm59Xue1CAK4X3wzoeXp/RdaIBrmOd4P75561atve1KuvFX0MAgDxlEeDkO23yq0JSSpZL/g5cV4BrBZ+Ynout3oZ1QXW26a/rB6bui+/iAc706W+vb9rmbW/QfhZCY7PndZ6DQH8O61wKZyw9Aa5zP5p1JXi5y0TWcda3xtW5ndD+2fOsfnpfq96xAABshCwCnPxUqfyet6effrqKaeGSdlku+adQYxdX/Xd3+cN57CJY9m1/V628qxK+wDptrXHJdu3AJO2xAJUQ4Kr9aJYpOOPxt9c3bfO2N2g/RTm2Zl65jDs2d53mHIT6s3njds5zT4Dr3I+C9LUzV3P7vHStE3uNJRyv6GvK7qfzOBW8fQcAbIYsApy45ZZb9O956/s9cLJce/3IBT8a4Exb80gqHhaqvouLZvMIy75gVhfjqm2nWM6+oJYX5qJNX8y94KHXjQWolAAnygt8eGz+9vqmXe7Yu/YzMjbnGBfb8X+IoeMcuNtu5htybOrlvRDTGeB6zlfd7vxwScc60ddYwvEq5pn29nmzprteqxIUnbECADZBNgFO/oUF+19ikO+6yQ8syJ8L/0sMo6gutq3ghMG8u1JLkVAzVl/ZktdmPIADAPI1UoDbDTamGjIIeTwq33GTH1SQnzaVP4/330IlwC1Gjpt/V6nrzthw+i7dFt996r7TCADI2TgB7vrLg42plh3E8SLALcx79EfYAAAgzTgB7poXBhtT5R3gAAAApjVKgLvuiucEG1MR4AAAANKNEuCufvFzg42pCHAAAADpRglwr3zxNn8HDgAAYFqjBLiXXHFVsDEVAQ4AACDdKAFuit8DBwAAgBIBDgAAIDMEuGN20003AQCw8ULXQHH77bcjIHSsbAS4YyYvaoqiKIra5OoLcHfffTcsBLgMEOAoiqKoTS8C3DAEuAwQ4CiKoqhNLwLcMAS4DBDgKIqiqE0vAtwwBLgMEOAoiqKoTS8C3DAbGeA++tGPqte//vXqBS94gbrkkkv0nzIt80PLr7vtCXAX1f6pmdo9X01SFEVRW1MEuGE2KsA9+OCD6uabb1bXXnut+sAHPqDuu+8+9eSTT+o/ZVrmS/sDDzwQXD/oaE/tzHbU3pFMH6r5bK4OQ8utUDvAlUFnNrPtqnDuOa92Z6fU/sVqcsE6v1tsw0tWF/dPqdmp/WI0TYXmpRcBjqIoaluLADfMRgW4N77xjerNb36zeuaZZ6qXg1sy/9Zbb9XLtdc/Uns7Xija2VNHaxzgnKBzflePORR+JFSdWj7BecHMhEg3HErQW3xbBDiKoqhtrTEC3MFp7zp+4qy6EFhuE2xMgJPHo9dcc031MlDqwx/+sPrJn/xJ/QhV/pRpU7Jc+3FqGeDmh/Y83xoHOKmL++pU6E6czF/4rlhVum87rMmdvV21WwS2ZhwyrmXu9hHgKIqitrXGCnAnzl5wpmenD5xlNsXGBLg3vOEN6vz58sr/kY98RJ04caLFhDhZTr4T5/axAQHOnu8ELn/5ctq+U2YefZ6XP60Q2Dw69fqQO3LFhF7PzGwFSAl51v8JOW2mv2oZHTC9bej+zParMQb7oiiKotatPvaxj1V/a1eobRUB7sLZEwS4wPxUkwS4Sy+9VH8HTkruuIUCnMyXuv/++9Xzn/98r49IgNOPUE1o8wOcTJtAMVM7e0fWOuax6/KWD3BFVYGrLueRqNtf/RjUe2xqhzVZpt6OWcbZRhnMnMep0l4Hr3Kb4VBX/FUva42/FQ4piqKoda2HH35YXXnllTpk+CXzpE2WsWv8AHdBnT0xU6cPTLuZPlCn5bpdP16tpmun1UGwv3K5ur+D03UfOigG1vf7tsPlsiYLcC958eXBxlR9g5CAZkoem/rhTch8KfnBBvm720f7O3A6kEUDnPzdDmlWADzWABd7hFk+8mxWkelq2eidM6+vOqzZ22n+7nz/zQt/ZdnjDu1DNW/XDnqmImOiKIqi1rJCIS4W3qTGCnD2ddz9DlwZ4EIBywlWEsyqZZw7eMV8nSeqZetwd+GsOuH0aUjfJ9TZC2baD5TLmSzAbdwduMO5+yKp1HfhRpQc4IKhqSn/BwzMdPuHHGJhqQqB9l03mavvxjVBrpoZCXB28AsHuFmxjPt9O7uqZQhyFEVRa192iOsKb1Lj34ErWGEsGKCsu2j1PHs5K5wdnJYwVoQyvbwsY8KZuctmh7WC3naZDWxj3YXbmAAn32mTXxUi9ed//ufBAPehD31It8tyyd+B6wpw8lOq9rIrkhTggo8cvZDjBa9yelft2sGrKDvY+SFMh7XdXTfwybaLebvOsmUIbC1XLxMPcHqeHr/VXkzv18uG1qUoiqLWsUyI6wpvUisJcM5dsCEBzl4nENwOimAXCX51kAv2PZ6NCXDyU6Xye96efvpp/UKQsPayl71M/eAP/qD+04Q3aZflkn8KtfMRqrv84bxqm+gRqpPq/btdoQCn17PnVf1YSUgCmh+yWt+ds4OVruqOXStRmTt5hv1YNBTC/Hlm/XI9PTbTV2tbFEVR1LqWBLeu8CZ1LHfgYo9QreAlj1HlJpB5lGqm63UunC0CXbWus42yb3t7B6erseg7e1XQi/29WidmYwKcuOWWW/Tveev7PXCyXHv9oQHOtDWhol535QFu8XJ+apSiKIqi1qTGCnD1/+hr9nfTQgFOmEegoXUKOlRZ6wVClrNd+6deq3VNW7CP2N9NHxEbFeDkX1iw/yUG+a6b/MCC/Lnwv8SwBsYMcOVdLf8HBCiKoijqeGuMALdNNirAGfJ4VL7jJj+oID9tKn/yb6FSFEVR1PoWAW6YjQxwm4YAR1EURW16EeCGmSzAXX311cHGVAQ4iqIoitrcIsANQ4DLAAGOoiiK2vQiwA1DgMsAAY6iKIra9CLADUOAqzz4P3/HUkJ9joUAR1EURW16EeCGmSLAyc8vrEWACwWvKYTGMoS8qAEA2HSha6CQsIK20LGyZRvgQmFqHYTGCgAAMKZlA9xll102XYALBaYUob6GCPWZItQXAADAspYNcCv/DlwoGMWE1l+10DhCQusCAAAsYm0DXCgE+fx1jltojL7QegAAAEOsXYALhR6bvd66C43fCC0PAACQYtkAN+p34EJBR/jL5ya0T0ZoeQAAgC5rcQcuFGxEaNmhZrPZ2vwZ2keRuj5/bu+fAADYjj3AxQLNJtvGfQYAAOM51gCXGmJmv/Tpwep11/gORur+Y3txBw4AEHJsAW5IeJFANqTsALfuhhyH0qGaz3bU3lGobRFHam9npuaHoTb0OtpTO6OeDwAA+o0S4OQ/ocYYP7T0DSI1wH3kEw/pP0N34A7nM/332s6eOqqWOW7+8Qgt0yDArZUVBzjuwAEAQiYPcKGwMkaAO/dfv6KX+/x9jwfvwEmA29k7cqZn80NnmePkH5M4AhwAANvuWAOcmbdsgDPhre8OnB3gjvZ21irAidCxaSPAbRPuwAEAQkYJcPLL4EKNIaGQkhLg5M6a8MsPb1J2gDPcAGeHlvLvdrh76HBePWI1y0loah6/OmFHP0ILt+mQWLfN1WHP/PaxcbdbjrEKcHvFGE2bE0RD63S1eQHO7E/VZ3Cseplm3O50Nb7D5rjovuWYVtPumHyx8bvz7eM2bJvLnFNvXXmNeMcidm6Txm+d0+5jBADYdpPegWsHlFJKgPsPf/hl9b/c/BknxIXCm5Qd4ORiKH92fgeuDmzltCxbXrTLC7ZzsdWhwEwX7XOvH9PmhxwjNr9iHyO54Lcv5FUQMKFN92fuyFVBoL47Z4ezcr12f9YyevzW+qn74ExX4zPH04QoZ7yx/Y+NMTDfOQ9DtrnEOQ2ta/cd3bfE8QfOqexH0w8AAKVsAtyjjz+trnj75+sQFwtvUnaAM/xHqO0LqB2CzHw7ABnteeG7LtVF2QlUXfNL9jFyQmbNHquQ8VTTJrh49H5LW7C/an/m9vEwImP1g4oz7Y+vb9oSG2Nwvn0ehmxzmXMaWLe174HjNXj80ub1AQCAJZsAJ2VCnEzHwpuUHeDkAix/tgKcd9GUC7a06wu3uRPSd7HXF2+rXz/YWMu3A1t4vn2MFgpwwXW62qpxFH3Ef5rSG+vaBDjTz5Btlvuy2DkNrJtyzgePv2kzr18AAGyjBLjU78DZ4cSenxrgpEyIi4U3KTvAGd134ApyId6Zq3l9URXVhbgOdObOTLWed2F22or+9uoLvXXhj82v+rCPkdzNadoO1Z4ef/xiX7a5/R3OzT76baY/d2wSXuplOvbBDnvOfrfG1zdti42xnN86f/WxH7LNcj8WOqeB81Uei55zPnj8sm7sGAEAkNkduNSK3YFrHokJc1Fu6GWcOyXVRVg/XgytV4WBqm2nWM6+G+Ns0woMsfnCOUZVoCqXNf32XOyddUJhw+/PDhqiDBumPTbWMtiU89399sfXN+0JjlGYccXaUre5zDn1j1VBj7dZP35uh4y/OaeybNMHAAClSQOccAJKNW+VAW4Iufg6d0lCF+wVCh0bjG3acwoAwCpkE+CGqtdNvYPh3UkpEeA2T14BjjtwAICQYw1wJqgsO4hlmcde7Yv6dBf70HHBKnAHDgCQv8kDnPDDyioDnLmDsc5/+scjdT3+3J4/AQCwHUuAE6HQso04DgAAYKi1CXDbGF62ff8BAMBi1uIRqi+0zibZxn0GAADjWcsAZ4TWzVloH0VoWQAAgJjJA1wowPQJ9ZOT0D4ZoeUBAAC6LBvg5F/RWjjA9U377H5yENoHI7Q8AABAiknvwIVCjD9PBiT8+T6z3LoJjdUWWgcAAGAoP2cNsXCAS5nvt3Xx15tKaCwhoXUBAACOw4/+6I+mBbiuQNPVZvOXSxHqZxGhvvuE+gEAADhuZ86cGR7gFmn32cuvk9BYAQAA1klSgEsJOSnLdPHXn0poLAAAAOtscIALtRupyw1h97mMUN8AAAA56g1wQ4LQkGUBAACwmEEBLtTuG7o8AAAAhukMcHYYSw1ki6wDAACAdMkBLtQes+h6AAAA6BcNcHYIGxrEllnXcThXs5Pn1FGobV3kMEaoh47OqZOzk+rcUaANAIDM6AAn/yCq37BsAIuvf6TOnZyp2fzQmx9AgMNYCHAAgA3yqle9Ss1OX32dM9MOXz57OV9oeaNeTi6kJ08WF9O5OrTWDco+wJVhdX4Yalu149z2Otj2/QcAbLJLLrlknAAXWtZmljs6V4S3c0fqcJ5wcSXALYEAR4ADAGwq+fpb0nfgDH85I7Ss0SwnF9XqMVYw+Byq+WymZja9THkxluBnlpUgWK+vH4816zQXbemv2N65YltVm92Hv72mLTa/3aYFA5y3XO9YTeCo1qv7tPup9sXZXmiskW179DGsl7PviIb6lPn+GOdq3nVekvsJj6+1H84YvT4q5fH01pP+9XGP72N7/+Ovm/hxAwBgGr2/B06Ew1jjrrvucpaRacNZ1glt1UWy/k5SeUG175jIXbp6eWddc/Gu/j63AoAsV19Uq4u0+b6d8z2oss2+MJf8cdnb6hlji71uNR0da7lsO0h468sy0WNob8/ftqcVaIz+Pp0xRs/LwH5ayn13zk/f8Qq01/vv7G9f32V78HUTPW4l+7UPAECX0HUk1UoCXGgZIWHHvmg6004QqDjzrEAQuIiG74qEQkQ1Hdqe0Bdy009Dj7N3jD4vRFTCYw0s27e9rrFGtt2oQopzfAqD+4ycl6XGVgjtu7NeqI+Odn9snX13vG5ixw0AgAEyCnDmwufruKh680zgkwBUBoFivr4wm2BgppcMcKH5sbau5YMhIjZWb1nRt70h244ql6sDyQJ9Bs/LsmMLri/rmfMZ6sOe57UnBTjTd1eAs+dZx62eDwBAv3wCnFw067BilKGuvMjafxfVBdK+0MpFWH/vyrpoehfj8u5WQoBrbe9QndPhw58vAcXur2eMjrK9Xr5zrN6yWt/2usYa6s9SHMtzXr/lsgv0GTovy4xNK9evA6Fwjl91LKxfR9N5PJ2w3Ne3tEdeN9HjBgBAumwCnP6uWOB3vznzdcgrprXigtn6wn77ol3Pq9Y7WYSEpDtwMq0v6mZds44/37tA947RVYaKYlm9TNdYI2HAGUtgex1jdbfdzDf0sTfr2sc02mdkjGa//PM7uB9fGbTM+u7/AFR9FMcw3O7tvx6L3d7Vd/frJnrcAABIlM8dOIzDu4u3vVJDIAAA64cAt9EkpLTvGjmP/rYWAQ4AkC8C3KbzHkMS3gwCHAAgXwQ4AACAzCwb4CS7EeAAAAAmtGyAu+yyywhwAAAAU1o2wF111VUEOAAAgCmNEuAuv+zyYKPRF84IcAAAAOlGCXDz+bXBRqMvnBHgAAAA0o0S4J73vOcFG42+cEaAAwAASDdKgBvyHbgUoT4AAABQyiLAnThxYjC/DwAAgE0xSYAToaAW468rgWxIrT7AHam9HX6LPwAAOB6TBbgufYNYNsAdzmdqNj905omjvR0129lb4B92J8ABAIDjszEB7otf/KJ64QtfqD74wQ+278AdztVsZv+j7mKZELYOAe44x7DNAXbMfd/m4wgAWMZGBDgT3mQcjz32WDvAPXSo5jPvQnm0p3ZaoS4VAY4AF2obigAHAFhM9gHOD29S7QDXfoyqH586j1XLkDer7OwdVfPNRbZq149cvXkV90Lstvl3APX2g22xcdi8vs1jYB1Km/nNeEL74Pezo/b25t4j5dBYItv2DN8/f4xzNS+m7f13H3mn9hMeX2s/7DH64b6e9tbRffe8Fgb0VY4LAIB+owQ4+QdRQ42pUgKcPBqVoCaBzVQovEmFApx7ITUXXdMuF9MiwByZabu9/LsbQgLznMe05cXZCV92u39Rr3WNw+e3FdNzK6w44wntQzlGZ31ZxglI3cckPK7CQvsXGKPsQz0ee9mB/bSU+558fjpfO5FxL9QXAABpRglwZ86cCTamSglwEtAkqJkQFwtvUsEAZ18s/YuqvuDKRdhVXuBDF9meeU7wCLRXAULf9apDSKFzHNZyWmgMsTtfgWVDY7TnDT4mtkX2L9SnFdTsc7bU2AqhfbfXGxS6QttbtC8AANJkE+Ck7BAXC29S4QDXPDY1f9ZtwQu60XOBDs2LBgQv0FTr1EGncxw+bww6GFhhry8ohLZlzxt8TEKG7F+4T3n0Lfsk56zet2XHFlxf1guERdF5LEPbs+YN6gsAgDRZBTgpE+Ji4U0qFuDKi2cRBFpBqrxjZF9ID+ddF9lyXus7dfWFOfKIzoSGYhx7dX92/13j8Hnj8kKJO57QPvjbqvap7mPoMbEstH+RPuWc6e/D2edsibFp5fpd50deJ2Z73ceynI6+Fgb1BQBAmuwCXEpFA5y52LbuvhT0hVbuFpX8C7R7ka3mFaGheYRnLspGGRJi7fqHKkybfTcwOo62MgwUy+n9qfatWm+nGFvvnR5nW0XI8H+IoWMs7rab+cbw/YuM0eyX3YcY3I+v+/zU+1dwj2X4uHe9FlL7MssDANBnywIcOnU+mkRYamAEAGA8ywY4yW6TBLihQv3AJsHDvlMUeKyIBAQ4AMD0sghwWBHvMSThbREEOADA9AhwAAAAmSHAAQAAZIYABwAAkBkCHAAAQGYIcAAAAJkhwAEAAGSGAAcAAJAZAtwxuummmwAAwIqErr3i9ttvX3uhcdsIcMdIXlwURVEURY1ffQHu7rvvXlsEuDVHgKMoiqKo1RQBrhsBbgkEOIqiKIpaTRHguhHglkCAoyiKoqjVFAGuGwFuCQQ4iqIoilpNEeC6EeCWQIBLqYtq/9RM7Z6vJqlMivNGUdTxFgGuW94B7mhP7cx21N5RoG0CaQGuvBDOZpZT+8XcWAWWn+2qpOvoxX11ql72vNq1+jAX4vO7xbR3Vb64f6o1pmbeEuPRNVEQqPZ9d4z9C52f87vWMtb+6+2a+afUfvDElufiVKsxMt/ZVqRP51yvoiY6byuv7vM7/P2QVrrfyDbTyn7/rvI8U9T61soD3MFpNTtxVl2Qv184q07MTqizF7xlFrR5AU4HLutDbdnwlVGAs68RoYtGU4ELZ3VB77uYykWmDAPlh3+zvPRZXQSkL+diYi5wblCQMZZ9LT6esgLrr6DqfR9h/1rnp9WnKe84R5bTF/9iuSaomTGVgbOZL1X0afehj3X7At6c61XVNOdt9dVzfge/XtLKX959TfUdW2lvznnr9UhRW1LDA9wFdfaEvH9tp9VBa7kKAS5xEIfz4kB6YasKdPNDa16nI7W3M2T51Vo0wOmLRvQDOfLh3nvHRcJEddGRZYOBoyjdj31xkvXKINFsU8Zglll0PKYi649a3r4vu3/O+bGX9Sp48feWrc7FvndBN9UfDKx9qys0b+ya4rxNUT3nd/DrJa3886pDvPOaGnBsW68zitqOWjTAnT7w50fYAc4xsJ+ADQpwh2oeC2o62M3VoT8/aBMCXDkdv2jHPtyt+a2LTlHOh7xchGIXCK//6mLmXGCccJYwHl3lNs3/9TT7Z5Zz2+v1nG0V5U87/Rb7vB+4mDn7vuz+ldP1+GXZKoC19q11YQ33JdOxoNYb4GQbzvEoytquu7533p3xucff7dOMs1pGr+Ptiz5mRVs1w9xVdPvy+qk0x6Mo00+ozRujv1/t+SkVPidNH1577+slrdzzYm/DOw/O6ydcva8RisqkPvaxj1V/a1eojQDXTQe4l1z6nGBjqt5BSEjb2VNHoTYd7sydORPQysBnPuTKwObO0/3pO3h2+POWabUV29mTwFi27+wdVW0PqaO9nfZ6PY9ohwS4pu++/5P3LzimrPn6guL2Ix/yzjr6wi/ba1947ItTvV4VVHSX1UWsrITx6IuSPR67rfy7Mw49tmravzg60+XFrtl21Zd30fP3fZH9i56f6jg2i1vHXv/davOWtS+89t/tis3X5fdflb2/TtAotn/qVPNotem7PI7OduxzEDpH1TzdtV7WOi7OObKr51xL+6517py2wBh1db22UsqMKXJ+ixr2ekkr6afZZsF5zSbsQ3XuZd2Bm6aotayHH35YXXnllTrY+CXzpE2WsWv8AHegTtvvS+E8QpXHrd4ywYDXb7IAt/I7cJ0Bzr6rVv7dCV7OHTrvDpwT4MrwZocyd90q3M0PrXWrcNYKgpXBAa68CJUn3lwo3A9r/cHe/ckd+XCX+e2Lj67oBVWaiouTjMfusL442X02f28u/GZ+z3j0hdjsd6PsI7S+Nc8fuz0t/XphrTUvtO9L7F/r/ATG4Kxv73ux3P6udUys9dxtNhWbX543M3arOo7Xeb3t4jXo73voOOp2s9/uMSirmrcr++cd32KqfJ374+vox5pXvya1jnMtZR9fS+iYNeMSZmzu9lvnV2rQ68Wv0DYD6+j9MMcxdJzipcccOjYUlVmFQlwsvEktGuDsz4rZ6YOqrQxmdrg7OF20twJc0w+PUEVvgPPvwPntbsALBrjgNuzlqztwdRizt2vu3MXDWsiQO3DNh3X5gR//8I58uMcucEU5d2GC5W9TpouLSX3hquYWF4rd8/ZFTCphPB1jC69vzesIJMF+vXnhfV9m/7xj5fUhFb+gV9ut+nQ+RIyEvvQFu7VPZbX31+xPsW3ddzV93hp38PzYx8E/BlLVPhTLtB7X11UtEwlLZVnz9Lm19rfvXEt1vrZSyh+Td351DXm9pFX7vMo2uo5TV9nrUlTeZYe4rvAmNeoduNDj0tYPMRDgAsqA5AazStcdtta8RQKcHdJiAc6eJxektCC3WICT61ZxEY5elAIf7nIRsz/A9YWv42Ig7W4HrQuAvjjt7rbvEhTznJ+CTBlP4KJ4vuinnCzXt0NHeQemanf2xWtr9Vv1VY8vMLaqltk/9/yU7W7ocI+lqfZFu6lYW2u+7t/sv1/h/ZXxyqNTc4zNdNNveRyd7TjBKNSvNU+PyWovpvfrZe11y79Hz7WzTa+tda7Pq3093q7XVkq19809v2Wlv17SqnVe9XvGjDt0vK2S4+0fwwXGQFHrWibEdYU3KQJcNx3gVv4duEL5HTMvGOkAZge2KkCZx5z1epGA5zz6LENi6xFqHeo6AlzRz543Br0N3X88zC0a4My88F2Wqq3Yl9hdG/0Bb0KE/D3w4V5eHJs+WpvSFxR/fnmxbN/hafrRQheT6iJvlmm6qPa/uBA2fbgXYHusp+Qiarc7/Rb7bP8QQ2TfdQ3cP3c5//xU61X8fTPznQu2V8kBrhq3T28ztr/VMarHpaf9kOnug3sO4segmWfWL9eTcdd9NRsu14mea+94dZ7r2Hx/nH0V3zfndZD8ekkr5/ho9nGQXape85HXr/v+ddelqE0oCW5d4U1q1ADXeoRaLpsU4HRb9StGYn/X6zU26A5cpQpszQeTH46q8DSXu3JmGRPQSvUPG0gwcwKcMI9CQ+t234E7nFvrhb4nV/fTSAtwq61YMNjYkgttddHbtn1f//0NhSWKoqjFatEA12QAYQUsueNmzz8buwN3t7pw9kS5nLQT4FJ4d9jW3PEHOLlDEH6ctxklgcC++1DeESlDzKbvu1857C8BjqKo8Wp4gFsfBLg1tw534Da+vMdnW3W3MbsiwFEUNV4R4LoR4JZAgKMoiqKo1RQBrtuaBbi8EOAoiqIoajVFgOtGgFsCAY6iKIqiVlMEuG4EuCUQ4CiKoihqNUWA60aAWwIBjqIoiqJWUwS4bgS4JciLCwAArEbo2iskIK270LhtBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMjNKgHsNAQ4AAGAyowS4q69fPsABAAAgXShTpdIB7pXXLBfgAAAAMB0d4F5yxVXBRgAAAKwfHeAuf/HlwUYAAACsn/I7cC9+brARAAAA60cHuOuueE6wEQAAAOtHB7gz17ww2AgAAID1Uwa4M/wQAwAAQC6qAMevEQEAAMgFAQ4AACAzBLhUh3M129lTR6G2mKM9tTPbUXtHgTYAAIAFjRzgDtV8NlMzY34YWCZTnQHuSO3tzNT80JtPgAMAACswXoCTgFOENifEFPN29o7c5XK1SIADAABYgZECXHnnbaMDDAEOAACsiXECXGe4MbzHq7O5OnTadtTeoTxyLNt1GKru6onmTt6QZQv6MWY5v17W7mcvsl5rvIWFHqGa/Ry2vbqNx7AAAMAzUYArw4kTWHTgssONFZBMGDPfoWsFodRli2A1t8YV2qaznglKZZsdyA7n1jZbUgNc1/bskGb15ywHAAAwVYALttuhxw8wXdNDli0d7e2UIU+zA5W9nIynmg6Nt3MfUwNcx/bq8TXcO3QAAAClcQJcIDQ5ogHOrNMXwuzpAcvqAOU/jlzTABftGwAAwDVSgDN3ueyAUiiCSRmeJLwEHqHWoaUjhLWmByzrBaNyjAmBSrfZgawMaCsLcK3tySPbaj3dh70eAADYdqMFOE0CUxFEas7vgStDStNugo1ps0NK1/SQZavgVW1zpwhFaYGq4OxLMV9++KAnwDXLF2TZ5ABX0Ms269dhjgAHAAA84wY4AAAArBwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIzCgB7qabbgIAAMAAoUyVarQAR1EURVEURaXVWgW4UBsAAMCmGCPvSBHgAAAAJjJG3pEiwAEAAExkjLwjRYADAACYyBh5R4oAhzVwpPZ2Zmp+GGrD+uK8IVereu3ynkC/MfKOFAFuSYfzmZrND1vzj/Z21GxnTx1588PKN/1sZulcN7D8bK4Og8t6jvbUTr3soZpbfZgPndA+hfanmbfEeLSJPvSqfZ+PsX+h83M4t5ax9l9v18zfUXtH1jq18lzs7B2lzXe2FenTOderMNF5W7nu8zv8/dDM66L7jWwzjf3+XdV5HvLelvHEXt/pVnW8Xat67ab26372hq4hrsjnQNdnS8pnRC3Sv0j6XOvb547+HanL5W2MvCO1xgFu6BtsVW/IHvrF7X+gLT/2WDAsBfqv3mR925QPvfLNUb5RmuWlz2o/pC/ng7Hcnv8hIGMs+1p8PKXA+itQ7/sI+9c6P60+De84R5bTFyPng8uMqQyczXxR9Gn3oY91+6LanGt3/nimOW+r13N+B79emnld/OXd11TfsZX25py3Xo+jCYxDv97CYxvlNbei4+3qO76LSug3dPyKeeF9Mfse+RyIfrakfUZ0919onQsjsO1F+h+83GZo5x3XnXfeGZxvkyLALc17IYvBdz4CY5c3xJAAJ3q3K2OtPgRl2eAbs2pzPixlvfKN1WxTxmCWWXQ8xhTnztv3ZffPOT/2sp7WB2Bg2epc7EU+uPovVNa+dc4b2xTnbQo953fw6yWNf151iE8OcJ7W62wskXHE3tvVa3mpcazoeLsGHt9kff3Kviy23dbnQMpnS63/86D9OdPXn33+F+k/LHW5nLXzTkPCmxFqN6TWNMCVL3L5vxStfpHa84sXy555AYeXN3c1SvJiK99crQ9Nvbx541V9OW+MbvKCs8OW+0Gcwn/Tt8fp8pcPzG99CBacN3y5n+EPEq//6mLm7JfzAZ4wHj3tnqdm/8xybnu9nn+x8KejrwvTXnD2fdn9K6fr8cuyRd8SwFr7FvyQbfcl07EPrt4PNNmGczyqedV23fW98+6Mzz3+bp9mnNUyeh1vX/QxK9qqY9h+/wX6qTTHw+on1BZ9DcXmpwifk6YPr7339ZLGPS/2Nrzz4Lx+wnpfI0Epx8w/NoH5et/NZ42/fDlt962PW7FPh/r10Rwz2YfyeHp9JB1v75it5LWbwttWxdmXhPMZ0jrHrb68fbLJsj370epfjknRf/BzrSDHqJ6uzlG9bkDqa3Sx13Je3LzTsMNbX4iTyugOXPmGaKbL9uYFHHpDBl6wzoveXqfqb9CbteJsyx93CrNto/v/ZOLbsObrMbn9yBvDWUe/qcP7bH9Y1utVb2h97Jw3bMJ49Pmzx2O3lX93xmF/4Pjn0pnue12U/H1fZP+i56c6jnX/9rHXf7favGXtDyv777bYfM3vv2Lvr3PhK7a/U/xfdXub5XF0tmOfg9A5ss+hXtY6Ls45svWca2mfW+fOaQuMsZ4fe22lMGOKnN/CsNdLGumn2WbBec0m7EN17mXd9H01YsfSFxuHNV+Pwzpm/rGQ6Xrf3P7q15+zzNDjPdVrN0VgW/ZYvP0conmvVvOq81+fG70dazq2XESrf78/7zzrc1S0G32vpVb/EanL5aydd9p33vxpn1Q+AS70wnfmectXb+r2h7H1Ye+8Uf31Q0yffr/Wur1v/lAf7rblBdx9MYiNVeb7+1vpGFf9RrS3KcvrY2v32fzdfZMljKf6MPCVfYTWt+b5Y7ene18XhdC+L7F/rfMTGIOzvr3vxXJ7c+uYWOu522zE5pfnzYzd0nG8DvW2i9egv++h4+jst3sMnPa57J93fKPvv45+rHnuxaHjXJv59bKN0DFb+P036PXiC39utNbR+2GOY+g4xekxh46NFth+7Fi2xMZhHwefbM9+Pch0tWzrvWjG5vU15HhP9tr1BY5r17ZkXvJxbwu+xuzXftFv/dlStUc/IwKCr0dvrPUy+jza/ZbHov06aQTHH5C6XM7aeacJbH3zDKkNDnBGOd9+EZsXiLy4mxdKbP00+o1SfOibP0PLxPnb7nszRMba8eHQPy5/mzJdfLDVH6TlfDl280PZvv3mTRhPx9jC61vz/A99e7r3dRHb92X2zztWXh+mn/CHULXdqs/6w9eW0JfMi53P9v6a/Sm2rfuupg+tcQfPj30c/GPQzJsVy/h3e1vLpF7g9Lm19rfvXHfNT+aPyTu/9bzU10ua9nmVbXQdpy72ugmSj1lkHD3r+/tmpuW12d5n+/Vhz0883pO9dlPEt1XOG3ieLO3Xi686ZtW0LD/kWtTqv+NzLfSZ2je+/vGXUpfLWTvvlGHNn9c1XyqfAFe90Ztp8wY0LzBv+eLFt+cta7ft7MyLi5n9RvK3N5D0Wbwx5fHU8Ddne9v6DRL9gAyMVT7E7A+GajzldGB5aXfegO0PFnkjyf+hOm8m2U4xz/kJp5TxtM6f9G8+bMr17Q8Evf/mw8jZF69t6OvCssz+ueenbK/78cZr6/pwirW15uv+mw9qV3h/Zbzy2jTH2Ew3/ZbH0dmOc2EM9WvN02Oy2ovp8Puv/Hv0XDvb7DvXh2pPj7frtZWivW/u+S2lv17StM6rfs+YcYeOt0WOt38MB40hdiztZURgHHqc1us79HqXefZ49LT/mdscg+WO91Sv3RTl8tHXdz3tHa9ivM74A1qvF4/Trvcp8h7Qbe3Pp3b/5b64fVbrtfqwXk+p/SePY/O0885wUmsc4MwL3boY6xNeTGvFife+rO4vLy+E+q6G9YYKvcmGv1F9VZ/OB0uq0LZDY/Ta6mMR2K7/ZguMqz5elda+6w9qf375Rg0eT6uv4HFwzp/db7X/xQdz04f74WOPdUc+1O32rtdFZN+1gfvXfX6q9Sr+vpn5XR9MsQ+u1vxq3D69zdj+VseoHpee9j883X1wz0H8GDTzzPrleuH3X7VO9Fx7x6vzXMfm++PsE98353WQ/HpJ4xwfzT4Ogc8/j/v+dddNEjuWDvd8aP54gq8lWc+e1z6eev/rvpY93lO8dlNU/XZ8lmn+ezhhG63PAXPMqj4GfUa0zleof+EeV+cYetuo21L7HzSOzRLKO0NJrXWA6yUvoNjFGbVteEM4rNfFtu37+u9v6GKKTaQD5qDwA2yHhfKORyqjACcf/Pb/yZT/Z7BVwWQhcpza/5ezObpeF5u+774c9pcAtz3k9Ri4+wRsuf68008qrztwzi1/whsqvC4yQoADsN2S8k4PqbwCHAAAQMbGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYyRt6RIsABAABMZIy8I0WAAwAAmMgYeUeKAAcAADCRMfKOFAEOAABgImPkHSkCHAAAwETGyDtSBDgAAICJjJF3pAhwAAAAExkj70gR4AAAACYSyjt33nlna17XfCkCHAAAwERCeUeCmh/WQvMMKQIcAADAREJ5x4Q1E9j8aZ8UAQ4AAGAisbxjh7au8CakCHAAAAAT6co7KeFNSBHgAAAAJtKXd/rCm5AiwAEAAExkjLwjRYADAACYyBh5R4oABwAAMJEx8o4UAQ4AAGAiY+QdKQIcAADARMbIO1JrFeAoiqIoiqKo/lqbAAcAAIB0oUyVapQABwAAgOkQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIzA033ECAAwAAyAkBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMzoAHf66uuCjQAAAFg/BDgAAIDM6AB32WWXBRsBAACwfiS7zSTFhRoBAACwfvQdOAIcAABAPghwAAAAmSHAAQAAZIYABwAAkBkCHAAAQGYIcAAAAJkhwAEAAGSGAAcAAJAZAhwAAEBmCHAAAACZIcABAABkhgAHAACQGQIcAABAZghwAAAAmSHAAQAAZIYABwAAkBkCHAAAQGYIcAAAAJkhwAEAAGSGAAcAAJAZAhwAAEBmCHAAAACZIcABAABkhgAHAACQGQIcAABAZghwAAAAmSHAAQAAZIYABwAAkBkCHAAAQGZ0gHvRi14UbAQAAMD6ueKKK9TspS99abARAAAA6+dVr3qVmv3ET/xEsBEAAADrZz6fq9nLXvayYCMAAADWz7XXXqtmL3/5y4ONAAAAWD+7u7tq9spXvjLYCAAAgPVz/fXXq9lP/dRPBRsBAACwfl7zmteomdyGCzUCAABg/dx4441qJv8JNQIAAGD9vO51r+M7cAAAADkhwAEAAGTm53/+59Xsx3/8x9XBwQEAAAAyoAOc3IajKIqiKIqi8qg6wIVuzwEAAGD9nDlzRs0kxYUaAQAAsH4IcAAAAJkhwAEAAGSGAAcAAJAZAhwAAEBmCHAAAACZ0QHu2c/+d8FGAAAArB/uwAEAAGSGO3AAAACZIcABAABkhgAHAACQGQIcAABAZghwAAAAmSHAAQAAZEYHOH6NCAAAQD4IcEAGbr/9dkyA4z0t+zUeMvulT2MCoWNvC507jC907LusPMDd8ZGPqrNveY962e7Njtff8n+q3/3D88F1ALjkzX333XdjhewPUI736qVcsEJhA+MLHXsb74fVS3k/+FYW4N5fBLerz7xJ/epbfk/9xUeP1EMPf0M9+eSTtf/26c+p/+N3/0yHuff84fuDfQAo8QG6evYHKMd79VIuWCZghNbH8ghw6yPl/eDTAe7SSy8NNi7qbQe3q+v/41t1SHviiSfUY489ph599FH1/33hy5r8XTz++OPqK//6gA55r7/5d4J9AeADdAr2ByjHe/VSLlgEuNUiwK2PlPeDb/Q7cBLeXn/zO/QdNwlu99x7v3r3H3xAveE/vdMh86RNgpyEPLkbR4gDwvgAXT37A5TjvXopFywC3GoR4NZHyvvBN2qA+7MP/pW+8ybhTYLZl79yn/rFX3+net0b3xYkbXd9/kt1iHvT225Tb377e4N9A9uMD9DVsz9AOd6rl3LBIsCtFgFufaS8H3w6wF177bXBxqHkO2/y2NQ8Mn3ru/9EXfeG39R+651/rP7xE59Vn/7s59U7fv999fz9d92uHn74Eb38w498Q139829WR5/4ZLB/YFvxAbp69gcox3v1Ui5YBLjVIsCtj5T3g08HuJ8Z4Q7chy/8jfrVWw/099q+8Y1vaK8qwph47a/8trr4uS/W8+22X37T76p77r1Pz5Pgd9t/uVP99nuG7wiwybbnA/SCOntipk4fhNpWy/4A5YK1eikXrLUKcBfOqhOzE+rshUCbLWm5A3U6pa8VI8B1kXN0Wh0E28aX8n7wjRbg3vT296qP/PWR+uY3v6keeeQR7Wtf/7r66Mf+m/q7o0+p+7/6QD3/U5/5nDp9wzntl869S33xy/fWbffc+1V9Jy+0DWBbresH6MHpmZrNLCfOqguB5dJtU4Ar93UWupAfnB7hWK63lAvW8QQ4c1681zUBbkJyfKzjf/ogsMyqbVGAk9D15XvvV18vQluXjx19Uv3M/3qresX1/0n7vw4/pO67/6t1u9yFk+/R3fn//m1wO8A2mv4DNI0EuBNnLzjTy33Ybl+AO306ENaWDnDHdxxTpVywjjPArfbYEeCi5LVfhDbn+Bfz7M+ZaWxRgJPf5ybfY7vvvvui/uuH/lq9/Oduqf3ab/6e+u+f/ZyzzMMPP6zeeOsBAQ6wTPoBOoAf4C6cPUGAS2b2tfzTuUAR4DQC3GqtX4Ar77ytx+t2ywKcfI/tnnvuCfrEpz6jXvXzb1I/+bO3aO9+7x3q05+52Frua1/7mnrjm99DgAMs032AWvSjoO4PUzfABS58VR/mUYjbV/lhbdrKfrw+zPoTPEKxP0CnOd7WvvrHuhXgQseqCszWRaa8A3raWTYaBJMe9a1OygVrrQKcPl7mWFcB7Gx5t8g+J+5y5hyZ8xFf/zgeE65dgEv5HxfzmVBpzlPHOanbm/Watq75WxLg5BHq3V+6V917b9iffeBC/c9ovf3gz9Rdn/tCcDn5HtzP/dJb+UlUwDLZB2h18dIfZgl3gHRgsD783HWKvk5b0/rRiH0B8z9ghXXx1MtPFzDsD9BpjrcbFJww5lzIqgtTfRzc9eoQ7azjLhNXLleev+kuViLlgnWcAc5+Xevj2wpwRZsJXXYYtpfzwlyjY/3WsquTX4Arzk3PZ0r4mMY+b7reW9I23Xsi5f3gGy3Anf2N96g/v/Bxdf/99zuPRI3f/YP31wHufR/66+AyQn6gQZYJbQPYVpN8gOoPvJSLfsN/hOp+oJaCdyCiH9TVB6h8L2zCD09hf4BOc8GyLxbNtL4A2cdHH1Nz/BrNca8uXB0hL8kC538ZKRes4wxwrePghLHQhb+abi3nnxszP7J+vczq5RfgSp13NUPHNNZv53tL+pvuMyjl/eAbLcDd8ZG/Vv/5t/9A30H76le/GvT5u7+k77zJn6F2eXz6Xz70/6g3v/3/Dm4D2FaTfYBWF6/yg6z/w6sV4OwP0SoQ1O32hS36QV1tv/jgnfqOhP0BOs3xLvfVCQomRNk/2NB7UQuFhEDfQcPO95hSLlj5Bzi73T5HHes7663W2gW41nHxdH2mdB3TrgAXfW9Jf9O9J1LeD77RApyQx6jyy3oliD3wwAOOf/rEf1evet2b1M/+h99Sn/iXi6127cGvq+t/6a3qnz75qWD/wLaa7gPUYsJERwjovAPnfTi639eSD0e77wN1VvdjXTwTtj8m+wN0muNt7as1v767UB87/1jJcW8uLOYc6PXqdcJ9O/TxnT40GCkXrOwDXPH3s3U/dr8d6+vpaaxfgDOvf+9YFJ8l+nOm9zMldkxjnzdd7y1pa95nq5byfvCNGuD+8H0fUf/bze9QD37tIR3ibL/3Rx+sH6G+9V23t9rlJ1j/99/9U/0oNtQ3sM2m/AAdQsKDeezQvotTXrBM24nig9G5M1EFNHc9+yInyg9Yt9/VsD9Apzne/r668507A86xatbRx98LbeY7QO0guF5SLljZB7iC8x4x38/qWr9abwrrGOA0/T+CoeNWvcar+e5nSs8xDX7e+PP9z57Vf+4YKe8H36gBTpx9y3t0EHvw6+W0+f1ud370H3R4u/K1/1n9+YW/q+eb3/32kb/+B36BLxAx+QfoFrI/QDneq5dywTqeALc91jbAbaGU94Nv9AAnXn/z7+g7cffce7/+lxnkd7vJ/AcefFDd/cV71L/e91U9Lb925Gtff1gHPsIbELeOH6Dm/1hjQuusM/sDdIrjHTpmIaF1N0HKBes4AlzoHIjQsrkjwK2PlPeDbyUBTrznD+/Qd9wknF383Jf1v5Eqj0nFt771Lf1PZv3Bn/2l/vdQeWwKdOMDdPXsD1CO9+qlXLCOI8BtEwLc+kh5P/hWFuCE/C43CXLX/8d9HebkH7sXEtrkjtvbDv5YffIznw2uC6DBB+jq2R+gHO/VS7lgEeBWiwC3PlLeD76VBjhDgpz8ywq20HIAwvgAXT37A5TjvXopFywC3GoR4NZHyvvBN0mAA7AcPkBXz/4A5XivXsoFiwC3WgS49ZHyfvAR4IAM8AG6evYHKMd79VIuWCZgYLVCx97G+2H1Ut4PPgIckAF5c2P1ON7Tsl/jIaGwgfGFjr0tdO4wvtCx70KAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJDgAMAAMgMAQ4AACAzBDgAAIDMEOAAAAAyQ4ADAADIDAEOAAAgMwQ4AACAzBDgAAAAMkOAAwAAyAwBDgAAIDMEOAAAgMwQ4AAAADJTB7h//29fp55VmL/2terqG2/UXnXDDeqq17xG/fRVP6e+81m76of+/RV6heP2Uz99pbrkkkvUzs6O+oEf+AH1/d///er7vu/71Pd+7/eqZz/72eq7v/u71Xd913epf/Nv/436zu/8Tu3HL/uJYF+O616kvudZz1LPqn2PetF17jLXveh7rPZnqR96hdtue8UPBdpf8UPO+s/6oVeU8/W2re3JcqYtY1dfffXCbihef6af1xavS7tfAAC22xn1/wMbvs4+75P2tgAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
