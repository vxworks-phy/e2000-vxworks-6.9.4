/* ftPinMux.c - pin mux/demux hardware driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <vxWorks.h>
#include <stdio.h>
#include <string.h>
#include "ftPinMux.h"


UINT32 padRead (UINT32 reg)
{
  return readl(reg+PINMUX_BASE_ADRS);
}
    
void padWrite (UINT32 reg, UINT32 value)
{
  writel(value, reg+PINMUX_BASE_ADRS);
}


void pinSetDelay(UINT32 reg, FPinDelayDir dir, FPinDelayType type, FPinDelay delay)
{

    UINT32 reg_val = padRead(reg);

    if (FPIN_OUTPUT_DELAY == dir)
    {
        if (FPIN_ROARSE_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_OUT_DELAY_ROARSE_MASK;
            reg_val |= FIOPAD_X_REG1_OUT_DELAY_ROARSE_SET(delay);
        }
        else if (FPIN_FRAC_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_OUT_DELAY_FRAC_MASK;
            reg_val |= FIOPAD_X_REG1_OUT_DELAY_FRAC_SET(delay);
        }
        else
        {
            uartf("%s,%d bad type %d\r\n", __func__,__LINE__,type);            
        }
    }
    else if (FPIN_INPUT_DELAY == dir)
    {
        if (FPIN_ROARSE_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_IN_DELAY_ROARSE_MASK;
            reg_val |= FIOPAD_X_REG1_IN_DELAY_ROARSE_SET(delay);
        }
        else if (FPIN_FRAC_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_IN_DELAY_FRAC_MASK;
            reg_val |= FIOPAD_X_REG1_IN_DELAY_FRAC_SET(delay);
        }
        else
        {
            uartf("%s,%d bad type %d\r\n", __func__,__LINE__,type);            
        }
    }
    else
    {
        uartf("%s,%d bad direct %d\r\n", __func__,__LINE__,dir);
    }
    
    padWrite(reg, reg_val);
    return;
}

void pinSetDelayEn(UINT32 reg, FPinDelayDir dir, BOOL enable)
{
    UINT32 reg_val = padRead(reg);

    if (FPIN_OUTPUT_DELAY == dir)
    {
        if (enable)
            reg_val |= FIOPAD_X_REG1_OUT_DELAY_EN;
        else
            reg_val &= ~FIOPAD_X_REG1_OUT_DELAY_EN;
    }
    else if (FPIN_INPUT_DELAY == dir)
    {
        if (enable)
            reg_val |= FIOPAD_X_REG1_IN_DELAY_EN;
        else
            reg_val &= ~FIOPAD_X_REG1_IN_DELAY_EN;
    }
    else
    {
        uartf("%s,%d bad direct %d\r\n", __func__,__LINE__,dir);
    }

    padWrite(reg, reg_val);
    return;    
}


void pinPWM0(void)
{
   /* pin AL55: func1 */
   
   padWrite(AL55_reg0, 1);
}

void pinBand3Mio3A(void)
{
    UINT32 reg = C53_reg0;

    UINT32 val = padRead(reg);

    /*val = 0x71;*/
    val = 0x141;

    padWrite(reg, val);
}
void pinBand3Mio3B(void)
{
    UINT32 reg = E53_reg0;
    UINT32 val = padRead(reg);

    /*val = 0x71;*/
    val = 0x141;

    padWrite(reg, val);
}

void pinBank4Mio8A(void)
{
    UINT32 reg = N39_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 3;
    padWrite(reg, val);
}

void pinBank4Mio8B(void)
{
    UINT32 reg = L27_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 3;
    padWrite(reg, val);
}

void pinBank2Mio8A(void)
{
    UINT32 reg = AA45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 4;
    padWrite(reg, val);
}

void pinBank2Mio8B(void)
{
    UINT32 reg = W45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 4;
    padWrite(reg, val);
}

void pinMio14A(void)
{
    UINT32 reg = L47_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 6;
    
    padWrite(reg, val);
}
void pinMio14B(void)
{
    UINT32 reg = L45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 6;
    
    padWrite(reg, val);
}

void pinMio15A(void)
{
    UINT32 reg = N49_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 6;
    
    padWrite(reg, val);
}
void pinMio15B(void)
{
    UINT32 reg = J49_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 6;
    
    padWrite(reg, val);
}

void pinSpim0Cs0(void)
{
    UINT32 val = padRead(U49_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(U49_reg0, val);
}
void pinSpim0Rxd(void)
{
    UINT32 val = padRead(U51_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(U51_reg0, val);
} 
void pinSpim0Txd(void)
{
    UINT32 val = padRead(W49_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(W49_reg0, val);
} 
void pinSpim0Sclk(void)
{
    UINT32 val = padRead(W51_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(W51_reg0, val);
}

void pinSpim1Cs0(void)
{
    UINT32 val = padRead(J25_reg0);
    val &= ~0x7;
    val |= 4;
    padWrite(J25_reg0, val);
}
void pinSpim1Rxd(void)
{
    UINT32 val = padRead(J27_reg0);
    val &= ~0x7;
    val |= 4;
    padWrite(J27_reg0, val);
} 
void pinSpim1Txd(void)
{
    UINT32 val = padRead(L27_reg0);
    val &= ~0x7;
    val |= 4;
    padWrite(L27_reg0, val);
} 
void pinSpim1Sclk(void)
{
    UINT32 val = padRead(N39_reg0);
    val &= ~0x7;
    val |= 4;
    padWrite(N39_reg0, val);
}

void pinSpim2Cs0(void)
{
    UINT32 val = padRead(A27_reg0);
    val &= ~0x7;
    val |= 0;
    padWrite(A27_reg0, val);
}

void pinSpim2Rxd(void)
{
    UINT32 val = padRead(C27_reg0);
    val &= ~0x7;
    val |= 0;
    padWrite(C27_reg0, val);
} 

void pinSpim2Txd(void)
{
    UINT32 val = padRead(C29_reg0);
    val &= ~0x7;
    val |= 0;
    padWrite(C29_reg0, val);
} 

void pinSpim2Sclk(void)
{
    UINT32 val = padRead(A29_reg0);
    val &= ~0x7;
    val |= 0;
    padWrite(A29_reg0, val);
}


void pinSpim3Cs0(void)
{
    UINT32 val = padRead(W47_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(W47_reg0, val);
}
void pinSpim3Rxd(void)
{
    UINT32 val = padRead(AE47_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(AE47_reg0, val);
} 
void pinSpim3Txd(void)
{
    UINT32 val = padRead(AC49_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(AC49_reg0, val);
} 
void pinSpim3Sclk(void)
{
    UINT32 val = padRead(AC51_reg0);
    val &= ~0x7;
    val |= 2;
    padWrite(AC51_reg0, val);
}

void pinGpio4p10(void)
{
	UINT32 reg = AE45_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio4p11(void)
{
	UINT32 reg = AC45_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio4p12(void)
{
	UINT32 reg = AE43_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio4p13(void)
{
	UINT32 reg = AA43_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}

void pinGpio5p9(void)
{
	UINT32 reg = N55_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}

void pinGpio5p10(void)
{
	UINT32 reg = C53_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio5p11(void)
{
    UINT32 reg = E53_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 6;
    
    padWrite(reg, val);
}

void pinBank0Pwm00(void)
{
    UINT32 reg = AL55_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 1;
    
    padWrite(reg, val);
}

void pinBank0Pwm01(void)
{
    UINT32 reg = AJ53_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 1;
    
    padWrite(reg, val);
}


void pinBank0Pwm00_In(void)
{
    UINT32 reg = AN53_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 1;
    
    padWrite(reg, val);
}

void pinJ41Delay(void)
{
    /* RGMII need */
    pinSetDelay(FIOPAD_J41_DELAY, FPIN_OUTPUT_DELAY, FPIN_ROARSE_DELAY, FPIN_DELAY_7);
    pinSetDelay(FIOPAD_J41_DELAY, FPIN_OUTPUT_DELAY, FPIN_FRAC_DELAY, FPIN_DELAY_5);
    pinSetDelayEn(FIOPAD_J41_DELAY, FPIN_OUTPUT_DELAY, 1);
}

void pinAdc0Chan0(void)
{
    UINT32 reg = R47_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan1(void)
{
    UINT32 reg = R45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan2(void)
{
    UINT32 reg = N47_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan3(void)
{
    UINT32 reg = N51_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan4(void)
{
    UINT32 reg = L51_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan5(void)
{
    UINT32 reg = J51_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan6(void)
{
    UINT32 reg = J41_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc0Chan7(void)
{
    UINT32 reg = E43_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan0(void)
{
    UINT32 reg = G43_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan1(void)
{
    UINT32 reg = J43_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan2(void)
{
    UINT32 reg = J45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan3(void)
{
    UINT32 reg = N45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan4(void)
{
    UINT32 reg = L47_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan5(void)
{
    UINT32 reg = L45_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan6(void)
{
    UINT32 reg = N49_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}

void pinAdc1Chan7(void)
{
    UINT32 reg = J49_reg0;
    UINT32 val = padRead(reg);
    val &= ~0x7;
    val |= 7;
    
    padWrite(reg, val);
}


void pinAJ45Delay(FPinDelay largeScaleDelay, FPinDelay smallScaleDelay)
{
    /* SD0 CLK pin need */
    pinSetDelay(FIOPAD_AJ45_DELAY, FPIN_OUTPUT_DELAY, FPIN_ROARSE_DELAY, largeScaleDelay);
    pinSetDelay(FIOPAD_AJ45_DELAY, FPIN_OUTPUT_DELAY, FPIN_FRAC_DELAY, smallScaleDelay);
    pinSetDelayEn(FIOPAD_AJ45_DELAY, FPIN_OUTPUT_DELAY, 1);
}

void pinJ53Delay(FPinDelay largeScaleDelay, FPinDelay smallScaleDelay)
{
    /* SD1 CLK pin need */
    pinSetDelay(FIOPAD_J53_DELAY, FPIN_OUTPUT_DELAY, FPIN_ROARSE_DELAY, largeScaleDelay);
    pinSetDelay(FIOPAD_J53_DELAY, FPIN_OUTPUT_DELAY, FPIN_FRAC_DELAY, smallScaleDelay);
    pinSetDelayEn(FIOPAD_J53_DELAY, FPIN_OUTPUT_DELAY, 1);
}

void pinBank1Mio6a(void)
{
	UINT32 reg = AA53_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}

void pinBank1Mio6b(void)
{
	UINT32 reg = AA55_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}


void pinBank1Mio10a(void)
{
	UINT32 reg = C45_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 5;
	
	padWrite(reg, val);
}

void pinBank1Mio10b(void)
{
	UINT32 reg = A47_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 5;
	
	padWrite(reg, val);
}

