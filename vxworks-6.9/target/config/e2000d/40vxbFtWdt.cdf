/* 40vxbFtWdt.cdf - Phytium Watchdog controller configuration file */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component DRV_FTWDT {
    NAME        Phytium E2000 WDT support
    SYNOPSIS    This component adds support of driver for watchdog timer module in Phytium E2000.
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS
    _CHILDREN   FOLDER_DRIVERS
    INIT_RTN    vxbFtWdtDrvRegister();
    PROTOTYPE   void vxbFtWdtDrvRegister(void);
    _INIT_ORDER hardWareInterFaceBusInit
}
