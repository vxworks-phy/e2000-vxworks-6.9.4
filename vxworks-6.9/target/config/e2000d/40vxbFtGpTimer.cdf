/* 40vxbFtGpTimer.cdf - Phytium General Timer controller configuration file */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component DRV_TIMER_FT_GPT {
    NAME        Phytium E2000 GPT support
    SYNOPSIS    This component adds support of driver for general purpus timer module in Phytium E2000.
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS
    _CHILDREN   FOLDER_DRIVERS
    INIT_RTN    vxbGptTimerDrvRegister();
    PROTOTYPE   void vxbGptTimerDrvRegister(void);
    _INIT_ORDER hardWareInterFaceBusInit
}
