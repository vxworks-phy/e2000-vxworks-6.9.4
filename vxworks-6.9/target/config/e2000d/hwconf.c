/* hwconf.c - Hardware configuration support module */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#include <vxWorks.h>
#include <vsbConfig.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbIntrCtlr.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/hwConf.h>
#include "config.h"
#include "defs.h"

#ifdef DRV_FTI2C
#include <hwif/vxbus/vxbI2cLib.h>
#endif
#ifdef DRV_FTQSPI
#include "vxbFtQspi.h"
#endif
#ifdef DRV_PCIBUS_FT
#   include <drv/pci/pciAutoConfigLib.h>
#endif
#ifdef DRV_FTSPI
#include "vxbFtSpi.h"
#endif

#ifdef DRV_FTPWM
#include "vxbFtPwm.h"
#endif

#ifdef DRV_VXBEND_FTGEM 
#include "vxbFtGemEnd.h"
#endif

#ifdef DRV_TIMER_FT_GPT
#include "vxbFtGpTimer.h"
#endif

#ifdef DRV_FTSD
#include "vxbFtSdCtrl.h"
#endif

#ifdef  DRV_ARM_GICV3

LOCAL struct intrCtlrInputs gicInputs[] = {
    /* pin,                driver,            unit,   index */
    { INT_LVL_TIMER_GEN,    "armGenTimer",      0,       0 }, 
   
#ifdef  DRV_ARM_GEN_AUX_TIMER    
    { INT_LVL_TIMER_AUX,   "armAuxTimer",      0,       0 }, 
#endif
    { INT_LVL_UART0,       "primeCellSioDev",  0,       0 },   
    { INT_LVL_UART1,       "primeCellSioDev",  1,       0 },  
    { INT_LVL_UART2,       "primeCellSioDev",  2,       0 },   
    { INT_LVL_UART3,       "primeCellSioDev",  3,       0 },

#ifdef CONFIG_MIO_UART
    { INT_LVL_MIO6,        "primeCellSioDev",  4,       0 },/*unit starts after UART3*/
    { INT_LVL_MIO10,       "primeCellSioDev",  5,       0 },
#endif
    
#ifdef DRV_VXBEND_FTGEM
    { INT_LVL_GEM0_Q0,        "gem",              0,       0 },
    { INT_LVL_GEM0_Q1,        "gem",              0,       1 },
    { INT_LVL_GEM0_Q2,        "gem",              0,       2 },
    { INT_LVL_GEM0_Q3,        "gem",              0,       3 },
    { INT_LVL_GEM0_Q4,        "gem",              0,       4 },
    { INT_LVL_GEM0_Q5,        "gem",              0,       5 },
    { INT_LVL_GEM0_Q6,        "gem",              0,       6 },
    { INT_LVL_GEM0_Q7,        "gem",              0,       7 },
    
    { INT_LVL_GEM1_Q0,        "gem",              1,       0 },
    { INT_LVL_GEM1_Q1,        "gem",              1,       1 },
    { INT_LVL_GEM1_Q2,        "gem",              1,       2 },
    { INT_LVL_GEM1_Q3,        "gem",              1,       3 },
    
    { INT_LVL_GEM2_Q0,        "gem",              2,       0 },
    { INT_LVL_GEM2_Q1,        "gem",              2,       1 },
    { INT_LVL_GEM2_Q2,        "gem",              2,       2 },
    { INT_LVL_GEM2_Q3,        "gem",              2,       3 },
    
    { INT_LVL_GEM3_Q0,        "gem",              3,       0 },
    { INT_LVL_GEM3_Q1,        "gem",              3,       1 },
    { INT_LVL_GEM3_Q2,        "gem",              3,       2 },
    { INT_LVL_GEM3_Q3,        "gem",              3,       3 },
    
#endif
    { INT_LVL_PCIA,        "legacy",     0,  INT_LVL_PCIA }, /* Peu0 legacy interrupt number */
    { INT_LVL_PCIB,        "legacy",     0,  INT_LVL_PCIB },
    { INT_LVL_PCIC,        "legacy",     0,  INT_LVL_PCIC },
    { INT_LVL_PCID,        "legacy",     0,  INT_LVL_PCID },
    { INT_LVL_MIO0,        "ftI2c",      0,       0 },   
    { INT_LVL_MIO1,        "ftI2c",      1,       0 },  
    { INT_LVL_MIO2,        "ftI2c",      2,       0 },
    { INT_LVL_MIO3,        "ftI2c",      3,       0 },
    { INT_LVL_MIO4,        "ftI2c",      4,       0 },
    { INT_LVL_MIO5,        "ftI2c",      5,       0 },
    { INT_LVL_MIO6,        "ftI2c",      6,       0 },
    { INT_LVL_MIO7,        "ftI2c",      7,       0 },
    { INT_LVL_MIO8,        "ftI2c",      8,       0 },
    { INT_LVL_MIO9,        "ftI2c",      9,       0 },
    { INT_LVL_MIO10,        "ftI2c",      10,       0 },
    { INT_LVL_MIO11,        "ftI2c",      11,       0 },
    { INT_LVL_MIO12,        "ftI2c",      12,       0 },
    { INT_LVL_MIO13,        "ftI2c",      13,       0 },
    { INT_LVL_MIO14,        "ftI2c",      14,       0 },
    { INT_LVL_MIO15,        "ftI2c",      15,       0 },
    
    
#ifdef DRV_TIMER_FT_GPT
    { INT_LVL_TM_TACHO(0),       "ftGpTimerDev",  0,       0 },
    { INT_LVL_TM_TACHO(1),       "ftGpTimerDev",  1,       0 },
    { INT_LVL_TM_TACHO(2),       "ftGpTimerDev",  2,       0 },
    { INT_LVL_TM_TACHO(3),       "ftGpTimerDev",  3,       0 },
    { INT_LVL_TM_TACHO(4),       "ftGpTimerDev",  4,       0 },
    { INT_LVL_TM_TACHO(5),       "ftGpTimerDev",  5,       0 },
    { INT_LVL_TM_TACHO(6),       "ftGpTimerDev",  6,       0 },
    { INT_LVL_TM_TACHO(7),       "ftGpTimerDev",  7,       0 },
    { INT_LVL_TM_TACHO(8),       "ftGpTimerDev",  8,       0 },
    { INT_LVL_TM_TACHO(9),       "ftGpTimerDev",  9,       0 },
    { INT_LVL_TM_TACHO(10),       "ftGpTimerDev",  10,       0 },
    { INT_LVL_TM_TACHO(11),       "ftGpTimerDev",  11,       0 },
    { INT_LVL_TM_TACHO(12),       "ftGpTimerDev",  12,       0 },
    { INT_LVL_TM_TACHO(13),       "ftGpTimerDev",  13,       0 },
    { INT_LVL_TM_TACHO(14),       "ftGpTimerDev",  14,       0 },
    { INT_LVL_TM_TACHO(15),       "ftGpTimerDev",  15,       0 },
    { INT_LVL_TM_TACHO(16),       "ftGpTimerDev",  16,       0 },
    { INT_LVL_TM_TACHO(17),       "ftGpTimerDev",  17,       0 },
    { INT_LVL_TM_TACHO(18),       "ftGpTimerDev",  18,       0 },
    { INT_LVL_TM_TACHO(19),       "ftGpTimerDev",  19,       0 },
    { INT_LVL_TM_TACHO(20),       "ftGpTimerDev",  20,       0 },
    { INT_LVL_TM_TACHO(21),       "ftGpTimerDev",  21,       0 },
    { INT_LVL_TM_TACHO(22),       "ftGpTimerDev",  22,       0 },
    { INT_LVL_TM_TACHO(23),       "ftGpTimerDev",  23,       0 },
    { INT_LVL_TM_TACHO(24),       "ftGpTimerDev",  24,       0 },
    { INT_LVL_TM_TACHO(25),       "ftGpTimerDev",  25,       0 },
    { INT_LVL_TM_TACHO(26),       "ftGpTimerDev",  26,       0 },
    { INT_LVL_TM_TACHO(27),       "ftGpTimerDev",  27,       0 },
    { INT_LVL_TM_TACHO(28),       "ftGpTimerDev",  28,       0 },
    { INT_LVL_TM_TACHO(29),       "ftGpTimerDev",  29,       0 },
    { INT_LVL_TM_TACHO(30),       "ftGpTimerDev",  30,       0 },
    { INT_LVL_TM_TACHO(31),       "ftGpTimerDev",  31,       0 },
    { INT_LVL_TM_TACHO(32),       "ftGpTimerDev",  32,       0 },
    { INT_LVL_TM_TACHO(33),       "ftGpTimerDev",  33,       0 },
    { INT_LVL_TM_TACHO(34),       "ftGpTimerDev",  34,       0 },
    { INT_LVL_TM_TACHO(35),       "ftGpTimerDev",  35,       0 },
    { INT_LVL_TM_TACHO(36),       "ftGpTimerDev",  36,       0 },
    { INT_LVL_TM_TACHO(37),       "ftGpTimerDev",  37,       0 },

#endif

#ifdef INCLUDE_DRV_STORAGE_AHCI
    { INT_LVL_SATA0,        "ahciSata"     , 0, 0}, /* AHCI on SOC PLB Bus */
    { INT_LVL_SATA1,        "ahciSata"     , 1, 0}, 
    /* { INT_LVL_PCIA,        "ahciSata"     , 0, 0},*/ /* AHCI on PCI card */
#endif

#ifdef DRV_FTSPI    
    { INT_LVL_SPI0,        "ftSpi",      0,       0 },   
    { INT_LVL_SPI1,        "ftSpi",      1,       0 }, 
    { INT_LVL_SPI2,        "ftSpi",      2,       0 },   
    { INT_LVL_SPI3,        "ftSpi",      3,       0 }, 
#endif

#ifdef DRV_FTWDT    
    { INT_LVL_WDT0,        "ftWdt",      0,       0 },
    { INT_LVL_WDT1,        "ftWdt",      1,       0 },
#endif

#ifdef DRV_FTPWM
    { INT_LVL_PWMINST0,        "ftPwm",     0,  0 },
    { INT_LVL_PWMINST1,        "ftPwm",     1,  0 },
    { INT_LVL_PWMINST2,        "ftPwm",     2,  0 },
    { INT_LVL_PWMINST3,        "ftPwm",     3,  0 },
    { INT_LVL_PWMINST4,        "ftPwm",     4,  0 },
    { INT_LVL_PWMINST5,        "ftPwm",     5,  0 },
    { INT_LVL_PWMINST6,        "ftPwm",     6,  0 },
    { INT_LVL_PWMINST7,        "ftPwm",     7,  0 },

#endif

#ifdef DRV_FTKEYPAD
    { INT_LVL_KEYPAD,       "ftKeypad",     0,  0 },
#endif

#ifdef DRV_FTGPIO
    { INT_LVL_GPIO0(0),     "ftGpio"     , 0, 0},
    { INT_LVL_GPIO0(1),     "ftGpio"     , 0, 1},
    { INT_LVL_GPIO0(2),     "ftGpio"     , 0, 2},
    { INT_LVL_GPIO0(3),     "ftGpio"     , 0, 3},
    { INT_LVL_GPIO0(4),     "ftGpio"     , 0, 4},
    { INT_LVL_GPIO0(5),     "ftGpio"     , 0, 5},
    { INT_LVL_GPIO0(6),     "ftGpio"     , 0, 6},
    { INT_LVL_GPIO0(7),     "ftGpio"     , 0, 7},
    { INT_LVL_GPIO0(8),     "ftGpio"     , 0, 8},
    { INT_LVL_GPIO0(9),     "ftGpio"     , 0, 9},
    { INT_LVL_GPIO0(10),    "ftGpio"     , 0, 10},
    { INT_LVL_GPIO0(11),    "ftGpio"     , 0, 11},
    { INT_LVL_GPIO0(12),    "ftGpio"     , 0, 12},
    { INT_LVL_GPIO0(13),    "ftGpio"     , 0, 13},
    { INT_LVL_GPIO0(14),    "ftGpio"     , 0, 14},
    { INT_LVL_GPIO0(15),    "ftGpio"     , 0, 15},
    { INT_LVL_GPIO1(0),     "ftGpio"     , 1, 0},
    { INT_LVL_GPIO1(1),     "ftGpio"     , 1, 1},
    { INT_LVL_GPIO1(2),     "ftGpio"     , 1, 2},
    { INT_LVL_GPIO1(3),     "ftGpio"     , 1, 3},
    { INT_LVL_GPIO1(4),     "ftGpio"     , 1, 4},
    { INT_LVL_GPIO1(5),     "ftGpio"     , 1, 5},
    { INT_LVL_GPIO1(6),     "ftGpio"     , 1, 6},
    { INT_LVL_GPIO1(7),     "ftGpio"     , 1, 7},
    { INT_LVL_GPIO1(8),     "ftGpio"     , 1, 8},
    { INT_LVL_GPIO1(9),     "ftGpio"     , 1, 9},
    { INT_LVL_GPIO1(10),    "ftGpio"     , 1, 10},
    { INT_LVL_GPIO1(11),    "ftGpio"     , 1, 11},
    { INT_LVL_GPIO1(12),    "ftGpio"     , 1, 12},
    { INT_LVL_GPIO1(13),    "ftGpio"     , 1, 13},
    { INT_LVL_GPIO1(14),    "ftGpio"     , 1, 14},
    { INT_LVL_GPIO1(15),    "ftGpio"     , 1, 15},
    { INT_LVL_GPIO2(0),     "ftGpio"     , 2, 0},
    { INT_LVL_GPIO2(1),     "ftGpio"     , 2, 1},
    { INT_LVL_GPIO2(2),     "ftGpio"     , 2, 2},
    { INT_LVL_GPIO2(3),     "ftGpio"     , 2, 3},
    { INT_LVL_GPIO2(4),     "ftGpio"     , 2, 4},
    { INT_LVL_GPIO2(5),     "ftGpio"     , 2, 5},
    { INT_LVL_GPIO2(6),     "ftGpio"     , 2, 6},
    { INT_LVL_GPIO2(7),     "ftGpio"     , 2, 7},
    { INT_LVL_GPIO2(8),     "ftGpio"     , 2, 8},
    { INT_LVL_GPIO2(9),     "ftGpio"     , 2, 9},
    { INT_LVL_GPIO2(10),    "ftGpio"     , 2, 10},
    { INT_LVL_GPIO2(11),    "ftGpio"     , 2, 11},
    { INT_LVL_GPIO2(12),    "ftGpio"     , 2, 12},
    { INT_LVL_GPIO2(13),    "ftGpio"     , 2, 13},
    { INT_LVL_GPIO2(14),    "ftGpio"     , 2, 14},
    { INT_LVL_GPIO2(15),    "ftGpio"     , 2, 15},
    { INT_LVL_GPIO3,        "ftGpio"     , 3, 0}, 
    { INT_LVL_GPIO4,        "ftGpio"     , 4, 0},
    { INT_LVL_GPIO5,        "ftGpio"     , 5, 0},
#endif 

#ifdef INCLUDE_USB_XHCI_HCD
    { INT_LVL_USB3_0,"vxbPlbUsbXhci",    0,      0 },
    { INT_LVL_USB3_1,"vxbPlbUsbXhci",    1,      0 },
#endif /* INCLUDE_USB_XHCI_HCD */

#ifdef DRV_FTGDMA
    { INT_LVL_GDMA0,        "ftGDma"       , 0, 0},
#endif

#ifdef DRV_FTSD    
    { INT_LVL_SD0,         "ftSdhci",        0,       0 },
    { INT_LVL_SD1,         "ftSdhci",        1,       0 },
#endif

#ifdef DRV_FTCAN
    { INT_LVL_CAN0,        "ftCan",      0,       0 },   
    { INT_LVL_CAN1,        "ftCan",      1,       0 },  
#endif
#ifdef DRV_FTE2000_DC																	    
    { INT_LVL_DC,        "ftE2000DC",      0,       0 },
#endif
};

LOCAL const struct intrCtlrPriority gicPriority[] = {
    /* pin,                priority */
    { INT_LVL_TIMER_GEN,   0x0  }, 
   
#ifdef  DRV_ARM_GEN_AUX_TIMER    
    { INT_LVL_TIMER_AUX,   0x0  }, 
#endif
    { INT_LVL_UART0,       0x10 },   
    { INT_LVL_UART1,       0x10 },  
    { INT_LVL_UART2,       0x10 },   
    { INT_LVL_UART3,       0x10 },
#ifdef DRV_VXBEND_FTGEM
    { INT_LVL_GEM0_Q0,     0x10 },
    { INT_LVL_GEM1_Q0,     0x10 },
    { INT_LVL_GEM2_Q0,     0x10 },
    { INT_LVL_GEM3_Q0,     0x10 },
#endif
    { INT_LVL_PCIA,        0x10 }, /* Peu0 legacy interrupt level */
    { INT_LVL_PCIB,        0x10 },
    { INT_LVL_PCIC,        0x10 },
    { INT_LVL_PCID,        0x10 },
    { INT_LVL_MIO0,        0x20 },   
    { INT_LVL_MIO1,        0x20 },  
    { INT_LVL_MIO2,        0x20 },
};

#ifdef _WRS_CONFIG_SMP
struct intrCtlrCpu gicCpu[] = {
    /* pin,                CPU */
    { INT_LVL_UART0,       0 },   
    { INT_LVL_UART1,       0 },  
    { INT_LVL_UART2,       0 },   
    { INT_LVL_UART3,       0 },
#ifdef DRV_VXBEND_FTGEM
    { INT_LVL_GEM0_Q0,     0 },
    { INT_LVL_GEM1_Q0,     0 },
    { INT_LVL_GEM2_Q0,     0 },
    { INT_LVL_GEM3_Q0,     0 },
#endif
    { INT_LVL_PCIA,        0 }, /* Peu0 legacy interrupt number */
    { INT_LVL_PCIB,        0 },
    { INT_LVL_PCIC,        0 },
    { INT_LVL_PCID,        0 },
    { INT_LVL_MIO0,        0 },   
    { INT_LVL_MIO1,        1 },  
    { INT_LVL_MIO2,        0 },
#ifdef DRV_TIMER_FT_GPT
    { INT_LVL_TM_TACHO(0),        0 },
#endif

};
#endif /* _WRS_CONFIG_SMP */

LOCAL const struct intrCtlrTrigger gicTrigger[] = {
    /* pin,                     sensitivity */
#ifdef DRV_FTGPIO
    { INT_LVL_GPIO0(0),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(1),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(2),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(3),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(4),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(5),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(6),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(7),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(8),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(9),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(10),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(11),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(12),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(13),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(14),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO0(15),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(0),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(1),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(2),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(3),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(4),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(5),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(6),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(7),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(8),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(9),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(10),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(11),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(12),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(13),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(14),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO1(15),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(0),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(1),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(2),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(3),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(4),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(5),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(6),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(7),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(8),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(9),     VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(10),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(11),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(12),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(13),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(14),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO2(15),    VXB_INTR_TRIG_FALLING_EDGE},
    { INT_LVL_GPIO3,        VXB_INTR_TRIG_FALLING_EDGE}, 
    { INT_LVL_GPIO4,        VXB_INTR_TRIG_ACTIVE_HIGH /*VXB_INTR_TRIG_FALLING_EDGE*/},
    { INT_LVL_GPIO5,        VXB_INTR_TRIG_FALLING_EDGE},
#endif
};
/*
 * interrupt mode - interrupts can be in either preemptive or non-preemptive
 * mode. For preemptive mode, change INT_MODE to INT_PREEMPT_MODEL
 */

#define INT_MODE                 INT_NON_PREEMPT_MODEL
LOCAL const struct hcfResource armGICv3Resources[] = {
    { "regBase",           HCF_RES_INT, { (void *)GIC_BASE_ADR } },
    { "distOffset",        HCF_RES_INT,  {(void *)0 } },
    { "redistOffset",      HCF_RES_INT,  {(void *)0x80000 } },
    { "input",             HCF_RES_ADDR, {(void *)&gicInputs[0] } },
#ifdef _WRS_CONFIG_SMP
    { "cpuRoute",          HCF_RES_ADDR, {(void *)&gicCpu[0] } },
    { "cpuRouteTableSize", HCF_RES_INT,  {(void *)NELEMENTS(gicCpu) } },
#endif
    { "inputTableSize",    HCF_RES_INT,  {(void *)NELEMENTS(gicInputs) } },
    { "priority",          HCF_RES_ADDR, {(void *)&gicPriority[0]} },
    { "priorityTableSize", HCF_RES_INT,  {(void *)NELEMENTS(gicPriority) } },
    { "trigger",           HCF_RES_ADDR, {(void *)&gicTrigger[0]} },
    { "triggerTableSize",  HCF_RES_INT,  {(void *)NELEMENTS(gicTrigger) } },
    { "intMode",           HCF_RES_INT,  {(void *)INT_MODE } },
};
#define armGICv3Num NELEMENTS(armGICv3Resources)
#endif  /* DRV_ARM_GICV3 */


struct hcfResource primeCellSioDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART0_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)FT2000_UART_CLK} },
    { "initialBaud", HCF_RES_INT, {(void *)CONSOLE_BAUD_RATE} },
};
#define primeCellSioDev0Num NELEMENTS(primeCellSioDev0Resources)

struct hcfResource primeCellSioDev1Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART1_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)FT2000_UART_CLK} }, /*In fact 100MHz*/
    { "initialBaud", HCF_RES_INT, {(void *)CONSOLE_BAUD_RATE} },
};
#define primeCellSioDev1Num NELEMENTS(primeCellSioDev1Resources)

struct hcfResource primeCellSioDev2Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART2_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)FT2000_UART_CLK} },
    { "initialBaud", HCF_RES_INT, {(void *)CONSOLE_BAUD_RATE} },
};
#define primeCellSioDev2Num NELEMENTS(primeCellSioDev2Resources)

struct hcfResource primeCellSioDev3Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART3_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)FT2000_UART_CLK} },
    { "initialBaud", HCF_RES_INT, {(void *)CONSOLE_BAUD_RATE} },
};
#define primeCellSioDev3Num NELEMENTS(primeCellSioDev3Resources)

#ifdef CONFIG_MIO_UART
/*MIO UART Clock: 50MHz*/
struct hcfResource mioSioDev6Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)MIO6_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)(CLK_50MHZ)} },
    { "initialBaud", HCF_RES_INT, {(void *)CONSOLE_BAUD_RATE} },
};
#define mioSioDev6Num NELEMENTS(mioSioDev6Resources)

struct hcfResource mioSioDev10Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)MIO10_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)(CLK_50MHZ)} },
    { "initialBaud", HCF_RES_INT, {(void *)CONSOLE_BAUD_RATE} },
};
#define mioSioDev10Num NELEMENTS(mioSioDev10Resources)
#endif

#ifdef DRV_FTQSPI
struct vxbSpiDevInfo qspiDevTbl[] = {
    /* name                     cs      width   freq        mode */
#ifdef  DRV_SPIFLASH_SP25   
    { QSPI_FLASH_DEVICE_NAME,     0,       8,   30000000,      1},
#endif     
};

struct hcfResource qspiResources[] =  {
    { "regBase",       HCF_RES_INT,   { (void *)(0x28008000) } },
    { "capacity",      HCF_RES_INT,   { (void *)(QSPI_FLASH_CAP_4MB) } },    
    { "clkDiv",        HCF_RES_INT,   { (void *)(QSPI_SCK_DIV_32) } },
    { "transMode",     HCF_RES_INT,   { (void *)(QSPI_MODE_NORMAL) } },
    { "spiDev",        HCF_RES_ADDR,  { (void *)&qspiDevTbl[0]} },
    { "spiDevNum",     HCF_RES_INT,   { (void *)NELEMENTS(qspiDevTbl)}},

};
#define qspiNum  NELEMENTS(qspiResources)
#endif /* DRV_FTQSPI */

#ifdef DRV_FTSPI
struct vxbSpiDevInfo spi0DevTbl[] = {
    /* name                     cs      width   freq        mode */
 
};
struct hcfResource spi0Resources[] =  {
    { "regBase",       HCF_RES_INT,   { (void *)(0x2803a000) } },
    { "clkFreq",       HCF_RES_INT,   { (void *)(50000000) } },
    { "rwMode",        HCF_RES_INT,   { (void *)(SPI_RW_MODE_INT) } },  
    { "spiDev",        HCF_RES_ADDR,  { (void *)&spi0DevTbl[0]} },
    { "spiDevNum",     HCF_RES_INT,   { (void *)NELEMENTS(spi0DevTbl)}},
};
#define spi0Num  NELEMENTS(spi0Resources)

struct vxbSpiDevInfo spi1DevTbl[] = {
    /* name                     cs      width   freq        mode */
  
};
struct hcfResource spi1Resources[] =  {
    { "regBase",       HCF_RES_INT,   { (void *)(0x2803b000) } },
    { "clkFreq",       HCF_RES_INT,   { (void *)(50000000) } },
    { "rwMode",        HCF_RES_INT,   { (void *)(SPI_RW_MODE_INT) } },  
    { "spiDev",        HCF_RES_ADDR,  { (void *)&spi1DevTbl[0]} },
    { "spiDevNum",     HCF_RES_INT,   { (void *)NELEMENTS(spi1DevTbl)}},
};
#   define spi1Num  NELEMENTS(spi1Resources)

struct vxbSpiDevInfo spi2DevTbl[] = {
    /* name                     cs      width   freq        mode */
#ifdef  DRV_SPIFLASH_SP25   
    { SPI_FLASH_DEVICE_NAME,     0,       8,   12000000,      SPI_MODE_0},
#endif  
};
struct hcfResource spi2Resources[] =  {
    { "regBase",       HCF_RES_INT,   { (void *)(0x2803c000) } },
    { "clkFreq",       HCF_RES_INT,   { (void *)(50000000) } },
    { "rwMode",        HCF_RES_INT,   { (void *)(SPI_RW_MODE_INT) } },   
    { "spiDev",        HCF_RES_ADDR,  { (void *)&spi2DevTbl[0]} },
    { "spiDevNum",     HCF_RES_INT,   { (void *)NELEMENTS(spi2DevTbl)}},
};
#define spi2Num  NELEMENTS(spi2Resources)

struct vxbSpiDevInfo spi3DevTbl[] = {
    /* name                     cs      width   freq        mode*/
 
};
struct hcfResource spi3Resources[] =  {
    { "regBase",       HCF_RES_INT,   { (void *)(0x2803d000) } },
    { "clkFreq",       HCF_RES_INT,   { (void *)(50000000) } },
    { "rwMode",        HCF_RES_INT,   { (void *)(SPI_RW_MODE_INT) } },  
    { "spiDev",        HCF_RES_ADDR,  { (void *)&spi3DevTbl[0]} },
    { "spiDevNum",     HCF_RES_INT,   { (void *)NELEMENTS(spi3DevTbl)}},
};
#define spi3Num  NELEMENTS(spi3Resources)
#endif /* DRV_FTSPI */

#ifdef INCLUDE_PC_CONSOLE
LOCAL const struct hcfResource pentiumM6845VgaResources[] =
    {
        { "colorMode",   HCF_RES_INT, {(void *) 1} },
        { "colorSetting",HCF_RES_INT, {(void *) 0x1f} },
    };
#define pentiumM6845VgaNum NELEMENTS(pentiumM6845VgaResources)
#endif
#ifdef DRV_FTI2C
LOCAL struct i2cDevInputs i2cDev9Input[] = {
       /* Name */   /* Addr (7-bit) */   /* flag */

#ifdef DRV_I2C_RTC  
    { "rtc_ds1339",    (0xD0>>1),    0 },
#endif

};
struct hcfResource i2cDev9Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)MIO9_BASE_ADR} },
    { "irq", HCF_RES_INT, {(void *)INT_LVL_MIO9} },
    { "clkFreq", HCF_RES_INT,{(void *)  CLK_50MHZ} },
    { "busSpeed", HCF_RES_INT,{(void *)CLK_400KHZ} },
    /*{ "polling", HCF_RES_INT,{(void *)TRUE} },*/ /* polling mode IS also OK */
    { "polling", HCF_RES_INT,{(void *)FALSE} },    /* interrupt mode IS also OK */
    { "i2cDev",  HCF_RES_ADDR, {(void *)&i2cDev9Input[0] } },
    { "i2cDevNum", HCF_RES_INT,{(void *)NELEMENTS(i2cDev9Input)} },
};
#define i2cDev9Num NELEMENTS(i2cDev9Resources)

#endif

#ifdef  DRV_FTGPIO
/*This table is used to set the default mode of gpio pins*/
LOCAL UINT32 gpio0ModeTable[] = {
/*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/       
         1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                            2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
};
struct hcfResource gpioDev0Resources[] = {
    { "regBase",      HCF_RES_INT,     { (void *)GPIO0_BASE_ADR } },
    { "modeTable",    HCF_RES_ADDR,    { (void *)&gpio0ModeTable[0] } },
    { "modeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio0ModeTable)} },
    { "trigTable",    HCF_RES_ADDR,    { (void *)&gicTrigger[0]} },
    { "trigTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gicTrigger) } },    
};
#define gpioDev0Num NELEMENTS(gpioDev0Resources)

LOCAL UINT32 gpio1ModeTable[] = {
/*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/           
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                            2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
};
struct hcfResource gpioDev1Resources[] = {
    { "regBase",      HCF_RES_INT,     { (void *)GPIO1_BASE_ADR } },
    { "modeTable",    HCF_RES_ADDR,    { (void *)&gpio1ModeTable[0] } },
    { "modeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio1ModeTable)} },
    { "trigTable",    HCF_RES_ADDR,    { (void *)&gicTrigger[0]} },
    { "trigTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gicTrigger) } },
};
#define gpioDev1Num NELEMENTS(gpioDev1Resources)

LOCAL UINT32 gpio2ModeTable[] = {
/*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/           
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                            2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
};
struct hcfResource gpioDev2Resources[] = {
    { "regBase",      HCF_RES_INT,     { (void *)GPIO2_BASE_ADR } }, 
    { "modeTable",    HCF_RES_ADDR,    { (void *)&gpio2ModeTable[0] } },
    { "modeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio2ModeTable)} },
    { "trigTable",    HCF_RES_ADDR,    { (void *)&gicTrigger[0]} },
    { "trigTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gicTrigger) } },
};
#define gpioDev2Num NELEMENTS(gpioDev2Resources)

LOCAL UINT32 gpio3ModeTable[] = {
/*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/           
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                            2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
};
struct hcfResource gpioDev3Resources[] = {
    { "regBase",      HCF_RES_INT,     { (void *)GPIO3_BASE_ADR } },
    { "modeTable",    HCF_RES_ADDR,    { (void *)&gpio3ModeTable[0] } },
    { "modeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio3ModeTable)} },
    { "trigTable",    HCF_RES_ADDR,    { (void *)&gicTrigger[0]} },
    { "trigTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gicTrigger) } },
};
#define gpioDev3Num NELEMENTS(gpioDev3Resources)

LOCAL UINT32 gpio4ModeTable[] = {
/*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/           
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                            2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
};
struct hcfResource gpioDev4Resources[] = {
    { "regBase",      HCF_RES_INT,    { (void *)GPIO4_BASE_ADR } },
    { "modeTable",    HCF_RES_ADDR,    { (void *)&gpio4ModeTable[0] } },
    { "modeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio4ModeTable)} },
    { "trigTable",    HCF_RES_ADDR,    { (void *)&gicTrigger[0]} },
    { "trigTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gicTrigger) } },
};
#define gpioDev4Num NELEMENTS(gpioDev4Resources)

LOCAL UINT32 gpio5ModeTable[] = {
/*pinNo: 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15*/           
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                                            2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/ 
};
struct hcfResource gpioDev5Resources[] = {
    { "regBase",      HCF_RES_INT,    { (void *)GPIO5_BASE_ADR } },
    { "modeTable",    HCF_RES_ADDR,    { (void *)&gpio5ModeTable[0] } },
    { "modeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio5ModeTable)} },
    { "trigTable",    HCF_RES_ADDR,    { (void *)&gicTrigger[0]} },
    { "trigTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gicTrigger) } },
};
#define gpioDev5Num NELEMENTS(gpioDev5Resources)
#endif

#ifdef  DRV_ARM_GEN_SYS_TIMER
struct hcfResource armGenSysTimerResources[] = {
    { "regBase", HCF_RES_INT, {(void *)0xeeee0000} },
    { "irq",     HCF_RES_INT, {(void *)INT_LVL_TIMER_GEN} },
    { "clkFreq", HCF_RES_INT, {(void *)SYS_CLK_FREQ} },
    { "minClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MIN} },
    { "maxClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MAX} },
};
#define armGenSysTimerNum NELEMENTS(armGenSysTimerResources)
#endif  /* DRV_ARM_GEN_SYS_TIMER */

#ifdef  DRV_ARM_GEN_AUX_TIMER
struct hcfResource armAuxGenTimerResources[] = {
    { "regBase", HCF_RES_INT, {(void *)AUX_CLK_REG_BASE} },
    { "irq",     HCF_RES_INT, {(void *)INT_LVL_TIMER_AUX} },
    { "clkFreq", HCF_RES_INT, {(void *)AUX_CLK_FREQ} },
    { "minClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MIN} },
    { "maxClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MAX} },
};
#define armAuxGenTimerNum NELEMENTS(armAuxGenTimerResources)
#endif  /* DRV_ARM_GEN_SYS_TIMER */

#ifdef DRV_PCIBUS_FT
IMPORT STATUS sysPciExAutoconfigInclude(PCI_SYSTEM *, PCI_LOC *, UINT);
IMPORT UCHAR sysPciExAutoconfigIntrAssign(PCI_SYSTEM *, PCI_LOC *, UCHAR);
IMPORT VOID sysPciPirqEnable (BOOL enable);
const struct hcfResource ftPci0Resources[] = {
    { "regBase",            HCF_RES_INT,  { (void *)(FT_PCI_CONFIG_ADDR) } },
    { "memIo32Addr",        HCF_RES_ADDR, { (void *)0x58000000 } },
    { "memIo32Size",        HCF_RES_INT,  { (void *)0x27f00000 } },
    { "io32Addr",           HCF_RES_ADDR, { (void *)0x50000000 } },
    { "io32Size",           HCF_RES_INT,  { (void *)0x08000000 } },
    { "io16Addr",           HCF_RES_ADDR, { (void *)0x1000 } },
    { "io16Size",           HCF_RES_INT,  { (void *)0x0ffff000 } },
    { "ioStart",            HCF_RES_ADDR,  { (void *)0x50000000 } },
    { "fbbEnable",          HCF_RES_INT,  { (void *)TRUE } },
    { "cacheSize",          HCF_RES_INT,  { (void *)(8) } },
    { "maxLatAllSet",       HCF_RES_INT,  { (void *)0x40 } },
    { "autoIntRouteSet",    HCF_RES_INT,  { (void *)FALSE } },
    { "includeFuncSet",     HCF_RES_ADDR, { (void *)sysPciExAutoconfigInclude } },
    { "intAssignFuncSet",   HCF_RES_ADDR, { (void *)sysPciExAutoconfigIntrAssign } },
    { "funcPirqEnable",     HCF_RES_ADDR, { (void *)sysPciPirqEnable}},
    { "maxBusSet",          HCF_RES_INT,  { (void *)32 } },
    { "autoConfig",         HCF_RES_INT,  { (void *)(FALSE)} },

#ifdef INCLUDE_INTCTLR_DYNAMIC_LIB
    { "msiEnable",          HCF_RES_INT,  { (void *)(TRUE)} },
    { "dynamicInterrupts",  HCF_RES_INT,  { (void *)(TRUE)} }
#else
    { "msiEnable",          HCF_RES_INT,  { (void *)(FALSE)} },
    { "dynamicInterrupts",  HCF_RES_INT,  { (void *)(FALSE)} }
#endif /* INCLUDE_INTCTLR_DYNAMIC_LIB */
};
#define ftPci0Num NELEMENTS(ftPci0Resources)
#endif /* DRV_PCIBUS_FT */

#ifdef DRV_VXBEND_FTGEM 
LOCAL void sysFtGemClkSet(UINT32 unit, UINT32 clkVal, BOOL exFlag);
LOCAL BOOL ifMemCached(UINT32 virtAddr);

struct hcfResource gem0Resources[] = {
    { "regBase",    HCF_RES_INT, {(void *)GMU0_BASE_ADR} },
    { "phyAddr",    HCF_RES_INT,    {(void *)PHYADDR_OF_AUTO} },
    { "ifCached",  HCF_RES_ADDR,   { (void *)ifMemCached} },
    { "phyMode",    HCF_RES_INT,    {(void *)PHY_INTERFACE_MODE_SGMII} },
    { "macAddrSet", HCF_RES_ADDR,   {(void *)sysGmacAddrSet} },
    { "miiIfName",  HCF_RES_STRING, {(void *)"gem"} },
    { "miiIfUnit",  HCF_RES_INT,    {(void *)0} },
};
#define gem0Num NELEMENTS(gem0Resources)

struct hcfResource gem1Resources[] = {
    { "regBase",    HCF_RES_INT, {(void *)GMU1_BASE_ADR} },
    { "phyAddr",    HCF_RES_INT,    {(void *)PHYADDR_OF_AUTO} },
    { "ifCached",  HCF_RES_ADDR,   { (void *)ifMemCached} },
    { "phyMode",    HCF_RES_INT,    {(void *)PHY_INTERFACE_MODE_SGMII} },
    { "macAddrSet", HCF_RES_ADDR,   {(void *)sysGmacAddrSet} },
    { "miiIfName",  HCF_RES_STRING, {(void *)"gem"} },
    { "miiIfUnit",  HCF_RES_INT,    {(void *)1} },
};
#define gem1Num NELEMENTS(gem1Resources)

struct hcfResource gem2Resources[] = {
    { "regBase",    HCF_RES_INT, {(void *)GMU2_BASE_ADR} },
    { "phyAddr",    HCF_RES_INT,    {(void *)PHYADDR_OF_AUTO} },
    { "ifCached",  HCF_RES_ADDR,   { (void *)ifMemCached} },
    { "phyMode",    HCF_RES_INT,    {(void *)PHY_INTERFACE_MODE_SGMII} },
    { "macAddrSet", HCF_RES_ADDR,   {(void *)sysGmacAddrSet} },
    { "miiIfName",  HCF_RES_STRING, {(void *)"gem"} },
    { "miiIfUnit",  HCF_RES_INT,    {(void *)2} },
};
#define gem2Num NELEMENTS(gem2Resources)

struct hcfResource gem3Resources[] = {
    { "regBase",    HCF_RES_INT, {(void *)GMU3_BASE_ADR} },
    { "phyAddr",    HCF_RES_INT,    {(void *)PHYADDR_OF_AUTO} },
    { "ifCached",  HCF_RES_ADDR,   { (void *)ifMemCached} },
    { "phyMode",    HCF_RES_INT,    {(void *)PHY_INTERFACE_MODE_SGMII} },
    { "macAddrSet", HCF_RES_ADDR,   {(void *)sysGmacAddrSet} },
    { "miiIfName",  HCF_RES_STRING, {(void *)"gem"} },
    { "miiIfUnit",  HCF_RES_INT,    {(void *)3} },
};
#define gem3Num NELEMENTS(gem3Resources)

#endif /* DRV_VXBEND_FTGEM */ 
#ifdef DRV_FTE2000_DC
struct hcfResource ftE2000DCResources[]= {
    { "regBase",    HCF_RES_ADDR, {(void *)FT_E2000DC_BASE_ADR} },
    { "pipeMask",    HCF_RES_INT,   { (void *)0x3 } },
    { "edpMask", HCF_RES_INT, {(void *)0} },
    { "dpNum", HCF_RES_INT, {(void *)0x1} },
    { "debug", HCF_RES_INT, {(void *)0x1} },
};
#define ftE2000DCNum NELEMENTS(ftE2000DCResources)

#endif   /* DRV_FTE2000_DC */ 

#ifdef DRV_TIMER_FT_GPT
UINT32 sysGptFreqGet (void);

LOCAL const struct hcfResource ftGptTimer0Resources[] = 
    {
    { "regBase",    HCF_RES_INT,  {(void *)TM_TACHO_BASE_ADR(0)}},
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysGptFreqGet}},
    { "timerBits",   HCF_RES_INT,  {(void *)TIMER_32_BITS}}, /* enum TimerBitsType */

    /* enum TimerTachoModeType: timer/tacho/capture. *ONLY* choose one of them  */
    
    { "workMode",   HCF_RES_INT,  {(void *)TIMER_WORK_MODE_TACHO}},
    
    /* timer mode: (not use. any value) */
    
    { "clkRateMin", HCF_RES_INT,  {(void *)SYS_CLK_RATE_MIN}},
    { "clkRateMax", HCF_RES_INT,  {(void *)SYS_CLK_RATE_MAX}},
    
    /* tacho mode: */
    
    { "edgeMode",   HCF_RES_INT,  {(void *)TACHO_RISING_EDGE}}, /* enum TachoEdgeType */
    { "debounce",   HCF_RES_INT,  {(void *)0}}, 
    { "pulsNum",    HCF_RES_INT,  {(void *)0xf4240}}, /* puls_num of period to calculate rpm */

    /* capture mode: (not use. any value) */
    
    { "captureCnt",  HCF_RES_INT,  {(void *)10}},
    };
#define ftGptTimer0Num NELEMENTS(ftGptTimer0Resources)

/* Example1:  Timer1 as repeated cycle timer  */

LOCAL const struct hcfResource ftGptTimer1Resources[] = 
    {
    { "regBase",    HCF_RES_INT,  {(void *)TM_TACHO_BASE_ADR(1)}},
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysGptFreqGet}},
    { "timerBits",   HCF_RES_INT,  {(void *)TIMER_32_BITS}}, /* enum TimerBitsType */

    /* enum TimerTachoModeType: timer/tacho/capture. *ONLY* choose one of them  */
    
    { "workMode",   HCF_RES_INT,  {(void *)TIMER_WORK_MODE_TIMER}},
    
    /* timer mode: */

    { "cmpType", HCF_RES_INT,  {(void *)TIMER_CYC_CMP}}, /* repeatedly compare */
    { "clkRateMin", HCF_RES_INT,  {(void *)SYS_CLK_RATE_MIN}},
    { "clkRateMax", HCF_RES_INT,  {(void *)SYS_CLK_RATE_MAX}},
    };
#define ftGptTimer1Num NELEMENTS(ftGptTimer1Resources)

/* Example2:  Timer2 as one-time timer  */

LOCAL const struct hcfResource ftGptTimer2Resources[] = 
    {
    { "regBase",    HCF_RES_INT,  {(void *)TM_TACHO_BASE_ADR(2)}},
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysGptFreqGet}},
    { "timerBits",   HCF_RES_INT,  {(void *)TIMER_32_BITS}}, /* enum TimerBitsType */

    /* enum TimerTachoModeType: timer/tacho/capture. *ONLY* choose one of them  */
    
    { "workMode",   HCF_RES_INT,  {(void *)TIMER_WORK_MODE_TIMER}},
    
    /* timer mode: */

    { "cmpType", HCF_RES_INT,  {(void *)TIMER_ONCE_CMP}}, /* one time compare */

    };
#define ftGptTimer2Num NELEMENTS(ftGptTimer2Resources)


/* Example2:  Timer18 as Tachometer device  */

LOCAL const struct hcfResource ftGptTimer18Resources[] = 
    {
    { "regBase",    HCF_RES_INT,  {(void *)TM_TACHO_BASE_ADR(18)}},
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysGptFreqGet}},
    { "timerBits",   HCF_RES_INT,  {(void *)TIMER_32_BITS}}, /* enum TimerBitsType */

    /* enum TimerTachoModeType: timer/tacho/capture. *ONLY* choose one of them  */
    
    { "workMode",   HCF_RES_INT,  {(void *)TIMER_WORK_MODE_TACHO}},
    
    /* tacho mode: */
    
    { "edgeMode",   HCF_RES_INT,  {(void *)TACHO_RISING_EDGE}}, /* enum TachoEdgeType */
    { "debounce",   HCF_RES_INT,  {(void *)0}}, 
    { "pulsNum",    HCF_RES_INT,  {(void *)0xf4240}}, /* puls_num of period to calculate rpm */

    };
#define ftGptTimer18Num NELEMENTS(ftGptTimer18Resources)

/* Example3:  Timer37 as capture device  */

LOCAL const struct hcfResource ftGptTimer37Resources[] = 
    {
    { "regBase",    HCF_RES_INT,  {(void *)TM_TACHO_BASE_ADR(37)}},
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysGptFreqGet}},
    { "timerBits",   HCF_RES_INT,  {(void *)TIMER_32_BITS}}, /* enum TimerBitsType */

    /* enum TimerTachoModeType: timer/tacho/capture. *ONLY* choose one of them  */
    
    { "workMode",   HCF_RES_INT,  {(void *)TIMER_WORK_MODE_CAPTURE}},
    
    /* capture mode: */
    
    { "captureCnt",  HCF_RES_INT,  {(void *)0x7f}},
    };
#define ftGptTimer37Num NELEMENTS(ftGptTimer37Resources)
#endif

#ifdef INCLUDE_DRV_STORAGE_AHCI
struct hcfResource ftSata0Resources[] =  {
    { "regBase",             HCF_RES_INT,   {(void *)FT_SATA0_BASE_ADR} },
};
#define ftSata0Num NELEMENTS(ftSata0Resources)

struct hcfResource ftSata1Resources[] =  {
    { "regBase",             HCF_RES_INT,   {(void *)FT_SATA1_BASE_ADR} },
};
#define ftSata1Num NELEMENTS(ftSata1Resources)
#endif /* INCLUDE_DRV_STORAGE_AHCI */

#ifdef DRV_FTWDT
struct hcfResource ftWdt0Resources[]= {
    { "regBase",    HCF_RES_INT, {(void *)WDT0_BASE_ADR} },
    { "clkFreq",    HCF_RES_INT, { (void *)SYS_CLK_FREQ } },
    { "minClkRate", HCF_RES_INT, {(void *)1 } },
    { "maxClkRate", HCF_RES_INT, {(void *)1000} },
};
#define ftWdt0Num NELEMENTS(ftWdt0Resources)

struct hcfResource ftWdt1Resources[]= {
    { "regBase",    HCF_RES_INT, {(void *)WDT1_BASE_ADR} },
    { "clkFreq",    HCF_RES_INT, { (void *)SYS_CLK_FREQ } },
    { "minClkRate", HCF_RES_INT, {(void *)1 } },
    { "maxClkRate", HCF_RES_INT, {(void *)1000} },
};
#define ftWdt1Num NELEMENTS(ftWdt1Resources)
#endif

#ifdef DRV_FTKEYPAD
const struct hcfResource ftKeypad0Resources[] = {
    { "regBase",    HCF_RES_INT,            { (void *)0x2807A000 } },
    { "rows",       HCF_RES_INT, {(void *)4 } },
    { "cols",       HCF_RES_INT, {(void *)4 } },
};
#define ftKeypad0Num NELEMENTS(ftKeypad0Resources)
#endif


#ifdef DRV_FTPWM
struct hcfResource ftPwm0Resources[]= {
    { "regBase",    HCF_RES_INT, {(void *)(PWM_BASE_ADR(0))} },
    { "clkFreq",    HCF_RES_INT,   { (void *)CLK_50MHZ } },
};
#define ftPwm0Num NELEMENTS(ftPwm0Resources)

struct hcfResource ftPwm1Resources[]= {
    { "regBase",    HCF_RES_INT, {(void *)(PWM_BASE_ADR(1))} },
    { "clkFreq",    HCF_RES_INT,   { (void *)CLK_50MHZ } },
};
#define ftPwm1Num NELEMENTS(ftPwm1Resources)

struct hcfResource ftPwm6Resources[]= {
    { "regBase",    HCF_RES_INT, {(void *)(PWM_BASE_ADR(6))} },
    { "clkFreq",    HCF_RES_INT,   { (void *)CLK_50MHZ } },
};
#define ftPwm6Num NELEMENTS(ftPwm6Resources)


/* PWM0~7 Exist. ignore [2-6] here  */

struct hcfResource ftPwm7Resources[]= {
    { "regBase",    HCF_RES_INT, {(void *)(PWM_BASE_ADR(7))} },
    { "clkFreq",    HCF_RES_INT,   { (void *)CLK_50MHZ } },
};
#define ftPwm7Num NELEMENTS(ftPwm7Resources)
#endif


#ifdef INCLUDE_USB_XHCI_HCD
const struct hcfResource vxbPlbUsbXhci0DevResources[]= {

     /* USB3_0 Base + 0x8000 for E2000 XHCI registers */
     
    { "regBase",        HCF_RES_INT,    {(void *)0x31A08000} },
};
#define vxbPlbUsbXhci0DevNum NELEMENTS(vxbPlbUsbXhci0DevResources)

const struct hcfResource vxbPlbUsbXhci1DevResources[]= {

     /* USB3_1 Base + 0x8000 for E2000 XHCI registers */

    { "regBase",        HCF_RES_INT,    {(void *)0x31A28000} },
};
#define vxbPlbUsbXhci1DevNum NELEMENTS(vxbPlbUsbXhci1DevResources)
#endif /* INCLUDE_USB_XHCI_HCD */

#ifdef DRV_FTGDMA
const struct hcfResource ftGDma0Resources[] = {
    { "regBase",    HCF_RES_INT,            { (void *)0x32b34000 } },
    { "polling",    HCF_RES_INT,            { (void *)FALSE} },
};
#define ftGDma0Num NELEMENTS(ftGDma0Resources)
#endif

#ifdef DRV_FTSD
LOCAL struct hcfResource ftSdhc0Resources[] = {
    { "regBase",    HCF_RES_INT,  {(void *)FT_SDHC0_BASE} },
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysSdhcClkFreqGet} },    
    { "type",       HCF_RES_INT,  {(void *)SDCI_TYPE_EMMC} }
};
#define ftSdhc0Num NELEMENTS(ftSdhc0Resources)

LOCAL struct hcfResource ftSdhc1Resources[] = {
    { "regBase",    HCF_RES_INT,  {(void *)FT_SDHC1_BASE} },
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysSdhcClkFreqGet} },    
    { "type",       HCF_RES_INT,  {(void *)SDCI_TYPE_SD} }
};
#define ftSdhc1Num NELEMENTS(ftSdhc1Resources)
#endif /* DRV_FTSD */

#ifdef DRV_FTCAN
struct hcfResource ftCanDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)FTCAN0_BASE} },
    { "irq", HCF_RES_INT, {(void *)INT_LVL_CAN0} },
    { "isCanFD", HCF_RES_INT, {(void *)TRUE} },    
    { "bitrate", HCF_RES_INT, {(void *)FTCAN0_BITRATE} },
    { "dataBitrate", HCF_RES_INT, {(void *)FTCAN0_DATA_BITRATE} },
};
#define ftCanDev0Num NELEMENTS(ftCanDev0Resources)

struct hcfResource ftCanDev1Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)FTCAN1_BASE} },
    { "irq", HCF_RES_INT, {(void *)INT_LVL_CAN1} },
    { "isCanFD", HCF_RES_INT, {(void *)TRUE} },  
    { "bitrate", HCF_RES_INT, {(void *)FTCAN1_BITRATE} },
    { "dataBitrate", HCF_RES_INT, {(void *)FTCAN1_DATA_BITRATE} },    
};
#define ftCanDev1Num NELEMENTS(ftCanDev1Resources)
#endif

#ifdef DRV_FTSCMI
struct hcfResource ftScmiDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)0x32a00000} },
};
#define ftScmiDev0Num NELEMENTS(ftScmiDev0Resources)
#endif

const struct hcfDevice hcfDeviceList[] = {
#ifdef  DRV_ARM_GICV3
    { "armGicDev", 0, VXB_BUSID_PLB, 0, armGICv3Num, armGICv3Resources},
#endif  /* DRV_ARM_GIC */

    { "primeCellSioDev",  0, VXB_BUSID_PLB, 0, primeCellSioDev0Num , primeCellSioDev0Resources },   
    { "primeCellSioDev",  1, VXB_BUSID_PLB, 0, primeCellSioDev1Num , primeCellSioDev1Resources },
    { "primeCellSioDev",  2, VXB_BUSID_PLB, 0, primeCellSioDev2Num , primeCellSioDev2Resources },
    { "primeCellSioDev",  3, VXB_BUSID_PLB, 0, primeCellSioDev3Num , primeCellSioDev3Resources },

#ifdef CONFIG_MIO_UART
/* UART and MIO_UART use the same driver, create file system device names sequentially.
 * For example:
 * -> devs
 * drv name
 *   0 /null
 *   1 /tyCo/0
 *   1 /tyCo/1
 *   1 /tyCo/2
 *   1 /tyCo/3
 *   1 /tyCo/4 <== MIO_UART6  (any)
 *   1 /tyCo/5 <== MIO_UART10 (any)
 *  Parameter <NUM_TTY> must cover all these UART ports.
 */
    { "primeCellSioDev",  4, VXB_BUSID_PLB, 0, mioSioDev6Num , mioSioDev6Resources },
    { "primeCellSioDev",  5, VXB_BUSID_PLB, 0, mioSioDev10Num , mioSioDev10Resources },
#endif

#ifdef DRV_VXBEND_FTGEM
# ifdef CONFIG_FT_GEM0
    { "gem", 0, VXB_BUSID_PLB, 0, gem0Num, gem0Resources},
# endif

# ifdef CONFIG_FT_GEM1
    { "gem", 1, VXB_BUSID_PLB, 0, gem1Num, gem1Resources},
# endif

# ifdef CONFIG_FT_GEM2
    { "gem", 2, VXB_BUSID_PLB, 0, gem2Num, gem2Resources},
# endif

# ifdef CONFIG_FT_GEM3
    { "gem", 3, VXB_BUSID_PLB, 0, gem3Num, gem3Resources},
# endif
#endif

#ifdef  DRV_ARM_GEN_SYS_TIMER
    { "armGenTimer", 0, VXB_BUSID_PLB, 0, armGenSysTimerNum, armGenSysTimerResources },
#endif  /* DRV_ARM_GEN_SYS_TIMER */

#ifdef  DRV_ARM_GEN_AUX_TIMER
    { "armAuxTimer", 0, VXB_BUSID_PLB, 0, armAuxGenTimerNum, armAuxGenTimerResources },
#endif  /* DRV_ARM_GEN_SYS_TIMER */


#ifdef DRV_PCIBUS_FT
    { "ftPcie", 0, VXB_BUSID_PLB, 0, ftPci0Num, ftPci0Resources },
#endif /* DRV_PCIBUS_FT */

#ifdef INCLUDE_PC_CONSOLE
{ "m6845Vga", 0, VXB_BUSID_PLB, 0, pentiumM6845VgaNum, pentiumM6845VgaResources },
#endif
#ifdef DRV_FTI2C
    { "ftI2c", 9, VXB_BUSID_PLB, 0, i2cDev9Num, i2cDev9Resources },
#endif /* DRV_FTI2C */

#ifdef DRV_FTQSPI
    { FT_QSPI_NAME,  0, VXB_BUSID_PLB, 0, qspiNum,  qspiResources },
#endif

#ifdef DRV_TIMER_FT_GPT
    {"ftGpTimerDev",  0, VXB_BUSID_PLB,  0, ftGptTimer0Num,    ftGptTimer0Resources},
    {"ftGpTimerDev",  1, VXB_BUSID_PLB,  0, ftGptTimer1Num,    ftGptTimer1Resources},
    {"ftGpTimerDev",  2, VXB_BUSID_PLB,  0, ftGptTimer2Num,    ftGptTimer2Resources},
    {"ftGpTimerDev",  18, VXB_BUSID_PLB, 0, ftGptTimer18Num,   ftGptTimer18Resources},
    {"ftGpTimerDev",  37, VXB_BUSID_PLB, 0, ftGptTimer37Num,   ftGptTimer37Resources},
#endif

#ifdef INCLUDE_DRV_STORAGE_AHCI
    { "ahciSata",      0, VXB_BUSID_PLB, 0, ftSata0Num,     ftSata0Resources},
    { "ahciSata",      1, VXB_BUSID_PLB, 0, ftSata1Num,     ftSata1Resources}, 
#endif /* INCLUDE_DRV_STORAGE_AHCI */

#ifdef INCLUDE_USB_XHCI_HCD
    { "vxbPlbUsbXhci",  0, VXB_BUSID_PLB, 0, vxbPlbUsbXhci0DevNum, vxbPlbUsbXhci0DevResources },
    { "vxbPlbUsbXhci",  1, VXB_BUSID_PLB, 0, vxbPlbUsbXhci1DevNum, vxbPlbUsbXhci1DevResources },
#endif /* INCLUDE_USB_XHCI_HCD */


#ifdef DRV_FTSPI
    { FT_SPI_NAME,      0,  VXB_BUSID_PLB,  0,  spi0Num,         spi0Resources },
    { FT_SPI_NAME,      1,  VXB_BUSID_PLB,  0,  spi1Num,         spi1Resources },
    { FT_SPI_NAME,      2,  VXB_BUSID_PLB,  0,  spi2Num,         spi2Resources },
    { FT_SPI_NAME,      3,  VXB_BUSID_PLB,  0,  spi3Num,         spi3Resources },
#endif
    
#ifdef DRV_FTWDT
    { "ftWdt",          0, VXB_BUSID_PLB,   0, ftWdt0Num,        ftWdt0Resources },
    { "ftWdt",          1, VXB_BUSID_PLB,   0, ftWdt1Num,        ftWdt1Resources },
#endif

#ifdef DRV_FTKEYPAD
    { "ftKeypad",       0, VXB_BUSID_PLB,   0, ftKeypad0Num,     ftKeypad0Resources },
#endif

#ifdef DRV_FTGPIO
    { "ftGpio", 0, VXB_BUSID_PLB, 0, gpioDev0Num, gpioDev0Resources },
    { "ftGpio", 1, VXB_BUSID_PLB, 0, gpioDev1Num, gpioDev1Resources },
    { "ftGpio", 2, VXB_BUSID_PLB, 0, gpioDev2Num, gpioDev2Resources },
    { "ftGpio", 3, VXB_BUSID_PLB, 0, gpioDev3Num, gpioDev3Resources },
    { "ftGpio", 4, VXB_BUSID_PLB, 0, gpioDev4Num, gpioDev4Resources },
    { "ftGpio", 5, VXB_BUSID_PLB, 0, gpioDev5Num, gpioDev5Resources },
#endif

#ifdef DRV_FTGDMA
    { "ftGDma",         0,  VXB_BUSID_PLB,  0,  ftGDma0Num,     ftGDma0Resources },
#endif

#ifdef DRV_FTPWM
    /* PWM0~7 Exist. ignore unused  */
    { "ftPwm",          0, VXB_BUSID_PLB,   0, ftPwm0Num,        ftPwm0Resources },
    { "ftPwm",          1, VXB_BUSID_PLB,   0, ftPwm1Num,        ftPwm1Resources },
    { "ftPwm",          6, VXB_BUSID_PLB,   0, ftPwm6Num,        ftPwm6Resources },
    { "ftPwm",          7, VXB_BUSID_PLB,   0, ftPwm7Num,        ftPwm7Resources },
#endif

#ifdef DRV_FTSD
# ifdef CONFIG_FT_SD0
    { "ftSdhci",0,VXB_BUSID_PLB, 0,ftSdhc0Num, ftSdhc0Resources },
# endif
# ifdef CONFIG_FT_SD1
    { "ftSdhci",1,VXB_BUSID_PLB, 0,ftSdhc1Num, ftSdhc1Resources },
# endif
#endif

#ifdef DRV_FTCAN
    { "ftCan", 0, VXB_BUSID_PLB, 0, ftCanDev0Num, ftCanDev0Resources },
    { "ftCan", 1, VXB_BUSID_PLB, 0, ftCanDev1Num, ftCanDev1Resources },
#endif
#ifdef DRV_FTE2000_DC  
   { "ftE2000DC",          0, VXB_BUSID_PLB,   0, ftE2000DCNum,        ftE2000DCResources },
#endif


#ifdef DRV_FTSCMI
{ "ftScmi", 0, VXB_BUSID_PLB, 0, ftScmiDev0Num, ftScmiDev0Resources },
#endif
};

const int hcfDeviceNum = NELEMENTS(hcfDeviceList);

VXB_INST_PARAM_OVERRIDE sysInstParamTable[] =
    {
    { NULL, 0, NULL, VXB_PARAM_END_OF_LIST, {(void *)0} }
    };


