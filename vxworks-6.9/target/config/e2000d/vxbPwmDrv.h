/* vxbPwmDrv.h - VxBus PWM interfaces header file */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbPwmDrvh
#define __INCvxbPwmDrvh 

#ifdef __cplusplus
extern "C" {
#endif

#define PWM_CMD_START       9900
#define PWM_CMD_STOP        9901
#define PWM_CMD_SET_DIV     9902
#define PWM_CMD_SET_POLAR   9903
#define PWM_CMD_SET_DEAD_BAND   9904

/* typedef */

/* The lower PWM driver publishes only this info for upper interface */
typedef struct vxbPwmInfo
    {
    char   pwmName[MAX_DRV_NAME_LEN + 1];
	
    STATUS (*pwmIoctl) (VXB_DEVICE_ID pInst, int  request, _Vx_ioctl_arg_t   arg);

    } VXB_PWM_INFO;

/* polarity of a PWM signal */
typedef enum {
/* a high signal for the duration of the duty-
 * cycle, followed by a low signal for the remainder of the pulse
 * period
 */
    PWM_POLARITY_NORMAL = 0,
    
/* a low signal for the duration of the duty-
 * cycle, followed by a high signal for the remainder of the pulse
 * period
 */
    PWM_POLARITY_INVERSED,
}PWM_POLARITY;

typedef struct {
    UINT32 nPwm;
    UINT32 nChan;

    UINT32 periodNs;
    UINT32 dutyNs;
    
    UINT32 updbclyNs;
    UINT32 dwdbclyNs;
}PWM_USER_CONFIG;

typedef struct {
    UINT32 div;
    PWM_POLARITY polarity;
    BOOL   multiDuty;
#ifdef PWM_SUPPORT_FIFO_MODE
    UINT32 dutiesNs[FIFO_DEPTH];   
#endif
}PWM_ADVANCE_CONFIG;


typedef struct {
    UINT32  cmd;
    PWM_USER_CONFIG usrCfg;
    PWM_ADVANCE_CONFIG advCfg;
}PWM_CMD;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbPwmDrvh */

