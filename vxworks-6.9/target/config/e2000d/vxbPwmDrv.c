/* vxbPwmDrv.c - PWM device library*/

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 

/* includes */

#include <vxWorks.h>
#include <semLib.h>
#include <lstLib.h>
#include <iosLib.h>
#include <stdlib.h>
#include <string.h>
#include <iolib.h>
#include <memPartLib.h>
#include <hwif/vxbus/vxBus.h>
#include "vxbPwmDrv.h"

/* defines */

#define PWM_DEVICE   "pwm"

/* typedefs */

typedef struct
    {
    DEV_HDR       devHdr;        /* device header */
    VXB_DEVICE_ID devID;         /* device id */
    char          name[MAX_DRV_NAME_LEN];

    STATUS (*pwmDrvIoctl)(VXB_DEVICE_ID * pDev,  int   request,    _Vx_ioctl_arg_t    arg);
    
    } PWM_DEV;

/* pwm device file descriptor */

typedef struct      /* PWM_FILE_DESC */
    {
    NODE         fdNode;       /* linked list node info */
    SEM_ID       fdSemId;      /* semaphore for this file descriptor */
    int          fdStatus;     /* (OK | NOT_IN_USE) */
    PWM_DEV * pPwmDevice;
    } PWM_FILE_DESC;

/* File descriptor status values */

#define FD_AVAILABLE      (-1)    /* file descriptor available */
#define FD_IN_USE         0       /* file descriptor in-use */

/* max files open at once */

#define PWM_DEF_MAX_FILES  10

/* LOCALS */

LOCAL int pwmDrvNum = ERROR;   /* driver number assigned to this driver */

LOCAL LIST pwmFdActiveList;    /* linked list of in-use Fd's */
LOCAL LIST pwmFdFreeList;      /* linked list of avail. Fd's */
LOCAL SEM_ID pwmFdListSemId;   /* file descr list semaphore  */

/* forward declarations */

LOCAL STATUS pwmDrvLoad (VXB_DEVICE_ID devID);
LOCAL PWM_FILE_DESC * pwmDrvOpen (PWM_DEV * pPwmDev, char * name, int flags);

LOCAL STATUS pwmDrvIoctl (PWM_FILE_DESC * pFd, int request, _Vx_ioctl_arg_t arg);
LOCAL STATUS pwmDrvClose (PWM_FILE_DESC * pFd);
LOCAL PWM_FILE_DESC * pwmFdGet (void);
LOCAL void pwmFdFree (PWM_FILE_DESC * pFd);

METHOD_DECL(vxbPwmInfoGet);


/*******************************************************************************
*
* pwmDrv - install a pwm driver
*
* This routine initializes the pwm driver. It is called automatically
* when VxWorks is configured with the INCLUDE_PWMDRV component.
*
* It must be called exactly once, before any other routine in the library.  
* The maximum number of file descriptors that may be open at once are MAXFILES. 
* This routine allocates and sets up the necessary memory structures and 
* initializes semaphores.
*
* This routine also installs pwm library routines in the VxWorks I/O
* system driver table.  The driver number assigned to pwm device is placed in
* the global variable "pwmDrvNum".  This number will later be associated
* with system file descriptors opened to pwm devices.
*
* RETURNS: OK, or ERROR if the I/O system cannot install the driver.
*
* ERRNO: N/A.
*/

STATUS pwmDrv(void)
    {
    PWM_FILE_DESC * pEepromFd;  /* pointer to created file descriptor */
    int ix;                        /* index var */

    /* Check if driver already installed */

    if (pwmDrvNum > 0)
        {
        return (OK);
        }

    pwmDrvNum = iosDrvInstall ((DRV_CREATE_PTR) pwmDrvOpen,
                                  (DRV_REMOVE_PTR) NULL,
                                  (DRV_OPEN_PTR) pwmDrvOpen,
                                  (DRV_CLOSE_PTR) pwmDrvClose,
                                  (DRV_READ_PTR) NULL,
                                  (DRV_WRITE_PTR) NULL,
                                  (DRV_IOCTL_PTR) pwmDrvIoctl);

    if (pwmDrvNum <= 0)
        {
        return (ERROR);
        }

    /* Create semaphore for locking access to file descriptor list */

    pwmFdListSemId = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE);

    /* Can't create semaphore */

    if (pwmFdListSemId == NULL)
        {
        return (ERROR);
        }

    /* Take control of fd list */

    if (semTake (pwmFdListSemId, WAIT_FOREVER) != OK)
        return ERROR;

    lstInit(&pwmFdFreeList);
    lstInit(&pwmFdActiveList);

    /* Allocate memory for required number of file descriptors */

    pEepromFd = (PWM_FILE_DESC *)
                malloc (PWM_DEF_MAX_FILES * sizeof (PWM_FILE_DESC));

    if (NULL == pEepromFd)
        {
        (void) semGive (pwmFdListSemId);
        return (ERROR);
        }

    bzero ((char *) pEepromFd, (PWM_DEF_MAX_FILES * sizeof (PWM_FILE_DESC)));

    for (ix = 0; ix < PWM_DEF_MAX_FILES; ix++)
        {
        pEepromFd->fdStatus = FD_AVAILABLE;

        /* Create semaphore for this fd (initially available) */

        pEepromFd->fdSemId = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE);

        if (pEepromFd->fdSemId == NULL)
            {
            free (pEepromFd);
            (void) semGive (pwmFdListSemId);
            return (ERROR);     /* Could not create semaphore */
            }

        /* Add file descriptor to free list */

        lstAdd (&pwmFdFreeList, &pEepromFd->fdNode);

        /* Next Fd */

        pEepromFd++;
        }

    /* Release Fd lists */

    (void) semGive (pwmFdListSemId);

    vxbDevIterate (pwmDrvLoad, (void *) pwmDrvNum, VXB_ITERATE_INSTANCES);

    return OK;
    }

/*******************************************************************************
*
* pwmDrvLoad - retrieve the pwm info and create device
*
* RETURN - OK or ERROR
*
* ERRNO - NA
*/

LOCAL STATUS pwmDrvLoad
    (
    VXB_DEVICE_ID        devID     /* vxBus device ID */
    )
    {
    PWM_DEV * pPwmDev = NULL;
    char         name[MAX_DRV_NAME_LEN];
    FUNCPTR      pFunc;
    struct       vxbPwmInfo * pCandidate = NULL;

    /* Check for the validity of the function parameters */

    VXB_ASSERT (devID != NULL, ERROR);

    pPwmDev = malloc (sizeof (PWM_DEV));

    VXB_ASSERT (pPwmDev != NULL, ERROR);

    bzero ((char *) pPwmDev, sizeof (PWM_DEV));

    /* Retrieve the pMethod that the vxBus device should maintain */

    pFunc = vxbDevMethodGet (devID, (VXB_METHOD_ID) vxbPwmInfoGet_desc);

    if (pFunc == NULL)
        {
        free (pPwmDev);
        return OK;
        }

    /* Retrieve the PWM info */

    (*pFunc) (devID, &pCandidate);

    /* Retrieve the PWM device name */

    strncpy (pPwmDev->name, pCandidate->pwmName, MAX_DRV_NAME_LEN);

    /* Retrieve the PWM device info */
    pPwmDev->pwmDrvIoctl = (void *) pCandidate->pwmIoctl;

    pPwmDev->devID = devID;

    /* Add the device to the I/O systems device list. 
     * Important: /pwm/xxx <==match==> devID->unitNumber 
     */

    snprintf (name, MAX_DRV_NAME_LEN, "/%s/%d", PWM_DEVICE, devID->unitNumber);

    if (iosDevAdd ((DEV_HDR *) & (pPwmDev->devHdr), name, pwmDrvNum) != OK)
        {
        free (pPwmDev);
        return ERROR;   
        }

    return OK;
    }

/*******************************************************************************
*
* pwmDrvOpen - open pwm device
*
* This routine is called to open a pwm.  It returns a pointer to the
* device.  This routine is normally reached only via the I/O system.
*
* RETURNS: The opaque PWM_FILE_DESC ptr. For this driver it is really a 
* pointer to a PWM_FILE_DESC structure.
*
* ERRNO: N/A
*/

LOCAL PWM_FILE_DESC * pwmDrvOpen
    (
    PWM_DEV * pPwmDev,
    char *       name,
    int          flags
    )
    {
    PWM_FILE_DESC * pFd;     /* file descriptor pointer */

    /* Make sure it's a valid pwm id */

    if (pPwmDev == NULL)
        {
        return((PWM_FILE_DESC *) ERROR);
        }

    /* Non-null filename */

    if (name[0] != EOS)
        {
        return ((PWM_FILE_DESC *) ERROR);
        }

    /* Get a free file descriptor */

    if ((pFd = pwmFdGet ()) == NULL)
        {
        return ((PWM_FILE_DESC *) ERROR);
        }

    /* Take control of fd */

    if (semTake (pFd->fdSemId, WAIT_FOREVER) != OK)
        {
        return ((PWM_FILE_DESC *) ERROR);
        }

    /* Save the flags */

    pFd->pPwmDevice = (PWM_DEV *) pPwmDev;


    /* Release fd */

    (void)semGive (pFd->fdSemId);

    return (pFd);
    }

/*******************************************************************************
*
* pwmDrvIoctl - do pwm device control function
*
* This routine performs the ioctl() function by PWM_DEV.
*
* RETURNS: OK, or ERROR if function failed or unknown function, or
* current byte pointer for FIOWHERE.
*
* ERRNO - NA
*/

LOCAL STATUS pwmDrvIoctl
    (
    PWM_FILE_DESC * pFd,        /* device to control */
    int                request,    /* request code */
    _Vx_ioctl_arg_t    arg         /* some argument */
    )
    {
    STATUS retVal = ERROR;
    int buf = 0;
    PWM_DEV * pVd;

    /* Make sure it's a valid id */

    if (pFd == NULL || (pVd = pFd->pPwmDevice) == NULL)
        {
        return ERROR;
        }

    /* Take control of fd */

    if (semTake (pFd->fdSemId, WAIT_FOREVER) != OK)
        {
        return ERROR;
        }

    if (pVd->pwmDrvIoctl (pVd->devID,
                         request,
                         arg) != OK)
        {
        (void) semGive (pFd->fdSemId);
        return ERROR;
        }
	
	retVal = OK;

    /* Release fd */

    (void) semGive (pFd->fdSemId);

    return (retVal);
    }

/*******************************************************************************
*
* pwmDrvClose - close a pwm device 
*
* This routine is called to close a pwm.  This routine is normally reached
* only via the I/O system.
*
* RETURNS:  OK or ERROR if NULL pwm device pointer.
*
* ERRNO: N/A.
*/

LOCAL STATUS pwmDrvClose
    (
    PWM_FILE_DESC * pEepromFd
    )
    {
    /* Take control of file descriptor */

    if (semTake (pEepromFd->fdSemId, WAIT_FOREVER) != OK)
        {
        return (ERROR);
        }

    /* Put fd on free list */

    pwmFdFree (pEepromFd);

    /* Release fd */

    (void) semGive (pEepromFd->fdSemId);

    return (OK);
    }

/*******************************************************************************
*
* pwmDevRemove - remove a pwm device
*
* This routine deletes a pwm device. The device is deleted with it own name.
*
* For example, to delete the device, the proper call would be:
* \cs
*   pwmDevRemove ("/pwm/0");
* \ce
*  
* RETURNS: OK, or ERROR if the device doesn't exist.
*
* ERRNO: N/A.
*/

STATUS pwmDevRemove
    (
    char * name           /* device name */
    )
    {
    DEV_HDR * pDevHdr;

    /* Get the device pointer corresponding to the device name */

    if ((pDevHdr = iosDevFind (name, NULL)) == NULL)
        {
        return (ERROR);
        }

    /* Delete the device from the I/O system */

    (void) iosDevDelete (pDevHdr);

    /* Free the device pointer */

    free ((PWM_DEV *) pDevHdr);

    return (OK);
    }

/*******************************************************************************
*
* pwmFdGet - get an available file descriptor
*
* This routine obtains a free pwm file descriptor.
*
* RETURNS: Pointer to file descriptor, or NULL if none available.
*
* ERRNO - NA
*/

LOCAL PWM_FILE_DESC * pwmFdGet
    (
    void
    )
    {
    PWM_FILE_DESC * pFd;

    /* Take control of Fd lists */

    if (semTake (pwmFdListSemId, WAIT_FOREVER) != OK)
        return NULL;

    /* Get a free Fd */

    pFd = (PWM_FILE_DESC *) lstGet (&pwmFdFreeList);

    if (pFd != NULL)
        {
        /* Mark Fd as in-use */
        pFd->fdStatus = FD_IN_USE;

        /* Add to active list */
        lstAdd (&pwmFdActiveList, (NODE *) pFd);
        }

    /* Release Fd lists */

    (void) semGive (pwmFdListSemId);

    return (pFd);
    }

/*******************************************************************************
*
* pwmFdFree - free a file descriptor
*
* This routine removes a pwm device file descriptor from the active
* Fd list and places it on the free Fd list.
*
* RETURN - NA
*
* ERRNO - NA
*/

LOCAL void pwmFdFree
    (
    PWM_FILE_DESC * pEepromFd      /* pointer to file descriptor to free */
    )
    {
    /* Take control of Fd lists */

    if (semTake (pwmFdListSemId, WAIT_FOREVER) != OK)
        return;

    pEepromFd->fdStatus = FD_AVAILABLE;

    /* Remove Fd from active list */

    lstDelete (&pwmFdActiveList, &pEepromFd->fdNode);

    /* Add Fd to free list */

    lstAdd (&pwmFdFreeList, &pEepromFd->fdNode);

    /* Release Fd lists */

    (void) semGive (pwmFdListSemId);
    }

#define FT_PWM_TEST
#ifdef FT_PWM_TEST

LOCAL int testPwmFd = 0;
LOCAL int testPwmId = 0;
LOCAL int testPwmChannel = 0;

int pwmDrvNumGet(void)
{
    return pwmDrvNum;
}

int testPwmOpen(int pwmId)
{
    int fd;
    char pwmFileName[16];

    pwmDrv(); /*install device driver file*/

    bzero(pwmFileName, sizeof(pwmFileName));
    sprintf(pwmFileName, "/pwm/%d" , pwmId);
    
    fd = open(pwmFileName, O_RDWR, 0644);
    if (fd < 0)
        {
        printf("Failed to open %s device!\r\n", pwmFileName);
        printErrno(0);
        return -1;
        }
	else
		{
		printf("(open %s) fd=%d   \r\n", pwmFileName, fd);
		}

    testPwmFd = fd;
    testPwmId = pwmId;
    return fd;
}

void testPwmClose(void)
{
    close(testPwmFd);
    return ;
}

int testPwmFdShow(void)
{
    printf("testPwmFd = %d\r\n", testPwmFd);
    printf("iosFdShow():\r\n");
    iosFdShow();
	return 0;
}


STATUS testPwmWaveConfig(void)
{
    int rtn;
    PWM_USER_CONFIG conf;

    conf.nPwm = testPwmId;
    conf.nChan = testPwmChannel;
    conf.periodNs = 10000; /* ns */
    conf.dutyNs = 2000; /* ns */
    conf.updbclyNs = 0;
    conf.dwdbclyNs = 0; /* bypass Dead Band */

    rtn = ioctl(testPwmFd, FIOSETOPTIONS, &conf);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }

}
STATUS testPwmWaveStart(void)
{
    int rtn;
    PWM_CMD cmd;

    cmd.cmd = PWM_CMD_START;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}


STATUS testPwmWavePolarityNormal(void)
{
    int rtn;
    PWM_CMD cmd;

    cmd.cmd = PWM_CMD_SET_POLAR;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;
    cmd.advCfg.polarity = PWM_POLARITY_NORMAL;

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}

STATUS testPwmWavePolarityInversed(void)
{
    int rtn;
    PWM_CMD cmd;

    cmd.cmd = PWM_CMD_SET_POLAR;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;
    cmd.advCfg.polarity = PWM_POLARITY_INVERSED;

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}

STATUS testPwmWaveDivSet(UINT32 DivFromOne)
{
    int rtn;
    PWM_CMD cmd;

    if (DivFromOne < 1)
        {
        printf("div=%d too small, should in [1 ~ 4096]\r\n", DivFromOne);
        return ERROR;
        }
    if (DivFromOne > 4096)
        {
        printf("div=%d too big, should in [1 ~ 4096]\r\n", DivFromOne);
        return ERROR;
        }
    

    cmd.cmd = PWM_CMD_SET_DIV;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;
    cmd.advCfg.div = DivFromOne - 1;

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}

STATUS testPwmWaveDeadBandSet(void)
{
    int rtn;
    PWM_CMD cmd;

    cmd.cmd = PWM_CMD_SET_DEAD_BAND;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;
    cmd.usrCfg.updbclyNs = 1000; /*channel-0*/
    cmd.usrCfg.dwdbclyNs = 0;    /*channel-1*/

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}
STATUS testPwmWaveDeadBandBypass(void)
{
    int rtn;
    PWM_CMD cmd;

    cmd.cmd = PWM_CMD_SET_DEAD_BAND;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;
    cmd.usrCfg.updbclyNs = 0; /*channel-0*/
    cmd.usrCfg.dwdbclyNs = 0; /*channel-1*/

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}

STATUS testPwmWaveStop(void)
{
    int rtn;
    PWM_CMD cmd;

    cmd.cmd = PWM_CMD_STOP;
    cmd.usrCfg.nPwm = testPwmId;
    cmd.usrCfg.nChan = testPwmChannel;

    rtn = ioctl(testPwmFd, FIOFCNTL, &cmd);

    if (0 == rtn)
        {
        printf("L%d: Successfully\r\n", __LINE__);
        return OK;
        }
    else
        {
        printf("L%d: Failed! rtn=%d;errno=0x%x\r\n", __LINE__, rtn, errnoGet());
        return ERROR;
        }
}

#endif

