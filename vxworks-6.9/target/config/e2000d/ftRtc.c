/* ftRtc.c - Phytium Real Timer Clock on chip driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
/* includes */

#include <vxWorks.h>
#include <stdio.h>
#include <syslib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <private/timerLibP.h>
#include <envLib.h>


#define SECS_PER_MIN    60
#define MINS_PER_HOUR   60
#define HOURS_PER_DAY   24
#define DAYS_PER_WEEK   7
#define DAYS_PER_NYEAR  365
#define DAYS_PER_LYEAR  366
#define SECS_PER_HOUR   (SECS_PER_MIN * MINS_PER_HOUR)
#define SECS_PER_DAY    ((long) SECS_PER_HOUR * HOURS_PER_DAY)
#define MONS_PER_YEAR   12

#define TM_SUNDAY   0
#define TM_MONDAY   1
#define TM_TUESDAY  2
#define TM_WEDNESDAY    3
#define TM_THURSDAY 4
#define TM_FRIDAY   5
#define TM_SATURDAY 6

#define TM_JANUARY  0
#define TM_FEBRUARY 1
#define TM_MARCH    2
#define TM_APRIL    3
#define TM_MAY      4
#define TM_JUNE     5
#define TM_JULY     6
#define TM_AUGUST   7
#define TM_SEPTEMBER    8
#define TM_OCTOBER  9
#define TM_NOVEMBER 10
#define TM_DECEMBER 11

#define TM_YEAR_BASE    1900

#define EPOCH_YEAR  2000
#define EPOCH_WDAY  TM_SATURDAY
#define isleap(y) (((y) % 4) == 0 && (((y) % 100) != 0 || ((y) % 400) == 0))

LOCAL const int    mon_lengths[2][MONS_PER_YEAR] = {
    {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

LOCAL const int    year_lengths[2] = {
    DAYS_PER_NYEAR, DAYS_PER_LYEAR
};

LOCAL const char * const dayname[] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

LOCAL const char * const monthname[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };


#define BILLION         1000000000  /* 1000 million nanoseconds / second */
IMPORT  struct clock _clockRealtime;

IMPORT STATUS vxbRtcSet (struct tm * rtcTime);
IMPORT STATUS vxbRtcGet (struct tm * rtcTime);

/*
*    struct tm (in time.h - POSIX time header)
*        {
*        int tm_sec;     seconds after the minute     - [0, 59]
*        int tm_min;     minutes after the hour       - [0, 59]
*        int tm_hour;    hours after midnight         - [0, 23]
*        int tm_mday;    day of the month             - [1, 31]
*        int tm_mon;     months since January         - [0, 11] <==since zero
*        int tm_year;    years since 1900          <===========           
*        int tm_wday;    days since Sunday            - [0, 6]
*        int tm_yday;    days since January 1         - [0, 365]
*        int tm_isdst;   Daylight Saving Time flag
*        };
*    
*    
*    -> sysRtcSet
*    Enter the year: 123
*    Enter the month: 5
*    Enter the day of the month: 8
*    Enter the hour(0-23): 18
*    Enter the minute: 15
*    Enter the second: 0
*    value = 0 = 0x0
*    ->
*    -> sysRtcGet
*    THU JUN 08 18:15:10 2023
*    value = 0 = 0x0
*    ->
*/

LOCAL int 
get2 (char *s)
{
    if (s[0] < '0' || s[0] > '9' || s[1] < '0' || s[1] > '9')
      return -1;
    return (s[0] - '0') * 10 + (s[1] - '0');
}


STATUS tgtSetTime(struct tm *tm)
{
    STATUS st;

    if (NULL == tm)
    {
        return ERROR;
    }
    
    if (tm->tm_isdst != 0)
    {
       tm->tm_isdst = 0; /*don't use Daylight Saving Time*/
    }
    
    st = vxbRtcSet(tm);
    return st;
}


time_t tgtGetTime(void)
{
    STATUS st;
    time_t t;
    struct tm rtcTime;

    st = vxbRtcGet (&rtcTime);

    if (ERROR == st)
    {
        return ERROR;
    }

    if (rtcTime.tm_isdst != 0)
    {
       rtcTime.tm_isdst = 0; /*don't use Daylight Saving Time*/
    }

    t = mktime(&rtcTime);
                                                             
    return(t);
}

struct tm* sysTimeGet(void)
{
    time_t stime;
    struct tm * tm;

    
    time(&stime);
    tm = gmtime(&stime);
    
    printf ("%s %s %2d %02d:%02d:%02d %d\n",
        dayname[tm->tm_wday], monthname[tm->tm_mon], tm->tm_mday,
        tm->tm_hour, tm->tm_min, tm->tm_sec, TM_YEAR_BASE+tm->tm_year);
    return tm;
}

void sysTimeInit(void)
{
    struct timespec ts;
    ts. tv_sec = tgtGetTime();
    ts. tv_nsec = 0;
    if(clock_settime( CLOCK_REALTIME, &ts)!=OK)
        printf("clock_settime failed!!!\r\n");
}


/*******************************************************************************
*
* sysTimeSet - set system RTC time
*
* Format example: sysTimeSet("202108131508.50") 
*
* NOTE:
*
* RETURNS: 0 OK; Error others
*
* ERRNO
*/

int sysTimeSet(char *date)
{
    struct tm * tm;
    struct timespec ts;
    time_t t;

    t = tgtGetTime ();
    tm = gmtime (&t);
    if (date != NULL)
    {
        char *s;
        int hadsecs = 0;
        unsigned int n;
        
        s = date;
        n = strlen (s);
        
        /* seconds */
        if (n >= 3 && s[n-3] == '.') { 
            if ((tm->tm_sec = get2 (&s[n-2])) < 0 || tm->tm_sec >= 60) {
                printf ("bad seconds\n");
                return (1);
            }
            hadsecs = 1;
            n -= 3;
        }

        /* minutes */
        if (n >= 2) {
            if ((tm->tm_min = get2 (&s[n-2])) < 0 || tm->tm_min >= 60) {
                printf ("bad minutes\n");
                return (1);
            }
            n -= 2;
            if (!hadsecs)
              tm->tm_sec = 0;
        }

        /* hours */
        if (n >= 2) {
            if ((tm->tm_hour = get2 (&s[n-2])) < 0 || tm->tm_hour >= 24) {
                printf ("bad hours\n");
                return (1);
            }
            n -= 2;
        }

        /* days */
        if (n >= 2) {
            if ((tm->tm_mday = get2 (&s[n-2])) < 0 || tm->tm_mday > 31) {
                /* test could be more thorough... */
                printf ("bad day\n");
                return (1);
            }
            n -= 2;
        }

        /* months */
        if (n >= 2) {
            if ((tm->tm_mon = get2 (&s[n-2])) < 1 || tm->tm_mon > 12) {
                printf ("bad month\n");
                return (1);
            }
            tm->tm_mon -= 1;    /* zero based */
            n -= 2;
        }

        /* years */
        if (n >= 2) {
            if ((tm->tm_year = get2 (&s[n-2])) < 0) {
                printf ("bad year\n");
                return (1);
            }
            if(n >= 4 && get2(&s[n-4]) == 20) {
                n -=4;
            }
            else if(n >= 4 && get2(&s[n-4]) != 20) {
                n -= 4;
                printf("bad year,please input year in [2000-2099]\n");
                return (1);
            }
            else
                n -= 2;
        }
        
        if (n != 0) {
            printf ("%s: bad date syntax\n", *date);
            return (1); 
        }
        
        tm->tm_year =tm->tm_year +100;
        tgtSetTime (tm);

    }
    
    ts. tv_sec = tgtGetTime();
    ts. tv_nsec = 0;
    if(clock_settime( CLOCK_REALTIME, &ts)!=OK){
        printf("clock_settime failed!!!\r\n");
    }
    time(&t);
    tm = gmtime(&t);
    printf ("%s %s %2d %02d:%02d:%02d %d\n",
        dayname[tm->tm_wday], monthname[tm->tm_mon], tm->tm_mday,
        tm->tm_hour, tm->tm_min, tm->tm_sec, TM_YEAR_BASE+tm->tm_year);

    return (0);
}


/*******************************************************************************
*
* rtcInfoShow - show info on RTC clock
*
*
* RETURNS:
* 0 (OK), or -1 (ERROR) if <clock_id> is invalid
*
* ERRNO: EINVAL
*
* \NOMANUAL
*/
int rtcInfoShow
    (
    void
    )
    {
    static char *title1 = "  seconds    nanosecs  freq (hz)  resolution\n";
    static char *title2 = "---------- ----------- ---------- ----------\n";  
    static char *title3 = "\n  base ticks          base secs  base nsecs\n";
    static char *title4 = "--------------------  ---------- ----------\n";

    clockid_t clock_id = CLOCK_REALTIME;
    struct timespec tp;

    if (clock_id != CLOCK_REALTIME)
    {
    errno = EINVAL;
    return (ERROR);
    }

    (void) clock_gettime (clock_id, &tp);

    printf (title1);
    printf (title2);

    printf (" %8.8ld   %9.9ld   %8.8d   %8.8d\n",
        tp.tv_sec, tp.tv_nsec, sysClkRateGet(), BILLION / sysClkRateGet());

    printf (title3);
    printf (title4);

    printf(" 0x%08x%08x    %8.8ld   %9.9ld\n",
       (UINT) (_clockRealtime.tickBase >> 32),
       (UINT)  _clockRealtime.tickBase, 
       _clockRealtime.timeBase.tv_sec, 
       _clockRealtime.timeBase.tv_nsec);

    return (OK);
    }

void zone8plusSet(void)
{
	/* env: from private to public (global) */
	envPrivateDestroy(0);


    /* time zone 8+ with daylight time save: from Apr.1, 2AM, to Oct,1,2AM */
    /*
	if(putenv("TIMEZONE=CTT::-480:040102:100102")<0)
	{
		printf("the putenv is error................\r\n");
	}
	*/

    /* time zone 8+ without daylight time save */
	if(putenv("TIMEZONE=CTT::-480::")<0)
	{
		printf("the putenv is error................\r\n");
	}
}
