/* vxbFtKeypad.c - Driver for Phytium Matrix Keypad controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <taskLib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <semLib.h>
#include <ffsLib.h>
#include <ioLib.h>
#include <tyLib.h>
#include <errnoLib.h>
#include <fcntl.h>
#include <usrLib.h>
#include <wdLib.h>
#include <private/wdLibP.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/util/hwMemLib.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include "vxbFtKeypad.h"
#include "defs.h"

#undef KPD_DBG_ON
#ifdef  KPD_DBG_ON
#define KPD_DBG_IRQ         0x00000001
#define KPD_DBG_RW          0x00000002
#define KPD_DBG_ERR         0x00000004
#define KPD_DBG_ALL         0xffffffff
#define KPD_DBG_OFF         0x00000000

LOCAL UINT32 kpdDbgMask = KPD_DBG_ALL;
IMPORT FUNCPTR _func_logMsg;

#define KPD_DBG(mask, string, a, b, c, d, e, f)         \
    if ((kpdDbgMask & mask) || (mask == KPD_DBG_ALL))   \
        if (_func_logMsg != NULL)                       \
            (* _func_logMsg)(string, a, b, c, d, e, f)
#else
#define KPD_DBG(mask, string, a, b, c, d, e, f)
#endif  /* KPD_DBG_ON */

/* local variables */

LOCAL int ftKpdDrvNum = 0;           /* driver number assigned to this driver */

/* forward declarations */

IMPORT void sysUsDelay(int us);
LOCAL void vxbFtKeypadInstInit (VXB_DEVICE_ID);
LOCAL void vxbFtKeypadInstInit2 (VXB_DEVICE_ID);
LOCAL void vxbFtKeypadInstConnect (VXB_DEVICE_ID);
LOCAL void vxbFtKeypadISR(VXB_DEVICE_ID);
LOCAL void vxbFtKeypadConfig(VXB_DEVICE_ID);
LOCAL void vxbFtKeypadInhibit(VXB_DEVICE_ID);
LOCAL void vxbFtKeypadDisable (VXB_DEVICE_ID);
LOCAL STATUS vxbFtKeypadEnable(VXB_DEVICE_ID);
LOCAL BOOL matrixKeypadMapKey(UINT16 *keycodes,
				  UINT32 rows, UINT32 cols,
				  UINT32 rowShift, UINT32 key);
LOCAL STATUS matrixKeypadBuildKeymap(UINT16 *keycodes,
			       UINT32 rows, UINT32 cols,
			       UINT32 *keymap,
			       UINT16 keymapSize);

LOCAL UINT32  ftKpdOpen
    (   
    DEV_HDR * hdr,
    char    * name,
    int     flag,
    int     mode
    );
LOCAL int ftKpdClose(DEV_HDR * pIoDev);
LOCAL STATUS ftKpdIoctl
(
 DEV_HDR * pIoDev,
 int            command,
 int            arg
 );
LOCAL STATUS ftKpdDrv(void);
LOCAL STATUS ftKpdDevCreate
    (
    char *        name,
    VXB_DEVICE_ID pDev,
    int           rdBufSize,
    int           wrtBufSize
    );
void vxbFtKeypadReportCallback(VXB_DEVICE_ID pDev, UINT16 key, UINT16 keyStat);

LOCAL struct drvBusFuncs vxbFtKeypadDrvFuncs =
    {
    vxbFtKeypadInstInit,      /* devInstanceInit */
    vxbFtKeypadInstInit2,     /* devInstanceInit2 */
    vxbFtKeypadInstConnect    /* devConnect */
    };

LOCAL device_method_t vxbFtKeypadDrv_methods[] =
    {
    DEVMETHOD_END
    };

/* phytium KPD VxBus registration info */

LOCAL struct vxbDevRegInfo vxbFtKeypadDrvRegistration =
    {
    NULL,                           /* pNext */
    VXB_DEVID_DEVICE,               /* devID */
    VXB_BUSID_PLB,                  /* busID = PLB */
    VXB_VER_4_0_0,                  /* busVer */
    FT_KEYPAD_DRIVER_NAME,          /* drvName */
    &vxbFtKeypadDrvFuncs,           /* pDrvBusFuncs */
    &vxbFtKeypadDrv_methods[0],     /* pMethods */
    NULL                            /* devProbe */
    };

#define KEYPAD_4x4
#ifdef KEYPAD_4x4
LOCAL UINT32 keymap[] = {
    0x00000012,	/* KEY_E          */
	0x00010013,	/* KEY_R          */
	0x00020014,	/* KEY_T          */
	0x00030066,	/* KEY_HOME       */

	0x01000020,	/* KEY_D          */
	0x01010021,	/* KEY_F          */
	0x01020022,	/* KEY_G          */
	0x010300e7,	/* KEY_SEND       */

	0x0200002d,	/* KEY_X          */
	0x0201002e,	/* KEY_C          */
	0x0202002f,	/* KEY_V          */
	0x0203006b,	/* KEY_END        */

	0x0300002c,	/* KEY_Z          */
	0x0301004e,	/* KEY_KPLUS      */
	0x03020030,	/* KEY_B          */
	0x0303003b,	/* KEY_F1         */
    };
#endif

#ifdef KEYPAD_8x8
LOCAL UINT32 keymap[] = {
    0x00000012,	/* KEY_E          */
	0x00010013,	/* KEY_R          */
	0x00020014,	/* KEY_T          */
	0x00030066,	/* KEY_HOME       */
	0x0004003f,	/* KEY_F5         */
	0x000500f0,	/* KEY_UNKNOWN    */
	0x00060017,	/* KEY_I          */
	0x0007002a,	/* KEY_LEFTSHIFT  */
	0x01000020,	/* KEY_D          */
	0x01010021,	/* KEY_F          */
	0x01020022,	/* KEY_G          */
	0x010300e7,	/* KEY_SEND       */
	0x01040040,	/* KEY_F6         */
	0x010500f0,	/* KEY_UNKNOWN    */
	0x01060025,	/* KEY_K          */
	0x0107001c,	/* KEY_ENTER      */
	0x0200002d,	/* KEY_X          */
	0x0201002e,	/* KEY_C          */
	0x0202002f,	/* KEY_V          */
	0x0203006b,	/* KEY_END        */
	0x02000041,	/* KEY_F7         */
	0x020100f0,	/* KEY_UNKNOWN    */
	0x02020034,	/* KEY_DOT        */
	0x0203003a,	/* KEY_CAPSLOCK   */
	0x0300002c,	/* KEY_Z          */
	0x0301004e,	/* KEY_KPLUS      */
	0x03020030,	/* KEY_B          */
	0x0303003b,	/* KEY_F1         */
	0x03040042,	/* KEY_F8         */
	0x030500f0,	/* KEY_UNKNOWN    */
	0x03060018,	/* KEY_O          */
	0x03070039,	/* KEY_SPACE      */
	0x04000011,	/* KEY_W          */
	0x04010015,	/* KEY_Y          */
	0x04020016,	/* KEY_U          */
	0x0403003c,	/* KEY_F2         */
	0x04040073,	/* KEY_VOLUMEUP   */
	0x040500f0,	/* KEY_UNKNOWN    */
	0x04060026,	/* KEY_L          */
	0x04070069,	/* KEY_LEFT       */
	0x0500001f,	/* KEY_S          */
	0x05010023,	/* KEY_H          */
	0x05020024,	/* KEY_J          */
	0x0503003d,	/* KEY_F3         */
	0x05040043,	/* KEY_F9         */
	0x05050072,	/* KEY_VOLUMEDOWN */
	0x05060032,	/* KEY_M          */
	0x0507006a,	/* KEY_RIGHT      */
	0x06000010,	/* KEY_Q          */
	0x0601001e,	/* KEY_A          */
	0x06020031,	/* KEY_N          */
	0x0603009e,	/* KEY_BACK       */
	0x0604000e,	/* KEY_BACKSPACE  */
	0x060500f0,	/* KEY_UNKNOWN    */
	0x06060019,	/* KEY_P          */
	0x06070067,	/* KEY_UP         */
	0x07000094,	/* KEY_PROG1      */
	0x07010095,	/* KEY_PROG2      */
	0x070200ca,	/* KEY_PROG3      */
	0x070300cb,	/* KEY_PROG4      */
	0x0704003e,	/* KEY_F4         */
	0x070500f0,	/* KEY_UNKNOWN    */
	0x07060160,	/* KEY_OK         */
	0x0707006c	/* KEY_DOWN       */
    };
#endif


/*  key board Maps Japanese and english */
LOCAL UCHAR keyMapAscii [2][4][144] =
    { 
    { /* Japanese Enhanced keyboard */
    { /* unshift code */
    0,  0x1b,   '1',   '2',   '3',   '4',   '5',   '6',	/* scan  0- 7 */
  '7',   '8',   '9',   '0',   '-',   '^',  0x08,  '\t',	/* scan  8- F */
  'q',   'w',   'e',   'r',   't',   'y',   'u',   'i',	/* scan 10-17 */
  'o',   'p',   '@',   '[',  '\r',   CN,    'a',   's',	/* scan 18-1F */
  'd',   'f',   'g',   'h',   'j',   'k',   'l',   ';',	/* scan 20-27 */
  ':',     0,   SH,    ']',   'z',   'x',   'c',   'v',	/* scan 28-2F */
  'b',   'n',   'm',   ',',   '.',   '/',   SH,    '*',	/* scan 30-37 */
  ' ',   ' ',   CP,     0,     0,     0,     0,     0,	/* scan 38-3F */
    0,     0,     0,     0,     0,   NM,    ST,    '7',	/* scan 40-47 */
  '8',   '9',   '-',   '4',   '5',   '6',   '+',   '1',	/* scan 48-4F */
  '2',   '3',   '0',   '.',     0,     0,     0,     0,	/* scan 50-57 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 58-5F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 60-67 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 68-6F */
  ' ',     0,     0,  '\\',     0,     0,   ' ',     0,	/* scan 70-77 */
    0,   ' ',     0,     0,     0,  '\\',     0,     0,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,    'F',   'A', /* extended */
    0,   'D',   'C',     0,   'B',     0,    '@',   'P'  /* extended */
    },
    { /* shift code */
    0,  0x1b,   '!',   '"',   '#',   '$',   '%',   '&',	/* scan  0- 7 */
 '\'',   '(',   ')',     0,   '=',   '~',  0x08,  '\t',	/* scan  8- F */
  'Q',   'W',   'E',   'R',   'T',   'Y',   'U',   'I',	/* scan 10-17 */
  'O',   'P',   '`',   '{',  '\r',   CN,    'A',   'S',	/* scan 18-1F */
  'D',   'F',   'G',   'H',   'J',   'K',   'L',   '+',	/* scan 20-27 */
  '*',     0,   SH,    '}',   'Z',   'X',   'C',   'V',	/* scan 28-2F */
  'B',   'N',   'M',   '<',   '>',   '?',   SH,    '*',	/* scan 30-37 */
  ' ',   ' ',   CP,      0,     0,     0,     0,     0,	/* scan 38-3F */
    0,     0,     0,     0,     0,   NM,    ST,    '7',	/* scan 40-47 */
  '8',   '9',   '-',   '4',   '5',   '6',   '+',   '1',	/* scan 48-4F */
  '2',   '3',   '0',   '.',     0,     0,     0,     0,	/* scan 50-57 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 58-5F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 60-67 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 68-6F */
  ' ',     0,     0,   '_',     0,     0,   ' ',     0,	/* scan 70-77 */
    0,   ' ',     0,     0,     0,   '|',     0,     0,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,    'F',   'A', /* extended */
    0,   'D',   'C',     0,   'B',     0,    '@',   'P'  /* extended */
    },
    { /* Control code */
 0xff,  0x1b,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan  0- 7 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0x1e,  0xff,  '\t',	/* scan  8- F */
 0x11,  0x17,  0x05,  0x12,  0x14,  0x19,  0x15,  0x09,	/* scan 10-17 */
 0x0f,  0x10,  0x00,  0x1b,  '\r',   CN,  0x01,   0x13,	/* scan 18-1F */
 0x04,  0x06,  0x07,  0x08,  0x0a,  0x0b,  0x0c,  0xff,	/* scan 20-27 */
 0xff,  0xff,   SH,   0x1d,  0x1a,  0x18,  0x03,  0x16,	/* scan 28-2F */
 0x02,  0x0e,  0x0d,  0xff,  0xff,  0xff,  SH,    0xff,	/* scan 30-37 */
 0xff,  0xff,   CP,   0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 38-3F */
 0xff,  0xff,  0xff,  0xff,  0xff,   NM,   ST,    0xff,	/* scan 40-47 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 48-4F */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 50-57 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 58-5F */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 60-67 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 68-6F */
 0xff,  0xff,  0xff,  0x1f,  0xff,  0xff,  0xff,  0xff,	/* scan 70-77 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0x1c,  0xff,  0xff,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,  0x0c,  0xff, /* extended */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff  /* extended */
    },
    { /* non numeric code */
    0,  0x1b,   '1',   '2',   '3',   '4',   '5',   '6',	/* scan  0- 7 */
  '7',   '8',   '9',   '0',   '-',   '^',  0x08,  '\t',	/* scan  8- F */
  'q',   'w',   'e',   'r',   't',   'y',   'u',   'i',	/* scan 10-17 */
  'o',   'p',   '@',   '[',  '\r',   CN,    'a',   's',	/* scan 18-1F */
  'd',   'f',   'g',   'h',   'j',   'k',   'l',   ';',	/* scan 20-27 */
  ':',     0,   SH,    ']',   'z',   'x',   'c',   'v',	/* scan 28-2F */
  'b',   'n',   'm',   ',',   '.',   '/',   SH,    '*',	/* scan 30-37 */
  ' ',   ' ',   CP,     0,     0,     0,     0,     0,	/* scan 38-3F */
    0,     0,     0,     0,     0,   NM,    ST,    'w',	/* scan 40-47 */
  'x',   'y',   'l',   't',   'u',   'v',   'm',   'q',	/* scan 48-4F */
  'r',   's',   'p',   'n',     0,     0,     0,     0,	/* scan 50-57 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 58-5F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 60-67 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 68-6F */
  ' ',     0,     0,  '\\',     0,     0,   ' ',     0,	/* scan 70-77 */
    0,   ' ',     0,     0,     0,  '\\',     0,     0,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,    'F',   'A', /* extended */
    0,   'D',   'C',     0,   'B',     0,    '@',   'P'  /* extended */
    }
    },
    { /* English Enhanced keyboard */
    { /* unshift code */
    0,  0x1b,   '1',   '2',   '3',   '4',   '5',   '6',	/* scan  0- 7 */
  '7',   '8',   '9',   '0',   '-',   '=',  0x08,  '\t',	/* scan  8- F */
  'q',   'w',   'e',   'r',   't',   'y',   'u',   'i',	/* scan 10-17 */
  'o',   'p',   '[',   ']',  '\r',   CN,    'a',   's',	/* scan 18-1F */
  'd',   'f',   'g',   'h',   'j',   'k',   'l',   ';',	/* scan 20-27 */
 '\'',   '`',   SH,   '\\',   'z',   'x',   'c',   'v',	/* scan 28-2F */
  'b',   'n',   'm',   ',',   '.',   '/',   SH,    '*',	/* scan 30-37 */
  ' ',   ' ',   CP,      0,     0,     0,     0,     0,	/* scan 38-3F */
    0,     0,     0,     0,     0,   NM,    ST,    '7',	/* scan 40-47 */
  '8',   '9',   '-',   '4',   '5',   '6',   '+',   '1',	/* scan 48-4F */
  '2',   '3',   '0',   '.',     0,     0,     0,     0,	/* scan 50-57 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 58-5F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 60-67 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 68-6F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 70-77 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,   'F',   'A', /* extended */
    0,   'D',   'C',     0,   'B',     0,    '@',  'P'  /* extended */
    },
    { /* shift code */
    0,  0x1b,   '!',   '@',   '#',   '$',   '%',   '^',	/* scan  0- 7 */
  '&',   '*',   '(',   ')',   '_',   '+',  0x08,  '\t',	/* scan  8- F */
  'Q',   'W',   'E',   'R',   'T',   'Y',   'U',   'I',	/* scan 10-17 */
  'O',   'P',   '{',   '}',  '\r',   CN,    'A',   'S',	/* scan 18-1F */
  'D',   'F',   'G',   'H',   'J',   'K',   'L',   ':',	/* scan 20-27 */
  '"',   '~',   SH,    '|',   'Z',   'X',   'C',   'V',	/* scan 28-2F */
  'B',   'N',   'M',   '<',   '>',   '?',   SH,    '*',	/* scan 30-37 */
  ' ',   ' ',   CP,      0,     0,     0,     0,     0,	/* scan 38-3F */
    0,     0,     0,     0,     0,   NM,    ST,    '7',	/* scan 40-47 */
  '8',   '9',   '-',   '4',   '5',   '6',   '+',   '1',	/* scan 48-4F */
  '2',   '3',   '0',   '.',     0,     0,     0,     0,	/* scan 50-57 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 58-5F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 60-67 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 68-6F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 70-77 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,   'F',   'A', /* extended */
    0,   'D',   'C',     0,   'B',     0,   '@',   'P'  /* extended */
    },
    { /* Control code */
 0xff,  0x1b,  0xff,  0x00,  0xff,  0xff,  0xff,  0xff,	/* scan  0- 7 */
 0x1e,  0xff,  0xff,  0xff,  0x1f,  0xff,  0xff,  '\t',	/* scan  8- F */
 0x11,  0x17,  0x05,  0x12,  0x14,  0x19,  0x15,  0x09,	/* scan 10-17 */
 0x0f,  0x10,  0x1b,  0x1d,  '\r',   CN,   0x01,  0x13,	/* scan 18-1F */
 0x04,  0x06,  0x07,  0x08,  0x0a,  0x0b,  0x0c,  0xff,	/* scan 20-27 */
 0xff,  0x1c,   SH,   0xff,  0x1a,  0x18,  0x03,  0x16,	/* scan 28-2F */
 0x02,  0x0e,  0x0d,  0xff,  0xff,  0xff,   SH,   0xff,	/* scan 30-37 */
 0xff,  0xff,   CP,   0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 38-3F */
 0xff,  0xff,  0xff,  0xff,  0xff,   NM,    ST,   0xff,	/* scan 40-47 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 48-4F */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 50-57 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 58-5F */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 60-67 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 68-6F */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 70-77 */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,  0xff,  0xff, /* extended */
 0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff,  0xff  /* extended */
    },
    { /* non numeric code */
    0,  0x1b,   '1',   '2',   '3',   '4',   '5',   '6',	/* scan  0- 7 */
  '7',   '8',   '9',   '0',   '-',   '=',  0x08,  '\t',	/* scan  8- F */
  'q',   'w',   'e',   'r',   't',   'y',   'u',   'i',	/* scan 10-17 */
  'o',   'p',   '[',   ']',  '\r',   CN,    'a',   's',	/* scan 18-1F */
  'd',   'f',   'g',   'h',   'j',   'k',   'l',   ';',	/* scan 20-27 */
 '\'',   '`',   SH,   '\\',   'z',   'x',   'c',   'v',	/* scan 28-2F */
  'b',   'n',   'm',   ',',   '.',   '/',   SH,    '*',	/* scan 30-37 */
  ' ',   ' ',   CP,      0,     0,     0,     0,     0,	/* scan 38-3F */
    0,     0,     0,     0,     0,   NM,    ST,    'w',	/* scan 40-47 */
  'x',   'y',   'l',   't',   'u',   'v',   'm',   'q',	/* scan 48-4F */
  'r',   's',   'p',   'n',     0,     0,     0,     0,	/* scan 50-57 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 58-5F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 60-67 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 68-6F */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 70-77 */
    0,     0,     0,     0,     0,     0,     0,     0,	/* scan 78-7F */
  '\r',   CN,   '/',   '*',   ' ',    ST,   'F',   'A', /* extended */
    0,   'D',   'C',     0,   'B',     0,    '@',  'P'  /* extended */
    }
    }
    };

/* default keyboard is English Enhanced key */
LOCAL int kbdMode = ENGLISH_KBD;

/* unshift state */
LOCAL int kbdState = 0;

LOCAL  int getCountOrder(UINT32 count)
{
	int order;

	order = ffsMsb(count) - 1;
	if (count & (count - 1))
		order++;
	return order;
}

/******************************************************************************
*
* vxbFtKeypadDrvRegister - register Phytium Keypad driver
*
* This routine registers the Phytium Keypad driver with the vxBus subsystem.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFtKeypadDrvRegister (void)
    {
    /* call the vxBus routine to register the GPIO driver */

    vxbDevRegister (&vxbFtKeypadDrvRegistration);
    }

/*******************************************************************************
*
* vxbFtKeypadInstInit - first level initialization routine of Keypad modules
*
* This is the function called to perform the first level initialization of
* the Phytium Keypad modules.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtKeypadInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    const struct hcfDevice * pHcf;

    /* Always use the unit number allocated to us in the hwconf file. */

    pHcf = (struct hcfDevice *)pDev->pBusSpecificDevInfo;
    vxbInstUnitSet (pDev, pHcf->devUnit);

    /*  func6  */
    regWrite32(0x32b301cc, 0x46);
    regWrite32(0x32b301d0, 0x46);
    regWrite32(0x32b301d4, 0x46);
    regWrite32(0x32b301d8, 0x46);
    regWrite32(0x32b301dc, 0x46);
    regWrite32(0x32b301e0, 0x46);
    regWrite32(0x32b301e4, 0x46);
    regWrite32(0x32b301e8, 0x46);
    regWrite32(0x32b301ec, 0x46);
    regWrite32(0x32b301f0, 0x46);
    regWrite32(0x32b301f4, 0x46);
    regWrite32(0x32b301f8, 0x46);
    regWrite32(0x32b301fc, 0x46);
    regWrite32(0x32b30200, 0x46);
    regWrite32(0x32b30204, 0x46);
    regWrite32(0x32b30208, 0x46);

    regWrite32(0x32b30210, 0x273);
    regWrite32(0x32b30214, 0x273);
    regWrite32(0x32b30218, 0x273);
    regWrite32(0x32b3021c, 0x273);
    regWrite32(0x32b30220, 0x273);
    regWrite32(0x32b30224, 0x273);
    regWrite32(0x32b30228, 0x273);
    regWrite32(0x32b3022c, 0x273);
    }

/*******************************************************************************
*
* vxbFtKeypadInstInit2 - second level initialization routine of Keypad modules
*
* This routine performs the second level initialization of the Keypad modules.
*
* This routine is called later during system initialization. OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtKeypadInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_KEYPAD_DRVCTRL * pDrvCtrl;
    HCF_DEVICE * pHcf;
    int st, i, row, col;

    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)hwMemAlloc(sizeof (FT_KEYPAD_DRVCTRL)); 
    
    if (pDrvCtrl == NULL)
        {
        KPD_DBG(KPD_DBG_ERR, "vxbFtKeypadInstInit2: pDrvCtrl alloc failed\r\n.",1,2,3,4,5,6);
        return;
        }
        
    pDev->pDrvCtrl = pDrvCtrl;
    pDrvCtrl->pInst = pDev;
    pDrvCtrl->bar = pDev->pRegBase[0];
	
    /* map register base */
	
    pDev->regBaseFlags[0] = VXB_REG_IO;
    if (vxbRegMap(pDev, 0, &pDrvCtrl->handle) == ERROR)
        {
        KPD_DBG (KPD_DBG_ERR, "vxbFtKeypadInstInit2: vxbRegMap ERROR\r\n", 1, 2, 3, 4, 5, 6);
        return;
        }
    
    pHcf = (struct hcfDevice *) hcfDeviceGet (pDev);

    if (pHcf == NULL)
        {
        KPD_DBG (KPD_DBG_ERR, "vxbFtKeypadInstInit2: no pHcf for KPD.\r\n", 1, 2, 3, 4, 5, 6);
        return;
        }
    
    if (devResourceGet (pHcf, "rows", HCF_RES_INT, (void *)&pDrvCtrl->rows) != OK)
        {
           pDrvCtrl->rows = MAX_MATRIX_KEY_ROWS;
        }
    if (devResourceGet (pHcf, "cols", HCF_RES_INT, (void *)&pDrvCtrl->cols) != OK)
        {
           pDrvCtrl->cols = MAX_MATRIX_KEY_COLS;
        }
    
    pDrvCtrl->stableCount = 0;
    pDrvCtrl->wdCheckMatrix = wdCreate();
    if (NULL == pDrvCtrl->wdCheckMatrix)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to create wd timer!\r\n", 1,2,3,4,5,6);
        return ;
        }

    st = matrixKeypadBuildKeymap(pDrvCtrl->keycodes,
                       pDrvCtrl->rows,
                       pDrvCtrl->cols,
                       (UINT32 *)keymap, NELEMENTS(keymap));
    if (OK != st)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to create keycodes table!\r\n", 1,2,3,4,5,6);
        return ;
        }

    pDrvCtrl->rowShift = getCountOrder(pDrvCtrl->cols);
	
    /* Search for rows and cols enabled */
    for (row = 0; row < pDrvCtrl->rows; row++) {
        for (col = 0; col <  pDrvCtrl->cols; col++) {
            i = MATRIX_SCAN_CODE(row, col, pDrvCtrl->rowShift);

			/*uartf("[%d,%d], i=%d, code=%d\r\n", row, col, i, pDrvCtrl->keycodes[i]);*/
			
            if (pDrvCtrl->keycodes[i] != KEY_RESERVED) {
                pDrvCtrl->rowsEnMask |= 1 << row;
                pDrvCtrl->colsEnMask |= 1 << col;
            }
        }
    }

    vxbFtKeypadInhibit(pDev);

    return;
    }

/*******************************************************************************
*
* vxbFtKeypadInstConnect - third level initialization routine of Keypad modules
*
* This is the function called to perform the third level initialization of
* the Keypad modules.
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtKeypadInstConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
        FT_KEYPAD_DRVCTRL * pDrvCtrl;
        char iosDevName [32];
        STATUS st;

        pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
        bzero(iosDevName, sizeof(iosDevName));
        ftKpdDrv();
        if (OK != st)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to install kpd drv!\r\n", 1,2,3,4,5,6);
        return ;
        }

        
        (void) snprintf (iosDevName, 32, "/keypad%d", pDev->unitNumber);
        
        st = ftKpdDevCreate(iosDevName,pDev, 32, 32);
        if (OK != st)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to create kpd device!\r\n", 1,2,3,4,5,6);
        return ;
        }
        

    }

LOCAL BOOL matrixKeypadMapKey(UINT16 *keycodes,
				  UINT32 rows, UINT32 cols,
				  UINT32 rowShift, UINT32 key)
{

	UINT32 row = KEY_ROW(key);
	UINT32 col = KEY_COL(key);
	UINT16 code = KEY_VAL(key);

	if (row >= rows || col >= cols) {
		KPD_DBG(KPD_DBG_ALL,
			"%s: invalid keymap entry 0x%x (row: %d, col: %d, rows: %d, cols: %d)\n",
			__func__, key, row, col, rows, cols);
		return FALSE;
	}

	keycodes[MATRIX_SCAN_CODE(row, col, rowShift)] = code;

	return TRUE;
}

LOCAL STATUS matrixKeypadBuildKeymap(UINT16 *keycodes,
			       UINT32 rows, UINT32 cols,
			       UINT32 *keymap,
			       UINT16 keymapSize)
{
	UINT32 rowShift = getCountOrder(cols);
	int i;

	for (i = 0; i < keymapSize; i++)
    {
		UINT32 key = keymap[i];

		if (!matrixKeypadMapKey(keycodes, rows, cols,
					   rowShift, key))
			return ERROR;
	}

	return OK;
}



/* Scan the matrix and return the new state in *matrixVolatileState. */
LOCAL void matrixKeypadScanMatrix(VXB_DEVICE_ID   pDev,
                  UINT16 *matrixVolatileState)
{
    FT_KEYPAD_DRVCTRL * pDrvCtrl;
    int col;
    UINT32 regVal;
    
    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
    
    for (col = 0; col < pDrvCtrl->cols; col++)
        {
        if ((pDrvCtrl->colsEnMask & (1 << col)) == 0)
            continue;
        /*
         * Discharge keypad capacitance:
         * 2. write 0s on KDDR[KCD], configure columns as input.
         */
        regVal = CSR_READ_4(pDev, KDDR);
        regVal = 0x00000000;
        CSR_WRITE_4(pDev, KDDR, regVal);

        /*
         * 3. Write a single column to 0, others to 1.
         * 4. Sample row inputs and save data.
         * 5. Repeat steps 3 - 4 for remaining columns.
         */
        regVal = 0;
        regVal |= (1 << (16 + col));
        CSR_WRITE_4(pDev, KDDR, regVal);
        regVal = CSR_READ_4(pDev, KPDR);
        regVal = 0x00000000;
        CSR_WRITE_4(pDev, KPDR, regVal);

        /*
         * Delay added to avoid propagating the 0 from column to row
         * when scanning.
         */

        sysUsDelay(5);


        /*
         * 1s in matrixVolatileState[col] means key pressures
         * throw data from non enabled rows.
         */
        regVal = CSR_READ_4(pDev, KPDR);
        matrixVolatileState[col] = (~regVal) & pDrvCtrl->rowsEnMask;
    }

    /*
     * Return in standby mode:
     * 6. write 0s to columns
     */
    /* Configure columns as output, output 0 */
	regVal = 0;
	regVal |= (pDrvCtrl->colsEnMask & 0xffff) << 16;	
    CSR_WRITE_4(pDev, KDDR, regVal);
    CSR_WRITE_4(pDev, KPDR, 0x00000000);
}

/*
 * Compare the new matrix state (volatile) with the stable one stored in
 * ->matrixStableState and fire events if changes are detected.
 */
LOCAL void matrixKeypadFireEvents(VXB_DEVICE_ID   pDev,
                   UINT16 *matrixVolatileState)
{
    FT_KEYPAD_DRVCTRL * pDrvCtrl;
    int row, col;
    UINT16 key;
    UINT16 keyStat; /* key status: pressed/released.  */

    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
    for (col = 0; col < pDrvCtrl->cols; col++)
        {
        UINT16 bitsChanged;
        int code;

        if ((pDrvCtrl->colsEnMask & (1 << col)) == 0)
            continue; /* Column is not enabled */

        bitsChanged = pDrvCtrl->matrixStableState[col] ^
                        matrixVolatileState[col];

        if (bitsChanged == 0)
            continue; /* Column does not contain changes */

        for (row = 0; row < pDrvCtrl->rows; row++)
            {
            if ((pDrvCtrl->rowsEnMask & (1 << row)) == 0)
                continue; /* Row is not enabled */
            
            if ((bitsChanged & (1 << row)) == 0)
                continue; /* Row does not contain changes */

            code = MATRIX_SCAN_CODE(row, col, pDrvCtrl->rowShift);
            key = pDrvCtrl->keycodes[code];
            keyStat = matrixVolatileState[col] & (1 << row);
            
            /* Here the key can be send to up layer... */
            if (pDrvCtrl->inputReportKey != NULL)
                {
                pDrvCtrl->inputReportKey(pDev, key, keyStat);
                }
            
            KPD_DBG(KPD_DBG_ALL, "Event code: %d, key: 0x%x,  keyStat: %d \r\n",
                code, key, keyStat, 4,5,6);
			uartf("Event code: %d, key: 0x%x,  keyStat: %d \r\n",
                code, key, keyStat, 4,5,6);
            }
        }
    
}

/*
 * matrixKeypadCheckForEvents is the timer handler.
 */
LOCAL void matrixKeypadCheckForEvents(VXB_DEVICE_ID   pDev)
{
    FT_KEYPAD_DRVCTRL * pDrvCtrl;
    UINT16 matrixVolatileState[MAX_MATRIX_KEY_COLS];
    UINT32 regVal;
    BOOL stateChanged, isZeroMatrix;
    int i;

    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
    memset(matrixVolatileState, 0, sizeof(matrixVolatileState));

    matrixKeypadScanMatrix(pDev, matrixVolatileState);

    stateChanged = FALSE;
    for (i = 0; i < pDrvCtrl->cols; i++)
        {
        if ((pDrvCtrl->colsEnMask & (1 << i)) == 0)
            continue;

        if (pDrvCtrl->matrixUnstableState[i] ^ matrixVolatileState[i])
            {
            stateChanged = TRUE;
            break;
            }
        }

    /*
     * If the matrix state is changed from the previous scan
     *   (Re)Begin the debouncing process, saving the new state in
     *    ->matrixUnstableState.
     * else
     *   Increase the count of number of scans with a stable state.
     */
    if (stateChanged)
        {
        memcpy(pDrvCtrl->matrixUnstableState, matrixVolatileState,
            sizeof(matrixVolatileState));
        pDrvCtrl->stableCount = 0;
        } 
    else
        {
        pDrvCtrl->stableCount++;
        }

    /*
     * If the matrix is not as stable as we want reschedule scan
     * in the near future.
     */
    if (pDrvCtrl->stableCount < PHYTIUM_KEYPAD_SCANS_FOR_STABILITY)
        {
        
        wdStart(pDrvCtrl->wdCheckMatrix, 1, 
            (FUNCPTR)matrixKeypadCheckForEvents, (_Vx_usr_arg_t)pDev);
        return;
        }

    /*
     * If the matrix state is stable, fire the events and save the new
     * stable state. Note, if the matrix is kept stable for longer
     * (->stableCount > PHYTIUM_KEYPAD_SCANS_FOR_STABILITY) all
     * events have already been generated.
     */
    if (pDrvCtrl->stableCount == PHYTIUM_KEYPAD_SCANS_FOR_STABILITY)
        {
        matrixKeypadFireEvents(pDev, matrixVolatileState);

        memcpy(pDrvCtrl->matrixStableState, matrixVolatileState,
            sizeof(matrixVolatileState));
        }


    isZeroMatrix = TRUE;
    for (i = 0; i < pDrvCtrl->cols; i++)
        {
        if (matrixVolatileState[i] != 0)
            {
            isZeroMatrix = FALSE;
            break;
            }
        }


    if (isZeroMatrix)
        {
        /*
         * All keys have been released. Enable only the KDI
         * interrupt for future key presses (clear the KDI
         * status bit and its sync chain before that).
         */
        regVal = CSR_READ_4(pDev, KPSR);
        regVal |= KBD_STAT_KPKD | KBD_STAT_KDSC;
        CSR_WRITE_4(pDev, KPSR, regVal);

        regVal = CSR_READ_4(pDev, KPSR);
        regVal |= KBD_STAT_KDIE;
        regVal &= ~KBD_STAT_KRIE;
        CSR_WRITE_4(pDev, KPSR, regVal);
        }
    else
        {
        /*
         * Some keys are still pressed. Schedule a rescan in
         * attempt to detect multiple key presses and enable
         * the KRI interrupt to react quickly to key release
         * event.
         */

    wdStart(pDrvCtrl->wdCheckMatrix, 3, (FUNCPTR)matrixKeypadCheckForEvents, (_Vx_usr_arg_t)pDev);

        regVal = CSR_READ_4(pDev, KPSR);
        regVal |= KBD_STAT_KPKR | KBD_STAT_KRSS;
        CSR_WRITE_4(pDev, KPSR, regVal);

        regVal = CSR_READ_4(pDev, KPSR);
        regVal |= KBD_STAT_KRIE;
        regVal &= ~KBD_STAT_KDIE;
        CSR_WRITE_4(pDev, KPSR, regVal);
       }
}

LOCAL void vxbFtKeypadISR(VXB_DEVICE_ID   pDev)
{
    FT_KEYPAD_DRVCTRL *pDrvCtrl;
    UINT32 regVal;
	
    uartf("##key interrupt... \r\n");
    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
    regVal = CSR_READ_4(pDev, KPSR);
    /* Disable both interrupt types */
    regVal &= ~(KBD_STAT_KRIE | KBD_STAT_KDIE);
    /* Clear interrupts status bits */
    regVal |= KBD_STAT_KPKR | KBD_STAT_KPKD;
    CSR_WRITE_4(pDev, KPSR, regVal);

    if (pDrvCtrl->enabled)
        {
        /* The matrix is supposed to be changed */
        pDrvCtrl->stableCount = 0;

        /* Schedule the scanning procedure near in the future */

        wdStart(pDrvCtrl->wdCheckMatrix, 1,
            (FUNCPTR)matrixKeypadCheckForEvents, (_Vx_usr_arg_t)pDev);

        }

    return ;
}

/* phytium_keypad_config */
LOCAL void vxbFtKeypadConfig(VXB_DEVICE_ID   pDev)
{
    FT_KEYPAD_DRVCTRL * pDrvCtrl;
    UINT32 regVal;

    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
    
    /*
     * Include enabled rows in interrupt generation (KPCR[15:0])
     * Configure keypad columns as open-drain (KPCR[31:16])
     */
    regVal = CSR_READ_4(pDev, KPCR);
    regVal |= pDrvCtrl->rowsEnMask & 0xffff;           /* rows */
    regVal |= (pDrvCtrl->colsEnMask & 0xffff) << 16;   /* cols */
    CSR_WRITE_4(pDev, KPCR, regVal);


    /* Configure columns as output, output 0 */
	regVal = 0;
	regVal |= (pDrvCtrl->colsEnMask & 0xffff) << 16;
    CSR_WRITE_4(pDev, KDDR, regVal);
    CSR_WRITE_4(pDev, KPDR, 0x00000000);

    /*
     * Clear Key Depress and Key Release status bit.
     * Clear both synchronizer chain.
     */
    regVal = CSR_READ_4(pDev, KPSR);
    regVal |= KBD_STAT_KPKR | KBD_STAT_KPKD |
           KBD_STAT_KDSC | KBD_STAT_KRSS;
    CSR_WRITE_4(pDev, KPSR, regVal);

    /* Enable KDI and disable KRI (avoid false release events). */
    regVal |= KBD_STAT_KDIE;
    regVal &= ~KBD_STAT_KRIE;
    CSR_WRITE_4(pDev, KPSR, regVal);
}

LOCAL void vxbFtKeypadInhibit(VXB_DEVICE_ID   pDev)
{
    UINT16 regVal;

    /* Inhibit KDI and KRI interrupts. */
    regVal = CSR_READ_4(pDev, KPSR);
    regVal &= ~(KBD_STAT_KRIE | KBD_STAT_KDIE);
    regVal |= KBD_STAT_KPKR | KBD_STAT_KPKD;
    CSR_WRITE_4(pDev, KPSR, regVal);
}

/* phytium_keypad_close */
LOCAL void vxbFtKeypadDisable (VXB_DEVICE_ID   pDev)
{
    FT_KEYPAD_DRVCTRL * pDrvCtrl;

    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;

    /* Mark keypad as being inactive */
    pDrvCtrl->enabled = FALSE;
    
    vxbIntDisable(pDev, 0, (VOIDFUNCPTR)vxbFtKeypadISR, pDev);
    vxbIntDisconnect(pDev, 0, (VOIDFUNCPTR)vxbFtKeypadISR, pDev);
    
    wdCancel(pDrvCtrl->wdCheckMatrix);
    wdTerminate(pDrvCtrl->wdCheckMatrix);

    vxbFtKeypadInhibit(pDev);
}

/* phytium_keypad_open */
LOCAL STATUS vxbFtKeypadEnable(VXB_DEVICE_ID   pDev)
{
    FT_KEYPAD_DRVCTRL * pDrvCtrl;

    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
    /* We became active from now */
    pDrvCtrl->enabled = TRUE;

    vxbFtKeypadConfig(pDev);

    /* Sanity control, not all the rows must be actived now. */
    if ((CSR_READ_4(pDev, KPDR) & pDrvCtrl->rowsEnMask) == 0)
        {
        KPD_DBG(KPD_DBG_ALL, "too many keys pressed, control pins initialisation\r\n",1,2,3,4,5,6);
        goto open_err;
        }

    return OK;

open_err:
    vxbFtKeypadDisable(pDev);
    return ERROR;
}


LOCAL UINT32  ftKpdOpen
    (   
    DEV_HDR * hdr,      /* pointer to the DEV_HDR struct */
    char    * name,     /* device name */
    int     flag,       /* open flag */
    int     mode        /* open mode */
    )
    {
    FT_KEYPAD_DRVCTRL * pDrvCtrl =  (FT_KEYPAD_DRVCTRL *) hdr;
    VXB_DEVICE_ID   pDev;
    STATUS st;

    pDev = pDrvCtrl->pInst;

    pDrvCtrl->wdCheckMatrix = wdCreate();
    if (NULL == pDrvCtrl->wdCheckMatrix)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to create wd timer!\r\n", 1,2,3,4,5,6);
        return ERROR;
        }

    st = vxbIntConnect(pDev, 0, (VOIDFUNCPTR)vxbFtKeypadISR, pDev);
    if (OK != st)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to connect kpd int!\r\n", 1,2,3,4,5,6);
        return ERROR;
        }
    
    st = vxbFtKeypadEnable(pDev);
    if (OK != st)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to open kpd!\r\n", 1,2,3,4,5,6);
        return ERROR;
        }
    
    vxbIntEnable(pDev, 0, (VOIDFUNCPTR)vxbFtKeypadISR, pDev);
    if (OK != st)
        {
        KPD_DBG(KPD_DBG_ALL, "Failed to enable kpd int!\r\n", 1,2,3,4,5,6);
        return ERROR;
        }

    return (UINT32)hdr;
    }

LOCAL int ftKpdClose(DEV_HDR * pIoDev)
    {
    FT_KEYPAD_DRVCTRL * pDrvCtrl =  (FT_KEYPAD_DRVCTRL *) pIoDev;
    VXB_DEVICE_ID   pDev;

    pDev = pDrvCtrl->pInst;
    
#if 0
    /* delete the semaphore to abort any pending read */

    (void) semDelete (pFtAdcCtrl->sampleSemId);
    (void) semDelete (pFtAdcCtrl->rdTskSemId);

    /* give up access right */

    (void) semGive (pFtAdcCtrl->accessSemId);
#endif
    vxbFtKeypadDisable(pDev);

    return OK;
    }

LOCAL STATUS ftKpdIoctl
(
 DEV_HDR * pIoDev,      /* pointer to DevIO file descriptor */
 int            command,     /* command function code */
 int            arg          /* arbitrary argument */
 )
{

    return OK;
}

LOCAL STATUS ftKpdDrv(void)
    {
    /* check if driver already installed */

    if (ftKpdDrvNum > 0)
        return (OK);


    ftKpdDrvNum = iosDrvInstall
            (
            (DRV_CREATE_PTR)  NULL,
            (DRV_REMOVE_PTR)  NULL,
            (DRV_OPEN_PTR)    ftKpdOpen,
            (DRV_CLOSE_PTR)   ftKpdClose,
            (DRV_READ_PTR)    tyRead,
            (DRV_WRITE_PTR)   NULL,
            (DRV_IOCTL_PTR)   ftKpdIoctl
            );
    if (ERROR == ftKpdDrvNum)
        {
        KPD_DBG(KPD_DBG_ALL,"Install IO dirver failed. \r\n",1,2,3,4,5,6);
        return ERROR;
        }

    return (ftKpdDrvNum == ERROR ? ERROR : OK);
    }


LOCAL STATUS ftKpdDevCreate
    (
    char *        name,           /* name to use for this device */
    VXB_DEVICE_ID pDev,           /* pointer to vxbus DEV structure */
    int           rdBufSize,      /* read buffer size, in bytes */
    int           wrtBufSize      /* write buffer size, in bytes */
    )
    {

    FT_KEYPAD_DRVCTRL * pDrvCtrl;
    TY_DEV_ID  pTyDev;
    
    pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;

    if (ftKpdDrvNum <= 0)
        {
        errnoSet (S_ioLib_NO_DRIVER);
        return (ERROR);
        }

    pTyDev = &pDrvCtrl->tyDev;

    /* initialize the ty descriptor */

    if (tyDevInit (pTyDev, (size_t) rdBufSize,
                   (size_t) wrtBufSize,
                   (TY_DEVSTART_PTR) NULL) != OK)
        {
        return (ERROR);
        }

    pDrvCtrl->inputReportKey = vxbFtKeypadReportCallback;

    /* add the device to the I/O system */

    if (iosDevAdd (&pTyDev->devHdr, name, ftKpdDrvNum) == ERROR)
	{
	return (ERROR);
	}

    return (OK);
    }

void vxbFtKeypadReportCallback(VXB_DEVICE_ID pDev, UINT16 key, UINT16 keyStat)
    {
        FT_KEYPAD_DRVCTRL * pDrvCtrl;
        TY_DEV_ID  pTyDev;
        char inChar;

        pDrvCtrl = (FT_KEYPAD_DRVCTRL *)pDev->pDrvCtrl;
        pTyDev = &pDrvCtrl->tyDev;

        /* key --> char: This is only an example for standard keyboard.
         * User Must Modify this map for actual used Hardware Keyboard!
         */

        key = key & 0xFF;
        inChar = keyMapAscii [kbdMode][kbdState][key];

        tyIRd(pTyDev, inChar);
    }

#define FT_KEYPAD_TEST
#ifdef FT_KEYPAD_TEST
void testFtKpd(void)
    {
        int fd, rcv, i;
        char buff[32];

        bzero(buff, sizeof(buff));
        fd = open("/keypad0", O_RDWR, 0644);
        if (fd < 0)
            {
            printf("Failed to open /keypad0 device!\r\n");
            printErrno(0);
            return;
            }

        rcv = read(fd, buff, 10);

        printf("Got %d bytes from keypad:\r\n", rcv);
        for (i=0; i<rcv; i++)
            {
            printf("%02x ", buff[i]);
            }
        printf(" \r\n");
        close(fd);
    }
#endif

