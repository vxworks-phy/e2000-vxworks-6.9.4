/* vxbFtGpio.h - Driver for GPIO controller*/

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbFtGpioh
#define __INCvxbFtGpioh

#ifdef __cplusplus
extern "C" {
#endif
                
#define GPIO_READ_4(pCtrl, addr)                                \
    vxbRead32 ((pCtrl->gpioHandle),                             \
               (UINT32 *)((char *)(pCtrl->pInst->pRegBase[0]) + (addr)))

#define GPIO_WRITE_4(pCtrl, addr, data)                         \
    vxbWrite32 ((pCtrl->gpioHandle),                            \
                (UINT32 *)((char *)(pCtrl->pInst->pRegBase[0]) + (addr)), data)

/* gpio mode */
#define GPIO_MODE_NOT_USED   0
#define GPIO_MODE_IN         1
#define GPIO_MODE_OUT        2
#define GPIO_MODE_INT        3  

#define FT_GPIO_PORT_WIDTH   16

/* Register offset */
#define GPIO_SW_DR_A         0x00   /* output register for port A*/
#define GPIO_SW_DDR_A        0x04   /* director register for port A. 0:input; 1:output */
#define GPIO_EXT_A           0x08   /* input register for port A*/

#define GPIO_INTEN_A         0x18   /* only Port A support interrupt */
#define GPIO_INTMASK_A       0x1c
#define GPIO_INTTYPE_LEVEL_A 0x20
#define GPIO_INT_POLARITY_A  0x24
#define GPIO_INTSTATUS_A     0x28
#define GPIO_RAW_INTSTATUS_A 0x2c
#define GPIO_LS_SYNC_A       0x30
#define GPIO_DEBOUNCE        0x34
#define GPIO_PORTA_EOI_A     0x38

/* gpio direction */
#define GPIO_DIR_IN   0
#define GPIO_DIR_OUT  1

/* int enable/disable  */
#define GPIO_INT_DIS      0
#define GPIO_INT_ENA      1

/* int mask  */
#define GPIO_INT_NOT_MASK 0
#define GPIO_INT_MASK     1

/* int type  */
#define GPIO_INT_TYPE_LEVEL 0
#define GPIO_INT_TYPE_EDGE  1

/* int polarity  */
#define GPIO_INT_POL_LOW_DOWN 0
#define GPIO_INT_POL_HIGH_UP  1

/* Private context structure */

typedef struct ftIsrEntry 
    {
    VOIDFUNCPTR     pIsr;       /* ISR */
    void *          pArg;        /* parameter */
    } FT_ISR_ENTRY;

typedef struct ftGpioDrvCtrl
    {
    VXB_DEVICE_ID           pInst;       /* pointer to the controller instance */
    void *                  gpioHandle;      /* handle of GPIO vxbRead/vxbWrite */
    FT_ISR_ENTRY            isrTable[FT_GPIO_PORT_WIDTH];    /* ISR handler table */
    UINT8                   pinMode[FT_GPIO_PORT_WIDTH];     /* GPIO_MODE_NOT_USED   0
                                                                GPIO_MODE_IN         1
                                                                GPIO_MODE_OUT        2
                                                                GPIO_MODE_INT        3   
                                                             */
    UINT32                  triggerMode[FT_GPIO_PORT_WIDTH]; /*Trigger mode*/
    STATUS                  intEnabled;      /*GPIO3~5: whether have pin been used as interrupt mode*/
    /* pin mode set. */
    STATUS (*gpioModeSet) 
        (
        VXB_DEVICE_ID pDev,
        UINT32        pin,
        UINT32        mode          
        );
    /* get input value of a pin */
    UINT32 (*gpioInput)
        (
        VXB_DEVICE_ID pDev, 
        UINT32 pin
        );
    /* output value to a pin */
    STATUS (*gpioOutput)
        (
        VXB_DEVICE_ID pDev, 
        UINT32 pin,
        UINT32 value
        );
    /* set ISR for a pin */
    STATUS (*gpioISRSet)
        (
        VXB_DEVICE_ID pDev, 
        UINT32          pin,
        VOIDFUNCPTR     pIsr,       /* ISR */
        void *          pArg        /* parameter */
        );

    /* GPIO INT Enable */
    STATUS (*gpioIntEnable)
        (
        VXB_DEVICE_ID pDev, 
        UINT32          pin
        );

    /* GPIO INT Disable */
    STATUS (*gpioIntDisable)
        (
        VXB_DEVICE_ID pDev, 
        UINT32          pin
        );
        
    } FT_GPIO_DRVCTRL;          

#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbFtGpioh */
