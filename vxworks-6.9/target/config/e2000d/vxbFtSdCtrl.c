/* vxbFtSdCtrl.c - SD card driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <vxWorks.h>
#include <stdio.h>
#include <String.h>
#include <semLib.h>
#include <sysLib.h>
#include <taskLib.h>
#include <vxBusLib.h>
#include <cacheLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/vxbus/hwConf.h>
#include <drv/pci/pciConfigLib.h>
#include <drv/pci/pciIntLib.h>
#include <hwif/vxbus/vxbSdLib.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include "vxbFtSdCtrl.h"
#include "defs.h"
#undef SDHC_DBG_ON
#ifdef  SDHC_DBG_ON

#ifdef  LOCAL
#undef  LOCAL
#define LOCAL
#endif

#define SDHC_DBG_IRQ            0x00000001
#define SDHC_DBG_RW             0x00000002
#define SDHC_DBG_XBD            0x00000004
#define SDHC_DBG_ERR            0x00000008
#define SDHC_DBG_ALL            0xffffffff
#define SDHC_DBG_OFF            0x00000000

UINT32 ftSdhcDbgMask = SDHC_DBG_ALL;

IMPORT FUNCPTR _func_logMsg;

#define SDHC_DBG(mask, string, a, b, c, d, e, f)        \
    if ((ftSdhcDbgMask & mask) || (mask == SDHC_DBG_ALL))  \
        if (_func_logMsg != NULL) \
           (* _func_logMsg)(string, a, b, c, d, e, f)
#else
#define SDHC_DBG(mask, string, a, b, c, d, e, f)
#endif  /* SDHC_DBG_ON */

/*irq*/
LOCAL void ftSdhcIrq(FTSDHC_DEV_CTRL * pDrvCtrl);
/* locals */
LOCAL void ftSdhcResetHw(VXB_DEVICE_ID);
LOCAL BOOL ftSdhcCtrlCardWpCheck (VXB_DEVICE_ID);
LOCAL BOOL ftSdhcCtrlCardInsertSts(VXB_DEVICE_ID);
LOCAL  UINT32 ftSdhcPrepareCmdRaw(SD_CMD * pSdCmd);
LOCAL  UINT32 ftSdhcCmdFindResp(SD_CMD * pSdCmd);
LOCAL void ftSdhcInstInit(VXB_DEVICE_ID pInst);
LOCAL void ftSdhcInstInit2 (VXB_DEVICE_ID pInst);
LOCAL void ftSdhcInstConnect (VXB_DEVICE_ID pInst);
LOCAL void ftSdhcClkFreqSetup(VXB_DEVICE_ID pDev, UINT32 clk);
LOCAL STATUS ftSdhcInit(VXB_DEVICE_ID pDev);
LOCAL STATUS ftSdhcCmdIssue (VXB_DEVICE_ID pDev, SD_CMD * pSdCmd);
LOCAL void ftSdhcPrivateSendCmd(VXB_DEVICE_ID pDev, UINT32 cmd,UINT32 arg);
LOCAL STATUS ftSdhcCtrlInstConnect(SD_HOST_CTRL * pSdHostCtrl);
LOCAL STATUS ftSdhcSpecInfoGet (VXB_DEVICE_ID pDev, void ** pHostSpec,VXB_SD_CMDISSUE_FUNC * pCmdIssue);
LOCAL void ftSdhcCtrlMonitor (VXB_DEVICE_ID pDev);
LOCAL void ftSdhcVddSetup(VXB_DEVICE_ID pDev, UINT32 vdd);
LOCAL int ftSdhcStartData(VXB_DEVICE_ID pDev,SD_CMD * pSdCmd);
LOCAL int ftSdhcStartCmd(VXB_DEVICE_ID pDev,SD_CMD * pSdCmd);
LOCAL void ftSdhcBusWidthSetup(VXB_DEVICE_ID pDev,UINT32 width);
LOCAL void ftSdhcCtrlAnnounceDevices(VXB_DEVICE_ID pDev);

LOCAL DRIVER_INITIALIZATION ftSdhcFuncs =
    {
    ftSdhcInstInit,        /* devInstanceInit */
    ftSdhcInstInit2,       /* devInstanceInit2 */
    ftSdhcInstConnect      /* devConnect */
    };

LOCAL device_method_t vxbftSdhcCtrl_methods[] =
    {
    DEVMETHOD (vxbSdSpecInfoGet, ftSdhcSpecInfoGet),
    DEVMETHOD_END
    };
  
LOCAL DRIVER_REGISTRATION ftSdhcPlbRegistration =
    {
    NULL,
    VXB_DEVID_DEVICE,                                   /* devID */
    VXB_BUSID_PLB,                                      /* busID = PLB */
    VXB_VER_5_0_0,                                      /* vxbVersion */
    FT_SDHC_NAME,                                       /* drvName */
    &ftSdhcFuncs,                                       /* pDrvBusFuncs */
    vxbftSdhcCtrl_methods,                              /* pMethods */
    NULL,                                               /* devProbe */
    NULL                                                /* pParamDefaults */
    };

LOCAL const UINT32 cmd_ints_mask = SDCI_INT_MASK_RE | SDCI_INT_MASK_CMD | SDCI_INT_MASK_RCRC |
                                   SDCI_INT_MASK_RTO | SDCI_INT_MASK_HTO | SDCI_INT_MASK_HLE;

LOCAL const UINT32 data_ints_mask = SDCI_INT_MASK_DTO | SDCI_INT_MASK_DCRC | SDCI_INT_MASK_DRTO | SDCI_INT_MASK_SBE_BCI;

LOCAL const UINT32 cmd_err_ints_mask = SDCI_INT_MASK_RTO | SDCI_INT_MASK_RCRC | SDCI_INT_MASK_RE | SDCI_INT_MASK_HLE;

LOCAL const UINT32 data_err_ints_mask = SDCI_INT_MASK_DCRC | SDCI_INT_MASK_DRTO | SDCI_INT_MASK_SBE_BCI;

LOCAL const UINT32 dmac_ints_mask = SDCI_DMAC_INT_ENA_FBE | SDCI_DMAC_INT_ENA_DU |
                                    SDCI_DMAC_INT_ENA_NIS | SDCI_DMAC_INT_ENA_AIS;
LOCAL const UINT32 dmac_err_ints_mask = SDCI_DMAC_INT_ENA_FBE | SDCI_DMAC_INT_ENA_DU |
                                        SDCI_DMAC_INT_ENA_AIS;  
/*******************************************************************************
*
* ftSdhcRegister - register Phytium SDHC driver
*
* This routine registers the Phytium SDHC driver with the vxbus subsystem.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void ftSdhcRegister (void)
    {
    (void)vxbDevRegister (&ftSdhcPlbRegistration);
    }

/*******************************************************************************
*
* ftSdhcInstInit - first level initialization routine of Phytium SDHC device
*
* This routine performs the first level initialization of the Phytium SDHC device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void ftSdhcInstInit
    (
    VXB_DEVICE_ID pInst
    )
    {
    }
 
/*******************************************************************************
*
* ftSdhcInstInit2 - second level initialization routine of Phytium SDHC device
*
* This routine performs the second level initialization of the Phytium SDHC device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void ftSdhcInstInit2
    (
    VXB_DEVICE_ID pInst
    )
    {
    FTSDHC_DEV_CTRL * pDrvCtrl;
    
    FUNCPTR clkFunc = NULL;

    const struct   hcfDevice * pHcf;

    pDrvCtrl = (FTSDHC_DEV_CTRL *)malloc (sizeof(FTSDHC_DEV_CTRL));
    if (pDrvCtrl == NULL)
        {
        return;
        }
    bzero ((char *)pDrvCtrl, sizeof(FTSDHC_DEV_CTRL));
  
    
    pDrvCtrl->sdHostCtrl.pDev = pInst;
    pDrvCtrl->sdHostCtrl.attached = FALSE;
    pDrvCtrl->sdHostCtrl.isInsert = FALSE;
    pDrvCtrl->pDev = pInst; 
    pInst->pDrvCtrl = pDrvCtrl;
    pDrvCtrl->regBase = pInst->pRegBase[0];
    pInst->regBaseFlags[0] = VXB_REG_IO;
    vxbRegMap (pInst, 0, &pDrvCtrl->regHandle);
    pDrvCtrl->sdHostCtrl.sdHostDmaParentTag = vxbDmaBufTagParentGet (pInst, 0);   
    pDrvCtrl->sdHostCtrl.sdHostDmaTag = vxbDmaBufTagCreate
                                              (pInst,
                                              pDrvCtrl->sdHostCtrl.sdHostDmaParentTag, /* parent */
                                              _CACHE_ALIGN_SIZE,                       /* alignment */
                                              0,                                       /* boundary */
                                              VXB_SPACE_MAXADDR_32BIT,                 /* lowaddr */
                                              VXB_SPACE_MAXADDR,                       /* highaddr */
                                              NULL,                                    /* filter */
                                              NULL,                                    /* filterarg */
                                              SDHC_MAX_RW_SECTORS * SDMMC_BLOCK_SIZE,  /* max size */
                                              SDHC_MAX_RW_SECTORS,                     /* nSegments */
                                              SDMMC_BLOCK_SIZE,                        /* max seg size */                   
                                              VXB_DMABUF_ALLOCNOW | VXB_DMABUF_NOCACHE,/* flags */
                                              NULL,                                    /* lockfunc */
                                              NULL,                                    /* lockarg */
                                              NULL);
    if (pDrvCtrl->sdHostCtrl.sdHostDmaTag == NULL)
        {
        SDHC_DBG (SDHC_DBG_ERR, "sdHostDmaTag create fault\n",
                  0, 0, 0, 0, 0, 0);
        free (pDrvCtrl);
        return;
        }   
    pHcf = (struct hcfDevice *)hcfDeviceGet (pInst);

    if (pHcf != NULL)
       {
       /*
       * resourceDesc {
       * The polling resource specifies whether
       * the driver uses polling mode or not.
       * If this property is not explicitly
       * specified, the driver uses interrupt
       * by default. }
       */
     
       (void)devResourceGet (pHcf, "clkFreq", HCF_RES_ADDR,
                             (void *)&clkFunc);

       if (clkFunc)
           {
            pDrvCtrl->sdHostCtrl.curClkFreq = (*clkFunc) ();
           }
       else
           {
           pDrvCtrl->sdHostCtrl.curClkFreq = SDCI_EXT_CLK;
           }

       if (devResourceGet (pHcf, "polling", HCF_RES_INT,
                           (void *)&(pDrvCtrl->sdHostCtrl.polling))!= OK )
           {
           pDrvCtrl->sdHostCtrl.polling = FALSE;
           }

       if (devResourceGet (pHcf, "type", HCF_RES_INT,
                          (void *)&(pDrvCtrl->type))!= OK )
           {
           pDrvCtrl->type = SDCI_TYPE_SD;
           }           
      
       /* Need not check return status at here */
        
       (void)devResourceGet (pHcf, "cardWpCheck", HCF_RES_ADDR,
                                    (void *)
                                    &(pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdCardWpCheck));
        
               /* Need not check return status at here */
        
       (void)devResourceGet (pHcf, "cardDetect", HCF_RES_ADDR,
                                    (void *)
                                    &(pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdCardInsertSts));
        
               /* Need not check return status at here */
        
       (void)devResourceGet (pHcf, "vddSetup", HCF_RES_ADDR,
                                    (void *)
                                    &(pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdVddSetup));
        
               /* Need not check return status at here */
        
       (void)devResourceGet (pHcf, "boardTuning", HCF_RES_ADDR,
                                    (void *)
                                    &(pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdBoardTuning));

      
        }
    /* initialize SDIO configuration library */

    pDrvCtrl->pIntInfo = (struct vxbSdioInt *)malloc(sizeof(struct vxbSdioInt));
    if(pDrvCtrl->pIntInfo == NULL)
        {
        return;
        }
    pDrvCtrl->dma.admaTable = (struct FtAdmaDesc *)
                                   cacheDmaMalloc(SD_MAX_BD_NUM * sizeof(struct FtAdmaDesc));
    
    if(pDrvCtrl->dma.admaTable == NULL)
        {
        return;
        }

    pDrvCtrl->dma.admaAddr = (UINT64)SD_CACHE_DRV_VIRT_TO_PHYS(pDrvCtrl->dma.admaTable);


    vxbSdioIntLibInit(pDrvCtrl->pIntInfo);

    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdCardInsertSts = ftSdhcCtrlCardInsertSts;
    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdCardWpCheck = ftSdhcCtrlCardWpCheck;
    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdHostCtrlInit = ftSdhcInit;
    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdClkFreqSetup = ftSdhcClkFreqSetup;
    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdVddSetup = ftSdhcVddSetup;
    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdResumeSet = NULL;
    pDrvCtrl->sdHostCtrl.sdHostOps.vxbSdBusWidthSetup = ftSdhcBusWidthSetup;
    pDrvCtrl->sdHostCtrl.capbility = OCR_VDD_VOL_32_33 | OCR_VDD_VOL_33_34 | OCR_CARD_CAP_STS;
    /* Need not check return status at here */

    (void)vxbBusAnnounce (pInst, VXB_BUSID_SD);  
    
    }

/*******************************************************************************
*
* ftSdhcInstConnect - third level initialization routine of Phytium SDHC device
*
* This routine performs the third level initialization of the Phytium SDHC device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftSdhcInstConnect
    (
    VXB_DEVICE_ID pInst
    )
    {
    STATUS rc;
    FTSDHC_DEV_CTRL * pDrvCtrl;
    SD_HOST_CTRL * pHostCtrl;
    TASK_ID monTaskId;
    pDrvCtrl = (FTSDHC_DEV_CTRL *)(pInst->pDrvCtrl);
    if (pDrvCtrl == NULL)
        {
        return;
        }
    pHostCtrl = (SD_HOST_CTRL *)pDrvCtrl; 
    rc = ftSdhcCtrlInstConnect(pHostCtrl);
    if (rc == ERROR)
        {
            return;
        }
    pHostCtrl->sdHostSpec.vxbSdBusWidthSetup = pHostCtrl->sdHostOps.vxbSdBusWidthSetup;
    pHostCtrl->sdHostSpec.vxbSdCardWpCheck = pHostCtrl->sdHostOps.vxbSdCardWpCheck;
    pHostCtrl->sdHostSpec.vxbSdClkFreqSetup = pHostCtrl->sdHostOps.vxbSdClkFreqSetup;
    pHostCtrl->sdHostSpec.vxbSdResumeSet = pHostCtrl->sdHostOps.vxbSdResumeSet;
    pHostCtrl->sdHostSpec.vxbSdVddSetup =  pHostCtrl->sdHostOps.vxbSdVddSetup;
    pHostCtrl->sdHostSpec.capbility = pHostCtrl->capbility;
    pHostCtrl->sdHostSpec.directBio = pHostCtrl->directBio;
    
    if (pHostCtrl->polling !=TRUE)  
        {
        rc = vxbIntConnect (pInst, 0,ftSdhcIrq,pDrvCtrl);
        if (rc == ERROR)
            {
             return;
            } 
         rc = vxbIntEnable (pInst, 0,ftSdhcIrq, pDrvCtrl);
         if (rc == ERROR)
            {
            return;
            }     
        }
    if(pDrvCtrl->type == SDCI_TYPE_SD)
        monTaskId = taskSpawn ("sdBusMonitor", 100, 0,8192, (FUNCPTR)ftSdhcCtrlMonitor,
                                       (_Vx_usr_arg_t)pInst,0, 0, 0, 0, 0, 0, 0, 0, 0);
    else if(pDrvCtrl->type == SDCI_TYPE_EMMC)
        ftSdhcCtrlAnnounceDevices(pInst);
    return;
    }

/*******************************************************************************
*
* ftSdhcCtrlInstConnect - third level initialization routine of Phytium SDHC device
*
* This routine performs the third level initialization of the Phytium SDHC device.
*
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/

 LOCAL STATUS ftSdhcCtrlInstConnect
    (
    SD_HOST_CTRL * pSdHostCtrl
    )
    {
    STATUS rc;
    /*
     * The devChange semaphore is used by the interrupt service routine
     * to inform the card monitor task that a state change has occurred.
     */
    pSdHostCtrl->cmdDone = semBCreate (SEM_Q_PRIORITY, SEM_EMPTY);
    if (pSdHostCtrl->cmdDone == NULL)
        {
        goto err;
        }
    pSdHostCtrl->dataDone = semBCreate (SEM_Q_PRIORITY, SEM_EMPTY);
    if (pSdHostCtrl->dataDone == NULL)
        {
        goto err;
        }
    pSdHostCtrl->hostDevSem = semMCreate(SEM_Q_PRIORITY |
                                             SEM_DELETE_SAFE |
                                             SEM_INVERSION_SAFE);
    if (pSdHostCtrl->hostDevSem == NULL)
        {
        SDHC_DBG (SDHC_DBG_ERR, "semBCreate failed for hostDevSem\n",
                      0, 0, 0, 0, 0, 0);
        goto err;
        }
    /* per-device init */
    if (pSdHostCtrl->sdHostOps.vxbSdHostCtrlInit != NULL)
        {
        rc = pSdHostCtrl->sdHostOps.vxbSdHostCtrlInit(pSdHostCtrl->pDev);        
        if (rc == ERROR)
            {  
            goto err;
            }
        }    
        return OK;    
    err:
        if (pSdHostCtrl->dataDone != NULL)
            {
            if (semDelete(pSdHostCtrl->dataDone) != OK)
                {
                SDHC_DBG (SDHC_DBG_ERR, "semDelete dataDone delete error\r\n",1,2,3,4,5,6);
                }
            }
    
        if (pSdHostCtrl->cmdDone != NULL)
            {
            if (semDelete(pSdHostCtrl->cmdDone) != OK)
               {
               SDHC_DBG (SDHC_DBG_ERR, "semDelete cmdDone delete error\r\n",1,2,3,4,5,6);
               }       
                
            }
        
        if (pSdHostCtrl->hostDevSem != NULL)
            {
            if (semDelete(pSdHostCtrl->hostDevSem) != OK)
                SDHC_DBG (SDHC_DBG_ERR, "semDelete hostDevSem delete error\r\n",1,2,3,4,5,6);
            }
    return ERROR;
    }


 /*******************************************************************************
 *
 * ftSdhcSpecInfoGet - get host controller spec info
 *
 * This routine gets host controller spec info.
 *
 * RETURNS: OK or ERROR
 *
 * ERRNO: N/A
 */

 LOCAL STATUS ftSdhcSpecInfoGet
     (
     VXB_DEVICE_ID pDev,
     void ** pHostSpec,
     VXB_SD_CMDISSUE_FUNC * pCmdIssue
     )
     {
     
     SD_HOST_CTRL * pSdHostCtrl;

     pSdHostCtrl = (SD_HOST_CTRL *)pDev->pDrvCtrl;
     if (pSdHostCtrl == NULL)
         return ERROR;

     *pHostSpec = (void *)(&(pSdHostCtrl->sdHostSpec));
     
     *pCmdIssue = ftSdhcCmdIssue;
     
     return (OK);
     }
 
/*******************************************************************************
*
* ftSdhcResetHw - hardware reset of Phytium SDHC device
*
* This routine performs Hardware reset of the Phytium SDHC device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/ 
 
LOCAL void ftSdhcResetHw(VXB_DEVICE_ID pDev)
    {
    FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)pDev->pDrvCtrl;
    CSR_SETBIT_4(pDev, SDCI_CNTRL, SDCI_CNTRL_FIFO_RESET | SDCI_CNTRL_DMA_RESET);
    while ((CSR_READ_4(pDev,SDCI_CNTRL) & (SDCI_CNTRL_FIFO_RESET | SDCI_CNTRL_DMA_RESET)))
        {
        VX_MEM_BARRIER_RW ();
        }
    ftSdhcPrivateSendCmd(pDev,SDCI_CMD_UPD_CLK,0);
    
    if (pDrvCtrl->type == SDCI_TYPE_SD)
        CSR_CLRBIT_4(pDev, SDCI_CARD_RESET, SDCI_CARD_RESET_ENABLE);
    else
        CSR_SETBIT_4(pDev, SDCI_CARD_RESET, SDCI_CARD_RESET_ENABLE);
    }

/*******************************************************************************
*
* ftSdhcCtrlCardInsertSts - check if card is inserted
*
* This routine checks if card is inserted.
*
* RETURNS: TRUE/FALSE
*
* ERRNO: N/A
*/ 

LOCAL BOOL ftSdhcCtrlCardInsertSts
    (
    VXB_DEVICE_ID pDev
    )
    {
    SD_HOST_CTRL * pSdHostCtrl;
    UINT32 status;  
    pSdHostCtrl = (SD_HOST_CTRL *)pDev->pDrvCtrl;
    status = CSR_READ_4(pDev,SDCI_CARD_DETECT);
    if ((status & 0x1) == 0x1)
        {
        pSdHostCtrl->isInsert = FALSE;
        }
    else
        {
        pSdHostCtrl->isInsert = TRUE; 
        }   
    return (pSdHostCtrl->isInsert);
    } 

/*******************************************************************************
*
* ftSdhcCtrlCardWpCheck - check if card is write protected
*
* This routine check if card is write protected.
*
* RETURNS: TRUE/FALSE
*
* ERRNO: N/A
*/ 

LOCAL BOOL ftSdhcCtrlCardWpCheck
    (
    VXB_DEVICE_ID pDev
    )
    {
    BOOL wp;
    wp = FALSE;        
    return  (wp);
    }

/*******************************************************************************
*
* ftSdhcUpdateExternalClk - update external clock
*
* update external clock
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftSdhcUpdateExternalClk(VXB_DEVICE_ID pDev, UINT32 uhs)
    {
    CSR_WRITE_4(pDev, SDCI_UHS_REG_EXT, 0);
    CSR_WRITE_4(pDev, SDCI_UHS_REG_EXT, uhs);
    /* Wait for the clock division to complete */
    while (!(CSR_READ_4(pDev, SDCI_CCLK_RDY) & 0x1));
    }

/*******************************************************************************
*
* ftSdhcClkFreqSetup - setup the clock frequency for Phytium SDHC device
*
* This routine setups the clock frequency. This routine will
* be called in the SDHC driver. The clock frequency calculation
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftSdhcClkFreqSetup  
    (
    VXB_DEVICE_ID pDev,
    UINT32       clock
    )
    {
    FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)(pDev->pDrvCtrl); 
    UINT32 div = 0xff, drv = 0, sample = 0;
    unsigned long clkRate;
    UINT32 sdciCmdBits = SDCI_CMD_UPD_CLK;
    UINT32 firstUhsDiv, tmpExtReg;
    
    if(clock)
    {
        if (clock > SDCI_MAX_CLK)
            clock = SDCI_MAX_CLK;
        
        if (clock >= 25000000)
            tmpExtReg = 0x202;
        else if (clock == 400000)
            tmpExtReg = 0x502;
        else
            tmpExtReg = 0x302;
        
        ftSdhcUpdateExternalClk(pDev, tmpExtReg);
        CSR_CLRBIT_4(pDev,SDCI_CLKENA, SDCI_CLKENA_CCLK_ENABLE);
        ftSdhcPrivateSendCmd(pDev, sdciCmdBits, 0);
        
        clkRate = pDrvCtrl->sdHostCtrl.curClkFreq;
        firstUhsDiv = 1 + ((tmpExtReg >> 8)&0xFF);
        div = clkRate / (2 * firstUhsDiv * clock);

        if (div > 2 ) 
            {
            sample = div / 2 + 1;
            drv = sample - 1;
            CSR_WRITE_4(pDev, SDCI_CLKDIV, (sample << 16) | (drv << 8) | (div & 0xff));
            } 
        else if (div == 2)
            {
            drv = 0;
            sample = 1;
            CSR_WRITE_4(pDev, SDCI_CLKDIV, (sample << 16) | (drv << 8) | (div & 0xff));
            } 

        CSR_SETBIT_4(pDev,SDCI_CLKENA, SDCI_CLKENA_CCLK_ENABLE);
        
        ftSdhcPrivateSendCmd(pDev, sdciCmdBits, 0);

        SDHC_DBG (SDHC_DBG_RW, "clock %d, UHS_REG_EXT ext: %x, CLKDIV: %x\n", clock,    
                CSR_READ_4(pDev, SDCI_UHS_REG_EXT),CSR_READ_4(pDev, SDCI_CLKDIV),4,5,6);
    }
    
    return;
    }


/*******************************************************************************
*
* ftSdhcVddSetup - setup the SD bus voltage level and power it up
*
* This routine setups the SD bus voltage and powers it up. This
* routine will be called in the SDHC driver.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftSdhcVddSetup
    (
    VXB_DEVICE_ID pDev,
    UINT32       vdd
    )
    {
    
    return;
    }

/*******************************************************************************
*
* ftSdhcBusWidthSetup - setup the SD bus width 
*
* This routine setups the SD bus width. This
* routine will be called in the SDHC driver.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftSdhcBusWidthSetup
    (
    VXB_DEVICE_ID pDev,
    UINT32       width
    )
    {
    UINT32 val;

    switch (width)
    {
    case SDMMC_BUS_WIDTH_1BIT:
        val = CARD0_WIDTH0_1BIT;               
        break;
    case SDMMC_BUS_WIDTH_4BIT:
        val = CARD0_WIDTH0_4BIT;
        break;
    case SDMMC_BUS_WIDTH_8BIT:
        val = CARD0_WIDTH1_8BIT;
        break;
    default:
        val = CARD0_WIDTH0_4BIT;
        break;
    }
    SDHC_DBG (SDHC_DBG_RW, "ftSdhcBusWidthSetup %x\n", val, 2,3,4,5,6);
    CSR_WRITE_4(pDev, SDCI_CTYPE, val);
    return;
    }

/*******************************************************************************
*
* ftSdhcInit - Phytium SDHC per device specific initialization
*
* This routine performs per device specific initialization of Phytium SDHC.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL STATUS ftSdhcInit
    (
    VXB_DEVICE_ID pInst
    )
    {
    FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)(pInst->pDrvCtrl);  

    UINT32 val;
    
    /* config creg_nand_mmcsd_hddr's DMA */
    writel(0, FT_LSD_CONFIG_BASE + FT_LSD_NAND_MMCSD_HADDR);
    
    CSR_WRITE_4(pInst, SDCI_FIFOTH, SDCI_SET_FIFOTH(0x2, 0x7, 0x100));
    CSR_WRITE_4(pInst, SDCI_CARD_THRCTL, SDCI_CARD_THRCTL_THRESHOLD(SDCI_FIFO_DEPTH_8) | SDCI_CARD_THRCTL_CARDRD);
    CSR_SETBIT_4(pInst, SDCI_PWREN, SDCI_PWREN_ENABLE);

    ftSdhcClkFreqSetup(pInst, SDMMC_CLK_FREQ_400KHZ);
    CSR_CLRBIT_4(pInst, SDCI_UHS_REG, SDCI_UHS_REG_VOLT);
 
    ftSdhcResetHw(pInst);
      
    CSR_WRITE_4(pInst, SDCI_INT_MASK, 0);
    val = CSR_READ_4(pInst, SDCI_RAW_INTS);
    CSR_WRITE_4(pInst, SDCI_RAW_INTS, val);
    
    CSR_WRITE_4(pInst, SDCI_DMAC_INT_ENA, 0);
    val = CSR_READ_4(pInst, SDCI_DMAC_STATUS);
    CSR_WRITE_4(pInst, SDCI_DMAC_STATUS, val);

    CSR_SETBIT_4(pInst, SDCI_CNTRL, SDCI_CNTRL_INT_ENABLE |
             SDCI_CNTRL_USE_INTERNAL_DMAC);
  
    CSR_WRITE_4(pInst, SDCI_TMOUT, 0xFFFFFFFF);
    if (vxbDmaBufMapCreate (pInst, pDrvCtrl->sdHostCtrl.sdHostDmaTag, 0,
                            &(pDrvCtrl->sdHostCtrl.sdHostDmaMap)) == NULL)
        {
        return ERROR;
        }
    return OK;
    }
 
/*******************************************************************************
*
* ftSdhcCtrlAnnounceDevices - Phytium SDHC announce devices
*
* This function will announce devices
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftSdhcCtrlAnnounceDevices
    (
    VXB_DEVICE_ID pDev
    )
    {
    SD_HOST_CTRL * pSdHostCtrl;

    pSdHostCtrl = (SD_HOST_CTRL *)(pDev->pDrvCtrl);
    if (pSdHostCtrl == NULL)
        return;   

    if (pSdHostCtrl->sdHostOps.vxbSdClkFreqSetup != NULL)
        pSdHostCtrl->sdHostOps.vxbSdClkFreqSetup(pDev, SDMMC_CLK_FREQ_400KHZ);
            

    if (pSdHostCtrl->sdHostOps.vxbSdBusWidthSetup != NULL)
        pSdHostCtrl->sdHostOps.vxbSdBusWidthSetup (pDev, SDMMC_BUS_WIDTH_1BIT);

     /* Need not check return status at here*/

     (void)sdBusAnnounceDevices(pDev, NULL);
    }


/*******************************************************************************
*
* ftSdhcCtrlMonitor - Phytium SDHC insert status monitor
*
* This function will check Phytium SDHC insert/remove status. If target device
* insert status is TRUE, the function will run sdioBusAnnounceDevices to add
* target device to vxbus system. If FLASE, will run vxbDevRemovalAnnounce to
* remove the target device from vxbus system.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftSdhcCtrlMonitor
    (
    VXB_DEVICE_ID pDev
    )
    {
    SD_HOST_CTRL * pSdHostCtrl;

    pSdHostCtrl = (SD_HOST_CTRL *)(pDev->pDrvCtrl);
    if (pSdHostCtrl == NULL)
        return;   
    pSdHostCtrl->attached = FALSE;
    while(1)
        {
        int  cardin;
        taskDelay ( sysClkRateGet());
    
        cardin= ftSdhcCtrlCardInsertSts(pDev);

        if(cardin == TRUE)
            {
            if (pSdHostCtrl->attached == TRUE)
                {
                continue;
                }
            ftSdhcCtrlAnnounceDevices(pDev);
            pSdHostCtrl->attached = TRUE; 
            }
        else
            {
            VXB_DEVICE_ID pDevList;
            int i=0;
            if (pSdHostCtrl->attached == FALSE)
               {
               continue;
               }     
            pDevList = pDev->u.pSubordinateBus->instList;
            for(i = 0; i < MAX_TARGET_DEV; i++)
                {
                if (pDevList != NULL)
                    {
                    /* Need not check return status at here */
                    (void)vxbDevRemovalAnnounce(pDevList);
                    pDevList = pDevList->pNext;
                    }
                else
                    break;
                }
              pSdHostCtrl->attached = FALSE;    
             }
         }
    }

/*******************************************************************************
*
* ftSdhcCmdDone - process the cmd after cmd done
*
* This routine process the cmd after cmd done
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS ftSdhcCmdDone(FTSDHC_DEV_CTRL * pDrvCtrl)
    {
    STATUS ret = OK;
    VXB_DEVICE_ID pDev;
    SD_CMD * pSdCmd;
    UINT32 cmdRsp[4];

    pDev= pDrvCtrl->pDev;
    pSdCmd = pDrvCtrl->pSdCmd;
    
    ret = semTake(pDrvCtrl->sdHostCtrl.cmdDone, sysClkRateGet() * SDCI_CMD_TIMEOUT);

    if(ret == ERROR)
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcCmdDone: semTake error\n",0,1,2,3,4,5);
        pSdCmd->cmdErr |= SDMMC_CMD_ERR_TIMEOUT;
        return ERROR;
        } 
        
    if (pSdCmd->rspType != SDMMC_CMD_RSP_NONE) 
        {
        if (pSdCmd->rspType & SDMMC_CMD_RSP_LEN136) 
            {
            cmdRsp[3] = CSR_READ_4(pDev,SDCI_RESP0);
            cmdRsp[2] = CSR_READ_4(pDev,SDCI_RESP1);
            cmdRsp[1] = CSR_READ_4(pDev,SDCI_RESP2);
            cmdRsp[0] = CSR_READ_4(pDev,SDCI_RESP3);

            pSdCmd->cmdRsp[0] = be32toh (cmdRsp[0]);
            pSdCmd->cmdRsp[1] = be32toh (cmdRsp[1]);
            pSdCmd->cmdRsp[2] = be32toh (cmdRsp[2]);
            pSdCmd->cmdRsp[3] = be32toh (cmdRsp[3]);    
            } 
        else 
            {
            pSdCmd->cmdRsp[0] = CSR_READ_4(pDev,SDCI_RESP0);
            pSdCmd->cmdRsp[1] = 0;
            pSdCmd->cmdRsp[2] = 0;
            pSdCmd->cmdRsp[3] = 0;  
            }
        }
    
    if((pSdCmd->cmdErr != 0) && (pSdCmd->cmdIdx != SDMMC_CMD_SEND_STATUS) )
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcCmdDone: cmdErr :%x\n",pSdCmd->cmdErr,1,2,3,4,5);
        return ERROR;
        }
    
    return OK;  
    }

/*******************************************************************************
*
* ftSdhcDataDone - process the data after data done
*
* This routine process the data after data done
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS ftSdhcDataDone(FTSDHC_DEV_CTRL * pDrvCtrl)
    {
    STATUS ret = OK, rc;
    VXB_DEVICE_ID pDev;
    SD_CMD * pSdCmd;
    SD_HOST_CTRL * pSdHostCtrl;

    ret = ftSdhcCmdDone(pDrvCtrl);
    if (ret == ERROR)
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcCmdDone error\n",0,1,2,3,4,5);
        return ERROR;
        }
    
    pDev= pDrvCtrl->pDev;
    pSdCmd = pDrvCtrl->pSdCmd;
    pSdHostCtrl = (SD_HOST_CTRL *)pDrvCtrl;
    rc = semTake(pDrvCtrl->sdHostCtrl.dataDone, sysClkRateGet() * SDCI_DATA_TIMEOUT);
    
    if(rc == ERROR)
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcDataDone: semTake error\n",0,1,2,3,4,5);
        pSdCmd->dataErr |= SDMMC_DATA_ERR_TIMEOUT;
        return ERROR;
        }   
    
    if(pSdCmd->dataErr != 0)
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcDataDone: dataErr :%x\n",pSdCmd->dataErr,1,2,3,4,5);
        return ERROR;
        }

    if (pSdCmd->cmdData.isRead == TRUE) 
        {   
        vxbDmaBufMapSync (pDev,
                      pSdHostCtrl->sdHostDmaTag,
                      pSdHostCtrl->sdHostDmaMap,
                      0,
                      pSdCmd->cmdData.blkNum * pSdCmd->cmdData.blkSize,
                      _VXB_DMABUFSYNC_DMA_POSTREAD);
        }
    else
        {
        vxbDmaBufMapSync (pDev,
                  pSdHostCtrl->sdHostDmaTag,
                  pSdHostCtrl->sdHostDmaMap,
                  0,
                  pSdCmd->cmdData.blkNum * pSdCmd->cmdData.blkSize,
                 _VXB_DMABUFSYNC_DMA_POSTWRITE);    
        }
    
    vxbDmaBufMapUnload(pSdHostCtrl->sdHostDmaTag, pSdHostCtrl->sdHostDmaMap);
    
    if(pSdCmd->cmdData.blkNum > 1)
        {
        ftSdhcPrivateSendCmd(pDev,SDMMC_CMD_STOP_TRANSMISSION,0);   
        }
 
    return OK;
    }

/*******************************************************************************
*
* ftSdhcIrq - interrupt service routine
*
* This routine handles interrupts of sdhc
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL void ftSdhcIrq(FTSDHC_DEV_CTRL * pDrvCtrl)
    { 
    VXB_DEVICE_ID pDev= pDrvCtrl->pDev;
    UINT32 events, event_mask, dmac_events, dmac_evt_mask;
    
    CSR_WRITE_4(pDev, 0xfd0, 0);
    events = CSR_READ_4(pDev, SDCI_RAW_INTS);
    dmac_events = CSR_READ_4(pDev, SDCI_DMAC_STATUS);
    event_mask = CSR_READ_4(pDev, SDCI_INT_MASK);
    dmac_evt_mask = CSR_READ_4(pDev, SDCI_DMAC_INT_ENA);
    
    if ((!events) && (!(dmac_events&0x1fff))) 
        return ;
    
    CSR_WRITE_4(pDev, SDCI_RAW_INTS, events);
    CSR_WRITE_4(pDev, SDCI_DMAC_STATUS, dmac_events);
    
    if (((events & event_mask) == 0) && ((dmac_evt_mask & dmac_events) == 0))
        return;

    if(events & cmd_err_ints_mask)
        {
        if(events & SDCI_RAW_INTS_RTO)
            pDrvCtrl->pSdCmd->cmdErr |= SDMMC_CMD_ERR_TIMEOUT;
        else
            pDrvCtrl->pSdCmd->cmdErr |= SDMMC_CMD_ERR_PROTOCOL;
        SDHC_DBG (SDHC_DBG_ERR, "CMD ERROR SDCI_RAW_INTS:%x  CMDID:%d\r\n",events, pDrvCtrl->pSdCmd->cmdIdx,2,3,4,5); 
        }
    if(events & data_err_ints_mask)
        {
        if((events & SDCI_RAW_INTS_DRTO))
            pDrvCtrl->pSdCmd->dataErr |= SDMMC_DATA_ERR_TIMEOUT;
        else
            pDrvCtrl->pSdCmd->dataErr |= SDMMC_DATA_ERR_PROTOCOL;
        SDHC_DBG (SDHC_DBG_ERR, "DATA ERROR: SDCI_RAW_INTS:%x  CMDID:%d\r\n",events, pDrvCtrl->pSdCmd->cmdIdx,2,3,4,5); 
        }   
    if(dmac_events & dmac_err_ints_mask)
        {
        pDrvCtrl->pSdCmd->dataErr |= SDMMC_DATA_ERR_PROTOCOL;
        SDHC_DBG (SDHC_DBG_ERR, "DATA ERROR: SDCI_DMAC_STATUS:%x  CMDID:%d\r\n",dmac_events, pDrvCtrl->pSdCmd->cmdIdx,2,3,4,5);
        }

    if (events & cmd_ints_mask)
        {
        CSR_CLRBIT_4(pDev,SDCI_INT_MASK, cmd_ints_mask);
        SDHC_DBG (SDHC_DBG_RW, "CMD: SDCI_RAW_INTS:%x\n",events,2,3,4,5,6);
        semGive(pDrvCtrl->sdHostCtrl.cmdDone);
        }
    if ((events & data_ints_mask)||(dmac_events & dmac_ints_mask)) 
        {
        SDHC_DBG (SDHC_DBG_RW, "DATA: SDCI_RAW_INTS:%x, SDCI_DMAC_STATUS:%x\n",events,dmac_events,3,4,5,6);
        CSR_CLRBIT_4(pDev,SDCI_DMAC_INT_ENA, dmac_ints_mask);
        CSR_CLRBIT_4(pDev,SDCI_INT_MASK, data_ints_mask);
        CSR_CLRBIT_4(pDev,SDCI_BUS_MODE, SDCI_BUS_MODE_DE);
        semGive(pDrvCtrl->sdHostCtrl.dataDone);
        }
    return;
    }

/*******************************************************************************
*
* ftSdhcAdmaReset - Reset the adma  
*
* This routine reset the adma.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftSdhcAdmaReset(VXB_DEVICE_ID pDev)
   {
    UINT32 bmod = CSR_READ_4(pDev,SDCI_BUS_MODE);

    bmod |= SDCI_BUS_MODE_SWR;
    CSR_WRITE_4(pDev, SDCI_BUS_MODE, bmod);
   }

/*******************************************************************************
*
* ftSdhcInitAdmaTable - init the adma table
*
* This routine init the adma table.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftSdhcInitAdmaTable(VXB_DEVICE_ID pDev, struct ftSdhcDma *dma)
{
    struct FtAdmaDesc *admaTable = dma->admaTable;
    UINT64 dmaAddr;
    UINT32 i;

    memset(admaTable, 0, sizeof(struct FtAdmaDesc) * SD_MAX_BD_NUM);

    for (i = 0; i < (SD_MAX_BD_NUM - 1); i++) 
        {
        dmaAddr = dma->admaAddr + sizeof(*admaTable) * (i + 1);

#ifdef _WRS_CONFIG_LP64
        admaTable[i].descLo = SD_ADDR_LO(dmaAddr);
        admaTable[i].descHi = SD_ADDR_HI(dmaAddr);
#else
        admaTable[i].descLo = (UINT32)dmaAddr;
        admaTable[i].descHi = 0;
#endif
        admaTable[i].attribute = 0;
        admaTable[i].NON1 = 0;
        admaTable[i].len = 0;
        admaTable[i].NON2 = 0;
        }

    ftSdhcAdmaReset(pDev);
}

/*******************************************************************************
*
* ftSdhcAdmaWriteDesc - write adma descriptor
*
* This routine write adma descriptor.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/    
LOCAL void ftSdhcAdmaWriteDesc(struct FtAdmaDesc *desc,
                               UINT64 addr, UINT32 len, UINT32 attribute)
    {
    desc->attribute = attribute;
    desc->len = len;
#ifdef _WRS_CONFIG_LP64
    desc->addrLo = SD_ADDR_LO(addr);
    desc->addrHi = SD_ADDR_HI(addr);
#else
    desc->addrLo = (UINT32)addr;
    desc->addrHi = 0;
#endif

    if ((attribute & IDMAC_DES0_LD) == IDMAC_DES0_LD) 
        {
        desc->descLo = 0;
        desc->descHi = 0;
        }
    }

/*******************************************************************************
*
* ftSdhcPrepareCmdRaw - prepare commond
*
* This routine prepare commond.
*
* RETURNS: commond
*
* ERRNO: N/A
*/

LOCAL UINT32 ftSdhcPrepareCmdRaw(SD_CMD * pSdCmd)
{

    UINT32 resp = ftSdhcCmdFindResp(pSdCmd);
    UINT32 opcode = pSdCmd->cmdIdx;
    UINT32 rawcmd = (SDCI_CMD_INDX(opcode) | resp);
    
    if (opcode == SDMMC_CMD_GO_IDLE_STATE)
        rawcmd |= SDCI_CMD_INIT;
    if (opcode == SDMMC_CMD_GO_INACTIVE_STATE)
        rawcmd |= SDCI_CMD_STOP;
    
    if (pSdCmd->hasData) 
        {
        rawcmd |= SDCI_CMD_DAT_EXP;
        if(!pSdCmd->cmdData.isRead)
            rawcmd |= SDCI_CMD_DAT_WR;
        }

    return (rawcmd | SDCI_CMD_START);
}

/*******************************************************************************
*
* ftSdhcCmdFindResp - get response mask
*
* This routine get response mask.
*
* RETURNS: response
*
* ERRNO: N/A
*/

LOCAL  UINT32 ftSdhcCmdFindResp(SD_CMD * pSdCmd)
 {
    UINT32 resp;
    switch (pSdCmd->rspType & 0xff) 
    {
    case SDMMC_CMD_RSP_R1:
    case SDMMC_CMD_RSP_R1B:
        resp = SDCI_CMD_RESP_CRC | SDCI_CMD_RESP_EXP;
        break;

    case SDMMC_CMD_RSP_R2:
        resp = SDCI_CMD_RESP_CRC | SDCI_CMD_RESP_LONG | SDCI_CMD_RESP_EXP;
        break;

    case SDMMC_CMD_RSP_R3:
        resp = SDCI_CMD_RESP_EXP;
        break;

    case SDMMC_CMD_RSP_NONE:
    default:
        resp = 0x0;
        break;
    }
    return resp;
 }

/*******************************************************************************
*
* ftSdCtrlReadBlock - read data from SD card
*
* This routine read data from SD card.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL int ftSdhcStartData(VXB_DEVICE_ID pDev,SD_CMD * pSdCmd)
   {
    FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)(pDev->pDrvCtrl);
    SD_HOST_CTRL * pSdHostCtrl = (SD_HOST_CTRL *)pDrvCtrl;  
    UINT64 dma_address = 0; 
    UINT32 rawcmd; 
    UINT32 i;
    
    if(pSdCmd->cmdData.blkNum > SD_MAX_BD_NUM)
        {
        SDHC_DBG (SDHC_DBG_ERR, "size in blocks of cache group is greater than the supported value,\
                please change DOSFS_BLOCKS_PER_DATA_DIR_CACHE_GROUP to be less than or equal to %d\n"
                , SD_MAX_BD_NUM,1,2,3,4,5);
        return ERROR;
        }
    
    struct FtAdmaDesc *desc = pDrvCtrl->dma.admaTable;
    
    if (pSdCmd->cmdData.isRead == TRUE) 
        {
        vxbDmaBufMapSync (pDev,
                pSdHostCtrl->sdHostDmaTag,
                pSdHostCtrl->sdHostDmaMap,
                0,
                pSdCmd->cmdData.blkNum * pSdCmd->cmdData.blkSize,
                _VXB_DMABUFSYNC_DMA_PREREAD);
        }
    else
        {
        vxbDmaBufMapSync (pDev,
                      pSdHostCtrl->sdHostDmaTag,
                      pSdHostCtrl->sdHostDmaMap,
                      0,
                      pSdCmd->cmdData.blkNum * pSdCmd->cmdData.blkSize,
                     _VXB_DMABUFSYNC_DMA_PREWRITE);
        }
   
    CSR_WRITE_4(pDev, SDCI_RAW_INTS, 0xffffe);
    
    CSR_SETBIT_4(pDev, SDCI_CNTRL, SDCI_CNTRL_FIFO_RESET | SDCI_CNTRL_DMA_RESET);
    while (CSR_READ_4(pDev, SDCI_CNTRL) & (SDCI_CNTRL_FIFO_RESET | SDCI_CNTRL_DMA_RESET))
        VX_MEM_BARRIER_RW ();
    
    CSR_SETBIT_4(pDev, SDCI_CNTRL, SDCI_CNTRL_USE_INTERNAL_DMAC);
    CSR_CLRBIT_4(pDev, SDCI_CNTRL, SDCI_CNTRL_INT_ENABLE);

    rawcmd = ftSdhcPrepareCmdRaw(pSdCmd);
    ftSdhcInitAdmaTable(pDev, &pDrvCtrl->dma);

    for(i = 0; i < pSdCmd->cmdData.blkNum; i++)
        {
        dma_address= (UINT64)(pSdHostCtrl->sdHostDmaMap->fragList[i].frag);
        if(i == 0)
            {
            if(pSdCmd->cmdData.blkNum == 1)
                ftSdhcAdmaWriteDesc(desc, dma_address,pSdCmd->cmdData.blkSize, IDMAC_DES0_OWN | IDMAC_DES0_LD | IDMAC_DES0_FD | IDMAC_DES0_ER);
            else
                ftSdhcAdmaWriteDesc(desc, dma_address,pSdCmd->cmdData.blkSize, IDMAC_DES0_OWN | IDMAC_DES0_DIC | IDMAC_DES0_FD | IDMAC_DES0_CH);
            }
        else if(i == (pSdCmd->cmdData.blkNum - 1))
            ftSdhcAdmaWriteDesc(desc, dma_address,pSdCmd->cmdData.blkSize, IDMAC_DES0_OWN | IDMAC_DES0_LD | IDMAC_DES0_ER);
        else
            ftSdhcAdmaWriteDesc(desc, dma_address,pSdCmd->cmdData.blkSize, IDMAC_DES0_OWN | IDMAC_DES0_DIC | IDMAC_DES0_CH);
        desc++;
        }            
    
    CSR_SETBIT_4(pDev, SDCI_DMAC_INT_ENA, dmac_ints_mask);
    CSR_SETBIT_4(pDev, SDCI_INT_MASK, cmd_ints_mask | data_ints_mask);
    CSR_SETBIT_4(pDev, SDCI_BUS_MODE, SDCI_BUS_MODE_DE);
#ifdef _WRS_CONFIG_LP64    
    CSR_WRITE_4(pDev, SDCI_DESC_LIST_ADDRL, SD_ADDR_LO(pDrvCtrl->dma.admaAddr));
    CSR_WRITE_4(pDev, SDCI_DESC_LIST_ADDRH, SD_ADDR_HI(pDrvCtrl->dma.admaAddr));
#else
    CSR_WRITE_4(pDev, SDCI_DESC_LIST_ADDRL, (UINT32)pDrvCtrl->dma.admaAddr);
    CSR_WRITE_4(pDev, SDCI_DESC_LIST_ADDRH, 0);
#endif
    CSR_WRITE_4(pDev, SDCI_BLKSIZ, pSdCmd->cmdData.blkSize);
    CSR_WRITE_4(pDev, SDCI_BYTCNT, pSdCmd->cmdData.blkNum * pSdCmd->cmdData.blkSize);
    CSR_SETBIT_4(pDev, SDCI_CNTRL, SDCI_CNTRL_INT_ENABLE);
    CSR_WRITE_4(pDev, SDCI_CMDARG, pSdCmd->cmdArg);
    
    /* drain writebuffer */
    VX_MEM_BARRIER_RW ();
    
    CSR_WRITE_4(pDev, SDCI_CMD, rawcmd);

    return ftSdhcDataDone(pDrvCtrl);
}

/*******************************************************************************
*
* ftSdhcPrivateSendCmd - send pravate command
*
* This routine send pravate command.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftSdhcPrivateSendCmd(VXB_DEVICE_ID pDev, UINT32 cmd, UINT32 arg)
    {
    CSR_WRITE_4(pDev, SDCI_CMDARG, arg);
    while ((CSR_READ_4(pDev, SDCI_STATUS) & (SDCI_STATUS_CARD_BUSY)))
        VX_MEM_BARRIER_RW ();
    CSR_WRITE_4(pDev, SDCI_CMD, SDCI_CMD_START | cmd);
    SDHC_DBG (SDHC_DBG_RW, "ftSdhcPrivateSendCmd  cmd:%x  arg:%x\n", cmd,arg,2,3,4,5);
    while (CSR_READ_4(pDev, SDCI_CMD) & SDCI_CMD_START)
        VX_MEM_BARRIER_RW ();
    }

/*******************************************************************************
*
* ftSdhcDoCmd - send sd command
*
* This routine send sd command.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL int ftSdhcStartCmd(VXB_DEVICE_ID pDev,SD_CMD * pSdCmd)
    {
    FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)(pDev->pDrvCtrl);
    UINT32 rawcmd;
    
    CSR_WRITE_4(pDev, SDCI_RAW_INTS, 0xffffe);
    rawcmd = ftSdhcPrepareCmdRaw(pSdCmd);
    CSR_SETBIT_4(pDev, SDCI_INT_MASK, cmd_ints_mask);
    CSR_WRITE_4(pDev, SDCI_CMDARG, pSdCmd->cmdArg);    
    CSR_WRITE_4(pDev, SDCI_CMD, rawcmd);
    
    return ftSdhcCmdDone(pDrvCtrl);
    }

/*******************************************************************************
*
* ftSdhcCmdIssue - issue the command to be sent
*
* This routine issue the command to be sent.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS ftSdhcCmdIssue
    (
    VXB_DEVICE_ID pDev,
    SD_CMD * pSdCmd
    )
    {        
    int result;
    FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)(pDev->pDrvCtrl); 
    SD_HOST_CTRL * pSdHostCtrl = (SD_HOST_CTRL *) pDrvCtrl;
    
    if (semTake (pSdHostCtrl->hostDevSem, WAIT_FOREVER) == ERROR)
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcCmdIssue: semTake error\r\n",0,1,2,3,4,5); 
        return ERROR;
        }
    pDrvCtrl->pSdCmd = pSdCmd;
    if(pSdCmd->cmdIdx == SDIO_CMD_SEND_OPCOND)
        {
        pSdCmd->cmdErr = SDMMC_CMD_ERR_TIMEOUT;
        semGive (pSdHostCtrl->hostDevSem);
        return OK;
        }
    while ((CSR_READ_4(pDev, SDCI_STATUS) & (SDCI_STATUS_CARD_BUSY)))
        VX_MEM_BARRIER_RW ();
    
    SDHC_DBG (SDHC_DBG_RW, "ftSdhcCmdIssue: cmd:%d\r\n",pSdCmd->cmdIdx,1,2,3,4,5);
    if (pSdCmd->hasData)
        {
        SD_HOST_CTRL * pSdHostCtrl;
        FTSDHC_DEV_CTRL * pDrvCtrl = (FTSDHC_DEV_CTRL *)(pDev->pDrvCtrl);   
        pSdHostCtrl = (SD_HOST_CTRL *)pDrvCtrl;  
        vxbDmaBufMapLoad (pDev, pDrvCtrl->sdHostCtrl.sdHostDmaTag,
                               pDrvCtrl->sdHostCtrl.sdHostDmaMap,
                               pSdCmd->cmdData.buffer,
                               pSdCmd->cmdData.blkNum * pSdCmd->cmdData.blkSize,
                               0);

        result = ftSdhcStartData(pDev, pSdCmd);
        if(result == ERROR)
            {
            SDHC_DBG (SDHC_DBG_ERR, "ftSdhcCmdIssue: ftSdhcStartData error\r\n",0,1,2,3,4,5); 
            }
        semGive (pSdHostCtrl->hostDevSem);
        return result;
        }

    result = ftSdhcStartCmd(pDev, pSdCmd);
    if(result == ERROR)
        {
        SDHC_DBG (SDHC_DBG_ERR, "ftSdhcCmdIssue: ftSdhcStartCmd error\r\n",0,1,2,3,4,5);
        }
    semGive (pSdHostCtrl->hostDevSem);
    return result;
    }
