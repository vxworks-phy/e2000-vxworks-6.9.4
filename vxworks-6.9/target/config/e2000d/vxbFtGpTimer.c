/* vxbFtGpTimer.c - Phytium General Purpose Timer driver for VxBus */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
/* includes */

#include <vxWorks.h>
#include <vxbTimerLib.h>
#include <hwif/util/hwMemLib.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/util/vxbParamSys.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <taskLib.h>
#include <drv/timer/timerDev.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include "vxbFtGpTimer.h"
#include "vxbFtGptLib.h"
#include "defs.h"

/* defines */

#undef GP_CLK_DEBUG
#ifdef GP_CLK_DEBUG

#define CLK_LOG(msg,a,b,c,d,e,f) \
    uartf(msg,a,b,c,d,e,f)
#else 

#define CLK_LOG(msg,a,b,c,d,e,f)
#endif 


#define FTGPT_TIMER_NAME            "ftGpTimerDev"

#define FTGPT_TIMER_MAX_COUNT        0xffffffff
#define FTGPT_TIMER_MAX_COUNT64      0xffffffffffffffffULL


#define DEFAULT_TICKS_PER_SECOND        60


/* access interface */

#define FT_GPT_BAR(p)   ((FTGPT_TIMER *)(p)->pDrvCtrl)->gptBar
#define FT_GPT_HANDLE(p)((FTGPT_TIMER *)(p)->pDrvCtrl)->gptHandle

#define CSR_READ_4(pDev, addr)                \
    vxbRead32 (FT_GPT_HANDLE(pDev), (UINT32 *)((char *)FT_GPT_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)            \
    vxbWrite32 (FT_GPT_HANDLE(pDev),          \
        (UINT32 *)((char *)FT_GPT_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))


/* structure to store the timer information */

typedef struct ftGptTimer
    {
    VXB_DEVICE_ID       pDev;
    void *              gptBar;
    void *              gptHandle;

    /* public attributes */
    UINT32              workMode;
    UINT32              timerBits;
    VOIDFUNCPTR         pIsrFunc[MAX_TIMER_TACHO_EVENT];
    int                 arg[MAX_TIMER_TACHO_EVENT];
    BOOL                intConnected;
    struct vxbTimerFunctionalityEx  timerFuncEx;

    
    /* timer mode: */
    TimerCmpType        cmpType; /* cycle/once */
    struct vxbTimerFunctionality    timerFunc;
    spinlockIsr_t       spinLock;
    UINT32              maxCount;
    UINT32              cmpVal;
    UINT64              maxCount64;
    UINT64              cmpVal64;
    UINT64              cmpValCurrent64;/*for workaround*/
    BOOL                isEnabled;
    UINT32              runMode; /*restart/free-run. (not open for user)*/

    /* tacho mode: */
    UINT32              edgeMode;
    UINT32              debounce;
    UINT32              pulsNum;
    
    /* capture mode: */
    UINT32              captureCnt;
    } FTGPT_TIMER;

/* function declarations */

LOCAL void gptInstInit(VXB_DEVICE_ID pDev);
LOCAL void gptInstInit2(VXB_DEVICE_ID pDev);
LOCAL void gptInstConnect(VXB_DEVICE_ID pDev);
LOCAL void gptTimerInt(VXB_DEVICE_ID pDev);
LOCAL STATUS gptTimerAllocate
    (
    VXB_DEVICE_ID    pDev,
    UINT32        flags,
    void **        pCookie,
    UINT32        timerNo
    );

LOCAL STATUS gptTimerRelease
    (
    VXB_DEVICE_ID    pDev,
    FTGPT_TIMER *    pTimer
    );

LOCAL STATUS gptTimerRolloverGet
    (
    VXB_DEVICE_ID    pDev,
    UINT32 *    count
    );

LOCAL STATUS gptTimerCountGet
    (
    VXB_DEVICE_ID    pDev,
    UINT32 *    count
    );

LOCAL STATUS gptTimerDisable
    (
    VXB_DEVICE_ID    pDev
    );

LOCAL STATUS gptTimerEnable
    (
    VXB_DEVICE_ID    pDev,
    UINT32    maxTimerCount
    );
LOCAL STATUS gptTimerRolloverGet64
    (
    VXB_DEVICE_ID    pDev,
    UINT64 *    count
    );

LOCAL STATUS gptTimerCountGet64
    (
    VXB_DEVICE_ID    pDev,
    UINT64 *    count
    );
LOCAL STATUS gptTimerEnable64
    (
    VXB_DEVICE_ID    pDev,
    UINT64    maxTimerCount
    );
LOCAL STATUS gptTimerCycleISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    );

LOCAL STATUS gptTimerSpecialISRSet
    (
    VXB_DEVICE_ID    pDev,
    TimerTachoEventType type,
    void    (*pFunc)(int),
    int        arg
    );
LOCAL STATUS gptTachoOverISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    );
    
LOCAL STATUS gptTachoUnderISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    );
    
LOCAL STATUS gptTimerRollOverISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    );
    
LOCAL STATUS gptTimerOnceISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    );
LOCAL STATUS gptTimerOnceEnable(VXB_DEVICE_ID pDev);
LOCAL STATUS gptTimerOnceDisable(VXB_DEVICE_ID pDev); 
LOCAL STATUS gptTimerCaptureISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    );
LOCAL void gptTachoOverLimitSet(VXB_DEVICE_ID pDev, UINT32 overLimit);
LOCAL void gptTachoUnderLimitSet(VXB_DEVICE_ID pDev, UINT32 underLimit);
LOCAL UINT32 gptTachoOverLimitGet(VXB_DEVICE_ID pDev);
LOCAL UINT32 gptTachoUnderLimitGet(VXB_DEVICE_ID pDev);
LOCAL STATUS gptTachoDisable(VXB_DEVICE_ID    pDev);
LOCAL STATUS gptTachoEnable(VXB_DEVICE_ID    pDev);
LOCAL void gptTachoDefaultOver(int arg);
LOCAL void gptTachoDefaultUnder(int arg);
LOCAL UINT32 gptTachoFanRPM(VXB_DEVICE_ID    pDev);
LOCAL UINT32 gptCaptureCntGet(VXB_DEVICE_ID    pDev);



/* structure to store the driver functions for vxBus */

LOCAL struct drvBusFuncs ftGptTimerDrvFuncs =
    {
    gptInstInit,      /* devInstanceInit */
    gptInstInit2,     /* devInstanceInit2 */
    gptInstConnect    /* devConnect */
    };

/* instance methods */

LOCAL device_method_t    ftGptTimerDrv_methods[] =
    {
    {0, NULL}
    };

/* structure used for registration with vxbus */

LOCAL struct vxbDevRegInfo ftGptTimerDrvRegistration =
    {
    NULL,                        /* pNext */
    VXB_DEVID_DEVICE,            /* devID */
    VXB_BUSID_PLB,               /* busID = PLB */
    VXB_VER_4_0_0,               /* vxbVersion */
    FTGPT_TIMER_NAME,         /* drvName */
    &ftGptTimerDrvFuncs,      /* pDrvBusFuncs */
    NULL,                        /* pMethods */
    NULL                         /* devProbe */
    };

/* local declarations */

/*******************************************************************************
*
* vxbGptTimerDrvRegister - register SOC timer driver
*
* This routine registers the SOC timer driver with the vxBus subsystem.
*
* RETURNS: N/A
*
* ERRNO: none
*/

void vxbGptTimerDrvRegister (void)
    {

    /* call the vxBus routine to register the timer driver */

    vxbDevRegister (&ftGptTimerDrvRegistration);
    }


/*******************************************************************************
*
* gptTimerModeInit - first level initialization routine of timer device
*
* This is the function called to perform the first level initialization of
* the timer device.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.  It may, however, call hwMemAlloc().
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptTimerModeInit
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER *    pTimer;
    HCF_DEVICE *    pHcf;
    struct vxbTimerFunctionality * pTimerFunc;
    FUNCPTR     pFunc = NULL;


    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

    /* check if memory allocation is successful */

    if (pTimer == NULL)
        return;


    pTimer->maxCount = FTGPT_TIMER_MAX_COUNT;
    pTimer->maxCount64 = FTGPT_TIMER_MAX_COUNT64;

    /* locate the timer functionality data structure */

    pTimerFunc = &(pTimer->timerFunc);


    /* get the HCF device from the hwconf */

    pHcf = hcfDeviceGet (pDev);

    if (
        /* if pHcf is NULL, no data is present in hwconf.c for this device */

        (pHcf == NULL) ||

        /* get the clock frequency */

        /*
         * resourceDesc {
         * The clkFreq resource specifies the frequency of the clock
         * supplied to the timer. }
         */

        (devResourceGet (pHcf, "clkFreq", HCF_RES_ADDR,
                         ((void *)&pFunc)) != OK) ||

        /* get the compare type */

        /*
         * resourceDesc {
         * The cmpType resource specifies the compare type: cycle or one-time. }
         */

        (devResourceGet (pHcf, "cmpType", HCF_RES_INT,
                         ((void *)&(pTimer->cmpType))) != OK) 

        )
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *)pTimer);
#endif /* !_VXBUS_BASIC_HWMEMLIB */
        return;
        }
    else
        {
        pTimerFunc->clkFrequency = (*pFunc)();
        }


    if (TIMER_CYC_CMP == pTimer->cmpType)
        {
        if (
            
            /* get the minimum and maximum clock rates */

            /*
             * resourceDesc {
             * The clkRateMin resource specifies the minimum
             * frequency to be supported. }
             */

            (devResourceGet (pHcf, "clkRateMin", HCF_RES_INT,
                             ((void *)&(pTimerFunc->minFrequency))) != OK) ||

            /*
             * resourceDesc {
             * The clkRateMax resource specifies the maximum
             * frequency to be supported. }
             */

            (devResourceGet (pHcf, "clkRateMax", HCF_RES_INT,
                             ((void *)&(pTimerFunc->maxFrequency))) != OK)
            )
            {
#ifndef _VXBUS_BASIC_HWMEMLIB
            hwMemFree ((char *)pTimer);
#endif /* !_VXBUS_BASIC_HWMEMLIB */
            return;
            }
        }
        
    /* set the default ticks per second */

    pTimerFunc->ticksPerSecond = DEFAULT_TICKS_PER_SECOND;

    /* calculate the maximum possible timer rollover period */

    pTimerFunc->rolloverPeriod = FTGPT_TIMER_MAX_COUNT / pTimerFunc->clkFrequency;

    /* for manual testing of the timer, copy the name of the timer */

    strncpy (pTimerFunc->timerName, ftGptTimerDrvRegistration.drvName,
             MAX_DRV_NAME_LEN);

    /* populate the function pointers */

    pTimerFunc->timerAllocate     = gptTimerAllocate;
    pTimerFunc->timerRelease      = gptTimerRelease;
    pTimerFunc->timerISRSet       = gptTimerCycleISRSet;
    pTimerFunc->timerDisable      = gptTimerDisable;

    if (TIMER_32_BITS == pTimer->timerBits)
        {
        pTimerFunc->timerCountGet     = gptTimerCountGet;
        pTimerFunc->timerEnable       = gptTimerEnable;
        pTimerFunc->timerRolloverGet  = gptTimerRolloverGet;
        pTimerFunc->timerCountGet64     = NULL;
        pTimerFunc->timerEnable64       = NULL;
        pTimerFunc->timerRolloverGet64  = NULL;
        }

    if (TIMER_64_BITS == pTimer->timerBits)
        {
        pTimerFunc->timerCountGet     = NULL;
        pTimerFunc->timerEnable       = NULL;
        pTimerFunc->timerRolloverGet  = NULL;
        pTimerFunc->timerCountGet64     = gptTimerCountGet64;
        pTimerFunc->timerEnable64       = gptTimerEnable64;
        pTimerFunc->timerRolloverGet64  = gptTimerRolloverGet64;
        }
    
    gptTimerAllocate(pDev,0,NULL,0);
    return;
    }


/*******************************************************************************
*
* gptTachoModeInit - first level initialization routine of timer device: tachoMode
*
* This is the function called to perform the first level initialization of
* the timer device.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.  It may, however, call hwMemAlloc().
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptTachoModeInit
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER *    pTimer;
    HCF_DEVICE *    pHcf;

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

    /* get the HCF device from the hwconf */

    pHcf = hcfDeviceGet (pDev);

    if (
        /* if pHcf is NULL, no data is present in hwconf.c for this device */

        (pHcf == NULL) ||

        /* get the edgeMode */

        /*
         * resourceDesc {
         * The edgeMode resource specifies the edgeMode
         * }
         */

        (devResourceGet (pHcf, "edgeMode", HCF_RES_INT,
                         ((void *)&(pTimer->edgeMode))) != OK) ||

        /*
         * resourceDesc {
         * The debounce resource specifies the debounce
         * . }
         */

        (devResourceGet (pHcf, "debounce", HCF_RES_INT,
                         ((void *)&(pTimer->debounce))) != OK) ||

        /*
         * resourceDesc {
         * The pulsNum resource specifies the pulsNum
         * . }
         */

        (devResourceGet (pHcf, "pulsNum", HCF_RES_INT,
                         ((void *)&(pTimer->pulsNum))) != OK) ||

        /*
         * resourceDesc {
         * The captureCnt resource specifies the debounce
         * . }
         */

        (devResourceGet (pHcf, "captureCnt", HCF_RES_INT,
                         ((void *)&(pTimer->captureCnt))) != OK)
        )
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *)pTimer);
#endif /* !_VXBUS_BASIC_HWMEMLIB */
        return;
        }

    if (0 == pDev->unitNumber)
        {
          /*PWM0_IN*/
          pinBank0Pwm00_In();
        }


    return;
    }

/*******************************************************************************
*
* gptInstInit - first level initialization routine of timer/tacho/capture device
*
* This is the function called to perform the first level initialization of
* the timer device.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.  It may, however, call hwMemAlloc().
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptInstInit
    (
    VXB_DEVICE_ID    pDev
    )
    {
    const struct hcfDevice * pHcf;
    FTGPT_TIMER *    pTimer;
    struct vxbTimerFunctionalityEx * pTimerFuncEx;

    /* check for valid parameter */

    if (pDev == NULL)
        return;
    
    /* Always use the unit number allocated to us in the hwconf file. */

    pHcf = hcfDeviceGet (pDev);
    vxbInstUnitSet (pDev, pHcf->devUnit);
    
    /* allocate the memory for the timer structure */

    pTimer = (FTGPT_TIMER *) hwMemAlloc (sizeof (FTGPT_TIMER));

    /* check if memory allocation is successful */

    if (pTimer == NULL)
        return;

    bzero((char*)pTimer, sizeof (FTGPT_TIMER));

    if (
        /* if pHcf is NULL, no data is present in hwconf.c for this device */

        (pHcf == NULL) ||


        /* get the workMode */

        /*
         * resourceDesc {
         * The workMode resource specifies the workMode. }
         */

        (devResourceGet (pHcf, "workMode", HCF_RES_INT,
                         ((void *)&(pTimer->workMode))) != OK) ||
        /*
         * resourceDesc {
         * The timerBits resource specifies the timerBits. }
         */

        (devResourceGet (pHcf, "timerBits", HCF_RES_INT,
                         ((void *)&(pTimer->timerBits))) != OK)
         
        )
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *)pTimer);
#endif /* !_VXBUS_BASIC_HWMEMLIB */
        return;
        }

    pTimer->runMode = TIMER_RESTART;

    /* initialize the spinlock */

    SPIN_LOCK_ISR_INIT (&pTimer->spinLock, 0);

    /* store the vxBus device pointer in the timer driver structure */

    pTimer->pDev = pDev;

    /* also store the timer driver pointer in the vxBus device structure */

    pDev->pDrvCtrl = pTimer;

    pTimer->gptBar = pDev->pRegBase[0];
    vxbRegMap(pDev, 0, &pTimer->gptHandle);

    /* publish methods */

    pDev->pMethods = ftGptTimerDrv_methods;

    /*CLK_LOG("GP Timer-%d Bar: 0x%x \r\n", 
            pDev->unitNumber, pTimer->gptBar,3,4,5,6);*/
            
    pTimerFuncEx = &pTimer->timerFuncEx;

    /* once time interfaces */
    pTimerFuncEx->timerOnceISRSet  = gptTimerOnceISRSet;
    pTimerFuncEx->timerOnceEnable  = gptTimerOnceEnable;
    pTimerFuncEx->timerOnceDisable = gptTimerOnceDisable;

    /* tacho interfaces */
    pTimerFuncEx->timerTachoOverLimitSet  = gptTachoOverLimitSet;
    pTimerFuncEx->timerTachoUnderLimitSet = gptTachoUnderLimitSet;
    pTimerFuncEx->timerTachoOverLimitGet  = gptTachoOverLimitGet;
    pTimerFuncEx->timerTachoUnderLimitGet = gptTachoUnderLimitGet;
	pTimerFuncEx->timerTachoOverISRSet    = gptTachoOverISRSet;
	pTimerFuncEx->timerTachoUnderISRSet   = gptTachoUnderISRSet;
	pTimerFuncEx->timerTachoDisable       = gptTachoDisable;
	pTimerFuncEx->timerTachoEnable        = gptTachoEnable;
	pTimerFuncEx->timerTachoRPM           = gptTachoFanRPM;

	/* rollover interfaces */
	pTimerFuncEx->timerRollOverISRSet = gptTimerRollOverISRSet;

	/* capture interfaces */
	pTimerFuncEx->timerCaptureISRSet = gptTimerCaptureISRSet;
	pTimerFuncEx->timerCaptureCntGet = gptCaptureCntGet;
	

    if (TIMER_WORK_MODE_TIMER == pTimer->workMode)
    {
        gptTimerModeInit(pDev);
    }
    else if ((TIMER_WORK_MODE_TACHO == pTimer->workMode) ||
             (TIMER_WORK_MODE_CAPTURE == pTimer->workMode))
    {
        gptTachoModeInit(pDev);
    }
    else
    {
        CLK_LOG("GP Timer-%d Init failed \r\n", 
            pDev->unitNumber, 2,3,4,5,6);
    }
    
    return;
    }

/*******************************************************************************
*
* gptTimerModeInit2 - second level initialization routine of timer device
*
* This routine performs the second level initialization of the timer device.
*
* This routine is called later during system initialization.  OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptTimerModeInit2
    (
    VXB_DEVICE_ID    pDev
    )
    {
    /* connect the ISR to the timer interrupt */

    if (vxbIntConnect (pDev, 0, gptTimerInt, (void *) pDev) != OK)
        {
        pDev->pDrvCtrl = NULL;
        }
    else
        {
        FTGPT_TIMER * pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

        pTimer->intConnected = TRUE;
        }

    return;
    }


/*******************************************************************************
*
* gptTachoModeInit2 - second level initialization routine of timer device
*
* This routine performs the second level initialization of the timer device.
*
* This routine is called later during system initialization.  OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptTachoModeInit2
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER * pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    UINT32 RegVal = 0;

    if (TIMER_WORK_MODE_TACHO == pTimer->workMode)
    {
        RegVal |= TIMER_REG_TACHO_MODE_TACHO;
        
        /* plus num of rpm calculate period */
        
        CSR_WRITE_4(pDev, TIMER_CMP_VALUE_LOW_REG, pTimer->pulsNum);
    }
    else if (TIMER_WORK_MODE_CAPTURE == pTimer->workMode)
    {
        RegVal |= TIMER_REG_TACHO_MODE_CAPTURE;
        
        /* set capture cnt to assert capture intr */
        RegVal |= TIMER_REG_TACHO_CAPTURE_CNT & 
                (pTimer->captureCnt << TIMER_REG_TACHO_CAPTURE_CNT_SHIFT);
    }
    else 
    {
        CLK_LOG("should not come here.\r\n", 1,2,3,4,5,6);
        return ;
    }

    /* set timer bits */
    if (TIMER_32_BITS == pTimer->timerBits)
    {
        RegVal &= (~ TIMER_REG_CNT_SERIES_64BIT);
    }
    else if (TIMER_64_BITS == pTimer->timerBits)
    {
        RegVal |= TIMER_REG_CNT_SERIES_64BIT;
    }
    else
    {
        CLK_LOG("invalid timerBits input \r\n", 1,2,3,4,5,6);
        return ;
    }

    /* set edge mode */
    if (TACHO_FALLING_EDGE == pTimer->edgeMode)
    {
        RegVal |= TACHO_REG_MODE_FALLING_EDGE;
    }
    else if (TACHO_RISING_EDGE == pTimer->edgeMode)
    {
        RegVal |= TACHO_REG_MODE_RISING_EDGE;
    }
    else if (TACHO_DOUBLE_EDGE == pTimer->edgeMode)
    {
        RegVal |= TACHO_REG_MODE_DOUBLE_EDGE;
    }
    else
    {
        CLK_LOG("invalid input for tacho init2\r\n" ,1,2,3,4,5,6);
        return ;
    }    

    /* set jitter level */
    RegVal |= TACHO_REG_ANTI_JITTER_MASK & 
              (pTimer->debounce << TACHO_REG_ANTI_JITTER_SHIFT);

    CSR_WRITE_4(pDev, TIMER_CTRL_REG, RegVal);
    
    return;
    }


/*******************************************************************************
*
* gptInstInit2 - second level initialization routine of timer device
*
* This routine performs the second level initialization of the timer device.
*
* This routine is called later during system initialization.  OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptInstInit2
    (
    VXB_DEVICE_ID    pDev
    )
    {

    FTGPT_TIMER * pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

    if (TIMER_WORK_MODE_TIMER == pTimer->workMode)
    {
        gptTimerModeInit2(pDev);
    }
    else if ((TIMER_WORK_MODE_TACHO == pTimer->workMode) ||
             (TIMER_WORK_MODE_CAPTURE == pTimer->workMode))
    {
        gptTachoModeInit2(pDev);
    }
    else
    {
        CLK_LOG("GP Timer-%d Init2 failed \r\n", 
            pDev->unitNumber, 2,3,4,5,6);
    }
    
    return;
    }


/*******************************************************************************
*
* gptInstConnect - third level initialization routine of timer device
*
* This is the function called to perform the third level initialization of
* the timer device.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void gptInstConnect
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER * pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

    /* tacho interrupts  */
    if (TIMER_WORK_MODE_TACHO == pTimer->workMode)
    {
        gptTachoOverLimitSet(pDev, TIMER_TACHO_DEFAULT_MAX);
        gptTachoUnderLimitSet(pDev, TIMER_TACHO_DEFAULT_MIN);

        gptTimerSpecialISRSet(pDev, TACHO_EVENT_OVER, gptTachoDefaultOver, pDev);
        gptTimerSpecialISRSet(pDev, TACHO_EVENT_UNDER, gptTachoDefaultUnder, pDev);

    /* connect the ISR to the timer interrupt */

    if (vxbIntConnect (pDev, 0, gptTimerInt, (void *) pDev) != OK)
        {
        pDev->pDrvCtrl = NULL;
        }
    else
        {
        FTGPT_TIMER * pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

        pTimer->intConnected = TRUE;
        }

        
    }

    return;
    }

/*******************************************************************************
*
* gptTimerAllocate -  a timer hardware init
*
* This is the function called to initialize timer config register
*
* RETURNS: OK or ERROR if parameter failed.
*
* ERRNO: N/A
*/

LOCAL STATUS gptTimerAllocate
    (
    VXB_DEVICE_ID    pDev,
    UINT32        flags,
    void **        pCookie,
    UINT32        timerNo
    )
    {
    FTGPT_TIMER *    pTimer;
    UINT32 value;
    UINT32  timeout = 0;

    pTimer = (FTGPT_TIMER *)(pDev->pDrvCtrl);
    if (pTimer == NULL)
    {
        CLK_LOG("%s,%d timer%d null pTimer\r\n", __func__, __LINE__, pDev->unitNumber,4,5,6);
        return ERROR;
    }

    /* soft reset GPT timer */
    CLK_LOG("reset GP TIMER-%d ...", pDev->unitNumber,2,3,4,5,6);
    CSR_SETBIT_4 (pDev, TIMER_CTRL_REG, TIMER_CTRL_RESET_SHIFT);
    do
    {
        value = CSR_READ_4(pDev, TIMER_CTRL_REG);
        timeout++;
    }while((value & TIMER_CTRL_RESET_SHIFT) && (timeout < 5000));
    CLK_LOG("Soft reset GPT timer Finished\r\n", 1,2,3,4,5,6);

    /* init timer mode, cycle/once, bit width  */

    /*value = CSR_READ_4(pDev, TIMER_CTRL_REG);*/
    value = 0; /* bit[1:0]work mode: 00:Timer (01:tachometer; 10:capture; 11:reserve) */

    if (TIMER_RESTART == pTimer->runMode)
    {
        value |= TIMER_CTRL_TIMER_CNT_MODE_SHIFT; /* counter mode: 1:Restart  */
    }
    else if (TIMER_FREE_RUN == pTimer->runMode)
    {
        value &= ~TIMER_CTRL_TIMER_CNT_MODE_SHIFT; /* counter mode: 0:free run  */
    }
    else
    {
        CLK_LOG(" Bad run mode:%d\r\n", pTimer->runMode,2,3,4,5,6);
    }
        
    if (TIMER_CYC_CMP == pTimer->cmpType)
    {
        value &= ~TIMER_CTRL_TIMER_MODE_SHIFT;    /* 0:cyc */
    }
    else if (TIMER_ONCE_CMP == pTimer->cmpType)
    {
        value |=  TIMER_CTRL_TIMER_MODE_SHIFT;      /* 1:once */
    }
    else
    {
        CLK_LOG(" Bad cmpare type:%d\r\n", pTimer->cmpType,2,3,4,5,6);
    }

    if (TIMER_32_BITS == pTimer->timerBits)
    {
        value &= ~TIMER_CTRL_BIT_SET_SHIFT;  /* 0:32bit */
    }
    else if (TIMER_64_BITS == pTimer->timerBits)
    {
        value |= TIMER_CTRL_BIT_SET_SHIFT;  /* 1:64bit */
    }
    else
    {
        CLK_LOG(" Bad timer bit width:%d\r\n", pTimer->timerBits,2,3,4,5,6);
    }
    
    
    value |= TIMER_CTRL_CNT_CLR_SHIFT; /* 1:clear counter(0: not clear) */
    CSR_WRITE_4(pDev, TIMER_CTRL_REG, value);

    /* reset the timer compare register (32 and 64 bit) */

    CSR_WRITE_4(pDev, TIMER_CMP_VALUE_LOW_REG, 0);
    CSR_WRITE_4(pDev, TIMER_CMP_VALUE_UP_REG, 0);

    CSR_WRITE_4(pDev, TIMER_START_VALUE_REG, 0); /* start value (OK both in 32&64 mode) */
    value = CSR_READ_4(pDev, TIMER_CTRL_REG);
    value |= TIMER_CTRL_FORCE_SHIFT;
    CSR_WRITE_4(pDev, TIMER_CTRL_REG, value); /* force to load start value */

    return OK;
    }

/*******************************************************************************
*
* gptTimerRelease - release the timer resource
*
* This is the function called to release a timer device
*
* RETURNS: OK or ERROR
*
* ERRNO
*/

LOCAL STATUS gptTimerRelease
    (
    VXB_DEVICE_ID    pDev,
    FTGPT_TIMER *    pTimer
    )
    {

    /* validate the parameters */

    if (pDev == NULL)
        {
        return ERROR;
        }

    if (pTimer->pDev != pDev)
        {
        return ERROR;
        }

    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);

    /* disable the timer */

    gptTimerDisable (pTimer);

    pTimer->pIsrFunc[TIMER_EVENT_CYC_CMP] = NULL;
    pTimer->arg[TIMER_EVENT_CYC_CMP] = 0;

    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);

    return OK;
    }

/*******************************************************************************
*
* gptTimerRolloverGet - retrieve the maximum value of the counter
*
* This is the function called to retrieve the maximum value of the counter.
* The maximum value is returned in 'pCount' parameter.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTimerRolloverGet
    (
    VXB_DEVICE_ID    pDev,
    UINT32 *    pCount
    )
    {
    FTGPT_TIMER *    pTimer;

    if ((pDev == NULL) || (pCount == NULL))
        {
        return ERROR;
        }
    
    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;
    /* validate the parameters */

    if (pTimer == NULL)
        {
        return ERROR;
        }

    *pCount = pTimer->maxCount;

    return OK;
    }

/*******************************************************************************
*
* gptTimerRolloverGet64 - retrieve the maximum value of the counter(64bit)
*
* This is the function called to retrieve the maximum value of the counter.
* The maximum value is returned in 'pCount' parameter.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTimerRolloverGet64
    (
    VXB_DEVICE_ID    pDev,
    UINT64 *    pCount
    )
    {
    FTGPT_TIMER *    pTimer;

    if ((pDev == NULL) || (pCount == NULL))
        {
        return ERROR;
        }
    
    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;

    /* validate the parameters */

    if (pTimer == NULL)
        {
        return ERROR;
        }

    *pCount = pTimer->maxCount64;

    return OK;
    }
/*******************************************************************************
*
* gptTimerCountGet - retrieve the current value of the counter
*
* This function is used to retrieve the current value of the counter.
* The current value is returned in 'pCount' parameter.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTimerCountGet
    (
    VXB_DEVICE_ID    pDev,
    UINT32 *    pCount
    )
    {
    UINT32              count;
    FTGPT_TIMER *    pTimer;

    if ((pDev == NULL) || (pCount == NULL))
        {
        return ERROR;
        }
    
    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;

    /* validate the parameters */

    if (pTimer == NULL)
        {
        return ERROR;
        }

    /* the timer counter is incrementing */

    count = CSR_READ_4 (pDev, TIMER_CNT_VALUE_LOW_REG); /* 32bit */

    *pCount = count;

    return OK;
    }

/*******************************************************************************
*
* gptTimerCountGet64 - retrieve the current value of the counter(64bit)
*
* This function is used to retrieve the current value of the counter.
* The current value is returned in 'pCount' parameter.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTimerCountGet64
    (
    VXB_DEVICE_ID    pDev,
    UINT64 *    pCount
    )
    {
    UINT32           countLow, countUp;
    FTGPT_TIMER *    pTimer;

    if ((pDev == NULL) || (pCount == NULL))
        {
        return ERROR;
        }
    
    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;

    /* validate the parameters */

    if (pTimer == NULL)
        {
        return ERROR;
        }

    /* the timer counter is incrementing */

    countLow = CSR_READ_4 (pDev, TIMER_CNT_VALUE_LOW_REG); /* 32bit */
    countUp = CSR_READ_4 (pDev, TIMER_CNT_VALUE_UP_REG); /* 32bit */

    *pCount = ((UINT64)countUp<<32) | countLow;

    return OK;
    }

/*******************************************************************************
*
* gptTimerCycleISRSet - set a function to be called on the timer interrupt
*
* This function is called to set a function which can be called whenever
* the timer interrupt occurs.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTimerCycleISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    )
    {
    FTGPT_TIMER *    pTimer;

    /* validate the parameters */

    if ((pDev == NULL)||(pFunc == NULL))
        {
        return ERROR;
        }

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    if (pTimer == NULL)
        {
        return ERROR;
        }

    /* take the spinlock to update pIsrFunc and arg atomically */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);

    /* store the interrupt routine and argument information */

    pTimer->pIsrFunc[TIMER_EVENT_CYC_CMP] = pFunc;
    pTimer->arg[TIMER_EVENT_CYC_CMP] = arg;

    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);

    return OK;
    }

/*******************************************************************************
*
* gptTimerDisable - disable the timer interrupt
*
* This function is called to disable the timer interrupt.
*
* RETURNS: OK or ERROR if timer is not disabled
*
* ERRNO
*/

LOCAL STATUS gptTimerDisable
    (
    VXB_DEVICE_ID    pDev
    
    )
    {
    FTGPT_TIMER *    pTimer;
    UINT32  value;
    int i;

    /* validate the parameters */

    if (pDev == NULL)
        {
        return ERROR;
        }


    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

    /* disable the timer interrupt */

    if ((pTimer->intConnected) &&
    (vxbIntDisable (pDev, 0, gptTimerInt, (void *) pDev) != OK))
        {
        return ERROR;
        }

    /* reset the timer control register */

    gptTimerAllocate(pDev,0,NULL,0);

    /* disable compare interrupt */

    CSR_CLRBIT_4 (pDev, TIMER_INT_MASK_REG, TIMER_INT_CYC_COMP_SHIFT /*BIT(4)*/);

    pTimer->maxCount = FTGPT_TIMER_MAX_COUNT;
    pTimer->maxCount64 = FTGPT_TIMER_MAX_COUNT64;

    pTimer->isEnabled = FALSE;

    return OK;
    }

/*******************************************************************************
*
* gptTimerEnable - enable the timer interrupt
* @maxTimerCount: It is compare value. The max value to trigger interrupt.
*
* This function enables the timer interrupt.
*
* RETURNS: OK or ERROR if timer is not enabled
*
* ERRNO
*/

LOCAL STATUS gptTimerEnable
    (
    VXB_DEVICE_ID    pDev,
    UINT32    maxTimerCount
    )
    {
    FTGPT_TIMER *    pTimer;

    /* validate the parameters */

    if (pDev == NULL)
        {
        return ERROR;
        }

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    if (pTimer->isEnabled)
        {
        (void) gptTimerDisable (pTimer);
        }

    /* enable the timer interrupt */

    if ((pTimer->intConnected) &&
    (vxbIntEnable (pDev, 0, gptTimerInt, (void *) pDev) != OK))
        {
        return ERROR;
        }

    /* update the timer compare register with maxTimerCount */

    pTimer->cmpVal = maxTimerCount;

    /*
     * The counter had been reset to zero in timer disable
     * or timer allocate function.
     */

    CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_LOW_REG, pTimer->cmpVal);

    /* enable cycle compare interrupt */

    CSR_SETBIT_4 (pDev, TIMER_INT_MASK_REG, TIMER_INT_CYC_COMP_SHIFT /*BIT(4)*/);

    /* enable counter to start */
    CSR_SETBIT_4 (pDev, TIMER_CTRL_REG, TIMER_CTRL_CNT_EN_SHIFT); 

    pTimer->isEnabled = TRUE;

    return OK;
    }

/*******************************************************************************
*
* gptTimerEnable64 - enable the timer interrupt(64bit)
* @maxTimerCount: It is compare value. The max value to trigger interrupt.
*
* This function enables the timer interrupt.
*
* RETURNS: OK or ERROR if timer is not enabled
*
* ERRNO
*/

LOCAL STATUS gptTimerEnable64
    (
    VXB_DEVICE_ID    pDev,
    UINT64    maxTimerCount
    )
    {
    FTGPT_TIMER *    pTimer;

    /* validate the parameters */

    if (pDev == NULL)
        {
        return ERROR;
        }

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    if (pTimer->isEnabled)
        {
        (void) gptTimerDisable (pTimer);
        }

    /* enable the timer interrupt */

    if ((pTimer->intConnected) &&
    (vxbIntEnable (pDev, 0, gptTimerInt, (void *) pDev) != OK))
        {
        return ERROR;
        }

    /* update the timer compare register with maxTimerCount */

    pTimer->cmpVal64 = maxTimerCount;
    pTimer->cmpValCurrent64 = pTimer->cmpVal64; /*save it for workaround*/

    /*
     * The counter had been reset to zero in timer disable
     * or timer allocate function.
     */

    CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_LOW_REG, (UINT32)(pTimer->cmpVal64 & 0xFFFFFFFF));
    CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_UP_REG, (UINT32)((pTimer->cmpVal64 >> 32)& 0xFFFFFFFF));


    /* enable cycle compare interrupt */

    CSR_SETBIT_4 (pDev, TIMER_INT_MASK_REG, TIMER_INT_CYC_COMP_SHIFT /*BIT(4)*/);

    /* enable counter to start */
    CSR_SETBIT_4 (pDev, TIMER_CTRL_REG, TIMER_CTRL_CNT_EN_SHIFT); 

    pTimer->isEnabled = TRUE;

    return OK;
    }

/*******************************************************************************
*
* gptTimerInt - ISR for the General Purpose Timer
*
* This routine handles the GP Timer interrupt. 
*
* RETURNS : N/A
*/

LOCAL void gptTimerInt
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER *    pTimer;
    VOIDFUNCPTR      handler;
    UINT32           value;
    UINT32           loop;

    
    /* validate the parameter */

    if (pDev == NULL)
        return;

    /*CLK_LOG("Enter GPT%d int ...\r\n", pDev->unitNumber,2,3,4,5,6);*/

    /* retrieve the pTimer */

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    if (pTimer == NULL)
        return;

    /* acknowledge interrupt: any write to Clear Register clears interrupt */
    value = CSR_READ_4 (pDev, TIMER_INT_STAT_REG);

    /* call the ISR hooked by ..ISRSet() */
    for (loop = 0; loop < MAX_TIMER_TACHO_EVENT; loop++)
        {
        handler = pTimer->pIsrFunc[loop];
        if ((handler != NULL) && (value & BIT(loop)))
            {
            handler (pTimer->arg[loop]);
            }
        }

    CSR_WRITE_4 (pDev, TIMER_INT_STAT_REG, value);


     if ((TIMER_CYC_CMP == pTimer->cmpType)
        && (TIMER_32_BITS == pTimer->timerBits)
        && (TIMER_FREE_RUN == pTimer->runMode))
        {
        UINT32 low, lowNext;
        low = CSR_READ_4(pDev, TIMER_CMP_VALUE_LOW_REG);
        lowNext = low + pTimer->cmpVal;
        CSR_WRITE_4(pDev, TIMER_CMP_VALUE_LOW_REG, lowNext);
        }


       if ((TIMER_CYC_CMP == pTimer->cmpType)
           && (TIMER_64_BITS == pTimer->timerBits)
           && (TIMER_FREE_RUN == pTimer->runMode))
        {
        UINT64 cmpValCurrent64, cmpValNext64;
        UINT32 low, up, lowNext,upNext;

        cmpValCurrent64 = pTimer->cmpValCurrent64;
        low = (UINT32)(cmpValCurrent64 & 0xFFFFFFFF);
        up = (UINT32)((cmpValCurrent64>>32) & 0xFFFFFFFF);
        
        cmpValNext64 = cmpValCurrent64  + pTimer->cmpVal64;
        lowNext = (UINT32)(cmpValNext64 & 0xFFFFFFFF);
        upNext = (UINT32)((cmpValNext64>>32) & 0xFFFFFFFF);


        CSR_WRITE_4(pDev, TIMER_CMP_VALUE_LOW_REG, lowNext);
        /*CSR_WRITE_4(pDev, TIMER_CMP_VALUE_UP_REG, upNext);*/

        if (upNext >  up)
            {
                UINT32 reg;

                /* disable counter */
                reg = CSR_READ_4(pDev, TIMER_CTRL_REG);
                reg &= ~ TIMER_CTRL_CNT_EN_SHIFT;
                CSR_WRITE_4(pDev, TIMER_CTRL_REG,  reg);

                CSR_WRITE_4(pDev, TIMER_CMP_VALUE_UP_REG, upNext);

                /* enable counter to start */
                reg |=  TIMER_CTRL_CNT_EN_SHIFT;
                CSR_WRITE_4(pDev, TIMER_CTRL_REG,  reg);

                /*uartf("=>RESET CTRL.....\r\n");
                uartf("[INT] current compare value = 0x%x_%08x; upNext=0x%x\r\n", up, low, upNext);
                */

            }

        
        pTimer->cmpValCurrent64 = cmpValNext64;
        
        }

    }

/*******************************************************************************
*
* ftGptDevGet - get VXB_DEVICE_ID ptr 
*
* This routine gets VXB_DEVICE_ID of general purpose timer.
*
* RETURNS : device pointer
*/
VXB_DEVICE_ID ftGptDevGet(UINT32 timerId)
{
    if (timerId>=GPT_TOTAL_COUNT)
    {
        CLK_LOG("General Perpus Timer %d is invalid.\r\n", timerId,2,3,4,5,6);
        return NULL;
    }
    
    return vxbInstByNameFind(FTGPT_TIMER_NAME,timerId);
}


/*******************************************************************************
*
* ftGptTimerFunctionalityGet - get struct vxbTimerFunctionality pointer 
*
* This routine gets struct  vxbTimerFunctionality pointer of general purpose timer.
*
* RETURNS : TimerFunc pointer
*/
struct  vxbTimerFunctionality * ftGptTimerFunctionalityGet(UINT32 timerId, VXB_DEVICE_ID *ppDev)
{
    VXB_DEVICE_ID pDev;
    FTGPT_TIMER *    pTimer;
    struct  vxbTimerFunctionality * pTimerFunc;

    if (NULL == ppDev)
    {
        return NULL;
    }

    pDev = ftGptDevGet(timerId);
    if (NULL == pDev)
    {
        return NULL;
    }

    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;
    if (NULL == pTimer)
    {
        return NULL;
    }

    pTimerFunc = &(pTimer->timerFunc);
    *ppDev = pDev; /* output pDev */
    
    return pTimerFunc;
}



/*******************************************************************************
*
* ftGptTimerFunctionalityExGet - get struct vxbTimerFunctionalityEx pointer 
*
* This routine gets struct  vxbTimerFunctionalityEx pointer of general purpose timer.
*
* RETURNS : TimerFuncEx pointer
*/
struct  vxbTimerFunctionalityEx * ftGptTimerFunctionalityExGet(UINT32 timerId, VXB_DEVICE_ID *ppDev)
{
    VXB_DEVICE_ID pDev;
    FTGPT_TIMER *    pTimer;
    struct  vxbTimerFunctionalityEx * pTimerFuncEx;

    if (NULL == ppDev)
    {
        return NULL;
    }

    pDev = ftGptDevGet(timerId);
    if (NULL == pDev)
    {
        return NULL;
    }

    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;
    if (NULL == pTimer)
    {
        return NULL;
    }

    pTimerFuncEx = &(pTimer->timerFuncEx);
    *ppDev = pDev; /* output pDev */

    return pTimerFuncEx;
}

/*******************************************************************************
*
* gptTimerSpecialISRSet - set a function to be called on the timer interrupt
*
* This function is called to set a function which can be called whenever
* the timer interrupt occurs.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTimerSpecialISRSet
    (
    VXB_DEVICE_ID    pDev,
    TimerTachoEventType type,
    void    (*pFunc)(int),
    int        arg
    )
    {
    FTGPT_TIMER *    pTimer;

    /* validate the parameters */

    if (pFunc == NULL)
        {
        return ERROR;
        }

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    if (pTimer == NULL)
        {
        return ERROR;
        }

    /* take the spinlock to update pIsrFunc and arg atomically */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);

    /* store the interrupt routine and argument information */

    pTimer->pIsrFunc[type] = pFunc;
    pTimer->arg[type] = arg;

    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);

    return OK;
    }


LOCAL STATUS gptTimerOnceISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    )
    {
    return gptTimerSpecialISRSet(pDev, TIMER_EVENT_ONCE_CMP, pFunc, arg);
    }


/*******************************************************************************
*
* gptTimerOnceEnable - enable the timer interrupt
* @maxTimerCount: It is compare value. The max value to trigger interrupt.
*
* This function enables the timer interrupt.
*
* RETURNS: OK or ERROR if timer is not enabled
*
* ERRNO
*/

LOCAL STATUS gptTimerOnceEnable
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER *    pTimer;
    struct  vxbTimerFunctionality * pTimerFunc;
    struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    UINT32 maxTimeMs;

    /* validate the parameters */

    if (pDev == NULL)
        {
        return ERROR;
        }

    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;
    pTimerFunc = &pTimer->timerFunc;
    pTimerFuncEx = &pTimer->timerFuncEx;

    /* check time if out of range */
    
    if (TIMER_32_BITS == pTimer->timerBits)
        {
        maxTimeMs = (pTimer->maxCount / pTimerFunc->clkFrequency)*1000;
        if (pTimerFuncEx->cmpValOnceTimeMs > maxTimeMs)
            {
            CLK_LOG("Time is out of range! (%d>%d) Please use 64bit mode!\r\n", 
                pTimerFuncEx->cmpValOnceTimeMs,maxTimeMs,3,4,5,6);
            return ERROR;
            }
        }

    /* enable the timer interrupt */

    if ( (vxbIntEnable (pDev, 0, gptTimerInt, (void *) pDev) != OK))
    {
        return ERROR;
    }

    /* update the timer compare register for once time */

    if (TIMER_32_BITS == pTimer->timerBits)
        {
        pTimer->cmpVal = pTimerFunc->clkFrequency /1000 *pTimerFuncEx->cmpValOnceTimeMs;

        /*
         * The counter had been reset to zero in timer disable
         * or timer allocate function.
         */

        CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_LOW_REG, pTimer->cmpVal);
        CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_UP_REG, 0);
        }

    if (TIMER_64_BITS == pTimer->timerBits)
        {
        pTimer->cmpVal64 = ((UINT64)pTimerFunc->clkFrequency) /1000 *((UINT64)pTimerFuncEx->cmpValOnceTimeMs);

        /*
         * The counter had been reset to zero in timer disable
         * or timer allocate function.
         */

        CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_LOW_REG, (UINT32)(pTimer->cmpVal64 & 0xFFFFFFFF));
        CSR_WRITE_4 (pDev, TIMER_CMP_VALUE_UP_REG, (UINT32)((pTimer->cmpVal64 >> 32)& 0xFFFFFFFF));
        }

    /* enable once compare interrupt */

    CSR_SETBIT_4 (pDev, TIMER_INT_MASK_REG, TIMER_INT_ONE_COMP_SHIFT);

    /* enable counter to start */
    CSR_SETBIT_4 (pDev, TIMER_CTRL_REG, TIMER_CTRL_CNT_EN_SHIFT);

    return OK;
    }

/*******************************************************************************
*
* gptTimerDisable - disable the timer interrupt
*
* This function is called to disable the timer interrupt.
*
* RETURNS: OK or ERROR if timer is not disabled
*
* ERRNO
*/

LOCAL STATUS gptTimerOnceDisable
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER *    pTimer;

    /* validate the parameters */

    if (pDev == NULL)
        {
        return ERROR;
        }


    pTimer = (FTGPT_TIMER *) pDev->pDrvCtrl;

    /* disable the timer interrupt */

    if (vxbIntDisable (pDev, 0, gptTimerInt, (void *) pDev) != OK)
        {
        return ERROR;
        }

    /* reset the timer control register */

    gptTimerAllocate(pDev,0,NULL,0);

    /* disable compare interrupt */

    CSR_CLRBIT_4 (pDev, TIMER_INT_MASK_REG, TIMER_INT_ONE_COMP_SHIFT);

    pTimer->maxCount = FTGPT_TIMER_MAX_COUNT;
    pTimer->maxCount64 = FTGPT_TIMER_MAX_COUNT64;

    return OK;
    }


/*******************************************************************************
*
* gptTachoOverISRSet - set a function to be called on the tacho over run
*
* This function is called to set a function which can be called whenever
* the tacho over run interrupt occurs.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/

LOCAL STATUS gptTachoOverISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    )
    {
    return gptTimerSpecialISRSet(pDev, TACHO_EVENT_OVER, pFunc, arg);
    }
    
/*******************************************************************************
*
* gptTachoUnderISRSet - set a function to be called on the tacho under run
*
* This function is called to set a function which can be called whenever
* the tacho under run interrupt occurs.
*
* RETURNS: OK or ERROR if the parameter is invalid.
*
* ERRNO
*/
    
LOCAL STATUS gptTachoUnderISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    )
    {
    return gptTimerSpecialISRSet(pDev, TACHO_EVENT_UNDER, pFunc, arg);
    }
    
/*******************************************************************************
*
* gptTachoDefaultOver - default tacho over run function
*
* This function is called to show over message.
*
* RETURNS: NO.
*
* ERRNO
*/
LOCAL void gptTachoDefaultOver(int arg)
{
    logMsg("##Tacho default over handler...\r\n",1,2,3,4,5,6);
}

/*******************************************************************************
*
* gptTachoDefaultUnder - default tacho under run function
*
* This function is called to show under message.
*
* RETURNS: NO.
*
* ERRNO
*/
LOCAL void gptTachoDefaultUnder(int arg)
{
    logMsg("##Tacho default under handler...\r\n",1,2,3,4,5,6);
}

/*******************************************************************************
*
* gptTachoOverLimitSet - Set over limit value in tachometer mode. 
*
* This routine sets the over limit.
*
* RETURNS : RPM value
*/

LOCAL void gptTachoOverLimitSet(VXB_DEVICE_ID pDev, UINT32 overLimit)
{
    CSR_WRITE_4 (pDev, TIMER_TACHO_OVER_REG, overLimit);
}

/*******************************************************************************
*
* gptTachoUnderLimitSet - Set under limit value in tachometer mode. 
*
* This routine sets the under limit.
*
* RETURNS : RPM value
*/

LOCAL void gptTachoUnderLimitSet(VXB_DEVICE_ID pDev, UINT32 underLimit)
{
    CSR_WRITE_4 (pDev, TIMER_TACHO_UNDER_REG, underLimit);
}

/*******************************************************************************
*
* gptTachoOverLimitGet - Get over limit value in tachometer mode. 
*
* This routine gets the over limit.
*
* RETURNS : over limit value
*/

LOCAL UINT32 gptTachoOverLimitGet(VXB_DEVICE_ID pDev)
{
    return CSR_READ_4 (pDev, TIMER_TACHO_OVER_REG);
}

/*******************************************************************************
*
* gptTachoUnderLimitGet - Get under limit value in tachometer mode. 
*
* This routine gets the under limit.
*
* RETURNS : under limit value
*/

LOCAL UINT32 gptTachoUnderLimitGet(VXB_DEVICE_ID pDev)
{
    return CSR_READ_4 (pDev, TIMER_TACHO_UNDER_REG);
}


/*******************************************************************************
*
* gptTachoDisable - disable the tacho interrupt
*
* This function is called to disable the tacho interrupt.
*
* RETURNS: OK or ERROR if timer is not disabled
*
* ERRNO
*/

LOCAL STATUS gptTachoDisable
    (
    VXB_DEVICE_ID    pDev
    )
    {
    FTGPT_TIMER *    pTimer;

    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;
    if (pTimer == NULL)
        {
        return ERROR;
        }
    
    /* disable the timer interrupt */

    if ((pTimer->intConnected) &&
    (vxbIntDisable (pDev, 0, gptTimerInt, (void *) pDev) != OK))
        {
        return ERROR;
        }

    /* reset the timer control register */
    
    gptTimerAllocate(pDev,0,NULL,0);

    /* disable over/under interrupt */

    CSR_CLRBIT_4 (pDev, TIMER_INT_MASK_REG, 
       (TIMER_INT_TACHO_OVER_SHIFT | TIMER_INT_TACHO_UNDER_SHIFT));


    return OK;
    }

/*******************************************************************************
*
* gptTachoEnable - enable the tacho interrupt
* @maxTimerCount: It is compare value. The max value to trigger interrupt.
*
* This function enables the timer interrupt.
*
* RETURNS: OK or ERROR if timer is not enabled
*
* ERRNO
*/

LOCAL STATUS gptTachoEnable
    (
    VXB_DEVICE_ID    pDev
    )
    {

    FTGPT_TIMER *    pTimer;
    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;

    /* enable the timer interrupt */

    if ((pTimer->intConnected) &&
    (vxbIntEnable (pDev, 0, gptTimerInt, (void *) pDev) != OK))
        {
        return ERROR;
        }


    /* enable over/under interrupts */

    CSR_SETBIT_4 (pDev, TIMER_INT_MASK_REG, 
        (TIMER_INT_TACHO_OVER_SHIFT | TIMER_INT_TACHO_UNDER_SHIFT));


    /* enable tacho counter to start */
    CSR_SETBIT_4 (pDev, TIMER_CTRL_REG, 
         (TIMER_CTRL_TACHO_EN_SHIFT | TIMER_CTRL_CNT_EN_SHIFT));

 

    return OK;
    }


/*******************************************************************************
*
* gptTachoFanRPM - Get RPM value in tachometer mode. 
*
* This routine gets the Rounds Per Minute.
*
* RETURNS : RPM value
*/

LOCAL UINT32 gptTachoFanRPM(VXB_DEVICE_ID    pDev)
{
    FTGPT_TIMER *    pTimer;
    UINT32 loopCnt;
    UINT32 rawDat;
    UINT32 Rpm;
    UINT32 cntNum;
    UINT64 tmp;
    int rate;

    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;
    if (NULL == pTimer)
    {
        return (UINT32)ERROR;
    }

    cntNum = CSR_READ_4 (pDev, TIMER_CMP_VALUE_LOW_REG); /* tacho mode: CMPL is pulse_number */
    rate = sysClkRateGet();

    for (loopCnt = 0;; loopCnt++)
    {
        rawDat = CSR_READ_4 (pDev, TIMER_TACHO_RES_REG);
        /* wait for tacho result */
        if (rawDat & (UINT32)TACHO_REG_RESU_ISVALID)
        {
            break;
        }

        if (loopCnt > 30)
        {
            taskDelay(rate/20); /*50ms*/
        }
        else
        {
            taskDelay(rate/10);  /*100ms*/
        }

        if (loopCnt > 35)
        {
            break;
        }
    }

    if (loopCnt > 35)
    {
        CLK_LOG("Time out for RPM!\r\n",1,2,3,4,5,6);
        return ERROR;
    }

    rawDat &= TACHO_REG_RESU_MASK; 
    if (0 == rawDat)
    {
        Rpm = 0;
    }
    else
    {
        /* calculate rpm */
        tmp = ((UINT64)CLK_50MHZ * 60 * (UINT64)rawDat);
        Rpm = (UINT32)(tmp / (2 * (cntNum + 1)));
    }

    return Rpm;
}

LOCAL STATUS gptTimerRollOverISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    )
    {
    return gptTimerSpecialISRSet(pDev, TIMER_EVENT_ROLL_OVER, pFunc, arg);
    }

    
LOCAL STATUS gptTimerCaptureISRSet
    (
    VXB_DEVICE_ID    pDev,
    void    (*pFunc)(int),
    int        arg
    )
    {
    return gptTimerSpecialISRSet(pDev, TACHO_EVENT_CAPTURE, pFunc, arg);
    }

/*******************************************************************************
*
* gptCaptureCntGet - Get counter in capture mode. 
*
* This routine gets the captured counter value.
*
* RETURNS : counter
*/

LOCAL UINT32 gptCaptureCntGet(VXB_DEVICE_ID    pDev)
{
    FTGPT_TIMER *    pTimer;
    UINT32 capCnt;

    pTimer = (FTGPT_TIMER *)pDev->pDrvCtrl;
    if (NULL == pTimer)
    {
        return (UINT32)ERROR;
    }

    /* read cap cnt will clear it */
    capCnt = TACHO_REG_RESU_MASK & CSR_READ_4 (pDev, TIMER_TACHO_RES_REG);
    return capCnt;
}
/*******************************************************************************
*
* ftGptDump - dump timer registers 
*
* This routine is a debug function.
*
* RETURNS : N/A
*/

void ftGptDump(UINT32 timerId)
{
    UINT32 CtrlReg;
    BOOL is64Bit;

    VXB_DEVICE_ID    pDev;
    if (timerId >= GPT_TOTAL_COUNT)
    {
        printf("This timer id %d is not exist! \r\n", timerId);
        return;
    }
    
    pDev = ftGptDevGet(timerId);

    if(NULL == pDev)
    {
        printf("This timer device %d is not exist! \r\n", timerId);
        return;
    }

    CtrlReg = CSR_READ_4 (pDev, TIMER_CTRL_REG);
    is64Bit = ((CtrlReg & TIMER_CTRL_BIT_SET_SHIFT) != 0);

    printf("[00]ctrl: 0x%08x, bar=0x%x\r\n", CtrlReg, (UINT32)FT_GPT_BAR(pDev));
    printf(" BIT[31]tacho enable: %s\r\n", (CtrlReg & (UINT32)TIMER_CTRL_TACHO_EN_SHIFT)?
                                    "1: tacho en":"0: tacho dis");
    printf(" BIT[27]once timer: %s\r\n", (CtrlReg & TIMER_CTRL_TIMER_MODE_SHIFT)?
                                    "1: once":"0: repeat");
    printf(" BIT[26]clear cnt: %s\r\n", (CtrlReg & TIMER_CTRL_CNT_CLR_SHIFT)?
                                     "1: true":": not");
    printf(" BIT[25]timer enabled: %s\r\n", (CtrlReg & TIMER_CTRL_CNT_EN_SHIFT)?
                                       "1: enable":"0: disable");
    printf(" BIT[24]: %s timer\r\n", is64Bit?"1: 64bit":"0:32bit");
    
    printf(" BIT[1:0]timer mode: %d [0:timer; 1:tachometer; 2:capture; 3:RSV]\r\n", 
                                        (CtrlReg & 0x3));
    
    printf(" BIT[22]count mode: %s\r\n", (CtrlReg & TIMER_CTRL_TIMER_CNT_MODE_SHIFT)?
                                    "1:restart":"0: free run");
    printf(" BIT[2]in resetting: %s\r\n", (CtrlReg & TIMER_CTRL_RESET_SHIFT)?
                                    "1:resetting":"0:normal");
    printf(" BIT[3]force load: %s\r\n", (CtrlReg & TIMER_CTRL_FORCE_SHIFT)?
                                    "1: true":"0: not");
    
    printf("[28]intr mask:   0x%08x\r\n", CSR_READ_4 (pDev, TIMER_INT_MASK_REG));
    printf("[2c]intr status: 0x%08x\r\n", CSR_READ_4 (pDev, TIMER_INT_STAT_REG));
    
    printf("[38]start cnt: 0x%08x\r\n",  CSR_READ_4 (pDev, TIMER_START_VALUE_REG));
    if (is64Bit)
    {
        printf("cmp low:       0x%08x\r\n", CSR_READ_4 (pDev, TIMER_CMP_VALUE_LOW_REG));
        printf("cmp high:      0x%08x\r\n", CSR_READ_4 (pDev, TIMER_CMP_VALUE_UP_REG));
        printf("curr cnt: low: 0x%08x\r\n", CSR_READ_4 (pDev, TIMER_CNT_VALUE_LOW_REG));
        printf("curr cnt high: 0x%08x\r\n", CSR_READ_4 (pDev, TIMER_CNT_VALUE_UP_REG));
    }
    else
    {
        printf("cmp low:      0x%08x\r\n", CSR_READ_4 (pDev, TIMER_CMP_VALUE_LOW_REG));
        printf("cur cnt: low: 0x%08x\r\n", CSR_READ_4 (pDev, TIMER_CNT_VALUE_LOW_REG));
    }

}

