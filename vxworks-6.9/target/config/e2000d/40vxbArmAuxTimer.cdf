/* 40vxbArmAuxTimer.cdf - ARM aux timer configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */



Component   DRV_ARM_GEN_AUX_TIMER {
    NAME        ARM Aux Timer Driver
    SYNOPSIS    ARM Aux Timer Driver
    _CHILDREN   FOLDER_DRIVERS
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    armAuxTimerRegister();
    PROTOTYPE   void armAuxTimerRegister (void);
    REQUIRES    INCLUDE_PLB_BUS \
                INCLUDE_VXB_AUX_CLK
}

