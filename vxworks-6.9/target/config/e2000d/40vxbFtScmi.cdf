/* 40vxbFtScmi.cdf - phytium scmi configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   DRV_FTSCMI {
   NAME        FT SCMI driver
    SYNOPSIS    FT SCMI driver
    _CHILDREN   FOLDER_DRIVERS
    CONFIGLETTES 
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    vxbFtMhuRegister();
    PROTOTYPE   void vxbFtMhuRegister (void);
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS
   INIT_AFTER  INCLUDE_PLB_BUS
}

