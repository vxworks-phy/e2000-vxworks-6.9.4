/* vxbIeee1588.h - IEEE1588 header file for vxBus */

/*
 * Copyright (c) 2019 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
11jul19,yal  define common 1588 data structures (VXWPG-82)
25jan19,yal  written, add new VxBus ID VXB_BUSID_IEEE1588. (V7NET-2235)
*/

#ifndef __INCVXBIEEE1588H
#define __INCVXBIEEE1588H

#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>
#include <time.h>
#include <endCommon.h>

#ifdef __cplusplus
extern "C" {
#endif


#define IEEE1588_TIMERS_MAX     2

#ifdef _WRS_KERNEL
#include <spinLockLib.h>
#include <vxbTimerLib.h>
struct ieee1588TimerProperties
    {
    void            (*pIsrFunc)(_Vx_usr_arg_t); /* User callback func */
    _Vx_usr_arg_t   arg;                        /* User callback arg  */
    UINT32          uMaxTimerCount;             /* Max timer count */
                                                /* set by user */
    BOOL            autoReload;                 /* Is the timer auto reload */
    BOOL            bEnabled;                   /* Is the timer enabled */
    UINT64          tickStartTime;              /* The tick start time */
    };

typedef struct vxbIeee1588Dev
    {
    /* Timer function used by timerLib */

    struct vxbTimerFunctionality    timerFunc[IEEE1588_TIMERS_MAX];

    /* Timer properties */

    struct ieee1588TimerProperties  timerProp[IEEE1588_TIMERS_MAX];
    void *                          pOwnerCtrl;     /* The parent owner END */
                                                    /*controller  */
    spinlockIsr_t                   spinLock;       /* Spin Lock */
    UINT32                          max1588Timers;  /* max 1588 timers of */
                                                    /* END device */
    UINT64                          timeRecordUsec; /* Record the time */
    UINT64                          sysTimeLastRead; /* Record the last read */
                                                    /* of SYSTIMH/L */

    /* Timer APIs */

    STATUS (* getSysTime)(struct vxbIeee1588Dev *, struct timespec *);
    STATUS (* setSysTime)(struct vxbIeee1588Dev *, struct timespec *);
    void (* updateSysTime)(struct vxbIeee1588Dev *);
    STATUS (* getTxTimeStamp)(struct vxbIeee1588Dev *, struct timespec *);
    STATUS (* getRxTimeStamp)(struct vxbIeee1588Dev *, struct timespec *);
    STATUS (* adjustSysTime)(struct vxbIeee1588Dev *, INT32);
    STATUS (* adjustFreq)(struct vxbIeee1588Dev *, INT32);

    UINT8                        txOneStepOffset;   /* The Tx Stamp offset */
                                                    /* value */
    } IEEE1588_HARDWARE;

STATUS ieee1588SysTimeDelayGet
    (
    IEEE1588_HARDWARE * p1588DevInfo,
    long *              TimeDelay
    );

STATUS ieee1588SysTimeIntervalGet
    (
    IEEE1588_HARDWARE *     p1588MainTimer,
    IEEE1588_TIMER_SYNC *   sync
    );

STATUS ieee1588SysTimeIntervalSet
    (
    IEEE1588_HARDWARE *     p1588MainTimer,
    IEEE1588_TIMER_SYNC *   sync
    );

STATUS ieee1588SysTimeAdd
    (
    IEEE1588_HARDWARE * p1588DevInfo,
    INT64 *             counts
    );

STATUS ieee1588TimersMaxGet
    (
    void *      pCookie,
    UINT32 *    pTimerMax
    );

void ieee1588Tsc2Time
    (
    IEEE1588_HARDWARE * p1588DevInfo,
    UINT64              nsCounts,
    struct timespec *   ts
    );
#endif /* _WRS_KERNEL */

#define NSEC_PER_SEC            (1000000000ULL)
#define TIMESPEC_TO_NSEC(ts)    ((UINT64)(ts).tv_sec * NSEC_PER_SEC + \
                                 (UINT64)(ts).tv_nsec)

#ifdef __cplusplus
}
#endif

#endif /* __INCVXBIEEE1588H */
