/* vxbFtPwm.h - Driver for PWM controller*/

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbFtPwmh
#define __INCvxbFtPwmh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxbPwmDrv.h"

#define PWM_BASE_ADR(n)        (0x2804A000+(n<<12)) /* 0<=n<=7 */

#define PWM_INSTANCE_MAX 8
#define PWM_CHANNEL_0 0
#define PWM_CHANNEL_1 1
#define PWM_CHANNEL_MAX 2

#define PWM_DIV_MIN 1
#define PWM_DIV_MAX 4096

#define FIFO_DEPTH 8
#define NSEC_PER_SEC        (1000000000ULL)

#define REG_TCNT            0x00 /* timer count register */
#define REG_TCTRL           0x04 /* timer control register */
#define REG_STAT            0x08 /* status register */
#define REG_TPERIOD         0x0c /* timer period register */
#define REG_PWMCTRL         0x10 /* pwm control register */
#define REG_PWMCCR          0x14 /* pwm duty or capture register */

#define TCTRL_DIV_MASK      0x1ff8
#define TCTRL_PWMMOD_MASK     0x4
#define TCTRL_CAPMOD_MASK     0x3
#define PWM_PERIOD_MASK     0xffff
#define PWM_DUTY_MASK       0xffff
#define PWM_MODE_MASK       0x4
#define PWM_CTRL_INIT		0xc4

#define REG_DBCTRL          0x00 /* Dead Band Control Register */
#define REG_DBCLY           0x04 /* Dead Band Delay Cycle counter */
#define PWM_UPDBCLY_MASK   0x3ff
#define PWM_DWDBCLY_MASK   0xffc00
#define PWM_DB_POLARITY_MASK 0xc

#define DUTY_FIFO_DEEPTH 8



/* 
AH:  Active High.  Both pwm0 and pwm1 are original signals.
ALC: Active Low Complementary.   pwm0 inverse.
AHC: Active High Complementary.  pwm1 inverse.
AL:  Active Low.   Both pwm0 and pwm1 inverse.
*/
typedef enum
{
    DB_POL_AH = 0,
    DB_POL_ALC = 1,
    DB_POL_AHC = 2,
    DB_POL_AL = 3
}DB_POLARITY;

typedef enum  {
/* counter increase from 0 to TxCCR , reset to 0.        => /|/|/| */
    MODULO = 0,
    
/* counter increase from 0 to TxCCR, then decrease to 0. => /\/\/\ */
    UP_AND_DOWN,
}TIMER_COUNT_MODE;

typedef enum  {
    DUTY_FROM_REGISTER = 0,
    DUTY_FROM_FIFO
}DUTY_SOURCE_MODE;


                

/* A PWM Channel parameter struct. In general, used privately*/
typedef struct {
    TIMER_COUNT_MODE cntmod;
    DUTY_SOURCE_MODE dutymod;
    UINT32 div; /* clock divisor [1...4096] */
    
#ifdef PWM_SUPPORT_FIFO_MODE
    UINT32 dutyCCR[FIFO_DEPTH];
    UINT32 dutyIndex;
#endif
}CHANNEL_PARAM;

typedef struct ftPwmDrvCtrl
    {
    VXB_DEVICE_ID           pInst;       /* pointer to the controller instance */
    void *                  pwmHandle;      /* handle of PWM vxbRead/vxbWrite */

    UINT32 dbBase;
    UINT32 pwmBase0;
    UINT32 pwmBase1;
    UINT64 baseClk;
    UINT32 updbclyNs;
    UINT32 dwdbclyNs;
    DB_POLARITY dbpolarity;
    CHANNEL_PARAM chParam[PWM_CHANNEL_MAX];
    spinlockIsr_t spinlockIsr;  /* ISR-callable spinlock */
    
#if defined(PWM_SUPPORT_INTERRUPT) || defined(PWM_SUPPORT_FIFO_MODE)
    UINT32 intCount;
#endif
    /* published info for upper interface */
    struct vxbPwmInfo pwmInfo;
    } FT_PWM_DRVCTRL;          

/* dead band registers */

#define DB_READ_4(pCtrl, addr)                                \
    vxbRead32 ((pCtrl->pwmHandle),                             \
               (UINT32 *)((char *)(pCtrl->dbBase) + (addr)))

#define DB_WRITE_4(pCtrl, addr, data)                         \
    vxbWrite32 ((pCtrl->pwmHandle),                            \
                (UINT32 *)((char *)(pCtrl->dbBase) + (addr)), data)

/* channel registers */

#define CH_READ_4(pCtrl, ch, addr)                                \
    vxbRead32 ((pCtrl->pwmHandle),                             \
               (UINT32 *)((char *)(pCtrl->pwmBase0) + ((ch)*0x400)+ (addr)))

#define CH_WRITE_4(pCtrl, ch, addr, data)                         \
    vxbWrite32 ((pCtrl->pwmHandle),                            \
                (UINT32 *)((char *)(pCtrl->pwmBase0) + ((ch)*0x400)+ (addr)), data)
                
#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbFtPwmh */
