/* vxbFtGemEnd.h - FT E2000 GEM VxBus End header file */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#ifndef __INCvxbFtGemEndh
#define __INCvxbFtGemEndh

#ifdef __cplusplus
extern "C" {
#endif

#include <base/b_struct_timespec.h>
#if ((defined _WRS_KERNEL ) && (defined _WRS_CONFIG_IEEE1588_PTP_DEVICE))
#include <vxbIeee1588.h>
#endif

IMPORT void ftGemRegister (void);

#define GEM_1G_CLK             1 
#define GEM_10M_CLK            2 
#define GEM_100M_CLK           3 

#define MACB_MAX_QUEUES 8

/* Interface Mode definitions */
typedef enum {
	PHY_INTERFACE_MODE_NA,
	PHY_INTERFACE_MODE_INTERNAL,
	PHY_INTERFACE_MODE_MII,
	PHY_INTERFACE_MODE_GMII,
	PHY_INTERFACE_MODE_SGMII,
	PHY_INTERFACE_MODE_TBI,
	PHY_INTERFACE_MODE_REVMII,
	PHY_INTERFACE_MODE_RMII,
	PHY_INTERFACE_MODE_RGMII,
	PHY_INTERFACE_MODE_RGMII_ID,
	PHY_INTERFACE_MODE_RGMII_RXID,
	PHY_INTERFACE_MODE_RGMII_TXID,
	PHY_INTERFACE_MODE_RTBI,
	PHY_INTERFACE_MODE_SMII,
	PHY_INTERFACE_MODE_XGMII,
	PHY_INTERFACE_MODE_MOCA,
	PHY_INTERFACE_MODE_QSGMII,
	PHY_INTERFACE_MODE_TRGMII,
	PHY_INTERFACE_MODE_1000BASEX,
	PHY_INTERFACE_MODE_2500BASEX,
	PHY_INTERFACE_MODE_RXAUI,
	PHY_INTERFACE_MODE_XAUI,
	PHY_INTERFACE_MODE_USXGMII,
	PHY_INTERFACE_MODE_5GBASER,
	/* 10GBASE-KR, XFI, SFI - single lane 10G Serdes */
	PHY_INTERFACE_MODE_10GKR,
	PHY_INTERFACE_MODE_MAX,
} phyInterface_t;


#ifndef BSP_VERSION

/* Gem register offsets */

#define GEM_CTL      0x00    /* Network Control Register              */
#define GEM_CFG      0x04    /* Network Configuration Register        */
#define GEM_SR       0x08    /* Network Status Register               */
#define GEM_USRIO    0x0C    /* User IO                               */
#define GEM_DCFG     0x10    /* DMA Configuration                     */
#define GEM_TSR      0x14    /* Transmit Status Register              */
#define GEM_RBQP     0x18    /* Receive Buffer Queue Pointer          */
#define GEM_TBQP     0x1C    /* Transmit Address Register             */
#define GEM_RSR      0x20    /* Receive Status Register               */
#define GEM_ISR      0x24    /* Interrupt Status Register             */
#define GEM_IER      0x28    /* Interrupt Enable Register             */
#define GEM_IDR      0x2C    /* Interrupt Disable Register            */
#define GEM_IMR      0x30    /* Interrupt Mask Register               */
#define GEM_MAN      0x34    /* PHY Maintenance Register              */
#define GEM_JML	     0x0048 /* Jumbo Max Length */
#define GEM_HSMAC    0x0050 /* Hs mac config register*/

#define GEM_HSL      0x80    /* Hash Address Low[31:0]                */
#define GEM_HSH      0x84    /* Hash Address High[63:32]              */
#define GEM_SA1L     0x88    /* Specific Address 1 Low, First 4 bytes */
#define GEM_SA1H     0x8C    /* Specific Address 1 High, Last 2 bytes */
#define GEM_SA2L     0x90    /* Specific Address 2 Low, First 4 bytes */
#define GEM_SA2H     0x94    /* Specific Address 2 High, Last 2 bytes */
#define GEM_SA3L     0x98    /* Specific Address 3 Low, First 4 bytes */
#define GEM_SA3H     0x9C    /* Specific Address 3 High, Last 2 bytes */
#define GEM_SA4L     0xA0    /* Specific Address 4 Low, First 4 bytes */
#define GEM_SA4H     0xA4    /* Specific Address 4 High, Last 2 bytes */
#define GEM_TID1     0xA8    /* Type ID Checking Register             */
#define GEM_TID2     0xAC    /* Type ID Checking Register             */
#define GEM_TID3     0xB0    /* Type ID Checking Register             */
#define GEM_TID4     0xB4    /* Type ID Checking Register             */
#define GEM_WAKE     0xB8    /* Wake on LAN                           */
#define GEM_SAML     0xC8    /* Specific Address Mask Low, 4 bytes    */
#define GEM_SAMH     0xCC    /* Specific Address Mask High, 2 bytes   */

#define GEM_FRA      0x108   /* Frames Transmitted OK Register        */
#define GEM_TUND     0x134   /* Transmit Underrun Register            */
#define GEM_SCOL     0x138   /* Single Collision Frame Register       */
#define GEM_MCOL     0x13C   /* Multiple Collision Frame Register     */
#define GEM_ECOL     0x140   /* Excessive Collision Register          */
#define GEM_LCOL     0x144   /* Late Collision Register               */
#define GEM_DTE      0x148   /* Deferred Transmission Frame Register  */
#define GEM_CSE      0x14C   /* Carrier Sense Error Register          */
#define GEM_OK       0x158   /* Frames Received OK Register           */
#define GEM_USF      0x184   /* Undersize Frames Error Register       */
#define GEM_RJA      0x18C   /* Receive Jabbers Error Register        */
#define GEM_SEQE     0x190   /* Frame Check Sequence Error Register   */
#define GEM_ELE      0x194   /* Excessive Lenght Error Register       */
#define GEM_RSE      0x198   /* Receive Symbol Error Register         */
#define GEM_ALE      0x19C   /* Alignment Error Register              */
#define GEM_RRE      0x1A0   /* Receive Resource  Error Register      */
#define GEM_ROV      0x1A4   /* Receive Overrun Error Register        */

#define GEM_EFTSL		0x01e0 /* PTP Event Frame Tx Seconds Low */
#define GEM_EFTN		0x01e4 /* PTP Event Frame Tx Nanoseconds */
#define GEM_EFRSL		0x01e8 /* PTP Event Frame Rx Seconds Low */
#define GEM_EFRN		0x01ec /* PTP Event Frame Rx Nanoseconds */
#define GEM_PEFTSL		0x01f0 /* PTP Peer Event Frame Tx Secs Low */
#define GEM_PEFTN		0x01f4 /* PTP Peer Event Frame Tx Ns */
#define GEM_PEFRSL		0x01f8 /* PTP Peer Event Frame Rx Sec Low */
#define GEM_PEFRN		0x01fc /* PTP Peer Event Frame Rx Ns */
#define GEM_PCSCTRL     0x0200 /* PCS control register*/
#define GEM_PCSSTATUS   0x0204 /* PCS Status*/
#define GEM_PCSANLPBASE 0x0214 /* PCS an lp base */
#define GEM_PFCSTATUS   0x026c /* PFC status*/
#define GEM_DCFG1		0x0280 /* Design Config 1 */
#define GEM_DCFG2		0x0284 /* Design Config 2 */
#define GEM_DCFG3		0x0288 /* Design Config 3 */
#define GEM_DCFG4		0x028c /* Design Config 4 */
#define GEM_DCFG5		0x0290 /* Design Config 5 */
#define GEM_DCFG6		0x0294 /* Design Config 6 */
#define GEM_DCFG7		0x0298 /* Design Config 7 */
#define GEM_DCFG8		0x029C /* Design Config 8 */
#define GEM_DCFG10		0x02A4 /* Design Config 10 */

#define GEM_TXDESCRDBUFCTL0 0x9c0
#define GEM_TXDESCRDBUFCTL4 0x9c4


/* Screener Type 2 match registers */
#define GEM_SCRT2		0x540

/* EtherType registers */
#define GEM_ETHT		0x06E0

/* Type 2 compare registers */
#define GEM_T2CMPW0		0x0700
#define GEM_T2CMPW1		0x0704
#define T2CMP_OFST(t2idx)	(t2idx * 2)

/* type 2 compare registers
 * each location requires 3 compare regs
 */
#define GEM_IP4SRC_CMP(idx)		(idx * 3)
#define GEM_IP4DST_CMP(idx)		(idx * 3 + 1)
#define GEM_PORT_CMP(idx)		(idx * 3 + 2)

/* Which screening type 2 EtherType register will be used (0 - 7) */
#define SCRT2_ETHT		0

#define GEM_USX_CONTROL         0x0A80  /* High speed PCS control register */
#define GEM_USX_STATUS		    0x0A88 /* High speed PCS status register */
#define GEM_USX_FECERRCNT       0x0AD0 /* usx fec error counter */

#define GEM_SRC_SEL_LN          0x1C04
#define GEM_DIV_SEL0_LN         0x1C08
#define GEM_DIV_SEL1_LN         0x1C0C
#define GEM_PMA_XCVR_POWER_STATE  0x1C10
#define GEM_SPEED_MODE          0x1C14
#define GEM_MII_SELECT          0x1C18
#define GEM_SEL_MII_ON_RGMII    0x1C1C
#define GEM_TX_CLK_SEL0         0x1C20
#define GEM_TX_CLK_SEL1         0x1C24
#define GEM_TX_CLK_SEL2         0x1C28
#define GEM_TX_CLK_SEL3         0x1C2C
#define GEM_RX_CLK_SEL0         0x1C30
#define GEM_RX_CLK_SEL1         0x1C34
#define GEM_CLK_250M_DIV10_DIV100_SEL 0x1C38
#define GEM_TX_CLK_SEL5         0x1C3C
#define GEM_TX_CLK_SEL6         0x1C40
#define GEM_RX_CLK_SEL4         0x1C44
#define GEM_RX_CLK_SEL5         0x1C48
#define GEM_TX_CLK_SEL3_0       0x1C70
#define GEM_TX_CLK_SEL4_0         0x1C74
#define GEM_RX_CLK_SEL3_0         0x1C78
#define GEM_RX_CLK_SEL4_0         0x1C7C
#define GEM_RGMII_TX_CLK_SEL0     0x1C80
#define GEM_RGMII_TX_CLK_SEL1     0x1C84

#define GEM_PHY_INT_ENABLE        0x1C88
#define GEM_PHY_INT_CLEAR         0x1C8C
#define GEM_PHY_INT_STATE         0x1C90


/* Bitfields in NCR */
#define MACB_LB_OFFSET		0 /* reserved */
#define MACB_LB_SIZE		1
#define MACB_LLB_OFFSET		1 /* Loop back local */
#define MACB_LLB_SIZE		1
#define MACB_RE_OFFSET		2 /* Receive enable */
#define MACB_RE_SIZE		1
#define MACB_TE_OFFSET		3 /* Transmit enable */
#define MACB_TE_SIZE		1
#define MACB_MPE_OFFSET		4 /* Management port enable */
#define MACB_MPE_SIZE		1
#define MACB_CLRSTAT_OFFSET	5 /* Clear stats regs */
#define MACB_CLRSTAT_SIZE	1
#define MACB_INCSTAT_OFFSET	6 /* Incremental stats regs */
#define MACB_INCSTAT_SIZE	1
#define MACB_WESTAT_OFFSET	7 /* Write enable stats regs */
#define MACB_WESTAT_SIZE	1
#define MACB_BP_OFFSET		8 /* Back pressure */
#define MACB_BP_SIZE		1
#define MACB_TSTART_OFFSET	9 /* Start transmission */
#define MACB_TSTART_SIZE	1
#define MACB_THALT_OFFSET	10 /* Transmit halt */
#define MACB_THALT_SIZE		1
#define MACB_NCR_TPF_OFFSET	11 /* Transmit pause frame */
#define MACB_NCR_TPF_SIZE	1
#define MACB_TZQ_OFFSET		12 /* Transmit zero quantum pause frame */
#define MACB_TZQ_SIZE		1
#define MACB_SRTSM_OFFSET	15
#define MACB_OSSMODE_OFFSET 24 /* Enable One Step Synchro Mode */
#define MACB_OSSMODE_SIZE	1
#define MACB_PFC_OFFSET     25 /* Enable PFC */
#define MACB_PFC_SIZE	    1
#define MACB_RGMII_OFFSET   28
#define MACB_RGMII_SIZE     1
#define MACB_2PT5G_OFFSET   29
#define MACB_2PT5G_SIZE     1
#define MACB_HSMAC_OFFSET   31 /* Use high speed MAC */
#define MACB_HSMAC_SIZE     1

/* Bitfields in NCFGR */
#define MACB_SPD_OFFSET		0 /* Speed */
#define MACB_SPD_SIZE		1
#define MACB_FD_OFFSET		1 /* Full duplex */
#define MACB_FD_SIZE		1

/* GEM specific NCFGR bitfields. */
#define GEM_GBE_OFFSET		10 /* Gigabit mode enable */
#define GEM_GBE_SIZE		1
#define GEM_PCSSEL_OFFSET	11
#define GEM_PCSSEL_SIZE		1
#define GEM_CLK_OFFSET		18 /* MDC clock division */
#define GEM_CLK_SIZE		3
#define GEM_DBW_OFFSET		21 /* Data bus width */
#define GEM_DBW_SIZE		2
#define GEM_RXCOEN_OFFSET	24
#define GEM_RXCOEN_SIZE		1
#define GEM_SGMIIEN_OFFSET	27
#define GEM_SGMIIEN_SIZE	1

/* Constants for data bus width. */
#define GEM_DBW32		0 /* 32 bit AMBA AHB data bus width */
#define GEM_DBW64		1 /* 64 bit AMBA AHB data bus width */
#define GEM_DBW128		2 /* 128 bit AMBA AHB data bus width */

/* Bitfields in TISUBN */
#define GEM_SUBNSINCR_OFFSET			0
#define GEM_SUBNSINCRL_OFFSET			24
#define GEM_SUBNSINCRL_SIZE			    8
#define GEM_SUBNSINCRH_OFFSET			0
#define GEM_SUBNSINCRH_SIZE			    16
#define GEM_SUBNSINCR_SIZE			    24

/* GEM specific constants for CLK. */
#define GEM_CLK_DIV8				0
#define GEM_CLK_DIV16				1
#define GEM_CLK_DIV32				2
#define GEM_CLK_DIV48				3
#define GEM_CLK_DIV64				4
#define GEM_CLK_DIV96				5
#define GEM_CLK_DIV128              6
#define GEM_CLK_DIV224              7


/* Capability mask bits */
#define MACB_CAPS_ISR_CLEAR_ON_WRITE		0x00000001
#define MACB_CAPS_USRIO_HAS_CLKEN		0x00000002
#define MACB_CAPS_USRIO_DEFAULT_IS_MII_GMII	0x00000004
#define MACB_CAPS_NO_GIGABIT_HALF		0x00000008
#define MACB_CAPS_USRIO_DISABLED		0x00000010
#define MACB_CAPS_JUMBO				0x00000020
#define MACB_CAPS_GEM_HAS_PTP			0x00000040
#define MACB_CAPS_BD_RD_PREFETCH		0x00000080
#define MACB_CAPS_NEEDS_RSTONUBR		0x00000100
#define MACB_CAPS_SEL_CLK			0x00000200
#define MACB_CAPS_PERFORMANCE_OPTIMIZING	0x00000400
#define MACB_CAPS_FIFO_MODE			0x10000000
#define MACB_CAPS_GIGABIT_MODE_AVAILABLE	0x20000000
#define MACB_CAPS_SG_DISABLED			0x40000000
#define MACB_CAPS_MACB_IS_GEM			0x80000000


/*GEM PCS status register bitfields*/
#define GEM_LINKSTATUS_OFFSET    2 
#define GEM_LINKSTATUS_SIZE      1

/*GEM hs mac config register bitfields*/
#define GEM_HSMACSPEED_OFFSET    0 
#define GEM_HSMACSPEED_SIZE      3
/*GEM pcs_an_lp_base register bitfields*/
#define GEM_SGMIISPEED_OFFSET    10
#define GEM_SGMIISPEED_SIZE      2
#define GEM_SGMIIDUPLEX_OFFSET    12
#define GEM_SGMIIDUPLEX_SIZE      1

/*GEM pcs control register bitfields*/
#define GEM_AUTONEG_OFFSET    12
#define GEM_AUTONEG_SIZE      1
/*pcs_an_lp_base register bitfields*/
#define GEM_SPEEDR_OFFSET 10
#define GEM_SPEEDR_SIZE   2
#define GEM_DUPLEX_OFFSET 12
#define GEM_DUPLEX_SIZE   1

/* Bitfields in USX_CONTROL. */
#define GEM_SIGNAL_OK_OFFSET			0
#define GEM_SIGNAL_OK_SIZE			    1
#define GEM_TX_EN_OFFSET			    1
#define GEM_TX_EN_SIZE				    1
#define GEM_RX_SYNC_RESET_OFFSET       2
#define GEM_RX_SYNC_RESET_SIZE          1
#define GEM_FEC_ENABLE_OFFSET           4
#define GEM_FEC_ENABLE_SIZE             1
#define GEM_FEC_ENA_ERR_IND_OFFSET      5
#define GEM_FEC_ENA_ERR_IND_SIZE        1
#define GEM_TX_SCR_BYPASS_OFFSET		8
#define GEM_TX_SCR_BYPASS_SIZE			1
#define GEM_RX_SCR_BYPASS_OFFSET		9
#define GEM_RX_SCR_BYPASS_SIZE			1
#define GEM_SERDES_RATE_OFFSET			12
#define GEM_SERDES_RATE_SIZE			2
#define GEM_USX_CTRL_SPEED_OFFSET		14
#define GEM_USX_CTRL_SPEED_SIZE			3

/* Bitfields in GEM_USX_STATUS. */
#define GEM_BLOCK_LOCK          0x00000001  /*link up*/

/* Bitfields in DCFG1. */
#define GEM_IRQCOR_OFFSET			23
#define GEM_IRQCOR_SIZE				1
#define GEM_DBWDEF_OFFSET			25
#define GEM_DBWDEF_SIZE				3

/* LSO settings */
#define MACB_LSO_UFO_ENABLE			0x01
#define MACB_LSO_TSO_ENABLE			0x02

/* USXGMII/SGMII/RGMII speed */
#define GEM_SPEED_100   0
#define GEM_SPEED_1000  1
#define GEM_SPEED_2500  2
#define GEM_SPEED_5000  3
#define GEM_SPEED_10000 4
#define GEM_SPEED_25000 5

#define MACB_SERDES_RATE_5G		0
#define MACB_SERDES_RATE_10G	1

/* Bit manipulation macros */
#define MACB_BIT(name)					\
	(1 << MACB_##name##_OFFSET)
#define MACB_BF(name,value)				\
	(((value) & ((1 << MACB_##name##_SIZE) - 1))	\
	 << MACB_##name##_OFFSET)
#define MACB_BFEXT(name,value)\
	(((value) >> MACB_##name##_OFFSET)		\
	 & ((1 << MACB_##name##_SIZE) - 1))
#define MACB_BFINS(name,value,old)			\
	(((old) & ~(((1 << MACB_##name##_SIZE) - 1)	\
		    << MACB_##name##_OFFSET))		\
	 | MACB_BF(name,value))
	 
#define GEM_BIT(name)					\
	(1 << GEM_##name##_OFFSET)
#define GEM_BF(name, value)				\
	(((value) & ((1 << GEM_##name##_SIZE) - 1))	\
	 << GEM_##name##_OFFSET)
#define GEM_BFEXT(name, value)\
	(((value) >> GEM_##name##_OFFSET)		\
	 & ((1 << GEM_##name##_SIZE) - 1))
#define GEM_BFINS(name, value, old)			\
	(((old) & ~(((1 << GEM_##name##_SIZE) - 1)	\
		    << GEM_##name##_OFFSET))		\
	 | GEM_BF(name, value))

#define FIRST_FRAME_IN_PACKAGE   0
#define COMPOSE_FRAME_TO_PACKAGE 1

/* rx/tx buffer descriptors definitions */

#define CL_OVERHEAD             4          /* prepended cluster overhead */
#define CL_ALIGNMENT            4          /* cluster required alignment */
#define MBLK_ALIGNMENT          4          /* mBlks required alignment */
#define GEM_END_BD_ALIGN        0x40       /* required alignment for RBDs */
#define TX_UNDERRUN             0x10000000 /* transmite under run */

/* Bit assignments for Receive Buffer Descriptor */

/* Address - Word 0 */

#define RXBUF_ADD_BASE_MASK         0xfffffffc /* Base address */
#define RXBUF_ADD_WRAP              0x00000002 /* The last buffer in the ring */
#define RXBUF_ADD_OWNED             0x00000001 /* SW or MAC owns the pointer */

/* Status - Word 1 */

#define RXBUF_STAT_BCAST            0x80000000 /* Global broadcast address */
#define RXBUF_STAT_MULTI            0x40000000 /* Multicast hash match*/
#define RXBUF_STAT_UNI              0x20000000 /* Unicast hash match */
#define RXBUF_STAT_EXT              0x10000000 /* External address */
#define RXBUF_STAT_UNK              0x08000000 /* Unknown source address */
#define RXBUF_STAT_LOC1             0x04000000 /* Specific address 1 match */
#define RXBUF_STAT_LOC2             0x02000000 /* Specific address 2 match */
#define RXBUF_STAT_LOC3             0x01000000 /* Specific address 3 match */
#define RXBUF_STAT_CHECKSUM_BIT1    0x00800000 /* checksum indicate 1 */
#define RXBUF_STAT_CHECKSUM_BIT0    0x00400000 /* checksum indicate 0 */
#define RXBUF_STAT_VLAN             0x00200000 /* VLAN Detected */
#define RXBUF_STAT_PRIOR            0x00100000 /* Priority Tag Detected */
#define RXBUF_STAT_PRIOR_MASK       0x000e0000 /* VLAN Priority Field */
#define RXBUF_STAT_CFI              0x00010000 /* Concatenation indicator */
#define RXBUF_STAT_EOF              0x00008000 /* Buffer is end of frame */
#define RXBUF_STAT_SOF              0x00004000 /* Buffer is start of frame */
#define RXBUF_STAT_FCS              0x00002000 /* Bad FCS */
#define RXBUF_STAT_LEN_MASK         0x00001fff /* Length of frame */
#define RXBUF_STAT_JLEN_MASK        0x00003fff /* Length of Jumbo frame */


/* Bit assignments for Transmit Buffer Descriptor */

/* Status - Word 1 */

#define TXBUF_STAT_USED             0x80000000 /* Successfully transmited */
#define TXBUF_STAT_WRAP             0x40000000 /* Wrap (last buffer in list) */
#define TXBUF_STAT_RETRY_LIMIT      0x20000000
#define TXBUF_STAT_UNDERRUN         0x10000000 /* Transmit Underrun */
#define TXBUF_STAT_BUFF_EXHAUSTED   0x08000000
#define TXBUF_STAT_LATE_COLLISION   0x04000000
#define TXBUF_STAT_CKSUM_ERR_MASK   0x00700000
#define TXBUF_STAT_NO_CRC           0x00010000 /* Do not transmit CRC */
#define TXBUF_STAT_LAST_BUFF        0x00008000
#define TXBUF_STAT_LEN_MASK         0x00003fff /* Length of frame */

/* GEM register definitions */

/* Control Register, GEM_CTL, Offset 0x0  */

#define GEM_CTL_LB             0x00000001     /* Set Loopback output signal */
#define GEM_CTL_LBL            0x00000002     /* Loopback local.*/
#define GEM_CTL_RE             0x00000004     /* Receive enable.*/
#define GEM_CTL_TE             0x00000008     /* Transmit enable.*/
#define GEM_CTL_MPE            0x00000010     /* Management port enable.*/
#define GEM_CTL_CSR            0x00000020     /* Clear statistics registers.*/
#define GEM_CTL_ISR            0x00000040     /* Open statistics registers */
#define GEM_CTL_WES            0x00000080     /* Enable statistics registers */
#define GEM_CTL_BP             0x00000100     /* Force collision */
#define GEM_CTL_TSTART         0x00000200     /* TX Start */
#define GEM_CTL_THALT          0x00000400     /* TX Stop */
#define GEM_CTL_TPAU           0x00000800     /* TX Pause Frame */
#define GEM_CTL_TZQPAU         0x00001000     /* TX Pause ZQ Frame */
#define GEM_CTL_RXSTMP         0x00008000     /* RX Timestamp */
#define GEM_CTL_PFCEN          0x00010000     /* PFC Enable */
#define GEM_CTL_FLUSH_DPRAM    0x00020000     /* Flush next packet in RX DPRAM*/

/* Configuration Register, GEM_CFG, Offset 0x4 */

#define GEM_CFG_SPD            0x00000001    /* 10/100 Speed */
#define GEM_CFG_FD             0x00000002    /* Full duplex.*/
#define GEM_CFG_JFRAME         0x00000008    /* Jumbo Frames UNSUPPORTED */
#define GEM_CFG_CAF            0x00000010    /* Accept all frames */
#define GEM_CFG_NBC            0x00000020    /* No recept broadcast frames */
#define GEM_CFG_MTI            0x00000040    /* Multicast hash enable */
#define GEM_CFG_UNI            0x00000080    /* Unicast hash enable. */
#define GEM_CFG_BIG            0x00000100    /* Open 802.3 1522 byte frames */
#define GEM_CFG_GIG            0x00000400    /* Gigabit mode enable */
#define GEM_CFG_RTY            0x00001000    /* Retry Test Mode */
#define GEM_CFG_PAE            0x00002000    /* Pause Enable */
#define GEM_CFG_RBOF_0         0x00000000    /* No offset */
#define GEM_CFG_RBOF_1         0x00004000    /* One Byte offset */
#define GEM_CFG_RBOF_2         0x00008000    /* Two Byte offset */
#define GEM_CFG_RBOF_3         0x0000c000    /* Three Byte offset */
#define GEM_CFG_RLCE           0x00010000    /* Checking Enable */
#define GEM_CFG_DRFCS          0x00020000    /* Discard Receive FCS */
#define GEM_CFG_CLK_8          0x00000000    /* CLK divided by 8 */
#define GEM_CFG_CLK_16         0x00040000    /* CLK divided by 16 */
#define GEM_CFG_CLK_32         0x00080000    /* CLK divided by 32 */
#define GEM_CFG_CLK_48         0x000C0000    /* CLK divided by 48 */
#define GEM_CFG_CLK_64         0x00100000    /* CLK divided by 64 */
#define GEM_CFG_CLK_96         0x00140000    /* CLK divided by 96 */
#define GEM_CFG_CLK_128        0x00180000    /* CLK divided by 128 */
#define GEM_CFG_CLK_224        0x001C0000    /* CLK divided by 224 */
#define GEM_CFG_DBUS_32        0x00000000    /* 32 bit Data Bus Width */
#define GEM_CFG_DBUS_64        0x00200000    /* 64 bit Data Bus Width */
#define GEM_CFG_DBUS_128       0x00400000    /* 128 bit Data Bus Width */
#define GEM_CFG_CAFDIS         0x00800000    /* Disable Copy Paused Frames */
#define GEM_CFG_RXOL           0x01000000    /* RX Checksum Offload */
#define GEM_CFG_EFRHD          0x02000000    /* Allow Half Duplex RX */
#define GEM_CFG_IRXFCS         0x04000000    /* Ignore RX FCS */
#define GEM_CFG_SGMII          0x08000000    /* SGMII Enable */
#define GEM_CFG_IPGST          0x10000000    /* IPG Stretch Enable */
#define GEM_CFG_BDPRE          0x20000000    /* RX Bad Preamble */
#define GEM_CFG_IPGIG          0x40000000    /* Ignore IPG RX Error */
#define GEM_CFG_UNIDIR         0x80000000    /* Uni-direction Enable */

/* Status Register, GEM_SR, Offset 0x8 */

#define GEM_LINK               0x00000001    /* Link pin */
#define GEM_MDIO               0x00000002    /* Real Time state of MDIO pin */
#define GEM_IDLE               0x00000004    /* Logic is idle or running */
#define GEM_PCSDUP             0x00000008    /* PCS Auto-neg duplex */
#define GEM_PCSRX              0x00000010    /* PCS Auto-neg pause RX */
#define GEM_PCSTX              0x00000020    /* PCS Auto-neg pause TX */
#define GEM_PCSPRI             0x00000040    /* PCS Auto-neg priority based  */

/* DMA Configuration Register, GEM_DCFG, Offset 0x10 */

#define GEM_DMA_BLEN_1         0x00000001    /* Single AHB Burst */
#define GEM_DMA_BLEN_2         0x00000002    /* Single AHB Burst */
#define GEM_DMA_BLEN_4         0x00000004    /* INCR4 AHB Burst */
#define GEM_DMA_BLEN_8         0x00000008    /* INCR8 AHB Burst */
#define GEM_DMA_BLEN_16        0x00000010    /* INCR16 AHB Burst */
#define GEM_DMA_SWAP           0x00000040    /* Mngmt Endian Swap mode */
#define GEM_DMA_ENDIAN         0x00000080    /* Packet Endian Swap mode */
#define GEM_DMA_RBUF           0x00000300    /* RX Packet Buffer Size */
#define GEM_DMA_TBUF           0x00000400    /* TX Packet Buffer Size */
#define GEM_DMA_TCKSUM         0x00000800    /* TX Checksum Offload Enable */
#define GEM_DMA_RDMABUF        0x00FF0000    /* RX DMA Buffer Size mask */
#define GEM_DMA_RDMALEN        0x00180000    /* RX DMA Buffer Size=1536 */
#define GEM_DMA_JUMBO_RDMALEN  0x00A00000    /* RX DMA Buffer Size=10240 */
#define GEM_DMA_RDISC          0x01000000    /* RX Discard Pkts if no AHB */

/* Transmit Status Register, GEM_TSR, Offset 0x14 */

#define GEM_TSR_OVR            0x00000001    /* Transmit buffer overrun */
#define GEM_TSR_COL            0x00000002    /* Collision occured */
#define GEM_TSR_RLE            0x00000004    /* Retry lmimt exceeded */
#define GEM_TSR_TXGO           0x00000008    /* Transmitter is idle */
#define GEM_TSR_BNQ            0x00000010    /* Transmit buffer not queued */
#define GEM_TSR_COMP           0x00000020    /* Transmit complete */
#define GEM_TSR_UND            0x00000040    /* Transmit underrun */
#define GEM_TSR_LCOL           0x00000080    /* Transmit late collision */
#define GEM_TSR_HRSP           0x00000100    /* Transmit HRESP not Ok */

/* Receive Status Register, GEM_RSR, Offset 0x20  */

#define GEM_RSR_BNA            0x00000001    /* Buffer not available */
#define GEM_RSR_REC            0x00000002    /* Frame received */
#define GEM_RSR_OVR            0x00000004    /* Receive overrun */
#define GEM_RSR_HRSP           0x00000008    /* Receive HRESP not Ok */

/*
 * Interrupt Status Register,  GEM_ISR, Offsen 0x24
 * Interrupt Enable Register,  GEM_IER, Offset 0x28
 * Interrupt Disable Register, GEM_IDR, Offset 0x2c
 * Interrupt Mask Register,    GEM_IMR, Offset 0x30
 */

#define GEM_INT_DONE           0x00000001    /* Phy management done */
#define GEM_INT_RCOM           0x00000002    /* Receive complete */
#define GEM_INT_RBNA           0x00000004    /* Receive buffer not available */
#define GEM_INT_TBNA           0x00000008
#define GEM_INT_TUND           0x00000010    /* Transmit buffer underrun */
#define GEM_INT_RTRY           0x00000020    /* Transmit Retry limt */
#define GEM_INT_TBRE           0x00000040
#define GEM_INT_TCOM           0x00000080    /* Transmit complete */
#define GEM_INT_LCHG           0x00000200    /* Link Change */
#define GEM_INT_ROVR           0x00000400    /* Receive overrun */
#define GEM_INT_HRESP_NOT_OK   0x00000800    /* Hresp not OK */
#define GEM_INT_PFRRX          0x00001000    /* Pause Non-zero Quantum RX */
#define GEM_INT_PTZ            0x00002000    /* Pause Time zero */
#define GEM_INT_PTX            0x00004000    /* Pause Frame TX */
#define GEM_INT_ETX            0x00008000    /* External Interrupt */
#define GEM_INT_ANEG           0x00010000    /* Auto-neg complete */
#define GEM_INT_PRX            0x00020000    /* Link Partner RX */
#define GEM_INT_RDEL           0x00040000    /* PTP RX Delay */
#define GEM_INT_RSYNC          0x00080000    /* PTP RX Sync */
#define GEM_INT_TRQDEL         0x00100000    /* PTP Delay Req TX */
#define GEM_INT_TSYNC          0x00200000    /* PTP Sync Frame TX */
#define GEM_INT_RPDEL_REQ      0x00400000    /* PTP RX PDelay request */
#define GEM_INT_RPDEL_RSP      0x00800000    /* PTP RX PDelay response */
#define GEM_INT_TPDEL_REQ      0x01000000    /* PTP TX PDelay request */
#define GEM_INT_TPDEL_RSP      0x02000000    /* PTP TX PDelay response */
#define GEM_INT_TSU            0x04000000    /* TSU incriment */

/* PHY Maintenance Register, GEM_MAN, Offset 0x34 */

#define GEM_MAN_DATA(x)        ((x & 0xFFFF) <<  0) /* PHY data register */
#define GEM_MAN_CODE           (0x2 << 16)          /* IEEE Code */
#define GEM_MAN_REGA(x)        ((x & 0x1F) << 18)   /* PHY register address */
#define GEM_MAN_PHYA(x)        ((x & 0x1F) << 23)   /* PHY address */
#define GEM_MAN_WRITE          (0x1 << 28)          /* Transfer is a write */
#define GEM_MAN_READ           (0x2 << 28)          /* Transfer is a read */
#define GEM_MAN_SOF             0x40000000          /* Must be set RAB?? */

/* JUMBO MAX LEN */

#define GEM_JUMBO_MAX_LEN           10240

#define GEM_MTU                1500
#define GEM_JUMBO_MTU          9000
#define GEM_CLSIZE             1536
#define GEM_NAME               "gem"
#define GEM_TIMEOUT            10000

#define GEM_INT_RX              (GEM_INT_RCOM | GEM_INT_RBNA | GEM_INT_ROVR |\
                                GEM_INT_HRESP_NOT_OK)
#define GEM_INT_TX              (GEM_INT_TUND | GEM_INT_RTRY | GEM_INT_TCOM |\
                                GEM_INT_TBRE | GEM_INT_HRESP_NOT_OK)
#define GEM_INT_TX_RX           (GEM_INT_TX | GEM_INT_RX)

#define GEM_TUPLE_CNT          (576*8) /* 1152  --> 576x8 */
#define GEM_ADJ(x)             (x)->m_data += 2

#define GEM_INC_DESC(x, y)     (x) = (((x) + 1) % y)
#define GEM_MAXFRAG            32
#define GEM_MAX_RX             32

#define GEM_RX_DESC_CNT        192
#define GEM_TX_DESC_CNT        192

#define GEM_RX_BUFF_SIZE       1536

/* CRC for logical address filter */

#define GEM_CRC_TO_FILTER_INDEX(crc)   ((crc) >> 26)   /* get 6 MSBits */

#define SPEED_10		10
#define SPEED_100		100
#define SPEED_1000		1000
#define SPEED_2500		2500
#define SPEED_5000		5000
#define SPEED_10000		10000
#define SPEED_14000		14000
#define SPEED_20000		20000
#define SPEED_25000		25000
#define SPEED_40000		40000
#define SPEED_50000		50000
#define SPEED_56000		56000
#define SPEED_100000		100000

#define SPEED_UNKNOWN		-1


/* TX buffer descriptor control register */

#define GEM_TXBDCTRL           0x000004cc    /* TX Buffer Descriptor control register */

/* Bitfield [5:4] */

#define GEM_TX_TS_PTP_EVENT_ONLY        0x00000010  /* Timestamp PTP Event Frames only */
#define GEM_TX_TS_ALL_PTP_FRAMES_ONLY   0x00000020  /* Timestamp All PTP Frames only */
#define GEM_TX_TS_ALL_FRAMES            0x00000030  /* Timestamp All Frames only */

/* RX buffer descriptor control register */

#define GEM_RXBDCTRL            0x00004d0 /* RX Buffer Descriptor control register */

/* Bitfield [5:4] */

#define GEM_RX_TS_PTP_EVENT_ONLY        0x00000010  /* Timestamp PTP Event Frames only */
#define GEM_RX_TS_ALL_PTP_FRAMES_ONLY   0x00000020  /* Timestamp All PTP Frames only */
#define GEM_RX_TS_ALL_FRAMES            0x00000030  /* Timestamp All Frames only */

/* store_rx_ts in GEM_CTL */
#define GEM_STORE_RX_TIMESTAMP  0x00008000    /* Store receive time stamp to memory */

#define GEM_TISUBN              0x000001bc    /* 1588 Timer Increment Sub-ns: */
                                              /* 15:0 msb[28:8],31:24 lsb[7:0] */
#define GEM_TSH                 0x000001c0    /* 1588 Timer Seconds High.*/
                                              /* msb [47:32] of seconds */
#define GEM_TSL                 0x000001d0    /* 1588 Timer Seconds Low */
                                              /* lsb [31:0] of seconds */
#define GEM_TN                  0x000001d4    /* 1588 Timer Nanoseconds [29:0]*/
                                              /* [31:30] reserved */
#define GEM_TA                  0x000001d8    /* 1588 Timer Adjust */
#define GEM_TI                  0x000001dc    /* 1588 Timer Increment */

#define GEM_EFTSL               0x01e0    /* PTP Event Frame Tx Seconds Low */
#define GEM_EFTN                0x01e4    /* PTP Event Frame Tx Nanoseconds */
#define GEM_EFRSL               0x01e8    /* PTP Event Frame Rx Seconds Low */
#define GEM_EFRN                0x01ec    /* PTP Event Frame Rx Nanoseconds */
#define GEM_PEFTSL              0x01f0    /* PTP Peer Event Frame Tx Secs Low */
#define GEM_PEFTN               0x01f4    /* PTP Peer Event Frame Tx Ns */
#define GEM_PEFRSL              0x01f8    /* PTP Peer Event Frame Rx Sec Low */
#define GEM_PEFRN               0x01fc    /* PTP Peer Event Frame Rx Ns */

/* Bitfields in TSH */
#define GEM_TSH_OFFSET          0 /* TSU timer value (s). MSB [47:32] of seconds */
#define GEM_TSH_SIZE            16

/* Bitfields in TSL */
#define GEM_TSL_OFFSET          0 /* TSU timer value (s). LSB [31:0] of seconds */
#define GEM_TSL_SIZE            32

/* Bitfields in TN */
#define GEM_TN_OFFSET           0 /* TSU timer value (ns) */
#define GEM_TN_SIZE             30

/* Transmit DMA buffer descriptor Word 1 */
#define GEM_DMA_TXVALID_OFFSET      23 /* timestamp has been captured in the 
Buffer Descriptor */
#define GEM_DMA_TXVALID_SIZE        1

/* Receive DMA buffer descriptor Word 0 */
#define GEM_DMA_RXVALID_OFFSET      2 /* indicates a valid timestamp in the 
Buffer Descriptor */
#define GEM_DMA_RXVALID_SIZE        1

/* DMA buffer descriptor Word 2 (32 bit addressing) or Word 4 (64 bit addressing) */
#define GEM_DMA_SECL_OFFSET         30 /* Timestamp seconds[1:0]   >> 30*/
#define GEM_DMA_SECL_SIZE           2
#define GEM_DMA_NSEC_MASK           0x3fffffff;  /* word2[29:0].Timestamp nanosecs [29:0] */
#define GEM_DMA_SECL_MASK           0xc0000000   /* word2[31:30].Timestamp seconds[1:0] */

/* DMA buffer descriptor Word 3 (32 bit addressing) or Word 5 (64 bit addressing) */

/* New hardware supports 12 bit precision of timestamp in DMA buffer descriptor.
 * Old hardware supports only 6 bit precision but it is enough for PTP.
 * Less accuracy is used always instead of checking hardware version.
 */
#define GEM_DMA_SECH_OFFSET         2       /*Timestamp seconds[5:2] << 2*/
#define GEM_DMA_SECH_SIZE           4
#define GEM_DMA_SEC_WIDTH           (GEM_DMA_SECH_SIZE + GEM_DMA_SECL_SIZE)
#define GEM_DMA_SEC_TOP             (1 << GEM_DMA_SEC_WIDTH)  /* 0x8 */
#define GEM_DMA_SEC_MASK            (GEM_DMA_SEC_TOP - 1)     /* 0x7F */
#define GEM_DMA_SECH_MASK           0xf    /* word3[3:0]->Timestamp seconds[5:2] */


#define GEM_INC_MAX_VALUE     0xff
#define GEM_SUB_NS_PRECISION  24    /* 2 << 24 */

#define GEM_INC_MASK          0xff         /* bits [7:0] nanoseconds in GEM_TI */

#define GEM_SUB_INCA_MASK_H   0xffff00     /* bits [15:0] in register, bits [23:8] of sub-ns. */
#define GEM_SUB_INCA_MASK_L   0xff         /* bits [31:24] in register, bits [7:0] of sub-ns */

#define GEM_SUB_INCA_OFFSET_H   8          /* (value & 0xffff00) >> 8 */
#define GEM_SUB_INCA_OFFSET_L   24         /* value << 24, bit [31:24] */
#define GEM_FIXED_NS_ADD_PER_CYCLE    0x4  /* 250MHz, should be obtained dynamically */

#define GEM_TSEC_SIZE  (GEM_TSH_SIZE + GEM_TSL_SIZE)

#define GEM_DMA_EXT_TX         0x20000000    /* Enable TX extended BD mode */
#define GEM_DMA_EXT_RX         0x10000000    /* Enable RX extended BD mode */

/**/
#define TSU_SEC_MAX_VAL (((UINT32)1 << 31) - 1)
#define TSU_NSEC_MAX_VAL ((1 << GEM_TN_SIZE) - 1)

#define GEM_SPIN_LOCK_ISR_TAKE  SPIN_LOCK_ISR_TAKE
#define GEM_SPIN_LOCK_ISR_GIVE  SPIN_LOCK_ISR_GIVE

#define USXGMII_MONITOR_DELAY  2  /* monitor taskDelay */

#define GEM_TX_Q_SEG_ALLOC_Q_LOWER   0x5a0
#define GEM_Qx_ISR(hw_q)		(0x0400 + ((hw_q) << 2))
#define GEM_Qx_TBQP(hw_q)		(0x0440 + ((hw_q) << 2))
#define GEM_Qx_TBQPH(hw_q)		(0x04C8)
#define GEM_Qx_RBQP(hw_q)		(0x0480 + ((hw_q) << 2))
#define GEM_Qx_RBQS(hw_q)		(0x04A0 + ((hw_q) << 2))
#define GEM_Qx_RBQPH(hw_q)		(0x04D4)
#define GEM_Qx_IER(hw_q)		(0x0600 + ((hw_q) << 2))
#define GEM_Qx_IDR(hw_q)		(0x0620 + ((hw_q) << 2))
#define GEM_Qx_IMR(hw_q)		(0x0640 + ((hw_q) << 2))

struct timeStampPtp
{
    struct timespec descTs;
    UINT32          flag;
};



typedef struct gemDescExt
    {
    volatile UINT32    bdAddr;
    volatile UINT32    bdSts;
   /*
    * tsSecLNano: word2 in buffer descriptor,
    * [31:30]:timestamp seconds [1:0]
    * [29:0]: timestamp nanoseconds [29:0]
    */
    UINT32             tsSecLNano;
   /*
    * tsSecH: word3 in buffer descriptor,
    * [31:4]:unused
    * [3:0]:timestamp seconds [5:2]
    */
    UINT32             tsSecH;
    } GEM_DESC_EXT;


typedef struct gem_desc
    {
    volatile UINT32    bdAddr;
    volatile UINT32    bdSts;
    } GEM_DESC;

enum gemBoardType
    {
    GEM_1588_VARIANT_NONE = 0,
    GEM_1588_VARIANT_E2000 = 1,
	GEM_1588_VARIANT_D2000 = 2,
    };
    
/* Private adapter context structure. */

typedef struct gem_drv_ctrl
    {
    END_OBJ          gemEndObj;
    VXB_DEVICE_ID    gemDev;
    void *           gemBar;
    void *           gemHandle;
    FUNCPTR          clkSetup;
    FUNCPTR          ifCached;
    void *           gemMuxDevCookie;

    JOB_QUEUE_ID     gemJobQueue;
    QJOB             gemIntJob;
    atomicVal_t      gemIntPending;
    QJOB             gemRxJob;
    atomicVal_t      gemRxPending;
    QJOB             gemTxJob;
    atomicVal_t      gemTxPending;

    BOOL             gemPolling;
    M_BLK_ID         gemPollBuf;
    UINT8            gemAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES gemCaps;
    END_IFDRVCONF    gemEndStatsConf;
    END_IFCOUNTERS   gemEndStatsCounters;

    UINT32           gemInErrors;
    UINT32           gemInDiscards;
    UINT32           gemInUcasts;
    UINT32           gemInMcasts;
    UINT32           gemInBcasts;
    UINT32           gemInOctets;
    UINT32           gemOutErrors;
    UINT32           gemOutUcasts;
    UINT32           gemOutMcasts;
    UINT32           gemOutBcasts;
    UINT32           gemOutOctets;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *  gemMediaList;
    END_ERR          gemLastError;
    UINT32           gemCurMedia;
    UINT32           gemCurStatus;
    VXB_DEVICE_ID    gemMiiBus;
    VXB_DEVICE_ID    gemMiiDev;
    FUNCPTR          gemMiiPhyRead;
    FUNCPTR          gemMiiPhyWrite;
    int              gemMiiPhyAddr;

    /* End MII/ifmedia required fields */

    GEM_DESC *       gemRxDescMem;
    GEM_DESC *       gemTxDescMem;
    M_BLK_ID         gemTxMblk[GEM_TX_DESC_CNT];
    M_BLK_ID         gemRxMblk[GEM_RX_DESC_CNT];

    UINT32           gemTxProd;
    UINT32           gemTxCons;
    UINT32           gemTxFree;
    UINT32           gemRxIdx;
    UINT32           gemTxLast;
    UINT32           gemTxStall;
    UINT32           gemRxStall;

    SEM_ID           gemDevSem;
    SEM_ID           gemPhySem;
    int              gemMaxMtu;

    phyInterface_t	 phyInterface;
    int              speed;
    int              duplex;
    BOOL             autoneg;

	void            (*gemAddrSet)(UINT8* macAddr);

    /* 1588 PTP */
#if ((defined _WRS_KERNEL ) && (defined _WRS_CONFIG_IEEE1588_PTP_DEVICE))
    VXB_DEVICE_ID          gem1588Dev;
    IEEE1588_HARDWARE * p1588DevInfo;
    struct timeStampPtp rxTs;
    struct timeStampPtp txTs;
    enum gemBoardType ieee1588Variant;
    
    VXB_DEVICE_ID       pTimerDev; /* TODO */
    struct ieee1588_timer * p1588Timer;
#endif /* _WRS_CONFIG_IEEE1588_PTP_DEVICE */
    } GEM_DRV_CTRL;

/* GEM control module register low level access routines */

#define GEM_BAR(p)        ((GEM_DRV_CTRL *)(p)->pDrvCtrl)->gemBar
#define GEM_HANDLE(p)     ((GEM_DRV_CTRL *)(p)->pDrvCtrl)->gemHandle

#define CSR_READ_4(pDev, addr)            \
        vxbRead32(GEM_HANDLE(pDev),       \
                 (UINT32 *)((char *)GEM_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)     \
        vxbWrite32(GEM_HANDLE(pDev),      \
                  (UINT32 *)((char *)GEM_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)   \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)   \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFtGemEndh */
