/* vxbFtSpi.c - Driver for spi controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <vsbConfig.h>
#include <intLib.h>
#include <stdlib.h>
#include <string.h>
#include <semLib.h>
#include <taskLib.h>
#include <stdio.h>
#include <vxbTimerLib.h>
#include <cacheLib.h>
#include <hwif/util/hwMemLib.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include <hwif/vxbus/vxbSpiLib.h>
#include "vxbFtSpi.h"
#include "ftPinMux.h"
/* debug macro */

#undef  SPI_DBG_ON
#ifdef  SPI_DBG_ON

#undef  LOCAL
#define LOCAL

#define SPI_DBG_OFF            0x00000000
#define SPI_DBG_ISR            0x00000001
#define SPI_DBG_RW             0x00000002
#define SPI_DBG_ERR            0x00000004
#define SPI_DBG_RTN            0x00000008
#define SPI_DBG_INFO           0x00000010
#define SPI_DBG_ALL            0xffffffff

LOCAL UINT32 spiDbgMask =  SPI_DBG_ALL;
IMPORT FUNCPTR _func_logMsg;

#define SPI_DBG(mask, string, X1, X2, X3, X4, X5, X6)           \
    if ((spiDbgMask & mask) || (mask == SPI_DBG_ALL))           \
        if (_func_logMsg != NULL)                               \
            (* _func_logMsg)(string, (int)X1, (int)X2, (int)X3, \
                             (int)X4, (int)X5, (int)X6)
#else
#define SPI_DBG(mask, string, X1, X2, X3, X4, X5, X6)
#endif  /* SPI_DBG_ON */

#if _BYTE_ORDER == _BIG_ENDIAN
#    define SPI_REG_HANDLE_SWAP(x) VXB_HANDLE_SWAP(x)
#else
#    define SPI_REG_HANDLE_SWAP(x) (x)
#endif /* _BYTE_ORDER == _BIG_ENDIAN */

#define IS_SPI_FULL_DUPLEX(mode)  (((mode) & SPI_FULL_DUPLEX) != 0)

/* VxBus methods */

LOCAL void vxbFtSpiInstInit (VXB_DEVICE_ID pDev);
LOCAL void vxbFtSpiInstInit2 (VXB_DEVICE_ID pDev);
LOCAL void vxbFtSpiInstConnect (VXB_DEVICE_ID pDev);
LOCAL VXB_SPI_BUS_CTRL * vxbFtSpiCtrlGet (VXB_DEVICE_ID pDev);
LOCAL void vxbFtSpiShow (VXB_DEVICE_ID, int);
LOCAL STATUS vxbFtSpiInstUnlink (VXB_DEVICE_ID pDev, void * unused);

/* forward declarations */

LOCAL void vxbFtSpiCtrlInit (VXB_DEVICE_ID pDev);
LOCAL STATUS vxbFtSpiChanCfg (VXB_DEVICE_ID pDev, SPI_HARDWARE * pSpiDev);
LOCAL void vxbFtSpiIsr (VXB_DEVICE_ID pDev);
LOCAL STATUS vxbFtSpiTransfer (VXB_DEVICE_ID pDev, SPI_HARDWARE * pSpiDev,
                                  SPI_TRANSFER * pPkg);
LOCAL void vxbFtSpiChanShutdown (VXB_DEVICE_ID  pDev,UINT32 chan);
LOCAL UINT32 vxbFtSpiTxFifoMax(VXB_DEVICE_ID pDev);
LOCAL void vxbFtSpiTx(VXB_DEVICE_ID pDev);
LOCAL UINT32 vxbFtSpiRxFifoMax(VXB_DEVICE_ID pDev);
LOCAL void vxbFtSpiRx(VXB_DEVICE_ID pDev);
LOCAL STATUS vxbFtSpiTrans(VXB_DEVICE_ID  pDev, SPI_HARDWARE * pSpiDev, 
                           UINT8 * txBuf, UINT8 * rxBuf,UINT32  len);

/* locals */

LOCAL struct drvBusFuncs vxbFtSpiVxbFuncs = {
    vxbFtSpiInstInit,      /* devInstanceInit */
    vxbFtSpiInstInit2,     /* devInstanceInit2 */
    vxbFtSpiInstConnect    /* devConnect */
};

LOCAL device_method_t vxbFtSpiDeviceMethods[] = {
    DEVMETHOD (vxbSpiControlGet, vxbFtSpiCtrlGet),
    DEVMETHOD (busDevShow,       vxbFtSpiShow),
    DEVMETHOD (vxbDrvUnlink,     vxbFtSpiInstUnlink),
    DEVMETHOD_END
};

LOCAL struct vxbPlbRegister vxbFtSpiDevRegistration = {
    {
    NULL,                       /* pNext */
    VXB_DEVID_DEVICE,           /* devID */
    VXB_BUSID_PLB,              /* busID = PLB */
    VXB_VER_4_0_0,              /* vxbVersion */
    FT_SPI_NAME,                /* drvName */
    &vxbFtSpiVxbFuncs,       /* pDrvBusFuncs */
    vxbFtSpiDeviceMethods,   /* pMethods */
    NULL,                       /* devProbe  */
    NULL,                       /* pParamDefaults */
    },
};

/* controllor read and write interface */

#define FT_SPI_BAR(p)    ((char *)((p)->pRegBase[0]))
#define FT_SPI_HANDLE(p) (((FT_SPI_CTRL *)(p)->pDrvCtrl)->regHandle)

#undef CSR_READ_4
#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FT_SPI_HANDLE(pDev),                             \
               (UINT32 *)((char *)FT_SPI_BAR(pDev) + (addr)))

#undef CSR_WRITE_4
#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FT_SPI_HANDLE(pDev),                            \
                (UINT32 *)((char *)FT_SPI_BAR(pDev) + (addr)), (data))

#undef CSR_SETBIT_4
#define CSR_SETBIT_4(pDev, offset, val)                         \
    CSR_WRITE_4((pDev), (offset), CSR_READ_4((pDev), (offset)) | (val))

#undef CSR_CLRBIT_4
#define CSR_CLRBIT_4(pDev, offset, val)                         \
    CSR_WRITE_4((pDev), (offset), CSR_READ_4((pDev), (offset)) & ~(val))

/*****************************************************************************
*
* vxbFtSpiRegister - register with the VxBus subsystem
*
* This routine registers the SPI driver with VxBus Systems.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFtSpiRegister (void)
    {
    vxbDevRegister ((struct vxbDevRegInfo *) &vxbFtSpiDevRegistration);
    }

/*****************************************************************************
*
* vxbFtSpiInstInit - initialize vxbFt SPI controller
*
* This function implements the VxBus instInit handler for a SPI controller
* device instance.
*
* Initialize the device by the following:
*
* - retrieve the resource from the hwconf
* - per-device init
* - announce SPI Bus and create device instance
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl;
    struct hcfDevice * pHcf;
    FUNCPTR clkFunc = NULL;
    int i;

    /* check for valid parameter */

    VXB_ASSERT_NONNULL_V (pDev);

    /* create controller driver context structure for core */

    pDrvCtrl = (FT_SPI_CTRL *) hwMemAlloc (sizeof (FT_SPI_CTRL));

    if (pDrvCtrl == NULL)
        {
        return;
        }

    /* save instance ID */

    pDev->pDrvCtrl = pDrvCtrl;
    pDrvCtrl->pDev = pDev;

    for (i = 0; i < VXB_MAXBARS; i++)
        {
        if (pDev->regBaseFlags[i] == VXB_REG_MEM)
            {
            break;
            }
        }

    if (i == VXB_MAXBARS)
        {
    #ifndef  _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *)pDrvCtrl);
    #endif /* _VXBUS_BASIC_HWMEMLIB */

        pDev->pDrvCtrl = NULL;
        return;
        }

    pDrvCtrl->regBase = pDev->pRegBase[i];
    vxbRegMap (pDev, i, &pDrvCtrl->regHandle);
    pDrvCtrl->regHandle = (void *)
                          SPI_REG_HANDLE_SWAP ((ULONG)pDrvCtrl->regHandle);

    pHcf = (struct hcfDevice *) hcfDeviceGet (pDev);
    if (pHcf == NULL)
        {
    #ifndef  _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *)pDrvCtrl);
    #endif /* _VXBUS_BASIC_HWMEMLIB */

        pDev->pDrvCtrl = NULL;
        return;
        }

    /* retrieve the integer value for clock frequency */

    /*
     * resourceDesc {
     * The clkFreq resource specifies the frequency of the external oscillator
     * connected to the device for baud rate determination. When specified
     * as an integer, it represents the frequency, in Hz, of the external
     * oscillator. }
     */

    if (devResourceGet (pHcf, "clkFreq", HCF_RES_INT,
                        (void *) &pDrvCtrl->clkFrequency) != OK)
        {

        /* retrieve function pointer if integer value not available */

        /*
         * resourceDesc {
         * The clkFreq resource specifies the frequency of the external 
         * oscillator connected to the device for baud rate determination. 
         * When specified as an address, it represents the address
         * of a function to call, which returns the frequency. }
         */

        if (devResourceGet (pHcf, "clkFreq", HCF_RES_ADDR, (void *)&clkFunc) !=
            OK)
            {
            pDrvCtrl->clkFrequency = SPI_CLK_DEFAULT;
            }
        else
            {

            /* call the function to retrieve the clock frequency */

            pDrvCtrl->clkFrequency = (*clkFunc) ();
            }
        }

    /*
     * resourceDesc {
     * The polling resource specifies whether the driver uses polling mode
     * or not. If this property is not explicitly specified, the driver
     * uses interrupt by default. }
     */

    if (devResourceGet (pHcf, "rwMode", HCF_RES_INT,
                        (void *) &pDrvCtrl->rwMode) != OK)
        {
        pDrvCtrl->rwMode = SPI_RW_MODE_INT;
        }

    /*
     * resourceDesc {
     * The csNum resource specifies maximum num of chipSelect on each SPI
     * controller. If this property is not explicitly specified, the driver
     * uses 4 by default. }
     */

    if (devResourceGet (pHcf, "csNum", HCF_RES_INT,
                        (void *) &pDrvCtrl->csNum) != OK)
        {
        pDrvCtrl->csNum = SPI_MAX_CS_NUM;
        }

    /*
     * resourceDesc {
     * The useInt1 resource specifies whether the SPI controller uses INT1 as
     * interrupt source. If this property is not explicitly specified, the 
     * driver uses TRUE by default. }
     */

    if (devResourceGet (pHcf, "useInt1", HCF_RES_INT,
                        (void *) &pDrvCtrl->useInt1) != OK)
        {
        pDrvCtrl->useInt1 = TRUE;
        }

    /*
     * resourceDesc {
     * The wdelay resource specifies delay in between transmissions. 
     * Idle time that will be applied at the end of the current transmission 
     * if the bit WDEL is set in the current buffer. The delay to be applied 
     * is equal to: WDELAY * P(SPI module clock) + 2 * P(SPI module clock).
     * If this property is not explicitly specified, the driver
     * uses the default value. }
     */

    if (devResourceGet (pHcf, "wdelay", HCF_RES_INT,
                        (void *) &pDrvCtrl->wdelay) != OK)
        {
        pDrvCtrl->wdelay = SPIFMT_WDELAY_DEFAULT;
        }

    /*
     * resourceDesc {
     * The c2tdelay resource specifies chip-select-active-to-transmit-start 
     * delay. It defines a setup time for the slave device that delays the 
     * data transmission from the chip select active edge by a multiple of 
     * SPI module clock cycles. C2TDELAY can be configured between 2 and 257
     * SPI module clock cycles, the setup time value is calculated as follows:
     * t(C2TDELAY) = (C2TDELAY + 2) * SPI module clock period.
     * If this property is not explicitly specified, the driver
     * uses the default value. }
     */

    if (devResourceGet (pHcf, "c2tdelay", HCF_RES_INT,
                        (void *) &pDrvCtrl->wdelay) != OK)
        {
        pDrvCtrl->c2tdelay = SPIFMT_C2TDELAY_DEFAULT;
        }

    /*
     * resourceDesc {
     * The t2cdelay resource specifies transmit-end-to-chip-select-inactive 
     * delay. It defines a hold time for the slave device that delays the chip
     * select deactivation by a multiple of SPI module clock cycles after the 
     * last bit is transferred. T2CDELAY can be configured between 2 and 256 
     * SPI module clock cycles. The hold time value is calculated as follows:
     * t(T2CDELAY) = (T2CDELAY + 1) * SPI module clock period.
     * If this property is not explicitly specified, the driver
     * uses the default value. }
     */

    if (devResourceGet (pHcf, "t2cdelay", HCF_RES_INT,
                        (void *) &pDrvCtrl->wdelay) != OK)
        {
        pDrvCtrl->t2cdelay = SPIFMT_T2CDELAY_DEFAULT;
        }

    /* SPI controller init */

    (void) vxbFtSpiCtrlInit (pDev);

    /* install the transfer routine for SPI Lib */

    pDrvCtrl->vxbSpiCtrl.spiTransfer = (void *)vxbFtSpiTransfer;

    /* announce that there's a SPI bus */

    (void) vxbBusAnnounce (pDev, VXB_BUSID_SPI);

    /* notify the bus subsystem of all devices on SPI */

    (void) spiBusAnnounceDevices (pDev);

    /* the first phase of the initialization */

    pDrvCtrl->initPhase = 1;
    }

/*******************************************************************************
*
* vxbFtSpiCtrlInit -  SPI controller initialization
*
* This routine performs the SPI controller initialization.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiCtrlInit
    (
    VXB_DEVICE_ID               pDev
    )
    {
    FT_SPI_CTRL *      pDrvCtrl;
    UINT32  fifo;

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    CSR_WRITE_4 (pDev, SPI_SSI_EN, SPI_SSI_DIS);
    
    /* config spi ctrl register */   
    CSR_WRITE_4 (pDev, SPI_CTRL0, SPI_CTRL0_DEFAULT);

    CSR_WRITE_4 (pDev, SPI_IM, 0);

    CSR_WRITE_4 (pDev, SPI_RX_SAMPLE_DLY, SPI_DEFAULT_RSD);

    CSR_WRITE_4 (pDev, SPI_CS_OFFSET, SPI_CHIP_SEL_EN_ALL);

    CSR_WRITE_4 (pDev, SPI_SSI_EN, SPI_SSI_ENA);
    
    for (fifo = 1; fifo < 256; fifo++) 
        {
        CSR_WRITE_4(pDev, SPI_TXFTL, fifo);
        if (fifo != CSR_READ_4(pDev, SPI_TXFTL))
            break;
        }
    CSR_WRITE_4(pDev, SPI_TXFTL, 0);
        pDrvCtrl->txFifoLen = (fifo == 1) ? 0 : fifo;
    
    for (fifo = 1; fifo < 256; fifo++) 
        {
        CSR_WRITE_4(pDev, SPI_RXFTL, fifo);
        if (fifo != CSR_READ_4(pDev, SPI_RXFTL))
            break;
        }
    CSR_WRITE_4(pDev, SPI_RXFTL, 0);
    pDrvCtrl->rxFifoLen = (fifo == 1) ? 0 : fifo;
    
    if(pDev->unitNumber == 0)
    {
        pinSpim0Cs0();
        pinSpim0Rxd();
        pinSpim0Txd();
        pinSpim0Sclk();
    }   
    if(pDev->unitNumber == 1)
    {
        pinSpim1Cs0();
        pinSpim1Rxd();
        pinSpim1Txd();
        pinSpim1Sclk();
    }     
    if(pDev->unitNumber == 2)
    {
        pinSpim2Cs0();
        pinSpim2Rxd();
        pinSpim2Txd();
        pinSpim2Sclk();
    }
    if(pDev->unitNumber == 3)
    {
        pinSpim3Cs0();
        pinSpim3Rxd();
        pinSpim3Txd();
        pinSpim3Sclk();
    }
    
    pDrvCtrl->initDone = TRUE;
    }

/*******************************************************************************
*
* vxbFtSpiInstInit2 - second level initialization routine of SPI controller
*
* This routine performs the second level initialization of the SPI controller.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl;

    /* check for valid parameter */

    VXB_ASSERT_NONNULL_V (pDev);

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    /* used for synchronize the SPI transfer */

    pDrvCtrl->semSyncInt = semBCreate (SEM_Q_PRIORITY, SEM_EMPTY);

    if (pDrvCtrl->semSyncInt == NULL)
        {
        SPI_DBG (SPI_DBG_ERR, "semBCreate failed for semSyncInt\n",
                 0, 0, 0, 0, 0, 0);
        return;
        }

    /* used for mutex accessing of the controller */

    pDrvCtrl->muxSem = semMCreate (SEM_Q_PRIORITY);

    if (pDrvCtrl->muxSem == NULL)
        {
        SPI_DBG (SPI_DBG_ERR, "semMCreate failed for muxSem\n",
                 0, 0, 0, 0, 0, 0);

        (void) semDelete (pDrvCtrl->semSyncInt);
        pDrvCtrl->semSyncInt = NULL;
        return;
        }

    pDrvCtrl->initPhase = 2;
    }

/*******************************************************************************
*
* vxbFtSpiInstConnect - third level initialization
*
* This routine performs the third level initialization of the SPI controller
* driver.
*
* RETURNS: N/A
*
* ERRNO : N/A
*/

LOCAL void vxbFtSpiInstConnect
    (
    VXB_DEVICE_ID       pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl;

    /* check for valid parameter */

    VXB_ASSERT_NONNULL_V (pDev);

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    /* connect and enable interrupt for non-poll mode */

    if (vxbIntConnect (pDev, 0, (VOIDFUNCPTR)vxbFtSpiIsr, pDev) != OK)
       {
       SPI_DBG (SPI_DBG_ERR, "vxbIntConnect return ERROR \n",
                     1, 2, 3, 4, 5, 6);
       }
    (void) vxbIntEnable (pDev, 0, (VOIDFUNCPTR)vxbFtSpiIsr, pDev);

    pDrvCtrl->initPhase = 3;
    }

/*****************************************************************************
*
* vxbFtSpiInstUnlink - VxBus unlink handler
*
* This function shuts down a SPI controller instance in response to an
* an unlink event from VxBus. This may occur if our VxBus instance has
* been terminated, or if the SPI driver has been unloaded.
*
* RETURNS: OK if device was successfully destroyed, otherwise ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtSpiInstUnlink
    (
    VXB_DEVICE_ID pDev,
    void *        unused
    )
    {
    FT_SPI_CTRL * pDrvCtrl;

    /* check if the pDev pointer is valid */

    VXB_ASSERT (pDev != NULL, ERROR)

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    /*
     * The semaphore and interrupt resource are released here . The 
     * semaphore was created at phase 2 and interrupt was installed
     * at phase 3.
     */

    if (pDrvCtrl->initPhase >= 2)
        {
        (void)semTake (pDrvCtrl->muxSem, WAIT_FOREVER);

        /* disconnect and disable interrupt */

        (void) vxbIntDisable (pDev, 0, vxbFtSpiIsr, pDev);
        if (vxbIntDisconnect (pDev, 0, vxbFtSpiIsr, pDev) != OK)
            {
            SPI_DBG (SPI_DBG_ERR, "vxbIntDisconnect return ERROR \n",
                         1, 2, 3, 4, 5, 6);
            }

        (void) semDelete (pDrvCtrl->muxSem);
        pDrvCtrl->muxSem = NULL;
        (void) semDelete (pDrvCtrl->semSyncInt);
        pDrvCtrl->semSyncInt = NULL;
        }
#ifndef _VXBUS_BASIC_HWMEMLIB
    hwMemFree ((char *) pDrvCtrl);
#endif  /* _VXBUS_BASIC_HWMEMLIB */

    pDev->pDrvCtrl = NULL;
    pDrvCtrl->initPhase = 0;

    return (OK);
    }

/*******************************************************************************
*
* vxbFtSpiCtrlGet - get the SPI controller struct
*
* This routine returns the SPI controller struct pointer (VXB_SPI_BUS_CTRL *)
* to caller (SPI Lib) by vxbSpiControlGet method. Currently, this struct
* only contain the spiTransfer routine(eg: vxbFtSpiTransfer) for SPI Lib,
* other parameters can be easily added in this struct.
*
* RETURNS: the pointer of SPI controller struct
*
* ERRNO: N/A
*/

LOCAL VXB_SPI_BUS_CTRL * vxbFtSpiCtrlGet
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl;

    /* check if the pDev pointer is valid */

    VXB_ASSERT (pDev != NULL, NULL)

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    SPI_DBG (SPI_DBG_RTN, "vxbFtSpiCtrlGet(0x08%x) called\n",
            (_Vx_usr_arg_t)pDev, 2, 3, 4, 5, 6);

    return (&(pDrvCtrl->vxbSpiCtrl));
    }

/*******************************************************************************
*
* vxbFtSpiSetCS - set the spi chip select 
*
* This function set the spi chip select.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
void vxbFtSpiSetCS(VXB_DEVICE_ID pDev, UINT32 chan, BOOL on)
{

    UINT32 val;

    val = CSR_READ_4(pDev, SPI_CS_OFFSET);

    if (on)
    {
        val |= SPI_CHIP_SEL(chan);
    }
    else
    {
        val &= ~SPI_CHIP_SEL(chan);
    }

    CSR_WRITE_4(pDev, SPI_CS_OFFSET, val);
    
    SPI_DBG (SPI_DBG_RTN, "vxbFtSpiSetCS %x\n",
            CSR_READ_4(pDev, SPI_CS_OFFSET), 2, 3, 4, 5, 6);

    return;
}

/*******************************************************************************
*
* vxbFtSpiChanCfg - configure the SPI channel
*
* This routine configures the SPI channel with its parameters.
*
* RETURNS: OK if device was successfully configured, otherwise ERROR
*
* ERRNO: N/A
*/

 LOCAL STATUS vxbFtSpiChanCfg
    (
    VXB_DEVICE_ID       pDev,     /* controller pDev */
    SPI_HARDWARE *      pSpiDev   /* device info */
    )
    {
    UINT32              charLen;
    UINT32              prescale;
    UINT32              channel;
    UINT32              mode; 
    UINT32              tmp;
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    CSR_WRITE_4 (pDev, SPI_SSI_EN, SPI_SSI_DIS);
    
    channel = pSpiDev->devInfo->chipSelect;
    if (channel >= SPI_MAX_CS_NUM)
        {
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiChanCfg: invalid SPI cs:%d\n",
                 channel, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* calculate clock divisor */
    if (pSpiDev->devInfo->devFreq == 0)
        {
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiChanCfg: device frequency is 0\n",
                 0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    prescale = ((pDrvCtrl->clkFrequency / pSpiDev->devInfo->devFreq) + 1) & 0xfffe;
    CSR_WRITE_4 (pDev, SPI_BAUD, prescale);
    
    /* save the current working frequency */
    pDrvCtrl->curWorkingFrq = pSpiDev->devInfo->devFreq;

    /*   check data length bits supported*/
    charLen = pSpiDev->devInfo->bitWidth;
    if (charLen > SPI_CHARLEN_MAX || charLen < SPI_CHARLEN_MIN)
         {
         SPI_DBG (SPI_DBG_ERR, "vxbFtSpiChanCfg: invalid SPI char length\n",
                  1, 2, 3, 4, 5, 6);
         return ERROR;
         }

    if (pDrvCtrl->txBuf && pDrvCtrl->rxBuf)
        mode = SPI_CTRL0_TMOD_RW;
    else if (pDrvCtrl->rxBuf)
        mode = SPI_CTRL0_TMOD_R;
    else
        mode = SPI_CTRL0_TMOD_RW;
    
    tmp = CSR_READ_4 (pDev, SPI_CTRL0);
    tmp &= ~(SPI_CTRL0_TMOD_MASK | SPI_CTRL0_DFS_MASK);
    tmp |= SPI_CTRL0_TMOD (mode) | SPI_CTRL0_DFS (charLen);
    CSR_WRITE_4 (pDev, SPI_CTRL0, tmp);
    
    /* SPI clock polarity */
    if ((pSpiDev->devInfo->mode & SPI_CKPOL) != 0)
        CSR_SETBIT_4(pDev, SPI_CTRL0, SPI_CTRL0_SCPOL);
    else
        CSR_CLRBIT_4(pDev, SPI_CTRL0, SPI_CTRL0_SCPOL);
        
    /* SPI clock phase */
    if ((pSpiDev->devInfo->mode & SPI_CKPHA) != 0)
        CSR_SETBIT_4(pDev, SPI_CTRL0, SPI_CTRL0_SCPH);
    else
        CSR_CLRBIT_4(pDev, SPI_CTRL0, SPI_CTRL0_SCPH);

    /*loopback*/
    /*CSR_SETBIT_4(pDev, SPI_CTRL0, SPI_CTRL0_SRL);*/
    
    CSR_WRITE_4 (pDev, SPI_CTRL1, 0);
    CSR_WRITE_4 (pDev, SPI_RXFTL, 0);
    CSR_WRITE_4 (pDev, SPI_TXFTL, 0);
    CSR_WRITE_4 (pDev, SPI_DMA_RDL, 0);
    CSR_WRITE_4 (pDev, SPI_DMA_TDL, 0);     
    
    /* chip select polarity */
    CSR_WRITE_4 (pDev, SPI_SE, SPI_SE_CHAN(channel));

    CSR_WRITE_4 (pDev, SPI_IM, 0);
    
    return OK;
    }

/*******************************************************************************
*
* vxbFtSpiChanShutdown - shutdown one SPI channel
*
* The routine performs shutdown operation of a specific SPI channel
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

 LOCAL void vxbFtSpiChanShutdown
    (
    VXB_DEVICE_ID  pDev,       /* controller pDev */
    UINT32         chan        /* SPI channel */
    )
    {
    FT_SPI_CTRL * pDrvCtrl;

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    /* disable SPI */
    CSR_WRITE_4 (pDev, SPI_SSI_EN, SPI_SSI_DIS);

    vxbFtSpiSetCS(pDev, chan, FALSE);

    /* disable all SPI interrupts */
    CSR_WRITE_4 (pDev, SPI_IM, 0);
    }

/*******************************************************************************
*
 * vxbFtSpiTxFifoMax - Tx Fifo Max
*
 * The routine return the max entries we can fill into tx fifo
*
 * RETURNS: the max entries we can fill into tx fifo
*
* ERRNO: N/A
*/

LOCAL UINT32 vxbFtSpiTxFifoMax
    (
    VXB_DEVICE_ID       pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    UINT32 txLeft, txRoom, rxtxGap;

    txLeft = (pDrvCtrl->txBufEnd - pDrvCtrl->txBuf) / pDrvCtrl->alignSize;
    txRoom = pDrvCtrl->txFifoLen - CSR_READ_4 (pDev, SPI_TXFL);

    rxtxGap =  ((pDrvCtrl->rxBufEnd - pDrvCtrl->rxBuf) - (pDrvCtrl->txBufEnd - pDrvCtrl->txBuf))
            / pDrvCtrl->alignSize;

    return min(min(txLeft, txRoom), (pDrvCtrl->txFifoLen - rxtxGap));
    }

/*******************************************************************************
*
* vxbFtSpiTx - Send data
*
* The routine  is used to send data
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiTx
    (
    VXB_DEVICE_ID       pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;
    UINT32 max = vxbFtSpiTxFifoMax(pDev);
    UINT16 data = 0;
    while (max--) 
        {
        /* Set the tx word if the transfer's original "tx" is not null */
        if (pDrvCtrl->txBufOriginal != NULL) 
        {
        if (pDrvCtrl->alignSize == sizeof(char))
                data = *(UINT8 *)(pDrvCtrl->txBuf);
            else
                data = *(UINT16 *)(pDrvCtrl->txBuf);
            }
        CSR_WRITE_4 (pDev, SPI_D, data);
        pDrvCtrl->txBuf  += pDrvCtrl->alignSize;
        }
    }

/*******************************************************************************
*
* vxbFtSpiTxFifoMax - Rx Fifo Max
*
* The routine return the max entries we should read out of rx fifo
*
* RETURNS: the max entries we should read out of rx fifo
*
* ERRNO: N/A
*/

LOCAL UINT32 vxbFtSpiRxFifoMax
    (
    VXB_DEVICE_ID       pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;
    
    UINT32 rxLeft;

    rxLeft = (pDrvCtrl->rxBufEnd - pDrvCtrl->rxBuf) / pDrvCtrl->alignSize;

    return min(rxLeft, CSR_READ_4 (pDev, SPI_RXFL));
    }

/*******************************************************************************
*
* vxbFtSpiRx - read data
*
* The routine  is used to read data
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiRx
    (
    VXB_DEVICE_ID       pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;
    UINT32 max = vxbFtSpiRxFifoMax(pDev);
    UINT16 data = 0;

    while (max--) 
        {
        data = CSR_READ_4 (pDev, SPI_D);
        /* Care rx only if the transfer's original "rx" is not null */
        if (pDrvCtrl->rxBufOriginal != NULL) 
            {
            if (pDrvCtrl->alignSize == sizeof(char))
                *(UINT8 *)(pDrvCtrl->rxBuf) = data;
            else
                *(UINT16 *)(pDrvCtrl->rxBuf) = data;
            }
        pDrvCtrl->rxBuf += pDrvCtrl->alignSize;
            }
    }

/*******************************************************************************
*
* vxbFtSpiTrans - Transfer data
*
* The routine  is used to transfer data
*
* RETURNS:  OK or ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtSpiTrans
    (
    VXB_DEVICE_ID       pDev,     /* controller pDev */
    SPI_HARDWARE *      pSpiDev,  /* device info */
    UINT8 * txBuf,    /* data to be written, or NULL */
    UINT8 * rxBuf,    /* data to be read, or NULL */
    UINT32  len
    )
    {
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;
    UINT16 txLevel = 0;
    UINT32 timeoutTick;

    if (pDev == NULL || pSpiDev == NULL ||  pSpiDev->devInfo == NULL)
        {
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiTransFifo NULL pointer\n",
                 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pDrvCtrl->transLen = len;
    pDrvCtrl->txBuf = txBuf;
    pDrvCtrl->txBufOriginal = txBuf;
    pDrvCtrl->txBufEnd = txBuf + len;
    pDrvCtrl->rxBuf = rxBuf;
    pDrvCtrl->rxBufOriginal = rxBuf;
    pDrvCtrl->rxBufEnd = rxBuf + len;

    if (vxbFtSpiChanCfg(pDev, pSpiDev) != OK)
         {
         SPI_DBG (SPI_DBG_ERR, "vxbFtSpiChanCfg failed.\n",
                  1, 2, 3, 4, 5, 6);
         return ERROR;
         }

    if ((pDrvCtrl->rwMode == SPI_RW_MODE_POLL) || pDrvCtrl->initPhase < 3)
        {
        SPI_DBG (SPI_DBG_RW, "poll mode.\n",
                 1, 2, 3, 4, 5, 6);
        CSR_WRITE_4 (pDev, SPI_SSI_EN, SPI_SSI_ENA);
        do 
            {
            vxbFtSpiTx (pDev);
            vxbFtSpiRx (pDev);
            } while (pDrvCtrl->rxBufEnd > pDrvCtrl->rxBuf);
        }
    else
        {
        SPI_DBG (SPI_DBG_RW, "int mode.\n",
                 1, 2, 3, 4, 5, 6);
        timeoutTick = (SPI_TIMEOUT_COUNT * vxbSysClkRateGet () * 
                   sizeof(char) * pDrvCtrl->transLen) /
                       (pDrvCtrl->curWorkingFrq) + SPI_TIMEOUT_EXTRA;
 
        txLevel = min(pDrvCtrl->txFifoLen / 2, pDrvCtrl->transLen / pDrvCtrl->alignSize);
        CSR_WRITE_4 (pDev, SPI_TXFTL, txLevel);
        CSR_WRITE_4 (pDev, SPI_IM, SPI_INT_TXEI | SPI_INT_TXOI | SPI_INT_RXUI | SPI_INT_RXOI);
        CSR_WRITE_4 (pDev, SPI_SSI_EN, SPI_SSI_ENA);

        (void)semTake (pDrvCtrl->semSyncInt, timeoutTick);
        }

    return OK;    
    }

/*******************************************************************************
*
* vxbFtSpiTransfer - SPI transfer routine
*
* This routine is used to perform one transmission. It is the interface which 
* can be called by SPI device driver to send and receive data via the SPI 
* controller.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/
LOCAL STATUS vxbFtSpiTransfer
    (
    VXB_DEVICE_ID       pDev,           /* controller pDev */
    SPI_HARDWARE *      pSpiDev,        /* device info */
    SPI_TRANSFER *      pPkg            /* transfer data info */
    )
    {
    STATUS              sts = OK;
    UINT32              channel;
    UINT32              alignSize;
    FT_SPI_CTRL * pDrvCtrl;

    /* check if the pointers are valid */

    if (pDev == NULL || pSpiDev == NULL || pPkg == NULL ||
    pSpiDev->devInfo == NULL)
        {
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiTransfer NULL pointer\n",
                 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;
    if (pDrvCtrl == NULL)
        {
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiTransfer pDrvCtrl is NULL\n",
                 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    SPI_DBG (SPI_DBG_RTN, "vxbFtSpiTransfer txLen[%d] rxLen[%d]\n",
             pPkg->txLen, pPkg->rxLen, 3, 4, 5, 6);

    if (pPkg->txLen == 0 && pPkg->rxLen == 0)
        {
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiTransfer tx and rx both are 0\n",
                 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if ((pPkg->txLen != 0 && pPkg->txBuf == NULL ) ||
        (pPkg->rxLen != 0 && pPkg->rxBuf == NULL))
        {
        SPI_DBG (SPI_DBG_ERR,
                 "vxbFtSpiTransfer invalid parameters[%x %x %x %x] \n",
                  pPkg->txBuf, pPkg->txLen, pPkg->rxLen, pPkg->rxBuf, 5, 6);

        return ERROR;
        }

    if (pSpiDev->devInfo->bitWidth <= 8)       /* 4 to 8 bits */
        {
        alignSize = sizeof(char);
        }
    else if (pSpiDev->devInfo->bitWidth <= 16) /* 9 to 16 bits */
        {
        alignSize = sizeof(UINT16);
        }
    else    /* 17 to 32 bits */
        {
        SPI_DBG (SPI_DBG_ERR, 
                 "vxbFtSpiTransfer: data word length must between 2-16\n",
                 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* check to see if the address is aligned with SPI bit width */

    if ((pPkg->txLen != 0 && 
        (((UINT32)pPkg->txBuf & (alignSize - 1)) != 0 ||
         (pPkg->txLen & (alignSize - 1)) != 0)) ||
        (pPkg->rxLen != 0 && 
         (((UINT32)pPkg->rxBuf & (alignSize - 1)) != 0 ||
          (pPkg->rxLen & (alignSize - 1)) != 0)))
        {
        SPI_DBG (SPI_DBG_ERR,
                 "vxbFtSpiTransfer address or len is not aligned:"
                 "[tx:%x-%x rx:%x-%x] with %d \n",
                 pPkg->txBuf, pPkg->txLen, pPkg->rxBuf, pPkg->rxLen, 
                 alignSize, 6);

        return ERROR;
        }

    channel = pSpiDev->devInfo->chipSelect;
    if (channel >= SPI_MAX_CS_NUM)
        {
        SPI_DBG (SPI_DBG_ERR,
                 "vxbFtSpiTransfer invalid channel[%x] \n",
                  channel, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * The controller can not be used by multichannel at the same time.
     * If the initPhase < 2, there is no multi-task context. So, the
     * semTake is not needed.
     */

    if (pDrvCtrl->initPhase >= 2)
        {
        (void)semTake (pDrvCtrl->muxSem, WAIT_FOREVER);
        }

    pDrvCtrl->alignSize = alignSize;
    vxbFtSpiSetCS(pDev, channel, TRUE);
    
    if (IS_SPI_FULL_DUPLEX (pSpiDev->devInfo->mode))
        {
        sts = vxbFtSpiTrans(pDev, pSpiDev, pPkg->txBuf,  pPkg->rxBuf, max(pPkg->txLen,pPkg->rxLen));
        }
    else
        {
        if ((pPkg->txLen != 0) && (pPkg->rxLen != 0))
            {
            sts = vxbFtSpiTrans(pDev, pSpiDev, pPkg->txBuf, NULL, pPkg->txLen);
            if(OK == sts)
                sts = vxbFtSpiTrans(pDev, pSpiDev, NULL, pPkg->rxBuf, pPkg->rxLen);
            }
        else if (pPkg->txLen != 0)
            {
            sts = vxbFtSpiTrans(pDev, pSpiDev, pPkg->txBuf, NULL, pPkg->txLen);
            }
        else
            {
            sts = vxbFtSpiTrans(pDev, pSpiDev, NULL, pPkg->rxBuf, pPkg->rxLen);
            }       
        }

    if (pPkg->usDelay > 0)
        {
        vxbUsDelay(pPkg->usDelay);
        }

    if (pDrvCtrl->initPhase >= 2)
        {
        semGive (pDrvCtrl->muxSem);
        }

    vxbFtSpiChanShutdown (pDev, channel);

    return sts;
    }

/*******************************************************************************
*
* vxbFtSpiIsr - interrupt service routine
*
* This routine handles interrupts of SPI.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiIsr
    (
    VXB_DEVICE_ID       pDev
    )
    {
    FT_SPI_CTRL * pDrvCtrl = (FT_SPI_CTRL *)pDev->pDrvCtrl;
    UINT16 irq_status = CSR_READ_4 (pDev, SPI_ISR);
   
    if (irq_status & (SPI_INT_TXOI | SPI_INT_RXOI | SPI_INT_RXUI)) {
        CSR_READ_4 (pDev, SPI_IC);
        SPI_DBG (SPI_DBG_ERR, "vxbFtSpiIsr: fifo overrun/underrun", 1,2,3,4,5,6);
        return;
        }

    vxbFtSpiRx(pDev);
    if (pDrvCtrl->rxBufEnd == pDrvCtrl->rxBuf) 
        {
        CSR_CLRBIT_4(pDev, SPI_IM, SPI_INT_TXEI);
        semGive (pDrvCtrl->semSyncInt);
        return;
        }
    if (irq_status & SPI_INT_TXEI) 
        {
        CSR_CLRBIT_4(pDev, SPI_IM, SPI_INT_TXEI);
        vxbFtSpiTx (pDev);
        CSR_SETBIT_4(pDev, SPI_IM, SPI_INT_TXEI);
        }   

    return;
    }

/*****************************************************************************
*
* vxbFtSpiShow - show the controller info
*
* This function shows the SPI controller's info.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtSpiShow
    (
    VXB_DEVICE_ID pDev,
    int verbose
    )
    {
    FT_SPI_CTRL * pDrvCtrl;

    /* check for valid parameter */

    VXB_ASSERT_NONNULL_V (pDev);

    pDrvCtrl = (FT_SPI_CTRL *) pDev->pDrvCtrl;

    printf ("        %s unit %d on %s @ %p with busInfo %p\n",
            pDev->pName,
            pDev->unitNumber,
            vxbBusTypeString (pDev->busID),
            pDev,
            pDev->u.pSubordinateBus);

    if (verbose > 0)
        {
        printf ("            BAR0 @ 0x%08x (memory mapped)\n",
                                                   pDev->pRegBase[0]);
        printf ("            pDrvCtrl @ 0x%08x\n", pDev->pDrvCtrl);
        printf ("            clkFrequency:  %dMhz\n",
                             pDrvCtrl->clkFrequency / 1000000);
        printf ("            current working frequency:  %dhz\n",
                             pDrvCtrl->curWorkingFrq);
        printf ("            interrupt   :  %s\n",
                             (pDrvCtrl->rwMode == SPI_RW_MODE_INT) ? 
                             "TRUE" : "FALSE");
        printf ("            init  :  %s\n", 
                             pDrvCtrl->initDone ? "SUCCESS" : "FAIL");
        }
    }

/*****************************************************************************
*
* vxbFtSpiLoopbackTest - test the spi controller in loopback mode 
*
* This function tests the spi controller in loopback mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
void vxbFtSpiLoopbackTest(UINT32 spiDevNo, UINT32 len)
{
    VXB_DEVICE_ID pDev = vxbInstByNameFind(FT_SPI_NAME, spiDevNo);
    SPI_HARDWARE      spiDev;
    SPI_TRANSFER      pkg; 
    SPI_DEV_INFO      devInfo;
    UINT32 i;

    UINT8 *  txBuf;
    UINT8 *  rxBuf;
    txBuf = (char *)malloc(len);
    rxBuf = (char *)malloc(len);
    
    devInfo.bitWidth = 8;
    devInfo.chipSelect = 0;
    devInfo.devFreq = 4800000;
    devInfo.mode = SPI_FULL_DUPLEX;
    
    spiDev.devInfo = &devInfo;
    
    for(i=0;i<len;i++)
    {
        *(txBuf + i) = (UINT8)i;
        *(rxBuf + i) = 0;
    }
   
    pkg.txBuf = txBuf; 
    pkg.rxBuf = rxBuf;
    pkg.txLen = len;
    pkg.rxLen = len;
    pkg.usDelay = 1;
    vxbFtSpiTransfer(pDev, &spiDev, &pkg);

    if(memcmp(txBuf, rxBuf, len) == 0)
        printf("test pass\n");
    else
        printf("test fail\n");
    
    free(txBuf);
    free(rxBuf);   
}
