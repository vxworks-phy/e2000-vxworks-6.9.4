/* vxbFtWdt.c -  VxBus WDT driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <vxbTimerLib.h>
#include <string.h>

#include <hwif/util/hwMemLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/vxbus/hwConf.h>
#include <spinLockLib.h>
#include <vxBusLib.h>
#include "defs.h"

/* defines */

#undef WDT_DBG_ON
#ifdef  WDT_DBG_ON
#define WDT_DBG_IRQ         0x00000001
#define WDT_DBG_RW          0x00000002
#define WDT_DBG_ERR         0x00000004
#define WDT_DBG_ALL         0xffffffff
#define WDT_DBG_OFF         0x00000000
LOCAL UINT32 wdtDbgMask = WDT_DBG_ALL;
IMPORT FUNCPTR _func_logMsg;

#define WDT_DBG(mask, string, a, b, c, d, e, f)         \
    if ((wdtDbgMask & mask) || (mask == WDT_DBG_ALL))   \
        if (_func_logMsg != NULL)                       \
            (* _func_logMsg)(string, a, b, c, d, e, f)
#else
#define WDT_DBG(mask, string, a, b, c, d, e, f)
#endif  /* WDT_DBG_ON */

#define WDT_MAX_COUNT                           0x7fffffff
#define WDT_DEFAULT_TICKS_PER_SECOND            60
#define WDT_DEFAULT_MIN_FREQ                    1 
#define WDT_DEFAULT_MAX_FREQ                    1000
#define WDT_MAX_CLK_FREQ                        50000000
#define WDT_MIN_CLK_FREQ                        1000000

#define FT_WDT_DRIVER_NAME          "ftWdt"

#define TIMER_BAR(p)    ((char *)((p)->pRegBase[0]))
#define TIMER_HANDLE(p) (((FT_WDT_TIMER *)(p)->pDrvCtrl)->handle)

#ifdef ARMBE8
#    define SWAP32 vxbSwap32
#else
#    define SWAP32 
#endif /* ARMBE8 */

#undef CSR_READ_4
#define CSR_READ_4(pDev, addr)          \
        SWAP32(vxbRead32(TIMER_HANDLE(pDev),    \
                  (UINT32 *)((char *)TIMER_BAR(pDev) + addr)))

#undef CSR_WRITE_4
#define CSR_WRITE_4(pDev, addr, data)   \
        vxbWrite32(TIMER_HANDLE(pDev),  \
                   (UINT32 *)((char *)TIMER_BAR(pDev) + addr), SWAP32(data))

/* Watchdog Register */

/* refresh frame */
#define FT_WDT_WRR      0x000

/* control frame */
#define FT_WDT_WCS      0x1000
#define FT_WDT_WOR      0x1008
#define FT_WDT_WCV      0x1010

/* refresh/control frame */
#define FT_WDT_W_IIDR   0xfcc
#define FT_WDT_IDR      0xfd0

/* Watchdog Control and Status Register */
#define FT_WDT_WCS_EN   BIT(0)
#define FT_WDT_WCS_WS0  BIT(1)
#define FT_WDT_WCS_WS1  BIT(2)

/* typedefs */

/* structure to store the timer information */

typedef struct ftWdtTimer
    {
    VXB_DEVICE_ID                   pDev;
    struct vxbTimerFunctionality    timerFunc;
    void                            (*pIsrFunc)(int);
    int                             arg;
    spinlockIsr_t                   spinLock;
    UINT32                          timerVal;
    BOOL                            isEnabled;
    void *                          handle;
    UINT32                          maxCount;
    } FT_WDT_TIMER;

#define WDT_TOTAL_COUNT 2
struct  vxbTimerFunctionality * pFtWdtTimer[WDT_TOTAL_COUNT] = {NULL};
VXB_DEVICE_ID    pDevFtWdtTimer[WDT_TOTAL_COUNT] = {NULL};    
    
/* forward declarations */

LOCAL void vxbFtWdtInstInit(VXB_DEVICE_ID);
LOCAL void vxbFtWdtInstInit2(VXB_DEVICE_ID);
LOCAL void vxbFtWdtInstConnect(VXB_DEVICE_ID);
    
LOCAL STATUS vxbFtWdtAllocate
    (
    VXB_DEVICE_ID   pInst,
    UINT32          flags,
    void **         pCookie,
    UINT32          timerNo
    );

LOCAL STATUS vxbFtWdtRelease
    (
    VXB_DEVICE_ID   pInst,
    void *          pCookie
    );

LOCAL STATUS vxbFtWdtRolloverGet
    (
    void *      pCookie,
    UINT32 *    count
    );
    
LOCAL STATUS vxbFtWdtCountGet
    (
    void *      pCookie,
    UINT32 *    count
    );
    
LOCAL STATUS vxbFtWdtISRSet
    (
    void *  pCookie,
    void    (*pFunc)(int),
    int     arg
    );
    
LOCAL STATUS vxbFtWdtEnable
    (
    void *  pCookie,
    UINT32  maxTimerCount
    );

LOCAL STATUS vxbFtWdtDisable
    (
    void *  pCookie
    );

LOCAL void vxbFtWdtIsr
    (
    VXB_DEVICE_ID        pDev   /* Information about the driver */
    );

LOCAL struct drvBusFuncs ftWdtDrvFuncs =
    {
    vxbFtWdtInstInit,          /* devInstanceInit */
    vxbFtWdtInstInit2,         /* devInstanceInit2 */
    vxbFtWdtInstConnect        /* devConnect */
    };

LOCAL device_method_t ftWdtDrv_methods[] =
    {
    DEVMETHOD_END
    };

LOCAL struct vxbDevRegInfo ftWdtDrvRegistration =
    {
    NULL,                       /* pNext */
    VXB_DEVID_DEVICE,           /* devID */
    VXB_BUSID_PLB,              /* busID = PLB */
    VXB_VER_4_0_0,              /* busVer */
    FT_WDT_DRIVER_NAME,         /* drvName */
    &ftWdtDrvFuncs,             /* pDrvBusFuncs */
    NULL,                       /* pMethods */
    NULL                        /* devProbe */
    };

/******************************************************************************
 *
 * vxbFtWdtDrvRegister - register ft Wdt driver
 *
 * This routine registers the ft Wdt driver with the vxBus subsystem.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

void vxbFtWdtDrvRegister (void)
    {  
    vxbDevRegister(&ftWdtDrvRegistration);
    }

/*******************************************************************************
 *
 * vxbFtWdtInstInit - first level initialization routine of Wdt device
 *
 * This routine performs the first level initialization of the Wdt device.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void vxbFtWdtInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    const struct hcfDevice * pHcf = NULL;
    
    FT_WDT_TIMER *pTimer;
    
    struct vxbTimerFunctionality * pTimerFunc;
    
    /* allocate the memory for the timer structure */

    pTimer = (FT_WDT_TIMER *) hwMemAlloc (sizeof (FT_WDT_TIMER));
    
    /* check if memory allocation is successful */

    if (pTimer == NULL)
        return;

    pTimer->pDev = pDev;
    pDev->pDrvCtrl = pTimer;
    
    /* locate the timer functionality data structure */

    pTimerFunc = &(pTimer->timerFunc);
    
    pFtWdtTimer[pDev->unitNumber] = pTimerFunc;
    pDevFtWdtTimer[pDev->unitNumber] = pDev;
    
    pHcf = hcfDeviceGet (pDev);
    if (pHcf == NULL)
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *) pTimer);
#endif /* _VXBUS_BASIC_HWMEMLIB */
        return;
        }

    vxbRegMap(pDev, 0, &pTimer->handle);   /* map the window */
    
    /*
     * resourceDesc {
     * The maxClkRate resource specifies the
     * maximum clock rate (in ticks/sec) of
     * timer }
     */

    if (devResourceGet (pHcf, "maxClkRate", HCF_RES_INT,
                    (void *)&pTimerFunc->maxFrequency) != OK)
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *) pTimer);
#endif /* _VXBUS_BASIC_HWMEMLIB */
        return;
        }

    /*
     * resourceDesc {
     * The minClkRate resource specifies the
     * minimum clock rate (in ticks/sec) of
     * timer }
     */

    if (devResourceGet (pHcf, "minClkRate", HCF_RES_INT,
                    (void *)&pTimerFunc->minFrequency) != OK)
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree ((char *) pTimer);
#endif /* _VXBUS_BASIC_HWMEMLIB */
        return;
        }

    /* clkFrequency: Prefer using "clkFreq" in hwconf.c. 
     * Because there are sometimes bad values from hardware register!
     */
    
    if (devResourceGet(pHcf,"clkFreq",HCF_RES_INT,
                       (void *)&pTimerFunc->clkFrequency) != OK)
    {
        /* To read it from hardware if no "clkFreq" */
        
        pTimerFunc->clkFrequency = __inline__GetCntFreq();
    }
    
    WDT_DBG (WDT_DBG_RW, "Clock Frequency: %d\r\n", pTimerFunc->clkFrequency,2,3,4,5,6);
    
    /* copy and null terminate the name of the timer */
    
    strncpy (pTimerFunc->timerName, FT_WDT_DRIVER_NAME, MAX_DRV_NAME_LEN);
    pTimerFunc->timerName [MAX_DRV_NAME_LEN] = '\0';

    pTimerFunc->features =  VXB_TIMER_CAN_INTERRUPT;
    
    /* set the default ticks per second */

    pTimerFunc->ticksPerSecond = WDT_DEFAULT_TICKS_PER_SECOND;
    
    pTimerFunc->timerAllocate = vxbFtWdtAllocate;
    pTimerFunc->timerRelease = vxbFtWdtRelease;
    pTimerFunc->timerRolloverGet = vxbFtWdtRolloverGet;
    pTimerFunc->timerCountGet = vxbFtWdtCountGet;
    pTimerFunc->timerDisable = vxbFtWdtDisable;
    pTimerFunc->timerEnable = vxbFtWdtEnable;
    pTimerFunc->timerISRSet = vxbFtWdtISRSet;
 
    /* initialize the spinlock */

    SPIN_LOCK_ISR_INIT (&pTimer->spinLock, 0);
    
    /* publish methods */

    pDev->pMethods = ftWdtDrv_methods;
    
    }

/*******************************************************************************
 *
 * vxbFtWdtInstInit2 - second level initialization routine of Wdt modules
 *
 * This routine performs the second level initialization of the Wdt modules.
 *
 * This routine is called later during system initialization.  OS features
 * such as memory allocation are available at this time.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 *
 * \NOMANUAL
 *
 */

LOCAL void vxbFtWdtInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    /* connect the ISR for the timer */
    
    if (vxbIntConnect (pDev, 0, vxbFtWdtIsr, pDev) != OK)
        pDev->pDrvCtrl = NULL;
    
    }

/*******************************************************************************
 *
 * vxbFtWdtInstConnect - third level initialization routine of Wdt device
 *
 * This routine performs the third level initialization of the Wdt device.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void vxbFtWdtInstConnect 
    (
    VXB_DEVICE_ID pDev
    )
    {
    return;
    }

/*******************************************************************************
 *
 * vxbFtWdtAllocate - allocate resources for a timer
 *
 * This routine allocates resources for a timer by the Timer Abstraction Layer.
 *
 * RETURNS: OK or ERROR if timer allocation fails
 *
 * ERRNO: N/A
 */

LOCAL STATUS vxbFtWdtAllocate
    (
    VXB_DEVICE_ID   pInst,
    UINT32          flags,
    void **         pCookie,
    UINT32          timerNo
    )
    {
    FT_WDT_TIMER * pTimer = NULL;

    /* validate the parameters */

    if ((pInst == NULL) || (pCookie == NULL))
        return ERROR;

    pTimer = (FT_WDT_TIMER *) pInst->pDrvCtrl;
    if (pTimer == NULL)
        return ERROR;

    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);

    /* check to see if the timer is already allocated */

    if (pTimer->timerFunc.allocated)
        {
        /* release the spinlock */

        SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
        return ERROR;
        }

    /* store the timer information in the pCookie */
    
    *pCookie = pTimer;

    /* set the timer allocated flag */

    pTimer->timerFunc.allocated = TRUE;

    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
    
    return OK;
    }
    
/*******************************************************************************
 *
 * vxbFtWdtRelease - release the timer resource
 *
 * This routine releases the resources allocated for a timer device.
 *
 * RETURNS: OK or ERROR if the parameter is invalid
 *
 * ERRNO: N/A
 */

LOCAL STATUS vxbFtWdtRelease
    (
    VXB_DEVICE_ID   pInst,
    void *          pCookie
    )
    {
    FT_WDT_TIMER * pTimer = NULL;
    
    /* validate the parameters */

    if ((pInst == NULL) || (pCookie == NULL))
        return ERROR;

    pTimer = (FT_WDT_TIMER *) pInst->pDrvCtrl;

    /* validate pInst and check to see if the timer is allocated */

    if ((pTimer->pDev != pInst) || (!pTimer->timerFunc.allocated))
        return ERROR;
    
    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    /* disable the timer */
    
    if (vxbFtWdtDisable (pCookie) != OK)
        {
        /* release the spinlock */

        SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
        return ERROR;
        }

    pTimer->pIsrFunc = NULL;
    pTimer->arg = 0;

    /* reset the timer allocated flag */

    pTimer->timerFunc.allocated = FALSE;
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
    
    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtRolloverGet - retrieve the maximum value of the counter
*
* This routine retrieves the maximum value of the counter.
*
* RETURNS: OK
*
* ERRNO: N/A
*/
   
LOCAL STATUS vxbFtWdtRolloverGet
    (
    void *  pCookie,
    UINT32 *    count
    )
    {
    FT_WDT_TIMER * pTimer = NULL;
    
    if ((pCookie == NULL) || (count == NULL))
        return ERROR;
    
    pTimer = (FT_WDT_TIMER *) pCookie;
    
    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    /* store the counter value */
    
    *count = pTimer->timerVal;
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
    
    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtCountGet - retrieve the current value of the counter
*
* This routine retrieves the current value of the counter.
*
* RETURNS: OK or ERROR if the parameter is invalid
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtWdtCountGet
    (
    void *  pCookie,
    UINT32 *    count
    )
    {
    FT_WDT_TIMER * pTimer = NULL;
    VXB_DEVICE_ID       pDev;
    UINT64      cnt64;
    UINT32      cnt32;
    
    if ((pCookie == NULL) || (count == NULL))
        return ERROR;
    
    pTimer = (FT_WDT_TIMER *) pCookie;
    pDev = pTimer->pDev ;
    
    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    /* the timer counter is from the lower 32 bits of the 64 bits register */

    cnt64 = __inline__GetPhyTimerCnt ();
    cnt32 = (UINT32)(cnt64 % ((UINT32)WDT_MAX_COUNT + 1));
    *count = cnt32;
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
    
    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtISRSet - set a function to be called on the timer interrupt
*
* This routine sets a function to be called on the timer interrupt.
*
* RETURNS: OK
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtWdtISRSet
    (
    void *  pCookie,
    void    (*pFunc)(int),
    int     arg
    )
    {
    FT_WDT_TIMER * pTimer = NULL;
    
    if ((pCookie == NULL) || (pFunc == NULL))
        return ERROR;
    
    pTimer = (FT_WDT_TIMER *) pCookie;
    
    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    /* store the interrupt routine and argument information */
    
    pTimer->pIsrFunc = pFunc;
    pTimer->arg = arg;
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
    
    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtEnable - enable the timer
*
* This routine updates the maximum count value and enables the timer.
*
* RETURNS: OK or ERROR if timer is not enabled
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtWdtEnable
    (
    void *  pCookie,
    UINT32  maxTimerCount
    )
    {
    VXB_DEVICE_ID       pDev;
    FT_WDT_TIMER * pTimer = NULL;
    struct vxbTimerFunctionality * pTF;
   
    /* validate the parameters */
    
    if ((pCookie == NULL) || (maxTimerCount == 0) || 
        (maxTimerCount > (UINT32)WDT_MAX_COUNT ))
        return ERROR;
    
    pTimer = (FT_WDT_TIMER *) pCookie;
    pDev = pTimer->pDev ;
    pTF = &pTimer->timerFunc;
     
    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    if (pTimer->isEnabled)
        {
        if (vxbFtWdtDisable (pCookie) != OK)
            {
            /* release the spinlock */

            SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
            return ERROR;
            }
        }

    /* save the clock speed */
    
    pTimer->timerVal = maxTimerCount;
    
    /* update rolloverPeriod */
    
    pTF->rolloverPeriod = pTimer->timerVal / (pTF->clkFrequency);
    
    /* set timeout value */
    
    CSR_WRITE_4(pDev, FT_WDT_WOR, pTimer->timerVal);
   
    CSR_WRITE_4(pDev, FT_WDT_WCS, FT_WDT_WCS_EN);

    if (vxbIntEnable (pDev, 0, vxbFtWdtIsr, (void *) pDev) != OK)
        {
        /* release the spinlock */

        SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);            
        return ERROR;
        }
    
    /* set the enabled flag */
    
    pTimer->isEnabled = TRUE;
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);
    
    return OK;
    
    }

/*******************************************************************************
*
* vxbFtWdtDisable - disable the timer
*
* This routine disables the timer.
*
* RETURNS: OK
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtWdtDisable
    (
    void *  pCookie
    )
    {
    FT_WDT_TIMER * pTimer = NULL;
    VXB_DEVICE_ID       pDev;
    
    if (pCookie == NULL)
        return ERROR;
    
    pTimer = (FT_WDT_TIMER *) pCookie;
    
    pDev = pTimer->pDev;
    
    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    /* reset WDT */
    
    CSR_WRITE_4(pDev, FT_WDT_WCS, 0);
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);

    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtIsr - Alter interal timer interrupt handler
*
* This routine handles the timer interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtWdtIsr
    (
    VXB_DEVICE_ID        pDev   /* Information about the driver */
    )
    {
    FT_WDT_TIMER * pTimer = NULL;
    UINT32 status = 0;
    
    if (pDev == NULL)
        return ;
    
    pTimer = pDev->pDrvCtrl;

    /* take the spinlock for the exclusive access to the timer resources */

    SPIN_LOCK_ISR_TAKE (&pTimer->spinLock);
    
    status = CSR_READ_4(pDev, FT_WDT_WCS);
    if (status & FT_WDT_WCS_WS0)
        {       
        CSR_WRITE_4(pDev, FT_WDT_WRR, 0);
        }
    
    /* release the spinlock */

    SPIN_LOCK_ISR_GIVE (&pTimer->spinLock);

    /* call the ISR hander if one is registered */
    
    if (pTimer->isEnabled && pTimer->pIsrFunc != NULL)
        {
        (*(pTimer->pIsrFunc))(pTimer->arg);
        }
    
    return ;
    }
