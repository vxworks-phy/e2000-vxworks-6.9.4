/* vxbFtcan.h - CAN driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbftcanh
#define __INCvxbftcanh

#ifdef __cplusplus
extern "C" {
#endif

#include <isrDeferLib.h>
#include "defs.h"

/* register definition */
enum phytium_can_reg {
    CAN_CTRL            = 0x00,     /* Global control register */
    CAN_INTR            = 0x04,     /* Interrupt register */
    CAN_ARB_RATE_CTRL   = 0x08,     /* Arbitration rate control register */
    CAN_DAT_RATE_CTRL   = 0x0c,     /* Data rate control register */
    CAN_ACC_ID0         = 0x10,     /* Acceptance identifier0 register */
    CAN_ACC_ID1         = 0x14,     /* Acceptance identifier1 register */
    CAN_ACC_ID2         = 0x18,     /* Acceptance identifier2 register */
    CAN_ACC_ID3         = 0x1c,     /* Acceptance identifier3 register */
    CAN_ACC_ID0_MASK    = 0x20,     /* Acceptance identifier0 mask register */
    CAN_ACC_ID1_MASK    = 0x24,     /* Acceptance identifier1 mask register */
    CAN_ACC_ID2_MASK    = 0x28,     /* Acceptance identifier2 mask register */
    CAN_ACC_ID3_MASK    = 0x2c,     /* Acceptance identifier3 mask register */
    CAN_XFER_STS        = 0x30,     /* Transfer status register */
    CAN_ERR_CNT         = 0x34,     /* Error counter register */
    CAN_FIFO_CNT        = 0x38,     /* FIFO counter register */
    CAN_DMA_CTRL        = 0x3c,     /* DMA request control register */
    CAN_XFER_EN         = 0x40,     /* Transfer enable register */
    CAN_INTR1           = 0x44,     /* Interrupt register 1 */
    CAN_FRM_INFO        = 0x48,     /* Frame valid number register */
    CAN_TIME_OUT        = 0x4c,     /* Timeout register */
    CAN_TIME_OUT_CNT    = 0x50,     /* Timeout counter register */
    CAN_INTR2           = 0x54,     /* Interrupt register 2 */
    CAN_TX_FIFO         = 0x100,    /* TX FIFO shadow register */
    CAN_RX_FIFO         = 0x200,    /* RX FIFO shadow register */
    CAN_RX_INFO_FIFO    = 0x300,    /* RX information FIFO shadow register */
    CAN_PIDR4           = 0xfd0,    /* Peripheral Identification Register 4 */
    CAN_PIDR0           = 0xfe0,    /* Peripheral Identification Register 0 */
    CAN_PIDR1           = 0xfe4,    /* Peripheral Identification Register 1 */
    CAN_PIDR2           = 0xfe8,    /* Peripheral Identification Register 2 */
    CAN_PIDR3           = 0xfec,    /* Peripheral Identification Register 3 */
    CAN_CIDR0           = 0xff0,    /* Component Identification Register 0 */
    CAN_CIDR1           = 0xff4,    /* Component Identification Register 1 */
    CAN_CIDR2           = 0xff8,    /* Component Identification Register 2 */
    CAN_CIDR3           = 0xffc,    /* Component Identification Register 3 */
};

/* Global control register (CTRL) */
#define CTRL_XFER       BIT(0)  /* Transfer enable */
#define CTRL_TXREQ      BIT(1)  /* Transmit request */
#define CTRL_AIME       BIT(2)  /* Acceptance identifier mask enable */
#define CTRL_TTS        BIT(3)  /* Transmit trigger strategy */
#define CTRL_RST        BIT(7)  /* Write 1 to soft reset and self clear */
#define CTRL_RFEIDF     BIT(8)  /* Allow RX frame end interrupt during ID filtered frame */
#define CTRL_RFEDT      BIT(9)  /* Allow RX frame end interrupt during TX frame */
#define CTRL_IOF        BIT(10) /* Ignore overload flag internally */
#define CTRL_FDCRC      BIT(11) /* CANFD CRC mode */

/* Interrupt register (INTR) */
#define INTR_BOIS   BIT(0)  /* Bus off interrupt status */
#define INTR_PWIS   BIT(1)  /* Passive warning interrupt status */
#define INTR_PEIS   BIT(2)  /* Passive error interrupt status */
#define INTR_RFIS   BIT(3)  /* RX FIFO full interrupt status */
#define INTR_TFIS   BIT(4)  /* TX FIFO empty interrupt status */
#define INTR_REIS   BIT(5)  /* RX frame end interrupt status */
#define INTR_TEIS   BIT(6)  /* TX frame end interrupt status */
#define INTR_EIS    BIT(7)  /* Error interrupt status */
#define INTR_BOIE   BIT(8)  /* Bus off interrupt enable */
#define INTR_PWIE   BIT(9)  /* Passive warning interrupt enable */
#define INTR_PEIE   BIT(10) /* Passive error interrupt enable */
#define INTR_RFIE   BIT(11) /* RX FIFO full interrupt enable */
#define INTR_TFIE   BIT(12) /* TX FIFO empty interrupt enable */
#define INTR_REIE   BIT(13) /* RX frame end interrupt enable */
#define INTR_TEIE   BIT(14) /* TX frame end interrupt enable */
#define INTR_EIE    BIT(15) /* Error interrupt enable */
#define INTR_BOIC   BIT(16) /* Bus off interrupt clear */
#define INTR_PWIC   BIT(17) /* Passive warning interrupt clear */
#define INTR_PEIC   BIT(18) /* Passive error interrupt clear */
#define INTR_RFIC   BIT(19) /* RX FIFO full interrupt clear */
#define INTR_TFIC   BIT(20) /* TX FIFO empty interrupt clear */
#define INTR_REIC   BIT(21) /* RX frame end interrupt clear */
#define INTR_TEIC   BIT(22) /* TX frame end interrupt clear */
#define INTR_EIC    BIT(23) /* Error interrupt clear */

#define INTR_STATUS_MASK (INTR_BOIS | INTR_PWIS | INTR_PEIS | INTR_RFIS | \
              INTR_TFIS | INTR_REIS | INTR_TEIS | INTR_EIS)
#define INTR_EN_MASK     (INTR_BOIE | INTR_PWIE | INTR_PEIE | INTR_RFIE | \
              INTR_REIE | INTR_TEIE | INTR_EIE)
#define INTR_CLEAR_MASK  (INTR_BOIC | INTR_PWIC | INTR_PEIC | INTR_RFIC | \
              INTR_TFIC | INTR_REIC | INTR_TEIC | INTR_EIC)

/* Arbitration rate control register (ARB_RATE_CTRL) */
#define ARB_RATE_CTRL_ARJW  GENMASK(1, 0)   /* Arbitration field resync jump width */
#define ARB_RATE_CTRL_APRS  GENMASK(4, 2)   /* Arbitration field propagation segment */
#define ARB_RATE_CTRL_APH1S GENMASK(7, 5)   /* Arbitration field phase1 segment */
#define ARB_RATE_CTRL_APH2S GENMASK(10, 8)  /* Arbitration field phase2 segment */
#define ARB_RATE_CTRL_APD   GENMASK(28, 16) /* Arbitration field prescaler divider */

/* Data rate control register (DAT_RATE_CTRL) */
#define DAT_RATE_CTRL_DRJW  GENMASK(1, 0)   /* Data field resync jump width */
#define DAT_RATE_CTRL_DPRS  GENMASK(4, 2)   /* Data field propagation segment */
#define DAT_RATE_CTRL_DPH1S GENMASK(7, 5)   /* Data field phase1 segment */
#define DAT_RATE_CTRL_DPH2S GENMASK(10, 8)  /* Data field phase2 segment */
#define DAT_RATE_CTRL_DPD   GENMASK(28, 16) /* Data field prescaler divider */

/* Acceptance identifierX register (ACC_IDX) */
#define ACC_IDX_AID_MASK    GENMASK(28, 0)  /* Acceptance identifier */

/* Acceptance identifier0 mask register (ACC_ID0_MASK) */
#define ACC_IDX_MASK_AID_MASK   GENMASK(28, 0)  /* Acceptance identifier mask */

/* Transfer status register (XFER_STS) */
#define XFER_STS_FRAS       GENMASK(2, 0)   /* Frame status */
#define XFER_STS_FIES       GENMASK(7, 3)   /* Field status */
#define XFER_STS_FIES_IDLE         (0x0)   /* idle */
#define XFER_STS_FIES_ARBITRATION  (0x1)   /* arbitration */
#define XFER_STS_FIES_TX_CTRL      (0x2)   /* transmit control */
#define XFER_STS_FIES_TX_DATA      (0x3)   /* transmit data */
#define XFER_STS_FIES_TX_CRC       (0x4)   /* transmit crc */
#define XFER_STS_FIES_TX_FRM       (0x5)   /* transmit frame */
#define XFER_STS_FIES_RX_CTRL      (0x6)   /* receive control */
#define XFER_STS_FIES_RX_DATA      (0x7)   /* receive data */
#define XFER_STS_FIES_RX_CRC       (0x8)   /* receive crc */
#define XFER_STS_FIES_RX_FRM       (0x9)   /* receive frame */
#define XFER_STS_FIES_INTERMISSION (0xa)   /* intermission */
#define XFER_STS_FIES_TX_SUSPD     (0xb)   /* transmit suspend */
#define XFER_STS_FIES_BUS_IDLE     (0xc)   /* bus idle */
#define XFER_STS_FIES_OVL_FLAG     (0xd)   /* overload flag */
#define XFER_STS_FIES_OVL_DLM      (0xe)   /* overload delimiter */
#define XFER_STS_FIES_ERR_FLAG     (0xf)   /* error flag */
#define XFER_STS_FIES_ERR_DLM      (0x10)  /* error delimiter */
#define XFER_STS_FIES_BUS_OFF      (0x11)  /* bus off */
#define XFER_STS_TS                BIT(8)  /* Transmit status */
#define XFER_STS_RS                BIT(9)  /* Receive status */
#define XFER_STS_XFERS             BIT(10) /* Transfer status */

/* Error counter register (ERR_CNT) */
#define ERR_CNT_REC         GENMASK(8, 0)   /* Receive error counter */
#define ERR_CNT_TEC         GENMASK(24, 16) /* Transmit error counter */

/* FIFO counter register (FIFO_CNT) */
#define FIFO_CNT_RFN        GENMASK(6, 0)   /* Receive FIFO valid data number */
#define FIFO_CNT_TFN        GENMASK(22, 16) /* Transmit FIFO valid data number */

/* DMA request control register (DMA_CTRL) */
#define DMA_CTRL_RFTH       GENMASK(5, 0)   /* Receive FIFO DMA request threshold */
#define DMA_CTRL_RFRE       BIT(6)      /* Receive FIFO DMA request enable */
#define DMA_CTRL_TFTH       GENMASK(21, 16) /* Transmit FIFO DMA request threshold */
#define DMA_CTRL_TFRE       BIT(22)     /* Transmit FIFO DMA request enable */

/* Transfer enable register (XFER_EN) */
#define XFER_EN_XFER        BIT(0)      /* Transfer enable */

/* Interrupt register 1 (INTR1) */
#define INTR1_RF1IS BIT(0)  /* RX FIFO 1/4 interrupt status */
#define INTR1_RF2IS BIT(1)  /* RX FIFO 1/2 interrupt status */
#define INTR1_RF3IS BIT(2)  /* RX FIFO 3/4 interrupt status */
#define INTR1_RF4IS BIT(3)  /* RX FIFO full interrupt status */
#define INTR1_TF1IS BIT(4)  /* TX FIFO 1/4 interrupt status */
#define INTR1_TF2IS BIT(5)  /* TX FIFO 1/2 interrupt status */
#define INTR1_TF3IS BIT(6)  /* TX FIFO 3/4 interrupt status */
#define INTR1_TF4IS BIT(7)  /* TX FIFO full interrupt status */
#define INTR1_RF1IE BIT(8)  /* RX FIFO 1/4 interrupt enable */
#define INTR1_RF2IE BIT(9)  /* RX FIFO 1/2 interrupt enable */
#define INTR1_RF3IE BIT(10) /* RX FIFO 3/4 interrupt enable */
#define INTR1_RF4IE BIT(11) /* RX FIFO full interrupt enable */
#define INTR1_TF1IE BIT(12) /* TX FIFO 1/4 interrupt enable */
#define INTR1_TF2IE BIT(13) /* TX FIFO 1/2 interrupt enable */
#define INTR1_TF3IE BIT(14) /* TX FIFO 3/4 interrupt enable */
#define INTR1_TF4IE BIT(15) /* TX FIFO full interrupt enable */
#define INTR1_RF1IC BIT(16) /* RX FIFO 1/4 interrupt clear */
#define INTR1_RF2IC BIT(17) /* RX FIFO 1/2 interrupt clear */
#define INTR1_RF3IC BIT(18) /* RX FIFO 3/4 interrupt clear */
#define INTR1_RF4IC BIT(19) /* RX FIFO full interrupt clear */
#define INTR1_TF1IC BIT(20) /* TX FIFO 1/4 interrupt clear */
#define INTR1_TF2IC BIT(21) /* TX FIFO 1/2 interrupt clear */
#define INTR1_TF3IC BIT(22) /* TX FIFO 3/4 interrupt clear */
#define INTR1_TF4IC BIT(23) /* TX FIFO full interrupt clear */
#define INTR1_RF1RIS    BIT(24) /* RX FIFO 1/4 raw interrupt status */
#define INTR1_RF2RIS    BIT(25) /* RX FIFO 1/2 raw interrupt status */
#define INTR1_RF3RIS    BIT(26) /* RX FIFO 3/4 raw interrupt status */
#define INTR1_RF4RIS    BIT(27) /* RX FIFO full raw interrupt status */
#define INTR1_TF1RIS    BIT(28) /* TX FIFO 1/4 raw interrupt status */
#define INTR1_TF2RIS    BIT(29) /* TX FIFO 1/2 raw interrupt status */
#define INTR1_TF3RIS    BIT(30) /* TX FIFO 3/4 raw interrupt status */
#define INTR1_TF4RIS    BIT(31) /* TX FIFO full raw interrupt status */

/* Frame valid number register (FRM_INFO) */
#define FRM_INFO_RXFC   GENMASK(5, 0)   /* Valid frame number in RX FIFO */
#define FRM_INFO_SSPD   GENMASK(31, 16) /* Secondary sample point delay */

/* Interrupt register 2 (INTR2) */
#define INTR2_TOIS  BIT(0)  /* RX FIFO time out interrupt status */
#define INTR2_TOIM  BIT(8)  /* RX FIFO time out interrupt mask */
#define INTR2_TOIC  BIT(16) /* RX FIFO time out interrupt clear */
#define INTR2_TORIS BIT(24) /* RX FIFO time out raw interrupt status */

/* RX information FIFO shadow register (RX_INFO_FIFO) */
#define RX_INFO_FIFO_WNORF  GENMASK(4, 0)   /* Word (4-byte) number of current receive frame */
#define RX_INFO_FIFO_RORF   BIT(5)      /* RTR value of current receive frame */
#define RX_INFO_FIFO_FORF   BIT(6)      /* FDF value of current receive frame */
#define RX_INFO_FIFO_IORF   BIT(7)      /* IDE value of current receive frame */


#define CAN_SFF_ID_BITS     11
#define CAN_EFF_ID_BITS     29

#define FTCAN_ERR_CNT_TFN_SHIFT     16  /* Tx Error Count shift */
#define FTCAN_FIFO_CNT_TFN_SHIFT    16  /* Tx FIFO Count shift*/
#define FTCAN_IDR_ID1_SHIFT         21  /* Standard Messg Identifier */
#define FTCAN_IDR_ID2_SHIFT         1   /* Extended Message Identifier */
#define FTCAN_IDR_SDLC_SHIFT        14
#define FTCAN_IDR_EDLC_SHIFT        26
#define FTCAN_ACC_IDN_SHIFT         18  /*Standard ACC ID shift*/

#define FTCANFD_IDR1_SDLC_SHIFT     11
#define FTCANFD_IDR_SDLC_SHIFT      17
#define FTCANFD_IDR_EDLC_SHIFT      24
#define FTCANFD_IDR_GET_EDLC_SHIFT  12

#define FTCAN_IDR_ID2_MASK          0x0007FFFE /* Extended message ident */
#define FTCAN_IDR_ID1_MASK          0xFFE00000 /* Standard msg identifier */
#define FTCAN_IDR_IDE_MASK          0x00080000 /* Identifier extension */
#define FTCAN_IDR_SRR_MASK          0x00100000 /* Substitute remote TXreq */
#define FTCAN_IDR_RTR_MASK          0x00000001 /* Extended frames remote TX request */
#define FTCAN_IDR_DLC_MASK          0x0003C000 /* Standard msg dlc */
#define FTCAN_IDR_PAD_MASK          0x00003FFF /* Standard msg padding 1 */
#define FTCAN_IDR_EDLC_MASK         0x3C000000 /* Extended msg dlc */

#define FTCANFD_ID1_FDL_MASK    0x00040000 /* CANFD Standard FDL */
#define FTCANFD_ID1_BRS_MASK    0x00010000 /* CANFD Standard BRS */
#define FTCANFD_ID1_ESI_MASK    0x00008000 /* CANFD Standard ESI */
#define FTCANFD_ID1_SDLC_MASK   0x00007800 /* CANFD Standard msg dlc */

#define FTCANFD_ID2_FDL_MASK    0x80000000 /* CANFD Extended FDL */
#define FTCANFD_ID2_BRS_MASK    0x20000000 /* CANFD Extended BRS */
#define FTCANFD_ID2_ESI_MASK    0x10000000 /* CANFD Extended ESI */
#define FTCANFD_ID2_EDLC_MASK   0x0F000000 /* CANFD Extended msg dlc */
#define FTCANFD_IDR_PAD_MASK    0x000007FF /* CANFD Standard msg padding 1 */

#define cpu_to_be32p(x) (htobe32((uint32_t)x))
#define be32_to_cpup(x) (be32toh((uint32_t)x))

#define CAN_QUEUE_SIZE  10240
#define CAN_FIFO_BYTE_LEN       256
#define KEEP_CAN_FIFO_MIN_LEN   16
#define KEEP_CANFD_FIFO_MIN_LEN	128

/* CAN payload length and DLC definitions according to ISO 11898-1 */
#define CAN_MAX_DLC     8
#define CAN_MAX_DLEN    8
/* CAN FD payload length and DLC definitions according to ISO 11898-7 */
#define CANFD_MAX_DLC   15
#define CANFD_MAX_DLEN  64
/*defined bits for canfd_frame.flags*/
#define CANFD_BRS       0x01 /* bit rate switch (second bitrate for payload data) */
#define CANFD_ESI       0x02 /* error state indicator of the transmitting node */

#define CAN_MAX_CTL     2

/* special address description flags for the CAN_ID */
#define CAN_EFF_FLAG 0x80000000U /* EFF/SFF is set in the MSB */
#define CAN_RTR_FLAG 0x40000000U /* remote transmission request */
#define CAN_ERR_FLAG 0x20000000U /* error message frame */

/* valid bits in CAN ID for frame formats */
#define CAN_SFF_MASK 0x000007FFU /* standard frame format (SFF) */
#define CAN_EFF_MASK 0x1FFFFFFFU /* extended frame format (EFF) */
#define CAN_ERR_MASK 0x1FFFFFFFU /* omit EFF, RTR, ERR flags */

/* Frame type */
#define STANDARD_FRAME  0 /* standard frame */
#define EXTEND_FRAME    1 /* extended frame */

/*
 * Controller Area Network Identifier structure
 *
 * bit 0-28 : CAN identifier (11/29 bit)
 * bit 29   : error message frame flag (0 = data frame, 1 = error message)
 * bit 30   : remote transmission request flag (1 = rtr frame)
 * bit 31   : frame format flag (0 = standard 11 bit, 1 = extended 29 bit)
 */

#define min_t(type, x, y) ({            \
    type __min1 = (x);          \
    type __min2 = (y);          \
    __min1 < __min2 ? __min1: __min2; })

/*
 * get_can_dlc(value) - helper macro to cast a given data length code (dlc)
 * to __u8 and ensure the dlc value to be max. 8 bytes.
 *
 * To be used in the CAN netdriver receive path to ensure conformance with
 * ISO 11898-1 Chapter 8.4.2.3 (DLC field)
 */
#define get_can_dlc(i)       (min_t(UINT8, (i), CAN_MAX_DLC))


#define FTCAN_ACC_IDN_SHIFT  18  /*Standard ACC ID shift*/
#define FTCAN_TX_FIFO_MAX    0x40

#define CAN_CALC_MAX_ERROR   50 /* in one-tenth of a percent */
#define CAN_CALC_SYNC_SEG    1
#define clamp(x, low, high)  (min(max(low, x), high))
#define CAN_CLK_FREQ         200000000
/*
 * CAN bit-timing parameters
 *
 * For further information, please read chapter "8 BIT TIMING
 * REQUIREMENTS" of the "Bosch CAN Specification version 2.0"
 * at http://www.semiconductors.bosch.de/pdf/can2spec.pdf.
 */
typedef struct can_bittiming {
    UINT bitrate;       /* Bit-rate in bits/second */
    UINT samplePoint;  /* Sample point in one-tenth of a percent */
    UINT tq;        /* Time quanta (TQ) in nanoseconds */
    UINT propSeg;      /* Propagation segment in TQs */
    UINT phaseSeg1;    /* Phase buffer segment 1 in TQs */
    UINT phaseSeg2;    /* Phase buffer segment 2 in TQs */
    UINT sjw;       /* Synchronisation jump width in TQs */
    UINT brp;       /* Bit-rate prescaler */
} FTCAN_BITTIMING;

/*
 * CAN harware-dependent bit-timing constant
 *
 * Used for calculating and checking bit-timing parameters
 */
typedef struct can_bittiming_const {
    char name[16];      /* Name of the CAN controller hardware */
    UINT tseg1Min; /* Time segement 1 = propSeg + phaseSeg1 */
    UINT tseg1Max;
    UINT tseg2Min; /* Time segement 2 = phaseSeg2 */
    UINT tseg2Max;
    UINT sjwMax;       /* Synchronisation jump width */
    UINT brpMin;       /* Bit-rate prescaler */
    UINT brpMax;
    UINT brpInc;
} FTCAN_BITTIMING_CONST;

typedef struct can_frame {
    UINT     canId;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
    UINT8    len; /* frame payload length in byte (0 .. CAN_MAX_DLEN) */
    UINT8    pad;     /* padding */
    UINT8    data[CAN_MAX_DLEN];
} FTCAN_FRAME;

typedef struct canfd_frame {
    UINT     canId;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
    UINT8    len;     /* frame payload length in byte (0 .. CANFD_MAX_DLEN) */
    UINT8    flags;   /* additional flags for CAN FD */
    UINT8    data[CANFD_MAX_DLEN];
} FTCANFD_FRAME;

typedef struct ftCan_drv_ctrl
    {
    VXB_DEVICE_ID   ftCanDev;
    void *      ftCanHandle;
    void *      ftCanRegbase;
    UINT        irq;
	UINT        isTxFifoFull;
    FTCAN_BITTIMING ftCanBittiming;
    FTCAN_BITTIMING ftCanDataBittiming;
    void (*ftCanRecvRtn)(FTCANFD_FRAME* pFtCanFrame);
    SEM_ID      canTxSem;
    SEM_ID      canRxSem;
	MSG_Q_ID    canRxQueue;
    UINT        isCanFD;
    } FTCAN_DRV_CTRL;


#define FTCAN_BASE(p)   ((FTCAN_DRV_CTRL *)(p)->pDrvCtrl)->ftCanRegbase
#define FTCAN_HANDLE(p)   ((FTCAN_DRV_CTRL *)(p)->pDrvCtrl)->ftCanHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FTCAN_HANDLE(pDev), (UINT32 *)((char *)FTCAN_BASE(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FTCAN_HANDLE(pDev),                             \
        (UINT32 *)((char *)FTCAN_BASE(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (UINT32)(~(val)))
    
    

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (FTCAN_HANDLE(pDev),              \
        (UINT16 *)((char *)FTCAN_BASE(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)               \
    vxbWrite16 (FTCAN_HANDLE(pDev),             \
        (UINT16 *)((char *)FTCAN_BASE(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (FTCAN_HANDLE(pDev),               \
        ((UINT8 *)FTCAN_BASE(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)               \
    vxbWrite8 (FTCAN_HANDLE(pDev),              \
        ((UINT8 *)FTCAN_BASE(pDev) + addr), data)

STATUS ftCanRecvCallback
    (
    unsigned char ctlNo,
    void (*ftCanRecvRtn)(FTCANFD_FRAME *pFtCanFrame)
    );
STATUS ftCanSend
    (
    unsigned char ctlNo,
    FTCANFD_FRAME *sendFrame
    );

#ifdef __cplusplus
}
#endif

#endif /*end of __INCvxbftcanh*/
