/* vxbFtSpi.h - Driver for spi controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbFtSpih
#define __INCvxbFtSpih

#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbSpiLib.h>
#include "defs.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/* defines */

#define FT_SPI_NAME            "ftSpi"

/* register addresses */
#define SPI_CTRL0         0x00      /*Ctrl register 0*/
#define SPI_CTRL1         0x04      /*Ctrl register 1*/
#define SPI_SSI_EN        0x08      /*SPI enable register*/
#define SPI_MWC           0x0c      /*Microwire ctrl register*/
#define SPI_SE            0x10      /*Slave enable register*/
#define SPI_BAUD          0x14      /*Baudrate set register*/
#define SPI_TXFTL         0x18      /*Tx threshold register*/
#define SPI_RXFTL         0x1c      /*Rx threshold register*/
#define SPI_TXFL          0x20      /*Tx level register*/
#define SPI_RXFL          0x24      /*Rx level register*/
#define SPI_S             0x28      /*Status register*/
#define SPI_IM            0x2c      /*Intr mask register*/
#define SPI_ISR           0x30      /* Irq Status register */
#define SPI_RIS           0x34      /*Intr status register*/
#define SPI_TXOIC         0x38      /*TX FIFO overflow intr clear register*/
#define SPI_RXOIC         0x3c      /*RX FIFO overflow intr clear register*/
#define SPI_RXUIC         0x40      /*TX FIFO underflow intr clear register*/
#define SPI_MSTIC         0x44      /*Multi slave intr clear register*/
#define SPI_IC            0x48      /*Intr clear register*/
#define SPI_DMAC          0x4c      /*DMA ctrl register*/
#define SPI_DMA_TDL       0x50      /*DMA TX Data level register*/
#define SPI_DMA_RDL       0x54      /*DMA RX Data level register*/
#define SPI_ID            0x58      /*Identification register*/
#define SPI_D             0x60      /*Data register*/
#define SPI_RX_SAMPLE_DLY 0xfc      /*RX Data delay register*/
#define SPI_CS_OFFSET     0x100     /* Chip selection register */  


/*Ctrl register 0*/
#define SPI_CTRL0_CFS_OFFSET            12
#define SPI_CTRL0_SRL                   BIT(11)
#define SPI_CTRL0_SLV_OE                BIT(10)
#define SPI_CTRL0_TMOD_OFFSET           8
#define SPI_CTRL0_SCPOL                 BIT(7)
#define SPI_CTRL0_SCPH                  BIT(6)
#define SPI_CTRL0_FRF_OFFSET            4
#define SPI_CTRL0_DFS_OFFSET            0

#define SPI_CTRL0_TMOD_MASK             (0x3 << SPI_CTRL0_TMOD_OFFSET)
#define SPI_CTRL0_TMOD_RW               (0x0)
#define SPI_CTRL0_TMOD_W                (0x1)
#define SPI_CTRL0_TMOD_R                (0x2)
#define SPI_CTRL0_TMOD_EEP              (0x3)
#define SPI_CTRL0_TMOD(x)               ((x) << SPI_CTRL0_TMOD_OFFSET)
#define SPI_CTRL0_DFS_MASK              (0xf << SPI_CTRL0_DFS_OFFSET)
#define SPI_CTRL0_DFS(x)                (((x-1)&0xf) << SPI_CTRL0_DFS_OFFSET)

#define SPI_CTRL0_DEFAULT               (SPI_CTRL0_TMOD(SPI_CTRL0_TMOD_RW) | SPI_CTRL0_DFS(8))

#define SPI_CHARLEN_MIN                 (2)
#define SPI_CHARLEN_MAX                 (0x10)

/*SPI enable register*/
#define SPI_SSI_ENA                     1
#define SPI_SSI_DIS                     0

/*Slave enable register*/
#define SPI_SE_CHAN(chan)               (1 << chan)

/*Status register*/
#define SPI_S_DCOL                     BIT(6) 
#define SPI_S_TXE                      BIT(5) 
#define SPI_S_RFF                      BIT(4) 
#define SPI_S_RFNE                     BIT(3) 
#define SPI_S_TFE                      BIT(2) 
#define SPI_S_TFNF                     BIT(1) 
#define SPI_S_BUSY                     BIT(0) 

/*Intr mask register*/
#define SPI_INT_TXEI                   BIT(0)
#define SPI_INT_TXOI                   BIT(1)
#define SPI_INT_RXUI                   BIT(2)
#define SPI_INT_RXOI                   BIT(3)

/*DMA Control Register*/

#define SPI_DMAC_RDMAE                 BIT(0) 
#define SPI_DMAC_TDMAE                 BIT(1)

/*RX Data delay register*/
#define SPI_DEFAULT_RSD                 0x6

/* chip selects supported */

#define SPI_MAX_CS_NUM                 (4)

/*SPI_CS_OFFSET Register*/
#define SPI_NUM_OF_CS                     4U
#define SPI_CHIP_SEL_EN(cs)               BIT((cs) + SPI_MAX_CS_NUM) /* 1: enable chip selection */
#define SPI_CHIP_SEL_EN_ALL               (0xf0)
#define SPI_CHIP_SEL(cs)                  BIT(cs)

/* device default value */

#define SPI_CLK_DEFAULT                 (48000000)
#define SPI_TIMEOUT_US                  (3000000)
#define SPIFMT_WDELAY_DEFAULT           (0x10)
#define SPIFMT_C2TDELAY_DEFAULT         (0x30)
#define SPIFMT_T2CDELAY_DEFAULT         (0x30)

/* time out for interrupt mode transfer */

#define SPI_TIMEOUT_COUNT               (5)   
#define SPI_TIMEOUT_EXTRA               (20)

/* SPI rw modes */

#define SPI_RW_MODE_INT                 (0)
#define SPI_RW_MODE_POLL                (1)

/* typedefs */

/* structure holding the instance specific details */

typedef struct ftSpiCtrl
    {
    VXB_DEVICE_ID    pDev;
    void *           regBase;
    void *           regHandle;
    UINT32           rwMode;
    UINT32           clkFrequency;
    UINT32           initPhase;
    UINT32           csNum;
    UINT8 *          txBuf;
    UINT8 *          txBufOriginal;    
    UINT8 *          txBufEnd;
    UINT8 *          rxBuf;
    UINT8 *          rxBufOriginal;    
    UINT8 *          rxBufEnd;
    UINT32           transLen;
    UINT32           alignSize;
    UINT32           curWorkingFrq;
    UINT32           wdelay;
    UINT32           c2tdelay;
    UINT32           t2cdelay;
    UINT32           txFifoLen;
    UINT32           rxFifoLen;
    BOOL             useInt1;
    BOOL             initDone;
    SEM_ID           semSyncInt;
    SEM_ID           muxSem;
    VXB_SPI_BUS_CTRL vxbSpiCtrl;
    } FT_SPI_CTRL;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbTiKeystoneSpih */
