/* vxbFtGDMA.c - Driver for general DMA controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <iv.h>
#include <vmLib.h>
#include <private/vmLibP.h>
#include <taskLib.h>
#include <../h/hwif/util/hwMemLib.h>

#include <cacheLib.h>
#include <excLib.h>
#include <stdlib.h>
#include <string.h>

#include <logLib.h>
#include <stdio.h>

#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/util/vxbDmaLib.h>
#include <hwif/util/vxbParamSys.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include <../src/hwif/h/util/vxbDmaDriverLib.h>
#include <vxbFtGDma.h>

/* defines */

#undef FT_DMA_DEBUG

#ifdef FT_DMA_DEBUG

#ifdef  LOCAL
#undef  LOCAL
#define LOCAL
#endif

void vxbFtDmaRegDump (VXB_DEVICE_ID);
IMPORT FUNCPTR _func_logMsg;
LOCAL int debugLevel = 10000;

#   undef PRINTF_DEBUG
#   ifdef PRINTF_DEBUG
#   define FT_DMA_LOG(lvl,fmt, X1, X2, X3, X4, X5, X6)  \
    if (debugLevel >= lvl)                                 \
        printf(fmt, X1, X2, X3, X4, X5, X6)
#   else /* PRINTF_DEBUG */
#   define FT_DMA_LOG(lvl, fmt, X1, X2, X3, X4, X5, X6) \
       if (debugLevel >= lvl)                              \
          {                                                \
          if (_func_logMsg != NULL)                        \
              _func_logMsg (fmt, X1, X2, X3,\
               X4, X5, X6);                 \
          }
#   endif /* PRINTF_DEBUG */
#else /* FT_DMA_DEBUG */
#   undef  FT_DMA_LOG
#   define FT_DMA_LOG(lvl,fmt,a,b,c,d,e,f)
#endif  /* VXB_FT_DMA_DEBUG */

/* forward declarations */

LOCAL void vxbFtDmaInstInit (VXB_DEVICE_ID);
LOCAL void vxbFtDmaInstInit2 (VXB_DEVICE_ID);
LOCAL void vxbFtDmaInstConnect (VXB_DEVICE_ID);
LOCAL void vxbFtDmaExcJob (VXB_DEVICE_ID, FT_GDMA_CHAN *, STATUS);
LOCAL void vxbFtDmaChanIsr (FT_GDMA_DRV_CTRL *);
LOCAL STATUS vxbFtDmaResourceGet (VXB_DEVICE_ID, VXB_DEVICE_ID, 
                                   struct vxbDmaRequest *);
LOCAL STATUS vxbFtDmaResourceRelease (VXB_DEVICE_ID, VXB_DMA_RESOURCE_ID);
LOCAL STATUS vxbFtDmaInstUnlink (VXB_DEVICE_ID, void *);
LOCAL STATUS vxbFtDmaSubmitJob(VXB_DMA_RESOURCE_ID ,FT_GDMA_DESC *);
LOCAL STATUS vxbFtDmaResume (VXB_DMA_RESOURCE_ID);
LOCAL STATUS vxbFtDmaSuspend (VXB_DMA_RESOURCE_ID);
LOCAL STATUS vxbFtDmaHwInit(VXB_DEVICE_ID);


LOCAL DRIVER_INITIALIZATION vxbFtDmaFuncs =
    {
    vxbFtDmaInstInit,          /* devInstanceInit */
    vxbFtDmaInstInit2,         /* devInstanceInit2 */
    vxbFtDmaInstConnect        /* devConnect */
    };

LOCAL struct vxbPlbRegister ftDmaDevRegistration =
    {
        {
        NULL,                   /* pNext */
        VXB_DEVID_DEVICE,       /* devID */
        VXB_BUSID_PLB,          /* busID = PLB */
        VXB_VER_4_0_0,          /* vxbVersion */
        "ftGDma",               /* drvName */
        &vxbFtDmaFuncs,        /* pDrvBusFuncs */
        NULL,                   /* pMethods */
        NULL                    /* devProbe */
        },
    };

LOCAL device_method_t   vxbFtDmaMethods[] =
    {
    DEVMETHOD(vxbDmaResourceGet,        vxbFtDmaResourceGet),
    DEVMETHOD(vxbDmaResourceRelease,    vxbFtDmaResourceRelease),
    DEVMETHOD(vxbDrvUnlink,             vxbFtDmaInstUnlink),
    {0, 0}
    };

/* imported functions supplied by the BSP */

IMPORT atomicVal_t    vxAtomicSet (atomic_t *, atomicVal_t);
IMPORT atomicVal_t    vxAtomicOr (atomic_t *, atomicVal_t);
IMPORT void           vxbUsDelay(int);

/*******************************************************************************
*
* vxbFtDmaInstInit - first pass initialization
*
* This function implements the VxBus instInit handler for a DMA controller
* device instance.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbFtDmaInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl;
    struct hcfDevice * pHcf;

    if ((pDrvCtrl = (FT_GDMA_DRV_CTRL *)hwMemAlloc (sizeof(FT_GDMA_DRV_CTRL))) == NULL)
        return;

    pDrvCtrl->vxbDev = pDev;

    pDrvCtrl->regBase = pDev->pRegBase[0];
    (void)vxbRegMap (pDev, 0, &pDrvCtrl->handle);

    /* fill in bus subsystem fields */

    pDev->pDrvCtrl = (void *)pDrvCtrl;
    pDev->pMethods = &vxbFtDmaMethods[0];

    pHcf = (struct hcfDevice *)hcfDeviceGet(pDev);
    if (pHcf == NULL)
        {
        (void)hwMemFree ((char *)pDrvCtrl);
        return;
        }

    if (devResourceGet (pHcf, "polling", HCF_RES_INT,
                       (void *) &pDrvCtrl->polling) != OK)
        pDrvCtrl->polling = FALSE;

    pDrvCtrl->ftDmaPhase = 1;
    }

/*******************************************************************************
*
* vxbFtDmaInstInit2 - second pass initialization
*
* This routine performs the second level initialization of the DMA controller.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbFtDmaInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;
    UINT32 chan;
    if (pDrvCtrl->dmaInitialized == TRUE)
        return;

    pDrvCtrl->ftDmaSemId = semBCreate (SEM_Q_FIFO, SEM_EMPTY);

    if (pDrvCtrl->ftDmaSemId == NULL)
        {
        return;
        }
    pDrvCtrl->ftDmaSyncSem = semBCreate (SEM_Q_FIFO, SEM_EMPTY);

    if (pDrvCtrl->ftDmaSyncSem == NULL)
        {
        (void)semDelete (pDrvCtrl->ftDmaSemId);
        return;
        }
    pDrvCtrl->dmaInitialized = TRUE;
    
    pDrvCtrl->chanNum = DMAC_MAX_CHANNELS;
    
    pDrvCtrl->pChan = (FT_GDMA_CHAN **)hwMemAlloc (sizeof(FT_GDMA_CHAN *) *  pDrvCtrl->chanNum);
    if (pDrvCtrl->pChan == NULL)
        {
        (void)semDelete (pDrvCtrl->ftDmaSemId);
        (void)semDelete (pDrvCtrl->ftDmaSyncSem);
        return;
        }

    /* init DMA channels */
    for (chan = 0; chan < pDrvCtrl->chanNum; chan++)
    {
        pDrvCtrl->pChan[chan] = NULL;
    }
    
    vxbFtDmaHwInit(pDev);
    (void)vxbIntConnect (pDev, 0, vxbFtDmaChanIsr, pDrvCtrl);
    (void)vxbIntEnable (pDev, 0, vxbFtDmaChanIsr, pDrvCtrl);
    pDrvCtrl->ftDmaPhase = 2;
    
    FT_DMA_LOG(1000, "[%s]: Finish!  chanNum:%d\n",__func__,
            pDrvCtrl->chanNum, 2, 3, 4, 5);
    }

/*******************************************************************************
*
* vxbFtDmaInstConnect - final initialization
*
* This routine performs the third level initialization of the DMA controller
* driver. Nothing to be done in this routine.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbFtDmaInstConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;

    pDrvCtrl->ftDmaPhase = 3;

    }

/*******************************************************************************
*
* vxbFtDmaRegister - register driver with vxbus
*
* RETURNS: N/A
*
* ERRNO
*/

void vxbFtDmaRegister (void)
    {
    (void)vxbDevRegister ((struct vxbDevRegInfo *)&ftDmaDevRegistration);
    }


LOCAL void vxbFtDmaDisable(VXB_DEVICE_ID pDev)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_CTL);
    val &= ~DMA_CTL_EN;
    DMA_WRITE_32(pDev, DMA_CTL, val);
}

LOCAL void vxbFtDmaEnable(VXB_DEVICE_ID pDev)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_CTL);
    val |= DMA_CTL_EN;
    DMA_WRITE_32(pDev, DMA_CTL, val);
}

LOCAL void vxbFtDmaReset(VXB_DEVICE_ID pDev)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_CTL);
    val |= DMA_CTL_SRST;
    DMA_WRITE_32(pDev, DMA_CTL, val);

    vxbUsDelay(10);
    val &= ~DMA_CTL_SRST;
    DMA_WRITE_32(pDev, DMA_CTL, val);
}

LOCAL void vxbFtDmaChanClkOff(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_LP_CTL);
    val |= DMA_LP_CLK(chanId);
    DMA_WRITE_32(pDev, DMA_LP_CTL, val);
}

LOCAL void vxbFtDmaChanClkOn(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_LP_CTL);
    val &= ~DMA_LP_CLK(chanId);
    DMA_WRITE_32(pDev, DMA_LP_CTL, val);
}

LOCAL void vxbFtDmaIrqDisable(VXB_DEVICE_ID pDev)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_INTR_CTL);
    val &= ~DMA_INTR_GEN;
    DMA_WRITE_32(pDev, DMA_INTR_CTL, val);
}

LOCAL void vxbFtDmaIrqEnable(VXB_DEVICE_ID pDev)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_INTR_CTL);
    val |= DMA_INTR_GEN;
    DMA_WRITE_32(pDev, DMA_INTR_CTL, val);
}


LOCAL UINT32 vxbFtDmaIrqRead(VXB_DEVICE_ID pDev)
{
    UINT32 val;
    
    val = DMA_READ_32(pDev, DMA_STAT);

    return val;
}

LOCAL void vxbFtChanIrqDisable(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;

    val = DMA_READ_32(pDev, DMA_INTR_CTL);
    val &= ~(0x01 << chanId);
    DMA_WRITE_32(pDev, DMA_INTR_CTL, val);
}

LOCAL void vxbFtChanIrqSet(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;

    DMA_CHAN_REG_WRITE(pDev,chanId, DMA_CX_INTR_CTL, DMA_CX_TRANS_END);

    val = DMA_READ_32(pDev, DMA_INTR_CTL);
    val |= (0x01 << chanId);
    DMA_WRITE_32(pDev, DMA_INTR_CTL, val);
}

LOCAL void vxbFtChanIrqClear(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    DMA_CHAN_REG_WRITE(pDev, chanId, DMA_CX_STAT, DMA_CX_TRANS_END);
}

LOCAL UINT32 vxbFtChanIrqRead(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;
    
    val = DMA_READ_32(pDev, DMA_STAT);
    val &= (0x01 << chanId);

    return val;
}

LOCAL UINT32 vxbFtChanStatusRead(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;
    
    val = DMA_CHAN_REG_READ(pDev, chanId, DMA_CX_STAT);
    val &= 0x1F;

    return val;
}

LOCAL void vxbFtChanDisable(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;

    val = DMA_CHAN_REG_READ(pDev, chanId, DMA_CX_CTL);
    val &= ~DMA_CX_EN;
    DMA_CHAN_REG_WRITE(pDev, chanId, DMA_CX_CTL, val);
}

LOCAL void vxbFtChanReset(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;

    val = DMA_CHAN_REG_READ(pDev, chanId, DMA_CX_CTL);
    val |= DMA_CX_SRST;
    DMA_CHAN_REG_WRITE(pDev, chanId, DMA_CX_CTL, val);

    vxbUsDelay(10);
    val &= ~DMA_CX_SRST;
    DMA_CHAN_REG_WRITE(pDev, chanId, DMA_CX_CTL, val);
}

LOCAL void vxbFtChanEnable(VXB_DEVICE_ID pDev, UINT8 chanId)
{   
    UINT32 val;

    val = DMA_CHAN_REG_READ(pDev, chanId, DMA_CX_CTL);
    val |= DMA_CX_EN;
    DMA_CHAN_REG_WRITE(pDev, chanId, DMA_CX_CTL, val);
}

LOCAL BOOL vxbFtChanIsRunning(VXB_DEVICE_ID pDev, UINT8 chanId)
{
    UINT32 val;
    
    val = DMA_CHAN_REG_READ(pDev, chanId, DMA_CX_STAT);

    if(val & DMA_CX_BUSY)
        return TRUE;
    else
        return FALSE;
}

LOCAL STATUS vxbFtDmaHwInit(VXB_DEVICE_ID  pDev)
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;
    UINT32 i;
    UINT32 val;
    
    vxbFtDmaDisable(pDev);
    vxbFtDmaReset(pDev);
    
    val = DMA_READ_32(pDev, DMA_CTL);
    val |= DMA_CTL_WR_ARB;       /* Enable Write Qos mode*/
    val |= DMA_CTL_RD_ARB;       /* Enable Read Qos mode*/
    DMA_WRITE_32(pDev, DMA_CTL, val);

    vxbFtDmaIrqEnable(pDev);
    vxbFtDmaEnable(pDev);

    for (i = 0; i < pDrvCtrl->chanNum; i++) {
        vxbFtChanIrqDisable(pDev, i);
        vxbFtChanDisable(pDev, i);
        vxbFtChanReset(pDev, i);
    }
    return OK;
    }

LOCAL UINT32 vxbFtDmaGetBurstSize(UINT32 addrWidth)
{
    UINT32 size = 0;

    switch(addrWidth) { 
    case DMA_SLAVE_BUSWIDTH_1_BYTE:
        size = 0;
        break;
    case DMA_SLAVE_BUSWIDTH_2_BYTES:
        size = 1;
        break;
    case DMA_SLAVE_BUSWIDTH_4_BYTES:
        size = 2;
        break;
    case DMA_SLAVE_BUSWIDTH_8_BYTES:
        size = 3;
        break;
    case DMA_SLAVE_BUSWIDTH_16_BYTES:
        size = 4;
        break;
    default:
        size = 2;
        break;
    }

    return size;
}
/*******************************************************************************
*
* vxbFtDmaMemCopy - copy memory from one address to another
*
* This routine copies user defined size bytes from source to destination
* address. The final argument is a 32bit unsigned integer used to manage or
* track the completion of vxbFtDmaMemCopy(). Possible usage for userData
* includes semaphore identifiers (SEM_ID), an array index, or as a general
* purpose unique identifier. The routine use the basic Chaining mode transmit the
* DMA data, the maximum transfer size depends on the numOfLinks. Currently,
* the maximum transfer size is about 1GB by default.
*
* RETURNS: OK if there are enough descriptors to fulfill the request
* or ERROR if alignment, address or size constraints would be violated.
*
* SEE ALSO: vxbFtDmaReserveChannel, vxbFtDmaReleaseChannel
*
* \NOMANUAL
*/

LOCAL STATUS vxbFtDmaMemCopy
    (
    VXB_DMA_RESOURCE_ID dmaRes,
    char *              dst,
    char *              src,
    UINT32              size,
    pVXB_DMA_COMPLETE_FN completeFunc,
    void *              pArg
    )
    {
    VXB_DEVICE_ID       pDev;
    FT_GDMA_DRV_CTRL *  pDrvCtrl;
    FT_GDMA_CHAN *      pChan;
    FT_GDMA_DESC *      desc;
    UINT32              ctl, burstSz, val, xfer_cfg;

    /* get the DMA engine vxbus instance id */

    pDev = dmaRes->dmaInst;
    pDrvCtrl = pDev->pDrvCtrl;

    /* get the associated DMA channel data */

    pChan = dmaRes->pDmaChan;
    desc = pChan->chan_desc;

    if (size == 0)
        {
        FT_DMA_LOG(0, "[%s]: size should not be 0!\n",
                                            __func__, 1, 2, 3, 4, 5);
        return ERROR;
        }

    if ((src == (char *)NULL) || (dst == (char *)NULL))
        {
        FT_DMA_LOG(0, "[%s]: src or dst should not be 0!\n",
                                            __func__, 1, 2, 3, 4, 5);
        return ERROR;
        }

    if (CHAN_SYNC_TAKE == ERROR)
        {
        return ERROR;
        }
    
    burstSz = vxbFtDmaGetBurstSize(pChan->dma_sconfig->src_addr_width);
    xfer_cfg = DMA_CX_RDSIZE(burstSz);
    burstSz = vxbFtDmaGetBurstSize(pChan->dma_sconfig->dst_addr_width);
    xfer_cfg |= DMA_CX_WRSIZE(burstSz);
    xfer_cfg |= DMA_CX_RDLEN(pChan->dma_sconfig->src_maxburst);
    xfer_cfg |= DMA_CX_WRLEN(pChan->dma_sconfig->dst_maxburst);
    xfer_cfg |= DMA_CX_WRBURST(DMA_INCR);    
    xfer_cfg |= DMA_CX_RDBURST(DMA_INCR);
    desc->axi_burst = xfer_cfg;
    
    ctl = DMA_READ_32(pDev, DMA_CTL);
    val = 0;
    if (ctl & DMA_CTL_RD_ARB) {
        val |= DMA_CX_RD_QOS(pChan->dma_sconfig->priority);
        val |= DMA_CX_RD_QOS_EN;
    }
    if (ctl & DMA_CTL_WR_ARB) {
        val |= DMA_CX_WR_QOS(pChan->dma_sconfig->priority);
        val |= DMA_CX_WR_QOS_EN;
    }   
 
    desc->dma_mode = val;
    desc->dma_mode &= ~DMA_CX_BDL;

    desc->dma_src_addr_l = (UINT32)src;
    desc->dma_src_addr_h = 0;
    desc->dma_dst_addr_l = (UINT32)dst;
    desc->dma_dst_addr_h = 0;
    desc->data_len = size;
    
    pChan->callback = completeFunc;
    pChan->pArg = pArg;   

    FT_DMA_LOG(1000, "[%s]: src:%x  dst:%x len:%d mode:%d!\n",__func__,
            desc->dma_src_addr_l, desc->dma_dst_addr_l, desc->data_len, desc->dma_mode & DMA_CX_BDL, 5);
    vxbFtDmaSubmitJob (dmaRes, desc);

    CHAN_SYNC_GIVE;
    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaExtChainMode - copy multiple memories via extended chain mode
*
* This routine copies user defined multiple memories. The memories transfer 
* requirement is described by a chain of structure vxbDmaBufferDesc. The 
* paramater pSrcAddr points to that chain's head. The routine uses the extended
* chaining mode to transmit the DMA data. Every node in user's chain consumes 
* one list descriptor and at least one link descriptor. If tranfer size in node is 
* greater than 64M bytes, more link descriptors will be consumed. 
*
* RETURNS: OK if dma transfer starts successfully, otherwise ERROR.
*
* SEE ALSO: vxbFtDmaReserveChannel, vxbFtDmaReleaseChannel
*
* \NOMANUAL
*/

LOCAL STATUS vxbFtDmaChainMode
    (
    VXB_DMA_RESOURCE_ID  dmaRes,
    char *               pSrcAddr,
    pVXB_DMA_COMPLETE_FN completeFunc,
    void *               pArg
    )
    {
    VXB_DEVICE_ID       pDev;
    FT_GDMA_DRV_CTRL *  pDrvCtrl;
    FT_GDMA_CHAN *      pChan;
    struct vxbDmaBufferDesc *   pDmaBufDesc;
    FT_GDMA_DESC*       pChanDesc;
    FT_GDMA_BDL_DESC *  pBdlDesc;      
    UINT32              i;
    UINT32              ctl, burstSz, val, xfer_cfg;

    /* check invalid source address */

    if (pSrcAddr == NULL)
        {
        FT_DMA_LOG(0, "[%s]: source address is NULL\n",__func__,
                                             1, 2, 3, 4, 5);
        return ERROR;
        }

    pDev = dmaRes->dmaInst;
    pDrvCtrl = pDev->pDrvCtrl;
    pChan = dmaRes->pDmaChan;

    if (CHAN_SYNC_TAKE == ERROR)
        {
        return ERROR;
        }

    pChanDesc =  pChan->chan_desc;
    pBdlDesc = pChan->chan_desc->bdldesc;

    burstSz = vxbFtDmaGetBurstSize(pChan->dma_sconfig->src_addr_width);
    xfer_cfg = DMA_CX_RDSIZE(burstSz);
    burstSz = vxbFtDmaGetBurstSize(pChan->dma_sconfig->dst_addr_width);
    xfer_cfg |= DMA_CX_WRSIZE(burstSz);
    xfer_cfg |= DMA_CX_RDLEN(pChan->dma_sconfig->src_maxburst);
    xfer_cfg |= DMA_CX_WRLEN(pChan->dma_sconfig->dst_maxburst);

    xfer_cfg |= DMA_CX_WRBURST(DMA_INCR);
    xfer_cfg |= DMA_CX_RDBURST(DMA_INCR);
    
    pChanDesc->axi_burst = xfer_cfg;
    
    pDmaBufDesc = (struct vxbDmaBufferDesc *)pSrcAddr;
    i = 0;
    while (pDmaBufDesc != NULL)
        {
        if (pDmaBufDesc->transferSize <= 0)
            {
            pDmaBufDesc = pDmaBufDesc->pNextDesc;
            continue;
            }
        
        pBdlDesc[i].desc_src_addr_l = (UINT32)pDmaBufDesc->srcAddr;
        pBdlDesc[i].desc_src_addr_h = 0;
        pBdlDesc[i].desc_dst_addr_l = (UINT32)pDmaBufDesc->destAddr;
        pBdlDesc[i].desc_dst_addr_h = 0; 
        pBdlDesc[i].desc_data_len = pDmaBufDesc->transferSize;
        pBdlDesc[i].desc_rise_irq = 0;
        pBdlDesc[i].desc_dst_tc = xfer_cfg & 0xFFFF;
        pBdlDesc[i].desc_src_tc = (xfer_cfg >> 16) & 0xFFFF;
        
        i++;
        pDmaBufDesc = pDmaBufDesc->pNextDesc;
        }
    pChanDesc->dma_bdl_cnt = i - 1;
    /*pBdlDesc[pChanDesc->dma_bdl_cnt].desc_rise_irq = 1; */
    pChanDesc->dma_src_addr_l = (UINT32)pBdlDesc;
    pChanDesc->dma_src_addr_h = 0;

    ctl = DMA_READ_32(pDev, DMA_CTL);
    val = 0;
    if (ctl & DMA_CTL_RD_ARB) {
        val |= DMA_CX_RD_QOS(pChan->dma_sconfig->priority);
        val |= DMA_CX_RD_QOS_EN;
    }
    if (ctl & DMA_CTL_WR_ARB) {
        val |= DMA_CX_WR_QOS(pChan->dma_sconfig->priority);
        val |= DMA_CX_WR_QOS_EN;
    }   
    pChanDesc->dma_mode = val;
    pChanDesc->dma_mode |= DMA_CX_BDL;
    
    pChan->callback  = completeFunc;
    pChan->pArg      = pArg;
    
    FT_DMA_LOG(1000, "[vxbFtDmaChainMode]:desc first:  src:%x  dst:%x len:%d irq:%x bdlcnd:%d mode:%d!\n",
            pBdlDesc[0].desc_src_addr_l, pBdlDesc[0].desc_dst_addr_l , pBdlDesc[0].desc_data_len, pBdlDesc[0].desc_rise_irq, pChanDesc->dma_bdl_cnt, pChanDesc->dma_mode & DMA_CX_BDL);
    FT_DMA_LOG(1000, "[vxbFtDmaChainMode]:desc last:  src:%x  dst:%x len:%d irq:%x bdlcnd:%d mode:%d!\n",
                pBdlDesc[pChanDesc->dma_bdl_cnt].desc_src_addr_l, pBdlDesc[pChanDesc->dma_bdl_cnt].desc_dst_addr_l , pBdlDesc[pChanDesc->dma_bdl_cnt].desc_data_len, pBdlDesc[pChanDesc->dma_bdl_cnt].desc_rise_irq, pChanDesc->dma_bdl_cnt, pChanDesc->dma_mode & DMA_CX_BDL);

    /* submit requirement */
    vxbFtDmaSubmitJob (dmaRes, pChanDesc);
    CHAN_SYNC_GIVE;
    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaReserveChannel - reserve an DMA channel
*
* vxbFtDmaReserveChannel() returns pointer to an FT_GDMA_CHAN structure which
* is supplied by client software to the Ft DMA driver as a handle for
* further channel activities.  vxbFtDmaReserveChannel() is usually followed by
* vxbFtDmaMemCopy().
*
* RETURNS: FT_GDMA_CHAN *
*
* SEE ALSO: vxbFtDmaMemCopy
*
*/

LOCAL FT_GDMA_CHAN * vxbFtDmaReserveChannel
    (
    VXB_DEVICE_ID   pDev,
    GDMA_SLAVE_CONFIG * pInfo
    )
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;
    FT_GDMA_CHAN *  pChan = NULL;
    UINT32  chan;

    for (chan = 0; chan < pDrvCtrl->chanNum; chan++)
        {
        if ((pDrvCtrl->pChan[chan] == NULL) &&
            /*The channel is not in secure state*/
            (DMA_CHAN_REG_READ(pDev, chan, DMA_CX_SECCTL) != 0))
            {
            pChan = malloc (sizeof (FT_GDMA_CHAN));
            if (pChan == NULL)
                break;  
            bzero ((char *)pChan, sizeof(FT_GDMA_CHAN));
            
            pChan->id = chan;
            pChan->mutex = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE |
                                       SEM_INVERSION_SAFE);
            if (pChan->mutex == NULL)
                {
                free(pChan);
                break;
                }
            pChan->chan_desc = (FT_GDMA_DESC*)malloc(sizeof(FT_GDMA_DESC));
            if (pChan->chan_desc == NULL)
                {
                free(pChan);
                break;
                }
            bzero ((char *)pChan->chan_desc, sizeof(FT_GDMA_DESC));
                
            pChan->chan_desc->bdldesc =(FT_GDMA_BDL_DESC *)cacheDmaMalloc(DMAC_BDL_NUM * sizeof(FT_GDMA_BDL_DESC));
            if (pChan->chan_desc->bdldesc == NULL)
                {
                free(pChan->chan_desc);
                free(pChan);
                break;
                }
            if((UINT32)(UINT32 *)(pChan->chan_desc->bdldesc) % DMAC_BDL_ALIGN)
                {
                FT_DMA_LOG(1000, "[%s]: BDL descriptor addr %x is not align\r\n",__func__,
                        (UINT32)(UINT32 *)pChan->chan_desc->bdldesc, 3, 4, 5, 6);
                free(pChan->chan_desc);
                free(pChan);
                break;
                }
            bzero ((char *)pChan->chan_desc->bdldesc, DMAC_BDL_NUM * sizeof(FT_GDMA_BDL_DESC));
                        
            if (pInfo != NULL)
                {
                pChan->dma_sconfig = pInfo;
                }
   
            vxbFtDmaChanClkOn(pDev, pChan->id);
            
            pDrvCtrl->pChan[chan] = pChan;

            FT_DMA_LOG(1000, "[%s]: Reserved Channel[%d] for DMA transfer\r\n",__func__,
                               chan, 3, 4, 5, 6);
            break;
            }
        }

    return (pChan);
    }

/*******************************************************************************
*
* vxbFtDmaReleaseChannel - release a reserved channel back to the driver
*
* vxbFtDmaReleaseChannel() releases a channel back to the FT DMA driver.
*
* RETURNS: OK or ERROR if unable to release channel
*
* SEE ALSO: vxbFtDmaReserveChannel
*
*/

LOCAL STATUS vxbFtDmaReleaseChannel
    (
    VXB_DEVICE_ID   pDev,
    FT_GDMA_CHAN *  pChan
    )
    {
     FT_GDMA_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;
    
    if (NULL == pChan)
        return ERROR;

    /* Wait until pChan->mutex free */

    if (CHAN_SYNC_TAKE == ERROR)
        return ERROR;

    /* ensure that channel is not in use */

    if (vxbFtChanIsRunning(pDev,  pChan->id)) {  
        FT_DMA_LOG(0, "[%s]: channel %d is non-idle!\n",__func__,
                pChan->id,3,4,5,6);
        CHAN_SYNC_GIVE;
        return ERROR;
    }
    
    vxbFtChanDisable(pDev,  pChan->id);
    vxbFtChanIrqDisable(pDev, pChan->id);
    vxbFtDmaChanClkOff(pDev, pChan->id);    
    vxbFtDmaHwInit(pDev);

    pDrvCtrl->pChan[pChan->id] = NULL;   
    semDelete (pChan->mutex);

    cacheDmaFree(pChan->chan_desc->bdldesc);
    free(pChan->chan_desc); 
    free(pChan);

    CHAN_SYNC_GIVE;
    FT_DMA_LOG(1000, "[%s]: Release Channel[%d]\r\n",__func__,
            pChan->id, 3, 4, 5, 6);
    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaSubmitJob - initiate DMA operations with a new descriptor chain
*
* vxbFtDmaSubmitJob() initiates DMA operations with a new descriptor chain.
*
* RETURNS: OK if dma transfer starts successfully, otherwise ERROR.
*
* SEE ALSO: vxbFtDmaMemCopy, vxbFtDmaReserveChannel
*
*/

LOCAL STATUS vxbFtDmaSubmitJob
    (
    VXB_DMA_RESOURCE_ID dmaRes,
    FT_GDMA_DESC *      desc
    )
    {
    VXB_DEVICE_ID   pDev;
    FT_GDMA_CHAN *  pChan = NULL;

    /* check for invalid arguments */

    if ((NULL == dmaRes) || (NULL == desc))
        {
        FT_DMA_LOG(0, "[%s]: channel %d have invalid arguments!\n",__func__,
                pChan->id,2,3,4,5);
        return ERROR;
        }
    else
        {
        pDev = dmaRes->dmaInst;
        pChan = dmaRes->pDmaChan;
        }

    if (vxbFtChanIsRunning(pDev,  pChan->id)) {  
        FT_DMA_LOG(0, "[%s]: channel %d is non-idle!\n",__func__,
                pChan->id,2,3,4,5);

        return ERROR;
    }

    vxbFtChanReset(pDev, pChan->id);
   
    if (desc->dma_mode & DMA_CX_BDL) {
        DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_LVI, desc->dma_bdl_cnt);
        FT_DMA_LOG(0, "[%s]: channel %d is in BDL mode!\n",__func__,
                pChan->id,2,3,4,5);
    } else {
        DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_TS, desc->data_len);
        DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_UPDADDR, desc->dma_dst_addr_h);
        DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_LWDADDR, desc->dma_dst_addr_l);
        FT_DMA_LOG(0, "[%s]: channel %d is in mem copy mode!\n",__func__,
                pChan->id,2,3,4,5);
    }
        
    DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_MODE, desc->dma_mode);   
    DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_INTR_CTL, DMA_CX_TRANS_END);
    DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_UPSADDR, desc->dma_src_addr_h);
    DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_LWSADDR, desc->dma_src_addr_l);  
    DMA_CHAN_REG_WRITE(pDev, pChan->id, DMA_CX_XFER_CFG, desc->axi_burst);
  
    vxbFtChanEnable(pDev, pChan->id);
    vxbFtChanIrqSet(pDev, pChan->id);
    
    FT_DMA_LOG(0, "[%s]: channel %d is submit! mode: %d \n",__func__,
            pChan->id,desc->dma_mode & DMA_CX_BDL,4,5,6);

    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaSuspend - suspend a DMA channel
*
* Suspend a DMA channel by setting the suspend bit in the Channel
* Control Register.
*
* RETURNS: OK if successful, ERROR if the suspend request failed
*
* SEE ALSO: vxbFtDmaResume, vxbFtDmaCancel
*
*/
LOCAL STATUS vxbFtDmaSuspend
    (
    VXB_DMA_RESOURCE_ID dmaRes
    )
    {
    VXB_DEVICE_ID  pDev = dmaRes->dmaInst;
    FT_GDMA_CHAN * pChan = dmaRes->pDmaChan;
    UINT32  timeOut = 0;
    volatile UINT32  regValue;

    if (CHAN_SYNC_TAKE == ERROR)
        {
        return ERROR;
        }

    vxbFtChanDisable(pDev,  pChan->id);

    do
        {
        vxbUsDelay(1);
        regValue = DMA_CHAN_REG_READ(pDev,  pChan->id, DMA_CX_STAT);
        timeOut++;
        }
    while ((regValue & DMA_CX_BUSY) && (timeOut <= 10));

    if (timeOut > 10)
        {
        FT_DMA_LOG(0, "DMA channel suspend failure! regValue=0x%x\r\n",
                                            regValue, 1, 2, 3, 4, 5);
        CHAN_SYNC_GIVE;
        return ERROR;
        }

    CHAN_SYNC_GIVE;
    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaResume - resume a DMA channel
*
* Resume a DMA channel previously suspended by vxbFtDmaSuspend.
*
* RETURNS: OK if successful, ERROR if the resume request failed
*
* SEE ALSO: vxbFtDmaSuspend, vxbFtDmaCancel
*
*/
LOCAL STATUS vxbFtDmaResume
    (
    VXB_DMA_RESOURCE_ID dmaRes
    )
    {
    VXB_DEVICE_ID  pDev = dmaRes->dmaInst;
    FT_GDMA_CHAN * pChan = dmaRes->pDmaChan;

    if (CHAN_SYNC_TAKE == ERROR)
        {
        return ERROR;
        }
    vxbFtChanEnable(pDev,  pChan->id);
    CHAN_SYNC_GIVE;
    return OK;

    }
/*******************************************************************************
*
* vxbFtDmaExcJob - maintenance function called after DMA events
*
* vxbFtDmaExcJob() performs descriptor maintenance and calls client software
* callback functions whenever an interrupt occurs.
*
* RETURNS: N/A
*
*/

LOCAL void vxbFtDmaExcJob
    (
    VXB_DEVICE_ID  pDev,
    FT_GDMA_CHAN * pChan,
    STATUS         jobStatus
    )
    {

    if (pChan != NULL)
        {
        if (pChan->callback != NULL)
            {
            if (jobStatus == OK)
                pChan->callback((void *)pChan->pArg, 0, DMA_STATUS_OK);
            else
                pChan->callback((void *)pChan->pArg, 0, DMA_STATUS_ERROR);
            }
        }
    FT_DMA_LOG(1000,"[%s} dma channel %d is excuted! jobStatus=%d\n",__func__,pChan->id,jobStatus,4,5,6);
    }

/*******************************************************************************
*
* vxbFtDmaExcEntry - entry function of DMA main execute task
*
* vxbFtDmaExcEntry() always waits for the message sent by the interrupt
* service routine, and subsequently invokes vxbFtDmaExcJob() to maintain
* the descriptor.
*
* RETURNS: N/A
*
*/

LOCAL void vxbFtDmaExcEntry
    (
    VXB_DEVICE_ID   pDev
    )
    {
    FT_GDMA_CHAN *      pChan;
    FT_GDMA_DRV_CTRL *  pDrvCtrl;
    atomicVal_t         events;
    UINT32              i;

    pDrvCtrl = pDev->pDrvCtrl;

    while (TRUE)
        {
        /* returns OK as ftDmaSemId is valid and wait forever */

        (void)semTake (pDrvCtrl->ftDmaSemId, WAIT_FOREVER);

        if ((events = vxAtomicSet (&(pDrvCtrl->ftDmaEvents), 0)) != 0)
            {
            for (i = 0; i < pDrvCtrl->chanNum; i++)
                {
                if (events & (1 << i))
                    {
                    pChan = pDrvCtrl->pChan[i];
                    vxbFtDmaExcJob (pDev, pChan, OK);
                    }
                }
            for (i = DMA_EVENTS_ERROR_BIT_OFFSET; 
                 i < DMA_EVENTS_ERROR_BIT_OFFSET + pDrvCtrl->chanNum; i++)
                {
                if (events & (1 << i))
                    {
                    pChan = pDrvCtrl->pChan[i-DMA_EVENTS_ERROR_BIT_OFFSET];
                    vxbFtDmaExcJob (pDev, pChan, ERROR);
                    }
                }
            }
        }
    }

/*******************************************************************************
*
* vxbFtDmaChanIsr - DMA driver channel interrupt service routine
*
* vxbFtDmaChanIsr() is called from interrupt context and determines if
* a DMA channel needs attention. If it does, it sends a message to inform
* the main service task. vxbFtDmaChanIsr() is called if a channel's interrupt
* enable bits are set in the DCR field for the current DMA descriptor.
*
* RETURNS: N/A
*
* SEE ALSO: vxbFtDmaMemCopy, vxbFtDmaSubmitJob
*
*/

LOCAL void vxbFtDmaChanIsr
    (
    FT_GDMA_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID pDev = pDrvCtrl->vxbDev;
    FT_GDMA_CHAN* chan = NULL;
    UINT32 irqStatus = 0;
    UINT32 chanStatus = 0;
    UINT32 irqMask = 0xFFFF;
    UINT32 i;

    irqStatus = vxbFtDmaIrqRead(pDev);

    FT_DMA_LOG(1000,"[%s]: irq status:%x, irq mask:%x\n",__func__,irqStatus,irqMask,4,5,6);
        if (!(irqStatus & irqMask))
            return ;

        FT_DMA_LOG(1000,"[%s]: irq status:%x, irq mask:%x\n",__func__,irqStatus,irqMask,4,5,6);
        /* Disable DMAC inerrupts. We'll enable them after processing chanels */
        vxbFtDmaIrqDisable(pDev);

        /* Poll, clear and process every chanel interrupt status */
        for (i = 0; i < pDrvCtrl->chanNum; i++) {
            if(!(irqStatus & BIT(i)))
                continue;

            chan = pDrvCtrl->pChan[i];

            chanStatus = vxbFtChanStatusRead(pDev, chan->id);
            vxbFtChanIrqClear(pDev, chan->id);

            if (DMA_CX_TRANS_END & chanStatus) {
                (void)vxAtomicOr (&(pDrvCtrl->ftDmaEvents), (1 << i));
                FT_DMA_LOG(1000,"[%s]: dma channel %d is  completed\n",__func__,i,3,4,5,6);
            } else if (DMA_CX_BUSY & chanStatus) {
                FT_DMA_LOG(0,"[%s]: dma channel %d is not completed. In busy\n",__func__,i,3,4,5,6);
            } else if (DMA_CX_BDL_END & chanStatus) {
                FT_DMA_LOG(0,"[%s]: dma channel %d is not completed. bdl end\n",__func__,i,3,4,5,6);
            } else {
                (void)vxAtomicOr (&(pDrvCtrl->ftDmaEvents), 
                            (1 << (i + DMA_EVENTS_ERROR_BIT_OFFSET)));
                FT_DMA_LOG(0,"[%s]: dma channel %d is not completed. chanStatus: %x\n", __func__,i,chanStatus,4,5,6);
            }

        }

        /* Re-enable interrupts */
        vxbFtDmaIrqEnable(pDev);
    
        if (pDrvCtrl->ftDmaEvents != 0)
            (void)semGive (pDrvCtrl->ftDmaSemId);
    }

/*******************************************************************************
*
* vxbFtDmaRead - initiate a DMA read transaction on a channel
*
* This routine initiates a DMA read operation on a given DMA channel.
* The DMA will be started, and when it completes, the supplied completion
* routine will be invoked. The parameter src is the physical address of source,
* the parameter dest is the physical address of destination.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaRead
    (
    VXB_DMA_RESOURCE_ID     dmaRes,
    char *                  src,
    char *                  dest,
    int                     transferSize,
    int                     unitSize,
    UINT32                  flags,
    pVXB_DMA_COMPLETE_FN    completeFunc,
    void *                  pArg
    )
    {
    if ((flags & DMA_CHAINED_BUFFERS) != DMA_CHAINED_BUFFERS)
        return (vxbFtDmaMemCopy (dmaRes, dest, src, transferSize,
                                  completeFunc, pArg));
    else
        return (vxbFtDmaChainMode(dmaRes, src, completeFunc, pArg));
    }

/*******************************************************************************
*
* vxbFtDmaSyncCallBack - callback when DMA transfer done
*
* This routine is the callback function when DMA transfer is done.
*
* RETURNS: N/A 
*
* ERRNO: N/A
*/

LOCAL void vxbFtDmaSyncCallBack
    (
    VXB_DEVICE_ID  pDev
    )
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl;

    pDrvCtrl = pDev->pDrvCtrl;

    (void)semGive (pDrvCtrl->ftDmaSyncSem);

    FT_DMA_LOG(1000, "[%s]: Sync Semaphore SemGive!\n", __func__, 2, 3, 4, 5, 6);
    }

/*******************************************************************************
*
* vxbFtDmaSyncRead - initiate a synchronous DMA read transaction on a channel
*
* This routine initiates a synchronous read operation on a given DMA channel.
* The function will block until the DMA completes. The parameter src is the
* physical address of source, the parameter dest is the physical address of
* destination.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaSyncRead
    (
    VXB_DMA_RESOURCE_ID dmaRes,
    char *              src,
    char *              dest,
    int *               transferSize,
    int                 unitSize,
    UINT32              flags
    )
    {
    VXB_DEVICE_ID  pDev;
    FT_GDMA_DRV_CTRL * pDrvCtrl;

    /* get the DMA engine vxbus instance id */

    pDev = dmaRes->dmaInst;

    pDrvCtrl = pDev->pDrvCtrl;

    if ((flags & DMA_CHAINED_BUFFERS) != DMA_CHAINED_BUFFERS)
        {
        if (vxbFtDmaMemCopy (dmaRes, dest, src,
                              *transferSize,
                             (pVXB_DMA_COMPLETE_FN)vxbFtDmaSyncCallBack,
                             (void *)pDev) != OK)
            return ERROR;
        }
    else
        {
        if (vxbFtDmaChainMode(dmaRes, src, 
                                  (pVXB_DMA_COMPLETE_FN)vxbFtDmaSyncCallBack,
                                  (void *)pDev) != OK)
            return ERROR;
        }

    if (semTake (pDrvCtrl->ftDmaSyncSem, WAIT_FOREVER) != OK)
        {
        FT_DMA_LOG(0, "[%s]: semTake ftDmaSyncSem error!\n", __func__, 2, 3, 4, 5, 6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaDmaWrite - initiate a DMA write transaction on a channel
*
* This routine initiates a DMA write operation on a given DMA channel.
* The DMA will be started, and when it completes, the supplied completion
* routine will be invoked. The parameter src is the physical address of source,
* the parameter dest is the physical address of destination.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaWrite
    (
    VXB_DMA_RESOURCE_ID     dmaRes,
    char *                  src,
    char *                  dest,
    int                     transferSize,
    int                     unitSize,
    UINT32                  flags,
    pVXB_DMA_COMPLETE_FN    completeFunc,
    void *                  pArg
    )
    {
    if ((flags & DMA_CHAINED_BUFFERS) != DMA_CHAINED_BUFFERS)
        return (vxbFtDmaMemCopy (dmaRes, dest, src, transferSize,
                                  completeFunc, pArg));
    else
        return (vxbFtDmaChainMode(dmaRes, src, completeFunc, pArg));
    }

/*******************************************************************************
*
* vxbFtDmaSyncWrite - initiate a synchronous DMA write transaction on a channel
*
* This routine initiates a synchronous write operation on a given DMA channel.
* The function will block until the DMA completes. The parameter src is the
* physical address of source the parameter dest is the physical address of
* destination.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaSyncWrite
    (
    VXB_DMA_RESOURCE_ID dmaRes,
    char *              src,
    char *              dest,
    int *               transferSize,
    int                 unitSize,
    UINT32              flags
    )
    {
    VXB_DEVICE_ID  pDev;
    FT_GDMA_DRV_CTRL * pDrvCtrl;

    /* get the DMA engine vxbus instance id */

    pDev = dmaRes->dmaInst;

    pDrvCtrl = pDev->pDrvCtrl;

    if ((flags & DMA_CHAINED_BUFFERS) != DMA_CHAINED_BUFFERS)
        {
        if (vxbFtDmaMemCopy (dmaRes, dest, src,
                              *transferSize,
                             (pVXB_DMA_COMPLETE_FN)vxbFtDmaSyncCallBack,
                             (void *)pDev) != OK)
            return ERROR;
        }
    else
        {
        if (vxbFtDmaChainMode(dmaRes, src, 
                                  (pVXB_DMA_COMPLETE_FN)vxbFtDmaSyncCallBack,
                                  (void *)pDev) != OK)
            return ERROR;
        }

    if (semTake (pDrvCtrl->ftDmaSyncSem, WAIT_FOREVER) != OK)
        {
        FT_DMA_LOG(0, "[%s]: semTake ftDmaSyncSem error!\n", __func__, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaStatus - return DMA channel status
*
* This routine returns the status of a current DMA channel. A channel
* may be paused, idle, running or have no DMA operation in progress.
*
* RETURNS: the status of a current DMA channel
*
* ERRNO
*/

LOCAL int vxbFtDmaStatus
    (
    VXB_DMA_RESOURCE_ID dmaRes
    )
    {
    VXB_DEVICE_ID  pDev;
    FT_GDMA_CHAN * pChan;

    /* get the DMA engine vxbus instance id */

    pDev = dmaRes->dmaInst;

    /* get the associated DMA channel data */

    pChan = dmaRes->pDmaChan;

    if (CHAN_SYNC_TAKE == ERROR)
        {
        return DMA_NOT_USED;
        }

    if(vxbFtChanIsRunning(pDev,  pChan->id))
        {
        CHAN_SYNC_GIVE;
        return DMA_RUNNING;
        }
    else
        {
        CHAN_SYNC_GIVE;
        return DMA_IDLE;
        }
    }

/*******************************************************************************
*
* vxbFtDmaInstUnlink - VxBus unlink method
*
* This is a cleanup routine invoked by VxBus when this device instance is
* destroyed. We release our allocated resources here.
*
* RETURNS: OK, or ERROR
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaInstUnlink
    (
    VXB_DEVICE_ID   pDev,
    void *          unused
    )
    {
    FT_GDMA_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;

    if(pDrvCtrl->ftDmaExcId != 0)
        {
        if (taskDelete(pDrvCtrl->ftDmaExcId) != OK)
            return ERROR;
        }
    pDrvCtrl->ftDmaExcId = 0;

    if (semDelete (pDrvCtrl->ftDmaSemId) != OK)
        return ERROR;

    pDrvCtrl->ftDmaSemId = NULL;

    if (semDelete (pDrvCtrl->ftDmaSyncSem) != OK)
        return ERROR;

    pDrvCtrl->ftDmaSyncSem = NULL;


    (void)vxbIntDisconnect (pDev, 0, vxbFtDmaChanIsr, pDev);
    (void)vxbIntDisable (pDev, 0, vxbFtDmaChanIsr, pDev);

    pDrvCtrl->ftDmaPhase = -1;

    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaResourceGet - method to get the dma channel resources
*
* This function is the driver method used to get the dma channel resources.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaResourceGet
    (
    VXB_DEVICE_ID           pDev,
    VXB_DEVICE_ID           pReqDev,
    struct vxbDmaRequest *  pReq
    )
    {
    VXB_DMA_RESOURCE_ID dmaRes;
    FT_GDMA_DRV_CTRL *  pDrvCtrl = pDev->pDrvCtrl;
    FT_GDMA_CHAN *      pChan;
    GDMA_SLAVE_CONFIG * pInfo = pReq->pDedicatedChanInfo;

    if(pInfo->direction != DMA_MEM_TO_MEM)
        {
        FT_DMA_LOG(0, "[%s]: direction error!\r\n",
                                        __func__, 1, 2, 3, 4, 5);
        return ERROR;
        }       
    pChan = vxbFtDmaReserveChannel (pDev, pInfo);

    if (pChan == NULL)
        {
        FT_DMA_LOG(0, "[%s]: can not get a free DMA channel!\r\n",
                                            __func__, 1, 2, 3, 4, 5);
        return ERROR;
        }

    /* get DMA resource data */

    dmaRes = pReq->pChan;

    /* update the function pointers */

    dmaRes->dmaFuncs.dmaRead         = vxbFtDmaRead;
    dmaRes->dmaFuncs.dmaReadAndWait  = vxbFtDmaSyncRead;
    dmaRes->dmaFuncs.dmaWrite        = vxbFtDmaWrite;
    dmaRes->dmaFuncs.dmaWriteAndWait = vxbFtDmaSyncWrite;
    dmaRes->dmaFuncs.dmaCancel       = NULL;
    dmaRes->dmaFuncs.dmaPause        = vxbFtDmaSuspend;
    dmaRes->dmaFuncs.dmaResume       = vxbFtDmaResume;
    dmaRes->dmaFuncs.dmaStatus       = vxbFtDmaStatus;

    /* save DMA controller vxbus instance id */

    dmaRes->dmaInst = pDev;

    /* save DMA per channel data to DMA resource */

    dmaRes->pDmaChan = pChan;

    if(pDrvCtrl->ftDmaExcId == 0)
        {
        pDrvCtrl->ftDmaExcId = taskSpawn ("tFtDmaExc", DMA_EXC_TASK_PRI, 0,
                                           4096, (FUNCPTR) vxbFtDmaExcEntry,
                                           (_Vx_usr_arg_t)pDev,
                                           0, 0, 0, 0, 0, 0, 0, 0, 0);
        if(pDrvCtrl->ftDmaExcId == (TASK_ID)ERROR)
            {
            FT_DMA_LOG(0, "Unable to create DMA execute task\n",
                              0, 1, 2, 3, 4, 5);
            return ERROR;
            }
        }
    FT_DMA_LOG(1000, "[%s]: get a free DMA channel %d!\r\n",__func__,
            pChan->id, 2, 3, 4, 5);
    return OK;
    }

/*******************************************************************************
*
* vxbFtDmaResourceRelease - method to release DMA resources
*
* This function is the driver method for releasing DMA resources when
* a channel is deallocated.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS vxbFtDmaResourceRelease
    (
    VXB_DEVICE_ID           pDev,
    VXB_DMA_RESOURCE_ID     dmaRes
    )
    {
    FT_GDMA_CHAN *  pChan;
    FT_GDMA_DRV_CTRL *  pDrvCtrl = pDev->pDrvCtrl;
    UINT32  chan;
    UINT32  deleteTask = 1;

    pChan = dmaRes->pDmaChan;

    /* free a DMA channel to DMA engine */

    if (vxbFtDmaReleaseChannel (pDev, pChan) != OK)
        return ERROR;

    for(chan = 0; chan < pDrvCtrl->chanNum; chan++)
        {
        if(pDrvCtrl->pChan[chan] != NULL)
            {
            deleteTask = 0;
            break;
            }
        }

    if (deleteTask)
        {
        if(taskDelete (pDrvCtrl->ftDmaExcId) != OK)
            return ERROR;
        pDrvCtrl->ftDmaExcId = 0;
        }

    return OK;
    }



#define FT_GDMA_TEST
#ifdef FT_GDMA_TEST

typedef struct
    {
    char *  src;
    char *  dst;
    int     size;
	STATUS  status;
    } FT_GDMA_TEST_DATA;

LOCAL SEM_ID ftGdmaTestSync = NULL;


/*******************************************************************************
*
* testGDmaMemCpyCallback - callback when DMA transfer done
*
* This function is callback when DMA transfer done.
*
* RETURNS: OK or ERROR if there is an error.
*
* ERRNO
*/

LOCAL STATUS testGDmaMemCpyCallback
    (
    void *  pArg,
    UINT32  actualTransferSize,
    UINT32  transferStatus
    )
    {

    FT_GDMA_TEST_DATA  *data = (FT_GDMA_TEST_DATA  *)pArg;
    if (memcmp(data->src, data->dst, data->size) == 0)
        {
        printf("[%s,%d]: GDMA DIRECT TEST PASS. \r\n", __func__,__LINE__);
        data->status = OK;
        }
    else
        {
        printf("[%s,%d]: GDMA DIRECT TEST FAIL! \r\n", __func__,__LINE__);
        data->status = ERROR;
        }

    if (ftGdmaTestSync != NULL)
        semGive(ftGdmaTestSync);

    return data->status;
    }

/*******************************************************************************
*
* testGDmaMemCpy - test routine for memory copy using DMA int mode
*
* This routine is a memory copy test routine that makes use of DMA int mode.
*
* RETURNS: N/A
*
* ERRNO
*/

STATUS testGDmaMemCpy
    (
    int     size
    )
    {
    VXB_DMA_RESOURCE_ID dmaRes;
    FT_GDMA_TEST_DATA * data;
    GDMA_SLAVE_CONFIG  *slave_config;
    char *tmp;
    int i;

    if (size == 0)
        {
        printf("[testGDmaMemCpy]: size should not be 0!\r\n");
        return ERROR;
        }

    if ((size % 4) != 0)
        {
        printf("[testGDmaMemCpy]: size should be multiple size of 4!\r\n");
        return ERROR;
        }

    slave_config = (GDMA_SLAVE_CONFIG *) malloc (sizeof (GDMA_SLAVE_CONFIG));
    if (slave_config == NULL)
        {
        printf("[testGDmaMemCpy] malloc slave config failed \r\n");
        return ERROR;
        }
    bzero ((char *)slave_config, sizeof (GDMA_SLAVE_CONFIG));
    
    slave_config->direction = DMA_MEM_TO_MEM;
    slave_config->src_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES;
    slave_config->dst_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES;
    slave_config->src_maxburst = 8;   
    slave_config->dst_maxburst = 8;  
    slave_config->priority = 0;
    
    dmaRes = vxbDmaChanAlloc(vxbInstByNameFind("ftGDma", 0),1, 
            DMA_TRANSFER_TYPE_RD | DMA_TRANSFER_TYPE_WR, (void *)slave_config);

    if (dmaRes == NULL)
        {
        free(slave_config);
        printf("[testGDmaMemCpy]: dmaChanAlloc error\r\n");
        return ERROR;
        }

    data = (FT_GDMA_TEST_DATA *)malloc(sizeof(FT_GDMA_TEST_DATA));
    if (data == NULL)
        {
        free(slave_config);
        return ERROR;
        }

    data->size = size;

    data->src = (char *)cacheDmaMalloc(size);
    if (data->src == NULL)
        {
        free(slave_config);
        free(data);
        return ERROR;
        }

    tmp = data->src;
    for (i = 0; i < size; i++)
        {
        *tmp++ = i;
        }

    data->dst = (char *)cacheDmaMalloc(size);
    if (data->dst == NULL)
        {
        free(slave_config);
        free(data->src);
        free(data);
        return ERROR;
        }
    bzero(data->dst, size);

	data->status = ERROR;

    if (ftGdmaTestSync == NULL)
        {
        ftGdmaTestSync = semBCreate(SEM_Q_FIFO | SEM_Q_PRIORITY, SEM_EMPTY);
        }

    vxbDmaRead(dmaRes, data->src, data->dst, data->size, 4, 0,
            (pVXB_DMA_COMPLETE_FN)testGDmaMemCpyCallback, (void *)data);

    semTake(ftGdmaTestSync, WAIT_FOREVER);

    cacheDmaFree(data->src);
    cacheDmaFree(data->dst);
    free(data);
    free(slave_config);

    vxbDmaChanFree(dmaRes);

    return OK;
    }

/* test log: 

-> testGDmaMemCpy 0x10000
[testGDmaMemCpyCallback,1623]: GDMA DIRECT TEST PASS.
value = 0 = 0x0
*/



LOCAL FT_GDMA_TEST_DATA data[DMAC_BDL_NUM];

LOCAL STATUS testGDmaChainCallback
    (
    void *  pArg,
    UINT32  actualTransferSize,
    UINT32  transferStatus
    )
    {
    STATUS status;
    UINT32 chainNum =(UINT32)pArg;
    UINT32 i;
    printf("[%s,%d]: CHAIN NUMBER:%d \r\n", __func__, __LINE__,chainNum);
    for(i = 0;i < chainNum; i++)
    {
        if (memcmp(data[i].src, data[i].dst, data[i].size) == 0)
            {
            printf("[%s,%d]: CHAIN:%d TEST GDMA BDL PASS.\r\n", __func__, __LINE__, chainNum);
            status = OK;
            }
        else
            {
            printf("[%s,%d]: CHAIN:%d TEST GDMA BDL FAIL! %d %d\r\n", __func__, __LINE__, chainNum);
            status = ERROR;
            break;
            }
    }
    if (ftGdmaTestSync != NULL)
        semGive(ftGdmaTestSync);

    return status;
    }    
    
STATUS testGDmaChain
    (
    int chainNum, int eachSize
    )
    {
    VXB_DMA_RESOURCE_ID dmaRes;

    struct vxbDmaBufferDesc *   pDmaBufDesc;
    GDMA_SLAVE_CONFIG*  slave_config;
    char *tmp;
    int i,j;
   
    if (chainNum > DMAC_BDL_NUM || chainNum <= 0)
        {
        printf("[%s,%d]: chain Num error!\r\n",__func__, __LINE__);
        return ERROR;
        }

    if ((eachSize % 4) != 0)
        {
        printf("[%s,%d]: size should be multiple size of 4!\r\n",__func__, __LINE__);
        return ERROR;
        }
    
    slave_config = (GDMA_SLAVE_CONFIG *) malloc (sizeof (GDMA_SLAVE_CONFIG));
    if (slave_config == NULL)
        {
        printf("[%s,%d]: malloc slave config failed \r\n",__func__, __LINE__);
        return ERROR;
        }
    bzero ((char *)slave_config, sizeof (GDMA_SLAVE_CONFIG));
    
    slave_config->direction = DMA_MEM_TO_MEM;
    slave_config->src_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES;
    slave_config->dst_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES;
    slave_config->src_maxburst = 8;   
    slave_config->dst_maxburst = 8;   
    slave_config->priority = 0;
    
    dmaRes = vxbDmaChanAlloc(vxbInstByNameFind("ftGDma", 0),1, 
            DMA_TRANSFER_TYPE_RD | DMA_TRANSFER_TYPE_WR, (void *)slave_config);

    if (dmaRes == NULL)
        {
        free(slave_config);
        printf("[%s,%d]: dmaChanAlloc error\r\n",__func__, __LINE__);
        return ERROR;
        }

 
    pDmaBufDesc = (struct vxbDmaBufferDesc *)malloc(chainNum * sizeof(struct vxbDmaBufferDesc));
    if(pDmaBufDesc == NULL)
        {
        free(slave_config);
        return ERROR;   
        }
    memset(pDmaBufDesc, 0, chainNum * sizeof(struct vxbDmaBufferDesc));
    memset(data, 0, chainNum * sizeof(FT_GDMA_TEST_DATA));
    
    for (i = 0; i < chainNum; i++) {
        if(i == chainNum - 1)
            pDmaBufDesc[i].pNextDesc = NULL;
        else
            pDmaBufDesc[i].pNextDesc = &pDmaBufDesc[i + 1];
        
        data[i].size = eachSize + 4 * i;
        data[i].src = (char *)cacheDmaMalloc(data[i].size); 
        if (data[i].src == NULL)
             {
             free(slave_config);
             free(pDmaBufDesc);
             return ERROR;
             }
        tmp = data[i].src;
        for (j = 0; j < data[i].size; j++)
            *tmp++ = j + i;
        
        data[i].dst = (char *)cacheDmaMalloc(data[i].size);
        if (data[i].dst == NULL)
            {
            free(data[i].src);
            free(slave_config);
            free(pDmaBufDesc);
            return ERROR;
            }
        bzero(data[i].dst, data[i].size);
        
        pDmaBufDesc[i].srcAddr = data[i].src;
        pDmaBufDesc[i].destAddr = data[i].dst;
        pDmaBufDesc[i].transferSize = data[i].size;
    }
    
    if (ftGdmaTestSync == NULL)
        ftGdmaTestSync = semBCreate(SEM_Q_FIFO | SEM_Q_PRIORITY, SEM_EMPTY);

    vxbDmaRead(dmaRes, (char*)pDmaBufDesc, NULL, 0, 4, DMA_CHAINED_BUFFERS,
            (pVXB_DMA_COMPLETE_FN)testGDmaChainCallback, (void *)chainNum);

    semTake(ftGdmaTestSync, WAIT_FOREVER);

    for (i = 0; i < chainNum; i++) 
        {    
        cacheDmaFree(data[i].src);
        cacheDmaFree(data[i].dst);
        }
    free(slave_config);
    free(pDmaBufDesc);
    vxbDmaChanFree(dmaRes);

    return OK;
    }
/*
-> testGDmaChain 2, 0x10000
[testGDmaChainCallback,1773]: CHAIN NUMBER:2
[testGDmaChainCallback,1778]: CHAIN:2 TEST GDMA BDL PASS.
[testGDmaChainCallback,1778]: CHAIN:2 TEST GDMA BDL PASS.
value = 0 = 0x0
*/
#endif /* FT_DMA_DEBUG */
