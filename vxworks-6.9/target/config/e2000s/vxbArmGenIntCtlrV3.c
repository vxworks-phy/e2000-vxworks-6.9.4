/* vxbArmGenIntCtlrV3.c - ARM generic interrupt controller driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#include <vxWorks.h>
#include <vsbConfig.h>
#include <intLib.h>
#include <arch/arm/intArmLib.h>
#include <arch/arm/excArmLib.h>
#include <arch/arm/vxAtomicArchLib.h>
#include <iv.h>

#include <sysLib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memLib.h>

#ifdef  _WRS_CONFIG_SV_INSTRUMENTATION
#   include <private/eventP.h>
#endif  /* _WRS_CONFIG_SV_INSTRUMENTATION */

#include <hwif/util/hwMemLib.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/vxbus/vxbIntrCtlr.h>
#include <../src/hwif/intCtlr/vxbIntCtlrLib.h>
#include <hwif/vxbus/hwConf.h>
#include "../h/vxbus/vxbAccess.h"
#include "vxbArmGenIntCtlrV3.h"

#ifdef _WRS_CONFIG_SMP
#   include <vxIpiLib.h>
#   include <private/cpcLibP.h>
#   include <cpuset.h>
#endif

IMPORT int uartf(const char *  fmt,	...) ;

/* debug macro */

#undef  ARM_GIC_DBG
#ifdef  ARM_GIC_DBG

/* turning local symbols into global symbols */

#ifdef  LOCAL
#undef  LOCAL
#define LOCAL
#endif


#define GIC_DBG_OFF          0x00000000
#define GIC_DBG_ISR          0x00000001
#define GIC_DBG_ERR          0x00000002
#define GIC_DBG_INFO         0x00000004
#define GIC_DBG_ALL          0xffffffff

LOCAL UINT32 gicDbgMask = GIC_DBG_ALL;

#define GIC_DBG(mask, ...)                                      \
    do                                                          \
        {                                                       \
        if ((gicDbgMask & (mask)) || ((mask) == GIC_DBG_ALL))   \
            {                                                   \
                uartf(__VA_ARGS__);                 \
            }                                                   \
        }                                                       \
    while ((FALSE))
#else
#define GIC_DBG(...)
#endif  /* ARM_GIC_DBG */

/* define  */

#ifdef ARMBE8
#    define SWAP32 vxbSwap32
#    define SWAP64(var64)   ((SWAP32((var64) & 0x00000000FFFFFFFF) << 32) | \
                         (SWAP32(((var64) & 0xFFFFFFFF00000000) >> 32)))
#else
#    define SWAP32
#    define SWAP64
#endif /* ARMBE8 */

/* vxBus Driver Name */

#define GIC_NAME    "armGicDev"

/* Connect the interrupt handler to it's exception vector */

#define INTR_EXC_ID (EXC_OFF_IRQ) /* exception id, for external interrupt */

#define GIC_IPI_PRIORITY 0
#define GIC_PRIORITY_LEVEL_STEP 0x10
#define GIC_SPI_PRIORITY_DEFAULT 0x10101010

#ifndef EXC_CONNECT_INTR_RTN
#   define EXC_CONNECT_INTR_RTN(rtn) \
    excIntConnect ((VOIDFUNCPTR *)INTR_EXC_ID,rtn)
#endif

/* This sets the CPU interrupt enable on */

#ifndef CPU_INTERRUPT_ENABLE
#   define CPU_INTERRUPT_ENABLE intCpuUnlock(0)
#endif

/* This resets the CPU interrupt enable */

#ifndef CPU_INTERRUPT_DISABLE
#   define CPU_INTERRUPT_DISABLE intCpuLock()
#endif

#define GIC_ISR(pEnt,inputPin,func)                          \
        {                                                    \
        struct vxbIntCtlrPin * pPin =                        \
           vxbIntCtlrPinEntryGet(pEnt,inputPin);             \
        func = pPin->isr;                                    \
        }

#define GIC_DESTCPU(pEnt,inputPin,destCpu)                   \
        {                                                    \
        struct vxbIntCtlrPin * pPin =                        \
           vxbIntCtlrPinEntryGet(pEnt,inputPin);             \
        destCpu = pPin->pinCpu;                              \
        }

/* structure holding Generic Interupt Controller details */

typedef struct armGicDrvCtrl
    {
    VXB_DEVICE_ID        pInst;         /* instance pointer */
    struct intCtlrHwConf isrHandle;

    UINT32               bootCpuId;  /* logical  id. by vxCpuIndexGet() */
#if defined (_WRS_CONFIG_LP64)
    UINT64               bootCpuBit; /* hardware id. by MPIDR_EL1 register  */
#else
    UINT32               bootCpuBit; /* hardware id. by MPIDR register  */
#endif
    
    UINT32               gicDist;       /* gic base address */
    void *               gicDistHdl;

    GIC_REDIST_REGION *  gicRedistRegion;
    UINT32               gicRedistRegionNum;
    GIC_REDIST           gicRedist[VX_MAX_SMP_CPUS];
    UINT32               gicRedistStride;
    
    int                  intMode;       /* type of interrupt handling*/
    UINT32               gicLvlCurrent;
    INT32                gicLinesNum;     /* the maximum interrupt level number */
    INT32                gicCpuNum;     /* the maximum CPU number in system */
    spinlockIsr_t        spinLock;
    } ARM_GIC_DRV_CTRL;

/* forward declarations */

IMPORT STATUS (*_func_intConnectRtn) (VOIDFUNCPTR *, VOIDFUNCPTR, int);
IMPORT STATUS (*_func_intDisconnectRtn) (VOIDFUNCPTR *, VOIDFUNCPTR, int);
IMPORT UINT32 vxCpuIdGetByIndex(UINT32 idx);
LOCAL STATUS vxbArmGicDevInit (VXB_DEVICE_ID pInst);
#ifdef _WRS_CONFIG_SMP
STATUS vxbArmGicLvlVecChk (VXB_DEVICE_ID pInst, int*, int*, int*);
STATUS vxbArmGicLvlVecAck (VXB_DEVICE_ID pInst, int, int, int);
#else
STATUS vxbArmGicLvlVecChk (VXB_DEVICE_ID pInst, int*, int*);
STATUS vxbArmGicLvlVecAck (VXB_DEVICE_ID pInst, int, int);
#endif
int vxbArmGicLvlChg (VXB_DEVICE_ID  pInst, UINT32);
LOCAL STATUS vxbArmGicIntDevDistInit (void);
LOCAL STATUS vxbArmGicIntDevRedistCpuInit (int cpuNum);
#ifdef INCLUDE_SHOW_ROUTINES
void vxbArmGicDataShow (VXB_DEVICE_ID  pInst, int verboseLevel);
#endif /* INCLUDE_SHOW_ROUTINES */

LOCAL void vxbArmGicCtlInit(VXB_DEVICE_ID pInst);
LOCAL void vxbArmGicCtlInit2(VXB_DEVICE_ID pInst);
LOCAL STATUS sysArmGicConnect
    (
    VOIDFUNCPTR *  vector,     /* interrupt vector to attach to     */
    VOIDFUNCPTR    routine,    /* routine to be called              */
    int            parameter   /* parameter to be passed to routine */
    );
LOCAL STATUS sysArmGicDisconnect
    (
    VOIDFUNCPTR *  vector,     /* interrupt vector to detach from   */
    VOIDFUNCPTR    routine,    /* routine to be disconnected        */
    int            parameter   /* parameter to be matched           */
    );
LOCAL STATUS sysArmGicISREnable
    (
    struct intCtlrHwConf *  pEntries,
    int             inputPin
    );
LOCAL STATUS sysArmGicEnable
    (
    int            vector
    );
LOCAL STATUS sysArmGicISRDisable
    (
    struct intCtlrHwConf *  pEntries,
    int                     inputPin
    );
LOCAL STATUS sysArmGicDisable
    (
    int vector
    );
LOCAL STATUS vxbArmGicConnect
    (
    VXB_DEVICE_ID           pIntCtlr,             /* int Ctlr device struct  */
    VXB_DEVICE_ID           pDev,                 /* device being connnected */
    int                     index,                /* device index            */
    void                    (*pIsr)(void * pArg), /* isr being connected     */
    void *                  pArg,                 /* isr's arg               */
    int *                   pInputPin             /* input pin device is on  */
    );
LOCAL STATUS vxbArmGicDisconnect
    (
    VXB_DEVICE_ID           pIntCtlr,             /* int Ctlr device struct  */
    VXB_DEVICE_ID           pDev,                 /* device being connnected */
    int                     index,                /* device index            */
    VOIDFUNCPTR             pIsr,                 /* isr being connected     */
    void *                  pArg                  /* isr's arg               */
    );
LOCAL STATUS vxbArmGicEnable
    (
    VXB_DEVICE_ID           pIntCtlr,             /* int Ctlr device struct  */
    VXB_DEVICE_ID           pDev,                 /* device being connnected */
    int                     index,                /* device index            */
    VOIDFUNCPTR             pIsr,                 /* isr being connected     */
    void *                  pArg                  /* isr's arg               */
    );
LOCAL STATUS vxbArmGicDisable
    (
    VXB_DEVICE_ID           pIntCtlr,             /* int Ctlr device struct  */
    VXB_DEVICE_ID           pDev,                 /* device being connnected */
    int                     index,                /* device index            */
    VOIDFUNCPTR             pIsr,                 /* isr being connected     */
    void *                  pArg                  /* isr's arg               */
    );
LOCAL STATUS vxbArmGicLvlEnable(VXB_DEVICE_ID pInst, int level);
LOCAL STATUS vxbArmGicLvlDisable(VXB_DEVICE_ID pInst, int level);
LOCAL int vxbArmGicHwLvlChg(int level);

#ifdef _WRS_CONFIG_SMP
LOCAL STATUS vxbArmGicHwEnable(int inputPin);
LOCAL STATUS vxbArmGicHwDisable(int inputPin);
void sysArmGicDevInit(void);
LOCAL STATUS vxbArmGicIpiGen(VXB_DEVICE_ID pCtlr, INT32 ipiId,
                                    cpuset_t cpus);
LOCAL STATUS vxbArmGicIpiConnect(VXB_DEVICE_ID pCtlr, INT32 ipiId, \
                                   IPI_HANDLER_FUNC ipiHandler, void * ipiArg);
LOCAL STATUS vxbArmGicIpiDisconnect(VXB_DEVICE_ID pCtlr, INT32 ipiId, \
                                   IPI_HANDLER_FUNC ipiHandler, void * ipiArg);
LOCAL STATUS vxbArmGicIpiEnable(VXB_DEVICE_ID pCtlr, INT32 ipiId);
LOCAL STATUS vxbArmGicIpiDisable(VXB_DEVICE_ID pCtlr, INT32 ipiId);
LOCAL INT32 vxbArmGicIpiPrioGet(VXB_DEVICE_ID pCtlr, INT32 ipiId);
LOCAL STATUS vxbArmGicIpiPrioSet(VXB_DEVICE_ID pCtlr, INT32 ipiId, \
                                        INT32 prio);
LOCAL VXIPI_CTRL_INIT * vxbArmGicIpiCtlGet(VXB_DEVICE_ID pInst, \
                                                  void * pArg);
LOCAL STATUS vxbArmGicIntReroute(VXB_DEVICE_ID pDev, int index, \
                                         cpuset_t destCpu);
LOCAL STATUS vxbArmGicCpuReroute(VXB_DEVICE_ID pDev,void * destCpu);

#endif /* _WRS_CONFIG_SMP */


/* armGicLinesNum is used to reduce interrupt process time */

LOCAL UINT32 armGicLinesNum = 0;

LOCAL VXB_DEVICE_ID vxbGicId;  /* GIC device ID */
LOCAL ARM_GIC_DRV_CTRL * pVxbArmGicDrvCtrl = NULL;

LOCAL struct drvBusFuncs vxbArmGicCtlFuncs =
    {
    vxbArmGicCtlInit,       /* devInstanceInit */
    vxbArmGicCtlInit2,      /* devInstanceInit2 */
    NULL                    /* devConnect */
    };

/* vxBus vxbArmGicCtl driver registration data structure */

LOCAL struct vxbDevRegInfo armGicCtlRegistration =
    {
    NULL,                         /* pNext */
    VXB_DEVID_DEVICE,             /* devID */
    VXB_BUSID_PLB,                /* busID = PLB */
    VXB_VER_4_0_0,                /* vxbVersion */
    GIC_NAME,                     /* drvName */
    &vxbArmGicCtlFuncs,           /* pDrvBusFuncs */
    NULL,                         /* pMethods */
    NULL,                         /* devProbe */
    NULL                          /* pParamDefaults */
    };

LOCAL device_method_t gicIntCtlr_methods[] =
    {
    DEVMETHOD(vxbIntCtlrConnect,    vxbArmGicConnect),
    DEVMETHOD(vxbIntCtlrDisconnect, vxbArmGicDisconnect),
    DEVMETHOD(vxbIntCtlrEnable,     vxbArmGicEnable),
    DEVMETHOD(vxbIntCtlrDisable,    vxbArmGicDisable),

#ifdef _WRS_CONFIG_SMP
    DEVMETHOD(vxIpiControlGet,      vxbArmGicIpiCtlGet),
    DEVMETHOD(vxbIntCtlrIntReroute, vxbArmGicIntReroute),
    DEVMETHOD(vxbIntCtlrCpuReroute, vxbArmGicCpuReroute),
#endif /* _WRS_CONFIG_SMP */

#ifdef INCLUDE_SHOW_ROUTINES
    DEVMETHOD(busDevShow,           vxbArmGicDataShow),
#endif /* INCLUDE_SHOW_ROUTINES */

    { 0, 0}
    };

#ifdef _WRS_CONFIG_SMP
IMPORT UINT32 vxCpuIndexGet(void);

/*
* This structure is initialized with the control functions for the IPI
* interface. This set of functions allow the CPC layer to manipulate IPI
* interrupts.
*/

LOCAL VXIPI_CTRL_INIT_DECL (vxArmGicIpiCtrlInit,
    {NULL},                   /* ipiList */
    0,                        /* pCpus */
    vxbArmGicIpiGen,          /* ipiEmitFunc */
    vxbArmGicIpiConnect,      /* ipiConnectFunc */
    vxbArmGicIpiEnable,       /* ipiEnableFunc */
    vxbArmGicIpiDisable,      /* ipiDisableFunc */
    vxbArmGicIpiDisconnect,   /* ipiDisconnFunc */
    vxbArmGicIpiPrioGet,      /* ipiPrioGetFunc */
    vxbArmGicIpiPrioSet,      /* ipiPrioSetFunc */
    ARM_GIC_IPI_COUNT,        /* ipiCount */
    NULL                      /* pCtlr */
    );
#endif /* _WRS_CONFIG_SMP */

/* CPU interrupt control with embedded assembly. 
 * GICv1~v2: SUPPORT_GICC with memory mapped register; 
 * GICv3: Must use system register for faster access
 */
 
#if !defined(__GNUC__) && !defined(__DCC__)
#error "Unknown compiler..........."
#endif

#if defined(__GNUC__)
/*****************************************************************************
*
* cpuWritePmr - write ICC_PMR
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

_WRS_INLINE void cpuWritePmr
    (
    UINT32 value
    )
    {

    /*
     * Writes to ICC_PMR are self-synchronizing,
     * no requiring an ISB or an exception boundary
     */

    __asm__ __volatile__ ("MCR p15, 0, %0, c4, c6, 0\n\t"
                          :
                          : "r" (value)
                          : "memory");
    }

/*****************************************************************************
*
* cpuWriteBpr1 - write ICC_BPR1
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

_WRS_INLINE void cpuWriteBpr1
    (
    UINT32 value
    )
    {
    __asm__ __volatile__ ("MCR p15, 0, %0, c12, c12, 3\n\t"
                          "ISB\n\t"
                          :
                          : "r" (value)
                          : "memory");
    }

/*****************************************************************************
*
* cpuWriteCtlr - write ICC_CTLR
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

_WRS_INLINE void cpuWriteCtlr
    (
    UINT32 value
    )
    {
    __asm__ __volatile__ ("MCR p15, 0, %0, c12, c12, 4\n\t"
                          "ISB\n\t"
                          :
                          : "r" (value)
                          : "memory");
    }

/*****************************************************************************
*
* cpuWriteEoir1 - write ICC_EOIR1
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

_WRS_INLINE void cpuWriteEoir1
    (
    UINT32 value
    )
    {
    __asm__ __volatile__ ("MCR p15, 0, %0, c12, c12, 1\n\t"
                          "ISB\n\t"
                          :
                          : "r" (value)
                          : "memory");
    }

/*****************************************************************************
*
* cpuWriteGrpen1 - write ICC_IGRPEN1
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

_WRS_INLINE void cpuWriteGrpen1
    (
    UINT32 value
    )
    {
    __asm__ __volatile__ ("MCR p15, 0, %0, c12, c12, 7\n\t"
                          "ISB\n\t"
                          :
                          : "r" (value)
                          : "memory");
    }
    
/*****************************************************************************
*
* cpuReadGrpen1 - Read ICC_IGRPEN1
*
* RETURNS: ICC_IGRPEN1
*
* ERRNO: N/A
*/

_WRS_INLINE UINT32 cpuReadGrpen1
    (
    void
    )
    {
    UINT32 value = 0;
    __asm__ __volatile__ ("MRC p15, 0, %0, c12, c12, 7\n\t"
                          "ISB\n\t"
                          : "=r" (value)
                          : 
                          : "memory");
      return value;
    }
    
/*****************************************************************************
*
* cpuReadIar - read ICC_IAR1, Interrupt Acknowledge Register 1
*
* RETURNS: the INTID of the signaled Group 1 interrupt
*
* ERRNO: N/A
*/

_WRS_INLINE UINT32 cpuReadIar (void)
    {
    UINT32 value = 0;

    /*
     * The effects of reading ICC_IAR0 and ICC_IAR1
     * on the state of a returned INTID are not guaranteed to be
     * visible until after the execution of a DSB.
     */

    __asm__ __volatile__ ("MRC p15, 0, %0, c12, c12, 0\n\t"
                          "DSB sy\n\t"
                          : "=r" (value)
                          :
                          : "memory");
    return value;
    }

/*****************************************************************************
*
* cpuReadPmr - read ICC_PMR, Priority Mask Register
*
* RETURNS: Priority Mask 
*
* ERRNO: N/A
*/

_WRS_INLINE UINT32 cpuReadPmr (void)
    {
    UINT32 value = 0;

    __asm__ __volatile__ ("MRC p15, 0, %0, c4, c6, 0\n\t"
                          : "=r" (value));
    return value;
    }

/*****************************************************************************
*
* cpuReadRpr - read ICC_RPR, Interrupt Controller Running Priority Register
*
* RETURNS: the Running priority of the CPU interface
*
* ERRNO: N/A
*/

_WRS_INLINE UINT32 cpuReadRpr (void)
    {
    UINT32 value = 0;

    __asm__ __volatile__ ("MRC p15, 0, %0, c12, c11, 3\n\t"
                          : "=r" (value));
    return value;
    }
#endif

#if defined(__DCC__)

/*****************************************************************************
*
* cpuWritePmr - write ICC_PMR
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
__asm volatile void __macro__cpuWritePmr (UINT32 val)
    {
% reg val
! "r0"
    mcr p15, 0, val, c4, c6, 0
    }
_WRS_INLINE void cpuWritePmr
    (
    UINT32 value
    )
    {

    /*
     * Writes to ICC_PMR are self-synchronizing,
     * no requiring an ISB or an exception boundary
     */

    __macro__cpuWritePmr(value);
    }

/*****************************************************************************
*
* cpuWriteBpr1 - write ICC_BPR1
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
__asm volatile void __macro__cpuWriteBpr1 (UINT32 val)
    {
% reg val
! "r0"
    mcr p15, 0, val, c12, c12, 3
    isb
    }
_WRS_INLINE void cpuWriteBpr1
    (
    UINT32 value
    )
    {
    __macro__cpuWriteBpr1(value);
    }

/*****************************************************************************
*
* cpuWriteCtlr - write ICC_CTLR
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
__asm volatile void __macro__cpuWriteCtlr (UINT32 val)
    {
% reg val
! "r0"
    mcr p15, 0, val, c12, c12, 4
    isb
    }
_WRS_INLINE void cpuWriteCtlr
    (
    UINT32 value
    )
    {
    __macro__cpuWriteCtlr(value);
    }

/*****************************************************************************
*
* cpuWriteEoir1 - write ICC_EOIR1
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
__asm volatile void __macro__cpuWriteEoir1 (UINT32 val)
    {
% reg val
! "r0"
    mcr p15, 0, val, c12, c12, 1
    isb
    }
_WRS_INLINE void cpuWriteEoir1
    (
    UINT32 value
    )
    {
    __macro__cpuWriteEoir1(value);
    }

/*****************************************************************************
*
* cpuWriteGrpen1 - write ICC_IGRPEN1
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
__asm volatile void __macro__cpuWriteGrpen1 (UINT32 val)
    {
% reg val
! "r0"
    mcr p15, 0, val, c12, c12, 7
    isb
    }
_WRS_INLINE void cpuWriteGrpen1
    (
    UINT32 value
    )
    {
    __macro__cpuWriteGrpen1(value);
    }


/*****************************************************************************
*
* cpuReadGrpen1 - Read ICC_IGRPEN1
*
* RETURNS: ICC_IGRPEN1
*
* ERRNO: N/A
*/
__asm volatile UINT32 __macro__cpuReadGrpen1 (void)
    {
! "r0"
   mrc p15, 0, r0, c12, c12, 7
   isb
    }
_WRS_INLINE UINT32 cpuReadGrpen1
    (
    void
    )
    {
    UINT32 value = 0;

    value =  __macro__cpuReadGrpen1();                  
    return value;
    }
    
/*****************************************************************************
*
* cpuReadIar - read ICC_IAR1, Interrupt Acknowledge Register 1
*
* RETURNS: the INTID of the signaled Group 1 interrupt
*
* ERRNO: N/A
*/
__asm volatile UINT32 __macro__cpuReadIar (void)
    {
! "r0"
   mrc p15, 0, r0, c12, c12, 0
   dsb sy
    }
_WRS_INLINE UINT32 cpuReadIar (void)
    {
    UINT32 value = 0;

    /*
     * The effects of reading ICC_IAR0 and ICC_IAR1
     * on the state of a returned INTID are not guaranteed to be
     * visible until after the execution of a DSB.
     */

    value = __macro__cpuReadIar();
    VX_SYNC_BARRIER();
    return value;
    }

/*****************************************************************************
*
* cpuReadPmr - read ICC_PMR, Priority Mask Register
*
* RETURNS: Priority Mask 
*
* ERRNO: N/A
*/
__asm volatile UINT32 __macro__cpuReadPmr (void)
    {
! "r0"
   mrc p15, 0, r0, c4, c6, 0
    }
_WRS_INLINE UINT32 cpuReadPmr (void)
    {
    UINT32 value = 0;

    value = __macro__cpuReadPmr();
    return value;
    }

/*****************************************************************************
*
* cpuReadRpr - read ICC_RPR, Interrupt Controller Running Priority Register
*
* RETURNS: the Running priority of the CPU interface
*
* ERRNO: N/A
*/
__asm volatile UINT32 __macro__cpuReadRpr (void)
    {
! "r0"
   mrc p15, 0, r0, c12, c11, 3
    }
_WRS_INLINE UINT32 cpuReadRpr (void)
    {
    UINT32 value = 0;

    value = __macro__cpuReadRpr();
    return value;
    }
#endif

/*******************************************************************************
*
* vxbArmGicNonPreempISR - non-pre-emptive interrupt exception handler
*
* This handler does not provide pre-emptive interrupts. If a high
* priority interrupt occurs while a low priority interrupt is being
* handled, the high priority interrupt must wait for the low priority
* interrupt handler to finish.  As soon as the low-priority handler is
* done, the high priority handler will be invoked.  This model has less
* exception handling overhead of the fully pre-emptive model, but has a
* greater worst case latency for high priority interrupts.
*
* RETURNS: N/A
*
* ERROR: N/A
*/

LOCAL void vxbArmGicNonPreempISR (void)
    {
    int level;
    int vector;
#ifdef _WRS_CONFIG_SV_INSTRUMENTATION
    int loopCnt = -1;
#endif
#ifdef _WRS_CONFIG_SMP
    int srcCpuId = 0;
#endif
     
#ifdef _WRS_CONFIG_SMP
    if (vxbArmGicLvlVecChk (pVxbArmGicDrvCtrl->pInst, &level, &vector,
                            &srcCpuId) == ERROR)
#else
    if (vxbArmGicLvlVecChk (pVxbArmGicDrvCtrl->pInst, &level, &vector) == ERROR)
#endif
        {
        return;
        }
   
    do
        {

        /* Loop until no more interrupts are found */

#ifdef _WRS_CONFIG_SV_INSTRUMENTATION

        /*
         * In the ARM architecture, exceptions cannot be locked out
         * with intCpuLock() which makes a two-stage logging approach (i.e.
         * timestamp saved in intEnt and then used here) dangerous...it
         * can lead to out-of sequence events in the event log, thus
         * confusing the parser. So we just use a single stage logging
         * here.
         */

        WV_EVT_INT_ENT (vector)
        loopCnt++;

#endif  /* _WRS_CONFIG_SV_INSTRUMENTATION */
        
        VXB_INTCTLR_ISR_CALL (&(pVxbArmGicDrvCtrl->isrHandle), vector)

        /* acknowledge the interrupt and restore interrupt level */

#ifdef _WRS_CONFIG_SMP
        vxbArmGicLvlVecAck (pVxbArmGicDrvCtrl->pInst, level, vector, srcCpuId);
#else
        vxbArmGicLvlVecAck (pVxbArmGicDrvCtrl->pInst, level, vector);
#endif
        }
#ifdef _WRS_CONFIG_SMP
        while (vxbArmGicLvlVecChk (pVxbArmGicDrvCtrl->pInst, &level, &vector,
                                   &srcCpuId) != ERROR);
#else
        while (vxbArmGicLvlVecChk (pVxbArmGicDrvCtrl->pInst, &level, &vector)
                                   != ERROR);
#endif

#ifdef _WRS_CONFIG_SV_INSTRUMENTATION
    while (loopCnt-- > 0)
        EVT_CTX_0(EVENT_INT_EXIT);
#endif

    }


/*******************************************************************************
*
* vxbArmGicPreempISR - pre-emptive interrupt exception handler
*
* This handler is fully pre-emptive. In this model, high priority
* interrupts are enabled during the processing of low-priority
* interrupts. Should a high priority interrupt occur, the low-priority
* handler is interrupted and the high priority handler takes over.
*
* \NOMANUAL
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void vxbArmGicPreempISR(void)
    {
    int vector, level;
#ifdef _WRS_CONFIG_SMP
    int srcCpuId = 0;
#endif

#ifdef _WRS_CONFIG_SMP
    if (vxbArmGicLvlVecChk (pVxbArmGicDrvCtrl->pInst, &level, &vector,
                            &srcCpuId) == ERROR)
#else
    if (vxbArmGicLvlVecChk (pVxbArmGicDrvCtrl->pInst, &level, &vector) == ERROR)
#endif
        {
        return;
        }


#ifdef _WRS_CONFIG_SV_INSTRUMENTATION

    /*
     * In the ARM architecture, exceptions cannot be locked out with intCpuLock()
     * which makes a two-stage logging approach (i.e. timestamp saved in intEnt
     * and then used here) dangerous...it can lead to out-of sequence events
     * in the event log, thus confusing the parser. So we just use a single
     * stage logging here
     */

    WV_EVT_INT_ENT(vector)
#endif  /* _WRS_CONFIG_SV_INSTRUMENTATION */

    CPU_INTERRUPT_ENABLE;

    VXB_INTCTLR_ISR_CALL (&(pVxbArmGicDrvCtrl->isrHandle), vector)

    CPU_INTERRUPT_DISABLE;

    /* acknowledge the interrupt and restore interrupt level */

#ifdef _WRS_CONFIG_SMP
    vxbArmGicLvlVecAck (pVxbArmGicDrvCtrl->pInst, level, vector, srcCpuId);
#else
    vxbArmGicLvlVecAck (pVxbArmGicDrvCtrl->pInst, level, vector);
#endif /* _WRS_CONFIG_SMP */

    }

/*******************************************************************************
*
* vxbFTIntCtlrRegister - register GIC driver
*
* This routine registers the GIC driver with vxbus as a child
* of the PLB bus type.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFTIntCtlrRegister(void)
   {
   vxbDevRegister ((struct vxbDevRegInfo *)&armGicCtlRegistration);
   }

/*******************************************************************************
*
* vxbArmGenIntCtlrInit - initialize the device
*
* This is the vxbArmGenIntrClt initialization routine.  It retrieves and stores
* device driver control data, connects the device driver specific routines into
* the architecture level hooks and initializes the interrupt controller.
* If the BSP needs to create a wrapper routine around any of the architecture
* level routines, it should install the pointer to the wrapper routine after
* calling this routine.
*
* NOTE:
*
* This routine is called early during system initialization (sysHwInit), and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O. It is called from the vxBus routine hardWareInterFaceInit();
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbArmGicCtlInit
    (
    VXB_DEVICE_ID pInst
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl;
    HCF_DEVICE * pHcf;
    INT32                 gicDistOffset   = 0; /* Distributor register base offset */
    INT32                 gicReDistOffset = 0; /* Redistributor register base offset */

    /* get the HCF device from the instance ID */

    pHcf = hcfDeviceGet (pInst);

    /* if pHcf is NULL, no device is present in hwconf.c */

    if (pHcf == NULL)
        return;

    /* allocate memory for the data */

    pDrvCtrl = (ARM_GIC_DRV_CTRL *)hwMemAlloc (sizeof(ARM_GIC_DRV_CTRL));

    if (pDrvCtrl == NULL)
        return;
    memset((void*)pDrvCtrl, 0, sizeof(ARM_GIC_DRV_CTRL));

    pDrvCtrl->pInst = pInst;

    vxbGicId = pInst;

    /* update the driver control data pointer */

    pInst->pDrvCtrl = pDrvCtrl;

    pVxbArmGicDrvCtrl = pDrvCtrl;

    /* get connectivity info from hwconf */

   intCtlrHwConfGet (pInst, pHcf, &(pDrvCtrl->isrHandle));

   /*
    * resourceDesc {
    * The intMode resource specifies the interrupt mode. Interrupts can be in
    * either preemptive or non-preemptive mode, and the associated macro
    * definition is INT_PREEMPT_MODEL or INT_NON_PREEMPT_MODEL.
    *
    * The maxIntLvl resource specifies the number of interrupt level.
    *
    * The maxCpuNum resource specifies the number of CPU. It is 1 for uni-core
    * system. It may be 2, 3 or 4 for multi-core system.
    }
    */

    if (devResourceGet (pHcf, "intMode", HCF_RES_INT,
                        (void *)&pDrvCtrl->intMode) != OK)
        {
        goto error;
        }


    /*
     * Get the distributor and CPU interface register base addresses,
     * using the default (for backward-compatibility with ARM11 MPCore and
     * Cortex-A9) if they're not supplied by the BSP.
     */

    if (devResourceGet (pHcf, "distOffset", HCF_RES_INT,
                        (void *)&gicDistOffset) != OK)
        {

        /* not found, so use default */

        gicDistOffset = 0;
        }

    pDrvCtrl->gicDist = (UINT32)pInst->pRegBase[0] + gicDistOffset;
    pInst->regBaseFlags[0] = VXB_REG_IO;
    vxbRegMap(pInst, 0, &pDrvCtrl->gicDistHdl);

    pDrvCtrl->gicRedistRegionNum = 1;
    pDrvCtrl->gicRedistStride =  0; /* no stride, use redistOffset */
    pDrvCtrl->gicRedistRegion =
        (GIC_REDIST_REGION *)hwMemAlloc (sizeof (GIC_REDIST_REGION) *
                                          pDrvCtrl->gicRedistRegionNum);
    if (pDrvCtrl->gicRedistRegion == NULL)
        goto error;

    memset((void*)pDrvCtrl->gicRedistRegion, 0, sizeof (GIC_REDIST_REGION) *
                                          pDrvCtrl->gicRedistRegionNum);
    
    if (devResourceGet (pHcf, "redistOffset", HCF_RES_INT,
                          (void *)&gicReDistOffset) != OK)
          {

          /* not found, so use default */

          gicReDistOffset = 0x80000;
          }

    pDrvCtrl->gicRedistRegion[0].rdRgnBase = (UINT32)pInst->pRegBase[0] + gicReDistOffset;
    pDrvCtrl->gicRedistRegion[0].rdRgnHdl  = pDrvCtrl->gicDistHdl;

    /* get the maximum interrupt level number from hardware register */
 
     pDrvCtrl->gicLinesNum = ((DIST_READ_4(pDrvCtrl, GICD_TYPER) & 0x1f) + 1) * 32;
     
    armGicLinesNum = pDrvCtrl->gicLinesNum;
    if (pDrvCtrl->gicLinesNum > GIC_INT_MAX_NUM)
        {
        goto error;
        }

    SPIN_LOCK_ISR_INIT (&pDrvCtrl->spinLock, 0);

#ifdef _WRS_CONFIG_SMP

    /* get the maximum CPU numbers */

    pDrvCtrl->gicCpuNum = sysCpuAvailableGet ();
#else
    pDrvCtrl->gicCpuNum = 1;
#endif /* _WRS_CONFIG_SMP */

    pInst->pMethods = &gicIntCtlr_methods[0];

    /* Install three pointers for legacy type intEnable/intDisable/intLevelSet */

    sysIntLvlEnableRtn  = (FUNCPTR)sysArmGicEnable;
    sysIntLvlDisableRtn = (FUNCPTR)sysArmGicDisable;
    sysIntLvlChgRtn     = (FUNCPTR)vxbArmGicHwLvlChg;

    if (_func_intConnectRtn == NULL)
        _func_intConnectRtn = sysArmGicConnect;

    if (_func_intDisconnectRtn == NULL)
        _func_intDisconnectRtn = sysArmGicDisconnect;

    /* initialize the GIC */
    
    vxbArmGicDevInit (pInst);
 
    /* assign ISR to hook (defined in excArchLib.c) */
 
    if (pDrvCtrl->intMode & INT_PREEMPT_MODEL)
        EXC_CONNECT_INTR_RTN (vxbArmGicPreempISR);
    else
        EXC_CONNECT_INTR_RTN (vxbArmGicNonPreempISR);
    return;

error:

    if (pDrvCtrl->gicRedistRegion != NULL)
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        (void) hwMemFree ((char*)pDrvCtrl->gicRedistRegion);
#endif
        }

        
    if (pDrvCtrl != NULL)
    {
#ifndef _VXBUS_BASIC_HWMEMLIB
        (void) hwMemFree ((char*)pDrvCtrl);
#endif
    }
    return;
    }

/*******************************************************************************
*
* vxbArmGenIntCtlrInit2 - second level initialization of the ARM GIC
*
* This is the second level ARM GIC initialization routine.  Nothing needs to
* be done during this level of initialization.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbArmGicCtlInit2
    (
    VXB_DEVICE_ID pInst
    )
    {
    }

/*******************************************************************************
*
* vxbArmGicDevInit - initialize the interrupt controller
*
* This routine initializes the interrupt controller device, disabling all
* interrupt sources. It connects the device driver specific routines
* into the architecture level hooks. If the BSP needs to create a wrapper
* routine around any of the architecture level routines, it should install the
* pointer to the wrapper routine after calling this routine.
*
* RETURNS: OK or ERROR if parameter is invalid.
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicDevInit
    (
    VXB_DEVICE_ID pInst
    )
    {

    ARM_GIC_DRV_CTRL * pDrvCtrl;
	int logicCpuId;

    /* if parameters are invalid, return ERROR */

    if ((pInst == NULL) || (pInst->pDrvCtrl == NULL))
        return (ERROR);

    /* retrieve the data */

    pDrvCtrl = (ARM_GIC_DRV_CTRL *)(pInst->pDrvCtrl);

    pDrvCtrl->gicLvlCurrent = GIC_INT_ALL_ENABLED;

    /* global distributor init */
    
    vxbArmGicIntDevDistInit();

    /* private redistributor and cpu interfaces init */

	logicCpuId = vxCpuIndexGet();
    vxbArmGicIntDevRedistCpuInit(logicCpuId); /* master core */


    return OK;
    }

/*******************************************************************************
*
* vxbArmGicIntDevDistInit - initialize the interrupt controller
*
* This routine initializes the interrupt controller device, disabling all
* interrupt sources. It connects the device driver specific routines into the
* architecture level hooks.  If the BSP needs to create a wrapper routine
* around any of the architecture level routines, it should install the pointer
* to the wrapper routine after calling this routine.
*
* Note that for multicore processor, some registers are banked for each process.
* This routine uses CPU0 to initialize the common resources and banked
* registers are processed by every processor.
*
* RETURNS: always OK
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicIntDevDistInit
    (
    void
    )
    {
    int                       i;
    UINT32                    aGicCfg = 0;
    STATUS                      stat;
    HCF_DEVICE                * pHcf;
    struct intrCtlrTrigger    * pTrigTable = NULL;
    int                         tableSize;
    void *                      pValue;
    UINT32 vector, offset;
    ARM_GIC_DRV_CTRL *  pCtrl = NULL;
	UINT32  bootCpuBit  = 0;

    pCtrl =  (ARM_GIC_DRV_CTRL *)(vxbGicId->pDrvCtrl);
    if (pCtrl == NULL)
        return (ERROR);

	bootCpuBit = vxMpidrGet () & MPIDR_AFFINITY_MASK;
    pCtrl->bootCpuId = vxCpuIndexGet();
    pCtrl->bootCpuBit = bootCpuBit;

    pHcf = hcfDeviceGet (vxbGicId);
    if (pHcf == NULL)
        {
        return (ERROR);
        }

   /* GICv3_Software_Overview_Official_Release_B.pdf
    * The Distributor control register (GICD_CTLR) must be configured to 
    * enable the interrupt groups and to set the routing mode.
    */
    
    DIST_WRITE_4 (pCtrl, GICD_CTLR, GICD_CTLR_ARE_NS | GICD_CTLR_ENABLE_G1A | GICD_CTLR_ENABLE_G1);
    DIST_SYNC(pCtrl);
        
    /* Get the max priority */
    vector = SPI_START_INT_NUM;
    offset = (vector / 4) * 4;
    DIST_WRITE_4(pCtrl, GICD_IPRIORITYR + offset, 0xff);
    pCtrl->gicLvlCurrent = DIST_READ_4(pCtrl, GICD_IPRIORITYR + offset);
    DIST_WRITE_4(pCtrl, GICD_IPRIORITYR + offset, 0);

    /*
     * Disable all SPI interrupts
     * Clear all pending SPI interrupts in the distributor
     */

    for(i = SPI_START_INT_NUM; i < GIC_INT_MAX_NUM; i += BITS_PER_WORD)
        {
        offset =(UINT32) ((i / 32) * 4);
        DIST_WRITE_4 (pCtrl, (GICD_ICENABLER + offset), 0xffffffff);
        DIST_WRITE_4 (pCtrl, (GICD_ICPENDR + offset), 0xffffffff);
        }

    /*
     * set default priority for all SPI interrupts to level 0 and direct all
     * interrupts to go to  <<< this local boot CPU >>>
     */

    for (i = SPI_START_INT_NUM; i < armGicLinesNum; )
        {
         offset = (i / 4) * 4;
         DIST_WRITE_4(pCtrl, GICD_IPRIORITYR + offset, GIC_SPI_PRIORITY_DEFAULT);

         offset = i * 8;
         DIST_WRITE_8 (pCtrl, GICD_IROUTER + offset, bootCpuBit);
          i ++;
          
        }   

    /* setting whether 1-N/N-N and Level/Edge triggered */

    for (i = SPI_START_INT_NUM; i < armGicLinesNum; i += CONFIGS_PER_WORD)
    {
        offset = (i / 16) * 4;
        DIST_WRITE_4(pCtrl, GICD_ICFGR + offset, GIC_INT_ONEMINUS_HIGH);/* 1-N, Level */
    }
   
    /*
     * get trigger table from hwconf
     *
     * resourceDesc {
     * The trigger resource specifies a pointer to a
     * intrCtlrTrigger structure which configures triggering
     * for particular interrupt vectors. }
     */

    stat = devResourceGet(pHcf, "trigger", HCF_RES_ADDR, &pValue);
    pTrigTable = (struct intrCtlrTrigger *)pValue;
    if (stat == OK)
        {
        stat = devResourceGet(pHcf, "triggerTableSize", HCF_RES_INT,
                              (void *) &tableSize);
        if (stat == OK)
            {
            for ( i = 0 ; i < tableSize ; i++ )
                {
                /* do not set SGI */

                if (pTrigTable->inputPin < PPI_START_INT_NUM)
                    {
                    pTrigTable++;
                    continue;
                    }
                    
                if (pTrigTable->inputPin > armGicLinesNum)
                    {
                    pTrigTable++;
                    continue;
                    }

                /*
                 * The GIC does not permit use of the Interrupt
                 * Configuration Register to program the trigger
                 * mode of PPIs.
                 */

                if (pTrigTable->inputPin >= SPI_START_INT_NUM)
                    {
                    offset = (pTrigTable->inputPin / 16) * 4;
                    aGicCfg = DIST_READ_4(pCtrl, GICD_ICFGR + offset);

                    /* GIC supports rising edge and active high level */

                    if (pTrigTable->trigger & VXB_INTR_TRIG_LEVEL)
                        {
                        aGicCfg &=
                           ~(BIT32 (((pTrigTable->inputPin % CONFIGS_PER_WORD)
                           * GIC_INT_TRIGGER_SHIFT + 1)));
                        }
                    else if (pTrigTable->trigger & VXB_INTR_TRIG_EDGE)
                        {
                        aGicCfg |=
                             BIT32 (((pTrigTable->inputPin % CONFIGS_PER_WORD)
                             * GIC_INT_TRIGGER_SHIFT + 1));
                        }
                        
                    DIST_WRITE_4(pCtrl, GICD_ICFGR + offset, aGicCfg);
                    }
                pTrigTable++;
                }
            }
        }
        
    return OK;
    }


 
/*****************************************************************************
*
* vxbArmGicRedistBaseGet - get redistributor base address for every PE
* 
* It must be called before accessing redistributor registers.
*
* RETURNS: OK, or ERROR if initialization failed.
*
* ERRNO: N/A
*/

STATUS vxbArmGicRedistBaseGet
    (
    UINT32              cpuId
    )
    {
    UINT32              i       = 0;
    UINT32              base    = 0;
    UINT64              typer   = 0;
    GIC_REDIST_REGION * pRdRgn  = NULL;
#ifdef _WRS_CONFIG_LP64
    UINT64              cpuBit  = vxMpidrGet () & MPIDR_EL1_AFFINITY_MASK;
#else
    UINT64              cpuBit  = vxMpidrGet () & MPIDR_AFFINITY_MASK;
#endif

    UINT32              offset  = 0;
    STATUS              stat  = ERROR;
    ARM_GIC_DRV_CTRL *  pCtrl = NULL;

    GIC_DBG (GIC_DBG_ALL, "%s,%d Enter... logical cpuId: %d \r\n",
	     __func__, __LINE__, cpuId);

    pCtrl =  (ARM_GIC_DRV_CTRL *)(vxbGicId->pDrvCtrl);
    if (pCtrl == NULL)
        return ERROR;

    if (pCtrl->gicRedistRegion == NULL)
        {
        GIC_DBG (GIC_DBG_ERR, "%s,%d No gicRedistRegion!\r\n", __func__, __LINE__);
        return ERROR;
        }
  
    for (i = 0; i < pCtrl->gicRedistRegionNum; i++)
        {
        offset = 0;

        pRdRgn = &pCtrl->gicRedistRegion[i];
        base = pRdRgn->rdRgnBase;

        while (GICR_TYPER_LAST != (typer & GICR_TYPER_LAST))
            {
            typer = RDRGN_READ_8 (pRdRgn, GICR_TYPER + offset);

            /* assume Aff3 and RES0 always zero. only compare Aff0/1/2 */
            if (cpuBit == (typer >> GICR_TYPER_AFFINITY_SHIFT))
                {
                pCtrl->gicRedist[cpuId].rdBase  = base;
                pCtrl->gicRedist[cpuId].sgiBase = base + GIC_SZ_64K;
                pCtrl->gicRedist[cpuId].rdHdl   = pRdRgn->rdRgnHdl;

#ifdef _WRS_CONFIG_LP64
                GIC_DBG (GIC_DBG_INFO, "#RedistInit cpuBit=0x%llx; cpuId=%d; rdBase=0x%llx\r\n",
                    cpuBit, cpuId, base);
#else
                GIC_DBG (GIC_DBG_INFO, "#RedistInit cpuBit=0x%x; cpuId=%d; rdBase=0x%x\r\n",
                    (UINT32)(cpuBit&0xFFFFFFFF), cpuId, (UINT32)base);
#endif

                /* got correct base address on this CPU */
                
                return stat;
                }

            if (pCtrl->gicRedistStride)
                {
                base = base + pCtrl->gicRedistStride;
                offset += pCtrl->gicRedistStride;
                }
            else
                {
                base += GIC_SZ_64K * 2;
                offset += GIC_SZ_64K * 2;
                if (typer & GICR_TYPER_VLPIS)
                    {
                    base += GIC_SZ_64K * 2;
                    offset += GIC_SZ_64K * 2;
                    }
                }
            }
        }
        
    return ERROR;
    }


LOCAL STATUS vxbArmGicIntDevRedistCpuInit
    (
    int cpuNum
    )
    {
    int                       i;
    UINT32                    aGicPrio = 0;
    STATUS                      stat;
    HCF_DEVICE                * pHcf;
    struct intrCtlrPriority   * pPrioTable;
    int                         tableSize;
    void *                      pValue;
    UINT32 val; 
    UINT32 count = 1000;    /* 1s! */
    UINT32 offset = 0;
	UINT32 value  = 0;
    ARM_GIC_DRV_CTRL *  pCtrl = NULL;
    GIC_REDIST *        pRedist;

    pCtrl =  (ARM_GIC_DRV_CTRL *)(vxbGicId->pDrvCtrl);
    if (pCtrl == NULL)
        return (ERROR);

    pHcf = hcfDeviceGet (vxbGicId);
    if (pHcf == NULL)
        {
        return (ERROR);
        }

    /* Get redistributor base addr on this cpu */

    vxbArmGicRedistBaseGet(cpuNum); 
    pRedist = &pCtrl->gicRedist[cpuNum];

	if (0 == pRedist->rdBase)
		{
        GIC_DBG (GIC_DBG_ERR, "%s,%d No gicRedistRegion!\r\n", __func__, __LINE__);		
		return (ERROR);
		}

    val = RDIST_READ_4 (pRedist, GICR_WAKER);
    /* Wake up this CPU redistributor */
#define GICR_WAKER_ProcessorSleep   (1U << 1)
#define GICR_WAKER_ChildrenAsleep   (1U << 2)
    val &= ~GICR_WAKER_ProcessorSleep;
    RDIST_WRITE_4 (pRedist, GICR_WAKER, val);

    while (--count) {
            val = RDIST_READ_4 (pRedist, GICR_WAKER);
            if (1 ^ (val & GICR_WAKER_ChildrenAsleep))
                break;

        };

        /* set non-secure group1 */

    RDSGI_WRITE_4 (pRedist, GICR_IGROUPR0, 0xffffffff);
    RDSGI_WRITE_4 (pRedist, GICR_IGRPMODR0, 0x0);

    /* set PPI level trigger, SGI edge trigger */

    RDSGI_WRITE_4 (pRedist, GICR_ICFGR0, 0xaaaaaaaa);
    RDSGI_WRITE_4 (pRedist, GICR_ICFGR1, 0);

	/* SGI must be same as bootCpuId */
    GIC_DBG (GIC_DBG_ERR, "%s,%d  bootCpuId=%d, idx=%d\r\n", 
             __func__, __LINE__, pCtrl->bootCpuId,
             vxCpuIndexGet());
    if (pCtrl->bootCpuId != vxCpuIndexGet())
        {
        value = RDSGI_READ_4 (&(pCtrl->gicRedist[pCtrl->bootCpuId]),
                              GICR_ISENABLER0);
        RDSGI_WRITE_4 (pRedist, GICR_ISENABLER0, (value & 0x0000ffff));
        }
	

    RDSGI_WRITE_4 (pRedist, GICR_ICENABLER0, ALL_PPI_INT_MASK);
    RDSGI_WRITE_4 (pRedist, GICR_ISENABLER0, ALL_SGI_INT_MASK);

    /* clear all pending PPI and SGI interrupts in the Redistributor */                                                      

    RDSGI_WRITE_4 (pRedist, GICR_ICPENDR0, ALL_PPI_INT_MASK | ALL_SGI_INT_MASK);
 
    /* set default priority for all PPI and SGI interrupts to level 0(highest) */

    for (i = 0; i < SPI_START_INT_NUM; i += PRIOS_PER_WORD)
    {
        offset = i / 4 * 4;
        RDSGI_WRITE_4 (pRedist, GICR_IPRIORITYR0+offset, GIC_IPI_PRIORITY);
    }

   
    /*
     * get priority table from hwconf
     *
     * resourceDesc {
     * The priority resource specifies a pointer to a
     * intrCtlrPriority structure which assigns priority
     * levels to particular interrupt vectors. }
     */

    stat = devResourceGet(pHcf, "priority", HCF_RES_ADDR, &pValue);
    pPrioTable = (struct intrCtlrPriority *)pValue;
    if (stat == OK)
        {
        stat = devResourceGet(pHcf, "priorityTableSize", HCF_RES_INT,
                              (void *) &tableSize);
        if (stat == OK)
            {
            for ( i = 0 ; i < tableSize ; i++ )
                {

                /* do not set SGI */

                if (pPrioTable->inputPin < PPI_START_INT_NUM)
                    {
                    pPrioTable++;
                    continue;
                    }

                /* use CPU0 to set the common SPI priority */

                if ((pPrioTable->inputPin >= SPI_START_INT_NUM) &&
                    (cpuNum != 0))
                    {
                    pPrioTable++;
                    continue;
                    }
                
                if (pPrioTable->inputPin < SPI_START_INT_NUM) /* PPI */
                    {
                    offset = pPrioTable->inputPin / 4 * 4;
                    aGicPrio = RDSGI_READ_4 (pRedist, GICR_IPRIORITYR0);
                    }
                else /* SPI */
                    {
                    offset = (pPrioTable->inputPin / 4) * 4;
                    aGicPrio = DIST_READ_4(pCtrl, GICD_IPRIORITYR + offset);    
                    }

                /*
                 * Priority range:
                 *
                 * 0x0  == GIC_INT_HIGHEST_PRIORITY, the highest priority
                 * 0xFF == GIC_INT_LOWEST_PRIORITY, the lowest priority
                 */

                if (pPrioTable->priority > pCtrl->gicLvlCurrent)
                    {
                    aGicPrio |=
                        ((pCtrl->gicLvlCurrent & GIC_INT_PRIORITY_MASK)
                        << ((pPrioTable->inputPin % PRIOS_PER_WORD)
                        * GIC_INT_PRIORITY_SHIFT));
                    }
                else if ((pPrioTable->inputPin >= SPI_START_INT_NUM) &&
                         (pPrioTable->inputPin <= armGicLinesNum) &&
                         (pPrioTable->priority < GIC_PRIORITY_LEVEL_STEP))
                    {
                    aGicPrio |=
                        ((GIC_PRIORITY_LEVEL_STEP & GIC_INT_PRIORITY_MASK)
                        << ((pPrioTable->inputPin % PRIOS_PER_WORD)
                        * GIC_INT_PRIORITY_SHIFT));
                    }
                else
                    {
                    aGicPrio |=
                        ((pPrioTable->priority & GIC_INT_PRIORITY_MASK)
                        << ((pPrioTable->inputPin % PRIOS_PER_WORD)
                        * GIC_INT_PRIORITY_SHIFT));
                    }
                
                if(pPrioTable->inputPin < SPI_START_INT_NUM) /* PPI */
                    {
                    offset = pPrioTable->inputPin / 4 * 4;
                    RDSGI_WRITE_4 (pRedist, GICR_IPRIORITYR0, aGicPrio);
                    }
                else /* SPI */
                    {
                    offset = (pPrioTable->inputPin / 4) * 4;
                    DIST_WRITE_4(pCtrl, GICD_IPRIORITYR + offset,aGicPrio);
                    }
                
                pPrioTable++;
                }
            }
        }

     RDIST_SYNC(pRedist);

    /* disable this processor's CPU interface */

    cpuWritePmr(0xf0); /* ICC_PMR. Priority Mask Register */
    cpuWriteCtlr(0);   /* ICC_CTLR.  */
    cpuWriteGrpen1(1); /* ICC_IGRPEN1. Group 1 Enable */
  
    if((cpuReadGrpen1() & 1) != 1)
    {
        return (ERROR);
    }

    if (pVxbArmGicDrvCtrl != NULL)
        {
        if (pVxbArmGicDrvCtrl->intMode & INT_PREEMPT_MODEL)
            {
            cpuWriteBpr1(GIC_CPU_BINP_PREEMPT_VAL);
            }
        else
            {
            cpuWriteBpr1(GIC_CPU_BINP_DEFAULT);           
            }
        }
   
    /* enable this processor's CPU interface */
 
    cpuWriteCtlr(GIC_CONTROL_ENABLE);
     
    return OK;
    }
    

/*******************************************************************************
*
* sysArmGicConnect - interrupt instance connect handler
*
* This function implements the connection of interrupt handler. It's used to
* support intConnect(...).
*
* NOTE:
*
* This routine is called somewhere by legacy driver.
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS sysArmGicConnect
    (
    VOIDFUNCPTR *  vector,     /* interrupt vector to attach to */
    VOIDFUNCPTR    routine,    /* routine to be called */
    int            parameter   /* parameter to be passed to routine */
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl = vxbGicId->pDrvCtrl;
   
    if ((int)(vector) >= pDrvCtrl->gicLinesNum)
        return(ERROR);

    GIC_DBG(GIC_DBG_INFO, "%s,%d vector=%d\r\n", __FUNCTION__, __LINE__, (UINT32)vector);

    return(intCtlrISRAdd(&pDrvCtrl->isrHandle, (int)vector, routine,
                         (void *)parameter));
    }

/*******************************************************************************
*
* vxbArmGicConnect - VxBus instance connect handler
*
* This function implements the VxBus InstConnect handler for an GIC instance.
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicConnect
    (
    VXB_DEVICE_ID       pIntCtlr,
    VXB_DEVICE_ID       pDev,
    int                 index,
    void                (*pIsr)(void * pArg),
    void *              pArg,
    int *               pInputPin
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl = pIntCtlr->pDrvCtrl;
    int     inputPin;

    /* get interrupt input pin */

    inputPin = intCtlrPinFind (pDev, index, pIntCtlr, &pDrvCtrl->isrHandle);
  
    if (inputPin == ERROR)
        return (ERROR);

    *pInputPin = inputPin;
    
    GIC_DBG(GIC_DBG_INFO, "%s,%d inputPin=%d\r\n", __FUNCTION__, __LINE__, inputPin);


    /* assign the ISR and arg to the specified cpu input pin */

    if (intCtlrISRAdd(&pDrvCtrl->isrHandle,
                  inputPin, pIsr, pArg) != OK)
        return (ERROR);

    return (OK);
    }

/*******************************************************************************
*
* sysArmGicDisconnect - disconnect a C routine from an interrupt
*
* This routine disconnects a specified C routine from a specified
* interrupt vector. It's used to support intDisconnect(...).
*
* This routine is called somewhere by legacy driver.
*
* RETURNS: OK, or ERROR if <vector> is out of range.
*
* ERRNO: N/A
*/

LOCAL STATUS sysArmGicDisconnect
    (
    VOIDFUNCPTR *  vector,     /* interrupt vector to detach from */
    VOIDFUNCPTR    routine,    /* routine to be disconnected      */
    int            parameter   /* parameter to be matched         */
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl = vxbGicId->pDrvCtrl;

    if ((int)(vector) >= pDrvCtrl->gicLinesNum)
        return(ERROR);

    return(intCtlrISRRemove(&pDrvCtrl->isrHandle, (int)vector, routine,
                            (void *)parameter));
    }

/*******************************************************************************
*
* vxbArmGicDisconnect - disconnect device interrupt
*
* This routine disconnects the supplied routine and arg from the interrupt
* input found with intCtlrPinFind.
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicDisconnect
    (
    VXB_DEVICE_ID       pIntCtlr,
    VXB_DEVICE_ID       pDev,
    int                 index,
    VOIDFUNCPTR         pIsr,
    void *              pArg
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl = pIntCtlr->pDrvCtrl;
    int inputPin;

    /* get interrupt input pin */

    inputPin = intCtlrPinFind (pDev, index, pIntCtlr, &pDrvCtrl->isrHandle);
    if (inputPin == ERROR)
        return (ERROR);

    /* remove the ISR and arg from the specified cpu input pin */

    if (intCtlrISRRemove(&pDrvCtrl->isrHandle, inputPin, pIsr, pArg) != OK)
        return (ERROR);

    return (OK);
    }

/*******************************************************************************
*
* sysArmGicISREnable - enable interrupt handler
*
* This routine enables interrupt for the configured input pin
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS sysArmGicISREnable
    (
    struct intCtlrHwConf *  pEntries,
    int                     inputPin
    )
    {
    struct intCtlrISRChainEntry * pChain;
    UINT32                        flagValue;
    VOIDFUNCPTR                   func;

    /* make sure top-level enabled */

    flagValue = intCtlrTableFlagsGet(pEntries, inputPin);
    flagValue |= VXB_INTCTLR_FLG_ENABLE;
    intCtlrTableFlagsSet(pEntries, inputPin, flagValue);

    func = intCtlrTableIsrGet(pEntries, inputPin);
    if ( func == intCtlrChainISR )
        {
        pChain = (struct intCtlrISRChainEntry *)
        intCtlrTableArgGet(pEntries, inputPin);
        while (pChain != NULL)
            {
            pChain->flags |= VXB_INTCTLR_FLG_ENABLE;
            pChain = pChain->pNext;
            }
        }

    return(OK);
    }

/*******************************************************************************
*
* sysArmGicEnable - enable an interrupt handler from the vector table
*
* This routine enables interrupt handler from the vector table. It's used
* to support intEnable(...).
*
* This routine is called somewhere by legacy driver.
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS sysArmGicEnable
    (
    int vector
    )
    {
    ARM_GIC_DRV_CTRL *pDrvCtrl = vxbGicId->pDrvCtrl;
    if ((int)(vector) >= pDrvCtrl->gicLinesNum)
        return(ERROR);

    if (sysArmGicISREnable(&(pDrvCtrl->isrHandle), vector) != OK)
        return (ERROR);

    vxbArmGicLvlEnable(pDrvCtrl->pInst, vector);
    return (OK);
    }

/*******************************************************************************
*
* vxbArmGicEnable - enable device interrupt
*
* This routine enables interrupt for the configured input pin
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicEnable
    (
    VXB_DEVICE_ID       pIntCtlr,
    VXB_DEVICE_ID       pDev,
    int                 index,
    VOIDFUNCPTR         pIsr,
    void *              pArg
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl = pIntCtlr->pDrvCtrl;
    int inputPin;

    /* get interrupt input pin */

    inputPin = intCtlrPinFind (pDev, index, pIntCtlr, &pDrvCtrl->isrHandle);
    if (inputPin == ERROR)
        return (ERROR);

    /* enable ISR */

    if (intCtlrISREnable(&pDrvCtrl->isrHandle, inputPin, pIsr, pArg) != OK)
        return (ERROR);

    vxbArmGicLvlEnable(pDrvCtrl->pInst, inputPin);

    return (OK);
    }

/*******************************************************************************
*
* sysArmGicISRDisable - disable interrupt handler
*
* This routine disables interrupt for the configured input pin
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS sysArmGicISRDisable
    (
    struct intCtlrHwConf *  pEntries,
    int                     inputPin
    )
    {
    struct intCtlrISRChainEntry * pChain;
    UINT32                        flagValue;
    VOIDFUNCPTR                   func;

    /* make sure top-level disabled */

    flagValue = intCtlrTableFlagsGet(pEntries, inputPin);
    flagValue &= ~VXB_INTCTLR_FLG_ENABLE;
    intCtlrTableFlagsSet(pEntries, inputPin, flagValue);

    func = intCtlrTableIsrGet(pEntries, inputPin);
    if ( func == intCtlrChainISR )
        {
        pChain = (struct intCtlrISRChainEntry *)
        intCtlrTableArgGet(pEntries, inputPin);
        while (pChain != NULL)
            {
            pChain->flags &= ~VXB_INTCTLR_FLG_ENABLE;
            pChain = pChain->pNext;
            }
        }

    return(OK);
    }

/*******************************************************************************
*
* sysArmGicDisable - disable an interrupt handler from the vector table
*
* This routine disables interrupt handler from the vector table. It's used
* to support intDisable(...).
*
* This routine is called somewhere by legacy driver.
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS sysArmGicDisable
    (
    int vector
    )
    {
    ARM_GIC_DRV_CTRL *pDrvCtrl = vxbGicId->pDrvCtrl;

    if ((int)(vector) >= pDrvCtrl->gicLinesNum)
        return(ERROR);

    if (sysArmGicISRDisable(&(pDrvCtrl->isrHandle), vector) != OK)
        return (ERROR);

    vxbArmGicLvlDisable(pDrvCtrl->pInst, vector);
    return (OK);
    }

/*******************************************************************************
*
* vxbArmGicDisable - disable device interrupt
*
* This routine disables interrupt for the configured input pin
*
* \NOMANUAL
*
* RETURNS: OK if operation succeed else ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicDisable
    (
    VXB_DEVICE_ID       pIntCtlr,
    VXB_DEVICE_ID       pDev,
    int                 index,
    VOIDFUNCPTR         pIsr,
    void *              pArg
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl = pIntCtlr->pDrvCtrl;
    BOOL allDisabled;
    int inputPin;

    /* get interrupt input pin */

    inputPin = intCtlrPinFind (pDev, index, pIntCtlr, &pDrvCtrl->isrHandle);
    if (inputPin == ERROR)
        return (ERROR);

    /* disable ISR */

    allDisabled = intCtlrISRDisable(&pDrvCtrl->isrHandle, inputPin, pIsr, pArg);

    if (allDisabled)
        vxbArmGicLvlDisable(pDrvCtrl->pInst, inputPin);

    return (OK);
    }

/*******************************************************************************
*
* vxbArmGicLvlVecChk - check for and return any pending interrupts
*
* This routine interrogates the hardware to determine the highest priority
* interrupt pending. It returns the vector associated with that interrupt,
* and also the level of that interrupt.
*
* This routine must be called with CPU interrupts disabled.
*
* The return value ERROR indicates that no pending interrupt was found and
* that the level and vector values were not returned.
*
* RETURNS: OK or ERROR if no interrupt is pending.
*
* ERRNO: N/A
*/

STATUS  vxbArmGicLvlVecChk
    (
    VXB_DEVICE_ID pInst,
    int * pLevel,
    int * pVector
#ifdef _WRS_CONFIG_SMP
    , int * pSrcCpuId
#endif
    )
    {
    UINT32 level;
    UINT32 intNumOrig, intNum;

    /* read pending interrupt register and mask undefined bits */

    intNumOrig = cpuReadIar();
 
    intNum = intNumOrig & GIC_INT_SPURIOUS;

    /*
     * If no interrupt is pending, register will have a value of 1023,
     * return ERROR
     */
     
    if (intNum == GIC_INT_SPURIOUS)
        {
        return ERROR;
        }

    level = cpuReadRpr();
    level &= GIC_INT_PRIORITY_MASK;

#ifdef _WRS_CONFIG_SMP

    /* check if interrupt is IPI */

    if(intNum < SGI_INT_MAX)
        {
        /*
         * Level for IPI is defined and processed by software.
         * Hardware always use 0 - SGI_INT_MAX as the IPI interrupt level.
         * When we find that this is a SGI interrupt, we add armGicLinesNum to
         * the current SGI level (0 - SGI_INT_MAX), in other words, the SGI
         * level is redefined by software to declare that this is a specific
         * interrupt, then we need save the source CPU ID to ack this interrupt.
         */

        *pSrcCpuId = 0;
        *pVector = (int)INUM_TO_IVEC(intNum + armGicLinesNum);
        
        }
    else
#endif /* _WRS_CONFIG_SMP */
        {

        /* fetch, or compute the interrupt vector number */

        *pVector = (int)INUM_TO_IVEC(intNum);
        }

        *pLevel = level;

    return OK;
    }


/*******************************************************************************
*
* vxbArmGicLvlVecAck - acknowledge the current interrupt
*
* This routine acknowledges the current interrupt cycle. The level and vector
* values are those generated during the vxbArmGicLvlVecChk() routine for this
* interrupt cycle.
*
* RETURNS: OK or ERROR if level is invalid.
*
* ERRNO
*/
STATUS  vxbArmGicLvlVecAck
    (
    VXB_DEVICE_ID pInst,
    int level,    /* old interrupt level to be restored */
    int vector    /* current interrupt vector, if needed */
#ifdef _WRS_CONFIG_SMP
    , int srcCpuId
#endif /* _WRS_CONFIG_SMP */
    )
    {
    int intNum;
    UINT32 maxIntLines;

    intNum = IVEC_TO_INUM(vector);

#ifdef _WRS_CONFIG_SMP
    maxIntLines = armGicLinesNum + ARM_GIC_IPI_COUNT;
#else
    maxIntLines = armGicLinesNum;
#endif /* _WRS_CONFIG_SMP */

    /* Validity check for level. */
     
    if (intNum < 0 || intNum >= maxIntLines ||
        (intNum >= SGI_INT_MAX && intNum < PPI_START_INT_NUM))
        return ERROR;

    /*
     * Ack the interrupt. It's implemented on the CPU interface to the
     * interrupt distributor.
     */

#ifdef _WRS_CONFIG_SMP
    if (intNum >= armGicLinesNum)
        {
        /*
         * SGI is used to implement the IPI. The source CPU ID must be carried
         * to acknowledge the SGI interrupts.
         */

        intNum -= armGicLinesNum;

        }
#endif /* _WRS_CONFIG_SMP */

    /*
     * Ack the interrupt. It's implemented on the CPU interface to the
     * interrupt distributor.
     */
     
    cpuWriteEoir1((UINT32)intNum);

    return OK;
    }

/*******************************************************************************
*
* vxbArmGicLvlChg - change the interrupt level value
*
* This routine sets the interrupt priority mask.
* All levels up to the specified level are disabled.
* All levels above the specified level will be enabled.
* This routine must be called with interrupts disabled.
*
* RETURNS: previous interrupt level or ERROR if level is invalid.
*
* ERRNO: N/A
*/

int  vxbArmGicLvlChg
    (
    VXB_DEVICE_ID pInst,
    UINT32 level    /* new interrupt level */
    )
    {
    int oldLevel;

    if (level > 0xff)
        return ERROR;

    oldLevel = (int)(cpuReadPmr() & GIC_INT_PRIORITY_MASK);

    /* change current interrupt level */

    cpuWritePmr(level);
    VX_SYNC_BARRIER();
    return oldLevel;
    }

/*******************************************************************************
*
* vxbArmGicLvlEnable - enable a single interrupt level
*
* This routine enables a specific interrupt level at the interrupt distributor.
* The enabled level will be allowed to generate an interrupt to the configured
* CPU(s). Without being enabled, the interrupt is blocked.
*
* RETURNS: OK or ERROR if level is invalid.
*
* ERRNO: N/A
*/

LOCAL STATUS  vxbArmGicLvlEnable
    (
    VXB_DEVICE_ID pInst,
    int level  /* level to be enabled */
    )
    {
    int key;
    int cpuNum = vxCpuIndexGet();
    UINT32 offset;
    ARM_GIC_DRV_CTRL *  pCtrl = NULL;
    GIC_REDIST *        pRedist;

    pCtrl =  (ARM_GIC_DRV_CTRL *)(pInst->pDrvCtrl);
    if (pCtrl == NULL)
        return (ERROR);

    pRedist = &pCtrl->gicRedist[cpuNum];
    
    GIC_DBG(GIC_DBG_INFO, "%s,%d level=%d\r\n", __FUNCTION__, __LINE__, level);

#ifdef _WRS_CONFIG_SMP

    /*
     * For SGI interrupt, we always return OK, since SGI interrupt can not be
     * disabled.
     */
  if ((level >= 0 && level < SGI_INT_MAX) ||
       (level >= armGicLinesNum&& level < (armGicLinesNum + ARM_GIC_IPI_COUNT)))
        {

            if (level >= armGicLinesNum)
                {
                level -= armGicLinesNum;
                }

            key = intCpuLock ();              /* LOCK INTERRUPTS */

            RDSGI_WRITE_4 (pRedist, GICR_ISENABLER0, BIT32(level)); /* enable interrupt */

            intCpuUnlock (key);               /* UNLOCK INTERRUPTS */

            return OK;
        }

 /* validity check for interrupt number */
                                                            /* req: VX7-16254 */
    if (level < 0 || level >= (armGicLinesNum +  ARM_GIC_IPI_COUNT))
        return ERROR;
#else
    if ((level >= 0) && (level < SGI_INT_MAX))
        return OK;

    /* Validity check for level. */

    if (level < PPI_START_INT_NUM || level >= armGicLinesNum )
        return ERROR;

#endif
    key = intCpuLock ();              /* LOCK INTERRUPTS */
  if  (level < SPI_START_INT_NUM)
        {
        RDSGI_WRITE_4 (pRedist, GICR_ISENABLER0,BIT32(level)); /* PPI/SGI enable */
        }
        else
        {
        offset = (UINT32) ((level / 32) * 4);
        DIST_WRITE_4 (pCtrl, (GICD_ISENABLER + offset),BIT32(level)); /* SPI enable */
        }

    intCpuUnlock (key);               /* UNLOCK INTERRUPTS */

    return OK;
    }

/*******************************************************************************
*
* vxbArmGicLvlDisable - disable a single interrupt level
*
* This routine disables a specific interrupt level at the interrupt distributor.
* The disabled level is prevented from generating an interrupts.
*
* RETURNS: OK or ERROR if level is invalid.
*
* ERRNO: N/A
*/

LOCAL STATUS vxbArmGicLvlDisable
    (
    VXB_DEVICE_ID pInst,
    int level  /* level to be disabled */
    )
    {
    int key;
    int cpuNum = vxCpuIndexGet();
    UINT32 offset;
    ARM_GIC_DRV_CTRL *  pCtrl = NULL;
    GIC_REDIST *        pRedist;

    pCtrl =  (ARM_GIC_DRV_CTRL *)(pInst->pDrvCtrl);
    if (pCtrl == NULL)
        return (ERROR);

    pRedist = &pCtrl->gicRedist[cpuNum];
        
    
    if ((level >= 0 && level < SGI_INT_MAX) ||
       (level >= armGicLinesNum&& level < (armGicLinesNum + ARM_GIC_IPI_COUNT)))
        {

        if (level >= armGicLinesNum)
            {
            level -= armGicLinesNum;
            }

        key = intCpuLock ();              /* LOCK INTERRUPTS */

        RDSGI_WRITE_4 (pRedist, GICR_ICENABLER0, BIT32(level));/* enable interrupt */

        intCpuUnlock (key);               /* UNLOCK INTERRUPTS */
            
        return OK;
        }
    /* Validity check for level. */

#ifdef _WRS_CONFIG_SMP
                                                            /* req: VX7-16258 */
    if (level < PPI_START_INT_NUM|| level >= (armGicLinesNum + ARM_GIC_IPI_COUNT))
        return ERROR;
#else
    if (level < PPI_START_INT_NUM || level >= armGicLinesNum)
        return ERROR;
#endif /* _WRS_CONFIG_SMP */

    key = intCpuLock ();
if  (level < SPI_START_INT_NUM)
        {
        RDSGI_WRITE_4 (pRedist, GICR_ICENABLER0, BIT32(level)); /* PPI/SGI */
        }
    else
        {
        offset =(UINT32) ((level / 32) * 4);
        DIST_WRITE_4 (pCtrl, (GICD_ICENABLER + offset), BIT32(level));/* SPI */
        }

    intCpuUnlock (key);

    return OK;
    }

/******************************************************************************
*
* vxbArmGicHwLvlChg - change the interrupt level value
*
* This routine sets the current interrupt level to the specified level.
*
* \NOMANUAL
*
* RETURNS: Previous interrupt level.
*
* ERRNO: N/A
*/

LOCAL int vxbArmGicHwLvlChg
    (
    int level
    )
    {
    return vxbArmGicLvlChg(pVxbArmGicDrvCtrl->pInst,level);
    }

#ifdef INCLUDE_SHOW_ROUTINES

/*******************************************************************************
*
* vxbArmGicDataShow - show data acquired by vxBus
*
* This routine shows data acquired by vxBus.
*
* RETURNS: N/A
*/

void vxbArmGicDataShow
    (
    VXB_DEVICE_ID  pInst,
    int			   verboseLevel
    )
    {
    ARM_GIC_DRV_CTRL * pDrvCtrl;
    int i;

    /* check if parameters are invalid */

    if ((pInst == NULL) || (pInst->pDrvCtrl == NULL))
        return;

    if (verboseLevel < 10)
        return;
        
    /* retrieve the data */

    pDrvCtrl = (ARM_GIC_DRV_CTRL *)(pInst->pDrvCtrl);

    printf ("\r\n        GIC Configuration Data acquired by vxBus\r\n");

    printf ("          ARM GIC Info :                       \r\n");

    printf ("          pInst              = 0x%08x\r\n", pInst);

    printf ("          gicDist            = 0x%08x\r\n",
                    pDrvCtrl->gicDist);
                    
    printf ("          gicDistHdl         = %d\r\n",
                    pDrvCtrl->gicDistHdl);
                    
    printf ("          rdRgnBase          = 0x%08x\r\n",
                    pDrvCtrl->gicRedistRegion->rdRgnBase);
    for(i=0; i<pDrvCtrl->gicCpuNum; i++)
        {
        printf ("            rdBase [%d]       = 0x%08x\r\n",
                    i, pDrvCtrl->gicRedist[i].rdBase);
                    
        printf ("            sgiBase[%d]       = 0x%08x\r\n",
                    i, pDrvCtrl->gicRedist[i].sgiBase);
        }
       
    printf ("          gicLvlCurrent      = 0x%08x\r\n",
                    pDrvCtrl->gicLvlCurrent);
                    
    printf ("          gicLinesNum        = %d\r\n",
                    pDrvCtrl->gicLinesNum);
                    
    printf ("          gicCpuNum          = %d\r\n",
                    pDrvCtrl->gicCpuNum);
                    
    printf ("          intMode            = %d\r\n",
                    pDrvCtrl->intMode);

    }

#endif /* INCLUDE_SHOW_ROUTINES */




