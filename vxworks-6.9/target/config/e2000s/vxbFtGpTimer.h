/* vxbFtGpTimer.h - Phytium General Purpose Timer driver for VxBus */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbFtGpTimerh
#define __INCvxbFtGpTimerh

#ifdef __cplusplus
extern "C" {
#endif
                    
#define TIMER_CTRL_REG  0x00
#define     TIMER_CTRL_MODE_SHIFT       0 /* 0:1 */
#define     TIMER_CTRL_RESET_SHIFT      BIT(2)
#define     TIMER_CTRL_FORCE_SHIFT      BIT(3)
#define     TIMER_CTRL_CAPTURE_EN_SHIFT BIT(4)
#define     TIMER_CTRL_CAPTURE_CNT_SHIFT    5 /* 5:11 */
#define     TIMER_CTRL_ANTI_JITTER_SHIFT    18 /* 18:19 */
#define     TIMER_CTRL_TACHO_MODE_SHIFT 20 /* 20:21 */
#define     TIMER_CTRL_TIMER_CNT_MODE_SHIFT BIT(22)
#define     TIMER_CTRL_BIT_SET_SHIFT    BIT(24)
#define     TIMER_CTRL_CNT_EN_SHIFT     BIT(25)
#define     TIMER_CTRL_CNT_CLR_SHIFT    BIT(26)
#define     TIMER_CTRL_TIMER_MODE_SHIFT BIT(27)
#define     TIMER_CTRL_PULSE_NUM_SHIFT  28 /* 28:30 */
#define     TIMER_CTRL_TACHO_EN_SHIFT   BIT(31)

/* timer ctrl */
#define     TIMER_REG_TACHO_MODE_MASK         (0x3 << 0) /* bit [1:0] RW */
#define     TIMER_REG_TACHO_MODE_TIMER        (0x0 << 0)
#define     TIMER_REG_TACHO_MODE_TACHO        (0x1 << 0)
#define     TIMER_REG_TACHO_MODE_CAPTURE      (0x2 << 0)
#define     TIMER_REG_TACHO_FORCE_LOAD        (0x1 << 3) /* force to update */
#define     TIMER_REG_TACHO_CAPTURE_ENABLE    (0x1 << 4) /* enable input capture */
#define     TIMER_REG_TACHO_CAPTURE_CNT       (0x7f << 5) /* in capture mode, cause intr when egde counting reach this val */
#define     TIMER_REG_TACHO_CAPTURE_CNT_SHIFT 5

#define     TACHO_REG_ANTI_JITTER_MASK        (0x3 << 18) /* anti jitter num = N + 1 */
#define     TACHO_REG_ANTI_JITTER_SHIFT       18

#define     TACHO_REG_MODE_MASK               (0x3 << 20) /* select tacho input mode  */
#define     TACHO_REG_MODE_FALLING_EDGE       (0x0 << 20)
#define     TACHO_REG_MODE_RISING_EDGE        (0x1 << 20)
#define     TACHO_REG_MODE_DOUBLE_EDGE        (0x2 << 20)

#define     TIMER_REG_CNT_SERIES_64BIT         (0x1 << 24) 
#define     TIMER_REG_CNT_SERIES_32BIT         (0x0 << 24)

/* tacho result */
#define     TACHO_REG_RESU_MASK                GENMASK(30, 0) /* bit [30:0], tacho result */
#define     TACHO_REG_RESU_ISVALID             (0x1 << 31) /* tacho result is valid */



#define TIMER_TACHO_RES_REG 0x04 /* tacho result value */
#define     TIMER_TACHO_RES_VALID_SHIFT BIT(31)
#define     TIMER_TACHO_RES_MASK    GENMASK(30, 0)

#define TIMER_CMP_VALUE_UP_REG  0x08 /* compare value: up-32-bit */
#define TIMER_CMP_VALUE_LOW_REG 0x1C /* compare value: low-32-bit */
#define TIMER_CNT_VALUE_UP_REG  0x20 /* count value: up-32-bit */
#define TIMER_CNT_VALUE_LOW_REG 0x24 /* count value: low-32-bit */
#define TIMER_INT_MASK_REG  0x28 /* interrupt mask */

#define TIMER_INT_STAT_REG  0x2C /* interrupt status */
#define     TIMER_INT_CAPTURE_SHIFT     BIT(5)
#define     TIMER_INT_CYC_COMP_SHIFT    BIT(4)
#define     TIMER_INT_ONE_COMP_SHIFT    BIT(3)
#define     TIMER_INT_ROLLOVER_SHIFT    BIT(2)
#define     TIMER_INT_TACHO_UNDER_SHIFT BIT(1)
#define     TIMER_INT_TACHO_OVER_SHIFT  BIT(0)

#define TIMER_TACHO_OVER_REG    0x30
#define TIMER_TACHO_UNDER_REG   0x34
#define TIMER_START_VALUE_REG   0x38

#define TIMER_TACHO_DEFAULT_MAX   10000
#define TIMER_TACHO_DEFAULT_MIN   10

#define TIMER_INT_CLR_MASK  GENMASK(5, 0)

typedef enum
{
    /* timer mode */
    TIMER_WORK_MODE_TIMER = 0,
    TIMER_WORK_MODE_TACHO,
    TIMER_WORK_MODE_CAPTURE
}TimerTachoModeType;

typedef enum
{
    TIMER_FREE_RUN = 0,
    TIMER_RESTART
}TimerRunModeType;

typedef enum
{
    TIMER_32_BITS = 0,
    TIMER_64_BITS
}TimerBitsType;

typedef enum
{
    TIMER_ONCE_CMP = 0,
    TIMER_CYC_CMP
}TimerCmpType;

typedef enum
{
    TACHO_EVENT_OVER = 0,
    TACHO_EVENT_UNDER,
    TIMER_EVENT_ROLL_OVER,
    TIMER_EVENT_ONCE_CMP,
    TIMER_EVENT_CYC_CMP,
    TACHO_EVENT_CAPTURE,
    
    MAX_TIMER_TACHO_EVENT
}TimerTachoEventType;

typedef enum
{
    TACHO_FALLING_EDGE = 0,
    TACHO_RISING_EDGE,
    TACHO_DOUBLE_EDGE
}TachoEdgeType;


#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbFtGpTimerh */
