/* 40vxbFtPcie.cdf - Phytium PCIe controller configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   DRV_PCIBUS_FT {
   NAME        Phytium PCIe controller driver
    SYNOPSIS    Phytium PCIe controller driver
    _CHILDREN   FOLDER_DRIVERS
    CONFIGLETTES 
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    ftPciRegister();
    PROTOTYPE   void ftPciRegister (void);
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS \
                INCLUDE_PCI_BUS \
                INCLUDE_PCI_BUS_AUTOCONF \
                INCLUDE_PCI_BUS_SHOW \
                INCLUDE_PCI_OLD_CONFIG_ROUTINES
				
    INIT_AFTER  INCLUDE_PLB_BUS

}

