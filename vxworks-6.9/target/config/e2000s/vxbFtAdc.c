/* vxbFtAdc.c - ADC driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <msgQLib.h>
#include <intLib.h>
#include <cacheLib.h>
#include <drv/timer/timerDev.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/times.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/util/hwMemLib.h>
#include <drv/pci/pciConfigLib.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/util/vxbParamSys.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include <iosLib.h>
#include "vxbFtAdc.h"
#include "defs.h"
#include "ftPinMux.h"

#define  FTADC_DEBUG
#ifdef FTADC_DEBUG
#define FTADC_LOGMSG(fmt,p1,p2,p3,p4,p5,p6) logMsg(fmt,p1,p2,p3,p4,p5,p6)
#else
#define FTADC_LOGMSG(fmt,p1,p2,p3,p4,p5,p6)
#endif

/* VxBus methods */

LOCAL void  ftAdcInstInit (VXB_DEVICE_ID);
LOCAL void  ftAdcInstInit2 (VXB_DEVICE_ID);
LOCAL void  ftAdcInstConnect (VXB_DEVICE_ID);
LOCAL void  ftAdcInt (FTADC_DRV_CTRL *pDrvCtrl);
LOCAL void  ftAdcRecvTask(FTADC_DRV_CTRL * pDrvCtrl);
LOCAL void  ftAdcInit (VXB_DEVICE_ID pDev);
LOCAL int ftAdcOpen(DEV_HDR * hdr, char * name, int flag, int mode);
LOCAL int ftAdcClose(FTADC_IO_DEV * pFtAdcIoDev);
LOCAL int ftAdcRead(FTADC_IO_DEV * pFtAdcIoDev, char * buffer, int num);
LOCAL STATUS ftAdcIoctl(FTADC_IO_DEV * pFtAdcIoDev,int command,int arg);
LOCAL void ftAdcEnableChannels(FTADC_DRV_CTRL * pFtAdcCtrl);
LOCAL void ftAdcDisableChannels(FTADC_DRV_CTRL * pFtAdcCtrl);
/* END functions */

LOCAL struct drvBusFuncs ftAdcFuncs =
    {
        ftAdcInstInit,  /* devInstanceInit */
        ftAdcInstInit2, /* devInstanceInit2 */
        ftAdcInstConnect    /* devConnect */
    };

LOCAL device_method_t ftAdc_methods[] =
    {
    { 0, 0}
    };

LOCAL struct vxbDevRegInfo ftAdcDevPlbRegistration =
    {
      
        NULL,           /* pNext */
        VXB_DEVID_DEVICE,   /* devID */
        VXB_BUSID_PLB,      /* busID = PLB */
        VXB_VER_4_0_0,      /* vxbVersion */
        "ftAdc",        /* drvName */
        &ftAdcFuncs,        /* pDrvBusFuncs */
        NULL,   /* pMethods */
        NULL,           /* devProbe */
        NULL    /* pParamDefaults */
    };

/*****************************************************************************
*
*vxbFtAdcRegister - register with the VxBus subsystem
*
* This routine registers the Template driver with VxBus as a
* child of the PCI bus type.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFtAdcRegister(void)
    {
    vxbDevRegister((struct vxbDevRegInfo *)&ftAdcDevPlbRegistration);
    return;
    }

/*****************************************************************************
*
* ftAdcInstInit - VxBus instInit handler
*
* This function implements the VxBus instInit handler for an template
* device instance. The only thing done here is to select a unit
* number for the device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftAdcInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
       FTADC_DRV_CTRL * pDrvCtrl;
       HCF_DEVICE * pHcf;
    
       /* get the HCF device from the instance ID */
    
       pHcf = hcfDeviceGet (pDev);
    
       /* if pHcf is NULL, no device is present in hwconf.c */
    
       if (pHcf == NULL)
           return;
    
       /* allocate memory for the data */
    
       pDrvCtrl = (FTADC_DRV_CTRL *)hwMemAlloc (sizeof(FTADC_DRV_CTRL));
    
       if (pDrvCtrl == NULL)
           return;

       bzero((char *)pDrvCtrl, sizeof(FTADC_DRV_CTRL));
       
       pDrvCtrl->ftAdcDev = pDev;
       if (devResourceGet (pHcf, "regBase", HCF_RES_INT,
                        (void *)&pDrvCtrl->ftAdcRegbase) != OK)
        {
#ifndef _VXBUS_BASIC_HWMEMLIB
        hwMemFree((char *)pDrvCtrl);
#endif
        return;
        }

      pDev->pDrvCtrl = pDrvCtrl;    
      vxbRegMap(pDev, 0, &pDrvCtrl->ftAdcHandle);
    return;
    }

/*****************************************************************************
*
* ftAdcInstInit2 - VxBus instInit2 handler
*
* This function implements the VxBus instInit2 handler for an template
* device instance. Once we reach this stage of initialization, it's
* safe for us to allocate memory, so we Adc create our pDrvCtrl
* structure and do some initial hardware setup. The important
* steps we do here are to create a child miiBus instance, connect
* our ISR to our assigned interrupt vector, read the station
* address from the EEPROM, and set up our vxbDma tags and memory
* regions. We need to allocate a 64K region for the RX DMA window
* here.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftAdcInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    if(pDev->unitNumber == 0)
        {
        pinAdc0Chan0();
        pinAdc0Chan1();
        pinAdc0Chan2();
        pinAdc0Chan3();
        pinAdc0Chan4();
        pinAdc0Chan5();
        pinAdc0Chan6();
        pinAdc0Chan7();
        }
    else if(pDev->unitNumber == 1)
        {
        pinAdc1Chan0();
        pinAdc1Chan1();
        pinAdc1Chan2();
        pinAdc1Chan3();
        pinAdc1Chan4();
        pinAdc1Chan5();
        pinAdc1Chan6();
        pinAdc1Chan7();
        }   
    }

/*****************************************************************************
*
* ftAdcInstConnect -  VxBus instConnect handler
*
* This function implements the VxBus instConnect handler for an template
* device instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftAdcInstConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
    int st;
    FTADC_DRV_CTRL * pFtAdcCtrl;
    FTADC_IO_DEV*    pFtAdcIoDev;
    char task_name[32];
    char iosDevName [32];
    int iosDrvNum;
    TASK_ID tid;
    SEM_ID semId;
    

    pFtAdcCtrl = pDev->pDrvCtrl;
    pFtAdcIoDev = &pFtAdcCtrl->ftAdcIoDev;
    
    iosDrvNum = iosDrvInstall
                (
                (DRV_CREATE_PTR)  NULL,
                (DRV_REMOVE_PTR)  NULL,
                (DRV_OPEN_PTR)    ftAdcOpen,
                (DRV_CLOSE_PTR)   ftAdcClose,
                (DRV_READ_PTR)    ftAdcRead,
                (DRV_WRITE_PTR)   NULL,
                (DRV_IOCTL_PTR)   ftAdcIoctl
                );
    if (ERROR == iosDrvNum)
        {
        FTADC_LOGMSG ("Install IO dirver failed.\n",1,2,3,4,5,6);
        return ;
        }
    pFtAdcIoDev->devIoDrvNum = iosDrvNum;
    
    (void) snprintf (iosDevName, 32, "/adc%d", pDev->unitNumber);
    if (ERROR == iosDevAdd (&(pFtAdcIoDev->devHdr), iosDevName, iosDrvNum))
        {
        FTADC_LOGMSG ("Ios device add failed.\n",1,2,3,4,5,6);
        return ;
        }

    semId = semBCreate (SEM_Q_FIFO, SEM_FULL);
    if (semId == SEM_ID_NULL)
        {
        FTADC_LOGMSG ("Create access semaphore failed.\n",1,2,3,4,5,6);
        return ;
        }
    pFtAdcCtrl->accessSemId = semId;
    
    semId = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
    if (SEM_ID_NULL == semId)
        {
        FTADC_LOGMSG ("Create sample semaphore failed\n",1,2,3,4,5,6);
        (void) semDelete (pFtAdcCtrl->accessSemId);
        return ;
        }
    pFtAdcCtrl->sampleSemId = semId;
    
    semId = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
    if (SEM_ID_NULL == semId)
        {
        FTADC_LOGMSG ("Create read semaphore failed\n",1,2,3,4,5,6);
        (void) semDelete (pFtAdcCtrl->accessSemId);
        (void) semDelete (pFtAdcCtrl->sampleSemId);
        return ;
        }
    pFtAdcCtrl->rdTskSemId = semId;
    
    
    bzero(task_name, sizeof(task_name));
    sprintf(task_name, "tAdcRx%d", pDev->unitNumber);
    tid = taskSpawn(task_name, 100, 0, 0x20000,
                    (FUNCPTR) ftAdcRecvTask,(_Vx_usr_arg_t) pFtAdcCtrl,
                    2,3,4,5,6,7,8,9,10);
    if(TASK_ID_ERROR == tid)
    {
        FTADC_LOGMSG("failed to spawn Adc recv task!\n",1,2,3,4,5,6);
        return;
    }
    
    pFtAdcCtrl->tid = tid; /* save this task id */
    
     /*
       * Attach our ISR. For PCI, the index value is always
       * 0, since the PCI bus controller dynamically sets
       * up interrupts for us.
       */
    st = vxbIntConnect (pDev, 0, ftAdcInt, pFtAdcCtrl);
    st |= vxbIntEnable (pDev, 0, ftAdcInt, pFtAdcCtrl);

    if(st != OK)
    {
        FTADC_LOGMSG("interrupt cn/en failed!\n",1,2,3,4,5,6);
        return;
    }
    
    return;
   }

/*****************************************************************************
*
* ftAdcInit -  Initialize ADC controller
*
* This function initialize ADC controller
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
void ftAdcInit (VXB_DEVICE_ID pDev)
{
    UINT32 cfg;

    cfg = ADC_CTRL_REG_CLK_DIV(4);
    cfg &= ~(ADC_CTRL_REG_PD_EN);
    cfg |= ADC_CTRL_REG_SINGLE_SEL;
    CSR_WRITE_4(pDev, ADC_CTRL_REG, cfg);
    
    CSR_WRITE_4(pDev, ADC_INTRMASK_REG, 0x1ffffff);/* disable all interrupt */

    return;
}   

/*****************************************************************************
*
* ftAdcOpen -  Open method of the IO device driver
*
* This function is called by IO system to open the device.
*
* RETURNS: The device data pointer cast into int, ERROR if failed to open.
*
* ERRNO: N/A
*/
LOCAL int  ftAdcOpen
    (   
    DEV_HDR * hdr,      /* pointer to the DEV_HDR struct */
    char    * name,     /* device name */
    int     flag,       /* open flag */
    int     mode        /* open mode */
    )
    {
    FTADC_DRV_CTRL * pFtAdcCtrl =  (FTADC_DRV_CTRL *) hdr;

    /* no shared access is supported */

    if (ERROR == semTake (pFtAdcCtrl->accessSemId, NO_WAIT))
        {
        FTADC_LOGMSG ("ADC access denied\n",1,2,3,4,5,6);
        return ERROR;
        }
    
    ftAdcInit(pFtAdcCtrl->ftAdcDev);
    
    return (UINT32)hdr;
    }

/*****************************************************************************
*
* ftAdcClose -  Close method of the IO device driver
*
* This function is called by IO system to close the device.
*
* RETURNS: Always returns OK.
*
* ERRNO: N/A
*/
LOCAL int ftAdcClose(FTADC_IO_DEV * pFtAdcIoDev)
    {
    FTADC_DRV_CTRL * pFtAdcCtrl = (FTADC_DRV_CTRL *)pFtAdcIoDev;

    /* give up access right */
    (void) semGive (pFtAdcCtrl->accessSemId);
    
    return OK;
    }

/*****************************************************************************
*
* ftAdcIoctl -  Ioctl method of the IO device driver
*
* This function is called by IO system to set the device.
*
* RETURNS: Always returns OK.
*
* ERRNO: N/A
*/
LOCAL STATUS ftAdcIoctl
(
 FTADC_IO_DEV * pFtAdcIoDev,      /* pointer to DevIO file descriptor */
 int            command,     /* command function code */
 int            arg          /* arbitrary argument */
 )
{
    FTADC_DRV_CTRL * pFtAdcCtrl = (FTADC_DRV_CTRL *)pFtAdcIoDev;
    VXB_DEVICE_ID pDev = pFtAdcCtrl->ftAdcDev;
    UINT8 chanNo, channelMask;

    switch (command)
    {
    case ADC_IOCTL_MODE:
        pFtAdcCtrl->mode = arg;
        break;
    case ADC_IOCTL_CHAN_MASK:
        pFtAdcCtrl->activeChanNum = 0;
        channelMask = arg;
        for(chanNo = 0; chanNo < ADC_CHANNEL_NUM; chanNo++)
        {
            if(channelMask & BIT(chanNo))
            {
                pFtAdcCtrl->activeChanNo[pFtAdcCtrl->activeChanNum++] = chanNo;
            }
        }
        break;  
    case ADC_IOCTL_NUMBER_OF_CONV:
        pFtAdcCtrl->numberOfConv = arg;
        break;
    case ADC_IOCTL_START:
        ftAdcEnableChannels(pFtAdcCtrl);
        break;     
    case ADC_IOCTL_STOP:
        ftAdcDisableChannels(pFtAdcCtrl);
        break;  
    case ADC_IOCTL_INTERVAL:
        CSR_WRITE_4(pDev, ADC_INTER_REG, arg);
        break; 
    case ADC_IOCTL_LEVEL:
        {
        ADC_LEVEL_REG_CFG cfg;
        UINT32 intrmask;
        cfg.data = arg;
        CSR_WRITE_4(pDev, ADC_LEVEL_REG(cfg.adcLevel.chanNo), 
                    ADC_LEVEL_REG_HIGH_LEVEL(cfg.adcLevel.highLevel) | ADC_LEVEL_REG_LOW_LEVEL(cfg.adcLevel.lowLevel));
        intrmask = CSR_READ_4(pDev, ADC_INTRMASK_REG);
        intrmask &= ~(ADC_INTRMASK_REG_ULIMIT_MASK(cfg.adcLevel.chanNo));
        intrmask &= ~(ADC_INTRMASK_REG_DLIMIT_MASK(cfg.adcLevel.chanNo));
        CSR_WRITE_4(pDev, ADC_INTRMASK_REG, intrmask);
        break;
        }
    case ADC_IOCTL_RECV_RTN:
        {
        ADC_RECV_RTN_CFG cfg;
        cfg.data = arg;
        pFtAdcCtrl->recvProcRtn = cfg.recvProcRtn;
        break; 
        }
    default:
        break;
    }
    
    return OK;
}

/*****************************************************************************
*
* ftAdcEnableChannels -  Enable adc channels
*
* This function enable adc channels.
*
* RETURNS: Always returns OK.
*
* ERRNO: N/A
*/
LOCAL void ftAdcEnableChannels(FTADC_DRV_CTRL * pFtAdcCtrl)
{
    VXB_DEVICE_ID pDev = pFtAdcCtrl->ftAdcDev;
    UINT32 cfg, intrmask, index, chanNo;
    
    cfg = CSR_READ_4(pDev, ADC_CTRL_REG);
    intrmask = CSR_READ_4(pDev, ADC_INTRMASK_REG);
    intrmask &= ~(ADC_INTRMASK_REG_ERR_INTR_MASK);
    if (pFtAdcCtrl->activeChanNum > 1) 
        {
        for(index = 0; index < pFtAdcCtrl->activeChanNum; index++)
            {
            chanNo = pFtAdcCtrl->activeChanNo[index];
            if(pFtAdcCtrl->mode == ADC_FIXED_NUMBER_OF_CONVERT_MODE)
                {
                pFtAdcCtrl->pChanBuf[chanNo] = (UINT16 *)malloc(sizeof(UINT16) * pFtAdcCtrl->numberOfConv);
                pFtAdcCtrl->pChanConvCnt[chanNo] = 0;
                }
            intrmask &= ~(ADC_INTRMASK_REG_COVFIN_MASK(chanNo));
            cfg |= ADC_CTRL_REG_CHANNEL_EN(chanNo);
            }
        cfg &= ~ADC_CTRL_REG_CH_ONLY_EN;
        }
    else
        {
        chanNo = pFtAdcCtrl->activeChanNo[0];
        if(pFtAdcCtrl->mode == ADC_FIXED_NUMBER_OF_CONVERT_MODE)
            {
            pFtAdcCtrl->pChanBuf[chanNo] = (UINT16 *)malloc(sizeof(UINT16) * pFtAdcCtrl->numberOfConv);
            pFtAdcCtrl->pChanConvCnt[chanNo] = 0;
            }
        intrmask &= ~(ADC_INTRMASK_REG_COVFIN_MASK(chanNo));
        cfg |= ADC_CTRL_REG_CH_ONLY_EN;
        cfg |= ADC_CTRL_REG_CH_ONLY_S(chanNo);   
        }

    CSR_WRITE_4(pDev, ADC_CTRL_REG, cfg);
    CSR_WRITE_4(pDev, ADC_INTRMASK_REG, intrmask);
    
    if ((pFtAdcCtrl->numberOfConv == 1) && (pFtAdcCtrl->mode == ADC_FIXED_NUMBER_OF_CONVERT_MODE))
        {
        cfg |= ADC_CTRL_REG_SINGLE_SEL;
        cfg |= ADC_CTRL_REG_SOC_EN | ADC_CTRL_REG_SINGLE_EN;
        }
    else
        {
        cfg &= (~ADC_CTRL_REG_SINGLE_SEL);
        cfg |= ADC_CTRL_REG_SOC_EN;    
        }
   
    CSR_WRITE_4(pDev, ADC_CTRL_REG, cfg);
}

/*****************************************************************************
*
* ftAdcDisableChannels -  Disable adc channels
*
* This function disable adc channels.
*
* RETURNS: Always returns OK.
*
* ERRNO: N/A
*/
LOCAL void ftAdcDisableChannels(FTADC_DRV_CTRL * pFtAdcCtrl)
    {
    VXB_DEVICE_ID pDev = pFtAdcCtrl->ftAdcDev;
    UINT32 reg_val;

    reg_val = CSR_READ_4(pDev, ADC_CTRL_REG);
    reg_val &= (~ADC_CTRL_REG_SOC_EN);
    CSR_WRITE_4(pDev, ADC_CTRL_REG, reg_val);
    }

/*****************************************************************************
*
* ftAdcRead -  read method of the IO device driver
*
* This function is called by IO system to read the device.
*
* RETURNS: Number of bytes actually read.
*
* ERRNO: N/A
*/

LOCAL int ftAdcRead(FTADC_IO_DEV * pFtAdcIoDev, char * buffer, int num )
    {
    FTADC_DRV_CTRL * pFtAdcCtrl = (FTADC_DRV_CTRL *)pFtAdcIoDev;

    UINT8 index = 0, chanNo = 0;

    ftAdcEnableChannels(pFtAdcCtrl);  
    
    semTake(pFtAdcCtrl->sampleSemId, WAIT_FOREVER);
    
    for(index = 0; index < pFtAdcCtrl->activeChanNum; index++)
    {
        chanNo = pFtAdcCtrl->activeChanNo[index];
        memcpy(buffer + index * (sizeof(UINT16) * pFtAdcCtrl->numberOfConv), 
                pFtAdcCtrl->pChanBuf[chanNo], 
                sizeof(UINT16) * pFtAdcCtrl->numberOfConv);
        free(pFtAdcCtrl->pChanBuf[chanNo]);
        pFtAdcCtrl->pChanConvCnt[chanNo] = 0;
    }
    
    ftAdcDisableChannels(pFtAdcCtrl);  

    return index * (sizeof(UINT16) * pFtAdcCtrl->numberOfConv);
    }

/*****************************************************************************
*
* ftAdcInt - VxBus interrupt handler
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void  ftAdcInt (FTADC_DRV_CTRL*  pDrvCtrl)
{
    VXB_DEVICE_ID pDev;
    UINT32 status,intrmask;
    UINT8 index,chanNo;
    pDev = pDrvCtrl->ftAdcDev;

    status = CSR_READ_4(pDev,ADC_INTR_REG);
    intrmask = CSR_READ_4(pDev, ADC_INTRMASK_REG);
    
    if ((status) & ADC_INTR_REG_ERR) 
        {    
        FTADC_LOGMSG ("ADC got conversion time out interrupt: 0x%08x\n",status,2,3,4,5,6);
        CSR_WRITE_4(pDev, ADC_INTR_REG, ADC_INTR_REG_ERR);    
        CSR_WRITE_4(pDev, ADC_ERRCLR_REG, 1);
        } 
    else if ((status) & 0xffffff) 
        {
        for (index = 0; index < pDrvCtrl->activeChanNum; index++) 
            {
            chanNo = pDrvCtrl->activeChanNo[index];
            
            if ((status) & ADC_INTR_REG_COVFIN(chanNo)) 
                {
                pDrvCtrl->rawVal[chanNo] = CSR_READ_4(pDev, ADC_COV_RESULT_REG(chanNo));
                
                if(pDrvCtrl->mode == ADC_FIXED_NUMBER_OF_CONVERT_MODE)
                    {
                    if(pDrvCtrl->pChanConvCnt[chanNo] < pDrvCtrl->numberOfConv)
                        {
                        pDrvCtrl->pChanBuf[chanNo][pDrvCtrl->pChanConvCnt[chanNo]] = pDrvCtrl->rawVal[chanNo];
                        pDrvCtrl->pChanConvCnt[chanNo]++;
                        }
                    }

                {
                (void)vxAtomicOr (&(pDrvCtrl->adcEvents), ADC_INTR_REG_COVFIN(chanNo));
                }
                CSR_WRITE_4(pDev, ADC_INTR_REG,  ADC_INTR_REG_COVFIN(chanNo));
                }
            
            if ((status) & ADC_INTR_REG_DLIMIT(chanNo)) 
                {
                (void)vxAtomicOr (&(pDrvCtrl->adcEvents), ADC_INTR_REG_DLIMIT(chanNo));
                CSR_WRITE_4(pDev, ADC_INTR_REG,  ADC_INTR_REG_DLIMIT(chanNo));
                }
                
            if ((status) & ADC_INTR_REG_ULIMIT(chanNo)) 
                {
                (void)vxAtomicOr (&(pDrvCtrl->adcEvents), ADC_INTR_REG_ULIMIT(chanNo));
                CSR_WRITE_4(pDev, ADC_INTR_REG,  ADC_INTR_REG_ULIMIT(chanNo));
                }
            }
        }

    if (pDrvCtrl->adcEvents != 0)
        {
        (void)semGive (pDrvCtrl->rdTskSemId);
        }
    return;
}   
/*****************************************************************************
*
* ftAdcRecvTask -  Receiving work
*
* This function receives data
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftAdcRecvTask
    (
    FTADC_DRV_CTRL * pDrvCtrl
    )
    {
    atomicVal_t  events;
    int channel, index;    
    
    while (TRUE)
    {   
        (void)semTake (pDrvCtrl->rdTskSemId, WAIT_FOREVER);
        events = vxAtomicSet (&(pDrvCtrl->adcEvents), 0);
        
        
         if (events != 0)
             {
             for (channel = 0; channel < ADC_CHANNEL_NUM; channel++)
                 {
                 if (events & ADC_INTR_REG_COVFIN(channel))
                     {
                     if(pDrvCtrl->mode == ADC_CONTINUOUS_CONVERT_MODE)
                        {
                         pDrvCtrl->recvProcRtn(channel, pDrvCtrl->rawVal[channel]);
                        }
                     }
                 if (events & ADC_INTR_REG_ULIMIT(channel))
                     {
                     printf("channel:%d, High Level Limit Alarm\r\n",channel);
                     }
                 if(events & ADC_INTR_REG_DLIMIT(channel))
                     {
                     printf("channel:%d, Low Level Limit Alarm\r\n",channel);
                     } 
                 }
                         
             if(pDrvCtrl->mode == ADC_FIXED_NUMBER_OF_CONVERT_MODE)
                {
                for (index = 0; index < pDrvCtrl->activeChanNum; index++) 
                    {
                    channel = pDrvCtrl->activeChanNo[index];
                    if(pDrvCtrl->pChanConvCnt[channel] < pDrvCtrl->numberOfConv)
                        {
                        break;
                        }
                    }
                if(index == pDrvCtrl->activeChanNum )
                    {
                    (void)semGive (pDrvCtrl->sampleSemId);
                    }
                }
             }
    }
    return ;       
    }   



#define ADC_DBG
#ifdef ADC_DBG
/*****************************************************************************
*
* ftAdcTestFixedNumberOfConv - Test the fixed number of convert mode
*
* This function test the fixed number of convert mode
* 
* PARAMETER:
*           channelMask: bit0~7 represent channel0~7 respectively
*           numberOfConv: the number of conversions 
*           convInterval: The interval between two conversions.
*                         The unit is the cycle time of controller clock(48MHz), 
*                         that is, 20.83ns.                          
* 
* RETURNS: N/A
*
* ERRNO: N/A
*/
void ftAdcTestFixedNumberOfConv(int channelMask, int numberOfConv, int convInterval)
{
    int dev;
    UINT8 chanNo, activeChanNum = 0;
    UINT8 activeChanNo[ADC_CHANNEL_NUM] = {0};
    UINT16 * samples;
    int i,j;
    
    dev = open ("/adc0", O_RDONLY, 0);
    ioctl(dev, ADC_IOCTL_MODE, ADC_FIXED_NUMBER_OF_CONVERT_MODE);
    ioctl(dev, ADC_IOCTL_CHAN_MASK, channelMask);
    ioctl(dev, ADC_IOCTL_NUMBER_OF_CONV, numberOfConv);
    ioctl(dev, ADC_IOCTL_INTERVAL, convInterval);/*The interval between two conversions. 
                                                   The unit is the cycle time of controller clock(48MHz), 
                                                   that is, 20.83ns*/
    
    for(chanNo = 0; chanNo < ADC_CHANNEL_NUM; chanNo++)
    {
        if(channelMask & BIT(chanNo))
        {
            activeChanNo[activeChanNum++] = chanNo;
            printf("chanNo = %d\r\n",chanNo);
        }
    }
    
    samples = malloc(sizeof (UINT16) * activeChanNum * numberOfConv);
    
    read (dev, (char *)&samples[0], sizeof (UINT16) * activeChanNum * numberOfConv);
    
    for(i = 0; i < activeChanNum; i++)
    {
        printf("channel%d�� ",activeChanNo[i]);
        for(j = 0; j < numberOfConv; j++)
        {
            printf("No.%d:%d  ",j,*(samples + i * numberOfConv + j));
        }
        printf("\n ");
    }

    free(samples);
    close(dev);
}

/*****************************************************************************
*
* ftAdcTestContinuousConvUserRecvProcess - test the continuous convert mode(recv process)
*
* This function is user's receive processing function of continuous convert mode. 
* 
* RETURNS: N/A 
*
* ERRNO: N/A
*/
void ftAdcTestContinuousConvUserRecvProcess(int channel, UINT16 value)
{
    printf("channel:%d, val:%x\r\n",channel, value);
    return;
}

/*****************************************************************************
*
* ftAdcTestContinuousConvStart - test the continuous convert mode(start)
*
* This function is to start continuous convert mode. 
* ftAdcTestContinuousConvStop can stop the convert.
* 
* PARAMETER:
*           channelMask: bit0~7 represent channel0~7 respectively
*           convInterval: The interval between two conversions.
*                         The unit is the cycle time of controller clock(48MHz), 
*                         that is, 20.83ns
*           recvProcRtn: :The pointer of receive processing function
* RETURNS: The ADC device 
*
* ERRNO: N/A
*/

int ftAdcTestContinuousConvStart(int channelMask,int convInterval)
{
    int dev;
    ADC_RECV_RTN_CFG cfg;
    
    dev = open ("/adc0", O_RDONLY, 0);
    ioctl(dev, ADC_IOCTL_MODE, ADC_CONTINUOUS_CONVERT_MODE);
    ioctl(dev, ADC_IOCTL_CHAN_MASK, channelMask);
    ioctl(dev, ADC_IOCTL_INTERVAL, convInterval);/*The interval between two conversions. 
                                                   The unit is the cycle time of controller clock(48MHz), 
                                                   that is, 20.83ns*/
    cfg.recvProcRtn = ftAdcTestContinuousConvUserRecvProcess;
    ioctl(dev, ADC_IOCTL_RECV_RTN, cfg.data);
    
    ioctl(dev, ADC_IOCTL_START, 0);
    
    return dev;
}

/*****************************************************************************
*
* ftAdcTestContinuousConvStop - test the continuous convert mode(stop)
*
* This function is to stop continuous convert mode. 
* 
* 
* PARAMETER: 
*           dev: The return value of ftAdcTestContinuousConvStart
* 
* RETURNS: N/A
*
* ERRNO: N/A
*/
void ftAdcTestContinuousConvStop(int dev)
{
    ioctl(dev, ADC_IOCTL_STOP, 0);
    close(dev);
}

/*****************************************************************************
*
* ftAdcTestLevel - test the high and low level limit
*
* This function is to test the high level limit and low level limit. 
* ftAdcTestContinuousConvStop can stop the convert.
* 
* PARAMETER:
*           chanNo: channel mumber
*           highLevel: high level limit
*           lowLevel: low level limit
* 
* RETURNS: The ADC device 
*
* ERRNO: N/A
*/
int ftAdcTestLevel(int chanNo, int highLevel, int lowLevel)
{
    int dev;
    ADC_LEVEL_REG_CFG cfg;
    
    dev = open ("/adc0", O_RDONLY, 0);
    ioctl(dev, ADC_IOCTL_MODE, ADC_CONTINUOUS_CONVERT_MODE);
    ioctl(dev, ADC_IOCTL_CHAN_MASK, BIT(chanNo));
    ioctl(dev, ADC_IOCTL_INTERVAL, 50000000);
    cfg.adcLevel.chanNo = chanNo;
    cfg.adcLevel.highLevel = highLevel;
    cfg.adcLevel.lowLevel = lowLevel;
    ioctl(dev, ADC_IOCTL_LEVEL, cfg.data);
    
    ioctl(dev, ADC_IOCTL_START, 0);
    
    return dev;
}

#endif
