/* 40vxbArmGenTimer.cdf - ARM general timer configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   DRV_ARM_GEN_SYS_TIMER {
    NAME        ARM General Timer Driver
    SYNOPSIS    ARM General Timer Driver
    _CHILDREN   FOLDER_DRIVERS
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    armGenTimerRegister();
    PROTOTYPE   void armGenTimerRegister (void);
    REQUIRES    INCLUDE_PLB_BUS \
                INCLUDE_PARAM_SYS \
                INCLUDE_TIMER_SYS
}

