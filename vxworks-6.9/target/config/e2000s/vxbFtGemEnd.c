/* vxbFtGemEnd.c - FT E2000 GEM VxBus END driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#include <vxWorks.h>
#include <intLib.h>
#include <taskLib.h>
#include <logLib.h>
#include <muxLib.h>
#include <netLib.h>
#include <netBufLib.h>
#include <semLib.h>
#include <sysLib.h>
#include <vxBusLib.h>
#include <wdLib.h>
#include <etherMultiLib.h>
#include <end.h>
#define END_MACROS
#include <endLib.h>
#include <cacheLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/util/vxbParamSys.h>
#include <../src/hwif/h/mii/miiBus.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include <../src/hwif/h/hEnd/hEnd.h>
#include "vxbFtGemEnd.h"
#include "ftPinMux.h"

#undef GEM_END_DEBUG
#ifdef GEM_END_DEBUG
#   define GEM_DEBUG(fmt,a,b,c,d,e,f) \
       uartf(fmt,a,b,c,d,e,f)
#else /* GEM_END_DEBUG */
#   define GEM_DEBUG(fmt,a,b,c,d,e,f)
#endif /* GEM_END_DEBUG */

/* forward declarations */

IMPORT FUNCPTR _func_m2PollStatsIfPoll;
IMPORT BOOL autoNegForce; /* usrMiiCfg.c */

LOCAL void ftGemMacSet(GEM_DRV_CTRL * pDrvCtrl);

/* VxBus methods */

LOCAL void   ftGemInstInit    (VXB_DEVICE_ID);
LOCAL void   ftGemInstInit2   (VXB_DEVICE_ID);
LOCAL void   ftGemInstConnect (VXB_DEVICE_ID);
LOCAL STATUS ftGemInstUnlink  (VXB_DEVICE_ID, void *);

/* miiBus methods */

LOCAL STATUS ftGemPhyRead    (VXB_DEVICE_ID, UINT8, UINT8, UINT16 *);
LOCAL STATUS ftGemPhyWrite   (VXB_DEVICE_ID, UINT8, UINT8, UINT16);
LOCAL STATUS ftGemLinkUpdate (VXB_DEVICE_ID);

/* mux methods */

LOCAL void  ftGemMuxConnect (VXB_DEVICE_ID, void *);
LOCAL int ftGemPhyAddrGet (VXB_DEVICE_ID   pDev);
LOCAL void ftGemMacSet(GEM_DRV_CTRL * pDrvCtrl);
LOCAL BOOL ftGemMediaExceed1G(GEM_DRV_CTRL *pDrvCtrl);

LOCAL struct drvBusFuncs ftGemFuncs =
    {
    ftGemInstInit,   /* devInstanceInit */
    ftGemInstInit2,  /* devInstanceInit2 */
    ftGemInstConnect /* devConnect */
    };

LOCAL struct vxbDeviceMethod ftGemMethods[] =
   {
   
   /* Only 10/100/1000 speed need miiBus.
    * XGMAC need NOT mii bus. Decide when runtime.
    */
    
   DEVMETHOD(miiRead,        ftGemPhyRead),
   DEVMETHOD(miiWrite,       ftGemPhyWrite),
   DEVMETHOD(miiMediaUpdate, ftGemLinkUpdate),
   DEVMETHOD(muxDevConnect,  ftGemMuxConnect),
   DEVMETHOD(vxbDrvUnlink,   ftGemInstUnlink),
   { 0, 0 }
   };

/* default queue parameters */

LOCAL HEND_RX_QUEUE_PARAM ftGemRxQueueDefault =
    {
    NULL,                       /* jobQueId */
    0,                          /* priority */
    0,                          /* rbdNum */
    0,                          /* rbdTupleRatio */
    0,                          /* rxBufSize */
    NULL,                       /* pBufMemBase */
    0,                          /* rxBufMemSize */
    0,                          /* rxBufMemAttributes */
    NULL,                       /* rxBufMemFreeMethod */
    NULL,                       /* pRxBdBase */
    0,                          /* rxBdMemSize */
    0,                          /* rxBdMemAttributes */
    NULL                        /* rxBdMemFreeMethod */
    };

LOCAL HEND_TX_QUEUE_PARAM ftGemTxQueueDefault =
    {
    NULL,                       /* jobQueId */
    0,                          /* priority */
    0,                          /* tbdNum */
    0,                          /* allowedFrags */
    NULL,                       /* pTxBdBase */
    0,                          /* txBdMemSize */
    0,                          /* txBdMemAttributes */
    NULL                        /* txBdMemFreeMethod */
    };

LOCAL VXB_PARAMETERS ftGemParamDefaults[] =
    {
    {"rxQueue00", VXB_PARAM_POINTER,     {(void *)&ftGemRxQueueDefault}},
    {"txQueue00", VXB_PARAM_POINTER,     {(void *)&ftGemTxQueueDefault}},
    {"jumboEnable", VXB_PARAM_INT32, {(void *)0}}, /*0:Disable; 1:Enable*/
    {NULL,        VXB_PARAM_END_OF_LIST, {NULL}}
    };

LOCAL struct vxbPlbRegister ftGemDevPlbRegistration =
    {
    {
    NULL,                   /* pNext */
    VXB_DEVID_DEVICE,       /* devID */
    VXB_BUSID_PLB,          /* busID = PLB */
    VXB_VER_4_0_0,          /* vxbVersion */
    GEM_NAME,               /* drvName */
    &ftGemFuncs,        /* pDrvBusFuncs */
    NULL,                   /* pMethods */
    NULL,                   /* devProbe */
    ftGemParamDefaults  /* pParamDefaults */
    },
    };

/* END functions */

LOCAL STATUS    ftGemReset               (VXB_DEVICE_ID pDev);
LOCAL END_OBJ * ftGemEndLoad             (char *,    void *);
LOCAL STATUS    ftGemEndUnload           (END_OBJ *);
LOCAL int       ftGemEndIoctl            (END_OBJ *, int, caddr_t);
LOCAL STATUS    ftGemEndMCastAddrAdd     (END_OBJ *, char *);
LOCAL STATUS    ftGemEndMCastAddrDel     (END_OBJ *, char *);
LOCAL STATUS    ftGemEndMCastAddrGet     (END_OBJ *, MULTI_TABLE *);
LOCAL void      ftGemEndHashTblPopulate  (GEM_DRV_CTRL *);
LOCAL STATUS    ftGemEndStatsDump        (GEM_DRV_CTRL *);
LOCAL void      ftGemEndRxConfig         (GEM_DRV_CTRL *);
LOCAL STATUS    ftGemEndStart            (END_OBJ *);
LOCAL STATUS    ftGemEndStop             (END_OBJ *);
LOCAL int       ftGemEndSend             (END_OBJ *, M_BLK_ID);
LOCAL STATUS    ftGemEndPollSend         (END_OBJ *, M_BLK_ID);
LOCAL int       ftGemEndPollReceive      (END_OBJ *, M_BLK_ID);
LOCAL void      ftGemEndInt              (GEM_DRV_CTRL *);
LOCAL void      ftGemEndRxHandle         (void *);
LOCAL void      ftGemEndTxHandle         (void *);
LOCAL void      ftGemRxReset             (GEM_DRV_CTRL *);
LOCAL void      ftGemTxReset             (GEM_DRV_CTRL *);
LOCAL int       ftGemInitInterface(VXB_DEVICE_ID pDev);
LOCAL UINT32    ftGemProbeQueues(VXB_DEVICE_ID pDev);
LOCAL UINT32    ftGemDmaBusWith(VXB_DEVICE_ID pDev);

LOCAL NET_FUNCS ftGemNetFuncs =
    {
    ftGemEndStart,                   /* start func. */
    ftGemEndStop,                    /* stop func. */
    ftGemEndUnload,                  /* unload func. */
    ftGemEndIoctl,                   /* ioctl func. */
    ftGemEndSend,                    /* send func. */
    ftGemEndMCastAddrAdd,            /* multicast add func. */
    ftGemEndMCastAddrDel,            /* multicast delete func. */
    ftGemEndMCastAddrGet,            /* multicast get fun. */
    ftGemEndPollSend,                /* polling send func. */
    ftGemEndPollReceive,             /* polling receive func. */
    endEtherAddressForm,                 /* put address info into a NET_BUFFER */
    endEtherPacketDataGet,               /* get pointer to data in NET_BUFFER */
    endEtherPacketAddrGet                /* Get packet addresses */
    };

/*******************************************************************************
*
* ftGemRegister - register with the VxBus subsystem
*
* This routine registers the ftGem end driver with VxBus
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void ftGemRegister(void)
    {
    vxbDevRegister((struct vxbDevRegInfo *)&ftGemDevPlbRegistration);
    return;
    }

/*******************************************************************************
*
* ftGemInstInit - VxBus instInit handler
*
* This function implements the VxBus instInit handler for an ftGem
* device instance. The only thing done here is to select a unit
* number for the device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    const struct hcfDevice * pHcf;

    /* Always use the unit number allocated to us in the hwconf file. */

    pHcf = (struct hcfDevice *)pDev->pBusSpecificDevInfo;
    vxbInstUnitSet (pDev, pHcf->devUnit);

    pDev->pMethods = &ftGemMethods[0];

    return;
    }

/*******************************************************************************
*
* ftGemInstInit2 - VxBus instInit2 handler
*
* This function implements the VxBus instInit2 handler for an ftGem
* device instance. Once we reach this stage of initialization, it's
* safe for us to allocate memory, so we can create our pDrvCtrl
* structure and do some initial hardware setup. The important
* steps we do here are to create a child miiBus instance, connect
* our ISR to our assigned interrupt vector, read the station
* address from NVRAM.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;
    VXB_INST_PARAM_VALUE val;
    const struct   hcfDevice * pHcf;
    FUNCPTR        clkSetFunc = NULL;
    FUNCPTR        ifCachedFunc = NULL;
    BOOL           exFlag = FALSE;
	STATUS         st;
    
    pDrvCtrl = malloc (sizeof (GEM_DRV_CTRL));

    if (pDrvCtrl == NULL)
        {
        return;
        }

    pHcf = hcfDeviceGet(pDev);
    if (pHcf == NULL)
        {
        free (pDrvCtrl);
        return;
        }

    bzero ((char *)pDrvCtrl, sizeof(GEM_DRV_CTRL));

    pDev->pDrvCtrl    = pDrvCtrl;
    pDrvCtrl->gemDev = pDev;

	pDrvCtrl->autoneg = autoNegForce;

    pDrvCtrl->gemBar = pDev->pRegBase[0];
	pDev->regBaseFlags[0] = VXB_REG_IO;
    vxbRegMap (pDev, 0, &pDrvCtrl->gemHandle);

    if (pHcf != NULL)
        {

        (void) devResourceGet (pHcf, "ifCached", HCF_RES_ADDR, 
                              (void *) &ifCachedFunc);

        if (ifCachedFunc != NULL)
        	{
            pDrvCtrl->ifCached = ifCachedFunc;
        	}
        else
        	{
            pDrvCtrl->ifCached = NULL;
        	}

        if (devResourceGet (pHcf, "phyMode", HCF_RES_INT, 
                              (void *) &pDrvCtrl->phyInterface) != OK)
        	{
        	pDrvCtrl->phyInterface = PHY_INTERFACE_MODE_SGMII; /* default */
        	}

        }

    /*
     * See if the user wants jumbo frame support for this
     * interface. If the "jumboEnable" option isn't specified,
     * or it's set to 0, then jumbo support stays off,
     * otherwise we switch it on. 
     */

    /*
     * paramDesc {
     * The jumboEnable parameter specifies whether
     * this instance should support jumbo frames.
     * The default is false.  }
     */
    st = vxbInstParamByNameGet (pDev, "jumboEnable", VXB_PARAM_INT32, &val);
    if ((st != OK) || (val.int32Val == 0))
        {
        pDrvCtrl->gemMaxMtu = GEM_MTU;
        }
    else
        {
        pDrvCtrl->gemMaxMtu = GEM_JUMBO_MTU;
        }



    st = vxbIntConnect (pDev, 0, ftGemEndInt, pDrvCtrl);
	if (OK != st)
		{
		    GEM_DEBUG ("%s FAIL!! vxbIntConnect\r\n",__func__,2,3,4,5,6);
		}
	else
		{
		    /*GEM_DEBUG ("%s OK : vxbIntConnect\r\n",__func__,2,3,4,5,6);*/
		}

    pDrvCtrl->gemDevSem = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE |
                                      SEM_INVERSION_SAFE);
    pDrvCtrl->gemPhySem = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE |
                                      SEM_INVERSION_SAFE);

    pDrvCtrl->gemRxDescMem = cacheDmaMalloc(sizeof(GEM_DESC) * GEM_RX_DESC_CNT);

    if (!pDrvCtrl->gemRxDescMem)
        {
        GEM_DEBUG ("can not create rx desc\n",1,2,3,4,5,6);
        goto fail;
        }
    bzero ((char*)pDrvCtrl->gemRxDescMem, sizeof(GEM_DESC) * GEM_RX_DESC_CNT);

    pDrvCtrl->gemTxDescMem = cacheDmaMalloc(sizeof(GEM_DESC) * GEM_TX_DESC_CNT);
    if (!pDrvCtrl->gemTxDescMem)
        {
        GEM_DEBUG ("can not create tx desc\n",1,2,3,4,5,6);
        goto fail;
        }
    bzero ((char*)pDrvCtrl->gemTxDescMem, sizeof(GEM_DESC) * GEM_TX_DESC_CNT);

    /* reset the device */

    ftGemReset(pDev);

    if (ftGemMediaExceed1G(pDrvCtrl) == TRUE)
        {
        if (PHY_INTERFACE_MODE_USXGMII == pDrvCtrl->phyInterface)
            {
            pDrvCtrl->speed = SPEED_10000;
            }
        if (PHY_INTERFACE_MODE_5GBASER == pDrvCtrl->phyInterface)
            {
            pDrvCtrl->speed = SPEED_5000;
            }
        if (PHY_INTERFACE_MODE_2500BASEX == pDrvCtrl->phyInterface)
            {
            pDrvCtrl->speed = SPEED_2500;
            }

        ftGemInitInterface(pDev);
        }

    return;
    
fail:
    if (pDrvCtrl)
        {
        if (pDrvCtrl->gemTxDescMem)
            (void)cacheDmaFree (pDrvCtrl->gemTxDescMem);

        if (pDrvCtrl->gemRxDescMem)
            (void)cacheDmaFree (pDrvCtrl->gemRxDescMem);

        free (pDrvCtrl);
        }
    }

/*******************************************************************************
*
* ftGemInstConnect -  VxBus instConnect handler
*
* This function implements the VxBus instConnect handler for an ftGem
* device instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemInstConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
    return;
    }

/*******************************************************************************
*
* ftGemInstUnlink -  VxBus unlink handler
*
* This function shuts down an ftGem device instance in response to an
* unlink event from VxBus. This may occur if our VxBus instance has
* been terminated, or if the ftGem driver has been unloaded. When an
* unlink event occurs, we must shut down and unload the END interface
* associated with this device instance and then release all the
* resources allocated during instance creation. We also must destroy
* our child miiBus and PHY devices.
*
* RETURNS: OK if device was successfully destroyed, otherwise ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemInstUnlink
    (
    VXB_DEVICE_ID   pDev,
    void *          unused
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;

    pDrvCtrl = pDev->pDrvCtrl;

    /*
     * Stop the device and detach from the MUX.
     * Note: it's possible someone might try to delete
     * us after our vxBus instantiation has completed,
     * but before anyone has called our muxConnect method.
     * In this case, there'll be no MUX connection to
     * tear down, so we can skip this step.
     */

    if (pDrvCtrl->gemMuxDevCookie != NULL)
        {
        if (muxDevStop (pDrvCtrl->gemMuxDevCookie) != OK)
            {
            return (ERROR);
            }

        /* Detach from the MUX. */

        if (muxDevUnload (GEM_NAME, pDev->unitNumber) != OK)
            {
            return (ERROR);
            }
        }

     if (pDrvCtrl->gemTxDescMem)
         (void)cacheDmaFree (pDrvCtrl->gemTxDescMem);

     if (pDrvCtrl->gemRxDescMem)
         (void)cacheDmaFree (pDrvCtrl->gemRxDescMem);

    /* Disconnect the ISR. */

    vxbIntDisconnect (pDev, 0, ftGemEndInt, pDrvCtrl);

    /* Destroy our MII bus and child PHYs. */
	
    if (ftGemMediaExceed1G(pDrvCtrl) != TRUE)
        {
        if (pDrvCtrl->gemMiiBus != NULL)
            {
            (void) miiBusDelete (pDrvCtrl->gemMiiBus);
            }
    	}

    (void) semDelete(pDrvCtrl->gemDevSem);
    (void) semDelete(pDrvCtrl->gemPhySem);
        
    /* Destroy the adapter context. */

    free (pDrvCtrl);
    pDev->pDrvCtrl = NULL;

    return (OK);
    }

/*******************************************************************************
*
* ftGemPhyRead - miiBus miiRead method
*
* This function implements an miiRead() method that allows PHYs on the miiBus
* to access our MII management registers.
*
* RETURNS: ERROR if invalid PHY addr or register is specified, else OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemPhyRead
    (
    VXB_DEVICE_ID   pDev,
    UINT8           phyAddr,
    UINT8           regAddr,
    UINT16 *        dataVal
    )
    {
    GEM_DRV_CTRL *  pDrvCtrl;
    STATUS          rval = ERROR;
    FUNCPTR         miiRead;
    UINT32          miiVal, status;
    int             i;

    pDrvCtrl = pDev->pDrvCtrl;


    if (phyAddr > 31)
        {
        phyAddr -= 32;
        }


    (void)semTake (pDrvCtrl->gemPhySem, WAIT_FOREVER);

    miiVal = ((GEM_MAN_SOF)            /* Start of Frame Delimiter */
              |(GEM_MAN_READ)          /* Operation, 0x01 = Write, 0x02 = Read */
              |(GEM_MAN_PHYA(phyAddr)) /* Phy Number, 0 as we only have one */
              |(GEM_MAN_REGA(regAddr)) /* Phy Register */
              |(GEM_MAN_CODE));        /* must be 0x02 for turn around field */

    CSR_WRITE_4(pDev, GEM_MAN, miiVal);

    for (i = 0; i < GEM_TIMEOUT; i++)
        {
        status = CSR_READ_4(pDev, GEM_SR);
        if ((status & GEM_IDLE))
            {
            break;
            }
        }

    if (i == GEM_TIMEOUT)
        {
        GEM_DEBUG("Phy read timeOut! i=%d\r\n", i,2,3,4,5,6);
        *dataVal = 0xFFFF;
        }
    else
        {  /* loop i is about 766 */
        miiVal   = CSR_READ_4 (pDev, GEM_MAN);
        *dataVal = (UINT16)(miiVal & 0xFFFF);
        rval     = OK;
        }

    (void)semGive (pDrvCtrl->gemPhySem);

    return (rval);
    }

/*******************************************************************************
*
* ftGemPhyWrite - miiBus miiWrite method
*
* This function implements an miiWrite() method that allows PHYs on the miiBus
* to access our MII management registers.
*
* RETURNS: ERROR if invalid PHY addr or register is specified, else OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemPhyWrite
    (
    VXB_DEVICE_ID   pDev,
    UINT8           phyAddr,
    UINT8           regAddr,
    UINT16          dataVal
    )
    {
    GEM_DRV_CTRL *  pDrvCtrl;
    STATUS          rval = ERROR;
    UINT32          miiVal, status;
    FUNCPTR         miiWrite;
    int             i;

    pDrvCtrl = pDev->pDrvCtrl;

    if (phyAddr != pDrvCtrl->gemMiiPhyAddr && phyAddr < 32)
        {
        return (ERROR);
        }

    if (phyAddr > 31)
        {
        phyAddr -= 32;
        }

    /*
     * If we're not the management device (GEM0), then
     * forward the write request.
     */

    if (pDrvCtrl->gemMiiDev != pDev && pDrvCtrl->gemMiiPhyWrite != NULL)
        {
        miiWrite = pDrvCtrl->gemMiiPhyWrite;
        phyAddr += 32;
        return (miiWrite (pDrvCtrl->gemMiiDev, phyAddr, regAddr, dataVal));
        }

    (void)semTake (pDrvCtrl->gemPhySem, WAIT_FOREVER);

    miiVal = ((GEM_MAN_SOF)            /* Start of Frame Delimiter */
              |(GEM_MAN_WRITE)         /* Operation, 0x01 = Write, 0x02 = Read */
              |(GEM_MAN_PHYA(phyAddr)) /* Phy Number, 0 as we only have one */
              |(GEM_MAN_REGA(regAddr)) /* Phy Register */
              |(GEM_MAN_CODE)          /* must be 0x02 for turn around field */
              |(GEM_MAN_DATA(dataVal)));

    CSR_WRITE_4(pDev, GEM_MAN, miiVal);

    for (i = 0; i < GEM_TIMEOUT; i++)
        {
        status = CSR_READ_4(pDev, GEM_SR);
        if (status & GEM_IDLE)
            {
            break;
            }
        }

    if (i != GEM_TIMEOUT)
        rval = OK;

    (void)semGive(pDrvCtrl->gemPhySem);

    return (rval);
    }

/*******************************************************************************
*
* ftGemLinkUpdate - miiBus miiLinkUpdate method
*
* This function implements an miiLinkUpdate() method that allows
* miiBus to notify us of link state changes. This routine will be
* invoked by the miiMonitor task when it detects a change in link
* status. Normally, the miiMonitor task checks for link events every
* two seconds.
*
* Once we determine the new link state, we will announce the change
* to any bound protocols via muxError(). We also update the ifSpeed
* fields in the MIB2 structures so that SNMP queries can detect the
* correct link speed.
*
* RETURNS: ERROR if obtaining the new media setting fails, else OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemLinkUpdate
    (
    VXB_DEVICE_ID pDev
    )
    {
    GEM_DRV_CTRL *  pDrvCtrl;
    UINT32          oldStatus;
	UINT32          status;
    BOOL            exFlag;

    if (pDev->pDrvCtrl == NULL)
        {
        return (ERROR);
        }
    pDrvCtrl = (GEM_DRV_CTRL *)pDev->pDrvCtrl;
	oldStatus = pDrvCtrl->gemCurStatus;

    if (ftGemMediaExceed1G(pDrvCtrl) != TRUE)
        {
        if (pDrvCtrl->gemMiiBus == NULL)
            {
            return (ERROR);
            }
		
        if (miiBusModeGet(pDrvCtrl->gemMiiBus,
            &pDrvCtrl->gemCurMedia, &pDrvCtrl->gemCurStatus) == ERROR)
            {
            GEM_DEBUG("%s,%d: failed to get mii bus mode!\r\n", __func__,__LINE__,3,4,5,6);
            return (ERROR);
            }
    	}

    (void)semTake (pDrvCtrl->gemDevSem, WAIT_FOREVER);

    if (ftGemMediaExceed1G(pDrvCtrl) != TRUE)
        {   
        /* configure full duplex mode accordingly */
    
        if (pDrvCtrl->gemCurMedia & IFM_FDX)
            {
            CSR_SETBIT_4(pDev, GEM_CFG, GEM_CFG_FD);
    
            pDrvCtrl->duplex = 1;
            }
        else
            {
            CSR_CLRBIT_4(pDev, GEM_CFG, GEM_CFG_FD);
    
            pDrvCtrl->duplex = 0;
            }
    
        if (IFM_SUBTYPE(pDrvCtrl->gemCurMedia) == IFM_1000_T)
            {
            /*GEM_DEBUG("%s,%d set IFM_1000_T\r\n", __func__, __LINE__,3,4,5,6);*/
            CSR_SETBIT_4(pDev, GEM_CFG, GEM_CFG_GIG);
            CSR_CLRBIT_4(pDev, GEM_CFG, GEM_CFG_SPD);
            pDrvCtrl->gemEndObj.mib2Tbl.ifSpeed = 1000000000;
    
            pDrvCtrl->speed = SPEED_1000;
            }
        else if (IFM_SUBTYPE(pDrvCtrl->gemCurMedia) == IFM_100_TX)
            {
            /*GEM_DEBUG("%s,%d set IFM_100_Tx\r\n", __func__, __LINE__,3,4,5,6);*/
            CSR_CLRBIT_4(pDev, GEM_CFG, GEM_CFG_GIG);
            CSR_SETBIT_4(pDev, GEM_CFG, GEM_CFG_SPD);
            pDrvCtrl->gemEndObj.mib2Tbl.ifSpeed = 100000000;
    
            pDrvCtrl->speed = SPEED_100;
            }
        else
            {
            /*GEM_DEBUG("%s,%d set IFM_10\r\n", __func__, __LINE__,3,4,5,6);*/
            CSR_CLRBIT_4(pDev, GEM_CFG, GEM_CFG_GIG);
            CSR_CLRBIT_4(pDev, GEM_CFG, GEM_CFG_SPD);
            pDrvCtrl->gemEndObj.mib2Tbl.ifSpeed = 10000000;
    
            pDrvCtrl->speed = SPEED_10;
            }
    	}


    if (ftGemMediaExceed1G(pDrvCtrl) == TRUE)
        {
    	status = CSR_READ_4(pDev, GEM_USX_STATUS);
        if ((status & GEM_BLOCK_LOCK) == GEM_BLOCK_LOCK)
        	{
        	/* USX linkup */
    		
    		CSR_SETBIT_4(pDev, GEM_CFG, GEM_CFG_FD);
            pDrvCtrl->duplex = 1;
    
    		pDrvCtrl->gemCurStatus |= IFM_ACTIVE;
        	}
    	else
    		{
    		
    		/* USX linkdown */
    		
    		pDrvCtrl->gemCurStatus &= (~ IFM_ACTIVE);
    		}
    	} /* USXGMII */


    if (!(pDrvCtrl->gemEndObj.flags & IFF_UP))
        {
        (void)semGive(pDrvCtrl->gemDevSem);
        return (OK);
        }

    /* if status went from down to up, announce link up */

    if (pDrvCtrl->gemCurStatus & IFM_ACTIVE && !(oldStatus & IFM_ACTIVE))
        {

        ftGemInitInterface(pDev);
        
        if (pDrvCtrl->gemEndObj.pMib2Tbl != NULL)
            {
            pDrvCtrl->gemEndObj.pMib2Tbl->m2Data.mibIfTbl.ifSpeed =
                pDrvCtrl->gemEndObj.mib2Tbl.ifSpeed;
            }
        /*GEM_DEBUG("%s,%d =>link up\r\n", __func__, __LINE__,3,4,5,6);*/
        jobQueueStdPost(pDrvCtrl->gemJobQueue, NET_TASK_QJOB_PRI,
                        muxLinkUpNotify, &pDrvCtrl->gemEndObj,
                        NULL, NULL, NULL, NULL);
        }

    /* if status went from up to down, announce link down */

    else if (!(pDrvCtrl->gemCurStatus & IFM_ACTIVE) && oldStatus & IFM_ACTIVE)
        {
        /*GEM_DEBUG("%s,%d =>link down\r\n", __func__, __LINE__,3,4,5,6);*/
        jobQueueStdPost(pDrvCtrl->gemJobQueue, NET_TASK_QJOB_PRI,
                        muxLinkDownNotify, &pDrvCtrl->gemEndObj,
                        NULL, NULL, NULL, NULL);
        }

    (void)semGive(pDrvCtrl->gemDevSem);

    return (OK);
    }

/*******************************************************************************
*
* usxgmiiMonitorTask - A task to monitor USX
*
* This function is a task to monitor USX status.
* Only used for speed > 1G.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void usxgmiiMonitorTask
    (
    VXB_DEVICE_ID   pDev
    )
    {
    FOREVER
    {
        taskDelay (USXGMII_MONITOR_DELAY * sysClkRateGet ());
        ftGemLinkUpdate (pDev);
    }
    }

/*******************************************************************************
*
* ftGemMuxConnect - muxConnect method handler
*
* This function handles muxConnect() events, which may be triggered
* manually or (more likely) by the bootstrap code. Most VxBus
* initialization occurs before the MUX has been fully initialized,
* so the usual muxDevLoad()/muxDevStart() sequence must be defered
* until the networking subsystem is ready. This routine will ultimately
* trigger a call to ftGemEndLoad() to create the END interface instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemMuxConnect
    (
    VXB_DEVICE_ID   pDev,
    void *          unused
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;
    const struct hcfDevice * pHcf;
    VXB_DEVICE_ID   miiDev;
    char *          miiIfName;
    int             miiIfUnit;
    char            monName[32]; /* monitor task name for usx. */

    pDrvCtrl = pDev->pDrvCtrl;

    /*
     * Initialize MII bus.
     * Note that we defer this until the muxConnect stage in this driver,
     * unlike the others.  This is because all of the PHYs are managed through
     * the GEM interface, and we have to allow primary initialization of GEM
     * to complete before we can start searching for PHYs.
     */

    pHcf = hcfDeviceGet(pDev);
    if (pHcf == NULL)
        return;

    if (ftGemMediaExceed1G(pDrvCtrl) != TRUE)
        {
        /*
         * resourceDesc {
         * The miiIfName resource specifies the name of the
         * driver that provides the MII interface for this
         * GEM unit.  On boards that have multiple GEM
         * devices, the management pins for all of the PHYs
         * will all be wired to the MDIO pins on just one
         * controller.  The <miiIfName> resource (and the
         * <miiIfUnit> resource below) are used to tell
         * each GEM instance which one is the management
         * controller.  If a device is not the management
         * controller, it will just forward its PHY register
         * read and write requests to the one that is. }
         */
    
        (void) devResourceGet(pHcf, "miiIfName", HCF_RES_STRING, 
                             (void *)&miiIfName);
    
        /*
         * resourceDesc {
         * The miiIfUnit resource specifies the unit number
         * of the device that provides the MII management
         * methods for this GEM instance. }
         */
    
        (void) devResourceGet(pHcf, "miiIfUnit", HCF_RES_INT, (void *)&miiIfUnit);
    
        miiDev = vxbInstByNameFind(miiIfName, miiIfUnit);
    
        pDrvCtrl->gemMiiDev      = miiDev;
        pDrvCtrl->gemMiiPhyRead  = vxbDevMethodGet(miiDev, (UINT32)&miiRead_desc);
        pDrvCtrl->gemMiiPhyWrite = vxbDevMethodGet(miiDev, (UINT32)&miiWrite_desc);
        if (NULL == pDrvCtrl->gemMiiPhyRead)
        {
            GEM_DEBUG("NULL ptr for phy read/write!\r\n", 1,2,3,4,5,6);
        }
    
          
        /*
         * resourceDesc {
         * The phyAddr resource specifies the MII management
         * address (0-31) of the PHY for this particular GEM
         * device.  Each GEM typically has at least one PHY
         * allocated to it. }
         */
         
       if((ERROR == devResourceGet(pHcf, "phyAddr", HCF_RES_INT,(void *)&pDrvCtrl->gemMiiPhyAddr))
            ||(-1 == pDrvCtrl->gemMiiPhyAddr))
        {
    
            /* (after ->gemMiiDev assigned) get phy addr dynamicly... */
        
            pDrvCtrl->gemMiiPhyAddr = ftGemPhyAddrGet(pDev);
        }
        
        GEM_DEBUG("%s,%d: gemMiiPhyAddr = %d\r\n", 
         __func__,__LINE__,pDrvCtrl->gemMiiPhyAddr,4,5,6);
    	}

     /*
     * resourceDesc {
     * The macAddrFun resource used to get mac address }
     */
    (void) devResourceGet (pHcf, "macAddrSet", HCF_RES_ADDR,(void *)&pDrvCtrl->gemAddrSet);


    /* create our MII bus */
    if (ftGemMediaExceed1G(pDrvCtrl) != TRUE)
        {
        miiBusCreate (pDev, &pDrvCtrl->gemMiiBus);
        miiBusMediaListGet (pDrvCtrl->gemMiiBus, &pDrvCtrl->gemMediaList);
    	}
    /* save the cookie */

    pDrvCtrl->gemMuxDevCookie = muxDevLoad(pDev->unitNumber,
                                            ftGemEndLoad, "", TRUE, pDev);

    if (pDrvCtrl->gemMuxDevCookie != NULL)
        {
        muxDevStart(pDrvCtrl->gemMuxDevCookie);
        }

    if (_func_m2PollStatsIfPoll != NULL)
        {
        endPollStatsInit(pDrvCtrl->gemMuxDevCookie, _func_m2PollStatsIfPoll);
        }

    if (ftGemMediaExceed1G(pDrvCtrl) == TRUE)
        {
        bzero((char*)monName, sizeof(monName));   
        sprintf(monName, "mon%d", pDev->unitNumber);
        (void)taskSpawn (monName, 252, VX_UNBREAKABLE, 4096,
			      (FUNCPTR) usxgmiiMonitorTask,
			      pDev, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    	}	
    return;
    }

/*******************************************************************************
*
* ftGemReset - reset the controller
*
* This function resets the GEM controller, and also stops any DMA
* operations currently in progress,
*
* RETURNS: ERROR if the reset bit never clears, otherwise OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemReset
    (
    VXB_DEVICE_ID pDev
    )
    {
    GEM_DRV_CTRL *  pDrvCtrl;
    UINT32   gemRxBufSize, config, dbw, qnum, i;
    
    pDrvCtrl = pDev->pDrvCtrl;
    
    /* Disable all interrupts */

    CSR_WRITE_4 (pDev, GEM_IDR, 0xFFFFFFFF);
    
    /* Disable TX/RX */

    CSR_CLRBIT_4 (pDev, GEM_CTL, GEM_CTL_TE | GEM_CTL_RE);

    if (pDrvCtrl->gemMaxMtu == GEM_JUMBO_MTU)
        {
        gemRxBufSize = GEM_DMA_JUMBO_RDMALEN;

        /* Enable Jumbo Frames */

        config = GEM_CFG_JFRAME;

        /* config jumbo max length to GEM_JUMBO_MAX_LEN */

        CSR_WRITE_4 (pDev, GEM_JML, GEM_JUMBO_MAX_LEN);

        /* MUST enlardge sram for Q0 jumbo. at least 2*MTU */
        
        CSR_WRITE_4 (pDev, GEM_TX_Q_SEG_ALLOC_Q_LOWER, 0x1114);

        }
    else
        {
        gemRxBufSize = GEM_DMA_RDMALEN;

        /* Received oversized frames */

        config = 0; /*GEM_CFG_BIG*/
        }
    /* Setup network configuration */

    dbw = ftGemDmaBusWith(pDev);
    CSR_WRITE_4 (pDev, GEM_CFG,   config | GEM_CFG_FD
                 |  GEM_CFG_RXOL |  GEM_CFG_DRFCS
                 | GEM_CFG_PAE | dbw |
                 GEM_CFG_CLK_32 | GEM_CFG_RLCE );    


    /* Configure DMA */

    CSR_WRITE_4 (pDev, GEM_DCFG, gemRxBufSize | GEM_DMA_RBUF |
                 GEM_DMA_TBUF | GEM_DMA_TCKSUM | GEM_DMA_BLEN_16);

    /* Enable MDIO */

    CSR_SETBIT_4 (pDev, GEM_CTL, GEM_CTL_MPE);

    /* To disable Q1~Qmax, in order to save hardware resource, and improve performance */
    
    qnum = ftGemProbeQueues(pDev);
    for (i=1; i<qnum; i++)
    {
        CSR_WRITE_4 (pDev, GEM_Qx_TBQP(i-1), 0xffffffff);
        CSR_WRITE_4 (pDev, GEM_Qx_RBQP(i-1), 0xffffffff);
        CSR_WRITE_4 (pDev, GEM_Qx_IDR(i-1), 0xffffffff);
    }

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndLoad - END driver entry point
*
* This routine initializes the END interface instance associated
* with this device. In traditional END drivers, this function is
* the only public interface, and it's typically invoked by a BSP
* driver configuration stub. With VxBus, the BSP stub code is no
* longer needed, and this function is now invoked automatically
* whenever this driver's muxConnect() method is called.
*
* For older END drivers, the load string would contain various
* configuration parameters, but with VxBus this use is deprecated.
* The load string should just be an empty string. The second
* argument should be a pointer to the VxBus device instance
* associated with this device. Like older END drivers, this routine
* will still return the device name if the init string is empty,
* since this behavior is still expected by the MUX. The MUX will
* invoke this function twice: once to obtain the device name,
* and then again to create the actual END_OBJ instance.
*
* When this function is called the second time, it will initialize
* the END object, perform MIB2 setup, allocate a buffer pool, and
* initialize the supported END capabilities. The only special
* capability we support is VLAN_MTU, since we can receive slightly
* larger than normal frames.
*
* RETURNS: An END object pointer, or NULL on error, or 0 and the name
* of the device if the <loadStr> was empty.
*
* ERRNO: N/A
*/

LOCAL END_OBJ *ftGemEndLoad
    (
    char * loadStr,
    void * pArg
    )
    {
    GEM_DRV_CTRL *  pDrvCtrl;
    VXB_DEVICE_ID   pDev;
    STATUS r;

    /* make the MUX happy */

    if (loadStr == NULL)
        {
        return NULL;
        }

    if (loadStr[0] == 0)
        {
        bcopy (GEM_NAME, loadStr, sizeof(GEM_NAME));
        return NULL;
        }

    pDev     = pArg;
    pDrvCtrl = pDev->pDrvCtrl;

    if (END_OBJ_INIT (&pDrvCtrl->gemEndObj, NULL, pDev->pName,
        pDev->unitNumber, &ftGemNetFuncs, "GEM VxBus END Driver") == ERROR)
        {
        GEM_DEBUG ("%s%d: END_OBJ_INIT failed\n", (int)GEM_NAME,
                pDev->unitNumber, 0, 0, 0, 0);
        return (NULL);
        }

    /* get station address */

    ftGemMacSet(pDrvCtrl);


    /* allocate a buffer pool */

    if (GEM_JUMBO_MTU == pDrvCtrl->gemMaxMtu)
        {
        r = endPoolJumboCreate(GEM_TUPLE_CNT, &pDrvCtrl->gemEndObj.pNetPool);
        }
    else
        {
        r = endPoolCreate(GEM_TUPLE_CNT, &pDrvCtrl->gemEndObj.pNetPool);
        }
    
    if (ERROR == r)
        {
        GEM_DEBUG ("%s%d: pool creation failed\n", (int)GEM_NAME,
                pDev->unitNumber, 0, 0, 0, 0);
        return (NULL);
        }
	
    endM2Init (&pDrvCtrl->gemEndObj, M2_ifType_ethernet_csmacd,
               pDrvCtrl->gemAddr, ETHER_ADDR_LEN, pDrvCtrl->gemMaxMtu, 100000000,
               IFF_NOTRAILERS | IFF_SIMPLEX | IFF_MULTICAST | IFF_BROADCAST);
               
    pDrvCtrl->gemPollBuf = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);

    /* set up polling stats */

    pDrvCtrl->gemEndStatsConf.ifPollInterval = sysClkRateGet();
    pDrvCtrl->gemEndStatsConf.ifEndObj = &pDrvCtrl->gemEndObj;
    pDrvCtrl->gemEndStatsConf.ifWatchdog = NULL;
    pDrvCtrl->gemEndStatsConf.ifValidCounters = (END_IFINUCASTPKTS_VALID |
        END_IFINMULTICASTPKTS_VALID | END_IFINBROADCASTPKTS_VALID |
        END_IFINOCTETS_VALID | END_IFINERRORS_VALID | END_IFINDISCARDS_VALID |
        END_IFOUTUCASTPKTS_VALID | END_IFOUTMULTICASTPKTS_VALID |
        END_IFOUTBROADCASTPKTS_VALID | END_IFOUTOCTETS_VALID |
        END_IFOUTERRORS_VALID);

    /* set up capabilities */

    pDrvCtrl->gemCaps.cap_available = IFCAP_VLAN_MTU | IFCAP_TXCSUM |IFCAP_RXCSUM;
    pDrvCtrl->gemCaps.cap_enabled   = IFCAP_VLAN_MTU | IFCAP_TXCSUM |IFCAP_RXCSUM;
    if (GEM_JUMBO_MTU == pDrvCtrl->gemMaxMtu)
        {
        pDrvCtrl->gemCaps.cap_available |= IFCAP_JUMBO_MTU;
        pDrvCtrl->gemCaps.cap_enabled   |= IFCAP_JUMBO_MTU;
        }
    pDrvCtrl->gemCaps.csum_flags_tx = CSUM_IP|CSUM_TCP|CSUM_UDP;
    pDrvCtrl->gemCaps.csum_flags_rx = CSUM_IP|CSUM_UDP|CSUM_TCP;

    return (&pDrvCtrl->gemEndObj);
    }

/*******************************************************************************
*
* ftGemEndUnload - unload END driver instance
*
* This routine undoes the effects of ftGemEndLoad(). The END object
* is destroyed, our network pool is released, the endM2 structures
* are released, and the polling stats watchdog is terminated.
*
* Note that the END interface instance can't be unloaded if the
* device is still running. The device must be stopped with muxDevStop()
* first.
*
* RETURNS: ERROR if device is still in the IFF_UP state, otherwise OK
*
* RETURN: ERROR or EALREADY
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndUnload
    (
    END_OBJ * pEnd
    )
    {
    GEM_DRV_CTRL * pDrvCtrl = (GEM_DRV_CTRL *)pEnd;

    /* We must be stopped before we can be unloaded. */

    if (pEnd->flags & IFF_UP)
        {
        return (ERROR);
        }

    netMblkClChainFree (pDrvCtrl->gemPollBuf);

    /* Relase our buffer pool */

    endPoolDestroy (pDrvCtrl->gemEndObj.pNetPool);

    /* terminate stats polling */

    wdDelete (pDrvCtrl->gemEndStatsConf.ifWatchdog);

    endM2Free (&pDrvCtrl->gemEndObj);

    END_OBJECT_UNLOAD (&pDrvCtrl->gemEndObj);

    return (EALREADY);  /* prevent freeing of pDrvCtrl */
    }

/*******************************************************************************
*
* ftGemMacHashClac - calculate a hash checksum
*
* This routine performs the GEM hash calculation over MAC addresses.
* The GEM implements multicast filtering using a hash table where a hash
* checksum of the multicast group address is used as the table index.
*
* RETURNS: the 32-bit checksum of the supplied buffer
*
* ERRNO: N/A
*/

UINT32 ftGemMacHashCalc
    (
    const UINT8 * pBuf
    )
    {
    UINT32 hash = 0;
    UINT8 bit[6];
    int i;

    bit[5] = ((pBuf[0] >> 5) ^ (pBuf[1] >> 3) ^ (pBuf[2] >> 1) ^ (pBuf[2] >> 7)
            ^ (pBuf[3] >> 5) ^ (pBuf[4] >> 3) ^ (pBuf[5] >> 1) ^ (pBuf[5] >> 7))
            & 1;
    bit[4] = ((pBuf[0] >> 4) ^ (pBuf[1] >> 2) ^ (pBuf[2]) ^ (pBuf[2] >> 6)
            ^ (pBuf[3] >> 4) ^ (pBuf[4] >> 2) ^ (pBuf[5]) ^ (pBuf[5] >> 6))
            & 1;
    bit[3] = ((pBuf[0] >> 3) ^ (pBuf[1] >> 1) ^ (pBuf[1] >> 7) ^ (pBuf[2] >> 5)
            ^ (pBuf[3] >> 3) ^ (pBuf[4] >> 1) ^ (pBuf[4] >> 7) ^ (pBuf[5] >> 5))
            & 1;
    bit[2] = ((pBuf[0] >> 2) ^ (pBuf[1]) ^ (pBuf[1] >> 6) ^ (pBuf[2] >> 4)
            ^ (pBuf[3] >> 2) ^ (pBuf[4]) ^ (pBuf[4] >> 6) ^ (pBuf[5] >> 4))
            & 1;
    bit[1] = ((pBuf[0] >> 1) ^ (pBuf[0] >> 7) ^ (pBuf[1] >> 5) ^ (pBuf[2] >> 3)
            ^ (pBuf[3] >> 1) ^ (pBuf[3] >> 7) ^ (pBuf[4] >> 5) ^ (pBuf[5] >> 3))
            & 1;
    bit[0] = ((pBuf[0]) ^ (pBuf[0] >> 6) ^ (pBuf[1] >> 4) ^ (pBuf[2] >> 2)
            ^ (pBuf[3]) ^ (pBuf[3] >> 6) ^ (pBuf[4] >> 4) ^ (pBuf[5] >> 2))
            & 1;

    for (i = 0; i < 6; i++)
        if (bit[i])
            hash += (1 << i);

    return hash;
    }

/*******************************************************************************
*
* ftGemEndHashTblPopulate - populate the multicast hash filter
*
* This function programs the ft controller's multicast hash
* filter to receive frames sent to the multicast groups specified
* in the multicast address list attached to the END object. If
* the interface is in IFF_ALLMULTI mode, the filter will be
* programmed to receive all multicast packets by setting all the
* bits in the hash table to one.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemEndHashTblPopulate
    (
    GEM_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID pDev;
    ETHER_MULTI * mCastNode = NULL;
    UINT32        h;
    int           count = 0;
    UINT32        hashes[2] = { 0, 0 };

    pDev = pDrvCtrl->gemDev;

    if (pDrvCtrl->gemEndObj.flags & IFF_ALLMULTI)
        {
        /* set all multicast mode */

        CSR_WRITE_4(pDev, GEM_HSH, 0xFFFFFFFF);
        CSR_WRITE_4(pDev, GEM_HSL, 0xFFFFFFFF);
        CSR_SETBIT_4(pDev,GEM_CFG, GEM_CFG_MTI);
        return;
        }

    /* First, clear out the original filter. */

    CSR_WRITE_4(pDev, GEM_HSH, 0x0);
    CSR_WRITE_4(pDev, GEM_HSL, 0x0);

    /* Now repopulate it. */

    for (mCastNode =
         (ETHER_MULTI *) lstFirst (&pDrvCtrl->gemEndObj.multiList);
         mCastNode != NULL;
         mCastNode  = (ETHER_MULTI *) lstNext (&mCastNode->node))
        {
        h = ftGemMacHashCalc ((const UINT8 *)mCastNode->addr);

        if (h < 32)
            hashes[0] |= (1 << h);
        else
            hashes[1] |= (1 << (h - 32));

        count++;
        }

    CSR_WRITE_4(pDev, GEM_HSH, hashes[1]);
    CSR_WRITE_4(pDev, GEM_HSL, hashes[0]);

    if (count > 0)
        CSR_SETBIT_4(pDev,GEM_CFG, GEM_CFG_MTI);
    else
        CSR_CLRBIT_4(pDev,GEM_CFG, GEM_CFG_MTI);

    return;
    }

/*******************************************************************************
*
* ftGemEndMCastAddrAdd - add a multicast address for the device
*
* This routine adds a multicast address to whatever the driver
* is already listening for.  It then resets the address filter.
*
* RETURNS: OK.
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndMCastAddrAdd
    (
    END_OBJ * pEnd,
    char *    pAddr
    )
    {
    int retVal;

    retVal = etherMultiAdd (&pEnd->multiList, pAddr);

    if (retVal == ENETRESET)
        {
        pEnd->nMulti++;
        ftGemEndHashTblPopulate ((GEM_DRV_CTRL *)pEnd);
        }

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndMCastAddrDel - delete a multicast address for the device
*
* This routine removes a multicast address from whatever the driver
* is listening for.  It then resets the address filter.
*
* RETURNS: OK.
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndMCastAddrDel
    (
    END_OBJ * pEnd,
    char *    pAddr
    )
    {
    int retVal;

    retVal = etherMultiDel (&pEnd->multiList, pAddr);

    if (retVal == ENETRESET)
        {
        pEnd->nMulti--;
        ftGemEndHashTblPopulate ((GEM_DRV_CTRL *)pEnd);
        }

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndMCastAddrGet - get the multicast address list for the device
*
* This routine gets the multicast list of whatever the driver
* is already listening for.
*
* RETURNS: OK or ERROR.
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndMCastAddrGet
    (
    END_OBJ *       pEnd,
    MULTI_TABLE *   pTable
    )
    {
    return etherMultiGet (&pEnd->multiList, pTable);
    }

/*******************************************************************************
*
* ftGemEndStatsDump - return polled statistics counts
*
* This routine is automatically invoked periodically by the polled statistics
* watchdog.  All stats are available from the MIB registers.
*
* RETURNS: always OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndStatsDump
    (
    GEM_DRV_CTRL * pDrvCtrl
    )
    {
    END_IFCOUNTERS *    pEndStatsCounters;

    pEndStatsCounters = &pDrvCtrl->gemEndStatsCounters;

    pEndStatsCounters->ifInOctets = pDrvCtrl->gemInOctets;
    pDrvCtrl->gemInOctets = 0;

    pEndStatsCounters->ifInUcastPkts = pDrvCtrl->gemInUcasts;
    pDrvCtrl->gemInUcasts = 0;

    pEndStatsCounters->ifInMulticastPkts = pDrvCtrl->gemInMcasts;
    pDrvCtrl->gemInMcasts = 0;

    pEndStatsCounters->ifInBroadcastPkts = pDrvCtrl->gemInBcasts;
    pDrvCtrl->gemInBcasts = 0;

    pEndStatsCounters->ifInErrors = pDrvCtrl->gemInErrors;
    pDrvCtrl->gemInErrors = 0;

    pEndStatsCounters->ifInDiscards = pDrvCtrl->gemInDiscards;
    pDrvCtrl->gemInDiscards = 0;

    pEndStatsCounters->ifOutOctets = pDrvCtrl->gemOutOctets;
    pDrvCtrl->gemOutOctets = 0;

    pEndStatsCounters->ifOutUcastPkts = pDrvCtrl->gemOutUcasts;
    pDrvCtrl->gemOutUcasts = 0;

    pEndStatsCounters->ifOutMulticastPkts = pDrvCtrl->gemOutMcasts;
    pDrvCtrl->gemOutMcasts = 0;

    pEndStatsCounters->ifOutBroadcastPkts = pDrvCtrl->gemOutBcasts;
    pDrvCtrl->gemOutBcasts = 0;

    pEndStatsCounters->ifOutErrors = pDrvCtrl->gemOutErrors;
    pDrvCtrl->gemOutErrors = 0;

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndIoctl - the driver I/O control routine
*
* This function processes ioctl requests supplied via the muxIoctl()
* routine. In addition to the normal boilerplate END ioctls, this
* driver supports the IFMEDIA ioctls, END capabilities ioctls, and
* polled stats ioctls.
*
* RETURNS: A command specific response, usually OK or ERROR.
*
* ERRNO: N/A
*/

LOCAL int ftGemEndIoctl
    (
    END_OBJ *   pEnd,
    int         cmd,
    caddr_t     data
    )
    {
    GEM_DRV_CTRL *      pDrvCtrl;
    END_MEDIALIST *     mediaList;
    END_CAPABILITIES *  hwCaps;
    END_MEDIA *         pMedia;
    END_RCVJOBQ_INFO *  qinfo;
    UINT32              nQs;
    VXB_DEVICE_ID       pDev;
    INT32               value;
    int                 error = OK;

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
    pDev     = pDrvCtrl->gemDev;

    if (cmd != EIOCPOLLSTART && cmd != EIOCPOLLSTOP)
        (void)semTake (pDrvCtrl->gemDevSem, WAIT_FOREVER);
    
    switch (cmd)
        {
        case EIOCSADDR:
            if (data == NULL)
                error = EINVAL;
            else
                bcopy ((char *)data, (char *)pDrvCtrl->gemAddr,
                       ETHER_ADDR_LEN);

            ftGemEndRxConfig (pDrvCtrl);
            break;

        case EIOCGADDR:
            if (data == NULL)
                error = EINVAL;
            else
                bcopy ((char *)pDrvCtrl->gemAddr, (char *)data,
                       ETHER_ADDR_LEN);
            break;

        case EIOCSFLAGS:
            value = (INT32) data;
            if (value < 0)
                {
                value = -value;
                value--;
                END_FLAGS_CLR (pEnd, value);
                }
            else
                END_FLAGS_SET (pEnd, value);

            ftGemEndRxConfig (pDrvCtrl);
            break;

        case EIOCGFLAGS:
            if (data == NULL)
                error = EINVAL;
            else
                *(long *)data = END_FLAGS_GET(pEnd);
            break;

        case EIOCMULTIADD:
            error = ftGemEndMCastAddrAdd (pEnd, (char *) data);
            break;

        case EIOCMULTIDEL:
            error = ftGemEndMCastAddrDel (pEnd, (char *) data);
            break;

        case EIOCMULTIGET:
            error = ftGemEndMCastAddrGet (pEnd, (MULTI_TABLE *) data);
            break;

        case EIOCPOLLSTART:
            pDrvCtrl->gemPolling = TRUE;

            /* Disable all interrupts */

            CSR_WRITE_4(pDev, GEM_IDR, 0xFFFFFFFF);

            /*
             * We may have been asked to enter polled mode while
             * there are transmissions pending. This is a problem,
             * because the polled transmit routine expects that
             * the TX ring will be empty when it's called. In
             * order to guarantee this, we have to drain the TX
             * ring here. We could also just plain reset and
             * reinitialize the transmitter, but this is faster.
             */

            END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);

            while (pDrvCtrl->gemTxFree < GEM_TX_DESC_CNT)
                {
                volatile GEM_DESC * pDesc;
                M_BLK_ID            pMblk;

                pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxCons];

                /* Wait for ownership bit to clear */

                while (!(pDesc->bdSts & TXBUF_STAT_USED))
                    ;

                pMblk = pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons];

                if (pMblk != NULL)
                    {
                    endPoolTupleFree (pMblk);
                    pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons] = NULL;
                    }

                pDrvCtrl->gemTxFree++;
                GEM_INC_DESC (pDrvCtrl->gemTxCons, GEM_TX_DESC_CNT);
                }

            END_TX_SEM_GIVE (pEnd);

            break;

        case EIOCPOLLSTOP:

            /* Enable TX RX interrupts */

            CSR_WRITE_4(pDev, GEM_IER, GEM_INT_TX_RX);

            pDrvCtrl->gemPolling = FALSE;

            break;    

        case EIOCGMIB2233:
        case EIOCGMIB2:
            error = endM2Ioctl (&pDrvCtrl->gemEndObj, cmd, data);
            break;

        case EIOCGPOLLCONF:
            if (data == NULL)
                error = EINVAL;
            else
                *((END_IFDRVCONF **)data) = &pDrvCtrl->gemEndStatsConf;
            break;

        case EIOCGPOLLSTATS:
            if (data == NULL)
                error = EINVAL;
            else
                {
                error = ftGemEndStatsDump(pDrvCtrl);
                if (error == OK)
                    *((END_IFCOUNTERS **)data) = &pDrvCtrl->gemEndStatsCounters;
                }
            break;

        case EIOCGMEDIALIST:
            if (data == NULL)
                {
                error = EINVAL;
                break;
                }
            if (pDrvCtrl->gemMediaList->endMediaListLen == 0)
                {
                error = ENOTSUP;
                break;
                }

            mediaList = (END_MEDIALIST *)data;
            if (mediaList->endMediaListLen <
                pDrvCtrl->gemMediaList->endMediaListLen)
                {
                mediaList->endMediaListLen =
                    pDrvCtrl->gemMediaList->endMediaListLen;
                error = ENOSPC;
                break;
                }

            bcopy((char *)pDrvCtrl->gemMediaList, (char *)mediaList,
                  sizeof(END_MEDIALIST) + (sizeof(UINT32) *
                  pDrvCtrl->gemMediaList->endMediaListLen));
            break;

        case EIOCGIFMEDIA:
            if (data == NULL)
                error = EINVAL;
            else
                {
                pMedia = (END_MEDIA *)data;
                pMedia->endMediaActive = pDrvCtrl->gemCurMedia;
                pMedia->endMediaStatus = pDrvCtrl->gemCurStatus;
                }
            break;

        case EIOCSIFMEDIA:
            if (data == NULL)
                error = EINVAL;
            else
                {
                pMedia = (END_MEDIA *)data;
                miiBusModeSet (pDrvCtrl->gemMiiBus, pMedia->endMediaActive);
                ftGemLinkUpdate (pDrvCtrl->gemDev);
                error = OK;
                }
            break;

        case EIOCGIFCAP:
            hwCaps = (END_CAPABILITIES *)data;
            if (hwCaps == NULL)
                {
                error = EINVAL;
                break;
                }
            hwCaps->csum_flags_tx = pDrvCtrl->gemCaps.csum_flags_tx;
            hwCaps->csum_flags_rx = pDrvCtrl->gemCaps.csum_flags_rx;
            hwCaps->cap_available = pDrvCtrl->gemCaps.cap_available;
            hwCaps->cap_enabled   = pDrvCtrl->gemCaps.cap_enabled;
            break;

        /*
         * The only special capability we support is VLAN_MTU, and
         * it can never be turned off.
         */

        case EIOCSIFCAP:
            error = ENOTSUP;
            break;

        case EIOCGIFMTU:
            if (data == NULL)
                error = EINVAL;
            else
                *(INT32 *)data = pEnd->mib2Tbl.ifMtu;
            break;

        case EIOCSIFMTU:
            value = (INT32)data;
            if (value <= 0 || value > GEM_JUMBO_MTU)
                {
                error = EINVAL;
                break;
                }
            pEnd->mib2Tbl.ifMtu = value;
            if (pEnd->pMib2Tbl != NULL)
                pEnd->pMib2Tbl->m2Data.mibIfTbl.ifMtu = value;

            if ((value > GEM_MTU) &&
                (pDrvCtrl->gemCaps.cap_available & IFCAP_JUMBO_MTU))
                {
                /*pDrvCtrl->gemEnableJumbo = TRUE;*/
                ftGemEndStop (pEnd);
                ftGemEndStart (pEnd);
                }
            else if (value <= GEM_MTU)
                {
                /*pDrvCtrl->gemEnableJumbo = FALSE;*/
                ftGemEndStop (pEnd);
                ftGemEndStart (pEnd);
                }
            break;

        case EIOCGRCVJOBQ:
            if (data == NULL)
                {
                error = EINVAL;
                break;
                }

            qinfo = (END_RCVJOBQ_INFO *)data;
            nQs = qinfo->numRcvJobQs;
            qinfo->numRcvJobQs = 1;
            if (nQs < 1)
                error = ENOSPC;
            else
                qinfo->qIds[0] = pDrvCtrl->gemJobQueue;
            break;

        default:
            error = EINVAL;
            break;
        }

    if (cmd != EIOCPOLLSTART && cmd != EIOCPOLLSTOP)        
        (void)semGive (pDrvCtrl->gemDevSem);
        
    return (error);
    }

/*******************************************************************************
*
* ftGemEndRxConfig - configure the FCC's RX filter
*
* This is a helper routine used by ftGemEndIoctl() and ftGemEndStart() to
* configure the controller's RX filter. The unicast address filter is
* programmed with the currently configured MAC address, and the RX
* configuration is set to allow unicast and broadcast frames to be
* received. If the interface is in IFF_PROMISC mode, the RX_PROMISC
* bit is set, which allows all packets to be received.
*
* The ftGemEndHashTblPopulate() routine is also called to update the
* multicast filter.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemEndRxConfig
    (
    GEM_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID pDev;

    pDev = pDrvCtrl->gemDev;

    /* set the MAC address - must write the low first */

    CSR_WRITE_4(pDev, GEM_SA1L, (pDrvCtrl->gemAddr[3] << 24) |
                                (pDrvCtrl->gemAddr[2] << 16) |
                                (pDrvCtrl->gemAddr[1] << 8)  |
                                (pDrvCtrl->gemAddr[0] << 0));
    CSR_WRITE_4(pDev, GEM_SA1H, (pDrvCtrl->gemAddr[5] << 8) |
                                (pDrvCtrl->gemAddr[4] << 0));

    /* Enable promisc mode, if specified. */

    if (pDrvCtrl->gemEndObj.flags & IFF_PROMISC)
        CSR_SETBIT_4(pDev, GEM_CFG, GEM_CFG_CAF);
    else
        CSR_CLRBIT_4(pDev, GEM_CFG, GEM_CFG_CAF);

    /* Program the multicast filter. */

    ftGemEndHashTblPopulate (pDrvCtrl);

    return;
    }

/*******************************************************************************
*
* ftGemEndStart - start the device
*
* This function resets the device to put it into a known state and
* then configures it for RX and TX operation. The RX and TX configuration
* registers are initialized, and the address of the RX and TX DMA rings
* are loaded into the device. Interrupts are then enabled, and the initial
* link state is configured.
*
* Note that this routine also checks to see if an alternate jobQueue
* has been specified via the vxbParam subsystem. This allows the driver
* to divert its work to an alternate processing task, such as may be
* done with TIPC. This means that the jobQueue can be changed while
* the system is running, but the device must be stopped and restarted
* for the change to take effect.
*
* RETURNS: ERROR if device initialization failed, otherwise OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndStart
    (
    END_OBJ * pEnd
    )
    {
    GEM_DRV_CTRL *         pDrvCtrl;
    VXB_DEVICE_ID          pDev;
    VXB_INST_PARAM_VALUE   val;
    HEND_RX_QUEUE_PARAM *  pRxQueue;
    GEM_DESC *             pDesc;
    int                    i;
    M_BLK_ID               pMblk = NULL;
	STATUS                 st;

    if (pEnd->flags & IFF_UP)
        return (OK);

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
    pDev     = pDrvCtrl->gemDev;

    (void)semTake (pDrvCtrl->gemDevSem, WAIT_FOREVER);

    /* Initialize job queues */

    pDrvCtrl->gemJobQueue = netJobQueueId;

    /* Override the job queue ID if the user supplied an alternate one. */

    /*
     * paramDesc {
     * The rxQueue00 parameter specifies a pointer to
     * a HEND_RX_QUEUE_PARAM structure, which contains,
     * among other things, an ID for the job queue
     * to be used for this instance. }
     */

    if (vxbInstParamByNameGet (pDev, "rxQueue00",
        VXB_PARAM_POINTER, &val) == OK)
        {
        pRxQueue = (HEND_RX_QUEUE_PARAM *) val.pValue;
        if (pRxQueue->jobQueId != NULL)
            pDrvCtrl->gemJobQueue = pRxQueue->jobQueId;
        }

    QJOB_SET_PRI(&pDrvCtrl->gemTxJob, NET_TASK_QJOB_PRI);
    pDrvCtrl->gemTxJob.func = ftGemEndTxHandle;
    QJOB_SET_PRI(&pDrvCtrl->gemRxJob, NET_TASK_QJOB_PRI);
    pDrvCtrl->gemRxJob.func = ftGemEndRxHandle;

    vxAtomicSet (&pDrvCtrl->gemRxPending, FALSE);
    vxAtomicSet (&pDrvCtrl->gemTxPending, FALSE);

    /* Reset controller to known state */

    ftGemReset (pDev);
    if (ftGemMediaExceed1G(pDrvCtrl) == TRUE)
        {
        ftGemInitInterface(pDev);
    	}

    /* Set up the RX ring. */

    for (i = 0; i < GEM_RX_DESC_CNT; i++)
        {

        /*
         * Try to conjure up a new mBlk tuple to hold the incoming
         * packet. If this fails, we have to skip the packet and move
         * to the next one.
         */

        pMblk = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);

        if (pMblk == NULL)
            {
            GEM_DEBUG("endPoolTupleGet() return NULL in RX path\n",1,2,3,4,5,6);
            (void)semGive (pDrvCtrl->gemDevSem);
            return (ERROR);
            }

        pMblk->m_next = NULL;

        (void)cacheInvalidate (DATA_CACHE, pMblk->m_data, pMblk->m_len);

        pDrvCtrl->gemRxMblk[i] = pMblk;

        pDesc = &pDrvCtrl->gemRxDescMem[i];

        pDesc->bdAddr = mtod(pMblk, UINT32);
        pDesc->bdAddr &= ~RXBUF_ADD_OWNED;
        pDesc->bdSts = 0;   
        }

    pDesc = &pDrvCtrl->gemRxDescMem[GEM_RX_DESC_CNT - 1];
    pDesc->bdAddr |= RXBUF_ADD_WRAP;

    /* Set up TX ring */

    for (i = 0; i < GEM_TX_DESC_CNT; i++)
        {
        pDesc = &pDrvCtrl->gemTxDescMem[i];

        pDesc->bdAddr = 0;
        pDesc->bdSts  = TXBUF_STAT_USED;   /* buffer not used to transmit */
        }

    pDesc = &pDrvCtrl->gemTxDescMem[GEM_TX_DESC_CNT - 1];
    pDesc->bdSts |= TXBUF_STAT_WRAP;

    /* Initialize state */

    pDrvCtrl->gemRxIdx     = 0;
    pDrvCtrl->gemTxLast    = 0;
    pDrvCtrl->gemTxStall   = FALSE;
    pDrvCtrl->gemRxStall   = FALSE;
    pDrvCtrl->gemTxProd    = 0;
    pDrvCtrl->gemTxCons    = 0;
    pDrvCtrl->gemTxFree    = GEM_TX_DESC_CNT;

    /* Set Tx/Rx pointer. */

    CSR_WRITE_4(pDev, GEM_TBQP,(UINT32)pDrvCtrl->gemTxDescMem);
    CSR_WRITE_4(pDev, GEM_RBQP,(UINT32)pDrvCtrl->gemRxDescMem);

    /* Zero the stats counters. */

    pDrvCtrl->gemInUcasts = pDrvCtrl->gemInBcasts = pDrvCtrl->gemInMcasts = 0;
    pDrvCtrl->gemOutUcasts = pDrvCtrl->gemOutBcasts =
        pDrvCtrl->gemOutMcasts = 0;
    pDrvCtrl->gemInOctets = pDrvCtrl->gemOutOctets = 0;

    /* Clear Tx status bits */

    CSR_CLRBIT_4(pDev, GEM_TSR, (GEM_TSR_UND | GEM_TSR_COMP | \
                                 GEM_TSR_BNQ | GEM_TSR_TXGO | \
                                 GEM_TSR_RLE | GEM_TSR_COL  | \
                                 GEM_TSR_OVR));

    /* Clear Rx status bits */

    CSR_WRITE_4 (pDev, GEM_RSR, (GEM_RSR_OVR | GEM_RSR_REC | GEM_RSR_BNA));

    ftGemEndRxConfig (pDrvCtrl);

    st = vxbIntEnable (pDev, 0, ftGemEndInt, pDrvCtrl);
	if (OK != st)
		{
		    GEM_DEBUG ("gem FAIL!! vxbIntEnable\r\n",1,2,3,4,5,6);
		}
	else
		{
		    /*GEM_DEBUG ("gem OK : vxbIntEnable\r\n",1,2,3,4,5,6);*/
		}

    /* Enable interrupts */

    CSR_WRITE_4 (pDev, GEM_IER, GEM_INT_TX_RX);

    /* Enable TX/RX and clear the statistics counters */

    CSR_SETBIT_4 (pDev, GEM_CTL, (GEM_CTL_TE  | GEM_CTL_RE |
                                  GEM_CTL_CSR | GEM_CTL_MPE));

    /* Set initial link state */

    pDrvCtrl->gemCurMedia  = IFM_ETHER | IFM_NONE;
    pDrvCtrl->gemCurStatus = IFM_AVALID;

    if (ftGemMediaExceed1G(pDrvCtrl) != TRUE)
        {
        miiBusModeSet (pDrvCtrl->gemMiiBus,
            pDrvCtrl->gemMediaList->endMediaListDefault);
    	}


    END_FLAGS_SET (pEnd, (IFF_UP | IFF_RUNNING));

    (void)semGive (pDrvCtrl->gemDevSem);

    CACHE_PIPE_FLUSH();

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndStop - stop the device
*
* This function undoes the effects of ftGemEndStart(). The device is shut
* down and all resources are released. Note that the shutdown process
* pauses to wait for all pending RX, TX and link event jobs that may have
* been initiated by the interrupt handler to complete. This is done
* to prevent tNetTask from accessing any data that might be released by
* this routine.
*
* RETURNS: ERROR if device shutdown failed, otherwise OK
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndStop
    (
    END_OBJ * pEnd
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;
    VXB_DEVICE_ID  pDev;
    int            i;

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
    pDev     = pDrvCtrl->gemDev;

    (void)semTake (pDrvCtrl->gemDevSem, WAIT_FOREVER);
    
    if (!(pEnd->flags & IFF_UP))
        {
        (void)semGive (pDrvCtrl->gemDevSem);
        return (OK);
        }

    END_FLAGS_CLR (pEnd, (IFF_UP | IFF_RUNNING));

    /* Disable TX/RX */

    CSR_CLRBIT_4 (pDev, GEM_CTL ,(GEM_CTL_TE | GEM_CTL_RE));

    /* Disable interrupts */

    CSR_WRITE_4 (pDev, GEM_IDR, 0xFFFFFFFF);

    (void) vxbIntDisable (pDev, 0, ftGemEndInt, pDrvCtrl);

    /*
     * Wait for all jobs to drain.
     * Note: this must be done before we disable the receiver
     * and transmitter below. If someone tries to reboot us via
     * WDB, this routine may be invoked while the RX handler is
     * still running in tNetTask. If we disable the chip while
     * that function is running, it'll start reading inconsistent
     * status from the chip. We have to wait for that job to
     * terminate first, then we can disable the receiver and
     * transmitter.
     */

    for (i = 0; i < GEM_TIMEOUT; i++)
        {
        if (vxAtomicGet (&pDrvCtrl->gemRxPending) == FALSE &&
            vxAtomicGet (&pDrvCtrl->gemTxPending) == FALSE)
            break;
        taskDelay (1);
        }

    if (i == GEM_TIMEOUT)
        {
        GEM_DEBUG ("%s%d: timed out waiting for job to complete\n",
               (int)GEM_NAME, pDev->unitNumber, 0, 0, 0, 0);
        }

    /*
     * Flush the recycle cache to shake loose any of our
     * mBlks that may be stored there.
     */

    endMcacheFlush ();

    END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);

    for (i = 0; i < GEM_TX_DESC_CNT; i++)
        {
        if (pDrvCtrl->gemTxMblk[i] != NULL)
            {
            netMblkClChainFree (pDrvCtrl->gemTxMblk[i]);
            pDrvCtrl->gemTxMblk[i] = NULL;
            }
        }

    for (i = 0; i < GEM_RX_DESC_CNT; i++)
        {
        if (pDrvCtrl->gemRxMblk[i] != NULL)
            {
            netMblkClChainFree (pDrvCtrl->gemRxMblk[i]);
            pDrvCtrl->gemRxMblk[i] = NULL;
            }
        }

    END_TX_SEM_GIVE (pEnd);

    (void)semGive (pDrvCtrl->gemDevSem);

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndInt - handle device interrupts
*
* This function is invoked whenever the FCC's interrupt line is asserted.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemEndInt
    (
    GEM_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID   pDev;
    volatile UINT32 status;

    pDev = pDrvCtrl->gemDev;

    status = CSR_READ_4(pDev, GEM_ISR);

#ifdef GEM_END_DEBUG    
    if (status & ~(GEM_INT_TX_RX))
        {
        GEM_DEBUG ("GemRndInt: unexpected ints 0x%x\n",
                   (status & ~(GEM_INT_TX_RX)), 0,0,0,0,0);
        }
#endif /* GEM_END_DEBUG */
                   
    /* Receive correlative interrupt */

    if ((status & GEM_INT_RX) &&        
        vxAtomic32Cas (&pDrvCtrl->gemRxPending, FALSE, TRUE))   
        {
        /*
         * Disbale receive correlative interrupt, it will be 
         * enabled again when this RX job is completed
         */

        CSR_WRITE_4 (pDev, GEM_IDR, GEM_INT_RX);
        
        /* Queue the RX job to tNet task */

        jobQueuePost (pDrvCtrl->gemJobQueue, &pDrvCtrl->gemRxJob);
        }

    /* Transmit correlative interrupt */

    if ((status & GEM_INT_TX) &&    
        vxAtomic32Cas (&pDrvCtrl->gemTxPending, FALSE, TRUE))
        {
        /*
         * Disable transmit correlative interrupt, it will be 
         * enabled again when this TX job is completed
         */

        CSR_WRITE_4 (pDev, GEM_IDR, GEM_INT_TX);
        
        /* Queue the TX job to tNet task */

        jobQueuePost (pDrvCtrl->gemJobQueue, &pDrvCtrl->gemTxJob);
        }

     if (status & GEM_INT_TUND)
        {
        GEM_DEBUG ("GemInt: TX TUND [0x%x]\n", status,0,0,0,0,0);
        }

     if (status & GEM_INT_RTRY)
        {
        GEM_DEBUG ("GemInt: TX RTRY [0x%x]\n", status,0,0,0,0,0);
        }

     if (status & GEM_INT_HRESP_NOT_OK)
        {
        GEM_DEBUG ("GemInt: GEM_INT_HRESP_NOT_OK [0x%x]\n", status,0,0,0,0,0);
        }

     if (status & GEM_INT_RBNA)
        {
        
        GEM_DEBUG ("GemInt: RX RBNA [0x%x]\n", status,0,0,0,0,0);
        }

     if (status & GEM_INT_ROVR)
        {
        GEM_DEBUG ("GemInt: GEM_INT_ROVR [0x%x]\n", status,0,0,0,0,0);
        }       


    /* Ack the interrupt */

    CSR_WRITE_4 (pDev, GEM_ISR, status);
    }

/*******************************************************************************
*
* ftGemEndRxHandle - process received frames
*
* This function is scheduled by the ISR to run in the context of tNetTask
* whenever an RX interrupt is received. It processes packets from the
* RX DMA ring and encapsulates them into mBlk tuples which are handed up
* to the MUX.
*
* There may be several packets waiting in the ring to be processed.
* We take care not to process too many packets in a single run through
* this function so as not to monopolize tNetTask and starve out other
* jobs waiting in the jobQueue. If we detect that there's still more
* packets waiting to be processed, we queue ourselves up for another
* round of processing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemEndRxHandle
    (
    void * pArg
    )
    {
    QJOB *          pJob;
    GEM_DRV_CTRL *  pDrvCtrl;
    VXB_DEVICE_ID   pDev;
    M_BLK_ID        pMblk = NULL, pNewMblk = NULL;
    GEM_DESC *      pDesc;
    int             rxLen;
    UINT32          regval;
    int             loopCounter = GEM_MAX_RX;
    UINT32          descBdSts;
    UINT32          regRxOvr1 = 0;
    UINT32          regRxOvr2 = 0;

    pJob     = pArg;
    pDrvCtrl = member_to_object (pJob, GEM_DRV_CTRL, gemRxJob);
    pDev     = pDrvCtrl->gemDev;
    pDesc    = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];

    /* Clear RX status register */

    regval = CSR_READ_4(pDev, GEM_RSR);
    CSR_WRITE_4 (pDev, GEM_RSR, regval);

    /*
     * If any RX HRESP error conditions were detected, make a note here,
     * we're required to reset the receiver later. RX buffer descriptor not
     * available error does not require any special processing.
     * It was observed that the rx module will hung and stop to trig the rx
     * interrupt when the rx module was reset by improper condition such as
     * rx overrun. According to "Zynq-7000 Extensible Processing Platform
     * Technical Reference Manual UG585 (DRAFT) March 29, 2012" section B.18,
     * it said: for DMA operation the buffer will be recovered if an overrun
     * occurs, which means rx overrun also does not require any special 
     * processing.
     */

    if (regval & ~(GEM_RSR_REC|GEM_RSR_BNA|GEM_RSR_OVR))
        {
        pDrvCtrl->gemRxStall = TRUE;
        }

    /*
     * Sometimes it was observed that rx module will stop to work under
     * heavy pressure without any error indication from GEM_RSR register,
     * and a reset of rx module could help the rx module work again.
     * Since there is no error indication(means any bit in GEM_RSR except
     * GEM_RSR_REC|GEM_RSR_BNA|GEM_RSR_OVR) is supplied by GEM_RSR register
     * when this bug happened, it's not possible for software to catch this
     * bug. But fortunately, there is a phenomenon happened at same time when
     * this bug happened: the value of GEM_ROV could not be cleared after
     * read it, we use this phenomenon as a criterion to say that rx module
     * stoped and need a reset.
     */

    /*
     * read the value of GEM_ROV, after reading the value of GEM_ROV
     * should be cleared.
     */

    regRxOvr1 = CSR_READ_4(pDev, GEM_ROV);

    /*
     * the value of GEM_ROV should be zero now, if not, it means rx module
     * stop to work.
     */

    regRxOvr2 = CSR_READ_4(pDev, GEM_ROV);

    if ((regRxOvr1 != 0) && (regRxOvr2 != 0))
        {
        pDrvCtrl->gemRxStall = TRUE;

        GEM_DEBUG ( "gemRxStall detected by GEM_ROV, "
                           "regRxOvr1(%d), regRxOvr2(%d)\n",
                           regRxOvr1, regRxOvr2,2,3,4,5);
        }
    
    while (loopCounter && (pDesc->bdAddr & RXBUF_ADD_OWNED))
        {
#ifdef GEM_END_DEBUG            
        if (!(pDesc->bdSts & RXBUF_STAT_SOF) || 
            !(pDesc->bdSts & RXBUF_STAT_EOF))
            {
            GEM_DEBUG ("bad package \r\n", 0, 1, 2, 3, 4, 5);
            }
#endif /* GEM_END_DEBUG */

        pNewMblk = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);

        if (pNewMblk == NULL)
            {
            GEM_DEBUG ("%s%d: out of mBlks at %d\n", (int)GEM_NAME,
                       pDev->unitNumber, pDrvCtrl->gemRxIdx,0,0,0);

            pDrvCtrl->gemLastError.errCode = END_ERR_NO_BUF;
            muxError (&pDrvCtrl->gemEndObj, &pDrvCtrl->gemLastError);
            pDrvCtrl->gemInDiscards++;

            pDesc->bdAddr &= ~RXBUF_ADD_OWNED;
            pDesc->bdSts = 0;
            if(pDrvCtrl->gemRxIdx == (GEM_RX_DESC_CNT-1))
                pDesc->bdAddr |= RXBUF_ADD_WRAP;

            GEM_INC_DESC(pDrvCtrl->gemRxIdx, GEM_RX_DESC_CNT);
            pDesc = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];
            continue;
            }

        
        
        if ((pDrvCtrl->gemCaps.cap_enabled & IFCAP_JUMBO_MTU) == IFCAP_JUMBO_MTU)
            {
            rxLen = pDesc->bdSts & RXBUF_STAT_JLEN_MASK;
            }
        else
            {
            rxLen = pDesc->bdSts & RXBUF_STAT_LEN_MASK;
            }
        
        pMblk = pDrvCtrl->gemRxMblk[pDrvCtrl->gemRxIdx];
        pDrvCtrl->gemRxMblk[pDrvCtrl->gemRxIdx] = pNewMblk;
        pNewMblk->m_next = NULL;

        /*
         * Handle checksum offload
         *
         * We use the parser results to check for good checksums.
         * If Bit 23 is set, then we know the both the L3 
         * and L4 checksum were checked and were correct.
         *
         * If Bit 23 is clear and Bit 22 is set, we know only L3
         * checksum were checked and were correct.
         */

        if (pDrvCtrl->gemCaps.cap_enabled & IFCAP_RXCSUM)
        {
        if (pDesc->bdSts & RXBUF_STAT_CHECKSUM_BIT1)
            {
            pMblk->m_pkthdr.csum_flags |= CSUM_IP_CHECKED;
            pMblk->m_pkthdr.csum_flags |= CSUM_IP_VALID;
            pMblk->m_pkthdr.csum_flags |= CSUM_DATA_VALID|CSUM_PSEUDO_HDR;
            pMblk->m_pkthdr.csum_data = 0xffff;
            }
        else if (pDesc->bdSts & RXBUF_STAT_CHECKSUM_BIT0)
            {
            pMblk->m_pkthdr.csum_flags |= CSUM_IP_CHECKED;
            pMblk->m_pkthdr.csum_flags |= CSUM_IP_VALID;
            }
        }
        pMblk->m_len = pMblk->m_pkthdr.len = rxLen;
        pMblk->m_flags = M_PKTHDR | M_EXT;

        (void)cacheInvalidate (DATA_CACHE, pNewMblk->m_data, pNewMblk->m_len);

        pDesc->bdSts = 0;
        pDesc->bdAddr = (mtod (pNewMblk, UINT32) | RXBUF_ADD_OWNED);

        if(pDrvCtrl->gemRxIdx == (GEM_RX_DESC_CNT-1))
            pDesc->bdAddr |= RXBUF_ADD_WRAP;
        pDesc->bdAddr &= ~RXBUF_ADD_OWNED;

        /* Bump stats counters */

        pDrvCtrl->gemInOctets += pMblk->m_len;
        if (pDesc->bdSts & RXBUF_STAT_BCAST)
            pDrvCtrl->gemInBcasts++;
        else if (pDesc->bdSts & RXBUF_STAT_MULTI)
            pDrvCtrl->gemInMcasts++;
        else if (pDesc->bdSts & RXBUF_STAT_LOC1)
            pDrvCtrl->gemInUcasts++;

        GEM_INC_DESC(pDrvCtrl->gemRxIdx, GEM_RX_DESC_CNT);
        pDesc = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];

        loopCounter--;

        (void)cacheInvalidate (DATA_CACHE, pMblk->m_data, pMblk->m_len);

        /* Give the packet to the stack. */

        END_RCV_RTN_CALL (&pDrvCtrl->gemEndObj, pMblk);
        }

    if (loopCounter == 0)
        {
        jobQueuePost (pDrvCtrl->gemJobQueue, &pDrvCtrl->gemRxJob);
        return;
        }

    /*
     * Some RX error conditions will cause the receiver to
     * stop generating RX interrupts. In order to recover
     * from this, we have to stop and restart the receiver.
     * This may be the result of one of the known problems
     * for the GEM ethernet docmented in the Xilinx Zynq
     * manual.
     */

    if (pDrvCtrl->gemRxStall == TRUE)
        {
        pDrvCtrl->gemRxStall = FALSE;

        ftGemRxReset (pDrvCtrl);
        }

    vxAtomicSet (&pDrvCtrl->gemRxPending, FALSE);

    /* Enable rx interrupt */

    if (pDrvCtrl->gemPolling == FALSE)
        {
        CSR_WRITE_4 (pDev, GEM_IER, GEM_INT_RX);
        }

    /*
     * In some situations, the receive complete interrupt is not generated even
     * if the FRAMERX bit in rx_status register is set and the corresponding
     * interrupt is unmasked. The frame received during interrupt is masked
     * would not generate interrupt until the next frame comes.
     * The following code detects such situation, and post the RX job again to
     * handle the received frames in time.
     */

    if ((CSR_READ_4(pDev, GEM_RSR) & GEM_RSR_REC) && 
         vxAtomic32Cas (&pDrvCtrl->gemRxPending, FALSE, TRUE))
        {
        (void)jobQueuePost (pDrvCtrl->gemJobQueue, &pDrvCtrl->gemRxJob);
        }

    }

/*******************************************************************************
*
* ftGemEndTxHandle - process TX completion events
*
* This function is scheduled by the ISR to run in the context of tNetTask
* whenever an TX interrupt is received. It runs through all of the
* TX register pairs and checks the TX status to see how many have
* completed. For each completed transmission, the associated TX mBlk
* is released, and the outbound packet stats are updated.
*
* In the event that a TX underrun error is detected, the TX FIFO
* threshold is increased. This will continue until the maximum TX
* FIFO threshold is reached.
*
* If the transmitter has stalled, this routine will also call muxTxRestart()
* to drain any packets that may be waiting in the protocol send queues,
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemEndTxHandle
    (
    void * pArg
    )
    {
    QJOB *          pJob;
    GEM_DRV_CTRL *  pDrvCtrl;
    VXB_DEVICE_ID   pDev;
    BOOL            restart = FALSE;
    BOOL            txBdReset = FALSE;
    GEM_DESC *      pDesc;
    M_BLK_ID        pMblk;
    UINT32          regval;
    BOOL            isFrag = FALSE;

    pJob     = pArg;
    pDrvCtrl = member_to_object (pJob, GEM_DRV_CTRL, gemTxJob);
    pDev     = pDrvCtrl->gemDev;
    
    END_TX_SEM_TAKE (&pDrvCtrl->gemEndObj, WAIT_FOREVER);

    /* Clear tx status register */

    regval = CSR_READ_4(pDev, GEM_TSR);
    CSR_WRITE_4 (pDev, GEM_TSR, regval);

#ifdef GEM_END_DEBUG    
    if (regval & GEM_TSR_COL)
        {
        GEM_DEBUG ("GemTxHandlle: TSR_COL [0x%x]\n", regval,0,0,0,0,0);
        }

    if (regval & GEM_TSR_UND)
        {
        GEM_DEBUG ("GemTxHandlle: TSR_UND [0x%x]\n", regval,0,0,0,0,0);
        }

    if (regval & GEM_TSR_RLE)
        {
        GEM_DEBUG ("GemTxHandlle: TSR_RLE [0x%x]\n", regval,0,0,0,0,0);
        }

    if (regval & GEM_TSR_LCOL)
        {
        GEM_DEBUG ("GemTxHandlle: TSR_LCOL [0x%x]\n", regval,0,0,0,0,0);
        }
#endif /* GEM_END_DEBUG */

    if (regval & (GEM_TSR_UND | GEM_TSR_BNQ | GEM_TSR_HRSP) )
        {
        txBdReset = TRUE;
        }

    while (pDrvCtrl->gemTxFree < GEM_TX_DESC_CNT)
        {
        pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxCons];

         if (!isFrag)
            {
            /* Current desc must a the first buffer of packet */

            if (pDesc->bdSts & TXBUF_STAT_USED)
                isFrag = TRUE;
            else
                {
                /*
                 * If current desc is the first buffer of a packet and DMA
                 * has not completed yet, but we receive the tx interrupts
                 * (this case occured in a very rare chance), there must be some
                 * hareware error, we just trig the TX DMA again and break out
                 * here, then wait next tx done interrupt.
                 */

                if ((regval & GEM_TSR_OVR) != 0)
                    {
                    CSR_SETBIT_4(pDev, GEM_CTL, GEM_CTL_TSTART);
                    }
                 
                break;
                }
            }

        if ((pDesc->bdSts & TXBUF_STAT_LAST_BUFF) && isFrag)
            {
            /*
             * Current desc is the last buffer of a packet, so next desc must
             * be the first buffer of other packet
             */
             
            isFrag = FALSE;
            }

        if (pDesc->bdSts & (TX_UNDERRUN))
            pDrvCtrl->gemOutErrors++;

        pDesc->bdSts |= TXBUF_STAT_USED;

        pMblk = pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons];

        /*
         * MBLK may has multi section data in a chain, each will consume
         * a desc, the point of mblk will only be saved in gemTxMblk
         * which has the same index with the first desc.
         */

        if (pMblk != NULL)
            {
            /* Update statistics data */

            pDrvCtrl->gemOutOctets += pMblk->m_pkthdr.len;
            if ((UINT8)pMblk->m_data[0] == 0xFF)
                pDrvCtrl->gemOutBcasts++;
            else if ((UINT8)pMblk->m_data[0] & 0x1)
                pDrvCtrl->gemOutMcasts++;
            else
                pDrvCtrl->gemOutUcasts++;

            /*
             * It's driver's responsibility to free the tx MBLK,
             * here is the right point to make that happen.
             */
            
            endPoolTupleFree (pMblk);
            
            /* Set corresponding point to NULL*/

            pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons] = NULL;
            }

        /* Increase the tx free desc count */

        pDrvCtrl->gemTxFree++;

        /* Move to next desc */

        GEM_INC_DESC (pDrvCtrl->gemTxCons, GEM_TX_DESC_CNT);

        /*
         * We released at least one descriptor,if the transmit
         * channel is stalled, unstall it.
         */

        if (pDrvCtrl->gemTxStall == TRUE)
            {
            pDrvCtrl->gemTxStall = FALSE;
            restart = TRUE;
            }
        }
        

    /*
     * When detecting "Hresp not OK", "Transmit underrun",
     * "Transmit buffer exhausted" error conditions we should
     * re-initialize the tx buffer descriptors.
     */

    if (txBdReset == TRUE)
        {
        ftGemTxReset (pDrvCtrl);
        }
        
    END_TX_SEM_GIVE (&pDrvCtrl->gemEndObj);

    vxAtomicSet (&pDrvCtrl->gemTxPending, FALSE);

    if (restart == TRUE)
        muxTxRestart (pDrvCtrl);

    /* enable the tx interrupt again */

    if (pDrvCtrl->gemPolling == FALSE)
        {
        CSR_WRITE_4 (pDev, GEM_IER, GEM_INT_TX);
        }
    }

/*******************************************************************************
*
* ftGemEndEncap - encapsulate an outbound packet in the TX DMA ring
*
* This function sets up a descriptor for a packet transmit operation.
* With the ft ethrnet controller, the TX DMA ring consists of
* descriptors that each describe a single packet fragment. We consume
* as many descriptors as there are mBlks in the outgoing packet, unless
* the chain is too long. The length is limited by the number of DMA
* segments we want to allow in a given DMA map. If there are too many
* segments, this routine will fail, and the caller must coalesce the
* data into fewer buffers and try again.
*
* This routine will also fail if there aren't enough free descriptors
* available in the ring, in which case the caller must defer the
* transmission until more descriptors are completed by the chip.
*
* RETURNS: ENOSPC if there are too many fragments in the packet, EAGAIN
* if the DMA ring is full, otherwise OK.
*
* ERRNO: N/A
*/

LOCAL int ftGemEndEncap
    (
    GEM_DRV_CTRL *  pDrvCtrl,
    M_BLK_ID        pMblk
    )
    {
    VXB_DEVICE_ID   pDev;
    GEM_DESC *      pDesc = NULL;
    UINT32          firstIdx;
    M_BLK_ID        pCurr;
    int             nFrags = 0;

    pDev     = pDrvCtrl->gemDev;
    firstIdx = pDrvCtrl->gemTxProd;

    if (pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxProd] != NULL)
        {
        GEM_DEBUG ("gemTxMblk[%d] is not NULL before programmed\n",
                   pDrvCtrl->gemTxProd,2,3,4,5,6);

        return (EAGAIN);
        }

    for (pCurr = pMblk; pCurr != NULL; pCurr = pCurr->m_next)
        {
        if (pCurr->m_len != 0)
            nFrags++;
        }

    if (nFrags > pDrvCtrl->gemTxFree)
        {
        GEM_DEBUG ("no engough tx desc, nFrags[%d], gemTxFree[%d]\n",
                   nFrags,pDrvCtrl->gemTxFree,3,4,5,6);

        return (ENOSPC);
        }

    for (pCurr = pMblk; pCurr != NULL; pCurr = pCurr->m_next)
        {
        if (pCurr->m_len != 0)
            {
            pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
            pDesc->bdAddr = mtod(pCurr, UINT32);
            pDesc->bdSts &= (TXBUF_STAT_WRAP | TXBUF_STAT_USED);
            pDesc->bdSts |= (pCurr->m_len & TXBUF_STAT_LEN_MASK);

            if ((pDrvCtrl->ifCached != NULL) &&
                (pDrvCtrl->ifCached(pCurr->m_data)))
                (void)cacheFlush (DATA_CACHE, pCurr->m_data, pCurr->m_len);

            /* Clear used flag of all BDs except the first one */

            if (pDrvCtrl->gemTxProd != firstIdx)
                pDesc->bdSts &= ~TXBUF_STAT_USED;

            pDrvCtrl->gemTxFree--;
            GEM_INC_DESC(pDrvCtrl->gemTxProd, GEM_TX_DESC_CNT);
            }
        }

    if (pDesc != NULL)
        pDesc->bdSts |= TXBUF_STAT_LAST_BUFF;

    /* Save the mBlk for later. */

    pDrvCtrl->gemTxMblk[firstIdx] = pMblk;

    /* Clear used flag of first BD */

    pDesc = &pDrvCtrl->gemTxDescMem[firstIdx];
    pDesc->bdSts &= ~TXBUF_STAT_USED;

    CACHE_PIPE_FLUSH();

    /* Transfer descriptors to the chip. */

    CSR_SETBIT_4(pDev, GEM_CTL, GEM_CTL_TSTART);

    return (OK);
    }

/*******************************************************************************
*
* ftGemEndSend - transmit a packet
*
* This function transmits the packet specified in <pMblk>.
*
* RETURNS: OK, ERROR, or END_ERR_BLOCK.
*
* ERRNO: N/A
*/

LOCAL int ftGemEndSend
    (
    END_OBJ *   pEnd,
    M_BLK_ID    pMblk
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;
    VXB_DEVICE_ID  pDev;
    M_BLK_ID       pTmp;
    int            rval;

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;

    if (pDrvCtrl->gemPolling == TRUE)
        {
        endPoolTupleFree (pMblk);
        return (ERROR);
        }

    pDev = pDrvCtrl->gemDev;

    END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);

    if (!pDrvCtrl->gemTxFree || !(pDrvCtrl->gemCurStatus & IFM_ACTIVE))
        goto blocked;

    /*
     * First, try to do an in-place transmission, using
     * gather-write DMA.
     */

    rval = ftGemEndEncap (pDrvCtrl, pMblk);

     /*
     * If ftGemEndEncap returns ENOSPC, it means it ran out of TX descriptors
     * and couldn't encapsulate the whole packet fragment chain.  In that case,
     * we need to coalesce everything into a single buffer and try again.  If
     * any other error is returned, then something went wrong, and we have to
     * abort the transmission entirely.
     */

    if (rval == ENOSPC)
        {
        if ((pTmp = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool)) == NULL)
            goto blocked;

        pTmp->m_len = pTmp->m_pkthdr.len =
            netMblkToBufCopy(pMblk, mtod(pTmp, char *), NULL);
        pTmp->m_flags = pMblk->m_flags;

        /* try transmission again, should succeed this time */

        rval = ftGemEndEncap (pDrvCtrl, pTmp);
        if (rval == OK)
            endPoolTupleFree (pMblk);
        else
            endPoolTupleFree (pTmp);
        }

    if (rval != OK)
        goto blocked;

    END_TX_SEM_GIVE (pEnd);
    return (OK);

blocked:
    pDrvCtrl->gemTxStall = TRUE;
    END_TX_SEM_GIVE (pEnd);

    return (END_ERR_BLOCK);
    }

/*******************************************************************************
*
* ftGemEndPollSend - polled mode transmit routine
*
* This function is similar to the ftGemEndSend() routine shown above, except
* it performs transmissions synchronously with interrupts disabled. After
* the transmission is initiated, the routine will poll the state of the
* TX status register associated with the current slot until transmission
* completed. If transmission times out, this routine will return ERROR.
*
* RETURNS: OK, EAGAIN, or ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS ftGemEndPollSend
    (
    END_OBJ *   pEnd,
    M_BLK_ID    pMblk
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;
    VXB_DEVICE_ID  pDev;
    GEM_DESC *     pDesc;
    M_BLK_ID       pTmp;
    int            len, i;
    UINT32         regVal;
    BOOL           txBdReset = FALSE;
    int            retVal = OK;

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;

    if (pDrvCtrl->gemPolling == FALSE)
        {
        return (ERROR);
        }
    
    pDev = pDrvCtrl->gemDev;

    /* Read and Clear TX status register */

    regVal = CSR_READ_4(pDev, GEM_TSR);
    CSR_WRITE_4 (pDev, GEM_TSR, regVal);

    /*
     * When detecting "Hresp not OK", "Transmit underrun",
     * "Transmit buffer exhausted" error conditions we should
     * re-initialize the buffer descriptors.
     */

    if (regVal & (GEM_TSR_UND | GEM_TSR_BNQ | GEM_TSR_HRSP) )
        {
        txBdReset = TRUE;
        }
      
    (void)END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);
    
    pTmp = pDrvCtrl->gemPollBuf;
   
    /* Copy the whole buffer(which maybe a buffer chain) into a single buffer */

    len = netMblkToBufCopy (pMblk, mtod(pTmp, char *), NULL);
    pTmp->m_len = pTmp->m_pkthdr.len = len;
    pTmp->m_flags = pMblk->m_flags;

    /* Program the single buffer into desc, this will consume only one desc */

    if (ftGemEndEncap (pDrvCtrl, pTmp) != OK)
        {
        GEM_DEBUG ("ftGemEndEncap  != OK\n",0,0,0,0,0,0);

        retVal = EAGAIN;
        
        goto txPollExit;
        }

    pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxCons];

    /* Wait some time, and then check if the packet is sent out */

    for (i = 0; i < GEM_TIMEOUT; i++)
        {
        /*
         * TXBUF_STAT_USED bit in tx desc means the packet is
         * sent out successfully.
         */
         
        if (pDesc->bdSts & TXBUF_STAT_USED)
            break;
        }

    if (i == GEM_TIMEOUT)
        {
        pDrvCtrl->gemOutErrors++;
        
        /*
         * Accoring test result, if timeout and send packet out failed
         * here, there is a great possibility that the transmitter module
         * enter an abnormal status and hung, it can't enter the normal
         * status again until reset the tx module.
         */

        txBdReset = TRUE;
        retVal = EAGAIN;
        
        goto txPollExit;
        }

    /*
     * Set the gemTxMblk to NULL, it's up caller's responsibilit
     * to free the tx mblk in poll mode.
     */
     
    pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons] = NULL;

    /* Increase the gemTxFree which is decreased by ftGemEndEncap */

    pDrvCtrl->gemTxFree++;
    
    /* Move the tx index to next point */
    
    GEM_INC_DESC(pDrvCtrl->gemTxCons, GEM_TX_DESC_CNT);

    /* Update statistics data */
    
    pDrvCtrl->gemOutOctets += pMblk->m_pkthdr.len;
    if ((UINT8)pMblk->m_data[0] == 0xFF)
        pDrvCtrl->gemOutBcasts++;
    else if ((UINT8)pMblk->m_data[0] & 0x1)
        pDrvCtrl->gemOutMcasts++;
    else
        pDrvCtrl->gemOutUcasts++;

    retVal = OK;  
    
txPollExit:

    (void)END_TX_SEM_GIVE (pEnd);  
    
    if (txBdReset == TRUE)
        {
        ftGemTxReset (pDrvCtrl);
        }

    return retVal;
    }

/*******************************************************************************
*
* ftGemEndPollReceive - polled mode receive routine
*
* This function receive a packet in polled mode, with interrupts disabled.
* It's similar in operation to the ftGemEndRxHandle() routine, except it
* doesn't process more than one packet at a time and does not load out
* buffers. Instead, the caller supplied an mBlk tuple into which this
* function will place the received packet.
*
* If no packet is available, this routine will return EAGAIN. If the
* supplied mBlk is too small to contain the received frame, the routine
* will return ERROR.
*
* RETURNS: OK, EAGAIN, or ERROR
*
* ERRNO: N/A
*/

LOCAL int ftGemEndPollReceive
    (
    END_OBJ *   pEnd,
    M_BLK_ID    pMblk
    )
    {
    GEM_DRV_CTRL * pDrvCtrl;
    VXB_DEVICE_ID  pDev;
    GEM_DESC *     pDesc;
    M_BLK_ID       pPkt;
    int            rxLen;
    UINT32         regVal;
    BOOL           rxBdReset = FALSE;
    int            retVal = OK;

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
    pDev = pDrvCtrl->gemDev;
    
    if (pDrvCtrl->gemPolling == FALSE)
        return (ERROR);

    if (!(pMblk->m_flags & M_EXT))
        return (ERROR);

    /* Read and Clear RX status register */

    regVal = CSR_READ_4(pDev, GEM_RSR);
    CSR_WRITE_4 (pDev, GEM_RSR, regVal);

    /*
     * Some RX error conditions will cause the receiver to
     * stop. In order to recover from this, we have to stop
     * and restart the receiver.
     * This may be the result of one of the known problems
     * for the GEM ethernet docmented in the Xilinx Zynq
     * manual.
     */

    if (regVal & ~(GEM_RSR_REC|GEM_RSR_BNA|GEM_RSR_OVR))
        {
        GEM_DEBUG("regVal = 0x%08x, reset rx module\n",regVal,0,0,0,0,0);

        rxBdReset = TRUE;
        }    
    
    pDesc = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];
    
    /* check for an incoming packet */

    if ((pDesc->bdAddr & RXBUF_ADD_OWNED) != RXBUF_ADD_OWNED)
        {
        retVal = EAGAIN;
        goto rxPollExit;
        }
     
    rxLen = pDesc->bdSts & RXBUF_STAT_LEN_MASK;
    pPkt = pDrvCtrl->gemRxMblk[pDrvCtrl->gemRxIdx];

    /* Bump stats counters */

    pDrvCtrl->gemInOctets += pMblk->m_len;
    if (pDesc->bdSts & RXBUF_STAT_BCAST)
        pDrvCtrl->gemInBcasts++;
    else if (pDesc->bdSts & RXBUF_STAT_MULTI)
        pDrvCtrl->gemInMcasts++;
    else if (pDesc->bdSts & RXBUF_STAT_LOC1)
        pDrvCtrl->gemInUcasts++;

    pMblk->m_flags |= M_PKTHDR;
    pMblk->m_len = pMblk->m_pkthdr.len = rxLen;

    (void)cacheInvalidate (DATA_CACHE, pPkt->m_data, rxLen);

    bcopy (mtod(pPkt, char *), mtod(pMblk, char *), (size_t)rxLen);

    pDesc->bdAddr &= ~RXBUF_ADD_OWNED;
    pDesc->bdSts = 0;

    /* Advance to the next descriptor */

    GEM_INC_DESC(pDrvCtrl->gemRxIdx, GEM_RX_DESC_CNT);

    retVal = OK;

rxPollExit:

    if(rxBdReset == TRUE)    
        {
        ftGemRxReset(pDrvCtrl);
        }

    return (retVal);
    }

/*******************************************************************************
*
* ftGemRxReset - reset the receive module
*
* This function reset the receive module. Some RX error conditions
* will cause the receiver to stop. In order to recover from this,
* we have to stop and restart the receiver. This may be the result
* of one of the known problems for the GEM ethernet docmented in
* the Xilinx Zynq manual.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemRxReset
    (
    GEM_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID  pDev;    
    GEM_DESC     * pDesc;
    int            i;

    pDev = pDrvCtrl->gemDev;

    (void)semTake (pDrvCtrl->gemDevSem, WAIT_FOREVER);  

    /* Disable receiver. */

    CSR_CLRBIT_4(pDev, GEM_CTL, GEM_CTL_RE);

    /* Reset descriptors */

    for (i = 0; i < GEM_RX_DESC_CNT; i++)
        {
        pDesc = &pDrvCtrl->gemRxDescMem[i];
        pDesc->bdAddr &= ~RXBUF_ADD_OWNED;
        pDesc->bdSts = 0;
        }

    pDesc = &pDrvCtrl->gemRxDescMem[GEM_RX_DESC_CNT - 1];
    pDesc->bdAddr |= RXBUF_ADD_WRAP;
    
    CACHE_PIPE_FLUSH();

    /* Reload RX index pointer */

    pDrvCtrl->gemRxIdx = 0;

    /* Flush whole RX desc area */

    (void)cacheFlush (DATA_CACHE, pDrvCtrl->gemRxDescMem,
                      sizeof(GEM_DESC) * GEM_RX_DESC_CNT);

    /* Reload RX DMA ring pointer */

    CSR_WRITE_4 (pDev, GEM_RBQP,(UINT32)pDrvCtrl->gemRxDescMem);
    
    /* Re-enable the receiver */

    CSR_SETBIT_4(pDev, GEM_CTL, GEM_CTL_RE);

    (void)semGive (pDrvCtrl->gemDevSem);
    }

/*******************************************************************************
*
* ftGemTxReset - reset the transmitter module
*
* This function reset the transmitter module. Some TX error conditions
* will cause the transmitter to stop. In order to recover from this,
* we have to stop and restart the transmitter.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemTxReset
    (
    GEM_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID  pDev;   
    int            i;
    M_BLK_ID       pMblk;
    BOOL           pollBufFreeFlag = FALSE;

    pDev = pDrvCtrl->gemDev;

    (void)semTake (pDrvCtrl->gemDevSem, WAIT_FOREVER);
    (void)END_TX_SEM_TAKE (&pDrvCtrl->gemEndObj, WAIT_FOREVER);

    /* Disable transmitter. */

    CSR_CLRBIT_4(pDev, GEM_CTL, GEM_CTL_TE);

    /* Reset the transmit buffer pool */

    for (i = 0; i < GEM_TX_DESC_CNT; i++)
        {
        pDrvCtrl->gemTxDescMem[i].bdAddr = 0;
        pDrvCtrl->gemTxDescMem[i].bdSts = TXBUF_STAT_USED;
        }

    pDrvCtrl->gemTxDescMem[GEM_TX_DESC_CNT-1].bdSts |= TXBUF_STAT_WRAP;

    /* Reset Tx pointer */

    CSR_WRITE_4 (pDev, GEM_TBQP,(UINT32)pDrvCtrl->gemTxDescMem);

    for(i = 0; i < GEM_TX_DESC_CNT; i++)
        {
        pMblk = pDrvCtrl->gemTxMblk[i];

        if (pMblk != NULL)
            {
            if (pMblk == pDrvCtrl->gemPollBuf)
                {
                pollBufFreeFlag = TRUE;
                pDrvCtrl->gemPollBuf = NULL;
                }

            endPoolTupleFree (pMblk);
            pDrvCtrl->gemTxMblk[i] = NULL;
            }
        }

    /* Reload TX index pointer */

    pDrvCtrl->gemTxProd = 0;
    pDrvCtrl->gemTxCons = 0;
    pDrvCtrl->gemTxFree = GEM_TX_DESC_CNT;

    /*
     * When driver switched from poll mode to interrupt mode,
     * there is a chance that tx module hung which will lead
     * tx module reset, then mblk(gemPollBuf) saved in gemTxMblk
     * will be freed in above section, so we must re-allocate
     * it here again.
     */

    if (pollBufFreeFlag == TRUE)
        {
        pDrvCtrl->gemPollBuf = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);
        }

    /* Re-enable the transmitter. */

    CSR_SETBIT_4(pDev, GEM_CTL, GEM_CTL_TE);

    (void)END_TX_SEM_GIVE (&pDrvCtrl->gemEndObj);
    (void)semGive (pDrvCtrl->gemDevSem);
    }

/*******************************************************************************
*
* ftGemPhyAddrGet - probe phy address
*
* This function probe phy address from 0~31 accroding to standard.
*
* RETURNS: (0~31) phy address; 32 error address.
*
* ERRNO: N/A
*
* !NOTE: For some PHYs, such as RTL8211E, PHY address 0 is a broadcast from 
*       the MAC; each PHY device should respond. (RTL8211EG Datasheet.)
*
*       (If so, WE HOPE user set phy address in hwconf.c by "phyAddr" resource.
*        Because it will take lots of time to check all 32 addresses when boot.)
*       
*/

LOCAL int ftGemPhyAddrGet (VXB_DEVICE_ID   pDev)
{
    int i = 0;
    UINT16 phyId1;
    UINT16 phyId2;
    BOOL flag = FALSE;
    
    for (i = 0; i < 32; i++)
    {
        ftGemPhyRead (pDev, i, MII_PHY_ID1_REG, &phyId1);  /*read ID1 from phy reg2*/
        ftGemPhyRead (pDev, i, MII_PHY_ID2_REG, &phyId2);  /*read ID2 from phy reg3*/
        
        if((phyId2 & 0xFFFF)  !=  0xFFFF)
        {
            if((0 == i) && (0x1C == phyId1) && ((phyId2>>10) == 0x32))
            {   
                /* skip RTL8211 broadcase address */
                continue;
            }
            if((0 == i) && (0x00 == phyId1) && (phyId2 == 0x11a))
            {   
                /* skip YT8521 broadcase address */
                continue;
            }
            
            flag = TRUE;
            break; /* get phy addr */
        }   
    }

    return i;
}

STATUS ftGemPhyAddrShow (int port, int verbose)
{
    VXB_DEVICE_ID   pDev;
    GEM_DRV_CTRL * pDrvCtrl;
    int i, j;
    UINT16 phyId1;
    UINT16 phyId2;
    STATUS st;

    pDev = vxbInstByNameFind("gem", port);
    if (NULL == pDev)
    {
        printf("gem%d is invalid! \r\n", port);
        return ERROR;
    }
    pDrvCtrl = pDev->pDrvCtrl;
    printf("gem%d:  pDrvCtrl->gemMiiPhyAddr = %d\r\n", port, pDrvCtrl->gemMiiPhyAddr);

    if(1 == verbose)
    {
        printf("---------------gem%d probe phy addr-------------- \r\n", port);
        for(j=0; j<32; j++)
        {
            phyId1 = phyId2 = 0;
            st = ftGemPhyRead(pDev,j,MII_PHY_ID1_REG, &phyId1);
            st |= ftGemPhyRead(pDev,j,MII_PHY_ID2_REG, &phyId2);
            if(OK != st)
            {
                printf("failed read phy id! j=%d \r\n",j);
            }
            printf("  phyAddr[%02d] IDs: 0x%04X : 0x%04X \r\n", j, phyId1, phyId2);
        }
        printf(" \r\n");
   
    }

    return OK;
}


/*******************************************************************************
*
* ftGemE2kSelClk - config mac clocks
*
* This function sets E2K mac clocks by interface type.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftGemE2kSelClk(VXB_DEVICE_ID pDev)
{
    GEM_DRV_CTRL *pDrvCtrl = pDev->pDrvCtrl;
	UINT32 hs_ctrl = 0;
	int speed = 0;

	if(pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_USXGMII)
	{
		if(pDrvCtrl->speed == SPEED_10000)
		{
			speed = GEM_SPEED_10000;
			CSR_WRITE_4 (pDev, GEM_SRC_SEL_LN, 0x1); /*0x1c04*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL0_LN, 0x4); /*0x1c08*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL1_LN, 0x1); /*0x1c0c*/
			CSR_WRITE_4 (pDev, GEM_PMA_XCVR_POWER_STATE, 0x1); /*0x1c10*/
		}
	}
	else if(pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_5GBASER)
	{
		if (pDrvCtrl->speed == SPEED_5000) {
			CSR_WRITE_4 (pDev, GEM_SRC_SEL_LN, 0x1); /*0x1c04*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL0_LN, 0x8); /*0x1c08*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL1_LN, 0x2); /*0x1c0c*/
			CSR_WRITE_4 (pDev, GEM_PMA_XCVR_POWER_STATE, 0x0); /*0x1c10*/
			speed = GEM_SPEED_5000;
		}
	}
	else if(pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_2500BASEX)
	{
		if(pDrvCtrl->speed == SPEED_2500)
		{
			speed = GEM_SPEED_2500;
			CSR_WRITE_4 (pDev, GEM_DIV_SEL0_LN, 0x1); /*0x1c08*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL1_LN, 0x2); /*0x1c0c*/
			CSR_WRITE_4 (pDev, GEM_PMA_XCVR_POWER_STATE, 0x1); /*0x1c10*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL0, 0x0); /*0x1c20*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL1, 0x1); /*0x1c24*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL2, 0x1); /*0x1c28*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3, 0x1); /*0x1c2c*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL0, 0x1); /*0x1c30*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL1, 0x0); /*0x1c34*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3_0, 0x0); /*0x1c70*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL4_0, 0x0); /*0x1c74*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL3_0, 0x0); /*0x1c78*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL4_0, 0x0); /*0x1c7c*/
		}
     }
     else if(pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_SGMII)
     {
		if(pDrvCtrl->speed == SPEED_1000)
		{
			speed = GEM_SPEED_1000;
			CSR_WRITE_4 (pDev, GEM_DIV_SEL0_LN, 0x4); /*0x1c08*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL1_LN, 0x8); /*0x1c0c*/
			CSR_WRITE_4 (pDev, GEM_PMA_XCVR_POWER_STATE, 0x1); /*0x1c10*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL0, 0x0); /*0x1c20*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL1, 0x0); /*0x1c24*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL2, 0x0); /*0x1c28*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3, 0x1); /*0x1c2c*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL0, 0x1); /*0x1c30*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL1, 0x0); /*0x1c34*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3_0, 0x0); /*0x1c70*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL4_0, 0x0); /*0x1c74*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL3_0, 0x0); /*0x1c78*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL4_0, 0x0); /*0x1c7c*/

		}
		else if(pDrvCtrl->speed == SPEED_100 || pDrvCtrl->speed == SPEED_10)
		{
			speed = GEM_SPEED_100;
			CSR_WRITE_4 (pDev, GEM_DIV_SEL0_LN, 0x4); /*0x1c08*/
			CSR_WRITE_4 (pDev, GEM_DIV_SEL1_LN, 0x8); /*0x1c0c*/
			CSR_WRITE_4 (pDev, GEM_PMA_XCVR_POWER_STATE, 0x1); /*0x1c10*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL0, 0x0); /*0x1c20*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL1, 0x0); /*0x1c24*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL2, 0x1); /*0x1c28*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3, 0x1); /*0x1c2c*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL0, 0x1); /*0x1c30*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL1, 0x0); /*0x1c34*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3_0, 0x1); /*0x1c70*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL4_0, 0x0); /*0x1c74*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL3_0, 0x0); /*0x1c78*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL4_0, 0x1); /*0x1c7c*/
		}
	}
	else if(pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_RGMII)
	{
		if (pDrvCtrl->speed == SPEED_1000)
		{
			speed = GEM_SPEED_1000;
			CSR_WRITE_4 (pDev, GEM_MII_SELECT, 0x1); /*0x1c18*/
			CSR_WRITE_4 (pDev, GEM_SEL_MII_ON_RGMII, 0x0); /*0x1c1c*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL0, 0x0); /*0x1c20*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL1, 0x1); /*0x1c24*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL2, 0x0); /*0x1c28*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3, 0x0); /*0x1c2c*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL0, 0x0); /*0x1c30*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL1, 0x1); /*0x1c34*/
			CSR_WRITE_4 (pDev, GEM_CLK_250M_DIV10_DIV100_SEL, 0x0); /*0x1c38*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL5, 0x1); /*0x1c48*/
			CSR_WRITE_4 (pDev, GEM_RGMII_TX_CLK_SEL0, 0x1); /*0x1c80*/
			CSR_WRITE_4 (pDev, GEM_RGMII_TX_CLK_SEL1, 0x0); /*0x1c84*/
		}
		else if (pDrvCtrl->speed == SPEED_100)
		{
			CSR_WRITE_4 (pDev, GEM_MII_SELECT, 0x1); /*0x1c18*/
			CSR_WRITE_4 (pDev, GEM_SEL_MII_ON_RGMII, 0x0); /*0x1c1c*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL0, 0x0); /*0x1c20*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL1, 0x1); /*0x1c24*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL2, 0x0); /*0x1c28*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3, 0x0); /*0x1c2c*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL0, 0x0); /*0x1c30*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL1, 0x1); /*0x1c34*/
			CSR_WRITE_4 (pDev, GEM_CLK_250M_DIV10_DIV100_SEL, 0x0); /*0x1c38*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL5, 0x1); /*0x1c48*/
			CSR_WRITE_4 (pDev, GEM_RGMII_TX_CLK_SEL0, 0x0); /*0x1c80*/
			CSR_WRITE_4 (pDev, GEM_RGMII_TX_CLK_SEL1, 0x0); /*0x1c84*/
			speed = GEM_SPEED_100;
		}
		else
		{
			CSR_WRITE_4 (pDev, GEM_MII_SELECT, 0x1); /*0x1c18*/
			CSR_WRITE_4 (pDev, GEM_SEL_MII_ON_RGMII, 0x0); /*0x1c1c*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL0, 0x0); /*0x1c20*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL1, 0x1); /*0x1c24*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL2, 0x0); /*0x1c28*/
			CSR_WRITE_4 (pDev, GEM_TX_CLK_SEL3, 0x0); /*0x1c2c*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL0, 0x0); /*0x1c30*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL1, 0x1); /*0x1c34*/
			CSR_WRITE_4 (pDev, GEM_CLK_250M_DIV10_DIV100_SEL, 0x1); /*0x1c38*/
			CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL5, 0x1); /*0x1c48*/
			CSR_WRITE_4 (pDev, GEM_RGMII_TX_CLK_SEL0, 0x0); /*0x1c80*/
			CSR_WRITE_4 (pDev, GEM_RGMII_TX_CLK_SEL1, 0x0); /*0x1c84*/
			speed = GEM_SPEED_100;
		}
	}
	else if(pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_RMII)
	{
		speed = GEM_SPEED_100;
		CSR_WRITE_4 (pDev, GEM_RX_CLK_SEL5, 0x1); /*0x1c48*/
	}

	/*GEM_HSMAC(0x0050) provide rate to the external*/
	hs_ctrl = CSR_READ_4(pDev, GEM_HSMAC);
	hs_ctrl = GEM_BFINS(HSMACSPEED, speed, hs_ctrl);
	CSR_WRITE_4 (pDev, GEM_HSMAC, hs_ctrl);

	return ;
}


LOCAL int ftGemInitInterface(VXB_DEVICE_ID pDev)
{
    GEM_DRV_CTRL *pDrvCtrl = pDev->pDrvCtrl;
	UINT32 ctrl = 0;

    GEM_DEBUG("%s,%d: phy interface %d; speed %d\r\n", __func__, __LINE__, 
              pDrvCtrl->phyInterface, pDrvCtrl->speed,5,6);
    
	ftGemE2kSelClk(pDev);
	
	if (pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_XGMII)
	{
		ctrl = CSR_READ_4(pDev, GEM_CFG);
		ctrl &=~ GEM_BIT(PCSSEL);
		CSR_WRITE_4 (pDev, GEM_CFG, ctrl);

		/*CTL bit[31] enable_hs_mac*/
		ctrl = CSR_READ_4(pDev, GEM_CTL);
		ctrl |=  MACB_BIT(HSMAC);
		CSR_WRITE_4 (pDev, GEM_CTL, ctrl);
		pDrvCtrl->duplex = 1;
	}
	else if (pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_USXGMII)
	{
	
        /*GEM_DEBUG("%s,%d: setting USXGMII mode...\r\n", __func__, __LINE__, 
                  3, 4,5,6);*/

		ctrl = CSR_READ_4(pDev, GEM_CFG);
		ctrl |= GEM_BIT(PCSSEL);
		CSR_WRITE_4 (pDev, GEM_CFG, ctrl);

		/*CTL bit[31] enable_hs_mac*/
		ctrl = CSR_READ_4(pDev, GEM_CTL);
		ctrl |=  MACB_BIT(HSMAC);
		CSR_WRITE_4 (pDev, GEM_CTL, ctrl);

		ctrl = CSR_READ_4(pDev, GEM_USX_CONTROL);
		ctrl &= ~(GEM_BIT(TX_SCR_BYPASS) | GEM_BIT(RX_SCR_BYPASS));
		ctrl |= GEM_BIT(RX_SYNC_RESET); 
		if(pDrvCtrl->speed == SPEED_10000){
             ctrl = GEM_BFINS(SERDES_RATE, MACB_SERDES_RATE_10G, ctrl);
             ctrl = GEM_BFINS(USX_CTRL_SPEED, GEM_SPEED_10000, ctrl);
		}
		else if (pDrvCtrl->speed == SPEED_5000) {
		     ctrl = GEM_BFINS(SERDES_RATE, MACB_SERDES_RATE_5G, ctrl);
		     ctrl = GEM_BFINS(USX_CTRL_SPEED, GEM_SPEED_5000, ctrl);
	    }
				
		CSR_WRITE_4 (pDev, GEM_USX_CONTROL, ctrl);
		
		/*  enable rx and tx  */
		ctrl = CSR_READ_4(pDev, GEM_USX_CONTROL);
		ctrl &= ~(GEM_BIT(RX_SYNC_RESET));
		ctrl |= GEM_BIT(TX_EN);
		ctrl |= GEM_BIT(SIGNAL_OK);

		CSR_WRITE_4 (pDev, GEM_USX_CONTROL, ctrl);

		pDrvCtrl->duplex = 1;
	}
	else if (pDrvCtrl->phyInterface == PHY_INTERFACE_MODE_SGMII)
	{
		UINT32 pcsctrl;

		ctrl = CSR_READ_4(pDev, GEM_CFG);
		ctrl |= GEM_BIT(PCSSEL) | GEM_BIT(SGMIIEN);

		ctrl &= ~(MACB_BIT(SPD) | MACB_BIT(FD));
		ctrl &= ~ GEM_BIT(GBE);

		if (pDrvCtrl->duplex)
			ctrl |= MACB_BIT(FD);
		if (pDrvCtrl->speed == SPEED_100)
			ctrl |= MACB_BIT(SPD);
		if (pDrvCtrl->speed == SPEED_1000)
			ctrl |= GEM_BIT(GBE);

		if (pDrvCtrl->speed == SPEED_2500) {
			UINT32 network_ctrl;
			network_ctrl = CSR_READ_4(pDev, GEM_CTL);
		    network_ctrl |= MACB_BIT(2PT5G);
			CSR_WRITE_4 (pDev, GEM_CTL, network_ctrl);
			ctrl |= GEM_BIT(GBE);
			ctrl &=~ GEM_BIT(SGMIIEN);
		}

		CSR_WRITE_4 (pDev, GEM_CFG, ctrl);

		ctrl = CSR_READ_4(pDev, GEM_CTL);
		ctrl &=~  MACB_BIT(HSMAC);
		CSR_WRITE_4 (pDev, GEM_CTL, ctrl);

		if (pDrvCtrl->autoneg) {
		    pcsctrl = CSR_READ_4(pDev, GEM_PCSCTRL);
			pcsctrl |= GEM_BIT(AUTONEG);
            CSR_WRITE_4 (pDev, GEM_PCSCTRL, pcsctrl);
		} else {
			pcsctrl = CSR_READ_4(pDev, GEM_PCSCTRL);
			pcsctrl &=~ GEM_BIT(AUTONEG);
            CSR_WRITE_4 (pDev, GEM_PCSCTRL, pcsctrl);
		}
	}
	else
	{
		ctrl = CSR_READ_4(pDev, GEM_CFG);

		ctrl &=~ GEM_BIT(PCSSEL);
		ctrl &= ~(MACB_BIT(SPD) | MACB_BIT(FD));

			ctrl &= ~ GEM_BIT(GBE);

		if (pDrvCtrl->duplex)
			ctrl |= MACB_BIT(FD);
		if (pDrvCtrl->speed == SPEED_100)
			ctrl |= MACB_BIT(SPD);
		if (pDrvCtrl->speed == SPEED_1000)
			ctrl |= GEM_BIT(GBE);

		CSR_WRITE_4 (pDev, GEM_CFG, ctrl);

		ctrl = CSR_READ_4(pDev, GEM_CTL);
		ctrl &=~  MACB_BIT(HSMAC);
		CSR_WRITE_4 (pDev, GEM_CTL, ctrl);
	}

	return 0;
}

/*******************************************************************************
*
* ftGemIsZeroMac -  Check if the given MAC address is zero
*
* This routine check if the given MAC address is zero
*
* RETURNS: TRUE/FALSE
*
* ERRNO: N/A
*/
LOCAL BOOL ftGemIsZeroMac(UINT8 *gmacAddr)
{
    UINT8 i = 0;
    
	/* Check that the MAC is 00:00:00:00:00:00*/    
	for(i = 0; i < ETHER_ADDR_LEN; i++)
		{
		if(gmacAddr[i] != 0x0)
			return FALSE;
		}
    return TRUE;  
}
/*******************************************************************************
*
* ftGemIsmulticastMac -  Check if the given MAC address is multicast address
*
* This routine check if the given MAC address is multicast address
*
* RETURNS: TRUE/FALSE
*
* ERRNO: N/A
*/
LOCAL BOOL ftGemIsmulticastMac(UINT8 *gmacAddr)
{
    UINT8 i = 0;
    
	/* Check that the MAC is FF:FF:FF:FF:FF:FF.*/
    for(i = 0; i < ETHER_ADDR_LEN; i++)
		{
		if(gmacAddr[i] != 0xff)
			return FALSE;
		}
    return TRUE; 
}
/*******************************************************************************
*
* ftGemMacSet - get the GEM mac address
*
* This routine gets the GEM mac address.
*
* RETURNS: OK
*
* ERRNO: N/A
*/
LOCAL void ftGemMacSet(GEM_DRV_CTRL * pDrvCtrl)
{
    UINT32 macLo;
    UINT32 macHi;

    /* firmware has been set SA3L/H. get it. */
    macLo = CSR_READ_4(pDrvCtrl->gemDev, GEM_SA1L);
    macHi = CSR_READ_4(pDrvCtrl->gemDev, GEM_SA1H);
    pDrvCtrl->gemAddr[0] = macLo & 0xFF;
    pDrvCtrl->gemAddr[1] = (macLo >> 8) & 0xFF;
    pDrvCtrl->gemAddr[2] = (macLo >> 16) & 0xFF;
    pDrvCtrl->gemAddr[3] = (macLo >> 24) & 0xFF;
    pDrvCtrl->gemAddr[4] = macHi & 0xFF;
    pDrvCtrl->gemAddr[5] = (macHi >> 8) & 0xFF;
    
    if (((ftGemIsZeroMac(pDrvCtrl->gemAddr) == TRUE) 
		|| (ftGemIsmulticastMac(pDrvCtrl->gemAddr) == TRUE))
		&& (pDrvCtrl->gemAddrSet != NULL))
    {
    (*pDrvCtrl->gemAddrSet)(pDrvCtrl->gemAddr);
    }
}

/*******************************************************************************
*
* ftGemPhyRegShow - show the GEM phy register value
*
* This routine shows the GEM phy register value. Debug function.
*
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/
STATUS ftGemPhyRegShow (int port, UINT8 phyAddr, UINT8 regAddr)
{
    VXB_DEVICE_ID   pDev = NULL;
    UINT16          dataVal = 0;
    STATUS st;

    pDev = vxbInstByNameFind("gem", port);
    if (NULL == pDev)
    {
        printf("gem%d is invalid! \r\n", port);
        return ERROR;
    }
  
    st = ftGemPhyRead(pDev,phyAddr,regAddr,&dataVal);
    if (OK != st)
    {
        printf("gem%d: failed to show phy reg 0x%x! \r\n", port, regAddr);
        return ERROR;
    }

    printf("gem%d,phy%d: Reg[0x%x]=0x%04x \r\n", port, phyAddr,regAddr, dataVal);

    return OK;
}


/*******************************************************************************
*
* ftGemPhyRegModify - modify the GEM phy register value
*
* This routine modifies the GEM phy register value. Debug function.
*
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/
STATUS ftGemPhyRegModify (int port, UINT8 phyAddr, UINT8 regAddr, UINT16 dataVal)
{
    VXB_DEVICE_ID   pDev = NULL;
    STATUS st;

    pDev = vxbInstByNameFind("gem", port);
    if (NULL == pDev)
    {
        printf("gem%d is invalid! \r\n", port);
        return ERROR;
    }
  
    st = ftGemPhyWrite(pDev,phyAddr,regAddr,dataVal);
    if (OK != st)
    {
        printf("gem%d: failed to modify phy reg 0x%x! \r\n", port, regAddr);
        return ERROR;
    }

    return OK;
}
/*******************************************************************************
*
* ftGemMediaExceed1G - check if interface media speed exceeds 1G
*
* This routine checks if interface media speed exceeds 1G
*
* RETURNS: TRUE/FALSE
*/

LOCAL BOOL ftGemMediaExceed1G
    (
    GEM_DRV_CTRL *pDrvCtrl
    )
    {
        switch(pDrvCtrl->phyInterface)
        {
            case PHY_INTERFACE_MODE_USXGMII:
              return TRUE;
            case PHY_INTERFACE_MODE_5GBASER:
              return TRUE;
            case PHY_INTERFACE_MODE_2500BASEX:
              return TRUE;
            case PHY_INTERFACE_MODE_XGMII:
              return TRUE;
              
            default:
              return FALSE;
        }

        return FALSE;
    }
    
/*******************************************************************************
*
* ftGemProbeQueues - Get hardware queues numbers
*
* This routine probes and gets hardware queues numbers
*
* RETURNS: TRUE/FALSE
*/
LOCAL UINT32 ftGemProbeQueues(VXB_DEVICE_ID pDev)
{
	UINT32 hw_q;
	UINT32 queue_mask;
	UINT32 num_queues;

	queue_mask = 0x1;
	num_queues = 1;

	/* bit 0 is never set but queue 0 always exists */
	queue_mask = CSR_READ_4 (pDev, GEM_DCFG6) & 0xff;
	queue_mask |= 0x1;

	for (hw_q = 1; hw_q < MACB_MAX_QUEUES; ++hw_q)
	{
		if (queue_mask & (1 << hw_q))
			(num_queues)++;
	}

    return num_queues;
}
/* ftGemDmaBusWith - Get DMA bus width
 *
 * This routine get the DMA bus width field of the network configuration register that we
 * should program.  We find the width from decoding the design configuration
 * register to find the maximum supported data bus width.
 *
 * RETURNS: GEM_CFG[0x4] bit[22:21]
 */
LOCAL UINT32 ftGemDmaBusWith(VXB_DEVICE_ID pDev)
{

    /*GEM_DCFG1[0x280] bit[27:25]*/
	switch (GEM_BFEXT(DBWDEF, CSR_READ_4 (pDev, GEM_DCFG1)))
	{
	case 4:
		return GEM_BF(DBW, GEM_DBW128);
		
	case 2:
		return GEM_BF(DBW, GEM_DBW64);
		
	case 1:
	default:
		return GEM_BF(DBW, GEM_DBW32);
	}
}