/* vxbFtGemIeee1588.h - header file for GEM IEEE1588 device driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */



#ifndef __INCvxbFtGemIeee1588h
#define __INCvxbFtGemIeee1588h

/* includes */

#include <endLib.h>   /* END_LOGX */
#include <vxbIeee1588.h>
#include <sys/ioctl.h>

IMPORT int uartf(char *  fmt, ...);

/* defines */

#define GEM_IEEE1588_LOGMSG(x,a,b,c,d,e,f)  uartf(x,a,b,c,d,e,f)

#define GEM_IEEE1588_NAME                   "gemIEEE1588"
#define GEM_IEEE1588_NAME_LEN               31


/* Set logical CPU index for the core that services timer interrupts */
#define EIOCS1588CLKINTCPUINDEX _IOW('e', 76, UINT32)

/* Get the associated IEEE1588 device ID of an END device */
#define EIOCG1588DEV            _IOR('e', 77, struct vxbDev **)

#ifdef __cplusplus
extern "C" {
#endif

/* typedefs */

typedef struct gem_ieee1588_drv_ctrl
    {
    VXB_DEVICE_ID                      ieee1588Dev;   /* IEEE1588 device handler */
    UINT32                          ieee1588Unit;  /* IEEE1588 device unit ID */
    VXB_DEVICE_ID                      parentEndDev;  /* parent device(MAC) */

    IEEE1588_HARDWARE                hw; /* TODO */
    } GEM_IEEE1588_DRV_CTRL;

/* forward declarations */

void gem1588Tsc2Time
    (
    IEEE1588_HARDWARE * p1588DevInfo,
    UINT64              nsCounts,
    struct timespec *   ts
    );

int gem1588Ioctl
    (
    IEEE1588_HARDWARE * p1588DevInfo,
    int                 cmd,
    caddr_t             data
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFtGemIeee1588h */
