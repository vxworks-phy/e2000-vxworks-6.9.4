/* vxbFtGpio.c - Driver for GPIO controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <vsbConfig.h>
#include <intLib.h>
#include <errnoLib.h>
#include <errno.h>
#include <sioLib.h>
#include <ioLib.h>
#include <stdio.h>
#include <string.h>
#include <logLib.h>
#include <hwif/util/hwMemLib.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/vxbIntrCtlr.h>
#include <hwif/vxbus/vxBus.h>
#include <vxbFtGpio.h>

#define GPIO_DBG_ON
#ifdef  GPIO_DBG_ON
#define GPIO_DBG_IRQ         0x00000001
#define GPIO_DBG_RW          0x00000002
#define GPIO_DBG_ERR         0x00000004
#define GPIO_DBG_ALL         0xffffffff
#define GPIO_DBG_OFF         0x00000000
LOCAL UINT32 gpioDbgMask = GPIO_DBG_ALL;
IMPORT FUNCPTR _func_logMsg;

#define GPIO_DBG(mask, string, a, b, c, d, e, f)         \
    if ((gpioDbgMask & mask) || (mask == GPIO_DBG_ALL))   \
        if (_func_logMsg != NULL)                       \
            (* _func_logMsg)(string, a, b, c, d, e, f)
#else
#define GPIO_DBG(mask, string, a, b, c, d, e, f)
#endif  /* GPIO_DBG_ON */

LOCAL void vxbFtGpioInstInit(VXB_DEVICE_ID pInst);
LOCAL void vxbFtGpioInstInit2(VXB_DEVICE_ID pInst);
LOCAL void vxbFtGpioInstConnect(VXB_DEVICE_ID pInst);

LOCAL STATUS vxbFtGpioModeSet (VXB_DEVICE_ID pDev, UINT32 pin, 
                                UINT32 mode);
LOCAL UINT32 vxbFtGpioPinInput (VXB_DEVICE_ID pDev, UINT32 pin);
LOCAL STATUS vxbFtGpioPinOuput (VXB_DEVICE_ID pDev, UINT32 pin, 
                                            UINT32 value);
LOCAL STATUS vxbFtGpioISRSet (VXB_DEVICE_ID pDev, UINT32 pin, 
                                       VOIDFUNCPTR pIsr, void * pArg);
LOCAL void vxbFtGpioISR (FT_GPIO_DRVCTRL * pDrvCtrl);
LOCAL STATUS vxbFtGpioPinIntEnable (VXB_DEVICE_ID pDev, UINT32 pin);
    
LOCAL STATUS vxbFtGpioPinIntDisable (VXB_DEVICE_ID pDev, UINT32 pin);
    
LOCAL struct drvBusFuncs vxbFtGpioDrvFuncs =
    {
    vxbFtGpioInstInit,      /* devInstanceInit */
    vxbFtGpioInstInit2,     /* devInstanceInit2 */
    vxbFtGpioInstConnect    /* devConnect */
    };

LOCAL device_method_t vxbFtGpioDrv_methods[] =
    {
    DEVMETHOD_END
    };

/* phytium GPIO VxBus registration info */

LOCAL struct vxbDevRegInfo vxbFtGpioDrvRegistration =
    {
    NULL,                           /* pNext */
    VXB_DEVID_DEVICE,               /* devID */
    VXB_BUSID_PLB,                  /* busID = PLB */
    VXB_VER_4_0_0,                  /* busVer */
    "ftGpio",                       /* drvName */
    &vxbFtGpioDrvFuncs,             /* pDrvBusFuncs */
    &vxbFtGpioDrv_methods[0],       /* pMethods */
    NULL                            /* devProbe */
    };
    
/******************************************************************************
*
* vxbFtGpioDrvRegister - register Phytium GPIO driver
*
* This routine registers the Phytium GPIO driver with the vxBus subsystem.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFtGpioDrvRegister (void)
    {
    /* call the vxBus routine to register the GPIO driver */

    vxbDevRegister (&vxbFtGpioDrvRegistration);
    }

/*******************************************************************************
*
* vxbFtGpioInstInit - first level initialization routine of GPIO modules
*
* This is the function called to perform the first level initialization of
* the six Phytium GPIO modules.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtGpioInstInit
    (
    VXB_DEVICE_ID pInst
    )
    {
    }

/*******************************************************************************
*
* vxbFtGpioInstInit2 - second level initialization routine of GPIO modules
*
* This routine performs the second level initialization of the GPIO modules.
*
* This routine is called later during system initialization. OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtGpioInstInit2
    (
    VXB_DEVICE_ID pInst
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl;
    UINT32 pin = 0,trigIndex;
    HCF_DEVICE * pHcf;
    UINT32 ** ppModeTable;
    UINT32 *pModeTable;
    struct intrCtlrTrigger * triggerTable;
    void * pValue;
    void ** ppValue;

    pDrvCtrl = (FT_GPIO_DRVCTRL *)hwMemAlloc(sizeof (FT_GPIO_DRVCTRL)); 
    
    if (pDrvCtrl == NULL)
        {
        GPIO_DBG(GPIO_DBG_ERR, "vxbFtGpioInstInit: pDrvCtrl alloc failed\r\n.",1,2,3,4,5,6);
        return;
        }
        
    pInst->pDrvCtrl = pDrvCtrl;
    pDrvCtrl->pInst = pInst;

    /* map register base */

    if (vxbRegMap(pInst, 0, &pDrvCtrl->gpioHandle) == ERROR)
        {
        GPIO_DBG (GPIO_DBG_ERR, "vxbFtGpioInstInit: vxbRegMap GPIO ERROR\n", 1, 2, 3, 4, 5, 6);
        return;
        }
    
    pDrvCtrl->gpioModeSet = vxbFtGpioModeSet;
    pDrvCtrl->gpioInput = vxbFtGpioPinInput;
    pDrvCtrl->gpioOutput = vxbFtGpioPinOuput;
    pDrvCtrl->gpioISRSet = vxbFtGpioISRSet;

    pDrvCtrl->gpioIntEnable = vxbFtGpioPinIntEnable;
    pDrvCtrl->gpioIntDisable = vxbFtGpioPinIntDisable;
    
    pDrvCtrl->intEnabled = FALSE;
    
    /* clear all interrupt */
    GPIO_WRITE_4 (pDrvCtrl, GPIO_INTEN_A, 0);
    
    ppValue = &pValue;
    ppModeTable = &pModeTable;
    pHcf = (struct hcfDevice *) hcfDeviceGet (pInst);

    if (pHcf == NULL)
            return;
    
    if (devResourceGet (pHcf, "trigTable", HCF_RES_ADDR, ppValue) != OK)
        {
        for(pin = 0; pin < FT_GPIO_PORT_WIDTH; pin ++)
            {
            pDrvCtrl->triggerMode[pin] = 0;
            }   
        }
    else
        {
        triggerTable = (struct intrCtlrTrigger *) pValue;
        if(pInst->unitNumber < 3)
            {
            for(pin = 0; pin < FT_GPIO_PORT_WIDTH; pin ++)
                {
                trigIndex = pInst->unitNumber * FT_GPIO_PORT_WIDTH + pin;
                pDrvCtrl->triggerMode[pin] = triggerTable[trigIndex].trigger;
                }
            }
        else
            {
            trigIndex = 3 * FT_GPIO_PORT_WIDTH + (pInst->unitNumber - 3);
            for(pin = 0; pin < FT_GPIO_PORT_WIDTH; pin ++)
                {
                pDrvCtrl->triggerMode[pin] = triggerTable[trigIndex].trigger;
                }
            }
        }
    
    if (devResourceGet (pHcf, "modeTable", HCF_RES_ADDR, (void **)ppModeTable) != OK)
        {
        for(pin = 0; pin < FT_GPIO_PORT_WIDTH; pin ++)
            vxbFtGpioModeSet(pInst, pin, 0);    
        }
    else
        {
        for(pin = 0; pin < FT_GPIO_PORT_WIDTH; pin ++)
            vxbFtGpioModeSet(pInst, pin, *(pModeTable + pin));  
        }       

    /* Demo board */
    if (4 == pInst->unitNumber)
        {
        pinGpio4p10();
        pinGpio4p11();
        pinGpio4p12();
        pinGpio4p13();
        }
    if (5 == pInst->unitNumber)
        {
        pinGpio5p9();
        }

    return;
    }

/*******************************************************************************
*
* vxbFtGpioInstConnect - third level initialization routine of GPIO modules
*
* This is the function called to perform the third level initialization of
* the GPIO modules.
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtGpioInstConnect
    (
    VXB_DEVICE_ID pInst
    )
    {
    /* nothing is done here */

    }

LOCAL STATUS vxbFtGpioBitSet(FT_GPIO_DRVCTRL * pDrvCtrl, UINT32 reg, UINT32 pin, UINT32 bit)
{
    UINT32 value=0;

    if(pin >= FT_GPIO_PORT_WIDTH)
        {
        GPIO_DBG(GPIO_DBG_ERR, "[reg 0x%x]Bad pin %d! Only support pin: 0~%d \r\n", reg, pin, FT_GPIO_PORT_WIDTH - 1,4,5,6);
        return ERROR;
        }
    if((bit!=0)&&(bit!=1))
        {
        GPIO_DBG(GPIO_DBG_ERR, "[reg 0x%x]Bad bit value %d! \r\n", reg, bit,3,4,5,6);
        return ERROR;
        }
    
    value = GPIO_READ_4(pDrvCtrl, reg);
    
    switch(bit)
        {
         case 0:
             value &= ~(1<<pin);
             break;
         case 1:
             value |= (1<<pin);
             break;
      
         default:
             return ERROR;
        }
    
    GPIO_WRITE_4(pDrvCtrl, reg, value);
    
    return OK;
}

/*******************************************************************************
*
* vxbFtGpioPinISREnable - enable interrupt of a pin
*
* the function enable interrupt of a pin.
*
* RETURNS: OK or ERROR 
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioPinISREnable 
    (
    VXB_DEVICE_ID pDev,
    UINT32        pin
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    STATUS st = OK;
    
    if(pDev->unitNumber < 3)
        {
        /*gpio0~2:every pin have an interrupt number*/
        st |= vxbIntConnect(pDev, pin, vxbFtGpioISR, pDrvCtrl);
        st |= vxbIntEnable(pDev, pin, vxbFtGpioISR, pDrvCtrl);
        if (OK != st)
            GPIO_DBG(GPIO_DBG_ERR, "Failed to enable pin %d interrupt\r\n", pin, 2,3,4,5,6);
        }
    else
        {
        /*gpio3~5:all pin of a gpio controller use a common interrupt number*/
        if(pDrvCtrl->intEnabled == FALSE)
            {
            st |= vxbIntConnect(pDev, 0, vxbFtGpioISR, pDrvCtrl);
            st |= vxbIntEnable(pDev, 0, vxbFtGpioISR, pDrvCtrl);
            if (OK == st)
                pDrvCtrl->intEnabled = TRUE;
            else
                GPIO_DBG(GPIO_DBG_ERR, "Failed to enable pin %d interrupt\r\n", pin, 2,3,4,5,6);
            }
        }
    
    return st;
    }

/*******************************************************************************
*
* vxbFtGpioPinISRDisable - disable interrupt of a pin
*
* the function disable interrupt of a pin.
*
* RETURNS: OK or ERROR 
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioPinISRDisable 
    (
    VXB_DEVICE_ID pDev,
    UINT32        pin
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    STATUS st = OK;
    STATUS disableFlag = TRUE;
    UINT32 i;
    
    if(pDev->unitNumber < 3)
        {
        /*gpio0~2:every pin have an interrupt number*/
        st |= vxbIntDisable(pDev, pin, vxbFtGpioISR, pDrvCtrl);
        st |= vxbIntDisconnect(pDev, pin, vxbFtGpioISR, pDrvCtrl);
        if (OK != st)
            GPIO_DBG(GPIO_DBG_ERR, "Failed to disable pin %d interrupt\r\n", pin, 2,3,4,5,6);
        }
    else
        {
        /*gpio3~5:all pin of a gpio controller use a common interrupt number,
          so only if all of pins of the controller are not used as interrupt pin,
          we need to disable the interrupt*/
        if(pDrvCtrl->intEnabled == TRUE)
            {
            for(i = 0; i < FT_GPIO_PORT_WIDTH; i++)
                {
                if((pDrvCtrl->pinMode[i] == GPIO_MODE_INT) && (i != pin))
                    {
                    /*have another pin that used as interrupt pin, so don't need to disable interrupt */
                    disableFlag = FALSE;
                    break;
                    }
                }
            if(disableFlag == TRUE)
                {
                st |= vxbIntDisable(pDev, 0, vxbFtGpioISR, pDrvCtrl);
                st |= vxbIntDisconnect(pDev, 0, vxbFtGpioISR, pDrvCtrl);
                if (OK == st)
                    pDrvCtrl->intEnabled = FALSE;
                else
                    GPIO_DBG(GPIO_DBG_ERR, "Failed to disable pin %d interrupt\r\n", pin, 2,3,4,5,6);
                }
            }
        }
    return st;
    }


/*******************************************************************************
*
* vxbFtGpioPinIntEnable - enable interrupt of a pin
*
* the function enable interrupt of a pin.
*
* RETURNS: OK or ERROR 
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioPinIntEnable 
    (
    VXB_DEVICE_ID pDev,
    UINT32        pin
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    STATUS st = OK;

    st = vxbFtGpioBitSet(pDrvCtrl, GPIO_INTMASK_A, pin, GPIO_INT_NOT_MASK);
    st |= vxbFtGpioPinISREnable(pDev, pin);
    return st;
    }

/*******************************************************************************
*
* vxbFtGpioPinIntDisable - disable interrupt of a pin
*
* the function disable interrupt of a pin.
*
* RETURNS: OK or ERROR 
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioPinIntDisable 
    (
    VXB_DEVICE_ID pDev,
    UINT32        pin
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    STATUS st = OK;

    st = vxbFtGpioPinISRDisable(pDev, pin);
    st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTMASK_A, pin, GPIO_INT_MASK);
    return st;
    }

/*******************************************************************************
*
* vxbFtGpioPinModeSet - set mode of a pin 
*
* the function set mode of a pin.
*
* RETURNS: OK or ERROR 
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioModeSet 
    (
    VXB_DEVICE_ID pDev,
    UINT32        pin,
    UINT32        mode
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    STATUS st = OK;
    UINT32 level = 0, polar = 0;  
    
    if ((pin >= FT_GPIO_PORT_WIDTH) || (mode > GPIO_MODE_INT))
        {
        GPIO_DBG(GPIO_DBG_ERR, "Parameter is out of range : GPIO pin %d, mode %d\r\n", pin, mode, 3,4,5,6);
        return ERROR;
        }
    
    switch(mode)
       {
       case GPIO_MODE_NOT_USED:  
       case GPIO_MODE_IN:
            if (pDrvCtrl->pinMode[pin] == GPIO_MODE_INT)
                {
                st |= vxbFtGpioPinISRDisable(pDev, pin);
                }
            
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_SW_DDR_A, pin, GPIO_DIR_IN);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTMASK_A, pin, GPIO_INT_MASK);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTEN_A, pin, GPIO_INT_DIS);
            break;
       case GPIO_MODE_OUT:
            if (pDrvCtrl->pinMode[pin] == GPIO_MODE_INT)
                {
                st |= vxbFtGpioPinISRDisable(pDev, pin);
                }
                
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_SW_DDR_A, pin, GPIO_DIR_OUT);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTMASK_A, pin, GPIO_INT_MASK);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTEN_A, pin, GPIO_INT_DIS);
            break;
       case GPIO_MODE_INT:
           switch(pDrvCtrl->triggerMode[pin])
                {
                case VXB_INTR_TRIG_FALLING_EDGE:
                    level = GPIO_INT_TYPE_EDGE;
                    polar = GPIO_INT_POL_LOW_DOWN;
                    break;
                case VXB_INTR_TRIG_RISING_EDGE:
                    level = GPIO_INT_TYPE_EDGE;
                    polar = GPIO_INT_POL_HIGH_UP;
                    break;
                case VXB_INTR_TRIG_ACTIVE_LOW:
                    level = GPIO_INT_TYPE_LEVEL;
                    polar = GPIO_INT_POL_LOW_DOWN;                      
                    break;
                case VXB_INTR_TRIG_ACTIVE_HIGH:
                    level = GPIO_INT_TYPE_LEVEL;
                    polar = GPIO_INT_POL_HIGH_UP; 
                    break;
                default:
                    pDrvCtrl->pinMode[pin] = GPIO_MODE_NOT_USED;
                    GPIO_DBG(GPIO_DBG_ERR, "Mode set error of pin %d: trigger mode error \r\n",pin,2,3,4,5,6);
                    return ERROR;
                }

            st |= vxbFtGpioPinISREnable(pDev, pin);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_SW_DDR_A, pin, GPIO_DIR_IN);
            
            /*st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTMASK_A, pin, GPIO_INT_NOT_MASK);*/
            /*disable at first; wait for user to enable it.(vxbFtGpioPinIntEnable) */
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTMASK_A, pin, GPIO_INT_MASK);
            
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTTYPE_LEVEL_A, pin, level);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INT_POLARITY_A, pin, polar);
            st |= vxbFtGpioBitSet(pDrvCtrl, GPIO_INTEN_A, pin, GPIO_INT_ENA);

            break;           
       default:
            return ERROR;
       }
    
    if (OK != st)
        {
        pDrvCtrl->pinMode[pin] = GPIO_MODE_NOT_USED;
        GPIO_DBG(GPIO_DBG_ERR, "Mode set error of pin %d\r\n", pin, 2,3,4,5,6);
        return ERROR;
        }
    else
        {
        pDrvCtrl->pinMode[pin] = mode;
        }
    return OK;
    }
    
/*******************************************************************************
*
* vxbFtGpioPinInput - gpio pin input value
*
* The function return value of specified intput pin.
*
* RETURNS: input value
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL UINT32 vxbFtGpioPinInput
    (
    VXB_DEVICE_ID pDev, 
    UINT32        pin
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    
    if (pin >= FT_GPIO_PORT_WIDTH)
        {
        GPIO_DBG(GPIO_DBG_ERR, "Parameter is out of range :GPIO pin %d \r\n", pin,2,3,4,5,6);
        return (UINT32)ERROR;
        }
    if (pDrvCtrl->pinMode[pin] != GPIO_MODE_IN)
        {
        GPIO_DBG(GPIO_DBG_ERR, " pin %d is not in input mode\r\n", pin,2,3,4,5,6);
        return (UINT32)ERROR;
        }    
  
    return ((GPIO_READ_4(pDrvCtrl, GPIO_EXT_A) >> pin) & 1);
    }

/*******************************************************************************
*
* vxbFtGpioPinOuput - output value to gpio pin
*
* Write value to the specified pin.
*
* RETURNS: OK or ERROR if the pin is invalid.
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioPinOuput
    (
     VXB_DEVICE_ID pDev, 
     UINT32        pin,
     UINT32        value
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    
    if (pin >= FT_GPIO_PORT_WIDTH)
        {
        GPIO_DBG(GPIO_DBG_ERR, "Parameter is out of range :GPIO pin %d \r\n", pin,2,3,4,5,6);
        return ERROR;
        }
    if (pDrvCtrl->pinMode[pin] != GPIO_MODE_OUT)
        {
        GPIO_DBG(GPIO_DBG_ERR, " pin %d is not in output mode\r\n", pin,2,3,4,5,6);
        return ERROR;
        } 
    if((value!=0)&&(value!=1))
        {
        GPIO_DBG(GPIO_DBG_ERR, "Bad pin value %d! Only support value: 0,1 \r\n", value,2,3,4,5,6);
        return ERROR;
        }
    
    vxbFtGpioBitSet(pDrvCtrl, GPIO_SW_DR_A, pin, value);
    
    return OK;
    }
    
/*******************************************************************************
*
* vxbFtGpioISRSet - set a function to be called on the gpio interrupt
*
* This function is called to set a function which can be called when
* the gpio interrupt occurs.
*
* RETURNS: OK or ERROR if the pin is invalid.
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL STATUS vxbFtGpioISRSet
    (
    VXB_DEVICE_ID   pDev, 
    UINT32          pin,
    VOIDFUNCPTR     pIsr,       /* ISR */
    void *          pArg        /* parameter */
    )
    {
    FT_GPIO_DRVCTRL * pDrvCtrl = pDev->pDrvCtrl;
    
    if (pin >= FT_GPIO_PORT_WIDTH)
        {
        GPIO_DBG(GPIO_DBG_ERR, "Parameter is out of range :GPIO pin %d \r\n", pin, 2,3,4,5,6);
        return ERROR;
        }
    if (pDrvCtrl->pinMode[pin] != GPIO_MODE_INT)
        {
        GPIO_DBG(GPIO_DBG_ERR, " pin %d is not in interrupt mode\r\n", pin,2,3,4,5,6);
        return ERROR;
        } 
        
    pDrvCtrl->isrTable[pin].pIsr = pIsr;
    pDrvCtrl->isrTable[pin].pArg = pArg;

    return OK;
    }

/*******************************************************************************
*
* vxbFtGpioISR - interrupt level processing for gpio module
*
* This routine handles gpio interrupts. It acknowledges the interrupt and
* calls the routine installed by vxbFtGpioISRSet().
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/
LOCAL void vxbFtGpioISR
    (
    FT_GPIO_DRVCTRL * pDrvCtrl
    )
    {
    UINT32 val;
    UINT32 ix;
    
    /* read interrupt flags */
 
    val = GPIO_READ_4 (pDrvCtrl, GPIO_INTSTATUS_A);
    
    if (val == 0)
        {
        GPIO_DBG(GPIO_DBG_ERR, "vxFtGpioISR: it's not a Gpio interrupt. \r\n.",
                            1,2,3,4,5,6);
        goto exit;
        }
    
    for (ix = 0; ix < FT_GPIO_PORT_WIDTH; ix++) 
        {
        UINT32 bit = 1 << ix;
        if (val & bit)
            {
            if (pDrvCtrl->isrTable[ix].pIsr != NULL)
                {
                pDrvCtrl->isrTable[ix].pIsr(pDrvCtrl->isrTable[ix].pArg);
                }
            else
                {
                /* suspicious int */
                
                GPIO_DBG(GPIO_DBG_ERR, "neet to set ISR for pin %d. \r\n.",
                            ix,2,3,4,5,6);
                goto exit;
                }
            }
        }

exit:   
    /* clear interrupt flags */ 

    GPIO_WRITE_4 (pDrvCtrl, GPIO_PORTA_EOI_A, val);
        
    return;
    }

#define FT_GPIO_DEBUG
#ifdef FT_GPIO_DEBUG
void gpioModeShow(UINT32 gpio)
    {
    int i;
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;

    pDev = vxbInstByNameFind("ftGpio", gpio);
            
    pCtrl = pDev->pDrvCtrl;
    for(i = 0; i < FT_GPIO_PORT_WIDTH; i++)
        {
        printf("pin %d  mode %d\n", i, pCtrl->pinMode[i]);
        printf("pin %d  triggerMode %x\n", i, pCtrl->triggerMode[i]);
        }
    
    printf("intEnabled %d\n",  pCtrl->intEnabled);   
    }

void gpioModeSetTest (UINT32 port, UINT32 pin, UINT32 mode)
    {
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;

    pDev = vxbInstByNameFind("ftGpio", port);
    
    pCtrl = pDev->pDrvCtrl;
    
    pCtrl->gpioModeSet(pDev, pin, mode); 

    }

void gpioOutTest (UINT32 port, UINT32 pin, UINT32 val)
    {
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;

    pDev = vxbInstByNameFind("ftGpio", port);
    
    pCtrl = pDev->pDrvCtrl;
    
    pCtrl->gpioOutput(pDev, pin, val); 
    }

void gpioInTest (UINT32 port, UINT32 pin)
    {
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;

    pDev = vxbInstByNameFind("ftGpio", port);
    
    pCtrl = pDev->pDrvCtrl;
    
    printf("input val %d\n", pCtrl->gpioInput(pDev, pin)); 
    }


LOCAL void gpioisrFunction(void * arg)
{
    UINT32 * pin = (UINT32 *)arg;
    logMsg("GPIO pin %d ISR \n",* pin,0,0,0,0,0);
}

LOCAL UINT32 isrArgTest = 0;

void gpioIsrSetTest (UINT32 pin)
    {
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;

    pDev = vxbInstByNameFind("ftGpio", 0);
    
    pCtrl = pDev->pDrvCtrl;
    
    isrArgTest = pin;
    
    pCtrl->gpioISRSet(pDev, pin, gpioisrFunction, &isrArgTest);
    
    }

void gpio5p11Demux(void)
{
  pinGpio5p11();
}

void gpio5p11Set(UINT32 level)
{
  gpioModeSetTest(5,11,GPIO_MODE_OUT);
  gpioOutTest (5,11, level);
}

void gpioIsrDemo(int pin)
{
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;
    
    pDev = vxbInstByNameFind("ftGpio", 4);
    pCtrl = pDev->pDrvCtrl;
    
    uartf("### Enter gpio pin %d interrupt\r\n", pin);

/*    pCtrl->gpioIntDisable(pDev,10); */
}

/* Test DEMO Board GPIO4.10 as interrupt pin. */
void gpio4p10IntTestStart(void)
{
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;

    pDev = vxbInstByNameFind("ftGpio", 4);
    pCtrl = pDev->pDrvCtrl;
    
    gpioModeSetTest (4, 10, GPIO_MODE_INT);

    pCtrl->gpioISRSet(pDev, 10, gpioIsrDemo,10); 

    /*GPIO pin int still disabled, use .gpioIntEnable to enable it.*/
}

void gpio4p10IntTestTrigger(void)
{
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;
    
    pDev = vxbInstByNameFind("ftGpio", 4);
    pCtrl = pDev->pDrvCtrl;
    pCtrl->gpioIntEnable(pDev,10); 
}

void gpio4p10IntTestStop(void)
{
    VXB_DEVICE_ID pDev;
    FT_GPIO_DRVCTRL * pCtrl;
    
    pDev = vxbInstByNameFind("ftGpio", 4);
    pCtrl = pDev->pDrvCtrl;
    pCtrl->gpioIntDisable(pDev,10);  
}

void gpio4p11OutModeSetTest(void)
{
    gpioModeSetTest(4,11,GPIO_MODE_OUT);
}



/*

gpio4p10IntTestStart

gpio4p10IntTestTrigger

gpio4p11OutModeSetTest
level=0
gpioOutTest (4,11, level);
level=1
gpioOutTest (4,11, level);

gpio4p10IntTestStop

*/


#endif
