/* vxbFtSdCtrl.h - SD card driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbFtSdCtrlh
#define __INCvxbFtSdCtrlh

#ifdef __cplusplus
extern "C" {
#endif
#include <hwif/vxbus/vxbSdLib.h>

#define FT_SDHC_NAME            "ftSdhci"
#define SDHC_MAX_RW_SECTORS     512
#define SD_MAX_BD_NUM           512 /*this value should greater than or equal to DOSFS_BLOCKS_PER_DATA_DIR_CACHE_GROUP when dosfs in cache mode*/
#define SD_BLOCK_SIZE           512

#define SDCI_BUS_1BITS          0x0
#define SDCI_BUS_4BITS          0x1
#define SDCI_BUS_8BITS          (0x1 << 16)

#define SDCI_TYPE_SD            0
#define SDCI_TYPE_EMMC          1
#define SDCI_TYPE_SDIO          2

#define SDCI_TIMEOUT_CMD_VALUE  0xFFFFFFFF
#define SDCI_POWER_ON           1
#define SDCI_POWER_OFF          0

#define SDCI_CMD_TIMEOUT        (1)     /* 1s */
#define SDCI_DATA_TIMEOUT       (10)    /* 10s */

#define SDCI_EXT_CLK            1200000000
#define SDCI_MAX_CLK            100000000

/*----------------------------------------------------------------------*/
/* Register Offset                          */
/*----------------------------------------------------------------------*/
#define SDCI_CNTRL          0x00 /* the controller config reg */
#define SDCI_PWREN          0x04 /* the power enable reg */
#define SDCI_CLKDIV         0x08 /* the clock divider reg */
#define SDCI_CLKENA         0x10 /* the clock enable reg */
#define SDCI_TMOUT          0x14 /* the timeout reg */
#define SDCI_CTYPE          0x18 /* the card type reg */
#define SDCI_BLKSIZ         0x1C /* the block size reg */
#define SDCI_BYTCNT         0x20 /* the byte count reg */
#define SDCI_INT_MASK       0x24 /* the interrupt mask reg */
#define SDCI_CMDARG         0x28 /* the command argument reg */
#define SDCI_CMD            0x2C /* the command reg */
#define SDCI_RESP0          0x30 /* the response reg0 */
#define SDCI_RESP1          0x34 /* the response reg1 */
#define SDCI_RESP2          0x38 /* the response reg2 */
#define SDCI_RESP3          0X3C /* the response reg3 */
#define SDCI_MASKED_INTS    0x40 /* the masked interrupt status reg */
#define SDCI_RAW_INTS       0x44 /* the raw interrupt status reg */
#define SDCI_STATUS         0x48 /* the status reg  */
#define SDCI_FIFOTH         0x4C /* the FIFO threshold watermark reg */
#define SDCI_CARD_DETECT    0x50 /* the card detect reg */
#define SDCI_CARD_WRTPRT    0x54 /* the card write protect reg */
#define SDCI_CCLK_RDY       0x58 /* first div is ready? 1:ready,0:not ready*/
#define SDCI_TRAN_CARD_CNT  0x5C /* the transferred CIU card byte count reg */
#define SDCI_TRAN_FIFO_CNT  0x60 /* the transferred host to FIFO byte count reg  */
#define SDCI_DEBNCE         0x64 /* the debounce count reg */
#define SDCI_UID            0x68 /* the user ID reg */
#define SDCI_VID            0x6C /* the controller version ID reg */
#define SDCI_HWCONF         0x70 /* the hardware configuration reg */
#define SDCI_UHS_REG        0x74 /* the UHS-I reg */
#define SDCI_CARD_RESET     0x78 /* the card reset reg */
#define SDCI_BUS_MODE       0x80 /* the bus mode reg */
#define SDCI_DESC_LIST_ADDRL 0x88 /* the descriptor list low base address reg */
#define SDCI_DESC_LIST_ADDRH 0x8C /* the descriptor list high base address reg */
#define SDCI_DMAC_STATUS    0x90 /* the internal DMAC status reg */
#define SDCI_DMAC_INT_ENA   0x94 /* the internal DMAC interrupt enable reg */
#define SDCI_CUR_DESC_ADDRL 0x98 /* the current host descriptor low address reg */
#define SDCI_CUR_DESC_ADDRH 0x9C /* the current host descriptor high address reg */
#define SDCI_CUR_BUF_ADDRL  0xA0 /* the current buffer low address reg */
#define SDCI_CUR_BUF_ADDRH  0xA4 /* the current buffer high address reg */
#define SDCI_CARD_THRCTL    0x100 /* the card threshold control reg */
#define SDCI_UHS_REG_EXT    0x108 /* the UHS register extension */
#define SDCI_EMMC_DDR_REG   0x10C /* the EMMC DDR reg */
#define SDCI_ENABLE_SHIFT   0x110 /* the enable phase shift reg */
#define SDCI_DATA           0x200 /* the data FIFO access */

/* Command register defines */
#define SDCI_CMD_START          BIT(31)
#define SDCI_CMD_USE_HOLD_REG   BIT(29)
#define SDCI_CMD_VOLT_SWITCH    BIT(28)
#define SDCI_CMD_CCS_EXP        BIT(23)
#define SDCI_CMD_CEATA_RD       BIT(22)
#define SDCI_CMD_UPD_CLK        BIT(21)
#define SDCI_CMD_INIT           BIT(15)
#define SDCI_CMD_STOP           BIT(14)
#define SDCI_CMD_PRV_DAT_WAIT   BIT(13)
#define SDCI_CMD_SEND_STOP      BIT(12)
#define SDCI_CMD_STRM_MODE      BIT(11)
#define SDCI_CMD_DAT_WR         BIT(10)
#define SDCI_CMD_DAT_EXP        BIT(9)
#define SDCI_CMD_RESP_CRC       BIT(8)
#define SDCI_CMD_RESP_LONG      BIT(7)
#define SDCI_CMD_RESP_EXP       BIT(6)
#define SDCI_CMD_INDX(n)        ((n) & 0x3F)

/*------------------------------------------------------*/
/* Register Mask                    */
/*------------------------------------------------------*/
/* SDCI_CNTRL mask */
#define SDCI_CNTRL_CONTROLLER_RESET  (0x1 << 0) /* RW */
#define SDCI_CNTRL_FIFO_RESET        (0x1 << 1) /* RW */
#define SDCI_CNTRL_DMA_RESET         (0x1 << 2) /* RW */
#define SDCI_CNTRL_RES               (0x1 << 3) /*  */
#define SDCI_CNTRL_INT_ENABLE        (0x1 << 4) /* RW */
#define SDCI_CNTRL_DMA_ENABLE        (0x1 << 5) /* RW */
#define SDCI_CNTRL_READ_WAIT         (0x1 << 6) /* RW */
#define SDCI_CNTRL_SEND_IRQ_RESPONSE (0x1 << 7) /* RW */
#define SDCI_CNTRL_ABORT_READ_DATA   (0x1 << 8) /* RW */
#define SDCI_CNTRL_ENDIAN            (0x1 << 11) /* RW */
/*//#define SDCI_CNTRL_CARD_VOLTAGE_A   (0xF << 16)  RW */
/*//#define SDCI_CNTRL_CARD_VOLTAGE_B   (0xF << 20)  RW */
#define SDCI_CNTRL_ENABLE_OD_PULLUP  (0x1 << 24) /* RW */
#define SDCI_CNTRL_USE_INTERNAL_DMAC (0x1 << 25) /* RW */

/* SDCI_PWREN mask */
#define SDCI_PWREN_ENABLE            (0x1 << 0)  /* RW */

/* SDCI_CLKENA mask */
#define SDCI_CLKENA_CCLK_ENABLE     (0x1 << 0) /* RW */
#define SDCI_CLKENA_CCLK_LOW_POWER  (0x1 << 16) /* RW */

/* SDCI_INT_MASK mask */
#define SDCI_INT_MASK_CD        (0x1 << 0) /* RW 卡检测 */
#define SDCI_INT_MASK_RE        (0x1 << 1) /* RW 响应错误 */
#define SDCI_INT_MASK_CMD       (0x1 << 2) /* RW 命令完成 */
#define SDCI_INT_MASK_DTO       (0x1 << 3) /* RW 数据传输完成 */
#define SDCI_INT_MASK_TXDR      (0x1 << 4) /* RW TX FIFO 数据请求 */
#define SDCI_INT_MASK_RXDR      (0x1 << 5) /* RW RX FIFO 数据请求 */
#define SDCI_INT_MASK_RCRC      (0x1 << 6) /* RW 响应 CRC 错误 */
#define SDCI_INT_MASK_DCRC      (0x1 << 7) /* RW 数据 CRC 错误 */
#define SDCI_INT_MASK_RTO       (0x1 << 8) /* RW 响应超时 */
#define SDCI_INT_MASK_DRTO      (0x1 << 9) /* RW 数据读超时 */
#define SDCI_INT_MASK_HTO       (0x1 << 10) /* RW 数 据 饥 饿 超 时/Volt_switch */
#define SDCI_INT_MASK_FRUN      (0x1 << 11) /* RW FIFO 上/下溢错误 */
#define SDCI_INT_MASK_HLE       (0x1 << 12) /* RW 硬件锁存写错误 */
#define SDCI_INT_MASK_SBE_BCI   (0x1 << 13) /* RW 起始位错误(SBE)/Busy 完成(BCI) */
#define SDCI_INT_MASK_ACD       (0x1 << 14) /* RW 自动命令完成 */
#define SDCI_INT_MASK_EBE       (0x1 << 15) /* RW 读写 End-bit 错误/写未收到CRC */
#define SDCI_INT_MASK_SDIO      (0x1 << 16) /* RW SDIO card 中断 */

/* SDCI_MASKED_INTS mask */
#define SDCI_MASKED_INTS_CD         (0x1 << 0) /* RO */
#define SDCI_MASKED_INTS_RE         (0x1 << 1) /* RO */
#define SDCI_MASKED_INTS_CMD        (0x1 << 2) /* RO */
#define SDCI_MASKED_INTS_DTO        (0x1 << 3) /* RO */
#define SDCI_MASKED_INTS_TXDR       (0x1 << 4) /* RO */
#define SDCI_MASKED_INTS_RXDR       (0x1 << 5) /* RO */
#define SDCI_MASKED_INTS_RCRC       (0x1 << 6) /* RO */
#define SDCI_MASKED_INTS_DCRC       (0x1 << 7) /* RO */
#define SDCI_MASKED_INTS_RTO        (0x1 << 8) /* RO */
#define SDCI_MASKED_INTS_DRTO       (0x1 << 9) /* RO */
#define SDCI_MASKED_INTS_HTO        (0x1 << 10) /* RO */
#define SDCI_MASKED_INTS_FRUN       (0x1 << 11) /* RO */
#define SDCI_MASKED_INTS_HLE        (0x1 << 12) /* RO */
#define SDCI_MASKED_INTS_SBE_BCI    (0x1 << 13) /* RO */
#define SDCI_MASKED_INTS_ACD        (0x1 << 14) /* RO */
#define SDCI_MASKED_INTS_EBE        (0x1 << 15) /* RO */
#define SDCI_MASKED_INTS_SDIO       (0x1 << 16) /* RO */

/* SDCI_RAW_INTS mask */
#define SDCI_RAW_INTS_CD        (0x1 << 0) /* W1C 卡检测 */
#define SDCI_RAW_INTS_RE        (0x1 << 1) /* W1C 响应错误 */
#define SDCI_RAW_INTS_CMD       (0x1 << 2) /* W1C 命令完成 */
#define SDCI_RAW_INTS_DTO       (0x1 << 3) /* W1C 数据传输完成 */
#define SDCI_RAW_INTS_TXDR      (0x1 << 4) /* W1C TX FIFO 数据请求 */
#define SDCI_RAW_INTS_RXDR      (0x1 << 5) /* W1C RX FIFO 数据请求 */
#define SDCI_RAW_INTS_RCRC      (0x1 << 6) /* W1C 响应 CRC 错误 */
#define SDCI_RAW_INTS_DCRC      (0x1 << 7) /* W1C 数据 CRC 错误 */
#define SDCI_RAW_INTS_RTO       (0x1 << 8) /* W1C 响应超时 */
#define SDCI_RAW_INTS_DRTO      (0x1 << 9) /* W1C 数据读超时 */
#define SDCI_RAW_INTS_HTO       (0x1 << 10) /* W1C 数 据 饥 饿 超 时/Volt_switch */
#define SDCI_RAW_INTS_FRUN      (0x1 << 11) /* W1C FIFO 上/下溢错误 */
#define SDCI_RAW_INTS_HLE       (0x1 << 12) /* W1C 硬件锁存写错误 */
#define SDCI_RAW_INTS_SBE_BCI   (0x1 << 13) /* W1C 起始位错误(SBE)/Busy 完成(BCI) */
#define SDCI_RAW_INTS_ACD       (0x1 << 14) /* W1C 自动命令完成 */
#define SDCI_RAW_INTS_EBE       (0x1 << 15) /* W1C 读写 End-bit 错误/写未收到CRC */
#define SDCI_RAW_INTS_SDIO      (0x1 << 16) /* W1C SDIO card 中断 */

/* SDCI_STATUS mask */
#define SDCI_STATUS_FIFO_RX     (0x1 << 0) /* RO */
#define SDCI_STATUS_FIFO_TX     (0x1 << 1) /* RO */
#define SDCI_STATUS_FIFO_EMPTY  (0x1 << 2) /* RO */
#define SDCI_STATUS_FIFO_FULL   (0x1 << 3) /* RO */
#define SDCI_STATUS_CARD_STATUS (0x1 << 8) /* RO */
#define SDCI_STATUS_CARD_BUSY   (0x1 << 9) /* RO */
#define SDCI_STATUS_DATA_BUSY   (0x1 << 10) /* RO */
#define SDCI_STATUS_DMA_ACK     (0x1 << 31) /* RO */
#define SDCI_STATUS_DMA_REQ     (0x1 << 32) /* RO */

/* SDCI_UHS_REG mask */
#define SDCI_UHS_REG_VOLT       (0x1 << 0) /* RW */
#define SDCI_UHS_REG_DDR        (0x1 << 16) /* RW */

/* SDCI_CARD_RESET mask */
#define SDCI_CARD_RESET_ENABLE  (0x1 << 0) /* RW */

/* SDCI_BUS_MODE mask */
#define SDCI_BUS_MODE_SWR       (0x1 << 0) /* RW */
#define SDCI_BUS_MODE_FB        (0x1 << 1) /* RW */
#define SDCI_BUS_MODE_DE        (0x1 << 7) /* RW */

/* SDCI_DMAC_STATUS mask */
#define SDCI_DMAC_STATUS_TI         (0x1 << 0) /* RW */
#define SDCI_DMAC_STATUS_RI         (0x1 << 1) /* RW */
#define SDCI_DMAC_STATUS_FBE        (0x1 << 2) /* RW */
#define SDCI_DMAC_STATUS_DU         (0x1 << 4) /* RW */
#define SDCI_DMAC_STATUS_NIS        (0x1 << 8) /* RW */
#define SDCI_DMAC_STATUS_AIS        (0x1 << 9) /* RW */

/* SDCI_DMAC_INT_ENA mask */
#define SDCI_DMAC_INT_ENA_TI        (0x1 << 0) /* RW 发送中断使能*/
#define SDCI_DMAC_INT_ENA_RI        (0x1 << 1) /* RW 接收中断使能*/
#define SDCI_DMAC_INT_ENA_FBE       (0x1 << 2) /* RW 总线错误中断使能*/
#define SDCI_DMAC_INT_ENA_DU        (0x1 << 4) /* RW 描述符不可读中断使能*/
#define SDCI_DMAC_INT_ENA_CES       (0x1 << 5) /* RW 卡错误中断使能*/
#define SDCI_DMAC_INT_ENA_NIS       (0x1 << 8) /* RW 正常中断使能*/
#define SDCI_DMAC_INT_ENA_AIS       (0x1 << 9) /* RW 异常中断使能*/

/* SDCI_CARD_THRCTL mask */
#define SDCI_CARD_THRCTL_CARDRD     (0x1 << 0) /* RW */
#define SDCI_CARD_THRCTL_BUSY_CLR   (0x1 << 1) /* RW */
#define SDCI_CARD_THRCTL_CARDWR     (0x1 << 2) /* RW */
enum
{
    SDCI_FIFO_DEPTH_8 = 23,
    SDCI_FIFO_DEPTH_16 = 24,
    SDCI_FIFO_DEPTH_32 = 25,
    SDCI_FIFO_DEPTH_64 = 26,
    SDCI_FIFO_DEPTH_128 = 27
};
#define SDCI_CARD_THRCTL_THRESHOLD(n) (0x1 << n) /* 读卡 Threshold */

/* SDCI_UHS_REG_EXT mask */
#define SDCI_UHS_REG_EXT_MMC_VOLT   (0x1 << 0) /* RW */
#define SDCI_UHS_REG_EXT_CLK_ENA    (0x1 << 1) /* RW */

/* SDCI_EMMC_DDR_REG mask */
#define SDCI_EMMC_DDR_CYCLE         (0x1 << 0) /* RW */

/* SDCI_CTYPE mask */
#define CARD0_WIDTH0_1BIT           (0) /* RW */ 
#define CARD0_WIDTH0_4BIT           (0x1 << 0) /* RW */         
#define CARD0_WIDTH1_8BIT           (0x1 << 16) /* RW */ 

/* FIFOTH register defines */
#define SDCI_SET_FIFOTH(m, r, t)    (((m) & 0x7) << 28 | \
                     ((r) & 0xFFF) << 16 | ((t) & 0xFFF))

#define ADMA2_64_DESC_SZ         32

/* Each descriptor can transfer up to 4KB of data in chained mode */
/*ADMA2 64-bit descriptor.*/
struct FtAdmaDesc {
    UINT32 attribute;
#define IDMAC_DES0_DIC  BIT(1)
#define IDMAC_DES0_LD   BIT(2)
#define IDMAC_DES0_FD   BIT(3)
#define IDMAC_DES0_CH   BIT(4)
#define IDMAC_DES0_ER   BIT(5)
#define IDMAC_DES0_CES  BIT(30)
#define IDMAC_DES0_OWN  BIT(31)
    UINT32  NON1;
    UINT32  len;
    UINT32  NON2;
    UINT32  addrLo; /* Lower 32-bits of Buffer Address Pointer 1*/
    UINT32  addrHi; /* Upper 32-bits of Buffer Address Pointer 1*/
    UINT32  descLo; /* Lower 32-bits of Next Descriptor Address */
    UINT32  descHi; /* Upper 32-bits of Next Descriptor Address */
};

#define SD_CACHE_DRV_VIRT_TO_PHYS(adrs) \
    CACHE_DRV_VIRT_TO_PHYS(&cacheDmaFuncs, (void *) (adrs))

#define SD_ADDR_LO(y)  ((UINT32)((UINT64)(y) & 0xFFFFFFFF))
#define SD_ADDR_HI(y)  ((UINT32)(((UINT64)(y) >> 32) & 0xFFFFFFFF))
        
struct ftSdhcDma {
    /* ADMA descriptor table, pointer to adma_table array */
    struct FtAdmaDesc *admaTable;
    /* Mapped ADMA descr. table, the physical address of adma_table array */
    UINT64 admaAddr;
};

/* SDHC driver control */

typedef struct ftsdhcDrvCtrl
    {
    SD_HOST_CTRL    sdHostCtrl;
    VXB_DEVICE_ID   pDev;
    void *          regBase;
    void *          regHandle;
    struct vxbSdioInt * pIntInfo;
    UINT32          type;
    struct ftSdhcDma dma;   /* dma channel */
    SD_CMD *        pSdCmd;
    } FTSDHC_DEV_CTRL;
/* register low level access routines */

#define FTSDHC_BAR(p)         ((FTSDHC_DEV_CTRL *)(p)->pDrvCtrl)->regBase
#define FTSDHC_HANDLE(p)      ((FTSDHC_DEV_CTRL *)(p)->pDrvCtrl)->regHandle

#define CSR_READ_4(pDev, addr)              \
        vxbRead32(FTSDHC_HANDLE(pDev),       \
                  (UINT32 *)((char *)FTSDHC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)       \
        vxbWrite32(FTSDHC_HANDLE(pDev),      \
                   (UINT32 *)((char *)FTSDHC_BAR(pDev) + addr), data)
#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (UINT32)(~(val)))
               
    void sdhcCtrlInstInit(VXB_DEVICE_ID);
    void sdhcCtrlInstInit2 (VXB_DEVICE_ID);
    STATUS sdhcCtrlInstConnect (SD_HOST_CTRL * pSdHostCtrl);
    STATUS sdhcCtrlIsr (VXB_DEVICE_ID);
    STATUS sdhcCtrlCmdIssue (VXB_DEVICE_ID, SD_CMD *);
    STATUS sdhcCtrlCmdPrepare (VXB_DEVICE_ID, SD_CMD *);
    STATUS sdhcCtrlCmdIssuePoll (VXB_DEVICE_ID, SD_CMD *);
    STATUS sdhcCtrlInit (VXB_DEVICE_ID);
    STATUS sdhcInterruptInfo (VXB_DEVICE_ID, UINT32 *);
    STATUS sdhcDevControl (VXB_DEVICE_ID, pVXB_DEVCTL_HDR);
    
    void sdhcCtrlBusWidthSetup (VXB_DEVICE_ID, UINT32);
    void sdhcCtrlCardMonTaskPoll (VXB_DEVICE_ID);
    void ftsdhcCtrlClkFreqSetup (VXB_DEVICE_ID, UINT32);
    BOOL sdhcCtrlCardWpCheck (VXB_DEVICE_ID);
    BOOL sdhcCtrlCardInsertSts (VXB_DEVICE_ID);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFtSdCtrlh */

