/* defs.h - some public definations header */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCdefsh
#define __INCdefsh

#ifdef __cplusplus
extern "C" {
#endif

#define CLK_400KHZ (400000)
#define CLK_48MHZ  (48000000)
#define CLK_50MHZ  (50000000)
#define CLK_100MHZ (100000000)
#define CLK_600MHZ (600000000)

#define FT2000_UART_CLK              CLK_100MHZ   /* UART clock       */

#define LBC_SPACE_BASE_ADRS             (0x10000000) /* Local Bus space */
#define LBC_SPACE_SIZE                  (0x10000000) /* 256MB */
#define NOR_FLASH_BASE_ADRS             (0x10000000) 
#define NAND_FLASH_BASE_ADRS            (0x28002000)

#define TM_TACHO_BASE_ADR(n)        (0x28054000+(n<<12)) /* 0<=n<=37 */


#define UART0_BASE_ADR             (0x2800c000)          /* UART 0 base address */
#define UART1_BASE_ADR             (0x2800d000)          /* UART 1 base address */
#define UART2_BASE_ADR             (0x2800e000)          /* UART 2 base address */
#define UART3_BASE_ADR             (0x2800f000)          /* UART 3 base address */

#define MIO0_BASE_ADR              (0x28014000)
#define MIO1_BASE_ADR              (0x28016000)
#define MIO2_BASE_ADR              (0x28018000)
#define MIO3_BASE_ADR              (0x2801A000)
#define MIO4_BASE_ADR              (0x2801C000)
#define MIO5_BASE_ADR              (0x2801E000)
#define MIO6_BASE_ADR              (0x28020000)
#define MIO7_BASE_ADR              (0x28022000)
#define MIO8_BASE_ADR              (0x28024000)
#define MIO9_BASE_ADR              (0x28026000)
#define MIO10_BASE_ADR              (0x28028000)
#define MIO11_BASE_ADR              (0x2802A000)
#define MIO12_BASE_ADR              (0x2802C000)
#define MIO13_BASE_ADR              (0x2802E000)
#define MIO14_BASE_ADR              (0x28030000)
#define MIO15_BASE_ADR              (0x28032000)

#define GPIO0_BASE_ADR             (0x28034000)          /* GPIO 0 base address */
#define GPIO1_BASE_ADR             (0x28035000)          /* GPIO 1 base address */
#define GPIO2_BASE_ADR             (0x28036000)          /* GPIO 2 base address */
#define GPIO3_BASE_ADR             (0x28037000)          /* GPIO 3 base address */
#define GPIO4_BASE_ADR             (0x28038000)          /* GPIO 4 base address */
#define GPIO5_BASE_ADR             (0x28039000)          /* GPIO 5 base address */

#define WDT0_BASE_ADR              (0x28040000)
#define WDT1_BASE_ADR              (0x28042000)

#define RTC_BASE_ADR                (0x2803E000) 
#define KEYPAD_BASE_ADR             (0x2807A000) 

#define GIC_BASE_ADR                (0x30800000)

#define GMU0_BASE_ADR               (0x3200C000)
#define GMU1_BASE_ADR               (0x3200E000)
#define GMU2_BASE_ADR               (0x32010000)
#define GMU3_BASE_ADR               (0x32012000)

#define FT_SATA0_BASE_ADR           (0x31A40000)
#define FT_SATA1_BASE_ADR           (0x32014000)

#define SYS_CLK_REG_BASE                0xeeee0000
#define SYS_CLK_FREQ                    CLK_50MHZ

#define AUX_CLK_REG_BASE                0xeeef0000
#define AUX_CLK_FREQ                    CLK_50MHZ

/*
 * interrupt mode - interrupts can be in either preemptive or non-preemptive
 * mode. For preemptive mode, change INT_MODE to INT_PREEMPT_MODEL
 */

#define INT_MODE           INT_NON_PREEMPT_MODEL

#define INT_LVL_TIMER_AUX  27
#define INT_LVL_TIMER_GEN  30

#define INT_LVL_PCIA       36
#define INT_LVL_PCIB       37
#define INT_LVL_PCIC       38
#define INT_LVL_PCID       39

#define INT_LVL_USB3_0     48
#define INT_LVL_USB3_1     49

#define INT_LVL_MHU        54

#define INT_LVL_SATA0      74 /* PSU SATA0 */
#define INT_LVL_SATA1      75 /* GSD SATA1 */

#define INT_LVL_DC            76 /*dc */
#define INT_LVL_GEM0_Q0       87
#define INT_LVL_GEM0_Q1       88
#define INT_LVL_GEM0_Q2       89
#define INT_LVL_GEM0_Q3       90
#define INT_LVL_GEM0_Q4       60
#define INT_LVL_GEM0_Q5       61
#define INT_LVL_GEM0_Q6       62
#define INT_LVL_GEM0_Q7       63

#define INT_LVL_GEM1_Q0       91
#define INT_LVL_GEM1_Q1       92
#define INT_LVL_GEM1_Q2       93
#define INT_LVL_GEM1_Q3       94

#define INT_LVL_GEM2_Q0       96
#define INT_LVL_GEM2_Q1       97
#define INT_LVL_GEM2_Q2       98
#define INT_LVL_GEM2_Q3       99

#define INT_LVL_GEM3_Q0       100
#define INT_LVL_GEM3_Q1       101
#define INT_LVL_GEM3_Q2       102
#define INT_LVL_GEM3_Q3       103

#define INT_LVL_SD0        104
#define INT_LVL_SD1        105

#define INT_LVL_NFC        106

#define INT_LVL_DDMA0      107
#define INT_LVL_DDMA1      108

#define INT_LVL_CAN0       113
#define INT_LVL_CAN1       114

#define INT_LVL_UART0      115
#define INT_LVL_UART1      116
#define INT_LVL_UART2      117
#define INT_LVL_UART3      118

#define INT_LVL_MIO0       124
#define INT_LVL_MIO1       125
#define INT_LVL_MIO2       126
#define INT_LVL_MIO3       127
#define INT_LVL_MIO4       128
#define INT_LVL_MIO5       129
#define INT_LVL_MIO6       130
#define INT_LVL_MIO7       131
#define INT_LVL_MIO8       132
#define INT_LVL_MIO9       133
#define INT_LVL_MIO10       134
#define INT_LVL_MIO11       135
#define INT_LVL_MIO12       136
#define INT_LVL_MIO13       137
#define INT_LVL_MIO14       138
#define INT_LVL_MIO15       139



#define INT_LVL_GPIO0(pin) (140 + pin) /*gpio0~2:every pin have a interrupt number (0 <= pin <= 15)*/
#define INT_LVL_GPIO1(pin) (156 + pin) 
#define INT_LVL_GPIO2(pin) (172 + pin)
#define INT_LVL_GPIO3      188         /*gpio3~5:all pin of a gpio controller use a common interrupt number*/
#define INT_LVL_GPIO4      189
#define INT_LVL_GPIO5      190

#define INT_LVL_SPI0       191
#define INT_LVL_SPI1       192
#define INT_LVL_SPI2       193
#define INT_LVL_SPI3       194

#define INT_LVL_WDT0       196
#define INT_LVL_WDT1       197

#define INT_LVL_PWMINST0   205
#define INT_LVL_PWMINST1   206
#define INT_LVL_PWMINST2   207
#define INT_LVL_PWMINST3   208
#define INT_LVL_PWMINST4   209
#define INT_LVL_PWMINST5   210
#define INT_LVL_PWMINST6   211
#define INT_LVL_PWMINST7   212

#define INT_LVL_KEYPAD     221

#define INT_LVL_ADC0       264
#define INT_LVL_ADC1       265

#define INT_LVL_GDMA0      266
#define INT_LVL_GDMA1      267

#define INT_LVL_TM_TACHO(n)  (226+n) /* 226~263. 0<=n<=37 */

#define INT_LVL_MAX        288 /* can be read from register. see also armGicLinesNum */

#define PHYADDR_OF_AUTO     -1 /* auto probe PHY addr */


/*CAN Baudrate*/
enum can_bitrate{
    CAN_BRATE_5    = 5,    /*  */
    CAN_BRATE_10   = 10,   /* */
    CAN_BRATE_20   = 20,   /*  */
    CAN_BRATE_40   = 40,   /*  */
    CAN_BRATE_50   = 50,   /*  */
    CAN_BRATE_80   = 80,   /*  */
    CAN_BRATE_100  = 100,  /*  */
    CAN_BRATE_125  = 125,  /*  */
    CAN_BRATE_200  = 200,  /*  */
    CAN_BRATE_250  = 250,  /*  */
    CAN_BRATE_400  = 400,  /*  */
    CAN_BRATE_500  = 500,  /*  */
    CAN_BRATE_800  = 800,  /*  */
    CAN_BRATE_1000 = 1000, /*  */
    CAN_BRATE_2000 = 2000, /*  */
    CAN_BRATE_3000 = 3000, /*  */
    CAN_BRATE_4000 = 4000, /*  */
    CAN_BRATE_5000 = 5000, /*  */
};

#define FT_SDHC0_BASE (0x28000000)
#define FT_SDHC1_BASE (0x28001000)
#define FT_LSD_CONFIG_BASE (0x2807E000)
#define FT_LSD_NAND_MMCSD_HADDR (0xC0U)

#define FTCAN_BRATE_DEFAULT  (CAN_BRATE_1000*1000)
#define FTCAN_DATA_BRATE_DEFAULT  (CAN_BRATE_1000*1000)

#define FTCAN0_BASE  0x2800a000
#define FTCAN0_BITRATE FTCAN_BRATE_DEFAULT
#define FTCAN0_DATA_BITRATE FTCAN_DATA_BRATE_DEFAULT


#define FTCAN1_BASE  0x2800b000
#define FTCAN1_BITRATE FTCAN_BRATE_DEFAULT
#define FTCAN1_DATA_BITRATE FTCAN_DATA_BRATE_DEFAULT


/*
*       addr                              size           usage
* 0x000_40000000~0x000_4FFFFFFF           256MB          Config
* 0x000_50000000~0x000_57FFFFFF           128MB          i/o
* 0x000_58000000~0x000_7FFFFFFF           640MB          mem32
* 0x010_00000000~0x01F_FFFFFFFF           64GB           mem64
*/

#define FT_PCI_CONFIG_ADDR 0x40000000
#define FT_PCI_CONFIG_LEN  0x10000000  /*256MB*/
#define CPU_IO_SPACE_OFFSET 0x50000000

#define FT_SECONDARY_BOOT_ADDR 0x8007fff0

#define FT_E2000DC_BASE_ADR 0x32000000         

typedef unsigned char   u8,  __u8, unchar;
typedef char        s8,  __s8;
typedef unsigned short  u16, __u16, __le16, __be16;
typedef short       s16, __s16;
typedef unsigned int    u32, __u32, __le32, __be32, uint;
typedef int     s32, __s32;
typedef unsigned long long u64;

#define bool BOOL
#define true TRUE
#define false FALSE

#undef __packed
#define __packed __attribute__((__packed__))

#define BIT(nr)         (1UL << (nr))
#define BIT_ULL(nr)     (1ULL << (nr))
#define BITS_PER_BYTE    8

#ifndef __WORDSIZE
#define __WORDSIZE (4 * 8)
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))
#endif

#if defined(_WRS_CONFIG_LP64)
#define BITS_PER_LONG 64
#else
#define BITS_PER_LONG __WORDSIZE
#endif

#ifndef BITS_PER_LONG_LONG
#define BITS_PER_LONG_LONG 64
#endif

/*
 * Create a contiguous bitmask starting at bit position @l and ending at
 * position @h. For example
 * GENMASK_ULL(39, 21) gives us the 64bit vector 0x000000ffffe00000.
 */
#define GENMASK(h, l) \
    (((~0UL) - (1UL << (l)) + 1) & ((~0UL) >> (BITS_PER_LONG - 1 - (h))))


#define GENMASK_ULL(h, l) \
    (((~0ULL) - (1ULL << (l)) + 1) & \
     (~0ULL >> (BITS_PER_LONG_LONG - 1 - (h))))

#define BITS_TO_LONGS(nr)       DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(long))
#define BITS_TO_U64(nr)         DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(UINT64))
#define BITS_TO_U32(nr)         DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(UINT32))
#define BITS_TO_BYTES(nr)       DIV_ROUND_UP(nr, BITS_PER_BYTE)

#define BIT_MASK(nr)        (1UL << ((nr) % BITS_PER_LONG))
#define BIT_WORD(nr)        ((nr) / BITS_PER_LONG)

#define BITS_TO_LONGS(nr)   DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(long))
#ifndef offsetof /* gcc stddef.h defined. */
#define offsetof(TYPE, MEMBER)  ((size_t)&((TYPE *)0)->MEMBER)
#endif

#define container_of(ptr, type, member) \
    ((type *)((char *)(ptr) - offsetof(type,member)))


#define min_t(type, x, y) ({            \
    type __min1 = (x);          \
    type __min2 = (y);          \
    __min1 < __min2 ? __min1: __min2; })


#define max_t(type, x, y) ({            \
    type __min1 = (x);          \
    type __min2 = (y);          \
    __min1 > __min2 ? __min1: __min2; })


/* ARM ARCH7 later. */
#define ISB __asm__ volatile ("isb sy" : : : "memory")
#define DSB __asm__ volatile ("dsb sy" : : : "memory")
#define DMB __asm__ volatile ("dmb sy" : : : "memory")
#define isb()   ISB
#define dsb()   DSB
#define dmb()   DMB
/*
 * Generic virtual read/write.  Note that we don't support half-word
 * read/writes.  We define __arch_*[bl] here, and leave __arch_*w
 * to the architecture specific code.
 */
#define __arch_getb(a)          (*(volatile unsigned char *)(a))
#define __arch_getw(a)          (*(volatile unsigned short *)(a))
#define __arch_getl(a)          (*(volatile unsigned int *)(a))
#define __arch_getq(a)          (*(volatile unsigned long long *)(a))

#define __arch_putb(v,a)        (*(volatile unsigned char *)(a) = (v))
#define __arch_putw(v,a)        (*(volatile unsigned short *)(a) = (v))
#define __arch_putl(v,a)        (*(volatile unsigned int *)(a) = (v))
#define __arch_putq(v,a)        (*(volatile unsigned long long *)(a) = (v))

#define __raw_writeb(v,a)   __arch_putb(v,a)
#define __raw_writew(v,a)   __arch_putw(v,a)
#define __raw_writel(v,a)   __arch_putl(v,a)
#define __raw_writeq(v,a)   __arch_putq(v,a)

#define __raw_readb(a)      __arch_getb(a)
#define __raw_readw(a)      __arch_getw(a)
#define __raw_readl(a)      __arch_getl(a)
#define __raw_readq(a)      __arch_getq(a)


#define mb()        dsb()
#define __iormb()   dmb()
#define __iowmb()   dmb()

#define writeb(v,c) ({ u8  __v = v; __iowmb(); __arch_putb(__v,c); __v; })
#define writew(v,c) ({ u16 __v = v; __iowmb(); __arch_putw(__v,c); __v; })
#define writel(v,c) ({ u32 __v = v; __iowmb(); __arch_putl(__v,c); __v; })

#define readb(c)    ({ u8  __v = __arch_getb(c); __iormb(); __v; })
#define readw(c)    ({ u16 __v = __arch_getw(c); __iormb(); __v; })
#define readl(c)    ({ u32 __v = __arch_getl(c); __iormb(); __v; })

IMPORT  UINT32 __inline__GetCntFreq (void);
IMPORT  UINT32 __inline__GetVirtTimerValue (void);
IMPORT  UINT32 __inline__GetVirtTimerCtrl (void);
IMPORT  void __inline__SetVirtTimerValue (UINT32 val);
IMPORT  void __inline__SetVirtTimerCtrl (UINT32 val);
IMPORT  UINT32 __inline__GetPhyTimerValue (void);
IMPORT  UINT32 __inline__GetPhyTimerCtrl (void);
IMPORT  void __inline__SetPhyTimerValue (UINT32 val);
IMPORT  void __inline__SetPhyTimerCtrl (UINT32 val);
IMPORT  UINT64 __inline__GetVirtTimerCnt(void);
IMPORT  UINT64 __inline__GetPhyTimerCnt(void);
IMPORT int uartf(char *  fmt, ...);

UINT8 regRead8 (UINT32 addr);
void regWrite8 (UINT32 addr, UINT8 value);
UINT16 regRead16 (UINT32 addr);
void regWrite16 (UINT32 addr, UINT16 value);
UINT32 regRead32 (UINT32 addr);
void regWrite32 (UINT32 addr, UINT32 value);

#ifdef __cplusplus
}
#endif
#endif  /* __INCdefsh */

