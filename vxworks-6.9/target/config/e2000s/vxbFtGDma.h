/* vxbFtGDMA.h - Driver for general DMA controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbFtGDmah
#define __INCvxbFtGDmah

#ifdef __cplusplus
extern "C" {
#endif

#include <lstLib.h>
#include <semLib.h>

/* DMA registers, fields and masks */

/* DMA register offsets */

#define DMAC_MAX_CHANNELS       16
#define DMAC_BDL_NUM            128
#define DMAC_BDL_ALIGN          0x80

#define CHAN_REG_LEN            0x60
#define DMA_REG_LEN             0x20

#define DMA_CTL                 0x00
#define DMA_STAT                0x04 
#define DMA_INTR_CTL            0x08 
#define DMA_LP_CTL              0x0C     /* channel low power control*/
#define DMA_QOS_CFG             0x10

#define DMA_CX_CTL              0x00
#define DMA_CX_MODE             0x04
#define DMA_CX_INTR_CTL         0x08
#define DMA_CX_STAT             0x0C     
#define DMA_CX_LVI              0x10     /* last valid index*/
#define DMA_CX_TS               0x14     /* total data length when in driect mode*/
#define DMA_CX_UPSADDR          0x18
#define DMA_CX_LWSADDR          0x1C
#define DMA_CX_UPDADDR          0x20
#define DMA_CX_LWDADDR          0x24
#define DMA_CX_XFER_CFG         0x28
#define DMA_CX_LCP              0x2C
#define DMA_CX_SECCTL           0x30
#define DMA_CX_AW_CFG           0x3C
#define DMA_CX_AR_CFG           0x40

#define BIT(bitNumber)          (1 << (bitNumber))
#define DMA_CTL_EN              BIT(0)
#define DMA_CTL_SRST            BIT(1)
#define DMA_CTL_WR_ARB          BIT(4)
#define DMA_CTL_RD_ARB          BIT(5)
#define DMA_OT_CTL              (((unsigned int)x & 0xF) << 8)

#define DMA_STAT_CHAL(id)       (0x01 << id)

#define DMA_INTR_CHAL(id)       (0x01 << id)
#define DMA_INTR_GEN            BIT(31)

#define DMA_LP_CLK(id)          (0x01 << id)   /* Close the clk of channel*/

#define DMA_QOS_AW(x)           ((unsigned int)x & 0xF)  
#define DMA_QOS_AR(x)           (((unsigned int)x & 0xF) << 4)  

#define DMA_CX_EN               BIT(0)
#define DMA_CX_SRST             BIT(1)

#define DMA_CX_BDL              BIT(0)
#define DMA_CX_BDL_ROLL         BIT(4)
#define DMA_CX_WR_QOS_EN        BIT(8)
#define DMA_CX_WR_QOS(x)        (((unsigned int)x & 0xF) << 12)  
#define DMA_CX_RD_QOS_EN        BIT(16)
#define DMA_CX_RD_QOS(x)        (((unsigned int)x & 0xF) << 20)  

/* For dma_cx_state & dma_cx_intr_ctl */
#define DMA_CX_FIFO_EMPTY       BIT(0) 
#define DMA_CX_FIFO_FULL        BIT(1) 
#define DMA_CX_BDL_END          BIT(2) 
#define DMA_CX_TRANS_END        BIT(3) 
#define DMA_CX_BUSY             BIT(4)

#define DMA_CX_WRBURST(x)       ((unsigned int)x & 0x3) 
#define DMA_CX_WRSIZE(x)        (((unsigned int)x & 0x7) << 4)   
#define DMA_CX_WRLEN(x)         (((unsigned int)x & 0xFF) << 8)   
#define DMA_CX_RDBURST(x)       (((unsigned int)x & 0x3) << 16)   
#define DMA_CX_RDSIZE(x)        (((unsigned int)x & 0x7) << 20)   
#define DMA_CX_RDLEN(x)         (((unsigned int)x & 0xFF) << 24)  

#define DMA_INCR                1
#define DMA_FIX                 0

#define DMA_EVENTS_ERROR_BIT_OFFSET    DMAC_MAX_CHANNELS
#define DMA_EXC_TASK_PRI        60

#define DMA_BAR(p)      (((FT_GDMA_DRV_CTRL *)(p)->pDrvCtrl)->regBase)
#define DMA_HANDLE(p)   (((FT_GDMA_DRV_CTRL *)(p)->pDrvCtrl)->handle)

#define DMA_READ_32(pDev, addr)                     \
            vxbRead32(DMA_HANDLE(pDev),             \
                      (UINT32 *)((char *)DMA_BAR(pDev) + addr))
#define DMA_WRITE_32(pDev, addr, data)              \
            vxbWrite32(DMA_HANDLE(pDev),            \
                      (UINT32 *)((char *)DMA_BAR(pDev) + addr), data)

/* 
 * For eight-channel type DMA engine, the channels are made up two groups, four
 * channels in each group. In the same group, channel register bank offset is 0x80,
 * but between the two group, the offset is 0x180. The following macro is used to
 * fit for this characteristic.
 */

#define DMA_CHAN_REG_READ(pDev, chan, reg)          \
            DMA_READ_32(pDev, (reg + DMA_REG_LEN + chan * CHAN_REG_LEN))
#define DMA_CHAN_REG_WRITE(pDev, chan, reg, value)  \
            DMA_WRITE_32(pDev, (reg + DMA_REG_LEN + chan * CHAN_REG_LEN), value)

#define CHAN_SYNC_TAKE semTake(pChan->mutex, WAIT_FOREVER)
#define CHAN_SYNC_GIVE (void)semGive(pChan->mutex)

/* type define */
enum dmaTransferDirection {
    DMA_MEM_TO_MEM,
    DMA_MEM_TO_DEV,
    DMA_DEV_TO_MEM,
    DMA_DEV_TO_DEV,
    DMA_TRANS_NONE,
};
enum dmaSlaveBuswidth {
    DMA_SLAVE_BUSWIDTH_UNDEFINED = 0,
    DMA_SLAVE_BUSWIDTH_1_BYTE = 1,
    DMA_SLAVE_BUSWIDTH_2_BYTES = 2,
    DMA_SLAVE_BUSWIDTH_4_BYTES = 4,
    DMA_SLAVE_BUSWIDTH_8_BYTES = 8,
    DMA_SLAVE_BUSWIDTH_16_BYTES = 16,
};
typedef struct gdmaSlaveConfig {
    enum dmaTransferDirection direction;
    enum dmaSlaveBuswidth src_addr_width;
    enum dmaSlaveBuswidth dst_addr_width;
    UINT32 src_maxburst;
    UINT32 dst_maxburst;
    UINT32 priority;
}GDMA_SLAVE_CONFIG;

typedef struct phytium_gdma_bdl_desc {
    UINT32 desc_src_addr_l;
    UINT32 desc_src_addr_h;
    UINT32 desc_dst_addr_l;
    UINT32 desc_dst_addr_h;
    UINT32 desc_src_tc;
    UINT32 desc_dst_tc;
    UINT32 desc_data_len;
    UINT32 desc_rise_irq;
}FT_GDMA_BDL_DESC;

typedef struct phytium_gdma_desc {
    FT_GDMA_BDL_DESC *bdldesc;
    UINT32    dma_src_addr_l;
    UINT32    dma_src_addr_h;
    UINT32    dma_dst_addr_l;          /* For direct*/
    UINT32    dma_dst_addr_h;          /* For direct*/
    UINT32    dma_bdl_cnt;             /* For bdl*/
    UINT32    data_len;                 /* For direct*/
    UINT32    axi_burst;          
    UINT32    chan_priority;           
    UINT32    dma_mode;                 /* bdl or direct*/
}FT_GDMA_DESC;   

typedef struct phytium_gdma_chan {
    UINT8               id;
    SEM_ID              mutex;
    VXB_DMA_RESOURCE_ID pRes;
    GDMA_SLAVE_CONFIG*  dma_sconfig;
    FT_GDMA_DESC *      chan_desc;
    pVXB_DMA_COMPLETE_FN callback;
    void*               pArg;
}FT_GDMA_CHAN;  
     
typedef struct ftGdmaDevice
{
    VXB_DEVICE_ID       vxbDev;
    VOID *              regBase;
    VOID *              handle;     /*type of base, mem or io*/
    FT_GDMA_CHAN **     pChan;
    FT_GDMA_BDL_DESC *  pDesc;
    UINT32              chanNum;
    SEM_ID              ftDmaSemId;
    SEM_ID              ftDmaSyncSem;
    atomicVal_t         ftDmaEvents;
    BOOL                dmaInitialized;
    TASK_ID             ftDmaExcId;
    int                 ftDmaPhase;
    BOOL                polling;    
} FT_GDMA_DRV_CTRL;

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* __INCvxbFtGDmah */

