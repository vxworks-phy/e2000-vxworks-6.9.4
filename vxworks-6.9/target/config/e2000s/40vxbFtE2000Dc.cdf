/* 40vxbFtE2000Dc.cdf - E2000 DC configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */



Component   DRV_FTE2000_DC {
    NAME        FT E2000 Dc Driver
    SYNOPSIS    FT E2000 Dc Driver
    _CHILDREN   FOLDER_DRIVERS
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    ftE2000DcDevicePciRegister();
    PROTOTYPE   void ftE2000DcDevicePciRegister (void);
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS 
   INIT_AFTER   INCLUDE_VXBUS
}

