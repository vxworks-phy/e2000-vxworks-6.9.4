/* vxbFtKeypad.h - keypad definations header */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbFtKeypad_h
#define __INCvxbFtKeypad_h

#ifdef __cplusplus
extern "C" {
#endif

#define FT_KEYPAD_DRIVER_NAME              "ftKeypad"

/*
 * Keys and buttons
 *
 * Most of the keys/buttons are modeled after USB HUT 1.12
 * (see http://www.usb.org/developers/hidpage).
 * Abbreviations in the comments:
 * AC - Application Control
 * AL - Application Launch Button
 * SC - System Control
 */

#define KEY_RESERVED		0
#define KEY_ESC			1
#define KEY_1			2
#define KEY_2			3
#define KEY_3			4
#define KEY_4			5
#define KEY_5			6
#define KEY_6			7
#define KEY_7			8
#define KEY_8			9
#define KEY_9			10
#define KEY_0			11
#define KEY_MINUS		12
#define KEY_EQUAL		13
#define KEY_BACKSPACE		14
#define KEY_TAB			15
#define KEY_Q			16
#define KEY_W			17
#define KEY_E			18
#define KEY_R			19
#define KEY_T			20
#define KEY_Y			21
#define KEY_U			22
#define KEY_I			23
#define KEY_O			24
#define KEY_P			25
#define KEY_LEFTBRACE		26
#define KEY_RIGHTBRACE		27
#define KEY_ENTER		28
#define KEY_LEFTCTRL		29
#define KEY_A			30
#define KEY_S			31
#define KEY_D			32
#define KEY_F			33
#define KEY_G			34
#define KEY_H			35
#define KEY_J			36
#define KEY_K			37
#define KEY_L			38
#define KEY_SEMICOLON		39
#define KEY_APOSTROPHE		40
#define KEY_GRAVE		41
#define KEY_LEFTSHIFT		42
#define KEY_BACKSLASH		43
#define KEY_Z			44
#define KEY_X			45
#define KEY_C			46
#define KEY_V			47
#define KEY_B			48
#define KEY_N			49
#define KEY_M			50
#define KEY_COMMA		51
#define KEY_DOT			52
#define KEY_SLASH		53
#define KEY_RIGHTSHIFT		54
#define KEY_KPASTERISK		55
#define KEY_LEFTALT		56
#define KEY_SPACE		57
#define KEY_CAPSLOCK		58
#define KEY_F1			59
#define KEY_F2			60
#define KEY_F3			61
#define KEY_F4			62
#define KEY_F5			63
#define KEY_F6			64
#define KEY_F7			65
#define KEY_F8			66
#define KEY_F9			67
#define KEY_F10			68
#define KEY_NUMLOCK		69
#define KEY_SCROLLLOCK		70
#define KEY_KP7			71
#define KEY_KP8			72
#define KEY_KP9			73
#define KEY_KPMINUS		74
#define KEY_KP4			75
#define KEY_KP5			76
#define KEY_KP6			77
#define KEY_KPPLUS		78
#define KEY_KP1			79
#define KEY_KP2			80
#define KEY_KP3			81
#define KEY_KP0			82
#define KEY_KPDOT		83

#define KEY_ZENKAKUHANKAKU	85
#define KEY_102ND		86
#define KEY_F11			87
#define KEY_F12			88
#define KEY_RO			89
#define KEY_KATAKANA		90
#define KEY_HIRAGANA		91
#define KEY_HENKAN		92
#define KEY_KATAKANAHIRAGANA	93
#define KEY_MUHENKAN		94
#define KEY_KPJPCOMMA		95
#define KEY_KPENTER		96
#define KEY_RIGHTCTRL		97
#define KEY_KPSLASH		98
#define KEY_SYSRQ		99
#define KEY_RIGHTALT		100
#define KEY_LINEFEED		101
#define KEY_HOME		102
#define KEY_UP			103
#define KEY_PAGEUP		104
#define KEY_LEFT		105
#define KEY_RIGHT		106
#define KEY_END			107
#define KEY_DOWN		108
#define KEY_PAGEDOWN		109
#define KEY_INSERT		110
#define KEY_DELETE		111
#define KEY_MACRO		112
#define KEY_MUTE		113
#define KEY_VOLUMEDOWN		114
#define KEY_VOLUMEUP		115
#define KEY_POWER		116	/* SC System Power Down */
#define KEY_KPEQUAL		117
#define KEY_KPPLUSMINUS		118
#define KEY_PAUSE		119
#define KEY_SCALE		120	/* AL Compiz Scale (Expose) */

#define KEY_KPCOMMA		121
#define KEY_HANGEUL		122
#define KEY_HANGUEL		KEY_HANGEUL
#define KEY_HANJA		123
#define KEY_YEN			124
#define KEY_LEFTMETA		125
#define KEY_RIGHTMETA		126
#define KEY_COMPOSE		127

#define KEY_STOP		128	/* AC Stop */
#define KEY_AGAIN		129
#define KEY_PROPS		130	/* AC Properties */
#define KEY_UNDO		131	/* AC Undo */
#define KEY_FRONT		132
#define KEY_COPY		133	/* AC Copy */
#define KEY_OPEN		134	/* AC Open */
#define KEY_PASTE		135	/* AC Paste */
#define KEY_FIND		136	/* AC Search */
#define KEY_CUT			137	/* AC Cut */
#define KEY_HELP		138	/* AL Integrated Help Center */
#define KEY_MENU		139	/* Menu (show menu) */
#define KEY_CALC		140	/* AL Calculator */
#define KEY_SETUP		141
#define KEY_SLEEP		142	/* SC System Sleep */
#define KEY_WAKEUP		143	/* System Wake Up */
#define KEY_FILE		144	/* AL Local Machine Browser */
#define KEY_SENDFILE		145
#define KEY_DELETEFILE		146
#define KEY_XFER		147
#define KEY_PROG1		148
#define KEY_PROG2		149
#define KEY_WWW			150	/* AL Internet Browser */
#define KEY_MSDOS		151
#define KEY_COFFEE		152	/* AL Terminal Lock/Screensaver */
#define KEY_SCREENLOCK		KEY_COFFEE
#define KEY_ROTATE_DISPLAY	153	/* Display orientation for e.g. tablets */
#define KEY_DIRECTION		KEY_ROTATE_DISPLAY
#define KEY_CYCLEWINDOWS	154
#define KEY_MAIL		155
#define KEY_BOOKMARKS		156	/* AC Bookmarks */
#define KEY_COMPUTER		157
#define KEY_BACK		158	/* AC Back */
#define KEY_FORWARD		159	/* AC Forward */
#define KEY_CLOSECD		160
#define KEY_EJECTCD		161
#define KEY_EJECTCLOSECD	162
#define KEY_NEXTSONG		163
#define KEY_PLAYPAUSE		164
#define KEY_PREVIOUSSONG	165
#define KEY_STOPCD		166
#define KEY_RECORD		167
#define KEY_REWIND		168
#define KEY_PHONE		169	/* Media Select Telephone */
#define KEY_ISO			170
#define KEY_CONFIG		171	/* AL Consumer Control Configuration */
#define KEY_HOMEPAGE		172	/* AC Home */
#define KEY_REFRESH		173	/* AC Refresh */
#define KEY_EXIT		174	/* AC Exit */
#define KEY_MOVE		175
#define KEY_EDIT		176
#define KEY_SCROLLUP		177
#define KEY_SCROLLDOWN		178
#define KEY_KPLEFTPAREN		179
#define KEY_KPRIGHTPAREN	180
#define KEY_NEW			181	/* AC New */
#define KEY_REDO		182	/* AC Redo/Repeat */

#define KEY_F13			183
#define KEY_F14			184
#define KEY_F15			185
#define KEY_F16			186
#define KEY_F17			187
#define KEY_F18			188
#define KEY_F19			189
#define KEY_F20			190
#define KEY_F21			191
#define KEY_F22			192
#define KEY_F23			193
#define KEY_F24			194

#define KEY_PLAYCD		200
#define KEY_PAUSECD		201
#define KEY_PROG3		202
#define KEY_PROG4		203
#define KEY_DASHBOARD		204	/* AL Dashboard */
#define KEY_SUSPEND		205
#define KEY_CLOSE		206	/* AC Close */
#define KEY_PLAY		207
#define KEY_FASTFORWARD		208
#define KEY_BASSBOOST		209
#define KEY_PRINT		210	/* AC Print */
#define KEY_HP			211
#define KEY_CAMERA		212
#define KEY_SOUND		213
#define KEY_QUESTION		214
#define KEY_EMAIL		215
#define KEY_CHAT		216
#define KEY_SEARCH		217
#define KEY_CONNECT		218
#define KEY_FINANCE		219	/* AL Checkbook/Finance */
#define KEY_SPORT		220
#define KEY_SHOP		221
#define KEY_ALTERASE		222
#define KEY_CANCEL		223	/* AC Cancel */
#define KEY_BRIGHTNESSDOWN	224
#define KEY_BRIGHTNESSUP	225
#define KEY_MEDIA		226

#define KEY_SWITCHVIDEOMODE	227	/* Cycle between available video
					   outputs (Monitor/LCD/TV-out/etc) */
#define KEY_KBDILLUMTOGGLE	228
#define KEY_KBDILLUMDOWN	229
#define KEY_KBDILLUMUP		230

#define KEY_SEND		231	/* AC Send */
#define KEY_REPLY		232	/* AC Reply */
#define KEY_FORWARDMAIL		233	/* AC Forward Msg */
#define KEY_SAVE		234	/* AC Save */
#define KEY_DOCUMENTS		235

#define KEY_BATTERY		236

#define KEY_BLUETOOTH		237
#define KEY_WLAN		238
#define KEY_UWB			239

#define KEY_UNKNOWN		240

#define KEY_VIDEO_NEXT		241	/* drive next video source */
#define KEY_VIDEO_PREV		242	/* drive previous video source */
#define KEY_BRIGHTNESS_CYCLE	243	/* brightness up, after max is min */
#define KEY_BRIGHTNESS_AUTO	244	/* Set Auto Brightness: manual
					  brightness control is off,
					  rely on ambient */
#define KEY_BRIGHTNESS_ZERO	KEY_BRIGHTNESS_AUTO
#define KEY_DISPLAY_OFF		245	/* display device to off state */

#define KEY_WWAN		246	/* Wireless WAN (LTE, UMTS, GSM, etc.) */
#define KEY_WIMAX		KEY_WWAN
#define KEY_RFKILL		247	/* Key that controls all radios */

#define KEY_MICMUTE		248	/* Mute / unmute the microphone */

/* Code 255 is reserved for special needs of AT keyboard driver */


#define AS	0	/* normal character index */
#ifdef SH
#undef SH
#endif
#define SH	1	/* shift index */
#define CN	2	/* control index */
#define NM	3	/* numeric lock index */
#define CP	4	/* capslock index */
#define ST	5	/* stop output index */
#define EX	6	/* extended code index */
#define ES	7	/* escape and extended code index */

/* Keyboard special key flags */

#define NORMAL		0x0000		/* normal key */
#define STP		0x0001  	/* capslock flag */
#define NUM		0x0002  	/* numeric lock flag */
#define CAPS		0x0004  	/* scroll lock stop output flag */
#define SHIFT		0x0008  	/* shift flag */
#define CTRL		0x0010  	/* control flag */
#define EXT		0x0020  	/* extended scan code 0xe0 */
#define ESC		0x0040  	/* escape key press */
#define	EW		EXT|ESC 	/* escape and Extend */
#define E1		0x0080  	/* extended scan code with 0xE1 */
#define PRTSC		0x0100  	/* print screen flag */
#define BRK		0x0200		/* make break flag for keyboard */

#define JAPANES_KBD		0
#define ENGLISH_KBD		1
/*
 * Keypad Controller registers
 */
 
#define KPCR        0x00 /* Keypad Control Register */

#define KPSR        0x04 /* Keypad Status Register */
#define KBD_STAT_KPKD   (0x1 << 0) /* Key Press Interrupt Status bit (w1c) */
#define KBD_STAT_KPKR   (0x1 << 1) /* Key Release Interrupt Status bit (w1c) */
#define KBD_STAT_KDSC   (0x1 << 2) /* Key Depress Synch Chain Status bit (w1c)*/
#define KBD_STAT_KRSS   (0x1 << 3) /* Key Release Synch Status bit (w1c)*/
#define KBD_STAT_KDIE   (0x1 << 8) /* Key Depress Interrupt Enable Status bit */
#define KBD_STAT_KRIE   (0x1 << 9) /* Key Release Interrupt Enable */

#define KDDR        0x08 /* Keypad Data Direction Register */
#define KPDR        0x0C /* Keypad Data Register */

#define MAX_MATRIX_KEY_ROWS 8
#define MAX_MATRIX_KEY_COLS 8

#define MAX_MATRIX_KEY_NUM  (MAX_MATRIX_KEY_ROWS * MAX_MATRIX_KEY_COLS)

/*
 * The matrix is stable only if no changes are detected after
 * PHYTIUM_KEYPAD_SCANS_FOR_STABILITY scans
 */
 
#define PHYTIUM_KEYPAD_SCANS_FOR_STABILITY 3
#define PHYTIUM_KEYPAD_SCANS_FOR_LONG      10

#define MATRIX_MAX_ROWS     32
#define MATRIX_MAX_COLS     32

#define KEY(row, col, val)  ((((row) & (MATRIX_MAX_ROWS - 1)) << 24) |\
                 (((col) & (MATRIX_MAX_COLS - 1)) << 16) |\
                 ((val) & 0xffff))

#define KEY_RESERVED        0
#define KEY_STAT_RELEASED   0 /* 0:released */

#define KEY_ROW(k)      (((k) >> 24) & 0xff)
#define KEY_COL(k)      (((k) >> 16) & 0xff)
#define KEY_VAL(k)      ((k) & 0xffff)

#define MATRIX_SCAN_CODE(row, col, rowShift)   (((row) << (rowShift)) + (col))


typedef struct ftKeypadDrvCtrl
    {
    TY_DEV      tyDev;       /* DEV_HDR must be the first member! */

    VXB_DEVICE_ID           pInst;       /* pointer to the controller instance */
    void *                  bar;
    void *                  handle;      /* handle of Keypad vxbRead/vxbWrite */

    WDOG_ID     wdCheckMatrix;
    int         stableCount;
    BOOL        enabled;
    UINT32      rows;
    UINT32      cols;
    int         rowShift;

    /* Masks for enabled rows/cols */
    
    UINT16      rowsEnMask;
    UINT16      colsEnMask;

    UINT16      keycodes[MAX_MATRIX_KEY_NUM];
    UINT16      keycodesize;

    /*
     * Matrix states:
     * -stable: achieved after a complete debounce process.
     * -unstable: used in the debouncing process.
     */
     
    UINT16      matrixStableState[MAX_MATRIX_KEY_COLS];
    UINT16      matrixUnstableState[MAX_MATRIX_KEY_COLS];

    /* Function that report key to application */
    
    void     (*inputReportKey)(VXB_DEVICE_ID pDev, UINT16 key, UINT16 keyStat);
    SEM_ID      accessSemId;
    } FT_KEYPAD_DRVCTRL;          

#define KPD_BAR(p)        ((FT_KEYPAD_DRVCTRL *)(p)->pDrvCtrl)->bar
#define KPD_HANDLE(p)     ((FT_KEYPAD_DRVCTRL *)(p)->pDrvCtrl)->handle

#define CSR_READ_4(pDev, addr)            \
        vxbRead32(KPD_HANDLE(pDev),       \
                 (UINT32 *)((char *)KPD_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)     \
        vxbWrite32(KPD_HANDLE(pDev),      \
                  (UINT32 *)((char *)KPD_BAR(pDev) + addr), data)

#ifdef __cplusplus
}
#endif
#endif  /* __INCvxbFtKeypad_h */

