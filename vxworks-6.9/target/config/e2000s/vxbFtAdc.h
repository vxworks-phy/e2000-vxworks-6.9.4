/* vxbFtAdc.h - ADC driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbftadch
#define __INCvxbftadch

#ifdef __cplusplus
extern "C" {
#endif

#include <ioLib.h>

#define ADC_MAX_CTL 2
#define ADC_CHANNEL_NUM 8

/* ADC register */
#define ADC_CTRL_REG                0x0000
#define ADC_INTER_REG               0x0004
#define ADC_STATE_REG               0x0008
#define ADC_ERRCLR_REG              0x000c
#define ADC_LEVEL_REG(x)            (0x0010+(x)*4)
#define ADC_INTRMASK_REG            0x0030
#define ADC_INTR_REG                0x0034
#define ADC_COV_RESULT_REG(x)       (0x0038+(x)*4)
#define ADC_FINISH_CNT_REG(x)       (0x0058+(x)*4)
#define ADC_HIS_LIMIT_REG(x)        (0x0078+(x)*4)

/*#define BIT(x)                          (1 << (x))*/
#define ADC_CTRL_REG_PD_EN              BIT(31)
#define ADC_CTRL_REG_CH_ONLY_S(x)       ((x)<<16)
#define ADC_CTRL_REG_CLK_DIV(x)         ((x)<<12)
#define ADC_CTRL_REG_CHANNEL_EN(x)      BIT((x)+4)
#define ADC_CTRL_REG_CH_ONLY_EN         BIT(3)
#define ADC_CTRL_REG_SINGLE_EN          BIT(2)
#define ADC_CTRL_REG_SINGLE_SEL         BIT(1)
#define ADC_CTRL_REG_SOC_EN             BIT(0)

#define ADC_STATE_REG_B_STA(x)            ((x)<<8)
#define ADC_STATE_REG_EOC_STA             BIT(7)
#define ADC_STATE_REG_S_STA(x)            ((x)<<4)
#define ADC_STATE_REG_SOC_STA             BIT(3)
#define ADC_STATE_REG_ERR_STA             BIT(2)
#define ADC_STATE_REG_COV_FINISH_STA      BIT(1)
#define ADC_STATE_REG_ADCCTL_BUSY_STA     BIT(0)

#define ADC_LEVEL_REG_HIGH_LEVEL(x)         ((x)<<16)
#define ADC_LEVEL_REG_LOW_LEVEL(x)          ((x)<<0)    
    
#define ADC_INTRMASK_REG_ERR_INTR_MASK        BIT(24)
#define ADC_INTRMASK_REG_ULIMIT_MASK(x)       BIT((x)*2+9)
#define ADC_INTRMASK_REG_DLIMIT_MASK(x)       BIT((x)*2+8)
#define ADC_INTRMASK_REG_COVFIN_MASK(x)       BIT((x))

#define ADC_INTR_REG_ERR             BIT(24)
#define ADC_INTR_REG_ULIMIT(x)       BIT((x)*2+9)
#define ADC_INTR_REG_DLIMIT(x)       BIT((x)*2+8)
#define ADC_INTR_REG_COVFIN(x)       BIT((x))

#define ADC_IOCTL_BASE               100 
#define ADC_IOCTL_MODE               (ADC_IOCTL_BASE + 0)
#define ADC_IOCTL_CHAN_MASK          (ADC_IOCTL_BASE + 1)
#define ADC_IOCTL_NUMBER_OF_CONV     (ADC_IOCTL_BASE + 2)
#define ADC_IOCTL_START              (ADC_IOCTL_BASE + 3)
#define ADC_IOCTL_STOP               (ADC_IOCTL_BASE + 4)
#define ADC_IOCTL_INTERVAL           (ADC_IOCTL_BASE + 5)
#define ADC_IOCTL_LEVEL              (ADC_IOCTL_BASE + 6)
#define ADC_IOCTL_RECV_RTN           (ADC_IOCTL_BASE + 7)

#define ADC_FIXED_NUMBER_OF_CONVERT_MODE     0
#define ADC_CONTINUOUS_CONVERT_MODE          1

typedef union
    {
    UINT32 data;
    struct
        {
        UINT32 lowLevel :10;        /* 9:0   */
        UINT32 dummy :6;            /* 15:10   */     
        UINT32 highLevel :10;       /* 25:16  */  
        UINT32 dummy1 :3;           /* 28:26 */  
        UINT32 chanNo :3;           /* 31:29 */                
        } adcLevel;
    } ADC_LEVEL_REG_CFG; 

typedef union
    {
    UINT32 data;
    void (*recvProcRtn)(int channel, UINT16 value);
    } ADC_RECV_RTN_CFG; 

typedef struct ftAdcIODevice
    {
    DEV_HDR     devHdr;         /* I/O device header */
    int         devIoDrvNum;
    } FTADC_IO_DEV;

typedef struct ftAdcDrvCtrl
    {
    FTADC_IO_DEV    ftAdcIoDev;
    VXB_DEVICE_ID   ftAdcDev;
    void *      ftAdcHandle;
    void *      ftAdcRegbase;
    TASK_ID     tid;
    SEM_ID      accessSemId;
    SEM_ID      sampleSemId;
    SEM_ID      rdTskSemId;
    UINT32      mode;    
    BOOL        singleEn;
    atomicVal_t adcEvents;
    UINT16      rawVal[ADC_CHANNEL_NUM];
    UINT16 *    pChanBuf[ADC_CHANNEL_NUM];
    UINT16      pChanConvCnt[ADC_CHANNEL_NUM];
    UINT32      numberOfConv;
    UINT8       activeChanNo[ADC_CHANNEL_NUM];    
    UINT8       activeChanNum;
    void        (*recvProcRtn)(int channel, UINT16 value);
    } FTADC_DRV_CTRL;    
    
    
#define FTADC_BASE(p)   ((FTADC_DRV_CTRL *)(p)->pDrvCtrl)->ftAdcRegbase
#define FTADC_HANDLE(p)   ((FTADC_DRV_CTRL *)(p)->pDrvCtrl)->ftAdcHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FTADC_HANDLE(pDev), (UINT32 *)((char *)FTADC_BASE(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FTADC_HANDLE(pDev),                             \
        (UINT32 *)((char *)FTADC_BASE(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (UINT32)(~(val)))
    

#ifdef __cplusplus
}
#endif

#endif /*end of __INCvxbftadch*/
