/* ftScmiDrv.h - SCMI driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INftScmiDrvh
#define __INftScmiDrvh

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*
* ftScmiGetSensorInfo -  Get sensor infomation
*
* This function get the sensor infomation that contains every sonsor's ID and name 
*
* PARAMETER: N/A
* 
* RETURNS: OK/ERROR
*/

int ftScmiGetSensorInfo(void);

/*****************************************************************************
*
* ftScmiGetTemp -  get the temperature sensor value
*
* This function get the temperature sensor value
*    
* PARAMETER: 
*          sensorId : temperature sensor id
* 
* RETURNS: temperature 
*/
long long ftScmiGetTemp(UINT32 sensorId);

/*****************************************************************************
*
* ftScmiGetPerfInfo -  Get performance domain infomation
*
* This function get the performance domain infomation that contains 
* every performance domain's name and performance level list.
*
* PARAMETER: N/A
* 
* RETURNS: OK/ERROR
*/

int ftScmiGetPerfInfo(void);

/*****************************************************************************
*
* ftScmiSetFreq -  set the frequency of a given performance domain
*
* This function set the frequency by the performance level index  
*    
* PARAMETER: domain : performance domain(cluster number)
*            perfLevel£ºperformance level index
*                      (the frequency of a performance level can be find in performance level list
*                        which is printed by ftScmiGetPerfInfo()) 
* 
* RETURNS: OK/ERROR
*/

int ftScmiSetFreq(UINT32 domain, UINT32 perfLevel);

/*****************************************************************************
*
* ftScmiGetFreq -  get the frequency of a given performance domain
*
* This function get the frequency of a given performance domain
*    
* PARAMETER: 
*          domain : performance domain(cluster number)
* 
* RETURNS: frequency
*/
UINT32 ftScmiGetFreq(UINT32 domain);

#ifdef __cplusplus
}
#endif

#endif /* __INftScmiDrvh */
