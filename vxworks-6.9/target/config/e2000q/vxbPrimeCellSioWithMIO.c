/* vxbPrimeCellSio.c - ARM AMBA UART tty driver for VxBus */

/*
 * Copyright (c) 2007-2010, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01p,12jul13,m_h  Fix potential unitialized variable pHcf for initial baudrate
01o,22feb13,m_h  Allow BSP to specify initial baud rate as resource setting
01n,01apr13,c_l  Remove build warnings. (WIND00409411)
01m,01nov10,x_z  Replaced legacy mode with vxBus mode for register and added
                 support for LSI ACP34XX and kprintf().
01l,12oct10,my_  Enable Tx Interrupt in IOCTL (WIND00232106)
01k,04aug10,my_  do not enable isr until isr defer queue is ready
                 (WIND00202314)
01j,20jul10,fao  Add be8 support
01i,28oct09,z_l  Clear the pending interrupt in time. This will avoid system
                 hang when a long string is pasted in input direction.
01h,15apr09,h_k  updated for kprintf() support in SMP. (CQ:158523)
01g,27aug08,jpb  Renamed VSB header file
01f,18jun08,jpb  Renamed _WRS_VX_SMP to _WRS_CONFIG_SMP.  Added include path
                 for kernel configurations options set in vsb.
01e,05may08,tor  update version
01d,06mar08,tor  update driver meta-data
01c,20sep07,tor  VXB_VERSION_3
01b,07may07,h_k  added an extra arg isrDeferQueueGet() for a future support.
01a,25apr07,h_k  written based on arm_ebmpcore/pimeCellSio.c 1b.
*/

/*
DESCRIPTION
This is the device driver for the Advanced RISC Machines (ARM) AMBA
UART. This is a generic design of UART used within a number of chips
containing (or for use with) ARM CPUs

This design contains a universal asynchronous receiver/transmitter, a
baud-rate generator, and an InfraRed Data Association (IrDa) Serial
InfraRed (SiR) protocol encoder. The Sir encoder is not supported by
this driver. The UART contains two 16-entry deep FIFOs for receive and
transmit: if a framing, overrun or parity error occurs during
reception, the appropriate error bits are stored in the receive FIFO
along with the received data. The FIFOs can be programmed to be one
byte deep only, like a conventional UART with double buffering, but the
only mode of operation supported is with the FIFOs enabled.

The UART design does not support the modem control output signals: DTR,
RI and RTS. Moreover, the implementation in the 21285 chip does not
support the modem control inputs: DCD, CTS and DSR.

The UART design can generate four interrupts: Rx, Tx, modem status
change and a UART disabled interrupt (which is asserted when a start
bit is detected on the receive line when the UART is disabled). The
implementation in the 21285 chip has only two interrupts: Rx and Tx,
but the Rx interrupt is a combination of the normal Rx interrupt status
and the UART disabled interrupt status.

Only asynchronous serial operation is supported by the UART which
supports 5 to 8 bit word lengths with or without parity and with
one or two stop bits. The only serial word format supported by the
driver is 8 data bits, 1 stop bit, no parity.

The exact baud rates supported by this driver will depend on the
crystal fitted (and consequently the input clock to the baud-rate
generator), but in general, baud rates from about 300 to about 115200
are possible.

In theory, any number of UART channels could be implemented within a
chip. This driver has been designed to cope with an arbitrary number of
channels, but at the time of writing, has only ever been tested with
one channel.

TARGET-SPECIFIC PARAMETERS
The parameters are provided through ` ' registration defined in
hwconf.c in the target BSP.

\is
\i <regBase>
ARM PrimeCellII UART register base address. Specify serial mode register
address.
The parameter type is HCF_RES_INT.

\i <irq>
Interrupt vector number for the ARM PrimeCellII UART single interrupt source.
The parameter type is HCF_RES_INT.

\i <irqLevel>
Interrupt number for the ARM PrimeCellII UART single interrupt source.
The parameter type is HCF_RES_INT.

\i <pclkFreq>
Clock frequency or routine to get Clock frequency for ARM PrimeCellII UART in Hz.
The parameter type is HCF_RES_INT or HCF_RES_ADDR.
\ie

An VXB_AMBA_CHAN data structure is used to describe each channel, this
structure is described in h/drv/sio/vxbAmbaSio.h.

CALLBACKS
Servicing a "transmitter ready" interrupt involves making a callback to
a higher level library in order to get a character to transmit.  By
default, this driver installs dummy callback routines which do nothing.
A higher layer library that wants to use this driver (e.g. ttyDrv)
will install its own callback routine using the SIO_INSTALL_CALLBACK
ioctl command.  Likewise, a receiver interrupt handler makes a callback
to pass the character to the higher layer library.

MODES
This driver supports both polled and interrupt modes.

INCLUDE FILES:
src/hwif/h/sio/vxbPrimeCellSio.h sioLib.h

SEE ALSO:
\tb Advanced RISC Machines AMBA UART (AP13) Data Sheet,
\tb ARM PrimeCell UART PL011 Technical Reference manual
*/

#include <vxWorks.h>
#include <vsbConfig.h>
#include <sioLib.h>
#include <intLib.h>
#include <errnoLib.h>
#include <vxBusLib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memLib.h>

#ifdef	_WRS_CONFIG_SMP
#include <private/spinLockLibP.h>
#endif	/* _WRS_CONFIG_SMP */

#include <hwif/util/hwMemLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/vxbus/hwConf.h>

#include "../h/util/sioChanUtil.h"
#include "../h/vxbus/vxbAccess.h"
#include "../h/sio/vxbPrimeCellSio.h"

#include "ftPinMux.h"

/* local defines  */

#ifdef ARMBE8
#    define SWAP32 vxbSwap32
#else
#    define SWAP32
#endif /* ARMBE8 */

#define X_DEBUG_MSG(level,fmt,a,b,c,d,e,f)

#define VXB_AMBA_UART_NUM_TTY		10
#define VXB_AMBA_CONSOLE_BAUD_RATE	9600

#ifdef  _WRS_CONFIG_SMP
LOCAL BOOL ambaSioSpinlockFuncReady = FALSE;

#define VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan)                           \
    if (ambaSioSpinlockFuncReady)                                        \
        SPIN_LOCK_ISR_TAKE(&pChan->spinlockIsr)
#define VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan)                           \
    if (ambaSioSpinlockFuncReady)                                        \
        SPIN_LOCK_ISR_GIVE(&pChan->spinlockIsr)
#define VXB_AMBA_SIO_SPIN_LOCK_ISR_HELD(pChan)				\
    (ambaSioSpinlockFuncReady ? spinLockIsrHeld(&pChan->spinlockIsr) : FALSE)
#define VXB_AMBA_SIO_ISR_SET(pChan)                                      \
    SPIN_LOCK_ISR_TAKE(&pChan->spinlockIsr)
#define VXB_AMBA_SIO_ISR_CLEAR(pChan)                                    \
    SPIN_LOCK_ISR_GIVE(&pChan->spinlockIsr)
#define VXB_AMBA_SIO_SPIN_LOCK_READY                                     \
    ambaSioSpinlockFuncReady = TRUE
#else	/* _WRS_CONFIG_SMP */
#define VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan)                           \
    SPIN_LOCK_ISR_TAKE(&pChan->spinlockIsr)
#define VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan)                           \
    SPIN_LOCK_ISR_GIVE(&pChan->spinlockIsr)
#define VXB_AMBA_SIO_SPIN_LOCK_ISR_HELD(pChan)	FALSE
#define VXB_AMBA_SIO_ISR_SET(pChan)
#define VXB_AMBA_SIO_ISR_CLEAR(pChan)
#define VXB_AMBA_SIO_SPIN_LOCK_READY
#endif  /* _WRS_CONFIG_SMP */

#define VXB_AMBA_UART_REG(pChan, reg) \
    (UINT32 *)((UINT32) ((pChan)->pDev->pRegBase[pChan->regIndex]) + (reg))

#define VXB_AMBA_UART_REG_READ(pChan, reg, result) \
    (result) = vxbRead32 ((pChan)->handle, VXB_AMBA_UART_REG(pChan, reg))

#define VXB_AMBA_UART_REG_WRITE(pChan, reg, data) \
    vxbWrite32 ((pChan)->handle, VXB_AMBA_UART_REG(pChan, reg), (data))

#define VXB_AMBA_UART_REG_BIT_SET(pChan, reg, data) \
    VXB_AMBA_UART_REG_WRITE (pChan, reg, \
    vxbRead32 ((pChan)->handle, VXB_AMBA_UART_REG(pChan, reg)) | (data))

#define VXB_AMBA_UART_REG_BIT_CLR(pChan, reg, data) \
    VXB_AMBA_UART_REG_WRITE (pChan, reg, \
    vxbRead32 ((pChan)->handle, VXB_AMBA_UART_REG(pChan, reg)) & (~(data)))

/* externs */

IMPORT int sioNextChannelNumberAssign(void);

/* locals */

/* function prototypes */

LOCAL STATUS vxbAmbaSioDummyCallback (void);
LOCAL void   vxbAmbaSioInitChannel (VXB_AMBA_CHAN * pChan);
LOCAL STATUS vxbAmbaSioIoctl (SIO_CHAN * pSioChan, int request, int arg);
LOCAL int    vxbAmbaSioTxStartup (SIO_CHAN * pSioChan);
LOCAL int    vxbAmbaSioCallbackInstall (SIO_CHAN * pSioChan, int callbackType,
			       STATUS (*callback)(), void * callbackArg);
LOCAL int    vxbAmbaSioPollInput (SIO_CHAN * pSioChan, char *);
LOCAL int    vxbAmbaSioPollOutput (SIO_CHAN * pSioChan, char);
LOCAL void vxbAmbaSioChanGet (VXB_DEVICE_ID pDev, void * pArg);
LOCAL void vxbAmbaSioChanConnect (VXB_DEVICE_ID pDev, void * arg);
LOCAL void vxbAmbaSioInstInit (struct vxbDev * pDev);
LOCAL void vxbAmbaSioInstInit2 (struct vxbDev * pDev);
LOCAL void vxbAmbaSioInstConnect (struct vxbDev * pDev);
LOCAL BOOL vxbAmbaSioProbe (struct vxbDev * pDev);
LOCAL void vxbPrimeCellSioDevInit (VXB_AMBA_CHAN * pChan);
LOCAL void vxbPrimeCellSioInt (VXB_AMBA_CHAN * pChan);
LOCAL void vxbPrimeCellSioIntTx (void * pData);
LOCAL void vxbPrimeCellSioIntRx (void * pData);

LOCAL device_method_t vxbAmbaSio_methods[] =
    {
    DEVMETHOD(sioChanGet, vxbAmbaSioChanGet),
    DEVMETHOD(sioChanConnect, vxbAmbaSioChanConnect),
    { 0, 0}
    };

/* typedefs */

LOCAL struct drvBusFuncs vxbAmbaSioFuncs =
    {
    vxbAmbaSioInstInit,             /* devInstanceInit */
    vxbAmbaSioInstInit2,            /* devInstanceInit2 */
    vxbAmbaSioInstConnect           /* devConnect */
    };

LOCAL struct vxbPlbRegister vxbAmbaSioDevRegistration =
    {
        {
        NULL,                   /* pNext */
        VXB_DEVID_DEVICE,       /* devID */
        VXB_BUSID_PLB,          /* busID = PLB */
        VXB_VER_4_0_0,        /* vxbVersion */
        "primeCellSioDev",      /* drvName */
        &vxbAmbaSioFuncs,       /* pDrvBusFuncs */
        NULL,                   /* pMethods */
        vxbAmbaSioProbe         /* devProbe */
        },
    };

/* driver functions */

LOCAL SIO_DRV_FUNCS vxbAmbaSioDrvFuncs =
    {
    (int (*)())                 vxbAmbaSioIoctl,
    (int (*)())                 vxbAmbaSioTxStartup,
    (int (*)())                 vxbAmbaSioCallbackInstall,
    (int (*)())                 vxbAmbaSioPollInput,
    (int (*)(SIO_CHAN *,char))  vxbAmbaSioPollOutput
    };

/******************************************************************************
*
* vxbPrimeCellSioRegister - register vxbAmbaSio driver
*
* This routine registers the vxbAmbaSio driver and device recognition
* data with the vxBus subsystem.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: N/A
*
* ERRNO
*/

void vxbPrimeCellSioRegister (void)
    {
    vxbDevRegister((struct vxbDevRegInfo *)&vxbAmbaSioDevRegistration);
    }

/******************************************************************************
*
* vxbAmbaSioDevProbe - probe for device presence at specific address
*
* Check for vxbAmbaSio device at the specified base
* address.  We assume one is present at that address, but
* we need to verify.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST* *NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: TRUE if probe passes and assumed a valid vxbAmbaSio
* (or compatible) device.  FALSE otherwise.
*
*/

LOCAL BOOL vxbAmbaSioDevProbe
    (
    struct vxbDev * pDev, /* Device information */
    int regBaseIndex      /* Index of base address */
    )
    {
    /* sanity test */

    if ( pDev->pRegBase[regBaseIndex] == NULL )
        return(FALSE);

    return(TRUE);
    }

/******************************************************************************
*
* vxbAmbaSioProbe - probe for device presence
*
* In pre bus subsystem versions of VxWorks, we assumed the
* configuration stating that a vxbAmbaSio was present at the specified
* address.  With the vxBus subsystem, we continue to demand that
* something or someone decides that the vxbAmbaSio is present at a
* specific address.  However, it is now verified.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: TRUE if probe passes and assumed a valid vxbAmbaSio device.
* FALSE otherwise.
*
* ERRNO
*/

LOCAL BOOL vxbAmbaSioProbe
    (
    struct vxbDev * pDev
    )
    {
    int i;

    for (i = 0; i < VXB_AMBA_UART_NUM_TTY; i++)
        if (vxbAmbaSioDevProbe (pDev, i))
            return(TRUE);

    return (FALSE);
    }

/******************************************************************************
*
* vxbAmbaSioInstInit - initialize amba Sio device
*
* This routine initializes the amba Sio device.  We do not assume a single RS232
* device for this physical device.  Instead, we assume or more devices.  Each
* device uses a separate pRegBase.  For each non-NULL pRegBase, we allocate a
* pChan structure and allocate a unit number with that structure.  In
* vxbAmbaSioInstConnect(), we attach the channel to the I/O system.
* This bypasses other BSP-based configuration mechanisms.
*
* Instead of waiting for vxbAmbaSioInstConnect(), as would be done for typical
* devices, we initialize the serial ports and connect them to the I/O system
* here.
* This allows early console output.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioInstInit
    (
    struct vxbDev * pDev
    )
    {
    VXB_AMBA_CHAN * pChan;
    int i;
    HCF_DEVICE *        pHcf;
    UINT32          (*getFreq) ();

    /*uartf("%s,%d uart NUMBER = %d \r\n", __FUNCTION__, __LINE__, pDev->unitNumber);*/

	if (4 == pDev->unitNumber)
		{
        /*unitNumber is logical, MIO number is physical. They are unrelated.*/
		regWrite32(MIO6_BASE_ADR+0x1000, 1); 

		pinBank1Mio6a();
		pinBank1Mio6b();
		}
		
	if (5 == pDev->unitNumber)
		{
		/*unitNumber is logical, MIO number is physical. They are unrelated.*/
		regWrite32(MIO10_BASE_ADR+0x1000, 1);
		pinBank1Mio10a();
		pinBank1Mio10b();
		}

    if (6 == pDev->unitNumber)
		{
		regWrite32(MIO8_BASE_ADR+0x1000, 1);
		pinBank2Mio8a();
		pinBank2Mio8b();
		}
    if (7 == pDev->unitNumber)
		{
		regWrite32(MIO11_BASE_ADR+0x1000, 1);
		pinBank3Mio11a();
		pinBank3Mio11b();
		}
    if (8 == pDev->unitNumber)
		{
		regWrite32(MIO15_BASE_ADR+0x1000, 1);
		pinBank5Mio15a();
		pinBank5Mio15b();
		}

    /* check each BAR for valid register set, and configure if appropriate */

    for (i = 0; i < VXB_AMBA_UART_NUM_TTY; i++)
        {

        /* check non-NULL pRegBase */

        if ( pDev->pRegBase[i] == NULL )
            continue;

        X_DEBUG_MSG(1, "vxbAmbaSio: probing BAR%d\n",
                    i, 2,3,4,5,6);

        /* check for vxbAmbaSio device present at that address */

        if ( vxbAmbaSioDevProbe(pDev, i) != TRUE )
            continue;

        X_DEBUG_MSG(1, "vxbAmbaSio: BAR%d refers to a valid vxbAmbaSio device\n",
                    i, 2,3,4,5,6);

        /* device present, so allocate pChan */

        pChan = (VXB_AMBA_CHAN *)hwMemAlloc(sizeof(*pChan));

        if (pChan == NULL)
            return;

        bzero((char *)pChan, sizeof(*pChan));

        /*
         * if the device resides on PLB, then retrieve the
         * clock frequency and initial baud from the bsp
         */

        if (pDev->busID == VXB_BUSID_PLB)
            {
            /* get the HCF_DEVICE address */

            pHcf = hcfDeviceGet(pDev);

            if (pHcf == NULL)
                {
                hwMemFree ((char *)pChan);
                return;
                }

	    /* UART_XTAL_FREQ */

            /*
             * resourceDesc {
             * The clkFreq resource specifies the
             * frequency of the external oscillator
             * connected to the device for baud
             * rate determination.  When specified
             * as an integer, it represents the
             * frequency, in Hz, of the external
             * oscillator. }
             */

	    if (devResourceGet (pHcf, "clkFreq", HCF_RES_INT,
				(void *)&pChan->xtal) != OK)
		{
                if (devResourceGet(pHcf, "clkFreq", HCF_RES_ADDR,
                                   (void *)&getFreq) == OK)
                    pChan->xtal = (*getFreq) ();
                else
                    {
                    hwMemFree ((char *)pChan);
                    return;
                    }
		}

            /*
             * There are typically three points in the boot sequence where baud
             * rate is initialized: 1) by the boot monitor or uboot prior to
             * execution of VxWorks or bootrom, 2) now, from this function which
             * is fairly early in the VxWorks boot sequence, and 3) later when the
             * ttys are initialized.
             *
             * If the "initialBaud" resource is specified in hwconf.c a temporary
             * switch to 9600 baud during boot can be avoided. For instance, if a
             * boot monitor or u-boot is configurated at 38400 baud, the board
             * will initially boot at that baud rate.  To avoid garbage in the log
             * output during boot (especially in a guestOS configuration) the BSP
             * can set the initial baud rate to 38400.
             */

            if (devResourceGet (pHcf, "initialBaud", HCF_RES_INT,
				(void *)&pChan->baudRate) != OK)
                {
                /* Use the default baud rate */

                pChan->baudRate = VXB_AMBA_CONSOLE_BAUD_RATE;
                }
            }

        /* fill in bus subsystem fields */

        pChan->pDev = pDev;
        pChan->regIndex = i;

        pChan->pNext = (VXB_AMBA_CHAN *)(pDev->pDrvCtrl);
        pDev->pDrvCtrl = (void *)pChan;

        /* get the channel number */

        pChan->channelNo = sioNextChannelNumberAssign();

        /*
         * if it is a plb based device, override the channel
         * number with the unit number
         */
        if (pDev->busID == VXB_BUSID_PLB)
            {
            pChan->channelNo = pDev->unitNumber;
            }
        else
            {
            /* assign the unit number to be the same as channel number */

            pDev->unitNumber = pChan->channelNo;
            }

        /* make non-standard methods available to OS layers */

        pDev->pMethods = &vxbAmbaSio_methods[0];

        X_DEBUG_MSG(1, "vxbAmbaSio BAR%d: calling vxbAmbaSioDevInit(0x%08x) for channel %d\n",
                    i, (int)pChan, pChan->channelNo,4,5,6);

	/* set function and data pointers for ISR deferral model */

        pChan->isrDefRd.func  = vxbPrimeCellSioIntRx;
        pChan->isrDefRd.pData = pChan;

        pChan->isrDefWr.func  = vxbPrimeCellSioIntTx;
        pChan->isrDefWr.pData = pChan;

        vxbRegMap (pDev, 0, &pChan->handle);

#if (CPU_FAMILY == PPC) || defined(ARMBE8)
        pChan->handle = (void *) VXB_HANDLE_SWAP ((UINT32) pChan->handle);
#endif /* CPU_FAMILY == PPC || defined(ARMBE8) */

        /* Initialize spinlock. */

        SPIN_LOCK_ISR_INIT (&pChan->spinlockIsr, 0);

        /*
         * Spinlock is not available unless MMU is enabled on SMP system.
         *
         * Interrupts are disabled on this device till vxbIntEnable()
         * is called and spinlock is not necessary till then.
         */

        /* initialize device */

        vxbPrimeCellSioDevInit(pChan);
        }
    }

/******************************************************************************
*
* vxbAmbaSioInstInit2 - initialize vxbAmbaSio device
*
* This routine is called later during system initialization.  OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioInstInit2
    (
    struct vxbDev * pDev
    )
    {
    /* MMU is already enabled when it comes here and safe to use spinlock. */

    VXB_AMBA_SIO_SPIN_LOCK_READY;
    }

/******************************************************************************
*
* vxbAmbaSioInstConnect - connect vxbAmbaSio device to I/O system
*
* Nothing to do here.  We want serial channels available for output reasonably
* early during the boot process.  Because this is a serial channel, we connect
* to the I/O system in vxbAmbaSioInstInit2() instead of here.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioInstConnect
    (
    struct vxbDev * pDev
    )
    {
    }

/******************************************************************************
*
* vxbAmbaSioChanGet - METHOD: get pChan for the specified interface
*
* This routine returns the pChan structure pointer for the
* specified interface.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioChanGet
    (
    VXB_DEVICE_ID       pDev,
    void *              pArg
    )
    {
    VXB_AMBA_CHAN * pChan;
    SIO_CHANNEL_INFO *  pInfo = (SIO_CHANNEL_INFO *)pArg;

    pChan = (VXB_AMBA_CHAN *)(pDev->pDrvCtrl);
    while (pChan != NULL)
        {
        if (pChan->channelNo == pInfo->sioChanNo)
            {
            pInfo->pChan = pChan;

            return;
            }
        pChan = pChan->pNext;
        }
    }

/******************************************************************************
*
* vxbAmbaSioChannelConnect - connect the specified channel to /tyCo/X
*
* This routine connects the specified channel number to the I/O subsystem.
* If successful, it returns the pChan structure pointer of the channel
* connected in the SIO_CHANNEL_INFO struct.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioChannelConnect
    (
    VXB_DEVICE_ID       pDev,
    VXB_AMBA_CHAN *     pChan
    )
    {
    if ((pDev == NULL) || (pChan == NULL))
        return;

    /* connect the ISR */

    (void) vxbIntConnect (pDev, 0, vxbPrimeCellSioInt, pChan);

    /* do not enable interrupt here until ISR defer queue is ready */

    }

/******************************************************************************
*
* vxbAmbaSioChanConnect - METHOD: connect the specified interface to /tyCo/X
*
* This routine connects the specified channel number to the I/O subsystem.
* If successful, it returns the pChan structure pointer of the channel
* connected in the SIO_CHANNEL_INFO struct.
*
* If the specified channel is -1, or if the specified channel matches
* any channel on this instance, then connect the channel.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioChanConnect
    (
    VXB_DEVICE_ID       pDev,
    void *              pArg
    )
    {
    SIO_CHANNEL_INFO *  pInfo = (SIO_CHANNEL_INFO *)pArg;
    VXB_AMBA_CHAN * pChan;

    /* connect all channels ? */

    if (pInfo->sioChanNo == -1)
        {
        /* yes, connect all channels */

        pChan = pDev->pDrvCtrl;

        while (pChan != NULL)
            {
            /* connect to I/O system and connect interrupts */

            vxbAmbaSioChannelConnect(pDev,pChan);

            pChan = pChan->pNext;
            }
        }
    else
        {
        /* no, only connect specified channel */

        /* check previous instance success */

        if (pInfo->pChan != NULL)
            return;

        /* find channel */

        pChan = pDev->pDrvCtrl;

        while (pChan && (pChan->channelNo != pInfo->sioChanNo))
            pChan = pChan->pNext;

        /* sanity check */

        if (pChan == NULL)
            return;
        if ((pInfo->sioChanNo != -1) &&
             (pChan->channelNo != pInfo->sioChanNo))
            return;

        /* connect to I/O system and connect interrupts */

        vxbAmbaSioChannelConnect (pDev,pChan);

        /* notify caller and downstream instances of success */

        pInfo->pChan = pChan;
        }
    }

/*******************************************************************************
*
* vxbAmbaSioDummyCallback - dummy callback routine.
*
* This routine does nothing.
*
* RETURNS: ERROR, always.
*
* ERRNO
*/

LOCAL STATUS vxbAmbaSioDummyCallback (void)
    {
    return ERROR;
    }

/*******************************************************************************
*
* vxbPrimeCellSioDevInit - initialise an AMBA channel
*
* This routine initialises some SIO_CHAN function pointers and then resets
* the chip to a quiescent state.  Before this routine is called, the BSP
* must already have initialised all the device addresses, etc. in the
* VXB_AMBA_CHAN structure.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbPrimeCellSioDevInit
    (
    VXB_AMBA_CHAN *	pChan	/* ptr to VXB_AMBA_CHAN */
    )
    {
    /* initialise the driver function pointers in the SIO_CHAN */

    pChan->pDrvFuncs = &vxbAmbaSioDrvFuncs;


    /* set the non BSP-specific constants */

    pChan->getTxChar  = vxbAmbaSioDummyCallback;
    pChan->putRcvChar = vxbAmbaSioDummyCallback;

    /* initialise the chip */

    vxbAmbaSioInitChannel (pChan);
    }

/*******************************************************************************
*
* vxbAmbaSioInitChannel - initialise UART
*
* This routine performs hardware initialisation of the UART channel.
*
* RETURNS: N/A
*
* ERRNO
*/

LOCAL void vxbAmbaSioInitChannel
    (
    VXB_AMBA_CHAN *	pChan /* ptr to VXB_AMBA_CHAN */
    )
    {
    int		i;
    UINT32      flags;

    unsigned int divider, remainder, fraction, temp;

    /* Disable the UART */
    VXB_AMBA_UART_REG_WRITE(pChan, UARTCR, 0);

    /* clear existing interrupts */
    VXB_AMBA_UART_REG_WRITE(pChan, UARTICR, 0xFFFF);

    /*
     * Prime the pump. Get TX FIFO interrupt to assert.
     */
    VXB_AMBA_UART_REG_WRITE(pChan, UARTCR, (UART_ENABLE | UART_TXE | UART_LBE));
    VXB_AMBA_UART_REG_WRITE(pChan, UARTFBRD, 0);
    VXB_AMBA_UART_REG_WRITE(pChan, UARTIBRD, 1);
    VXB_AMBA_UART_REG_WRITE(pChan, UARTLCR_H,0);
    VXB_AMBA_UART_REG_WRITE(pChan, UARTIFLS, 0);
    VXB_AMBA_UART_REG_WRITE(pChan, UARTDR,0);

    i = 0;
    flags = FLG_BUSY;
    while ((flags & FLG_BUSY) && (i < 5000))
        {
        VXB_AMBA_UART_REG_READ(pChan, UARTFR, flags);
        i++;
        }

    VXB_AMBA_UART_REG_WRITE(pChan, UARTCR, (UART_ENABLE | UART_TXE | UART_RXE));

    /* Set baud rate divisor */
    temp      = 16 * pChan->baudRate;
    divider   = pChan->xtal / temp;
    remainder = pChan->xtal % temp;
    temp      = (128 * remainder) / temp;
    fraction  = temp / 2;

    if (temp & 1)
        fraction++;

    VXB_AMBA_UART_REG_WRITE(pChan, UARTIBRD, divider);
    VXB_AMBA_UART_REG_WRITE(pChan, UARTFBRD, fraction);

    /*
     * Set word format, enable FIFOs: set 8 bits, 1 stop bit, no parity.
     * This also latches the writes to the two (sub)registers above.
     */

    VXB_AMBA_UART_REG_WRITE(pChan, UARTLCR_H,
                        (WORD_LEN_8 | FIFO_ENABLE | ONE_STOP | PARITY_NONE));
    }

/*******************************************************************************
*
* vxbAmbaSioIoctl - special device control
*
* This routine handles the IOCTL messages from the user.
*
* RETURNS: OK on success
*
* ERRNO:  ENOSYS on unsupported request, EIO on failed request.
*/

LOCAL STATUS vxbAmbaSioIoctl
    (
    SIO_CHAN *	pSioChan,	/* ptr to SIO_CHAN describing this channel */
    int		request,	/* request code */
    int		arg		/* some argument */
    )
    {
    STATUS	status;		/* status to return */
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *)pSioChan;
    unsigned int divider, remainder, fraction, temp;
    BOOL    spinLockOwn;

    status = OK;	/* preset to return OK */

    switch (request)
        {
        case SIO_BAUD_SET:

            /*
             * Set the baud rate. Return EIO for an invalid baud rate, or
             * OK on success.
             */

            /*
             * baudrate divisor must be non-zero and must fit in a 12-bit
             * register.
             */

            /* Set baud rate divisor */
            temp      = 16 * arg;
            divider   = pChan->xtal / temp;
            remainder = pChan->xtal % temp;
            temp      = (128 * remainder) / temp;
            fraction  = temp / 2;

            if (temp & 1)
                fraction++;


            if ((divider < 1) || (divider > 0xFFFF))
                {
                status = EIO;       /* baud rate out of range */
                break;
                }

            /* disable interrupts during chip access */

            VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);


            /* Set baud rate divisor in UART */

            VXB_AMBA_UART_REG_WRITE(pChan, UARTIBRD, divider);
            VXB_AMBA_UART_REG_WRITE(pChan, UARTFBRD, fraction);

            /*
             * Set word format, enable FIFOs: set 8 bits, 1 stop bit, no parity.
             * This also latches the writes to the two (sub)registers above.
             */

            VXB_AMBA_UART_REG_WRITE(pChan, UARTLCR_H,
                                (WORD_LEN_8 | FIFO_ENABLE | ONE_STOP |
				 PARITY_NONE));

            pChan->baudRate = arg;

            VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);

            break;

        case SIO_BAUD_GET:

            /* Get the baud rate and return OK */

            *(int *)arg = pChan->baudRate;
            break;


        case SIO_MODE_SET:

            /*
             * Set the mode (e.g., to interrupt or polled). Return OK
             * or EIO for an unknown or unsupported mode.
             */

            if ((arg != SIO_MODE_POLL) && (arg != SIO_MODE_INT))
                {
                status = EIO;
                break;
                }

            /* set the pointer of the  queue for deferred work */

            if (arg == SIO_MODE_INT)
                {
                if (pChan->queueId == NULL)
                    pChan->queueId = isrDeferQueueGet (pChan->pDev, 0, 0, 0);
                }

	    /* test if spinlock is already owned by the current CPU core */

	    spinLockOwn = VXB_AMBA_SIO_SPIN_LOCK_ISR_HELD(pChan);

	    if (!spinLockOwn)
		{
		VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);
		}

            if (arg == SIO_MODE_INT)
                {
                /* Enable interrupts for this UART */

                VXB_AMBA_UART_REG_WRITE(pChan, UARTIMSC,
                                        (IMSC_RXIM | IMSC_RTIM | IMSC_TXIM));
                }
            else
                {

                /* Disable all interrupts for this UART. */

                VXB_AMBA_UART_REG_WRITE(pChan, UARTIMSC, 0);
                }

            pChan->channelMode = arg;

	    if (!spinLockOwn)
		{
		VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);
		}

            /* enable channel interrupt after ISR defer queue is ready */

            (void) vxbIntEnable(pChan->pDev, 0, vxbPrimeCellSioInt, pChan);

            break;


        case SIO_MODE_GET:

            /* Get the current mode and return OK */

            *(int *)arg = pChan->channelMode;
            break;


        case SIO_AVAIL_MODES_GET:

            /* Get the available modes and return OK */

            *(int *)arg = SIO_MODE_INT | SIO_MODE_POLL;
            break;

#ifdef	_WRS_CONFIG_SMP
	case SIO_DEV_LOCK:
	    VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);
	    break;

	case SIO_DEV_UNLOCK:
	    VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);
	    break;
#endif	/* _WRS_CONFIG_SMP */

        case SIO_HW_OPTS_SET:

            /*
             * Optional command to set the hardware options (as defined
             * in sioLib.h).
             * Return OK, or ENOSYS if this command is not implemented.
             * Note: several hardware options are specified at once.
             * This routine should set as many as it can and then return
             * OK. The SIO_HW_OPTS_GET is used to find out which options
             * were actually set.
             */

        case SIO_HW_OPTS_GET:

            /*
             * Optional command to get the hardware options (as defined
             * in sioLib.h). Return OK or ENOSYS if this command is not
             * implemented.  Note: if this command is not implemented, it
             * will be assumed that the driver options are CREAD | CS8
             * (e.g., eight data bits, one stop bit, no parity, ints enabled).
             */

        default:
            status = ENOSYS;
        }

    return status;
    }

/*******************************************************************************
 *
 * vxbPrimeCellSioIntTx - deferred handle a transmitter interrupt
 *
 * This routine handles the seconnd level write interrupts from the UART.
 *
 * RETURNS: N/A
 *
 * ERRNO
 */

LOCAL void vxbPrimeCellSioIntTx
    (
    void *	pData	/* ptr to VXB_AMBA_CHAN describing this channel */
    )
    {
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *) pData;
    UINT16 flags;
    char outChar;
    BOOL fifo_full = FALSE;

    VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);

    /*
     * Clear the txDefer.
     * Another deferred job can be posted before completing the deferred job
     * since the txDefer is cleared prior to start the getTxChar.
     * But the job queue is already cleared before entering into
     * vxbPrimeCellSioIntTx() and the maximum required job queue number for
     * transmit is one.
     */

    pChan->txDefer = FALSE;

    VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);

    do
	{
	/* While TX FIFO isn't full, we have more buffer to transmit */

	VXB_AMBA_UART_REG_READ(pChan, UARTFR, flags);
	fifo_full = ((flags & FLG_UTXFF) != 0);

	if (fifo_full)
	    {
	    /* Enable TX interrupts. Leave TX interrupts enabled. */

	    VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);
	    VXB_AMBA_UART_REG_BIT_SET(pChan, UARTIMSC, IMSC_TXIM );
	    VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);
	    }
	else
	    {
	    if ((*pChan->getTxChar) (pChan->getTxArg, &outChar) != ERROR)
		{
		/*
		 * Re-take the spinlock to mutual exclude the TX Reg
		 * access between the kprintf() in SMP mode.
		 */

		VXB_AMBA_SIO_ISR_SET(pChan);

		VXB_AMBA_UART_REG_WRITE(pChan, UARTDR, outChar);

		VXB_AMBA_SIO_ISR_CLEAR(pChan);
		}
	    else
		break;
	    }
	} while (!fifo_full);
    }

/*****************************************************************************
 *
 * vxbPrimeCellSioIntRx - deferred handle a receiver interrupt
 *
 * This routine handles the second level read interrupts from the UART.
 *
 * RETURNS: N/A
 *
 * ERRNO
 */

LOCAL void vxbPrimeCellSioIntRx
    (
    void *	pData	/* ptr to VXB_AMBA_CHAN describing this channel */
    )
    {
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *) pData;
    UINT16 inchar;
    UINT16 flags;
    BOOL more_data = FALSE;

    /* read characters from Receive Holding Reg. */
    do
        {
        /* While RX FIFO isn't empty, we have more data to read */
        VXB_AMBA_UART_REG_READ(pChan, UARTFR, flags);
        more_data = ( (flags & FLG_URXFE) == 0);

        if (more_data)
            {
            /* Read from data register. */
            VXB_AMBA_UART_REG_READ(pChan, UARTDR, inchar);
            (*pChan->putRcvChar) (pChan->putRcvArg, (char)inchar);
            }
        } while (more_data);

    /* Enable reception interrupts. Leave transmit interrupts enabled. */

    VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);
    VXB_AMBA_UART_REG_BIT_SET(pChan, UARTIMSC, (IMSC_RXIM | IMSC_RTIM) );
    VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);
    }

/******************************************************************************
 *
 * vxbPrimeCellSioInt - handle any UART interrupt
 *
 * This routine handles interrupts from the UART and determines whether
 * the source is a transmit interrupt or receive/receive-timeout interrupt.
 *
 * The Prime Cell UART generates a receive interrupt when the RX FIFO is
 * half-full, and a receive-timeout interrupt after 32 bit-clocks have
 * elapsed with no incoming data.
 *
 * RETURNS: N/A
 *
 * ERRNO
 */

LOCAL void vxbPrimeCellSioInt
    (
    VXB_AMBA_CHAN * pChan   /* ptr to VXB_AMBA_CHAN describing this channel */
    )
    {
    UINT16 intId;

    VXB_AMBA_UART_REG_READ(pChan, UARTMIS, intId);
    intId &= (UART_RTMIS | UART_RXMIS | UART_TXMIS);

    do
        {
        VXB_AMBA_SIO_ISR_SET(pChan);
        VXB_AMBA_UART_REG_BIT_CLR(pChan, UARTIMSC, intId);

        VXB_AMBA_UART_REG_WRITE(pChan, UARTICR,
                                (intId &
                                 ~(UART_RTMIS | UART_RXMIS | UART_TXMIS)));

        if (intId & UART_TXMIS)
            {
            if (!pChan->txDefer)
                {
                pChan->txDefer = TRUE;

                VXB_AMBA_SIO_ISR_CLEAR(pChan);

                isrDeferJobAdd (pChan->queueId, &pChan->isrDefWr);
                }
            else
                {
                VXB_AMBA_SIO_ISR_CLEAR(pChan);
                }
            }
        else
            {
            VXB_AMBA_SIO_ISR_CLEAR(pChan);
            }

        if (intId & (UART_RXMIS | UART_RTMIS))
            {
            /* defer the job */

            isrDeferJobAdd (pChan->queueId, &pChan->isrDefRd);
            }

        VXB_AMBA_UART_REG_READ(pChan, UARTMIS, intId);
        intId &= (UART_RTMIS | UART_RXMIS | UART_TXMIS);
        }
    while((intId & UART_TXMIS) || (intId & (UART_RXMIS | UART_RTMIS)));

    }


/*******************************************************************************
 *
 * vxbAmbaSioTxStartup - transmitter startup routine
 *
 * Enable interrupt so that interrupt-level char output routine will be called.
 *
 * RETURNS: OK on success
 *
 * ERRNO: ENOSYS if the device is polled-only, or EIO on hardware error.
 */

LOCAL int vxbAmbaSioTxStartup
    (
    SIO_CHAN *	pSioChan	/* ptr to SIO_CHAN describing this channel */
    )
    {
    int status;
    BOOL  spinLockOwn;
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *)pSioChan;

    /* test if spinlock is already owned by the current CPU core */

    spinLockOwn = VXB_AMBA_SIO_SPIN_LOCK_ISR_HELD(pChan);

    if (!spinLockOwn)
	{
	VXB_AMBA_SIO_SPIN_LOCK_ISR_TAKE(pChan);
	}

    if (pChan->channelMode == SIO_MODE_INT)
        {
	if (!pChan->txDefer)
	    {
	    VXB_AMBA_UART_REG_BIT_SET(pChan, UARTIMSC, IMSC_TXIM );
	    }

	status = OK;
        }
    else
        status = ENOSYS;

    if (!spinLockOwn)
	{
	VXB_AMBA_SIO_SPIN_LOCK_ISR_GIVE(pChan);
	}

    return (status);
    }

/******************************************************************************
 *
 * vxbAmbaSioPollOutput - output a character in polled mode.
 *
 * This routine sends a character in polled mode.
 *
 * RETURNS: OK if a character arrived
 *
 * ERRNO: EIO on device error, EAGAIN if the output buffer is full,
 * ENOSYS if the device is interrupt-only.
 */

LOCAL int vxbAmbaSioPollOutput
    (
    SIO_CHAN *	pSioChan,	/* ptr to SIO_CHAN describing this channel */
    char	outChar 	/* char to output */
    )
    {
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *)pSioChan;
    FAST UINT32 pollStatus;

    VXB_AMBA_UART_REG_READ(pChan, UARTFR, pollStatus);

    /* is the transmitter ready to accept a character? */

    if ((pollStatus & FLG_UTXFF) != 0x00)
        return EAGAIN;


    /* write out the character */

    VXB_AMBA_UART_REG_WRITE(pChan, UARTDR, outChar);    /* transmit character */

    return OK;
    }

/******************************************************************************
 *
 * vxbAmbaSioPollInput - poll the device for input.
 *
 * This routines polls for an input character.
 *
 * RETURNS: OK if a character arrived
 *
 * ERRNO: EIO on device error, EAGAIN if the input buffer is empty,
 * ENOSYS if the device is interrupt-only.
 */

LOCAL int vxbAmbaSioPollInput
    (
    SIO_CHAN *	pSioChan,	/* ptr to SIO_CHAN describing this channel */
    char *	thisChar	/* pointer to where to return character */
    )
    {
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *)pSioChan;
    FAST UINT32 pollStatus;

    VXB_AMBA_UART_REG_READ(pChan, UARTFR, pollStatus);

    if ((pollStatus & FLG_URXFE) != 0x00)
        return EAGAIN;


    /* got a character */

    VXB_AMBA_UART_REG_READ(pChan, UARTDR, *thisChar);

    return OK;

    }

/******************************************************************************
 *
 * vxbAmbaSioCallbackInstall - install ISR callbacks to get/put chars.
 *
 * This routine installs interrupt callbacks for transmitting characters
 * and receiving characters.
 *
 * RETURNS: OK on success
 *
 * ERRNO: ENOSYS for an unsupported callback type.
 */

LOCAL int vxbAmbaSioCallbackInstall
    (
    SIO_CHAN *	pSioChan,	/* ptr to SIO_CHAN describing this channel */
    int		callbackType,	/* type of callback */
    STATUS	(*callback)(),	/* callback */
    void *	callbackArg	/* parameter to callback */

    )
    {
    VXB_AMBA_CHAN * pChan = (VXB_AMBA_CHAN *)pSioChan;

    switch (callbackType)
        {
        case SIO_CALLBACK_GET_TX_CHAR:
            pChan->getTxChar    = callback;
            pChan->getTxArg = callbackArg;
            return OK;

        case SIO_CALLBACK_PUT_RCV_CHAR:
            pChan->putRcvChar   = callback;
            pChan->putRcvArg    = callbackArg;
            return OK;

        default:
            return ENOSYS;
        }

    }

STATUS mioUartTest(int mio)
{
    int fd;
	int count;
    char devname[16];

    bzero(devname, sizeof(devname));
    sprintf(devname, "/tyCo/%d", mio);

	fd = open(devname, 2, 0777);
	if (fd < 0)
		{
		printf("Failed to open  %s \r\n", devname);
		return ERROR;
		}

	count = write(fd, "hello,world\r\n", 13);

	printf("write 13 char, return %d char\r\n", count);

	close(fd);
	return OK;
}
