/* vxbFtWdtLib.c - vxBus watchdog timer library */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
DESCRIPTION

This module implements the watchdog timer library interfaces.
*/

/* includes */

#include <vxWorks.h>
#include <vxBusLib.h>
#include <vxbTimerLib.h>
#include <edrLib.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* defines */

#undef VXB_WDT_DEBUG
#ifdef VXB_WDT_DEBUG

LOCAL int vxbFtWdtDebugLevel = 1000;

#undef VXB_FT_WDT_LOG
#define VXB_FT_WDT_LOG(lvl,msg,a,b,c,d,e,f) \
    if ( vxbFtWdtDebugLevel >= lvl ) printf(msg,a,b,c,d,e,f)
#else /* VXB_WDT_DEBUG */

#undef VXB_FT_WDT_LOG
#define VXB_FT_WDT_LOG(lvl,msg,a,b,c,d,e,f)

#endif /* VXB_WDT_DEBUG */

/* globals */


/* locals */
#define WDT_TOTAL_COUNT 2
LOCAL void *    pFtWdtCookie[WDT_TOTAL_COUNT] = {NULL};
IMPORT struct   vxbTimerFunctionality * pFtWdtTimer[WDT_TOTAL_COUNT] ;
IMPORT VXB_DEVICE_ID    pDevFtWdtTimer[WDT_TOTAL_COUNT];
LOCAL BOOL  vxbFtWdtRunning[WDT_TOTAL_COUNT] = {FALSE,};

/* to find appropriate watchdog timer */


/*******************************************************************************
* vxbFtWdtLibInit - initialize the watchdog timer library
*
* This routine initializes the watchdog timer library by selecting the
* timer which is best suited for use as watchdog timer from the timers 
* available in the system.
*
* RETURNS: OK or ERROR.
*
*/
STATUS vxbFtWdtLibInit (int n)
    {
    if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

    if (NULL == pFtWdtTimer[n])
    {
        printf("General Perpus Timer %d is not initialized!\r\n", n);
        return ERROR;
    }
     
    /* allocate watchdog clock */

    if ((pFtWdtTimer[n]->timerAllocate(pDevFtWdtTimer[n],
       pFtWdtTimer[n]->features & VXB_TIMER_AUTO_RELOAD,
       &pFtWdtCookie[n], n))!= OK )
        {
        pFtWdtCookie[n] = NULL;

        return(ERROR);
        }
    
        
    return OK; 
    }

/*******************************************************************************
*
* vxbFtWdtConnect - connect a routine to the watchdog timer interrupt
*
* This routine later specifies the interrupt service routine to be called at
* each watchdog timer interrupt. It does not enable watchdog timer interrupts.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
* SEE ALSO: vxbFtWdtEnable()
*/

STATUS vxbFtWdtConnect
    (
    int n,
    FUNCPTR routine,    /* routine called at each wdt clock interrupt    */
    _Vx_usr_arg_t arg   /* argument to watchdog timer interrupt routine */
    )
    {

    if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

    if (NULL == pFtWdtTimer[n])
    {
        printf("General Perpus Timer %d is not initialized!\r\n", n);
        return ERROR;
    }
    
    /* call the function to hook the wdtclk ISR */

    if (pFtWdtTimer[n]->timerISRSet (pFtWdtCookie[n],
                                  (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_FT_WDT_LOG(1, "vxbFtWdtConnect : not able to set ISR\n",
                                        1,2,3,4,5,6);
        return ERROR;
        }

    VXB_FT_WDT_LOG(3, "vxbFtWdtConnect : returns OK\n",1,2,3,4,5,6);

    return (OK);

    }

/*******************************************************************************
*
* vxbFtWdtDisable - turn off watchdog timer interrupts
*
* This routine disables watchdog timer interrupts.
*
* RETURNS: OK, or ERROR if fail
*
* SEE ALSO: vxbFtWdtEnable()
*/

STATUS vxbFtWdtDisable (int n)
    {
    if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

    if (NULL == pFtWdtTimer[n])
    {
        printf("General Perpus Timer %d is not initialized!\r\n", n);
        return ERROR;
    }

    /*
     * If Aux clock is enabled, then we can disable it.
     * If the wdtclock is not enabled, then it means that is already disabled
     * and nothing needs to be done.
     */

    if (vxbFtWdtRunning[n])
        {
        if ( pFtWdtTimer[n]->timerDisable (pFtWdtCookie[n]) != OK )
            {
            VXB_FT_WDT_LOG(1, "vxbFtWdtDisable : not able to disable timer\n",
                                                  1,2,3,4,5,6);
            return ERROR;
            }

        vxbFtWdtRunning[n] = FALSE;
        }

    VXB_FT_WDT_LOG(3, "vxbFtWdtDisable : returns\n",1,2,3,4,5,6);

    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtEnable - turn on watchdog timer interrupts
*
* This routine enables watchdog timer interrupts.
*
* RETURNS: OK, or ERROR if fail
*
* SEE ALSO: vxbFtWdtConnect(), sysWdtDisable(), sysWdtRateSet()
*/

STATUS vxbFtWdtEnable (int n)
    {

    if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

    if (NULL == pFtWdtTimer[n])
    {
        printf("General Perpus Timer %d is not initialized!\r\n", n);
        return ERROR;
    }

    /* if wdtClock is not enabled, then enable it */

    if (!vxbFtWdtRunning[n])
        {

        if (pFtWdtTimer[n]->timerEnable (pFtWdtCookie[n], 
                                       (pFtWdtTimer[n]->clkFrequency / 
                                       pFtWdtTimer[n]->ticksPerSecond)) != OK)
            {
            VXB_FT_WDT_LOG(1, "vxbFtWdtEnable : not able to enable timer\n",
                                                  1,2,3,4,5,6); 
            return ERROR;
            }
  
        vxbFtWdtRunning[n] = TRUE;
        }

    VXB_FT_WDT_LOG(3, "vxbFtWdtEnable : returns\n",1,2,3,4,5,6);

    return OK;
    }

/*******************************************************************************
*
* vxbFtWdtRateGet - get the watchdog timer rate
*
* This routine returns the interrupt rate of the watchdog timer.
*
* RETURNS: The number of ticks per second of the watchdog timer.
*
* SEE ALSO: vxbFtWdtEnable(), vxbFtWdtRateSet()
*/

int vxbFtWdtRateGet (int n)
    {
    if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

    if (NULL == pFtWdtTimer[n])
    {
        printf("General Perpus Timer %d is not initialized!\r\n", n);
        return ERROR;
    }

    VXB_FT_WDT_LOG(2, "vxbFtWdtRateGet : value %d\n",
                        pFtWdtTimer[n]->ticksPerSecond,2,3,4,5,6);

    return (pFtWdtTimer[n]->ticksPerSecond);
    }

/*******************************************************************************
*
* vxbFtWdtRateSet - set the watchdog timer rate
*
* This routine sets the interrupt rate of the watchdog timer.
* It does not enable watchdog timer interrupts.
*
* RETURNS: OK, or ERROR if the tick rate is invalid or the timer cannot be set.
*
* SEE ALSO: vxbFtWdtEnable(), vxbFtWdtRateGet()
*/

STATUS vxbFtWdtRateSet
    (
    int n,
    int ticksPerSecond          /* number of clock interrupts per second */
    )
    {

    if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

    if (NULL == pFtWdtTimer[n])
    {
        printf("General Perpus Timer %d is not initialized!\r\n", n);
        return ERROR;
    }

    VXB_FT_WDT_LOG(2, "vxbFtWdtRateSet : value %d\n",
                        ticksPerSecond,2,3,4,5,6);

    /* 
     * if the clk rate to be set does not fall within the limits,
     * return ERROR.
     */

    if ( ((UINT)ticksPerSecond < pFtWdtTimer[n]->minFrequency) ||
         ((UINT)ticksPerSecond > pFtWdtTimer[n]->maxFrequency) )
        return (ERROR);

    /* if only the rate to be set is different, set the rate */

    if (pFtWdtTimer[n]->ticksPerSecond != (UINT)ticksPerSecond)
        {
        pFtWdtTimer[n]->ticksPerSecond = ticksPerSecond;

        if (vxbFtWdtRunning[n])
            {
            vxbFtWdtDisable (n);
            vxbFtWdtEnable (n);
            }
        }

    VXB_FT_WDT_LOG(3, "vxbFtWdtRateSet : returns OK\n",1,2,3,4,5,6);

    return (OK);
    }

#define TEST_WDT_CLK
#ifdef TEST_WDT_CLK

LOCAL int debugClkIntCnt = 0; 
void testFtWdtInt (int n)
{
    debugClkIntCnt++;
    logMsg("##### Enter watchdog timer %d interrupt cnt:%d......\r\n", n,debugClkIntCnt,3,4,5,6);
}

STATUS testFtWdt(int n, int rate)
{
  if ((n<0) || (n>=WDT_TOTAL_COUNT))
    {
        printf("General Perpus Timer %d is invalid.\r\n", n);
        return ERROR;
    }

  if (vxbFtWdtLibInit(n) != OK)
  {
      printf ("vxbFtWdtLibInit  Failed!\r\n");
      return (ERROR);
  }  

  if (vxbFtWdtConnect (n, (FUNCPTR)testFtWdtInt, (_Vx_usr_arg_t)n) != OK)
    {
        printf ("vxbFtWdtConnect  Failed!\r\n");
        return (ERROR);
    }


      (void) vxbFtWdtRateSet (n, rate);
      
    if (vxbFtWdtEnable (n) != OK)
    {
        printf ("vxbFtWdtEnable  Failed!\r\n");
        return (ERROR);
    }
    else
    {
        printf ("vxbFtWdtEnable  successfully.\r\n");
    }
    

  return OK;
}
#endif
