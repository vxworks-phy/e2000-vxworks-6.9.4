/* ftPinMux.h - Driver for pin mux/demux controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#ifndef __FT_PINMUX_H
#define __FT_PINMUX_H

#ifdef __cplusplus
extern "C" {
#endif

#include "defs.h"

#define PINMUX_BASE_ADRS             0x32B30000


/*****************************************************************************/
/* register offset of iopad function / pull / driver strength */
#define FIOPAD_0_FUNC_OFFSET     0x0000U
#define FIOPAD_2_FUNC_OFFSET     0x0004U
#define FIOPAD_3_FUNC_OFFSET     0x0008U
#define FIOPAD_4_FUNC_OFFSET     0x000CU
#define FIOPAD_5_FUNC_OFFSET     0x0010U
#define FIOPAD_6_FUNC_OFFSET     0x0014U
#define FIOPAD_7_FUNC_OFFSET     0x0018U
#define FIOPAD_8_FUNC_OFFSET     0x001CU
#define FIOPAD_9_FUNC_OFFSET     0x0020U
#define FIOPAD_10_FUNC_OFFSET    0x0024U
#define FIOPAD_11_FUNC_OFFSET    0x0028U
#define FIOPAD_12_FUNC_OFFSET    0x002CU
#define FIOPAD_13_FUNC_OFFSET    0x0030U
#define FIOPAD_14_FUNC_OFFSET    0x0034U
#define FIOPAD_15_FUNC_OFFSET    0x0038U
#define FIOPAD_16_FUNC_OFFSET    0x003CU
#define FIOPAD_17_FUNC_OFFSET    0x0040U
#define FIOPAD_18_FUNC_OFFSET    0x0044U
#define FIOPAD_19_FUNC_OFFSET    0x0048U
#define FIOPAD_20_FUNC_OFFSET    0x004CU
#define FIOPAD_21_FUNC_OFFSET    0x0050U
#define FIOPAD_22_FUNC_OFFSET    0x0054U
#define FIOPAD_23_FUNC_OFFSET    0x0058U
#define FIOPAD_24_FUNC_OFFSET    0x005CU
#define FIOPAD_25_FUNC_OFFSET    0x0060U
#define FIOPAD_26_FUNC_OFFSET    0x0064U
#define FIOPAD_27_FUNC_OFFSET    0x0068U
#define FIOPAD_28_FUNC_OFFSET    0x006CU
#define FIOPAD_31_FUNC_OFFSET    0x0070U
#define FIOPAD_32_FUNC_OFFSET    0x0074U
#define FIOPAD_33_FUNC_OFFSET    0x0078U
#define FIOPAD_34_FUNC_OFFSET    0x007CU
#define FIOPAD_35_FUNC_OFFSET    0x0080U
#define FIOPAD_36_FUNC_OFFSET    0x0084U
#define FIOPAD_37_FUNC_OFFSET    0x0088U
#define FIOPAD_38_FUNC_OFFSET    0x008CU
#define FIOPAD_39_FUNC_OFFSET    0x0090U
#define FIOPAD_40_FUNC_OFFSET    0x0094U
#define FIOPAD_41_FUNC_OFFSET    0x0098U
#define FIOPAD_42_FUNC_OFFSET    0x009CU
#define FIOPAD_43_FUNC_OFFSET    0x00A0U
#define FIOPAD_44_FUNC_OFFSET    0x00A4U
#define FIOPAD_45_FUNC_OFFSET    0x00A8U
#define FIOPAD_46_FUNC_OFFSET    0x00ACU
#define FIOPAD_47_FUNC_OFFSET    0x00B0U
#define FIOPAD_48_FUNC_OFFSET    0x00B4U
#define FIOPAD_49_FUNC_OFFSET    0x00B8U
#define FIOPAD_50_FUNC_OFFSET    0x00BCU
#define FIOPAD_51_FUNC_OFFSET    0x00C0U
#define FIOPAD_52_FUNC_OFFSET    0x00C4U
#define FIOPAD_53_FUNC_OFFSET    0x00C8U
#define FIOPAD_54_FUNC_OFFSET    0x00CCU
#define FIOPAD_55_FUNC_OFFSET    0x00D0U
#define FIOPAD_56_FUNC_OFFSET    0x00D4U
#define FIOPAD_57_FUNC_OFFSET    0x00D8U
#define FIOPAD_58_FUNC_OFFSET    0x00DCU
#define FIOPAD_59_FUNC_OFFSET    0x00E0U
#define FIOPAD_60_FUNC_OFFSET    0x00E4U
#define FIOPAD_61_FUNC_OFFSET    0x00E8U
#define FIOPAD_62_FUNC_OFFSET    0x00ECU
#define FIOPAD_63_FUNC_OFFSET    0x00F0U
#define FIOPAD_64_FUNC_OFFSET    0x00F4U
#define FIOPAD_65_FUNC_OFFSET    0x00F8U
#define FIOPAD_66_FUNC_OFFSET    0x00FCU
#define FIOPAD_67_FUNC_OFFSET    0x0100U
#define FIOPAD_68_FUNC_OFFSET    0x0104U
#define FIOPAD_148_FUNC_OFFSET   0x0108U
#define FIOPAD_69_FUNC_OFFSET    0x010CU
#define FIOPAD_70_FUNC_OFFSET    0x0110U
#define FIOPAD_71_FUNC_OFFSET    0x0114U
#define FIOPAD_72_FUNC_OFFSET    0x0118U
#define FIOPAD_73_FUNC_OFFSET    0x011CU
#define FIOPAD_74_FUNC_OFFSET    0x0120U
#define FIOPAD_75_FUNC_OFFSET    0x0124U
#define FIOPAD_76_FUNC_OFFSET    0x0128U
#define FIOPAD_77_FUNC_OFFSET    0x012CU
#define FIOPAD_78_FUNC_OFFSET    0x0130U
#define FIOPAD_79_FUNC_OFFSET    0x0134U
#define FIOPAD_80_FUNC_OFFSET    0x0138U
#define FIOPAD_81_FUNC_OFFSET    0x013CU
#define FIOPAD_82_FUNC_OFFSET    0x0140U
#define FIOPAD_83_FUNC_OFFSET    0x0144U
#define FIOPAD_84_FUNC_OFFSET    0x0148U
#define FIOPAD_85_FUNC_OFFSET    0x014CU
#define FIOPAD_86_FUNC_OFFSET    0x0150U
#define FIOPAD_87_FUNC_OFFSET    0x0154U
#define FIOPAD_88_FUNC_OFFSET    0x0158U
#define FIOPAD_89_FUNC_OFFSET    0x015CU
#define FIOPAD_90_FUNC_OFFSET    0x0160U
#define FIOPAD_91_FUNC_OFFSET    0x0164U
#define FIOPAD_92_FUNC_OFFSET    0x0168U
#define FIOPAD_93_FUNC_OFFSET    0x016CU
#define FIOPAD_94_FUNC_OFFSET    0x0170U
#define FIOPAD_95_FUNC_OFFSET    0x0174U
#define FIOPAD_96_FUNC_OFFSET    0x0178U
#define FIOPAD_97_FUNC_OFFSET    0x017CU
#define FIOPAD_98_FUNC_OFFSET    0x0180U
#define FIOPAD_29_FUNC_OFFSET    0x0184U
#define FIOPAD_30_FUNC_OFFSET    0x0188U
#define FIOPAD_99_FUNC_OFFSET    0x018CU
#define FIOPAD_100_FUNC_OFFSET  0x0190U
#define FIOPAD_101_FUNC_OFFSET  0x0194U
#define FIOPAD_102_FUNC_OFFSET  0x0198U
#define FIOPAD_103_FUNC_OFFSET  0x019CU
#define FIOPAD_104_FUNC_OFFSET  0x01A0U
#define FIOPAD_105_FUNC_OFFSET  0x01A4U
#define FIOPAD_106_FUNC_OFFSET  0x01A8U
#define FIOPAD_107_FUNC_OFFSET  0x01ACU
#define FIOPAD_108_FUNC_OFFSET  0x01B0U
#define FIOPAD_109_FUNC_OFFSET  0x01B4U
#define FIOPAD_110_FUNC_OFFSET  0x01B8U
#define FIOPAD_111_FUNC_OFFSET  0x01BCU
#define FIOPAD_112_FUNC_OFFSET  0x01C0U
#define FIOPAD_113_FUNC_OFFSET  0x01C4U
#define FIOPAD_114_FUNC_OFFSET  0x01C8U
#define FIOPAD_115_FUNC_OFFSET  0x01CCU
#define FIOPAD_116_FUNC_OFFSET  0x01D0U
#define FIOPAD_117_FUNC_OFFSET  0x01D4U
#define FIOPAD_118_FUNC_OFFSET  0x01D8U
#define FIOPAD_119_FUNC_OFFSET  0x01DCU
#define FIOPAD_120_FUNC_OFFSET  0x01E0U
#define FIOPAD_121_FUNC_OFFSET  0x01E4U
#define FIOPAD_122_FUNC_OFFSET  0x01E8U
#define FIOPAD_123_FUNC_OFFSET  0x01ECU
#define FIOPAD_124_FUNC_OFFSET  0x01F0U
#define FIOPAD_125_FUNC_OFFSET  0x01F4U
#define FIOPAD_126_FUNC_OFFSET  0x01F8U
#define FIOPAD_127_FUNC_OFFSET  0x01FCU
#define FIOPAD_128_FUNC_OFFSET  0x0200U
#define FIOPAD_129_FUNC_OFFSET  0x0204U
#define FIOPAD_130_FUNC_OFFSET  0x0208U
#define FIOPAD_131_FUNC_OFFSET  0x020CU
#define FIOPAD_132_FUNC_OFFSET  0x0210U
#define FIOPAD_133_FUNC_OFFSET  0x0214U
#define FIOPAD_134_FUNC_OFFSET  0x0218U
#define FIOPAD_135_FUNC_OFFSET  0x021CU
#define FIOPAD_136_FUNC_OFFSET  0x0220U
#define FIOPAD_137_FUNC_OFFSET  0x0224U
#define FIOPAD_138_FUNC_OFFSET  0x0228U
#define FIOPAD_139_FUNC_OFFSET  0x022CU
#define FIOPAD_140_FUNC_OFFSET  0x0230U
#define FIOPAD_141_FUNC_OFFSET  0x0234U
#define FIOPAD_142_FUNC_OFFSET  0x0238U
#define FIOPAD_143_FUNC_OFFSET  0x023CU
#define FIOPAD_144_FUNC_OFFSET  0x0240U
#define FIOPAD_145_FUNC_OFFSET  0x0244U
#define FIOPAD_146_FUNC_OFFSET  0x0248U
#define FIOPAD_147_FUNC_OFFSET  0x024CU

/* register offset of iopad delay */
#define FIOPAD_10_DELAY_OFFSET  0x1024U
#define FIOPAD_11_DELAY_OFFSET  0x1028U
#define FIOPAD_12_DELAY_OFFSET  0x102CU
#define FIOPAD_13_DELAY_OFFSET  0x1030U
#define FIOPAD_14_DELAY_OFFSET  0x1034U
#define FIOPAD_23_DELAY_OFFSET  0x1058U
#define FIOPAD_24_DELAY_OFFSET  0x105CU
#define FIOPAD_25_DELAY_OFFSET  0x1060U
#define FIOPAD_26_DELAY_OFFSET  0x1064U
#define FIOPAD_32_DELAY_OFFSET  0x1074U
#define FIOPAD_33_DELAY_OFFSET  0x1078U
#define FIOPAD_34_DELAY_OFFSET  0x107CU
#define FIOPAD_35_DELAY_OFFSET  0x1080U
#define FIOPAD_55_DELAY_OFFSET  0x10D0U
#define FIOPAD_56_DELAY_OFFSET  0x10D4U
#define FIOPAD_57_DELAY_OFFSET  0x10D8U
#define FIOPAD_58_DELAY_OFFSET  0x10DCU
#define FIOPAD_59_DELAY_OFFSET  0x10E0U
#define FIOPAD_60_DELAY_OFFSET  0x10E4U
#define FIOPAD_61_DELAY_OFFSET  0x10E8U
#define FIOPAD_62_DELAY_OFFSET  0x10ECU
#define FIOPAD_63_DELAY_OFFSET  0x10F0U
#define FIOPAD_64_DELAY_OFFSET  0x10F4U
#define FIOPAD_65_DELAY_OFFSET  0x10F8U
#define FIOPAD_66_DELAY_OFFSET  0x10FCU
#define FIOPAD_67_DELAY_OFFSET  0x1100U
#define FIOPAD_68_DELAY_OFFSET  0x1104U
#define FIOPAD_148_DELAY_OFFSET 0x1108U
#define FIOPAD_69_DELAY_OFFSET  0x110CU
#define FIOPAD_70_DELAY_OFFSET  0x1110U
#define FIOPAD_71_DELAY_OFFSET  0x1114U
#define FIOPAD_72_DELAY_OFFSET  0x1118U
#define FIOPAD_73_DELAY_OFFSET  0x111CU
#define FIOPAD_74_DELAY_OFFSET  0x1120U
#define FIOPAD_75_DELAY_OFFSET  0x1124U
#define FIOPAD_76_DELAY_OFFSET  0x1128U
#define FIOPAD_77_DELAY_OFFSET  0x112CU
#define FIOPAD_78_DELAY_OFFSET  0x1130U
#define FIOPAD_80_DELAY_OFFSET  0x1138U
#define FIOPAD_81_DELAY_OFFSET  0x113CU
#define FIOPAD_82_DELAY_OFFSET  0x1140U
#define FIOPAD_83_DELAY_OFFSET  0x1144U
#define FIOPAD_84_DELAY_OFFSET  0x1148U
#define FIOPAD_85_DELAY_OFFSET  0x114CU
#define FIOPAD_86_DELAY_OFFSET  0x1150U
#define FIOPAD_87_DELAY_OFFSET  0x1154U
#define FIOPAD_88_DELAY_OFFSET  0x1158U
#define FIOPAD_89_DELAY_OFFSET  0x115CU
#define FIOPAD_90_DELAY_OFFSET  0x1160U
#define FIOPAD_92_DELAY_OFFSET  0x1168U
#define FIOPAD_93_DELAY_OFFSET  0x116CU
#define FIOPAD_94_DELAY_OFFSET  0x1170U
#define FIOPAD_95_DELAY_OFFSET  0x1174U
#define FIOPAD_96_DELAY_OFFSET  0x1178U
#define FIOPAD_97_DELAY_OFFSET  0x117CU
#define FIOPAD_98_DELAY_OFFSET  0x1180U
#define FIOPAD_99_DELAY_OFFSET  0x118CU
#define FIOPAD_100_DELAY_OFFSET 0x1190U
#define FIOPAD_101_DELAY_OFFSET 0x1194U
#define FIOPAD_102_DELAY_OFFSET 0x1198U
#define FIOPAD_103_DELAY_OFFSET 0x119CU
#define FIOPAD_104_DELAY_OFFSET 0x11A0U
#define FIOPAD_105_DELAY_OFFSET 0x11A4U
#define FIOPAD_106_DELAY_OFFSET 0x11A8U
#define FIOPAD_107_DELAY_OFFSET 0x11ACU
#define FIOPAD_108_DELAY_OFFSET 0x11B0U
#define FIOPAD_109_DELAY_OFFSET 0x11B4U
#define FIOPAD_110_DELAY_OFFSET 0x11B8U
#define FIOPAD_111_DELAY_OFFSET 0x11BCU
#define FIOPAD_112_DELAY_OFFSET 0x11C0U
#define FIOPAD_115_DELAY_OFFSET 0x11CCU
#define FIOPAD_116_DELAY_OFFSET 0x11D0U
#define FIOPAD_117_DELAY_OFFSET 0x11D4U
#define FIOPAD_118_DELAY_OFFSET 0x11D8U
#define FIOPAD_119_DELAY_OFFSET 0x11DCU
#define FIOPAD_120_DELAY_OFFSET 0x11E0U
#define FIOPAD_121_DELAY_OFFSET 0x11E4U
#define FIOPAD_122_DELAY_OFFSET 0x11E8U
#define FIOPAD_123_DELAY_OFFSET 0x11ECU
#define FIOPAD_124_DELAY_OFFSET 0x11F0U
#define FIOPAD_125_DELAY_OFFSET 0x11F4U
#define FIOPAD_126_DELAY_OFFSET 0x11F8U
#define FIOPAD_127_DELAY_OFFSET 0x11FCU
#define FIOPAD_128_DELAY_OFFSET 0x1200U
#define FIOPAD_136_DELAY_OFFSET 0x1220U
#define FIOPAD_137_DELAY_OFFSET 0x1224U
#define FIOPAD_138_DELAY_OFFSET 0x1228U
#define FIOPAD_139_DELAY_OFFSET 0x122CU
#define FIOPAD_140_DELAY_OFFSET 0x1230U
#define FIOPAD_141_DELAY_OFFSET 0x1234U
#define FIOPAD_142_DELAY_OFFSET 0x1238U
#define FIOPAD_143_DELAY_OFFSET 0x123CU
#define FIOPAD_144_DELAY_OFFSET 0x1240U
#define FIOPAD_145_DELAY_OFFSET 0x1244U
#define FIOPAD_146_DELAY_OFFSET 0x1248U
#define FIOPAD_147_DELAY_OFFSET 0x124CU

/* set 32-bit register [a:b] as x, where a is high bit, b is low bit, x is setting/getting value */
#define GET_REG32_BITS(x, a, b)                  (UINT32)((((UINT32)(x)) & GENMASK(a, b)) >> b)
#define SET_REG32_BITS(x, a, b)                  (UINT32)((((UINT32)(x)) << b) & GENMASK(a, b))


/** @name X_reg1 Register
 */
#define FIOPAD_X_REG1_OUT_DELAY_EN              BIT(8)
#define FIOPAD_X_REG1_OUT_DELAY_FRAC_MASK         GENMASK(11, 9)
#define FIOPAD_X_REG1_OUT_DELAY_FRAC_GET(x)       GET_REG32_BITS((x), 11, 9) /* small-scale adjustment */
#define FIOPAD_X_REG1_OUT_DELAY_FRAC_SET(x)       SET_REG32_BITS((x), 11, 9)
#define FIOPAD_X_REG1_OUT_DELAY_ROARSE_MASK       GENMASK(14, 12)
#define FIOPAD_X_REG1_OUT_DELAY_ROARSE_GET(x)     GET_REG32_BITS((x), 14, 12) /* large-scale adjustment */
#define FIOPAD_X_REG1_OUT_DELAY_ROARSE_SET(x)     SET_REG32_BITS((x), 14, 12)

#define FIOPAD_X_REG1_IN_DELAY_EN              BIT(0)
#define FIOPAD_X_REG1_IN_DELAY_FRAC_MASK          GENMASK(3, 1)
#define FIOPAD_X_REG1_IN_DELAY_FRAC_GET(x)        GET_REG32_BITS((x), 3, 1) /* small-scale adjustment */
#define FIOPAD_X_REG1_IN_DELAY_FRAC_SET(x)        SET_REG32_BITS((x), 3, 1)
#define FIOPAD_X_REG1_IN_DELAY_ROARSE_MASK        GENMASK(6, 4)
#define FIOPAD_X_REG1_IN_DELAY_ROARSE_GET(x)      GET_REG32_BITS((x), 6, 4) /* large-scale adjustment */
#define FIOPAD_X_REG1_IN_DELAY_ROARSE_SET(x)      SET_REG32_BITS((x), 6, 4)

#define FIOPAD_DELAY_MAX                       15


typedef struct
{
    UINT32 reg_off; /* register offset */
    UINT32 reg_bit; /* start bit */
} FPinIndex;

typedef enum
{
    FPIN_PULL_NONE = 0b00,
    FPIN_PULL_DOWN = 0b01,
    FPIN_PULL_UP = 0b10,

    FPIN_NUM_OF_PULL
} FPinPull;

typedef enum
{
    FPIN_OUTPUT_DELAY = 0,
    FPIN_INPUT_DELAY,

    FPIN_NUM_OF_DELAY_DIR
} FPinDelayDir;

typedef enum
{
    FPIN_ROARSE_DELAY = 0, /* roar. large-scale adjustment */
    FPIN_FRAC_DELAY, /* fraction. small-scale adjustment */

    FPIN_NUM_OF_DELAY_TYPE
} FPinDelayType;

typedef enum 
{
    FPIN_DELAY_NONE = 0,
    FPIN_DELAY_1,
    FPIN_DELAY_2,
    FPIN_DELAY_3,
    FPIN_DELAY_4,
    FPIN_DELAY_5,
    FPIN_DELAY_6,
    FPIN_DELAY_7,

    FPIN_NUM_OF_DELAY
} FPinDelay;


/* E2000Q reg0/reg1 offset */

#define  AN59_reg0     0x0000
#define  AW47_reg0     0x0004
#define  AR55_reg0     0x0020
#define  AJ55_reg0     0x0024
#define  AL55_reg0     0x0028
#define  AL53_reg0     0x002c
#define  AN51_reg0     0x0030
#define  AR51_reg0     0x0034
#define  BA57_reg0     0x0038
#define  BA59_reg0     0x003c
#define  AW57_reg0     0x0040
#define  AW59_reg0     0x0044
#define  AU55_reg0     0x0048
#define  AN57_reg0     0x004c
#define  AL59_reg0     0x0050
#define  AJ59_reg0     0x0054
#define  AJ57_reg0     0x0058
#define  AG59_reg0     0x005c
#define  AG57_reg0     0x0060
#define  AE59_reg0     0x0064
#define  AC59_reg0     0x0068
#define  AC57_reg0     0x006c
#define  AR49_reg0     0x0070
#define  BA55_reg0     0x0074
#define  BA53_reg0     0x0078
#define  AR59_reg0     0x007c
#define  AU59_reg0     0x0080
#define  AR57_reg0     0x0084
#define  BA49_reg0     0x0088
#define  AW55_reg0     0x008c
#define  A35_reg0      0x0090
#define  R57_reg0      0x0094
#define  R59_reg0      0x0098
#define  U59_reg0      0x009c
#define  W59_reg0      0x00a0
#define  U57_reg0      0x00a4
#define  AA57_reg0     0x00a8
#define  AA59_reg0     0x00ac
#define  AW51_reg0     0x00b0
#define  AU51_reg0     0x00b4
#define  A39_reg0      0x00b8
#define  C39_reg0      0x00bc
#define  C37_reg0      0x00c0
#define  A37_reg0      0x00c4
#define  A41_reg0      0x00c8
#define  A43_reg0      0x00cc
#define  A45_reg0      0x00d0
#define  C45_reg0      0x00d4
#define  A47_reg0      0x00d8
#define  A49_reg0      0x00dc
#define  C49_reg0      0x00e0
#define  A51_reg0      0x00e4
#define  A33_reg0      0x00e8
#define  C33_reg0      0x00ec
#define  C31_reg0      0x00f0
#define  A31_reg0      0x00f4
#define  AJ53_reg0     0x00f8
#define  AL49_reg0     0x00fc
#define  AL47_reg0     0x0100
#define  AN49_reg0     0x0104
#define  AG51_reg0     0x0108
#define  AJ51_reg0     0x010c
#define  AG49_reg0     0x0110
#define  AE55_reg0     0x0114
#define  AE53_reg0     0x0118
#define  AG55_reg0     0x011c
#define  AJ49_reg0     0x0120
#define  AC55_reg0     0x0124
#define  AC53_reg0     0x0128
#define  AE51_reg0     0x012c
#define  W51_reg0      0x0130
#define  W55_reg0      0x0134
#define  W53_reg0      0x0138
#define  U55_reg0      0x013c
#define  U53_reg0      0x0140
#define  AE49_reg0     0x0144
#define  AC49_reg0     0x0148
#define  AE47_reg0     0x014c
#define  AA47_reg0     0x0150
#define  AA49_reg0     0x0154
#define  W49_reg0      0x0158
#define  AA51_reg0     0x015c
#define  U49_reg0      0x0160
#define  G59_reg0      0x0164
#define  J59_reg0      0x0168
#define  L57_reg0      0x016c
#define  C59_reg0      0x0170
#define  E59_reg0      0x0174
#define  J57_reg0      0x0178
#define  L59_reg0      0x017c
#define  N59_reg0      0x0180
#define  C57_reg0      0x0184
#define  E57_reg0      0x0188
#define  E31_reg0      0x018c
#define  G31_reg0      0x0190
#define  N41_reg0      0x0194
#define  N39_reg0      0x0198
#define  J33_reg0      0x019c
#define  N33_reg0      0x01a0
#define  L33_reg0      0x01a4
#define  N45_reg0      0x01a8
#define  N43_reg0      0x01ac
#define  L31_reg0      0x01b0
#define  J31_reg0      0x01b4
#define  J29_reg0      0x01b8
#define  E29_reg0      0x01bc
#define  G29_reg0      0x01c0
#define  N27_reg0      0x01c4
#define  L29_reg0      0x01c8
#define  J37_reg0      0x01cc
#define  J39_reg0      0x01d0
#define  G41_reg0      0x01d4
#define  E43_reg0      0x01d8
#define  L43_reg0      0x01dc
#define  C43_reg0      0x01e0
#define  E41_reg0      0x01e4
#define  L45_reg0      0x01e8
#define  J43_reg0      0x01ec
#define  J41_reg0      0x01f0
#define  L39_reg0      0x01f4
#define  E37_reg0      0x01f8
#define  E35_reg0      0x01fc
#define  G35_reg0      0x0200
#define  J35_reg0      0x0204
#define  L37_reg0      0x0208
#define  N35_reg0      0x020c
#define  R51_reg0      0x0210
#define  R49_reg0      0x0214
#define  N51_reg0      0x0218
#define  N55_reg0      0x021c
#define  L55_reg0      0x0220
#define  J55_reg0      0x0224
#define  J45_reg0      0x0228
#define  E47_reg0      0x022c
#define  G47_reg0      0x0230
#define  J47_reg0      0x0234
#define  J49_reg0      0x0238
#define  N49_reg0      0x023c
#define  L51_reg0      0x0240
#define  L49_reg0      0x0244
#define  N53_reg0      0x0248
#define  J53_reg0      0x024c

#define  AJ55_reg1     0x1024
#define  AL55_reg1     0x1028
#define  AL53_reg1     0x102c
#define  AN51_reg1     0x1030
#define  AR51_reg1     0x1034
#define  AJ57_reg1     0x1058
#define  AG59_reg1     0x105c
#define  AG57_reg1     0x1060
#define  AE59_reg1     0x1064
#define  BA55_reg1     0x1074
#define  BA53_reg1     0x1078
#define  AR59_reg1     0x107c
#define  AU59_reg1     0x1080
#define  A45_reg1      0x10d0
#define  C45_reg1      0x10d4
#define  A47_reg1      0x10d8
#define  A49_reg1      0x10dc
#define  C49_reg1      0x10e0
#define  A51_reg1      0x10e4
#define  A33_reg1      0x10e8
#define  C33_reg1      0x10ec
#define  C31_reg1      0x10f0
#define  A31_reg1      0x10f4
#define  AJ53_reg1     0x10f8
#define  AL49_reg1     0x10fc
#define  AL47_reg1     0x1100
#define  AN49_reg1     0x1104
#define  AG51_reg1     0x1108
#define  AJ51_reg1     0x110c
#define  AG49_reg1     0x1110
#define  AE55_reg1     0x1114
#define  AE53_reg1     0x1118
#define  AG55_reg1     0x111c
#define  AJ49_reg1     0x1120
#define  AC55_reg1     0x1124
#define  AC53_reg1     0x1128
#define  AE51_reg1     0x112c
#define  W51_reg1      0x1130
#define  W53_reg1      0x1138
#define  U55_reg1      0x113c
#define  U53_reg1      0x1140
#define  AE49_reg1     0x1144
#define  AC49_reg1     0x1148
#define  AE47_reg1     0x114c
#define  AA47_reg1     0x1150
#define  AA49_reg1     0x1154
#define  W49_reg1      0x1158
#define  AA51_reg1     0x115c
#define  U49_reg1      0x1160
#define  J59_reg1      0x1168
#define  L57_reg1      0x116c
#define  C59_reg1      0x1170
#define  E59_reg1      0x1174
#define  J57_reg1      0x1178
#define  L59_reg1      0x117c
#define  N59_reg1      0x1180
#define  E31_reg1      0x118c
#define  G31_reg1      0x1190
#define  N41_reg1      0x1194
#define  N39_reg1      0x1198
#define  J33_reg1      0x119c
#define  N33_reg1      0x11a0
#define  L33_reg1      0x11a4
#define  N45_reg1      0x11a8
#define  N43_reg1      0x11ac
#define  L31_reg1      0x11b0
#define  J31_reg1      0x11b4
#define  J29_reg1      0x11b8
#define  E29_reg1      0x11bc
#define  G29_reg1      0x11c0
#define  J37_reg1      0x11cc
#define  J39_reg1      0x11d0
#define  G41_reg1      0x11d4
#define  E43_reg1      0x11d8
#define  L43_reg1      0x11dc
#define  C43_reg1      0x11e0
#define  E41_reg1      0x11e4
#define  L45_reg1      0x11e8
#define  J43_reg1      0x11ec
#define  J41_reg1      0x11f0
#define  L39_reg1      0x11f4
#define  E37_reg1      0x11f8
#define  E35_reg1      0x11fc
#define  G35_reg1      0x1200
#define  L55_reg1      0x1220
#define  J55_reg1      0x1224
#define  J45_reg1      0x1228
#define  E47_reg1      0x122c
#define  G47_reg1      0x1230
#define  J47_reg1      0x1234
#define  J49_reg1      0x1238
#define  N49_reg1      0x123c
#define  L51_reg1      0x1240
#define  L49_reg1      0x1244
#define  N53_reg1      0x1248
#define  J53_reg1      0x124c

/************************** Constant Definitions *****************************/
/* register offset of iopad function / pull / driver strength */
#define FIOPAD_AN59     (FIOPAD_0_FUNC_OFFSET)
#define FIOPAD_AW47     (FIOPAD_2_FUNC_OFFSET)
#define FIOPAD_AR55     (FIOPAD_9_FUNC_OFFSET)
#define FIOPAD_AJ55     (FIOPAD_10_FUNC_OFFSET)
#define FIOPAD_AL55     (FIOPAD_11_FUNC_OFFSET)
#define FIOPAD_AL53     (FIOPAD_12_FUNC_OFFSET)
#define FIOPAD_AN51     (FIOPAD_13_FUNC_OFFSET)
#define FIOPAD_AR51     (FIOPAD_14_FUNC_OFFSET)
#define FIOPAD_BA57     (FIOPAD_15_FUNC_OFFSET)
#define FIOPAD_BA59     (FIOPAD_16_FUNC_OFFSET)
#define FIOPAD_AW57     (FIOPAD_17_FUNC_OFFSET)
#define FIOPAD_AW59     (FIOPAD_18_FUNC_OFFSET)
#define FIOPAD_AU55     (FIOPAD_19_FUNC_OFFSET)
#define FIOPAD_AN57     (FIOPAD_20_FUNC_OFFSET)
#define FIOPAD_AL59     (FIOPAD_21_FUNC_OFFSET)
#define FIOPAD_AJ59     (FIOPAD_22_FUNC_OFFSET)
#define FIOPAD_AJ57     (FIOPAD_23_FUNC_OFFSET)
#define FIOPAD_AG59     (FIOPAD_24_FUNC_OFFSET)
#define FIOPAD_AG57     (FIOPAD_25_FUNC_OFFSET)
#define FIOPAD_AE59     (FIOPAD_26_FUNC_OFFSET)
#define FIOPAD_AC59     (FIOPAD_27_FUNC_OFFSET)
#define FIOPAD_AC57     (FIOPAD_28_FUNC_OFFSET)
#define FIOPAD_AR49     (FIOPAD_31_FUNC_OFFSET)
#define FIOPAD_BA55     (FIOPAD_32_FUNC_OFFSET)
#define FIOPAD_BA53     (FIOPAD_33_FUNC_OFFSET)
#define FIOPAD_AR59     (FIOPAD_34_FUNC_OFFSET)
#define FIOPAD_AU59     (FIOPAD_35_FUNC_OFFSET)
#define FIOPAD_AR57     (FIOPAD_36_FUNC_OFFSET)
#define FIOPAD_BA49     (FIOPAD_37_FUNC_OFFSET)
#define FIOPAD_AW55     (FIOPAD_38_FUNC_OFFSET)
#define FIOPAD_A35      (FIOPAD_39_FUNC_OFFSET)
#define FIOPAD_R57      (FIOPAD_40_FUNC_OFFSET)
#define FIOPAD_R59      (FIOPAD_41_FUNC_OFFSET)
#define FIOPAD_U59      (FIOPAD_42_FUNC_OFFSET)
#define FIOPAD_W59      (FIOPAD_43_FUNC_OFFSET)
#define FIOPAD_U57      (FIOPAD_44_FUNC_OFFSET)
#define FIOPAD_AA57     (FIOPAD_45_FUNC_OFFSET)
#define FIOPAD_AA59     (FIOPAD_46_FUNC_OFFSET)
#define FIOPAD_AW51     (FIOPAD_47_FUNC_OFFSET)
#define FIOPAD_AU51     (FIOPAD_48_FUNC_OFFSET)
#define FIOPAD_A39      (FIOPAD_49_FUNC_OFFSET)
#define FIOPAD_C39      (FIOPAD_50_FUNC_OFFSET)
#define FIOPAD_C37      (FIOPAD_51_FUNC_OFFSET)
#define FIOPAD_A37      (FIOPAD_52_FUNC_OFFSET)
#define FIOPAD_A41      (FIOPAD_53_FUNC_OFFSET)
#define FIOPAD_A43      (FIOPAD_54_FUNC_OFFSET)
#define FIOPAD_A45      (FIOPAD_55_FUNC_OFFSET)
#define FIOPAD_C45      (FIOPAD_56_FUNC_OFFSET)
#define FIOPAD_A47      (FIOPAD_57_FUNC_OFFSET)
#define FIOPAD_A49      (FIOPAD_58_FUNC_OFFSET)
#define FIOPAD_C49      (FIOPAD_59_FUNC_OFFSET)
#define FIOPAD_A51      (FIOPAD_60_FUNC_OFFSET)
#define FIOPAD_A33      (FIOPAD_61_FUNC_OFFSET)
#define FIOPAD_C33      (FIOPAD_62_FUNC_OFFSET)
#define FIOPAD_C31      (FIOPAD_63_FUNC_OFFSET)
#define FIOPAD_A31      (FIOPAD_64_FUNC_OFFSET)
#define FIOPAD_AJ53     (FIOPAD_65_FUNC_OFFSET)
#define FIOPAD_AL49     (FIOPAD_66_FUNC_OFFSET)
#define FIOPAD_AL47     (FIOPAD_67_FUNC_OFFSET)
#define FIOPAD_AN49     (FIOPAD_68_FUNC_OFFSET)
#define FIOPAD_AG51     (FIOPAD_148_FUNC_OFFSET)
#define FIOPAD_AJ51     (FIOPAD_69_FUNC_OFFSET)
#define FIOPAD_AG49     (FIOPAD_70_FUNC_OFFSET)
#define FIOPAD_AE55     (FIOPAD_71_FUNC_OFFSET)
#define FIOPAD_AE53     (FIOPAD_72_FUNC_OFFSET)
#define FIOPAD_AG55     (FIOPAD_73_FUNC_OFFSET)
#define FIOPAD_AJ49     (FIOPAD_74_FUNC_OFFSET)
#define FIOPAD_AC55     (FIOPAD_75_FUNC_OFFSET)
#define FIOPAD_AC53     (FIOPAD_76_FUNC_OFFSET)
#define FIOPAD_AE51     (FIOPAD_77_FUNC_OFFSET)
#define FIOPAD_W51      (FIOPAD_78_FUNC_OFFSET)
#define FIOPAD_W55      (FIOPAD_79_FUNC_OFFSET)
#define FIOPAD_W53      (FIOPAD_80_FUNC_OFFSET)
#define FIOPAD_U55      (FIOPAD_81_FUNC_OFFSET)
#define FIOPAD_U53      (FIOPAD_82_FUNC_OFFSET)
#define FIOPAD_AE49     (FIOPAD_83_FUNC_OFFSET)
#define FIOPAD_AC49     (FIOPAD_84_FUNC_OFFSET)
#define FIOPAD_AE47     (FIOPAD_85_FUNC_OFFSET)
#define FIOPAD_AA47     (FIOPAD_86_FUNC_OFFSET)
#define FIOPAD_AA49     (FIOPAD_87_FUNC_OFFSET)
#define FIOPAD_W49      (FIOPAD_88_FUNC_OFFSET)
#define FIOPAD_AA51     (FIOPAD_89_FUNC_OFFSET)
#define FIOPAD_U49      (FIOPAD_90_FUNC_OFFSET)
#define FIOPAD_G59      (FIOPAD_91_FUNC_OFFSET)
#define FIOPAD_J59      (FIOPAD_92_FUNC_OFFSET)
#define FIOPAD_L57      (FIOPAD_93_FUNC_OFFSET)
#define FIOPAD_C59      (FIOPAD_94_FUNC_OFFSET)
#define FIOPAD_E59      (FIOPAD_95_FUNC_OFFSET)
#define FIOPAD_J57      (FIOPAD_96_FUNC_OFFSET)
#define FIOPAD_L59      (FIOPAD_97_FUNC_OFFSET)
#define FIOPAD_N59      (FIOPAD_98_FUNC_OFFSET)
#define FIOPAD_C57      (FIOPAD_29_FUNC_OFFSET)
#define FIOPAD_E57      (FIOPAD_30_FUNC_OFFSET)
#define FIOPAD_E31      (FIOPAD_99_FUNC_OFFSET)
#define FIOPAD_G31      (FIOPAD_100_FUNC_OFFSET)
#define FIOPAD_N41      (FIOPAD_101_FUNC_OFFSET)
#define FIOPAD_N39      (FIOPAD_102_FUNC_OFFSET)
#define FIOPAD_J33      (FIOPAD_103_FUNC_OFFSET)
#define FIOPAD_N33      (FIOPAD_104_FUNC_OFFSET)
#define FIOPAD_L33      (FIOPAD_105_FUNC_OFFSET)
#define FIOPAD_N45      (FIOPAD_106_FUNC_OFFSET)
#define FIOPAD_N43      (FIOPAD_107_FUNC_OFFSET)
#define FIOPAD_L31      (FIOPAD_108_FUNC_OFFSET)
#define FIOPAD_J31      (FIOPAD_109_FUNC_OFFSET)
#define FIOPAD_J29      (FIOPAD_110_FUNC_OFFSET)
#define FIOPAD_E29      (FIOPAD_111_FUNC_OFFSET)
#define FIOPAD_G29      (FIOPAD_112_FUNC_OFFSET)
#define FIOPAD_N27      (FIOPAD_113_FUNC_OFFSET)
#define FIOPAD_L29      (FIOPAD_114_FUNC_OFFSET)
#define FIOPAD_J37      (FIOPAD_115_FUNC_OFFSET)
#define FIOPAD_J39      (FIOPAD_116_FUNC_OFFSET)
#define FIOPAD_G41      (FIOPAD_117_FUNC_OFFSET)
#define FIOPAD_E43      (FIOPAD_118_FUNC_OFFSET)
#define FIOPAD_L43      (FIOPAD_119_FUNC_OFFSET)
#define FIOPAD_C43      (FIOPAD_120_FUNC_OFFSET)
#define FIOPAD_E41      (FIOPAD_121_FUNC_OFFSET)
#define FIOPAD_L45      (FIOPAD_122_FUNC_OFFSET)
#define FIOPAD_J43      (FIOPAD_123_FUNC_OFFSET)
#define FIOPAD_J41      (FIOPAD_124_FUNC_OFFSET)
#define FIOPAD_L39      (FIOPAD_125_FUNC_OFFSET)
#define FIOPAD_E37      (FIOPAD_126_FUNC_OFFSET)
#define FIOPAD_E35      (FIOPAD_127_FUNC_OFFSET)
#define FIOPAD_G35      (FIOPAD_128_FUNC_OFFSET)
#define FIOPAD_J35      (FIOPAD_129_FUNC_OFFSET)
#define FIOPAD_L37      (FIOPAD_130_FUNC_OFFSET)
#define FIOPAD_N35      (FIOPAD_131_FUNC_OFFSET)
#define FIOPAD_R51      (FIOPAD_132_FUNC_OFFSET)
#define FIOPAD_R49      (FIOPAD_133_FUNC_OFFSET)
#define FIOPAD_N51      (FIOPAD_134_FUNC_OFFSET)
#define FIOPAD_N55      (FIOPAD_135_FUNC_OFFSET)
#define FIOPAD_L55      (FIOPAD_136_FUNC_OFFSET)
#define FIOPAD_J55      (FIOPAD_137_FUNC_OFFSET)
#define FIOPAD_J45      (FIOPAD_138_FUNC_OFFSET)
#define FIOPAD_E47      (FIOPAD_139_FUNC_OFFSET)
#define FIOPAD_G47      (FIOPAD_140_FUNC_OFFSET)
#define FIOPAD_J47      (FIOPAD_141_FUNC_OFFSET)
#define FIOPAD_J49      (FIOPAD_142_FUNC_OFFSET)
#define FIOPAD_N49      (FIOPAD_143_FUNC_OFFSET)
#define FIOPAD_L51      (FIOPAD_144_FUNC_OFFSET)
#define FIOPAD_L49      (FIOPAD_145_FUNC_OFFSET)
#define FIOPAD_N53      (FIOPAD_146_FUNC_OFFSET)
#define FIOPAD_J53      (FIOPAD_147_FUNC_OFFSET)

/* register offset of iopad delay */
#define FIOPAD_AJ55_DELAY	(FIOPAD_10_DELAY_OFFSET)
#define FIOPAD_AL55_DELAY	(FIOPAD_11_DELAY_OFFSET)
#define FIOPAD_AL53_DELAY	(FIOPAD_12_DELAY_OFFSET)
#define FIOPAD_AN51_DELAY	(FIOPAD_13_DELAY_OFFSET)
#define FIOPAD_AR51_DELAY	(FIOPAD_14_DELAY_OFFSET)
#define FIOPAD_AJ57_DELAY	(FIOPAD_23_DELAY_OFFSET)
#define FIOPAD_AG59_DELAY	(FIOPAD_24_DELAY_OFFSET)
#define FIOPAD_AG57_DELAY	(FIOPAD_25_DELAY_OFFSET)
#define FIOPAD_AE59_DELAY	(FIOPAD_26_DELAY_OFFSET)
#define FIOPAD_BA55_DELAY	(FIOPAD_32_DELAY_OFFSET)
#define FIOPAD_BA53_DELAY	(FIOPAD_33_DELAY_OFFSET)
#define FIOPAD_AR59_DELAY	(FIOPAD_34_DELAY_OFFSET)
#define FIOPAD_AU59_DELAY	(FIOPAD_35_DELAY_OFFSET)
#define FIOPAD_A45_DELAY    (FIOPAD_55_DELAY_OFFSET)
#define FIOPAD_C45_DELAY    (FIOPAD_56_DELAY_OFFSET)
#define FIOPAD_A47_DELAY    (FIOPAD_57_DELAY_OFFSET)
#define FIOPAD_A49_DELAY    (FIOPAD_58_DELAY_OFFSET)
#define FIOPAD_C49_DELAY    (FIOPAD_59_DELAY_OFFSET)
#define FIOPAD_A51_DELAY    (FIOPAD_60_DELAY_OFFSET)
#define FIOPAD_A33_DELAY    (FIOPAD_61_DELAY_OFFSET)
#define FIOPAD_C33_DELAY    (FIOPAD_62_DELAY_OFFSET)
#define FIOPAD_C31_DELAY    (FIOPAD_63_DELAY_OFFSET)
#define FIOPAD_A31_DELAY    (FIOPAD_64_DELAY_OFFSET)
#define FIOPAD_AJ53_DELAY	(FIOPAD_65_DELAY_OFFSET)
#define FIOPAD_AL49_DELAY	(FIOPAD_66_DELAY_OFFSET)
#define FIOPAD_AL47_DELAY	(FIOPAD_67_DELAY_OFFSET)
#define FIOPAD_AN49_DELAY	(FIOPAD_68_DELAY_OFFSET)
#define FIOPAD_AG51_DELAY	(FIOPAD_148_DELAY_OFFSET)
#define FIOPAD_AJ51_DELAY	(FIOPAD_69_DELAY_OFFSET)
#define FIOPAD_AG49_DELAY	(FIOPAD_70_DELAY_OFFSET)
#define FIOPAD_AE55_DELAY	(FIOPAD_71_DELAY_OFFSET)
#define FIOPAD_AE53_DELAY	(FIOPAD_72_DELAY_OFFSET)
#define FIOPAD_AG55_DELAY	(FIOPAD_73_DELAY_OFFSET)
#define FIOPAD_AJ49_DELAY	(FIOPAD_74_DELAY_OFFSET)
#define FIOPAD_AC55_DELAY	(FIOPAD_75_DELAY_OFFSET)
#define FIOPAD_AC53_DELAY	(FIOPAD_76_DELAY_OFFSET)
#define FIOPAD_AE51_DELAY	(FIOPAD_77_DELAY_OFFSET)
#define FIOPAD_W51_DELAY    (FIOPAD_78_DELAY_OFFSET)
#define FIOPAD_W53_DELAY    (FIOPAD_80_DELAY_OFFSET)
#define FIOPAD_U55_DELAY    (FIOPAD_81_DELAY_OFFSET)
#define FIOPAD_U53_DELAY    (FIOPAD_82_DELAY_OFFSET)
#define FIOPAD_AE49_DELAY	(FIOPAD_83_DELAY_OFFSET)
#define FIOPAD_AC49_DELAY	(FIOPAD_84_DELAY_OFFSET)
#define FIOPAD_AE47_DELAY	(FIOPAD_85_DELAY_OFFSET)
#define FIOPAD_AA47_DELAY	(FIOPAD_86_DELAY_OFFSET)
#define FIOPAD_AA49_DELAY	(FIOPAD_87_DELAY_OFFSET)
#define FIOPAD_W49_DELAY    (FIOPAD_88_DELAY_OFFSET)
#define FIOPAD_AA51_DELAY	(FIOPAD_89_DELAY_OFFSET)
#define FIOPAD_U49_DELAY    (FIOPAD_90_DELAY_OFFSET)
#define FIOPAD_J59_DELAY    (FIOPAD_92_DELAY_OFFSET)
#define FIOPAD_L57_DELAY    (FIOPAD_93_DELAY_OFFSET)
#define FIOPAD_C59_DELAY    (FIOPAD_94_DELAY_OFFSET)
#define FIOPAD_E59_DELAY    (FIOPAD_95_DELAY_OFFSET)
#define FIOPAD_J57_DELAY    (FIOPAD_96_DELAY_OFFSET)
#define FIOPAD_L59_DELAY    (FIOPAD_97_DELAY_OFFSET)
#define FIOPAD_N59_DELAY    (FIOPAD_98_DELAY_OFFSET)
#define FIOPAD_E31_DELAY    (FIOPAD_99_DELAY_OFFSET)
#define FIOPAD_G31_DELAY    (FIOPAD_100_DELAY_OFFSET)
#define FIOPAD_N41_DELAY    (FIOPAD_101_DELAY_OFFSET)
#define FIOPAD_N39_DELAY    (FIOPAD_102_DELAY_OFFSET)
#define FIOPAD_J33_DELAY    (FIOPAD_103_DELAY_OFFSET)
#define FIOPAD_N33_DELAY    (FIOPAD_104_DELAY_OFFSET)
#define FIOPAD_L33_DELAY    (FIOPAD_105_DELAY_OFFSET)
#define FIOPAD_N45_DELAY    (FIOPAD_106_DELAY_OFFSET)
#define FIOPAD_N43_DELAY    (FIOPAD_107_DELAY_OFFSET)
#define FIOPAD_L31_DELAY    (FIOPAD_108_DELAY_OFFSET)
#define FIOPAD_J31_DELAY    (FIOPAD_109_DELAY_OFFSET)
#define FIOPAD_J29_DELAY    (FIOPAD_110_DELAY_OFFSET)
#define FIOPAD_E29_DELAY    (FIOPAD_111_DELAY_OFFSET)
#define FIOPAD_G29_DELAY    (FIOPAD_112_DELAY_OFFSET)
#define FIOPAD_J37_DELAY    (FIOPAD_115_DELAY_OFFSET)
#define FIOPAD_J39_DELAY    (FIOPAD_116_DELAY_OFFSET)
#define FIOPAD_G41_DELAY    (FIOPAD_117_DELAY_OFFSET)
#define FIOPAD_E43_DELAY    (FIOPAD_118_DELAY_OFFSET)
#define FIOPAD_L43_DELAY    (FIOPAD_119_DELAY_OFFSET)
#define FIOPAD_C43_DELAY    (FIOPAD_120_DELAY_OFFSET)
#define FIOPAD_E41_DELAY    (FIOPAD_121_DELAY_OFFSET)
#define FIOPAD_L45_DELAY    (FIOPAD_122_DELAY_OFFSET)
#define FIOPAD_J43_DELAY    (FIOPAD_123_DELAY_OFFSET)
#define FIOPAD_J41_DELAY    (FIOPAD_124_DELAY_OFFSET)
#define FIOPAD_L39_DELAY    (FIOPAD_125_DELAY_OFFSET)
#define FIOPAD_E37_DELAY    (FIOPAD_126_DELAY_OFFSET)
#define FIOPAD_E35_DELAY    (FIOPAD_127_DELAY_OFFSET)
#define FIOPAD_G35_DELAY    (FIOPAD_128_DELAY_OFFSET)
#define FIOPAD_L55_DELAY    (FIOPAD_136_DELAY_OFFSET)
#define FIOPAD_J55_DELAY    (FIOPAD_137_DELAY_OFFSET)
#define FIOPAD_J45_DELAY    (FIOPAD_138_DELAY_OFFSET)
#define FIOPAD_E47_DELAY    (FIOPAD_139_DELAY_OFFSET)
#define FIOPAD_G47_DELAY    (FIOPAD_140_DELAY_OFFSET)
#define FIOPAD_J47_DELAY    (FIOPAD_141_DELAY_OFFSET)
#define FIOPAD_J49_DELAY    (FIOPAD_142_DELAY_OFFSET)
#define FIOPAD_N49_DELAY    (FIOPAD_143_DELAY_OFFSET)
#define FIOPAD_L51_DELAY    (FIOPAD_144_DELAY_OFFSET)
#define FIOPAD_L49_DELAY    (FIOPAD_145_DELAY_OFFSET)
#define FIOPAD_N53_DELAY    (FIOPAD_146_DELAY_OFFSET)
#define FIOPAD_J53_DELAY    (FIOPAD_147_DELAY_OFFSET)

void pinPWM0(void);
void pinBand3Mio3A(void);
void pinBand3Mio3B(void);
void pinBank4Mio8A(void);
void pinBank4Mio8B(void);
void pinBank2Mio8A(void);
void pinBank2Mio8B(void);
void pinSpim0Cs0(void);
void pinSpim0Rxd(void);
void pinSpim0Txd(void);
void pinSpim0Sclk(void);
void pinSpim1Cs0(void);
void pinSpim1Rxd(void);
void pinSpim1Txd(void);
void pinSpim1Sclk(void);
void pinSpim2Cs0(void);
void pinSpim2Rxd(void);
void pinSpim2Txd(void);
void pinSpim2Sclk(void);
void pinSpim3Cs0(void);
void pinSpim3Rxd(void);
void pinSpim3Txd(void);
void pinSpim3Sclk(void);
void pinAdc0Chan0(void);
void pinAdc0Chan1(void);
void pinAdc0Chan2(void);
void pinAdc0Chan3(void);
void pinAdc0Chan4(void);
void pinAdc0Chan5(void);
void pinAdc0Chan6(void);
void pinAdc0Chan7(void);
void pinAdc1Chan0(void);
void pinAdc1Chan1(void);
void pinAdc1Chan2(void);
void pinAdc1Chan3(void);
void pinAdc1Chan4(void);
void pinAdc1Chan5(void);
void pinAdc1Chan6(void);
void pinAdc1Chan7(void);
void pinAJ49Delay(FPinDelay largeScaleDelay, FPinDelay smallScaleDelay);
void pinJ57Delay(FPinDelay largeScaleDelay, FPinDelay smallScaleDelay);

#ifdef __cplusplus
}
#endif
#endif /* __FT_PINMUX_H */
