/* vxbArmGenIntCtlrV3.h - ARM Generic interrupt controller driver  */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#ifndef __INCvxbArmGenIntCtlrV3h
#define __INCvxbArmGenIntCtlrV3h

#ifdef __cplusplus
extern "C"{
#endif


/*
 * the explanation of some abbreviations
 *
 * CPC - Cross Processor Call
 * IPI - Inter Processor Interrupt
 * GIC - General Interrupt Controller
 * SGI - Software Generated Interrupt
 * PPI - Private Peripheral Interrupt
 * SPI - Shared Peripheral Interrupt
 */
 
#define SGI_INT_MAX             16
#define ARM_GIC_IPI_COUNT       16    /* MPCore IPI count         */
#define SPI_START_INT_NUM       32    /* SPI start at ID32        */
#define PPI_START_INT_NUM       16    /* PPI start at ID16        */
#define GIC_INT_MAX_NUM         1020  /* GIC max interrupts count */


/* Distributor registers */

#define GICD_CTLR                       0x0000
#define GICD_TYPER                      0x0004
#define GICD_IIDR                       0x0008
#define GICD_STATUSR                    0x0010
#define GICD_SETSPI_NSR                 0x0040
#define GICD_CLRSPI_NSR                 0x0048
#define GICD_SETSPI_SR                  0x0050
#define GICD_CLRSPI_SR                  0x0058
#define GICD_SEIR                       0x0068
#define GICD_IGROUPR                    0x0080
#define GICD_ISENABLER                  0x0100
#define GICD_ICENABLER                  0x0180
#define GICD_ISPENDR                    0x0200
#define GICD_ICPENDR                    0x0280
#define GICD_ISACTIVER                  0x0300
#define GICD_ICACTIVER                  0x0380
#define GICD_IPRIORITYR                 0x0400
#define GICD_ICFGR                      0x0C00
#define GICD_IGRPMODR                   0x0D00
#define GICD_NSACR                      0x0E00
#define GICD_IROUTER                    0x6000
#define GICD_IDREGS                     0xFFD0
#define GICD_PIDR2                      0xFFE8

/* GICD_CTLR: bits When access is Non-secure (different from secure)*/

#define GICD_CTLR_RWP                   (1U << 31)
#define GICD_CTLR_DS                    (1U << 6)
#define GICD_CTLR_ARE_NS                (1U << 4)
#define GICD_CTLR_ENABLE_G1A            (1U << 1)
#define GICD_CTLR_ENABLE_G1             (1U << 0)

#define GICD_PIDR2_ARCHREV_MASK         (0xf0)
#define GICD_PIDR2_ARCHREV_SHIFT        (4)
#define GICD_PIDR2_ARCHREV(n)           (((n) & GICD_PIDR2_ARCHREV_MASK) >> GICD_PIDR2_ARCHREV_SHIFT)
#define GICD_PIDR2_ARCHREV_GICV3        (3)
#define GICD_PIDR2_ARCHREV_GICV4        (4)

#define GICD_TYPER_LPIS                 (0x1U << 17)
#define GICD_TYPER_ITLINESNUMBER_MASK   (0x1f)
#define GICD_TYPER_ITLINESNUMBER_SHIFT  (0)

#define GICD_IIDR_PRODUCT_ID(n)         (((n) & 0xF0000000U) >> 24)
#define GICD_IIDR_PRODUCT_ID_GIC500     0x0
#define GICD_IIDR_PRODUCT_ID_GIC600     0x2


/* Re-Distributor registers */

#define GICR_CTLR                       GICD_CTLR
#define GICR_IIDR                       0x0004
#define GICR_TYPER                      0x0008
#define GICR_STATUSR                    GICD_STATUSR
#define GICR_WAKER                      0x0014
#define GICR_SETLPIR                    0x0040
#define GICR_CLRLPIR                    0x0048
#define GICR_SEIR                       GICD_SEIR
#define GICR_PROPBASER                  0x0070
#define GICR_PENDBASER                  0x0078
#define GICR_INVLPIR                    0x00A0
#define GICR_INVALLR                    0x00B0
#define GICR_SYNCR                      0x00C0
#define GICR_MOVLPIR                    0x0100
#define GICR_MOVALLR                    0x0110
#define GICR_PIDR2                      GICD_PIDR2
#define GICR_IGROUPR0                   GICD_IGROUPR
#define GICR_ISENABLER0                 GICD_ISENABLER
#define GICR_ICENABLER0                 GICD_ICENABLER
#define GICR_ISPENDR0                   GICD_ISPENDR
#define GICR_ICPENDR0                   GICD_ICPENDR
#define GICR_ISACTIVER0                 GICD_ISACTIVER
#define GICR_ICACTIVER0                 GICD_ICACTIVER
#define GICR_IPRIORITYR0                GICD_IPRIORITYR
#define GICR_ICFGR0                     GICD_ICFGR
#define GICR_ICFGR1                     (GICR_ICFGR0 + 4)
#define GICR_IGRPMODR0                  GICD_IGRPMODR
#define GICR_NSACR                      GICD_NSACR

#define GICR_TYPER_PLPIS                (1U << 0)
#define GICR_TYPER_VLPIS                (1U << 1)
#define GICR_TYPER_LAST                 (1U << 4)
#define GICR_TYPER_AFFINITY_MASK        (0xffffffff00000000)
#define GICR_TYPER_AFFINITY_SHIFT       (32)
#define GICR_TYPER_PROC_NUM(n)          ((((n) & 0x00FFFF00ULL) >> 8))
#define GICR_CTLR_UWP                   (1U << 31)
#define GICR_CTLR_LPI_EN                (1U)


/* GICR bits */

#define GICR_PROPBASER_IC(mode)         (((mode) & 0x7ULL) << 7)
#define GICR_PROPBASER_OC(mode)         (((mode) & 0x7ULL) << 56)

#define GICR_BASER_SH_IS                (0x01ULL << 10)

#define GICR_PROPBASER_SH_IS            GICR_BASER_SH_IS
#define GICR_PROPBASER_DEVICE           (GICR_PROPBASER_IC(0x0ULL) | \
                                        GICR_PROPBASER_OC(0x0ULL))
#define GICR_PROPBASER_NC               (GICR_PROPBASER_IC(0x1ULL) | \
                                        GICR_PROPBASER_OC(0x1ULL))
#define GICR_PROPBASER_IDBITS           (0x0FULL)       /* IDBits 16 */

#define GICR_PROPBASER_PHY(phy)         (phy & 0x000FFFFFFFFFF000ULL)

#define GICR_PENDBASER_PTZ              (0x1ULL << 62)
#define GICR_PENDBASER_SH_IS            GICR_PROPBASER_SH_IS
#define GICR_PENDBASER_DEVICE           GICR_PROPBASER_DEVICE
#define GICR_PENDBASER_NC               GICR_PROPBASER_NC

#define GICR_PENDBASER_PHY(phy)         (phy & 0x000FFFFFFFFF0000ULL)

/* CPU interface registers */

#define GIC_INT_SPURIOUS                0x3FF
#define GIC_INT_PRIORITY_MASK           0xFF

#define MPIDR_EL1_AFFINITY_MASK         0xff00ffffffULL
#define MPIDR_AFFINITY_MASK             0x00ffffff


/* target list width*/

#define TARGET_LIST_WIDTH               (15)


/* MPCore GIC interrupt levels */

#define INT_LVL_MPCORE_IPI00         0 /* This is vxWorks CPC IPI */
#define INT_LVL_MPCORE_CPC           INT_LVL_MPCORE_IPI00

#define INT_LVL_MPCORE_IPI01         1 /* This is vxWorks Debug IPI */
#define INT_LVL_MPCORE_DEBUG         INT_LVL_MPCORE_IPI01

#define INT_LVL_MPCORE_IPI02         2 /* This is vxWorks Reschedule IPI */
#define INT_LVL_MPCORE_SCHED         INT_LVL_MPCORE_IPI02

#define INT_LVL_MPCORE_IPI03         3 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI04         4 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI05         5 /* IPI -> use SWI register in GIC */

#define INT_LVL_MPCORE_IPI06         6 /* This is vxWorks Core Reset IPI */
#define INT_LVL_MPCORE_RESET         INT_LVL_MPCORE_IPI06

#define INT_LVL_MPCORE_IPI07         7 /* This is vxWorks Core Start IPI */
#define INT_LVL_MPCORE_START         INT_LVL_MPCORE_IPI07

#define INT_LVL_MPCORE_IPI08         8 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI09         9 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI10        10 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI11        11 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI12        12 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI13        13 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI14        14 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI15        15 /* IPI -> use SWI register in GIC */


#define GIC_INT_ALL_ENABLED     0xFF  /* priority 0-0xFF can run */
#define GIC_INT_ALL_DISABLED    0x00  /* nothing higher than 0 hence disabled */
#define GIC_INT_SPURIOUS        0x3FF /* no interrupt currently */
#define GIC_INT_ONEMINUS_HIGH   0x55555555 /* interrupt config to 1-N, High */
#define GIC_CPU_BINP_DEFAULT    0x07  /* split all priority to subpriority */
#define GIC_CPU_BINP_PREEMPT_VAL    0x00  /* split all priority to group-priority */
#define GIC_CPU_BINP_NONPREEMPT_VAL 0x07  /* split all priority to sub-priority */
#define GIC_CPU_CONTROL_ENABLE  0x1   /* enable the processor interface */
#define GIC_CPU_ALL_ENABLED     0xFF  /* priority 0-E can run */
#define GIC_CPU_ALL_DISABLED    0x00  /* nothing higher than 0 */
#define GIC_SGI_SRC_CPU_ID_MASK 0x1C00
#define GIC_INT_HIGHEST_PRIORITY 0x0  /* the highest priority for interrupts */
#define GIC_INT_LOWEST_PRIORITY  0x1F /* the lowest priority for interrupts */
#define GIC_INT_PRIORITY_SHIFT   0x8
#define GIC_INT_PRIORITY_MASK    0xFF
#define GIC_INT_TRIGGER_SHIFT    0x2

#define GIC_CPU_DIR_DEFAULT 0x01010101 /* all interrupts are directed to CPU0 */
#define GIC_V3_CPU_DIR_DEFAULT      0x0llu /* all interrupts are directed to CPU0 */
#define GIC_CONTROL_ENABLE  0x01

#define ALL_PPI_INT_MASK  0xFFFF0000 /* bit field for all PPI interrupts */
#define ALL_SGI_INT_MASK  0x0000FFFF /* bit field for all SGI interrupts */

#define GIC_AFF_ALL_MASK            0xff00ffffff
#define GIC_AFF_LOW32_MASK          0x00ffffff
/* ARM GIC interrupt distributor and CPU interface register access macros */

#define SHIFT_PER_WORD              5
#define BITS_PER_WORD           32
#define CONFIGS_PER_WORD        16
#define PRIOS_PER_WORD          4
#define TARGETS_PER_WORD        4
#define NWORD(bitnum)           (bitnum / 32)
#define BIT32(bitnum)           (1 << (bitnum % BITS_PER_WORD)) /* shift max (32-1)bits */


/* SGI & PPI  configuration  for GIC 400 */

#define GIC_SGI_OFF     (1 << 16)


/* SGI Configuration Register */

#define IMR              (1 << (40 - 32))
#define ipiId_MASK       (0xffff)

#define GIC_SZ_64K                      (0x10000)


#ifndef _ASMLANGUAGE

#define GIC_DIST_HDL(pCtrl)         (pCtrl)->gicDistHdl
#define GIC_DIST_BAR(pCtrl)         (pCtrl)->gicDist
#define GIC_RDIST_HDL(pRdst)        (pRdst)->rdHdl
#define GIC_RDIST_BAR(pRdst)        (pRdst)->rdBase
#define GIC_RDSGI_HDL(pRdst)        (pRdst)->rdHdl
#define GIC_RDSGI_BAR(pRdst)        (pRdst)->sgiBase
#define GIC_RDRGN_HDL(pRdRgn)       (pRdRgn)->rdRgnHdl
#define GIC_RDRGN_BAR(pRdRgn)       (pRdRgn)->rdRgnBase

#define DIST_READ_4(pCtrl, addr)    \
    SWAP32 (vxbRead32 (GIC_DIST_HDL(pCtrl), \
    (UINT32 *)((char *)GIC_DIST_BAR(pCtrl) + (addr))))

#define DIST_WRITE_4(pCtrl, addr, data) \
    vxbWrite32 (GIC_DIST_HDL(pCtrl),    \
        (UINT32 *)((char *)GIC_DIST_BAR(pCtrl) + (addr)), SWAP32(data))
        
#define DIST_READ_8(pCtrl, addr)    \
    SWAP64 (vxbRead64 (GIC_DIST_HDL(pCtrl),  \
    (UINT64 *)((char *)GIC_DIST_BAR(pCtrl) + (addr))))

#define DIST_WRITE_8(pCtrl, addr, data) \
    vxbWrite64 (GIC_DIST_HDL(pCtrl),    \
        (UINT64 *)((char *)GIC_DIST_BAR(pCtrl) + (addr)), SWAP64(data))

#define RDIST_READ_4(pCtrl, addr)   \
    SWAP32 (vxbRead32 (GIC_RDIST_HDL(pCtrl),    \
    (UINT32 *)((char *)GIC_RDIST_BAR(pCtrl) + (addr))))

#define RDIST_WRITE_4(pCtrl, addr, data)    \
    vxbWrite32 (GIC_RDIST_HDL(pCtrl),   \
        (UINT32 *)((char *)GIC_RDIST_BAR(pCtrl) + (addr)), SWAP32(data))

#define RDIST_READ_8(pCtrl, addr)   \
    SWAP64 (vxbRead64 (GIC_RDIST_HDL(pCtrl), \
    (UINT64 *)((char *)GIC_RDIST_BAR(pCtrl) + (addr))))

#define RDIST_WRITE_8(pCtrl, addr, data)    \
    vxbWrite64 (GIC_RDIST_HDL(pCtrl),   \
        (UINT64 *)((char *)GIC_RDIST_BAR(pCtrl) + (addr)), SWAP64(data))

#define RDRGN_READ_4(pCtrl, addr)   \
    SWAP32 (vxbRead32 (GIC_RDRGN_HDL(pCtrl),    \
    (UINT32 *)((char *)GIC_RDRGN_BAR(pCtrl) + (addr))))

#define RDRGN_WRITE_4(pCtrl, addr, data)    \
    vxbWrite32 (GIC_RDRGN_HDL(pCtrl),   \
        (UINT32 *)((char *)GIC_RDRGN_BAR(pCtrl) + (addr)), SWAP32(data))

#define RDRGN_READ_8(pCtrl, addr)   \
    SWAP64 (vxbRead64 (GIC_RDRGN_HDL(pCtrl), \
    (UINT64 *)((char *)GIC_RDRGN_BAR(pCtrl) + (addr))))

#define RDRGN_WRITE_8(pCtrl, addr, data)    \
    vxbWrite64 (GIC_RDRGN_HDL(pCtrl),   \
        (UINT64 *)((char *)GIC_RDRGN_BAR(pCtrl) + (addr)), SWAP64(data))
        
#define RDSGI_READ_4(pCtrl, addr)   \
    SWAP32 (vxbRead32 (GIC_RDSGI_HDL(pCtrl),    \
    (UINT32 *)((char *)GIC_RDSGI_BAR(pCtrl) + (addr))))

#define RDSGI_WRITE_4(pCtrl, addr, data)    \
    vxbWrite32 (GIC_RDSGI_HDL(pCtrl),   \
        (UINT32 *)((char *)GIC_RDSGI_BAR(pCtrl) + (addr)), SWAP32(data))

#define RDSGI_READ_8(pCtrl, addr)   \
    SWAP64 (vxbRead64 (GIC_RDSGI_HDL(pCtrl), \
    (UINT64 *)((char *)GIC_RDSGI_BAR(pCtrl) + (addr))))

#define RDSGI_WRITE_8(pCtrl, addr, data)    \
    vxbWrite64 (GIC_RDSGI_HDL(pCtrl),   \
        (UINT64 *)((char *)GIC_RDSGI_BAR(pCtrl) + (addr)), SWAP64(data))

#define DIST_SYNC(pCtrl)    \
    while ((DIST_READ_4 (pCtrl, GICD_CTLR) & GICD_CTLR_RWP)== \
            GICD_CTLR_RWP)
        
#define RDIST_SYNC(pRdst)    \
    while ((RDIST_READ_4 (pRdst, GICR_CTLR) & GICR_CTLR_UWP) == \
            GICR_CTLR_UWP)


/* structure holding Generic Interrupt Controller in details */

typedef struct gicRedistRegion
    {
    UINT32       rdRgnBase;
    UINT32       rdRgnBasePhy;
    void *          rdRgnHdl;
    } GIC_REDIST_REGION;

typedef struct gicRedist
    {
    UINT32   rdBase;
    UINT32   sgiBase;
    void *      rdHdl;
    UINT32      processorNum;   /* the unique identifier for the PE */
    } GIC_REDIST;

    
#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbArmGenIntCtlrV3h */
