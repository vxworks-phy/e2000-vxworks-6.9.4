/* ftPinMux.c - pin mux/demux hardware driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <vxWorks.h>
#include <stdio.h>
#include <string.h>
#include "ftPinMux.h"


UINT32 padRead (UINT32 reg)
{
  return readl(reg+PINMUX_BASE_ADRS);
}
    
void padWrite (UINT32 reg, UINT32 value)
{
  writel(value, reg+PINMUX_BASE_ADRS);
}


void pinSetDelay(UINT32 reg, FPinDelayDir dir, FPinDelayType type, FPinDelay delay)
{

    UINT32 reg_val = padRead(reg);

    if (FPIN_OUTPUT_DELAY == dir)
    {
        if (FPIN_ROARSE_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_OUT_DELAY_ROARSE_MASK;
            reg_val |= FIOPAD_X_REG1_OUT_DELAY_ROARSE_SET(delay);
        }
        else if (FPIN_FRAC_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_OUT_DELAY_FRAC_MASK;
            reg_val |= FIOPAD_X_REG1_OUT_DELAY_FRAC_SET(delay);
        }
        else
        {
            uartf("%s,%d bad type %d\r\n", __func__,__LINE__,type);            
        }
    }
    else if (FPIN_INPUT_DELAY == dir)
    {
        if (FPIN_ROARSE_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_IN_DELAY_ROARSE_MASK;
            reg_val |= FIOPAD_X_REG1_IN_DELAY_ROARSE_SET(delay);
        }
        else if (FPIN_FRAC_DELAY == type)
        {
            reg_val &= ~FIOPAD_X_REG1_IN_DELAY_FRAC_MASK;
            reg_val |= FIOPAD_X_REG1_IN_DELAY_FRAC_SET(delay);
        }
        else
        {
            uartf("%s,%d bad type %d\r\n", __func__,__LINE__,type);            
        }
    }
    else
    {
        uartf("%s,%d bad direct %d\r\n", __func__,__LINE__,dir);
    }
    
    padWrite(reg, reg_val);
    return;
}

void pinSetDelayEn(UINT32 reg, FPinDelayDir dir, BOOL enable)
{
    UINT32 reg_val = padRead(reg);

    if (FPIN_OUTPUT_DELAY == dir)
    {
        if (enable)
            reg_val |= FIOPAD_X_REG1_OUT_DELAY_EN;
        else
            reg_val &= ~FIOPAD_X_REG1_OUT_DELAY_EN;
    }
    else if (FPIN_INPUT_DELAY == dir)
    {
        if (enable)
            reg_val |= FIOPAD_X_REG1_IN_DELAY_EN;
        else
            reg_val &= ~FIOPAD_X_REG1_IN_DELAY_EN;
    }
    else
    {
        uartf("%s,%d bad direct %d\r\n", __func__,__LINE__,dir);
    }

    padWrite(reg, reg_val);
    return;    
}


void pinPWM0(void)
{
   /* pin AL59: func1 */
   
   padWrite(AL59_reg0, 1);
}

/* TESTA Board */
void pinBand3Mio3A(void)
{

}
void pinBand3Mio3B(void)
{

}

void pinBank4Mio8A(void)
{

}

void pinBank4Mio8B(void)
{

}

void pinBank2Mio8A(void)
{

}

void pinBank2Mio8B(void)
{

}

void pinMio14A(void)
{

}
void pinMio14B(void)
{

}

void pinMio15A(void)
{

}
void pinMio15B(void)
{

}

void pinSpim0Cs0(void)
{
	UINT32 val = padRead(U53_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(U53_reg0, val);
}
void pinSpim0Rxd(void)
{
	UINT32 val = padRead(U55_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(U55_reg0, val);
} 
void pinSpim0Txd(void)
{
	UINT32 val = padRead(W53_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(W53_reg0, val);
} 
void pinSpim0Sclk(void)
{
	UINT32 val = padRead(W55_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(W55_reg0, val);
}

void pinSpim1Cs0(void)
{
	UINT32 val = padRead(J29_reg0);
	val &= ~0x7;
	val |= 4;
	padWrite(J29_reg0, val);
}
void pinSpim1Rxd(void)
{
	UINT32 val = padRead(J31_reg0);
	val &= ~0x7;
	val |= 4;
	padWrite(J31_reg0, val);
} 
void pinSpim1Txd(void)
{
	UINT32 val = padRead(L31_reg0);
	val &= ~0x7;
	val |= 4;
	padWrite(L31_reg0, val);
} 
void pinSpim1Sclk(void)
{
	UINT32 val = padRead(N43_reg0);
	val &= ~0x7;
	val |= 4;
	padWrite(N43_reg0, val);
}
void pinSpim2Cs0(void)
{
	UINT32 val = padRead(A31_reg0);
	val &= ~0x7;
	val |= 0;
	padWrite(A31_reg0, val);
}

void pinSpim2Rxd(void)
{
	UINT32 val = padRead(C31_reg0);
	val &= ~0x7;
	val |= 0;
	padWrite(C31_reg0, val);
} 

void pinSpim2Txd(void)
{
	UINT32 val = padRead(C33_reg0);
	val &= ~0x7;
	val |= 0;
	padWrite(C33_reg0, val);
} 

void pinSpim2Sclk(void)
{
	UINT32 val = padRead(A33_reg0);
	val &= ~0x7;
	val |= 0;
	padWrite(A33_reg0, val);
}


void pinSpim3Cs0(void)
{
	UINT32 val = padRead(W51_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(W51_reg0, val);
}
void pinSpim3Rxd(void)
{
	UINT32 val = padRead(AE51_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(AE51_reg0, val);
} 
void pinSpim3Txd(void)
{
	UINT32 val = padRead(AC53_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(AC53_reg0, val);
} 
void pinSpim3Sclk(void)
{
	UINT32 val = padRead(AC55_reg0);
	val &= ~0x7;
	val |= 2;
	padWrite(AC55_reg0, val);
}

void pinGpio4p10(void)
{
	UINT32 reg = AE49_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio4p11(void)
{
	UINT32 reg = AC49_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio4p12(void)
{
	UINT32 reg = AE47_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio4p13(void)
{
	UINT32 reg = AA47_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}

void pinGpio5p9(void)
{
	UINT32 reg = N59_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}
void pinGpio5p10(void)
{
	UINT32 reg = C57_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}

void pinGpio5p11(void)
{
	UINT32 reg = E57_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}

void pinBank0Pwm00(void)
{
	UINT32 reg = AL59_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 1;
	
	padWrite(reg, val);
}

void pinBank0Pwm01(void)
{
	UINT32 reg = AJ57_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 1;
	
	padWrite(reg, val);
}


void pinBank0Pwm00_In(void)
{
	UINT32 reg = AN57_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 1;
	
	padWrite(reg, val);
}

void pinJ41Delay(void)
{
    /* RGMII need */
    pinSetDelay(FIOPAD_J41_DELAY, FPIN_OUTPUT_DELAY, FPIN_ROARSE_DELAY, FPIN_DELAY_5);
    pinSetDelay(FIOPAD_J41_DELAY, FPIN_OUTPUT_DELAY, FPIN_FRAC_DELAY, FPIN_DELAY_7);
    pinSetDelayEn(FIOPAD_J41_DELAY, FPIN_OUTPUT_DELAY, 1);
}

void pinAJ49Delay(FPinDelay largeScaleDelay, FPinDelay smallScaleDelay)
{
    /* SD0 CLK pin need */
    pinSetDelay(FIOPAD_AJ49_DELAY, FPIN_OUTPUT_DELAY, FPIN_ROARSE_DELAY, largeScaleDelay);
    pinSetDelay(FIOPAD_AJ49_DELAY, FPIN_OUTPUT_DELAY, FPIN_FRAC_DELAY, smallScaleDelay);
    pinSetDelayEn(FIOPAD_AJ49_DELAY, FPIN_OUTPUT_DELAY, 1);
}

void pinJ57Delay(FPinDelay largeScaleDelay, FPinDelay smallScaleDelay)
{
    /* SD1 CLK pin need */
    pinSetDelay(FIOPAD_J57_DELAY, FPIN_OUTPUT_DELAY, FPIN_ROARSE_DELAY, largeScaleDelay);
    pinSetDelay(FIOPAD_J57_DELAY, FPIN_OUTPUT_DELAY, FPIN_FRAC_DELAY, smallScaleDelay);
    pinSetDelayEn(FIOPAD_J57_DELAY, FPIN_OUTPUT_DELAY, 1);
}

void pinBank1Mio6a(void)
{
	UINT32 reg = AA57_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}

void pinBank1Mio6b(void)
{
	UINT32 reg = AA59_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}


void pinBank1Mio10a(void)
{
	UINT32 reg = C49_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 5;
	
	padWrite(reg, val);
}

void pinBank1Mio10b(void)
{
	UINT32 reg = A51_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 5;
	
	padWrite(reg, val);
}

void pinBank2Mio8a(void)
{
	UINT32 reg = AA49_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}

void pinBank2Mio8b(void)
{
	UINT32 reg = W49_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}

void pinBank3Mio11a(void)
{
	UINT32 reg = G59_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}

void pinBank3Mio11b(void)
{
	UINT32 reg = J59_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 4;
	
	padWrite(reg, val);
}

void pinBank5Mio15a(void)
{
	UINT32 reg = N53_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}

void pinBank5Mio15b(void)
{
	UINT32 reg = J53_reg0;
	UINT32 val = padRead(reg);
	val &= ~0x7;
	val |= 6;
	
	padWrite(reg, val);
}