/* vxbFtPwm.c - Driver for PWM controller */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <vsbConfig.h>
#include <intLib.h>
#include <errnoLib.h>
#include <errno.h>
#include <sioLib.h>
#include <ioLib.h>
#include <stdio.h>
#include <string.h>
#include <logLib.h>
#include <hwif/util/hwMemLib.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/vxbIntrCtlr.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/vxbus/hwConf.h>
#include "vxbFtPwm.h"
#include "defs.h"

#undef PWM_DBG_ON
#ifdef  PWM_DBG_ON
#define PWM_DBG_IRQ         0x00000001
#define PWM_DBG_RW          0x00000002
#define PWM_DBG_ERR         0x00000004
#define PWM_DBG_ALL         0xffffffff
#define PWM_DBG_OFF         0x00000000
LOCAL UINT32 pwmDbgMask = PWM_DBG_ALL;
IMPORT FUNCPTR _func_logMsg;

#define PWM_DBG(mask, string, a, b, c, d, e, f)         \
    if ((pwmDbgMask & mask) || (mask == PWM_DBG_ALL))   \
        if (_func_logMsg != NULL)                       \
            (* _func_logMsg)(string, a, b, c, d, e, f)
#else
#define PWM_DBG(mask, string, a, b, c, d, e, f)
#endif  /* PWM_DBG_ON */

LOCAL BOOL pwmSpinlockFuncReady = FALSE;

#define PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl)				\
    if (pwmSpinlockFuncReady)					\
	SPIN_LOCK_ISR_TAKE(&pDrvCtrl->spinlockIsr)
#define PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl)				\
    if (pwmSpinlockFuncReady)					\
	SPIN_LOCK_ISR_GIVE(&pDrvCtrl->spinlockIsr)

#define PWM_SPIN_LOCK_READY					\
    pwmSpinlockFuncReady = TRUE

LOCAL void vxbFtPwmInstInit(VXB_DEVICE_ID pInst);
LOCAL void vxbFtPwmInstInit2(VXB_DEVICE_ID pInst);
LOCAL void vxbFtPwmInstConnect(VXB_DEVICE_ID pInst);
LOCAL STATUS vxbFtPwmInfoGet (VXB_DEVICE_ID pDev,
                                  struct vxbPwmInfo ** ppPwmInfo);
LOCAL STATUS vxbFtPwmIoctl(VXB_DEVICE_ID pDev,  int  request,  _Vx_ioctl_arg_t  arg);

#ifdef INCLUDE_SHOW_ROUTINES
LOCAL void vxbPwmShow (VXB_DEVICE_ID pDev, int  verbose);
#endif

/* The best location for vxbPwmInfoGet: hwif/methods/vxbPwmInfoGet.c */

DEVMETHOD_DEF(vxbPwmInfoGet, "Get PWM Info");

LOCAL struct drvBusFuncs vxbFtPwmDrvFuncs =
    {
    vxbFtPwmInstInit,      /* devInstanceInit */
    vxbFtPwmInstInit2,     /* devInstanceInit2 */
    vxbFtPwmInstConnect    /* devConnect */
    };

LOCAL device_method_t vxbFtPwmDrv_methods[] =
    {
    DEVMETHOD (vxbPwmInfoGet,        vxbFtPwmInfoGet),

#ifdef INCLUDE_SHOW_ROUTINES
    DEVMETHOD (busDevShow,              vxbPwmShow),
#endif /* INCLUDE_SHOW_ROUTINES */

    DEVMETHOD_END
    };

/* phytium PWM VxBus registration info */

LOCAL struct vxbDevRegInfo vxbFtPwmDrvRegistration =
    {
    NULL,                           /* pNext */
    VXB_DEVID_DEVICE,               /* devID */
    VXB_BUSID_PLB,                  /* busID = PLB */
    VXB_VER_4_0_0,                  /* busVer */
    "ftPwm",                       /* drvName */
    &vxbFtPwmDrvFuncs,             /* pDrvBusFuncs */
    &vxbFtPwmDrv_methods[0],       /* pMethods */
    NULL                            /* devProbe */
    };

/******************************************************************************
*
* vxbFtPwmDrvRegister - register Phytium PWM driver
*
* This routine registers the Phytium PWM driver with the vxBus subsystem.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFtPwmDrvRegister (void)
    {
    /* call the vxBus routine to register the PWM driver */

    vxbDevRegister (&vxbFtPwmDrvRegistration);
    }

/*******************************************************************************
*
* vxbFtPwmInstInit - first level initialization routine of PWM modules
*
* This is the function called to perform the first level initialization of
* the Phytium PWM modules.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to OS facilities such as memory allocation
* and I/O.
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtPwmInstInit
    (
    VXB_DEVICE_ID pInst
    )
    {
      /* pinmux */
      if (0 == pInst->unitNumber)
        {
        pinBank0Pwm00();
        pinBank0Pwm01();
        }

      if (6 == pInst->unitNumber)
        {
        padWrite(0x00f4U,2);/*channel-0*/
        padWrite(0x01d0U,3);/*channel-1*/
        
        }
      
    }
    
/*******************************************************************************
*
* vxbFtPwmInstInit2 - second level initialization routine of GPIO modules
*
* This routine performs the second level initialization of the GPIO modules.
*
* This routine is called later during system initialization. OS features
* such as memory allocation are available at this time.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtPwmInstInit2
    (
    VXB_DEVICE_ID pInst
    )
    {
    FT_PWM_DRVCTRL * pDrvCtrl;
    HCF_DEVICE * pHcf;
    int j, k;
    UINT32 regVal;
    
    pDrvCtrl = (FT_PWM_DRVCTRL *)hwMemAlloc(sizeof (FT_PWM_DRVCTRL)); 
    
    if (pDrvCtrl == NULL)
        {
        PWM_DBG(PWM_DBG_ERR, "vxbFtGpioInstInit: pDrvCtrl alloc failed\r\n.",1,2,3,4,5,6);
        return;
        }
    bzero((char*)pDrvCtrl, sizeof(FT_PWM_DRVCTRL));
        
    pInst->pDrvCtrl = pDrvCtrl;
    pDrvCtrl->pInst = pInst;

	snprintf(pDrvCtrl->pwmInfo.pwmName,6,"PWM-%d",pInst->unitNumber);
	pDrvCtrl->pwmInfo.pwmIoctl = vxbFtPwmIoctl;

    /* map register base */
    pInst->regBaseFlags[0] = VXB_REG_IO;
    if (vxbRegMap(pInst, 0, &pDrvCtrl->pwmHandle) == ERROR)
        {
        PWM_DBG (PWM_DBG_ERR, "%s: vxbRegMap  ERROR\r\n", __func__, 2, 3, 4, 5, 6);
        return;
        }
    
    pDrvCtrl->dbBase = pInst->pRegBase[0];
    pDrvCtrl->pwmBase0 = pDrvCtrl->dbBase + 0x400;
    pDrvCtrl->pwmBase1 = pDrvCtrl->dbBase + 0x800;
    pDrvCtrl->dbpolarity = DB_POL_AH;
    pDrvCtrl->updbclyNs = 0;
    pDrvCtrl->dwdbclyNs = 0;
    

    pHcf = (struct hcfDevice *) hcfDeviceGet (pInst);
    if (pHcf == NULL)
        return;
    
    if (devResourceGet (pHcf, "clkFreq", HCF_RES_INT, &(pDrvCtrl->baseClk)) != OK)
        {
        pDrvCtrl->baseClk =  CLK_50MHZ; 
        }

        
     for (j=0; j<PWM_CHANNEL_MAX; j++)
        {
            pDrvCtrl->chParam[j].cntmod = MODULO;
            
            pDrvCtrl->chParam[j].div = 0; /*value range[0~4095] => div[1~4096]*/

#ifdef PWM_SUPPORT_FIFO_MODE
             pDrvCtrl->chParam[j].dutymod = DUTY_FROM_FIFO;
             pDrvCtrl->chParam[j].dutyIndex = 0;
             for (k=0; k < FIFO_DEPTH; k++)
             {
                 pDrvCtrl->chParam[j].dutyCCR[k]=0;
             }
             
#else
             pDrvCtrl->chParam[j].dutymod = DUTY_FROM_REGISTER;
#endif
        }

#if defined(PWM_SUPPORT_INTERRUPT) || defined(PWM_SUPPORT_FIFO_MODE)
        st = vxbIntConnect(pInst,0, ftPwmISR, pDrvCtrl);
        if (OK != st)
        {
            PWM_DBG (PWM_DBG_ERR,"Failed to vxbIntConnect for %s%d!\r\n", 
            pInst->pName, pInst->unitNumber,3,4,5,6);
        }
#endif

    SPIN_LOCK_ISR_INIT (&pDrvCtrl->spinlockIsr, 0);
    PWM_SPIN_LOCK_READY;

    /* lsd_mio_pwm_syn[7:0]: PWM instance [7...0] global enable. */
    regVal = readl(0x2807E020);
    regVal |= (1<< pInst->unitNumber);
    writel(regVal, 0x2807E020); 
    
    return;
    }

/*******************************************************************************
*
* vxbFtPwmInstConnect - third level initialization routine of GPIO modules
*
* This is the function called to perform the third level initialization of
* the GPIO modules.
*
* RETURNS: N/A
*
* ERRNO:  N/A
*
* \NOMANUAL
*
*/

LOCAL void vxbFtPwmInstConnect
    (
    VXB_DEVICE_ID pInst
    )
    {
    /* nothing is done here */
	/* Do not call pwmDrv() here. */
    }


/*****************************************************************************
*
* vxbFtPwmEnable - Enable a PWM channel
*
* This routine enable PWM.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
LOCAL STATUS vxbFtPwmEnable(VXB_DEVICE_ID pDev, UINT32 nChan)
{    
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    reg |= BIT(1);
    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmDisable - Disable a PWM channel
*
* This routine disable PWM.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
LOCAL STATUS vxbFtPwmDisable(VXB_DEVICE_ID pDev, UINT32 nChan)
{    
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    reg &= ~ BIT(1);
    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}


/*****************************************************************************
*
* vxbFtPwmDutymodSet - Set duty mode
*
* This routine sets duty mode: register mode or FIFO mode.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDutymodSet(VXB_DEVICE_ID pDev, UINT32 nChan, DUTY_SOURCE_MODE dutymod)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_PWMCTRL);

    if (dutymod == DUTY_FROM_REGISTER) /* duty from reg PWM_CCR */
    {
        reg &= ~ BIT(8);
    }
    else if (dutymod == DUTY_FROM_FIFO)
    {
        reg |= BIT(8); /* from FIFO */
    }
    else
    {
        PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
        uartf("Bad duty source mode: %d \r\n", dutymod);
        return ERROR;
    }

    CH_WRITE_4(pDrvCtrl, nChan, REG_PWMCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}


/*****************************************************************************
*
* vxbFtPwmReset - Reset a PWM channel
*
* This routine reset PWM.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmReset(VXB_DEVICE_ID pDev, UINT32 nChan)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    reg |= BIT(0);
    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);

    while ((CH_READ_4(pDrvCtrl, nChan, REG_TCTRL) & BIT(0)) != 0x0)
    {
    };
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);

    return OK;
}

/*****************************************************************************
*
* vxbFtPwmDbReset - Reset Dead Band
*
* This routine reset dead band.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDbReset(VXB_DEVICE_ID pDev)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = DB_READ_4(pDrvCtrl, REG_DBCTRL);
    reg |= BIT(0);
    DB_WRITE_4(pDrvCtrl, REG_DBCTRL, reg);
    while ((DB_READ_4(pDrvCtrl, REG_DBCTRL) & BIT(0)) != 0x0)
    {
    };
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);

    return OK;
}

/*****************************************************************************
*
* vxbFtPwmDivCalc - Calculate clock divisor
*
* This routine calculates a suitable clock divisor by time value in nano second.
*
* RETURNS: [1...4096], or 0 for ERROR
*
* ERRNO: N/A
*/
UINT32 vxbFtPwmDivCalc(UINT64 timeNs)
{
    UINT32 timeUnit;
    UINT32 div;

    timeUnit = NSEC_PER_SEC/CLK_50MHZ; /* 20ns */

    div = (UINT32)(timeNs/(timeUnit*0xFFFFULL))+1;

    if (div > PWM_DIV_MAX)
    {
        uartf("divisor %d is invalid for this PWM!\r\n", div);
        return 0;
    }

    return div; /* from PWM_DIV_MIN to PWM_DIV_MAX */
}

/*****************************************************************************
*
* vxbFtPwmDivSet - Set clock divisor
*
* This routine sets divisor to register. div values:[0...4095]
* Suggest to use it when PWM is disabled. 
* It is also OK when enabled.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDivSet(VXB_DEVICE_ID pDev, UINT32 nChan, UINT32 div)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    reg &= 0x0000ffff;
    reg |= (div<<16);
    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmTModeSet - Set timer mode
*
* This routine sets timer count mode: modulo or up-and-down.
* NOTE: Switch TMode must be in PWM disabled!
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
/* 
timer count mode: 
*/
STATUS vxbFtPwmTModeSet(VXB_DEVICE_ID pDev, UINT32 nChan, TIMER_COUNT_MODE tmode) 
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    if (tmode == MODULO)
    {
        reg &= ~ BIT(2);
    }
    else if (tmode == UP_AND_DOWN) /* up-and-down */
    {
        reg |= BIT(2);
    }
    else
    {
        PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
        uartf("Bad timer count mode: %d\r\n", tmode);
        return ERROR;
    }

    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmPeriodNsSet - Set PWM period in nano second.
*
* This routine sets PWM period.
* Supported to change period when PWM is running.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmPeriodNsSet(VXB_DEVICE_ID pDev, UINT32 nChan, UINT32 periodNs)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;
    UINT32 div;
    UINT64 cycles;

    pDrvCtrl = pDev->pDrvCtrl;
    div = pDrvCtrl->chParam[nChan].div;
    
    cycles = pDrvCtrl->baseClk;
    cycles *= (UINT64)(periodNs / (div + 1));
    cycles /=  NSEC_PER_SEC;
    cycles = (cycles & PWM_PERIOD_MASK) - 0x1; /* workaround 1 cyc */

    if ((cycles>>16) != 0)
    {
        uartf("period cycles only 16bit!\r\n");
    }

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TPERIOD);
    reg = (reg & 0xFFFF0000) | (UINT16)cycles;

    CH_WRITE_4(pDrvCtrl, nChan, REG_TPERIOD, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmDutySet - Set PWM duty value
*
* This routine sets PWM duty value in nano second.
* Supported to change duty when PWM is running.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDutySet(VXB_DEVICE_ID pDev, UINT32 nChan, UINT32 duty)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;
    UINT32 div;
    UINT64 cycles;
#ifdef PWM_SUPPORT_FIFO_MODE
    int ix;
#endif

    pDrvCtrl = pDev->pDrvCtrl;
    div = pDrvCtrl->chParam[nChan].div;
    
    cycles = pDrvCtrl->baseClk;
    cycles *= (UINT64)(duty / (div + 1));
    cycles /= NSEC_PER_SEC;
    cycles = (cycles & PWM_DUTY_MASK) - 0x1; /* workaround 1 cyc */

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_PWMCCR);
    reg &= 0xFFFF0000;
    reg |= (UINT16)(cycles & PWM_DUTY_MASK);
    CH_WRITE_4(pDrvCtrl, nChan, REG_PWMCCR, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);

#ifdef PWM_SUPPORT_FIFO_MODE
    ix = pDrvCtrl->chParam[nChan].dutyIndex;
    if (ix < FIFO_DEPTH)
    { 
        pDrvCtrl->chParam[nChan].dutyCCR[ix] = reg;
        pDrvCtrl->chParam[nChan].dutyIndex = ix+1;
    }
#endif
    return OK;
}

#ifdef PWM_SUPPORT_FIFO_MODE
/*****************************************************************************
*
* vxbFtPwmDutyFifoSet - Set PWM duty value by FIFO Emptry interrupt
*
* This routine sets PWM duty values.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
void vxbFtPwmDutyFifoSet(VXB_DEVICE_ID pDev, UINT32 nChan)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;
    UINT32 ccr;
    int ix;

    pDrvCtrl = pDev->pDrvCtrl;

    for (ix=0; ix<FIFO_DEPTH; ix++)
    {
        reg = CH_READ_4(pDrvCtrl, nChan, REG_STAT);
        if ((reg & BIT(3)) != BIT(3)) /* bit3: FIFO_FULL */
        {
            ccr = pDrvCtrl->chParam[nChan].dutyCCR[ix];
            CH_WRITE_4(pDrvCtrl, nChan, REG_PWMCCR, ccr);
        }
    }
}
#endif
/*****************************************************************************
*
* vxbFtPwmDbClyNsSet - Set DB area
*
* This routine sets Dead Band up/down signal in nano second.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDbClyNsSet(VXB_DEVICE_ID pDev, UINT32 nChan, UINT32 updbclyNs, UINT32 dwdbclyNs)
{
	FT_PWM_DRVCTRL * pDrvCtrl;
	UINT32 reg;
	UINT64 dbcly, cycles, upcycles, dwcycles;

    pDrvCtrl = pDev->pDrvCtrl;
	cycles = pDrvCtrl->baseClk;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
	reg = CH_READ_4(pDrvCtrl, nChan, REG_TPERIOD);
	PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
	
	dbcly = 0x0;
	if (updbclyNs)
	{
		upcycles = cycles * updbclyNs;
		upcycles /= NSEC_PER_SEC;

		if (upcycles < reg)
		{
			dbcly |= (upcycles & PWM_UPDBCLY_MASK);
		}
		else
		{
			return ERROR;
		}
	}
	
	if (dwdbclyNs)
	{
		dwcycles = cycles * dwdbclyNs;
		dwcycles /= NSEC_PER_SEC;

		if (dwcycles < reg)
		{
			dbcly |= ((dwcycles<<10) & PWM_DWDBCLY_MASK);
		}
		else
		{
			return ERROR;
		}
	}
	
	PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
	DB_WRITE_4(pDrvCtrl, REG_DBCLY, (UINT32)dbcly);
	reg = DB_READ_4(pDrvCtrl, REG_DBCTRL);
	reg &= ~0x30; /*both disabled*/

    if ((0 != updbclyNs) && (0 != dwdbclyNs))
        {
        reg |= 0x30; /*both used*/
        }
    else if ((0 == updbclyNs) && (0 != dwdbclyNs))
        {
        reg |= 0x10;
        }
    else if ((0 != updbclyNs) && (0 == dwdbclyNs))
        {
        reg |= 0x20;
        }
    else
        {
        /*has been disabled*/
        }
    
	DB_WRITE_4(pDrvCtrl, REG_DBCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    
	return OK;
}

/*****************************************************************************
*
* vxbFtPwmDbPolaritySet - Set DB polarity
*
* This routine sets Dead Band polarity.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDbPolaritySet(VXB_DEVICE_ID pDev, DB_POLARITY dbPolarity)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = DB_READ_4(pDrvCtrl, REG_DBCTRL);
    reg &= 0x33;
    reg |= ((dbPolarity<<2) & PWM_DB_POLARITY_MASK);
    DB_WRITE_4(pDrvCtrl, REG_DBCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmHwInit - PWM hardware parameter init
*
* This routine sets default parameters to hardware register.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmHwInit(VXB_DEVICE_ID pDev, UINT32 nChan)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    pDrvCtrl = pDev->pDrvCtrl;

    CH_WRITE_4(pDrvCtrl, nChan, REG_PWMCTRL, PWM_CTRL_INIT);

    /* set div, dutymode, tmode for pwm */
    vxbFtPwmDivSet(pDev, nChan, pDrvCtrl->chParam[nChan].div);
    vxbFtPwmDutymodSet(pDev, nChan, pDrvCtrl->chParam[nChan].dutymod);
    vxbFtPwmTModeSet(pDev, nChan, pDrvCtrl->chParam[nChan].cntmod);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmDbInit - DB init
*
* This routine initializes Dead Band.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmDbInit(VXB_DEVICE_ID pDev)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    
    pDrvCtrl = pDev->pDrvCtrl;
    vxbFtPwmDbPolaritySet(pDev, pDrvCtrl->dbpolarity);
    return OK;
}

/*****************************************************************************
*
* vxbFtPwmPolaritySet - Set PWM polarity
*
* This routine sets PWM polarity: normal or inversed.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
STATUS vxbFtPwmPolaritySet(VXB_DEVICE_ID pDev, UINT32 nChan, PWM_POLARITY polarity)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_PWMCTRL);

    if (polarity == PWM_POLARITY_INVERSED)
    {
        reg &= 0xffffff0f;
        reg |= 0x30;
    }
    else if (polarity == PWM_POLARITY_NORMAL)
    {
        reg &= 0xffffff0f;
        reg |= 0x40;
    }
    else
    {
        PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
        uartf("Bad polarity mode: %d\r\n", polarity);
        return ERROR;
    }

    CH_WRITE_4(pDrvCtrl, nChan, REG_PWMCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}

#if defined(PWM_SUPPORT_INTERRUPT) || defined(PWM_SUPPORT_FIFO_MODE)
STATUS vxbFtPwmInterruptEnable(VXB_DEVICE_ID pDev, UINT32 nChan)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;
    STATUS st;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    reg |= BIT(4)|BIT(5); /* counter-overflow intr | global intr */
    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);

    reg = CH_READ_4(pDrvCtrl, nChan, REG_PWMCTRL);
    reg |= BIT(3);  /* compare interrupt */

# if defined(PWM_SUPPORT_FIFO_MODE)
    reg |= BIT(9);  /* FIFO Empty interrupt */
# endif

    CH_WRITE_4(pDrvCtrl, nChan, REG_PWMCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);

    st = vxbIntEnable(pDev,0,ftPwmISR,pDrvCtrl);
    if (OK != st)
    {
        uartf("Failed intEnalbe for PWM%d!\r\n", pDev->unitNumber);
    }

    return OK;
}

STATUS ftPwmInterruptDisable(VXB_DEVICE_ID pDev, UINT32 nChan)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    UINT32 reg;

    pDrvCtrl = pDev->pDrvCtrl;

    PWM_SPIN_LOCK_ISR_TAKE(pDrvCtrl);
    reg = CH_READ_4(pDrvCtrl, nChan, REG_TCTRL);
    reg &= ~(BIT(4)|BIT(5)); /* counter-overflow intr | global intr */
    CH_WRITE_4(pDrvCtrl, nChan, REG_TCTRL, reg);
    PWM_SPIN_LOCK_ISR_GIVE(pDrvCtrl);
    return OK;
}


void ftPwmISR(FT_PWM_DRVCTRL * pDrvCtrl)
{

    UINT32 nChan;
    UINT32 status;


    for(nChan = 0; nChan < PWM_CHANNEL_MAX; nChan++)
    {

        status = CH_READ_4(pDrvCtrl, nChan, REG_PWMCTRL);
        if(!(status & (BIT(3) | BIT(9))))
            continue;

        status = CH_READ_4(pDrvCtrl, nChan, REG_STAT);
        if (0 == status)
            continue;
        
        /* Check for the type of error interrupt and Processing it */
        if (status & BIT(1))
        {
            status &= (~ BIT(1));
            status |= BIT(0);
            CH_WRITE_4(pDrvCtrl, nChan, REG_STAT, status);
            /*now nothing*/
 
        }

        if (status & BIT(2)) /* FIFO_EMPTY */
        {
            status |= BIT(2);
            CH_WRITE_4(pDrvCtrl, nChan, REG_STAT, status);
            vxbFtPwmDutyFifoSet(pDrvCtrl->pInst, nChan);
        }
    } 
}
#endif /* PWM_SUPPORT_INTERRUPT || PWM_SUPPORT_FIFO_MODE */

/*****************************************************************************
*
* ftPwmConfig - Configurate a PWM channel
*
* This routine configurates PWM parameters set by application.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/
STATUS ftPwmConfig(PWM_USER_CONFIG * pConf)
{
    VXB_DEVICE_ID pDev = NULL;
    FT_PWM_DRVCTRL * pDrvCtrl = NULL;
#ifdef PWM_SUPPORT_FIFO_MODE
    UINT32 reg;
#endif
    UINT32 nChan;
    UINT32 periodNs;
    UINT32 dutyNs;
    UINT32 updbclyNs;
    UINT32 dwdbclyNs;
    UINT32 div;
    /*PWM_POLARITY polarity = PWM_POLARITY_NORMAL;*/
    BOOL   multiDuty = FALSE;
#ifdef PWM_SUPPORT_FIFO_MODE
    int i;
#endif

    if (NULL == pConf)
    {
        return ERROR;
    }

    nChan = pConf->nChan;
    periodNs = pConf->periodNs;
    dutyNs = pConf->dutyNs;
    updbclyNs = pConf->updbclyNs;
    dwdbclyNs = pConf->dwdbclyNs;
    /*polarity = pConf->polarity;*/
    /*multiDuty = pConf->multiDuty;*/

    pDev = vxbInstByNameFind("ftPwm", pConf->nPwm);
	if (NULL == pDev)
    {
        return ERROR;
    }
	pDrvCtrl = pDev->pDrvCtrl;

    vxbFtPwmDisable(pDev, nChan);
    
    div = vxbFtPwmDivCalc(dutyNs);
    if (div == 0)
    {
        return ERROR;
    }

    pDrvCtrl->chParam[nChan].div = (div-1); /*save div*/
    pDrvCtrl->updbclyNs = updbclyNs; /*save updbcly*/
    pDrvCtrl->dwdbclyNs = dwdbclyNs; /*save dwdbcly*/

    if ((updbclyNs != 0) || (dwdbclyNs != 0))
    {
        vxbFtPwmDbReset(pDev);
        vxbFtPwmDbInit(pDev);
        vxbFtPwmDbClyNsSet(pDev, 0, updbclyNs, dwdbclyNs);
    }

    vxbFtPwmHwInit(pDev, nChan);
    vxbFtPwmDivSet(pDev, nChan, (div-1));
    vxbFtPwmPolaritySet(pDev, nChan, PWM_POLARITY_NORMAL); /* default polar */
    vxbFtPwmPeriodNsSet(pDev, nChan, (periodNs/div));

    if (multiDuty == FALSE)
    {
        pDrvCtrl->chParam[nChan].dutymod = DUTY_FROM_REGISTER;
        vxbFtPwmDutySet(pDev, nChan, (dutyNs/div));
    }
    else
    {
#ifdef PWM_SUPPORT_FIFO_MODE
        pDrvCtrl->chParam[(nChan)].dutymod = DUTY_FROM_FIFO;

        for (i=0; i<FIFO_DEPTH; i++)
        {
            dutyNs = pConf->dutiesNs[i];
            reg = CH_READ_4(pDrvCtrl, nChan, REG_STAT);
            if ((reg & BIT(3)) != BIT(3)) /* bit3: FIFO_FULL */
            {
                vxbFtPwmDutySet(pDev, nChan, (dutyNs/div));
            }
        }
#else
        printf("error: PWM_SUPPORT_FIFO_MODE not defined! \r\n");
        return ERROR;
#endif
    }

#if defined(PWM_SUPPORT_INTERRUPT) || defined(PWM_SUPPORT_INTERRUPT)
    vxbFtPwmInterruptEnable(pDev, nChan);
#endif


    return OK;
}


/*****************************************************************************
*
* vxbFtPwmIoctl - io control
*
* This routine controls PWM logically.
*
* RETURNS: OK, ERROR if invalid parameters
*
* ERRNO: N/A
*/
LOCAL STATUS vxbFtPwmIoctl(VXB_DEVICE_ID pDev,  int  request,  _Vx_ioctl_arg_t  arg)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
	PWM_CMD * pCmd;
	STATUS retVal;
    
    pDrvCtrl = pDev->pDrvCtrl;
	
    /*uartf("%s,%d Enter...\r\n", __FUNCTION__, __LINE__);*/

    switch (request)
    {
    /* Return file name in arg */

    case FIOGETNAME:
        strncpy ((char *) arg, pDrvCtrl->pwmInfo.pwmName, MAX_DRV_NAME_LEN);
        retVal = OK;
        break;

    /* Set file options */

    case FIOSETOPTIONS:
		retVal = ftPwmConfig((PWM_USER_CONFIG *) arg);
        break;

    /* control this device */

    case FIOFCNTL:
        pCmd = (PWM_CMD *)arg;

		switch (pCmd->cmd)
		{
		case PWM_CMD_START:
			
		    PWM_DBG(PWM_DBG_ALL, "%s,%d PWM_CMD_START...\r\n", __FUNCTION__, __LINE__,3,4,5,6);
            retVal = vxbFtPwmEnable(pDev, 
                         pCmd->usrCfg.nChan);
            break;

        case PWM_CMD_STOP:

		    PWM_DBG(PWM_DBG_ALL, "%s,%d PWM_CMD_STOP...\r\n", __FUNCTION__, __LINE__,3,4,5,6);
            retVal = vxbFtPwmDisable(pDev, 
                         pCmd->usrCfg.nChan);
            break;
            
        case PWM_CMD_SET_DIV:
		     PWM_DBG(PWM_DBG_ALL, "%s,%d PWM_CMD_SET_DIV...\r\n", __FUNCTION__, __LINE__,3,4,5,6);
             retVal =  vxbFtPwmDivSet(pDev, 
                         pCmd->usrCfg.nChan,
                         pCmd->advCfg.div);
            break;
            
        case PWM_CMD_SET_POLAR:
		     PWM_DBG(PWM_DBG_ALL, "%s,%d PWM_CMD_SET_POLAR...\r\n", __FUNCTION__, __LINE__,3,4,5,6);
             retVal =  vxbFtPwmPolaritySet(pDev, 
                         pCmd->usrCfg.nChan,
                         pCmd->advCfg.polarity);
            break;

        case PWM_CMD_SET_DEAD_BAND:
            PWM_DBG(PWM_DBG_ALL, "%s,%d PWM_CMD_SET_DEAD_BAND...\r\n", __FUNCTION__, __LINE__,3,4,5,6);
            retVal = vxbFtPwmDbClyNsSet(pDev, 0, pCmd->usrCfg.updbclyNs, pCmd->usrCfg.dwdbclyNs);
            break;
            
        default:
		    PWM_DBG(PWM_DBG_ALL, "%s,%d default command...\r\n", __FUNCTION__, __LINE__,3,4,5,6);
            retVal = ERROR;
            break;
          
		}
        break;
		

    default:
        retVal = ERROR;
        break;
    }
    return retVal;
}

/*****************************************************************************
*
* vxbFtPwmInfoGet -  VxBus PWM info get
*
* This function implements the VxBus I2C EEPROM callback routine install. This 
* routine will supply the read() function, write() function, device name, and 
* chipSize info of EEPROM with I2C interface to upper layer.
*
* RETURNS: OK, or ERROR if failed to get the info.
*
* ERRNO: N/A
*/

LOCAL STATUS vxbFtPwmInfoGet
    (
    VXB_DEVICE_ID               pDev,
    struct vxbPwmInfo **     ppPwmInfo
    )
    {
    FT_PWM_DRVCTRL *           pDrvCtrl;

    if ((pDev == NULL) || (pDev->pDrvCtrl == NULL))
        return ERROR;

    pDrvCtrl = (FT_PWM_DRVCTRL *) pDev->pDrvCtrl;
    *ppPwmInfo = &(pDrvCtrl->pwmInfo);

    return OK;
    }

/*****************************************************************************
*
* vxbFtPwmDump - Dump PWM 
*
* This routine dumps register values. Only a debug routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
void vxbFtPwmDump(VXB_DEVICE_ID pDev, UINT32 nChan)
{
    FT_PWM_DRVCTRL * pDrvCtrl;
    pDrvCtrl = pDev->pDrvCtrl;
    
    printf("Off[0x%02x]: DBCTRL  = 0x%08x\r\n", REG_DBCTRL,DB_READ_4(pDrvCtrl, REG_DBCTRL));
    printf("Off[0x%02x]: DBCLY   = 0x%08x\r\n", REG_DBCLY, DB_READ_4(pDrvCtrl, REG_DBCLY));
    printf("\r\n");
    printf("Off[0x%02x]: TCNT    = 0x%08x\r\n", REG_TCNT,  CH_READ_4(pDrvCtrl, nChan, REG_TCNT));
    printf("Off[0x%02x]: TCTRL   = 0x%08x\r\n", REG_TCTRL, CH_READ_4(pDrvCtrl, nChan, REG_TCTRL));
    printf("Off[0x%02x]: STAT    = 0x%08x\r\n", REG_STAT,  CH_READ_4(pDrvCtrl, nChan, REG_STAT));
    printf("Off[0x%02x]: TPERIOD = 0x%08x\r\n", REG_TPERIOD, CH_READ_4(pDrvCtrl, nChan, REG_TPERIOD));
    printf("Off[0x%02x]: PWMCTRL = 0x%08x\r\n", REG_PWMCTRL, CH_READ_4(pDrvCtrl, nChan, REG_PWMCTRL));
    printf("Off[0x%02x]: PWMCCR  = 0x%08x\r\n", REG_PWMCCR,  CH_READ_4(pDrvCtrl, nChan, REG_PWMCCR));
    printf("\r\n");

    return ;
}

#ifdef INCLUDE_SHOW_ROUTINES
/*******************************************************************************
*
* vxbPwmShow - show the PWM information on the console.
*
* This routine shows the PWM information on the console.
*
* RETURNS: N/A
*
* ERRNO : N/A
*/

LOCAL void vxbPwmShow 
    (
    VXB_DEVICE_ID       pDev, 
    int                 verbose
    )
    {
    FT_PWM_DRVCTRL *   pDrvCtrl;

    if ((pDev == NULL) || ( pDev->pDrvCtrl == NULL))
        return;


    pDrvCtrl = (FT_PWM_DRVCTRL *) pDev->pDrvCtrl;


    (void) printf ("        %s unit %d on %s @ 0x%08x", pDev->pName,
                   pDev->unitNumber, vxbBusTypeString (pDev->busID), pDev);
    (void) printf (" with busInfo %p\n", pDev->u.pSubordinateBus);

    if (verbose >= 1)
        (void) printf ("            Device Addr @ 0x%08x\n", pDev->pRegBase[0]);

    if (verbose >= 100)
        {
            (void) printf ("             DB   Addr @ 0x%08x\n", pDrvCtrl->dbBase);
            (void) printf ("             PWM0 Addr @ 0x%08x\n", pDrvCtrl->pwmBase0);
            (void) printf ("             PWM1 Addr @ 0x%08x\n", pDrvCtrl->pwmBase1);

        }
    return;
    }
#endif /* INCLUDE_SHOW_ROUTINES */

