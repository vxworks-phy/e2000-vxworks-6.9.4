/* vxbFtcan.c - CAN driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <intLib.h>
#include <stdlib.h>
#include <string.h>
#include <sys/times.h>
#include <hwif/util/hwMemLib.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/util/vxbParamSys.h>
#include "vxbFtcan.h"

#undef FTCAN_DEBUG
#ifdef FTCAN_DEBUG
#define FTCAN_LOGMSG(fmt,p1,p2,p3,p4,p5,p6) logMsg(fmt,p1,p2,p3,p4,p5,p6)
#else
#define FTCAN_LOGMSG(fmt,p1,p2,p3,p4,p5,p6)
#endif

/* VxBus methods */

LOCAL void  ftCanInstInit (VXB_DEVICE_ID);
LOCAL void  ftCanInstInit2 (VXB_DEVICE_ID);
LOCAL void  ftCanInstConnect (VXB_DEVICE_ID);
LOCAL void  ftCanInt (FTCAN_DRV_CTRL *pDrvCtrl);
LOCAL void  ftCanRecv(FTCAN_DRV_CTRL * pDrvCtrl);
LOCAL int   ftCanSetBittiming(VXB_DEVICE_ID pDev,FTCAN_BITTIMING *bt,FTCAN_BITTIMING *dbt);
LOCAL int   ftCanCalcBittiming( FTCAN_BITTIMING *bt,const FTCAN_BITTIMING_CONST *btc);
LOCAL void  ftCanRecvTask(FTCAN_DRV_CTRL * pDrvCtrl);
IMPORT int  sysClkRateGet (void);
/* END functions */

LOCAL struct drvBusFuncs ftCanFuncs =
    {
        ftCanInstInit,  /* devInstanceInit */
        ftCanInstInit2, /* devInstanceInit2 */
        ftCanInstConnect    /* devConnect */
    };

LOCAL device_method_t ftCanMethods[] =
    {
    { 0, 0}
    };

LOCAL struct vxbDevRegInfo ftCanDevPlbRegistration =
    {
      
        NULL,           /* pNext */
        VXB_DEVID_DEVICE,   /* devID */
        VXB_BUSID_PLB,      /* busID = PLB */
        VXB_VER_4_0_0,      /* vxbVersion */
        "ftCan",        /* drvName */
        &ftCanFuncs,        /* pDrvBusFuncs */
        NULL,   /* pMethods */
        NULL,           /* devProbe */
        NULL    /* pParamDefaults */
    };

LOCAL VXB_DEVICE_ID glftCanDev[CAN_MAX_CTL];

#ifdef __DCC__  
LOCAL const FTCAN_BITTIMING_CONST ftCanBittimingConst_512 = {
        "vxbftCan",
        1,
        16,
        1,
        8,
        4,
        1,
        512,
        2
    };
LOCAL const FTCAN_BITTIMING_CONST ftCanBittimingConst_8192 = {
        "vxbftCan",
        1,
        16,
        1,
        8,
        4,
        1,
        8192,
        2
    };
#else
LOCAL const FTCAN_BITTIMING_CONST ftCanBittimingConst_512 = {
        .name = "vxbftCan",
        .tseg1Min = 1,
        .tseg1Max = 16,
        .tseg2Min = 1,
        .tseg2Max = 8,
        .sjwMax = 4,
        .brpMin = 1,
        .brpMax = 512,
        .brpInc = 2,
    };
LOCAL const FTCAN_BITTIMING_CONST ftCanBittimingConst_8192 = {
        .name = "vxbftCan",
        .tseg1Min = 1,
        .tseg1Max = 16,
        .tseg2Min = 1,
        .tseg2Max = 8,
        .sjwMax = 4,
        .brpMin = 1,
        .brpMax = 8192,
        .brpInc = 2,
    };
#endif

/* CAN DLC to real data length conversion helpers */

LOCAL const UINT8 dlc2len[] = {0, 1, 2, 3, 4, 5, 6, 7,
                 8, 12, 16, 20, 24, 32, 48, 64};

/* get data length from can_dlc with sanitized can_dlc */
UINT8 ftCanDlc2Len(UINT8 can_dlc)
    {
    return dlc2len[can_dlc & 0x0F];
    }

LOCAL const UINT8 len2dlc[] = {0, 1, 2, 3, 4, 5, 6, 7, 8,      /* 0 - 8 */
                 9, 9, 9, 9,            /* 9 - 12 */
                 10, 10, 10, 10,            /* 13 - 16 */
                 11, 11, 11, 11,            /* 17 - 20 */
                 12, 12, 12, 12,            /* 21 - 24 */
                 13, 13, 13, 13, 13, 13, 13, 13,    /* 25 - 32 */
                 14, 14, 14, 14, 14, 14, 14, 14,    /* 33 - 40 */
                 14, 14, 14, 14, 14, 14, 14, 14,    /* 41 - 48 */
                 15, 15, 15, 15, 15, 15, 15, 15,    /* 49 - 56 */
                 15, 15, 15, 15, 15, 15, 15, 15};   /* 57 - 64 */

/* map the sanitized data length to an appropriate data length code */
UINT8 ftCanLen2Dlc(UINT8 len)
    {
    if (len > 64)
        return 0xF;

    return len2dlc[len];
    }
/*****************************************************************************
*
*vxbFtCanRegister - register with the VxBus subsystem
*
* This routine registers the Template driver with VxBus as a
* child of the PCI bus type.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbFtCanRegister(void)
    {
    vxbDevRegister((struct vxbDevRegInfo *)&ftCanDevPlbRegistration);
    return;
    }

/*****************************************************************************
*
* ftCanInstInit - VxBus instInit handler
*
* This function implements the VxBus instInit handler for an template
* device instance. The only thing done here is to select a unit
* number for the device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftCanInstInit
    (
    VXB_DEVICE_ID pDev
    )
    {
       FTCAN_DRV_CTRL * pDrvCtrl;
       HCF_DEVICE * pHcf;
    
       /* get the HCF device from the instance ID */
    
       pHcf = hcfDeviceGet (pDev);
    
       /* if pHcf is NULL, no device is present in hwconf.c */
    
       if (pHcf == NULL)
           return;
    
       /* allocate memory for the data */
    
       pDrvCtrl = (FTCAN_DRV_CTRL *)hwMemAlloc (sizeof(FTCAN_DRV_CTRL));
    
       if (pDrvCtrl == NULL)
           return;

       bzero((char *)pDrvCtrl, sizeof(FTCAN_DRV_CTRL));
       
       pDrvCtrl->ftCanDev = pDev;
       if (devResourceGet (pHcf, "regBase", HCF_RES_INT,
                        (void *)&pDrvCtrl->ftCanRegbase) != OK)
           {
#ifndef _VXBUS_BASIC_HWMEMLIB
           hwMemFree((char *)pDrvCtrl);
#endif
           return;
           }
       if (devResourceGet (pHcf, "irq", HCF_RES_INT,
                        (void *)&pDrvCtrl->irq) != OK)
           {
#ifndef _VXBUS_BASIC_HWMEMLIB
           hwMemFree((char *)pDrvCtrl);
#endif
           return;
           }
       if (devResourceGet (pHcf, "bitrate", HCF_RES_INT,
                        (void *)&pDrvCtrl->ftCanBittiming.bitrate) != OK)
           {
#ifndef _VXBUS_BASIC_HWMEMLIB
           hwMemFree((char *)pDrvCtrl);
#endif
           return;
           }
       if (devResourceGet (pHcf, "isCanFD", HCF_RES_INT,
                        (void *)&pDrvCtrl->isCanFD) != OK)
           {
#ifndef _VXBUS_BASIC_HWMEMLIB
           hwMemFree((char *)pDrvCtrl);
#endif
           return;
           }
       if(pDrvCtrl->isCanFD)
           {
            if (devResourceGet (pHcf, "dataBitrate", HCF_RES_INT,
                            (void *)&pDrvCtrl->ftCanDataBittiming.bitrate) != OK)
               {
#ifndef _VXBUS_BASIC_HWMEMLIB
               hwMemFree((char *)pDrvCtrl);
#endif
               return;
               }
           }
      pDev->pDrvCtrl = pDrvCtrl;    
      vxbRegMap(pDev, 0, &pDrvCtrl->ftCanHandle);
      glftCanDev[pDev->unitNumber]= pDev;
      
      return;
      }

/*****************************************************************************
*
* ftCanInstInit2 - VxBus instInit2 handler
*
* This function implements the VxBus instInit2 handler for an template
* device instance. 
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftCanInstInit2
    (
    VXB_DEVICE_ID pDev
    )
    {
    FTCAN_DRV_CTRL *pDrvCtrl;
    FTCAN_BITTIMING bt;
    FTCAN_BITTIMING dbt;
    int ret;
    
    pDrvCtrl = (FTCAN_DRV_CTRL *)pDev->pDrvCtrl;  

	if ((pDrvCtrl->canRxQueue = msgQCreate(CAN_QUEUE_SIZE, sizeof(FTCANFD_FRAME),
                                       MSG_Q_FIFO)) == NULL)
        {
        FTCAN_LOGMSG("<ftCanInstInit2> Unable to create FT CAN message queue\n",
                             0,0,0,0,0,0);
        return;
        }	
    
    if ((pDrvCtrl->canTxSem = semBCreate (SEM_Q_PRIORITY, SEM_EMPTY)) == NULL)
        {
        FTCAN_LOGMSG("<ftCanInstInit2> Unable to create FT CAN Tx Sem\n",
                             0,0,0,0,0,0);
        return;
        }
    
    if ((pDrvCtrl->canRxSem = semBCreate (SEM_Q_PRIORITY, SEM_EMPTY)) == NULL)
        {
        FTCAN_LOGMSG("<ftCanInstInit2> Unable to create FT CAN Rx Sem\n",
                             0,0,0,0,0,0);
        return;
        }
    /*Disable Transfer*/
    CSR_CLRBIT_4(pDev, CAN_CTRL, CTRL_XFER);
    /*reset canfd*/
    CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_RST);

    bt.bitrate = pDrvCtrl->ftCanBittiming.bitrate;
    bt.sjw = 0;
    bt.samplePoint = 0;
    ret = ftCanCalcBittiming(&bt,&ftCanBittimingConst_8192);
    if(ret<0)
        {
        FTCAN_LOGMSG("ftCanCalcBittiming error\n",1,2,3,4,5,6);
         return ;
        }
    if(pDrvCtrl->isCanFD)
        {
        dbt.bitrate = pDrvCtrl->ftCanDataBittiming.bitrate;
        dbt.sjw = 0;
        dbt.samplePoint = 0;
        ret = ftCanCalcBittiming(&dbt,&ftCanBittimingConst_8192);
        if(ret<0)
            {
            FTCAN_LOGMSG("ftCanCalcBittiming error\n",1,2,3,4,5,6);
            return ;
            }
        }
    
    if (ftCanSetBittiming(pDev,&bt,&dbt) < 0) 
        {
        FTCAN_LOGMSG("ftCanSetBittiming error\n",1,2,3,4,5,6);
        return ;
        }
    pDrvCtrl->ftCanBittiming.brp=bt.brp;
    pDrvCtrl->ftCanBittiming.sjw=bt.sjw;
    pDrvCtrl->ftCanBittiming.tq =bt.tq;
    pDrvCtrl->ftCanBittiming.phaseSeg1= bt.phaseSeg1;
    pDrvCtrl->ftCanBittiming.phaseSeg2= bt.phaseSeg2;
    pDrvCtrl->ftCanBittiming.samplePoint=bt.samplePoint;
    if(pDrvCtrl->isCanFD)
        {
        pDrvCtrl->ftCanDataBittiming.brp=dbt.brp;
        pDrvCtrl->ftCanDataBittiming.sjw=dbt.sjw;
        pDrvCtrl->ftCanDataBittiming.tq =dbt.tq;
        pDrvCtrl->ftCanDataBittiming.phaseSeg1= dbt.phaseSeg1;
        pDrvCtrl->ftCanDataBittiming.phaseSeg2= dbt.phaseSeg2;
        pDrvCtrl->ftCanDataBittiming.samplePoint=dbt.samplePoint;
        }
    
    CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_AIME | CTRL_IOF);
    CSR_WRITE_4(pDev, CAN_ACC_ID0_MASK, ACC_IDX_MASK_AID_MASK);
    CSR_WRITE_4(pDev, CAN_ACC_ID1_MASK, ACC_IDX_MASK_AID_MASK);
    CSR_WRITE_4(pDev, CAN_ACC_ID2_MASK, ACC_IDX_MASK_AID_MASK);
    CSR_WRITE_4(pDev, CAN_ACC_ID3_MASK, ACC_IDX_MASK_AID_MASK);
    
    if(pDrvCtrl->isCanFD)
        CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_FDCRC); 
    else
    	CSR_CLRBIT_4(pDev, CAN_CTRL, CTRL_FDCRC);
    
    pDrvCtrl->isTxFifoFull = FALSE;
    }

/*****************************************************************************
*
* ftCanInstConnect -  VxBus instConnect handler
*
* This function implements the VxBus instConnect handler for an template
* device instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void ftCanInstConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
    int st;
    FTCAN_DRV_CTRL * pFtCanCtrl;
	char task_name[32];
	TASK_ID tid;
	
    pFtCanCtrl = pDev->pDrvCtrl;   
    
    bzero(task_name, sizeof(task_name));
	sprintf(task_name, "tCanRx%d", pDev->unitNumber);
    tid = taskSpawn(task_name, 100, 0, 0x20000,
		            (FUNCPTR) ftCanRecvTask,(_Vx_usr_arg_t) pFtCanCtrl,
		            2,3,4,5,6,7,8,9,10);
	if(TASK_ID_ERROR == tid)
	{
	    FTCAN_LOGMSG("failed to spawn can recv task!\n",1,2,3,4,5,6);
		return;
	}

    st = vxbIntConnect (pDev, 0, ftCanInt, pFtCanCtrl);
    st |= vxbIntEnable (pDev, 0, ftCanInt, pFtCanCtrl);

    if(st != OK)
        {
        FTCAN_LOGMSG("interrupt cn/en failed!\n",1,2,3,4,5,6);
        return;
        }
    
    /* Enable interrupts */
    CSR_WRITE_4(pDev, CAN_INTR, INTR_EN_MASK);

    /*Enable Transfer*/
    CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_XFER | CTRL_TXREQ);
    FTCAN_LOGMSG("status:#x%08x\n",CSR_READ_4(pDev,CAN_XFER_STS),2,3,4,5,6);
    return;
   }


LOCAL unsigned int ftCanDiv64To32(unsigned long long *n, unsigned int base)
    {
    unsigned long long rem = *n;
    unsigned long long b = base;
    unsigned long long res, d = 1;
    unsigned int high = rem >> 32;

    /* Reduce the thing a bit first */
    res = 0;
    if (high >= base) 
        {
        high /= base;
        res = (unsigned long long) high << 32;
        rem -= (unsigned long long) (high*base) << 32;
        }

    while ((unsigned long long)b > 0 && b < rem) 
        {
        b = b+b;
        d = d+d;
        }

    do  {
        if (rem >= b) 
            {
            rem -= b;
            res += d;
            }
        b >>= 1;
        d >>= 1;
        } while (d);

    *n = res;
    return rem;
    }

/*****************************************************************************
*
* ftCanUpdateSamplePoint -  Bit-timing calculation
*
* Calculates proper bit-timing parameters for a specified bit-rate
* and sample-point, which can then be used to set the bit-timing
* registers of the CAN controller.
*
* RETURNS: sample point
*
* ERRNO: N/A
*/

LOCAL int ftCanUpdateSamplePoint( const FTCAN_BITTIMING_CONST *btc,
              unsigned int samplePointNominal, unsigned int tseg,
              unsigned int *tseg1Ptr, unsigned int *tseg2Ptr,
              unsigned int *samplePointErrorPtr)
    {
    unsigned int samplePointError, bestSamplePointError = UINT_MAX;
    unsigned int samplePoint, bestSamplePoint = 0;
    unsigned int tseg1, tseg2;
    int i;

    for (i = 0; i <= 1; i++) 
        {
        tseg2 = tseg + CAN_CALC_SYNC_SEG - (samplePointNominal * (tseg + CAN_CALC_SYNC_SEG)) / 1000 - i;
        tseg2 = clamp(tseg2, btc->tseg2Min, btc->tseg2Max);
        tseg1 = tseg - tseg2;
        if (tseg1 > btc->tseg1Max) 
            {
            tseg1 = btc->tseg1Max;
            tseg2 = tseg - tseg1;
            }

        samplePoint = 1000 * (tseg + CAN_CALC_SYNC_SEG - tseg2) / (tseg + CAN_CALC_SYNC_SEG);
        samplePointError = abs(samplePointNominal - samplePoint);

        if ((samplePoint <= samplePointNominal) && (samplePointError < bestSamplePointError)) 
            {
            bestSamplePoint = samplePoint;
            bestSamplePointError = samplePointError;
            *tseg1Ptr = tseg1;
            *tseg2Ptr = tseg2;
            }
        }

    if (samplePointErrorPtr)
        *samplePointErrorPtr = bestSamplePointError;

    return bestSamplePoint;
    }

/*****************************************************************************
*
* ftCanCalcBittiming -  Bit-timing calculation
*
* Calculates proper bit-timing parameters for a specified bit-rate
* and sample-point, which can then be used to set the bit-timing
* registers of the CAN controller.
*
* RETURNS: 0 on success and failure value on error
*
* ERRNO: N/A
*/

LOCAL int ftCanCalcBittiming( FTCAN_BITTIMING *bt,
                  const FTCAN_BITTIMING_CONST *btc)
    {
    unsigned int bitrate;           /* current bitrate */
    unsigned int bitrateError;     /* difference between current and nominal value */
    unsigned int bestBitrateError = UINT_MAX;
    unsigned int samplePointError;    /* difference between current and nominal value */
    unsigned int bestSamplePointError = UINT_MAX;
    unsigned int samplePointNominal;  /* nominal sample point */
    unsigned int bestTseg = 0;     /* current best value for tseg */
    unsigned int bestBrp = 0;      /* current best value for brp */
    unsigned int brp, tsegall, tseg, tseg1 = 0, tseg2 = 0;
    unsigned long long v64;
  
    /* Use CiA recommended sample points */
    if (bt->samplePoint) 
        {
        samplePointNominal = bt->samplePoint;
        } 
    else 
        {
        if (bt->bitrate > 1000000)
            samplePointNominal = 650;
        else if (bt->bitrate > 800000)
            samplePointNominal = 750;
        else if (bt->bitrate > 500000)
            samplePointNominal = 800;
        else
            samplePointNominal = 875;
        }

    /* tseg even = round down, odd = round up */
    for (tseg = (btc->tseg1Max + btc->tseg2Max) * 2 + 1;
         tseg >= (btc->tseg1Min + btc->tseg2Min) * 2; tseg--) 
        {
        tsegall = CAN_CALC_SYNC_SEG + tseg / 2;
      
        /* Compute all possible tseg choices (tseg=tseg1+tseg2) */
        brp = CAN_CLK_FREQ / (tsegall * bt->bitrate) + tseg % 2;

        /* choose brp step which is possible in system */
        brp = (brp / btc->brpInc) * btc->brpInc;
    
        if ((brp < btc->brpMin) || (brp > btc->brpMax))
            continue;

        bitrate = CAN_CLK_FREQ / (brp * tsegall);
    
        bitrateError = abs(bt->bitrate - bitrate);
        /* tseg brp biterror */
        if (bitrateError > bestBitrateError)
            continue;

        /* reset sample point error if we have a better bitrate */
        if (bitrateError < bestBitrateError)
            bestSamplePointError = UINT_MAX;

        ftCanUpdateSamplePoint(btc, samplePointNominal, tseg / 2, &tseg1, &tseg2, &samplePointError);
        if (samplePointError > bestSamplePointError)
            continue;

        bestSamplePointError = samplePointError;
        bestBitrateError = bitrateError;
        bestTseg = tseg / 2;
        bestBrp = brp;

        if (bitrateError == 0 && samplePointError == 0)
            break;
        }

    if (bestBitrateError) 
        {
        /* Error in one-tenth of a percent */
        v64 = (unsigned long long)bestBitrateError * 1000;
        ftCanDiv64To32(&v64, bt->bitrate);
        bitrateError = (unsigned int)v64;
        if (bitrateError > CAN_CALC_MAX_ERROR) 
            {
            FTCAN_LOGMSG("bitrate error %d.%d%% too high\n",
                   bitrateError / 10, bitrateError % 10,3,4,5,6);
            return ERROR;
            }
        FTCAN_LOGMSG( "bitrate error %d.%d%%\n",
                bitrateError / 10, bitrateError % 10,3,4,5,6);
        }

    /* real sample point */
    bt->samplePoint = ftCanUpdateSamplePoint(btc, samplePointNominal, bestTseg,
                      &tseg1, &tseg2, NULL);

    v64 = (unsigned long long)bestBrp * 1000 * 1000 * 1000;
    ftCanDiv64To32(&v64, CAN_CLK_FREQ);
    bt->tq = (unsigned long long)v64;
    bt->propSeg = tseg1 / 2;
    bt->phaseSeg1 = tseg1 - bt->propSeg;
    bt->phaseSeg2 = tseg2;

    /* check for sjw user settings */
    if (!bt->sjw || !btc->sjwMax) 
        {
        bt->sjw = 1;
        } 
    else 
        {
        /* bt->sjw is at least 1 -> sanitize upper bound to sjwMax */
        if (bt->sjw > btc->sjwMax)
            bt->sjw = btc->sjwMax;
        /* bt->sjw must not be higher than tseg2 */
        if (tseg2 < bt->sjw)
            bt->sjw = tseg2;
        }

    bt->brp = bestBrp;

    /* real bitrate */
    bt->bitrate = CAN_CLK_FREQ / (bt->brp * (CAN_CALC_SYNC_SEG + tseg1 + tseg2));

    return OK;
    }

/*****************************************************************************
*
* ftCanSetBittiming -  CAN set bit timing routine
*
* This is the driver set bittiming  routine.
*
* RETURNS: 0 on success and failure value on error
*
* ERRNO: N/A
*/
LOCAL int ftCanSetBittiming(VXB_DEVICE_ID pDev,FTCAN_BITTIMING *bt,FTCAN_BITTIMING *dbt)
    {
    FTCAN_DRV_CTRL* pDrvCtrl;
    UINT btr,dbtr;
    UINT is_xfer_mode;

    pDrvCtrl = (FTCAN_DRV_CTRL *)pDev->pDrvCtrl;
    /* Setting Baud Rate prescalar value in BRPR Register */
    btr = (bt->brp - 1) << 16;
    /* Setting Time Segment 1 in BTR Register */
    btr |= (bt->propSeg - 1) << 2;
    btr |= (bt->phaseSeg1 - 1) << 5;
    /* Setting Time Segment 2 in BTR Register */
    btr |= (bt->phaseSeg2 - 1) << 8;
    /* Setting Synchronous jump width in BTR Register */
    btr |= (bt->sjw - 1);

    if(pDrvCtrl->isCanFD)
        {
        dbtr = (dbt->brp - 1) << 16;
        dbtr |= (dbt->propSeg - 1) << 2;
        dbtr |= (dbt->phaseSeg1 - 1) << 5;
        dbtr |= (dbt->phaseSeg2 - 1) << 8;
        dbtr |= (dbt->sjw - 1);
        }
    FTCAN_LOGMSG("%s:Update Can btr:0x%x dbtr:0x%x \n", __func__, btr,dbtr,4,5,6);
    /* Check whether Phytium CAN is in configuration mode.
     * It cannot set bit timing if Phytium CAN is not in configuration mode.
     */
    is_xfer_mode = (CSR_READ_4(pDev, CAN_CTRL) & CTRL_XFER);
    if (is_xfer_mode) 
        {
        /*Disable Transfer*/
        CSR_CLRBIT_4(pDev, CAN_CTRL, CTRL_XFER);
    
        }
    
    CSR_WRITE_4(pDev, CAN_ARB_RATE_CTRL, btr);
    if(pDrvCtrl->isCanFD)
        {
        CSR_WRITE_4(pDev, CAN_DAT_RATE_CTRL, dbtr);
        }
    else
        {
        CSR_WRITE_4(pDev, CAN_DAT_RATE_CTRL, btr);
        }
    if (is_xfer_mode) 
        {
    /*Enable Transfer*/
    CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_XFER);
        }
    FTCAN_LOGMSG( "DAT=0x%08x, ARB=0x%08x\n",
            CSR_READ_4(pDev, CAN_DAT_RATE_CTRL),
            CSR_READ_4(pDev, CAN_ARB_RATE_CTRL),3,4,5,6);
    return OK;
    }

/*****************************************************************************
*
* ftCanErrInterrupt - error frame Isr
*
* This is the CAN error interrupt and it will
* check the the type of error
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftCanErrInterrupt(VXB_DEVICE_ID pDev, UINT isr)
    {
    UINT  txerr = 0, rxerr = 0;

    rxerr = CSR_READ_4(pDev, CAN_ERR_CNT) & ERR_CNT_REC;
    txerr = ((CSR_READ_4(pDev, CAN_ERR_CNT) & ERR_CNT_TEC) >> 16);

    FTCAN_LOGMSG("%s: rx error count %d, tx error count %d\n",
            __func__, rxerr,txerr,4,5,6);   
    FTCAN_LOGMSG("%s: error status register:0x%x\n",
            __func__, (CSR_READ_4(pDev, CAN_INTR) & INTR_STATUS_MASK),3,4,5,6);
    }

/*****************************************************************************
*
* ftCanTxInterrupt - Tx Done Isr
*
* This is the CAN Tx Done interrupt 
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftCanTxInterrupt(VXB_DEVICE_ID pDev, UINT isr)
    {
    FTCAN_DRV_CTRL * pFtCanCtrl;
    UINT32 txFifoUsed;
	UINT32 keepCanFifoMinLen;
	
    pFtCanCtrl = pDev->pDrvCtrl;
    
    if(isr & INTR_TEIS)
        CSR_SETBIT_4(pDev, CAN_INTR, INTR_TEIC);

    if(pFtCanCtrl->isTxFifoFull == TRUE)
    	{
    	/*剩下的FIFO空间大于CAN/CANFD一帧的长度时，释放信号量,进行后续传输*/
		txFifoUsed = 4 * ((CSR_READ_4(pDev, CAN_FIFO_CNT) & FIFO_CNT_TFN) >> 16);
		
		if(pFtCanCtrl->isCanFD)
			keepCanFifoMinLen = KEEP_CANFD_FIFO_MIN_LEN;
		else
			keepCanFifoMinLen = KEEP_CAN_FIFO_MIN_LEN;
		
		if(CAN_FIFO_BYTE_LEN - txFifoUsed > keepCanFifoMinLen)
			{
			if (semGive (pFtCanCtrl->canTxSem) == ERROR)
				{
				FTCAN_LOGMSG("semGive error\n",  0, 0, 0, 0, 0, 0);
				}
			pFtCanCtrl->isTxFifoFull = FALSE;
			}
    	}
    }

/*****************************************************************************
*
* ftCanRecvTask -  Receiving task
*
* This function receives data
* RETURNS: N/A
*
* ERRNO: N/A
*/
ULONG       ftCanRecvCnt = 1; 
LOCAL void ftCanRecvTask
    (
    FTCAN_DRV_CTRL * pDrvCtrl
    )
    {
	FTCANFD_FRAME ftCanFrame;
    UINT  maxNBytes = sizeof(ftCanFrame);
    
    for(;;)
    {
       bzero((char *)&ftCanFrame, sizeof(ftCanFrame));
	   msgQReceive(pDrvCtrl->canRxQueue,(char *)&ftCanFrame,maxNBytes,WAIT_FOREVER);

       if(pDrvCtrl->ftCanRecvRtn != NULL)
       	   {
           pDrvCtrl->ftCanRecvRtn(&ftCanFrame);
       	   }
	   else
	   	   {
		   printf("%s count:%d \r\n  recevied canid:%x can_dlc:%d.  \r\n",
			      __FUNCTION__,ftCanRecvCnt, ftCanFrame.canId & CAN_EFF_MASK,ftCanFrame.len);
		   printf("data:%02x %02x %02x %02x %02x %02x\r\n", 
		    		 ftCanFrame.data[0],ftCanFrame.data[1],ftCanFrame.data[2],
		    		 ftCanFrame.data[ftCanFrame.len - 3],ftCanFrame.data[ftCanFrame.len - 2],
		    		 ftCanFrame.data[ftCanFrame.len - 1]);
			   
		   ftCanRecvCnt++;
	   	   }
    }
	return ;	   
    }

/*****************************************************************************
*
* ftCanReadFifo - Read Rx Fifo
*
* This function reads the CAN Rx fifo 
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftCanReadFifo(FTCAN_DRV_CTRL*  pDrvCtrl)
    {
    UINT id;
    UINT dlc = 0;
    FTCANFD_FRAME cf;
    VXB_DEVICE_ID pDev;
    UINT i;
    
    memset(&cf,0,sizeof(FTCANFD_FRAME));
    pDev= pDrvCtrl->ftCanDev;
   
    /* Read the frame header from FIFO */
    id = CSR_READ_4(pDev, CAN_RX_FIFO);
    id = be32_to_cpup(id);
    if (id & FTCAN_IDR_IDE_MASK) 
        {
        /* Received an extended frame */
        dlc = CSR_READ_4(pDev, CAN_RX_FIFO);

        dlc = be32_to_cpup(dlc);

        if (dlc & FTCANFD_ID2_FDL_MASK) 
            {
            /* CAN FD extended frame */
            if (dlc & FTCANFD_ID2_BRS_MASK)
                cf.flags |= CANFD_BRS;
            if (dlc & FTCANFD_ID2_ESI_MASK)
                cf.flags |= CANFD_ESI;
            dlc = (dlc & FTCANFD_ID2_EDLC_MASK) >> FTCANFD_IDR_EDLC_SHIFT;
            cf.len = ftCanDlc2Len(dlc);
            } 
        else 
            {
            /* CAN extended frame */
            dlc = ((dlc & FTCAN_IDR_EDLC_MASK) >> FTCAN_IDR_EDLC_SHIFT);
            cf.len = get_can_dlc(dlc);
            }

        cf.canId = (id & FTCAN_IDR_ID1_MASK) >> 3;
        cf.canId |= (id & FTCAN_IDR_ID2_MASK) >> FTCAN_IDR_ID2_SHIFT;
        cf.canId |= CAN_EFF_FLAG;

        if (id & FTCAN_IDR_RTR_MASK)
            cf.canId |= CAN_RTR_FLAG;
        } 
    else 
        {
        /* Received a standard frame */
        if (id & FTCANFD_ID1_FDL_MASK) 
            {
            /* CAN FD extended frame */
            if (id & FTCANFD_ID1_BRS_MASK)
                cf.flags |= CANFD_BRS;
            if (id & FTCANFD_ID1_ESI_MASK)
                cf.flags |= CANFD_ESI;
            dlc = ((id & FTCANFD_ID1_SDLC_MASK) >> FTCANFD_IDR1_SDLC_SHIFT);
            cf.len = ftCanDlc2Len(dlc);
            } 
        else 
            {
            /* CAN extended frame */
            dlc = ((id & FTCAN_IDR_DLC_MASK ) >> FTCAN_IDR_SDLC_SHIFT);
            cf.len = get_can_dlc(dlc);
            }
        cf.canId = (id & FTCAN_IDR_ID1_MASK) >> FTCAN_IDR_ID1_SHIFT;
        if (id & FTCAN_IDR_SRR_MASK)
            cf.canId |= CAN_RTR_FLAG;
        }
        
    if (!(cf.canId & CAN_RTR_FLAG))
        {
        for (i = 0; i < cf.len; i += 4) 
            {
            *(UINT32 *)(cf.data + i) = CSR_READ_4(pDev, CAN_RX_FIFO);
            }
        }

	FTCAN_LOGMSG("recevied canid %x can_dlc %d data 0x%2x 0x%2x 0x%2x 0x%2x ",
			cf.can_id & CAN_EFF_MASK,cf.len,cf.data[0],cf.data[1],cf.data[cf.len - 2],cf.data[cf.len - 1]);

    msgQSend(pDrvCtrl->canRxQueue,(char * )&cf,sizeof(FTCANFD_FRAME),NO_WAIT,MSG_PRI_NORMAL);
    }

/*****************************************************************************
*
* ftCanRecv -  Receiving data
*
* This function receives data
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void ftCanRecv
    (
    FTCAN_DRV_CTRL * pDrvCtrl
    )
    {
    VXB_DEVICE_ID pDev;
    u32 rxfs = 0;
    
    pDev = pDrvCtrl->ftCanDev;
    rxfs = CSR_READ_4(pDev, CAN_FIFO_CNT) & FIFO_CNT_RFN;
    
    if (!rxfs) 
        {
        FTCAN_LOGMSG("no messages in RX FIFO\n",1,2,3,4,5,6);
        return;
        }
   
    while ((rxfs > 0)) 
        {
        ftCanReadFifo(pDrvCtrl);
        rxfs = CSR_READ_4(pDev, CAN_FIFO_CNT) & FIFO_CNT_RFN;
        }

    return;
    }


STATUS ftCanReset(VXB_DEVICE_ID pDev)
    {
    FTCAN_DRV_CTRL * pDrvCtrl = pDev->pDrvCtrl;
    
    /*Disable Transfer*/
    CSR_CLRBIT_4(pDev, CAN_CTRL, CTRL_XFER);
    /*reset canfd*/
    CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_RST);

    pDrvCtrl->isTxFifoFull = FALSE;

    while(CSR_READ_4(pDev,CAN_XFER_STS) & XFER_STS_TS);

    /*Enable Transfer*/
    CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_XFER);
    return OK;
    }
/*****************************************************************************
*
* ftCanInt - VxBus interrupt handler
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
LOCAL void  ftCanInt (FTCAN_DRV_CTRL*  pDrvCtrl)
    {
    VXB_DEVICE_ID pDev;
    UINT isr;
    
    pDev = pDrvCtrl->ftCanDev;
    /* Get the interrupt status from Phytium CAN */
    isr = (CSR_READ_4(pDev, CAN_INTR) & INTR_STATUS_MASK);
    if (!isr)
        return;

    /* Handle tx IRQ
     * - TEIS: TX frame end
     */
    if (isr & INTR_TEIS) 
        {
    	ftCanTxInterrupt(pDev, isr);
        }
    
    /* Handle error IRQ 
     * - error IRQ (BOIS|PWIS|PEIS|EIS|RFIS)
     */
    if (isr & (INTR_BOIS | INTR_PWIS | INTR_PEIS | INTR_EIS  | INTR_RFIS)) 
        {
    	CSR_CLRBIT_4(pDev, CAN_INTR, INTR_BOIE | INTR_PWIE | INTR_PEIE | INTR_EIE  | INTR_RFIE);
    	ftCanErrInterrupt(pDev, isr);
    	FTCAN_LOGMSG("ftCanInt isr: %x\n",isr,2,3,4,5,6);
    	CSR_SETBIT_4(pDev, CAN_INTR, INTR_BOIC | INTR_PWIC | INTR_PEIC | INTR_EIC | INTR_RFIC);
        }    
    
    /* Handle rx IRQ
     *   - RFIS: RX FIFO Full
     *   - REIS: RX frame end
     */
    if (isr & (INTR_REIS)) 
        {
    	CSR_CLRBIT_4(pDev, CAN_INTR, INTR_REIE );
        ftCanRecv(pDrvCtrl);
        CSR_SETBIT_4(pDev, CAN_INTR, INTR_REIC );
        CSR_SETBIT_4(pDev, CAN_INTR, INTR_REIE);
        }

    return;
   }

/*****************************************************************************
*
* ftCanWrite - Sending the CAN data 
*
* RETURNS: Sent frame number
*
* ERRNO: N/A
*/
 LOCAL STATUS ftCanWrite(VXB_DEVICE_ID pDev,FTCANFD_FRAME *sendFrame)
    {
    FTCAN_DRV_CTRL * pFtCanCtrl;
    FTCANFD_FRAME *cf = sendFrame;
    UINT id = 0, dlc = 0, len = 0, i;
    UINT32 txFifoUsed;
	UINT32 keepCanFifoMinLen;
	
    pFtCanCtrl = pDev->pDrvCtrl;    
    if(sendFrame == NULL)
        {
        FTCAN_LOGMSG("Send Frame is NULL.\r\n", 1,2,3,4,5,6);
        return ERROR;
        }   
    
    len = ftCanLen2Dlc(cf->len);
    if (cf->canId & CAN_EFF_FLAG) 
        {
        /* Extended CAN ID format */
        id = ((cf->canId & CAN_EFF_MASK) << FTCAN_IDR_ID2_SHIFT) &
                FTCAN_IDR_ID2_MASK;
        id |= (((cf->canId & CAN_EFF_MASK) >>
                (CAN_EFF_ID_BITS-CAN_SFF_ID_BITS)) <<
                FTCAN_IDR_ID1_SHIFT) & FTCAN_IDR_ID1_MASK;

        /* Set SRR and IDE bit of extended frame */
        id |= FTCAN_IDR_IDE_MASK | FTCAN_IDR_SRR_MASK;

        /* Set RTR bit for remote TX request */
        if (cf->canId & CAN_RTR_FLAG)
            id |= FTCAN_IDR_RTR_MASK;
    
        if(pFtCanCtrl->isCanFD)
            {
            dlc = len << FTCANFD_IDR_EDLC_SHIFT;
            
            dlc |=  FTCANFD_ID2_FDL_MASK;

            if (cf->flags & CANFD_BRS)
                    dlc |= FTCANFD_ID2_BRS_MASK;
            if (cf->flags & CANFD_ESI)
                    dlc |= FTCANFD_ID2_ESI_MASK;
            }
        else
            {
            dlc = len << FTCAN_IDR_EDLC_SHIFT;
            }
        } 
    else 
        {
        /* Standard CAN ID format */
        id = ((cf->canId & CAN_SFF_MASK) << FTCAN_IDR_ID1_SHIFT) &
                FTCAN_IDR_ID1_MASK;

        /* Set RTR bit for remote TX request */
        if (cf->canId & CAN_RTR_FLAG)
            id |= FTCAN_IDR_SRR_MASK;
        
        if(pFtCanCtrl->isCanFD)
            {
            id |= ((len << FTCANFD_IDR1_SDLC_SHIFT) | FTCANFD_IDR_PAD_MASK);
            id |= FTCANFD_ID1_FDL_MASK;

            if (cf->flags & CANFD_BRS)
                    id |= FTCANFD_ID1_BRS_MASK;
            if (cf->flags & CANFD_ESI)
                    id |= FTCANFD_ID1_ESI_MASK;
            }
        else
            {
            id |= ((len << FTCAN_IDR_SDLC_SHIFT) | FTCAN_IDR_PAD_MASK);
            }
        }

    /*软件向fifo写完一帧完整帧后，硬件对fifo中是否存在完整帧进行判断，
                判断为完整帧后会自动进行发送，不再需要驱动使能发送*/
    CSR_WRITE_4(pDev, CAN_TX_FIFO, cpu_to_be32p(id));
    if (dlc)
        CSR_WRITE_4(pDev, CAN_TX_FIFO, cpu_to_be32p(dlc));
    
    if (!(cf->canId & CAN_RTR_FLAG))
        {
        for (i = 0; i < ftCanDlc2Len(len); i += 4)
            CSR_WRITE_4(pDev, CAN_TX_FIFO, *(UINT32 *)(cf->data + i));
        }  
    
	/*剩下的FIFO空间小于等于CAN/CANFD一帧的长度时，等待发送完成中断释放FIFO空间*/
	txFifoUsed = 4 * ((CSR_READ_4(pDev, CAN_FIFO_CNT) & FIFO_CNT_TFN) >> 16);

    if(pFtCanCtrl->isCanFD)
        keepCanFifoMinLen = KEEP_CANFD_FIFO_MIN_LEN;
    else
		keepCanFifoMinLen = KEEP_CAN_FIFO_MIN_LEN;

	if(CAN_FIFO_BYTE_LEN - txFifoUsed <= keepCanFifoMinLen)
		{
		pFtCanCtrl->isTxFifoFull = TRUE;
		if (semTake (pFtCanCtrl->canTxSem, sysClkRateGet()*2) == ERROR)
			{
			ftCanReset(pDev);
			FTCAN_LOGMSG("SemTake Error. Reset can %d.\n", vxbDevUnitGet(pDev), 2, 3, 4, 5, 6);
			return ERROR;
			}
		}

    return OK;
    }
 
 /*****************************************************************************
*
* ftCanSend -  send can msg. API function.
*
* This function  is used to send data
* 
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/
STATUS ftCanSend
    (
    unsigned char ctlNo,
    FTCANFD_FRAME *sendFrame
    )
    {
    VXB_DEVICE_ID pDev;
      
    if(ctlNo >= CAN_MAX_CTL)
        {
        printf("ctlNo %d is invalid.\r\n", ctlNo);
        return ERROR;
        }
      
    pDev= glftCanDev[ctlNo];
    if(pDev == NULL)
        {
        printf("pDev is NULL!\r\n");
        return ERROR;
        }
      
    return ftCanWrite(pDev,sendFrame); 
    }
    
 
 /*****************************************************************************
*
* ftCanRecvCallback - receive callback
*
* Registers the receive processing function for the specified CAN controller
* ftCanRecvRtn:The pointer function of the receiving process
* 
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/  
STATUS ftCanRecvCallback
    (
    unsigned char ctlNo,
    void (*ftCanRecvRtn)(FTCANFD_FRAME* pftCanfdFrame)
    )
    {
    VXB_DEVICE_ID  pDev;
    FTCAN_DRV_CTRL * pDrvCtrl;

    if(ctlNo >= CAN_MAX_CTL)
        {
        FTCAN_LOGMSG("ctlNo %d is invalid.\r\n", ctlNo,2,3,4,5,6);
        return ERROR;
        }
    pDev= glftCanDev[ctlNo];

    if(pDev == NULL)
        {
        FTCAN_LOGMSG("pDev is NULL!\r\n",1,2,3,4,5,6);
        return ERROR;
        }

    pDrvCtrl = (FTCAN_DRV_CTRL * )pDev->pDrvCtrl;

    if(pDrvCtrl == NULL)
        {
        FTCAN_LOGMSG("pDrvCtrl is NULL for %d!\r\n", pDev->unitNumber,2,3,4,5,6);
        return ERROR;
        }

    pDrvCtrl->ftCanRecvRtn = ftCanRecvRtn;
    return OK;
    }
    
 /*****************************************************************************
*
* ftCanSetBitrate -  set bitrate
*
* This function Set Bitrate for can
* 
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/  
STATUS ftCanSetBitrate
     (
     unsigned char ctlNo,
     UINT bitrate,
     UINT dataBitrate
     )
     {
      VXB_DEVICE_ID pDev;
      FTCAN_BITTIMING bt,dbt;
      int ret;
      FTCAN_DRV_CTRL * pDrvCtrl;
      pDev= glftCanDev[ctlNo];
      pDrvCtrl=  pDev->pDrvCtrl;
      bt.bitrate = bitrate;
      bt.sjw = 0;
      bt.samplePoint = 0;
      ret = ftCanCalcBittiming(&bt,&ftCanBittimingConst_8192);
      if(ret<0)
          {
          return ERROR;
          }
      if(pDrvCtrl->isCanFD)
          {
          dbt.bitrate = dataBitrate;
          dbt.sjw = 0;
          dbt.samplePoint = 0;
          ret = ftCanCalcBittiming(&dbt,&ftCanBittimingConst_8192);
          if(ret<0)
              {
              return ERROR;
              }
          }
      if (ftCanSetBittiming(pDev,&bt,&dbt) < 0) 
          {
          return ERROR;
          }
      pDrvCtrl->ftCanBittiming.bitrate = bitrate;
      pDrvCtrl->ftCanBittiming.brp=bt.brp;
      pDrvCtrl->ftCanBittiming.sjw=bt.sjw;
      pDrvCtrl->ftCanBittiming.tq =bt.tq;
      pDrvCtrl->ftCanBittiming.phaseSeg1= bt.phaseSeg1;
      pDrvCtrl->ftCanBittiming.phaseSeg2= bt.phaseSeg2;
      pDrvCtrl->ftCanBittiming.samplePoint=bt.samplePoint;
      if(pDrvCtrl->isCanFD)
          {
          pDrvCtrl->ftCanDataBittiming.brp=dbt.brp;
          pDrvCtrl->ftCanDataBittiming.sjw=dbt.sjw;
          pDrvCtrl->ftCanDataBittiming.tq =dbt.tq;
          pDrvCtrl->ftCanDataBittiming.phaseSeg1= dbt.phaseSeg1;
          pDrvCtrl->ftCanDataBittiming.phaseSeg2= dbt.phaseSeg2;
          pDrvCtrl->ftCanDataBittiming.samplePoint=dbt.samplePoint;
          }
      return OK; 
     }

 /*****************************************************************************
*
* ftCanSetIDFilter -  set id filter
*
* This function Set set id filter for can
* 
* RETURNS: OK/ERROR
*
* ERRNO: N/A
*/
STATUS ftCanSetIDFilter
    (
    unsigned char ctlNo,
    UINT filterNo,
    UINT id,
    UINT mask,
    UINT type /*STANDARD_FRAME  0 
                EXTEND_FRAME    1*/
    )
    {
      VXB_DEVICE_ID pDev;
      UINT32 idRegOffset[4] = {CAN_ACC_ID0,  CAN_ACC_ID1,  CAN_ACC_ID2,  CAN_ACC_ID3};
      UINT32 maskRegOffset[4] = {CAN_ACC_ID0_MASK,  CAN_ACC_ID1_MASK,  CAN_ACC_ID2_MASK,  CAN_ACC_ID3_MASK} ;

      pDev= glftCanDev[ctlNo];

      CSR_CLRBIT_4(pDev, CAN_CTRL, CTRL_XFER);
      CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_RST);
      
      if(type == STANDARD_FRAME)
          {
          id = id << FTCAN_ACC_IDN_SHIFT;
          mask = mask << FTCAN_ACC_IDN_SHIFT;
          }
      CSR_WRITE_4(pDev, idRegOffset[filterNo],   id);
      CSR_WRITE_4(pDev, maskRegOffset[filterNo], mask);
      CSR_SETBIT_4(pDev, CAN_CTRL, CTRL_XFER | CTRL_AIME); 

      return OK;        
    }


#define FTCAN_DEBUG
#ifdef FTCAN_DEBUG
/*****************************************************************************
*
* testCanRecvCallback -  Test Receive Callback
*
* This function  is used to test the Receive Callback
* 
* RETURNS: N/A
*
* ERRNO: N/A
*/
void testCanRecvCallback(FTCANFD_FRAME* ftCanfdFrame)
    {
     static ULONG cnt = 1; 
     
     if(NULL == ftCanfdFrame)
        {
    	 printf("%s,%d NULL can frame, no recv\r\n", __FUNCTION__, __LINE__);
        return;
        }
     printf("%s,count:%d, recevied canid:%x, can_dlc:%d.\r\n", __FUNCTION__, cnt, ftCanfdFrame->canId & CAN_EFF_MASK,ftCanfdFrame->len);
     printf("%02x %02x %02x %02x %02x %02x \r\n",ftCanfdFrame->data[0],ftCanfdFrame->data[1],ftCanfdFrame->data[2],
    			 ftCanfdFrame->data[ftCanfdFrame->len - 3],ftCanfdFrame->data[ftCanfdFrame->len - 2],ftCanfdFrame->data[ftCanfdFrame->len - 1]);
     
     cnt++;
    }

/*****************************************************************************
*
* testCanRecv -  Test Receive Callback
*
* This function is used to test the Receive Callback
* 
* RETURNS: N/A
*
* ERRNO: N/A
*/
void testCanRecv(unsigned char ctlNo)
    {
    ftCanRecvCallback(ctlNo, testCanRecvCallback);
    }

/*****************************************************************************
*
* testCanSend -  TEST CAN
*
* This function  is used to test the transmitted data
* 
* RETURNS: N/A
*
* ERRNO: N/A
*/
STATUS testCanSend(int channel,int id, int dataLen, int loopCnt)
    {
	STATUS ret;
    FTCANFD_FRAME canfdFrame;
    int i = 0, j = 0;

    if(0 == loopCnt) 
        loopCnt=1;
    if(dataLen >= CANFD_MAX_DLEN) 
        dataLen = CANFD_MAX_DLEN;
 
    canfdFrame.canId = (id) | CAN_EFF_FLAG;
    canfdFrame.len = dataLen;
    canfdFrame.flags = CANFD_BRS | CANFD_ESI;
    for(j=0;j<dataLen;j++)
        {
        canfdFrame.data[j]=j;
        } 
    
    for(i=0;i<loopCnt;i++)
        {
    	ret = ftCanSend(channel,&canfdFrame);
    	if (ret == ERROR)
    	    {
    		printf("testCanSend error\n");
    		return ERROR;
    	    }
        }
    return OK;
    }

/*****************************************************************************
*
* dumpFtCanReg 
*
* This function  is used to print the value of the can register
*
* RETURNS: N/A
*
* ERRNO: N/A
*/
void dumpFtCanReg(unsigned char ctlNo)
    {
     VXB_DEVICE_ID pDev;
     int i =0;
     FTCAN_DRV_CTRL * pDrvCtrl;
     pDev= glftCanDev[ctlNo]; 
     pDrvCtrl=  pDev->pDrvCtrl;
     for ( i=0; i<0x54;)
         {
         printf("addr 0x%x value 0x%x\r\n",i,CSR_READ_4(pDev,i));
         i=i+4;
         }
    }
#endif /* FTCAN_DEBUG */

