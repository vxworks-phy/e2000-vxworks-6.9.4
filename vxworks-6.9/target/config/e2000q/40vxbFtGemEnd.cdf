/* 40vxbFtGemEnd.cdf - GEM configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
Component DRV_VXBEND_FTGEM {
    NAME            FT E2000 GEM Enhanced Network Driver
    SYNOPSIS        FT E2000 GEM Enhanced Network Driver
    _CHILDREN       FOLDER_DRIVERS
    _INIT_ORDER     hardWareInterFaceBusInit
    INIT_RTN        ftGemRegister();
    REQUIRES        INCLUDE_PLB_BUS   \
                    INCLUDE_PARAM_SYS \
                    INCLUDE_MII_BUS
    INIT_AFTER      INCLUDE_PLB_BUS
}
