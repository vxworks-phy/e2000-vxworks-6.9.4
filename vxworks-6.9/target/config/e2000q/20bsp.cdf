/* 20bsp.cdf - BSP component description file */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Bsp e2000q {
    NAME        board support package
    CPU         ARMARCH7
    ENDIAN      little
    MP_OPTIONS  SMP
    REQUIRES    INCLUDE_KERNEL      \
                DRV_ARM_GEN_SYS_TIMER   \
                DRV_ARM_GICV3
}

Component INCLUDE_SPY {
    REQUIRES    DRV_ARM_GEN_AUX_TIMER
}

Parameter RAM_HIGH_ADRS {
    NAME        Bootrom Copy region
    DEFAULT     (INCLUDE_BOOT_RAM_IMAGE)::(0x82500000)  \
                (INCLUDE_BOOT_APP)::(0x82000000)        \
                0x81000000
}

Parameter RAM_LOW_ADRS {
    NAME        Runtime kernel load address
    DEFAULT     (INCLUDE_BOOT_RAM_IMAGE)::(0x81a00000)  \
                (INCLUDE_BOOT_APP)::(0x81000000)        \
                0x80100000
}

Parameter VX_SMP_NUM_CPUS {
        NAME            Number of CPUs available to be enabled for VxWorks SMP
        TYPE            UINT
        DEFAULT         4
}


