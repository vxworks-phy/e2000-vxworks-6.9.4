/* vxbFtGptLib.c - vxBus Phytium General Purpose Timer library */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* includes */

#include <vxWorks.h>
#include <vxBusLib.h>
#include <vxbTimerLib.h>
#include <edrLib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <logLib.h>
#include "vxbFtGptLib.h"
#include "defs.h"

/* defines */

#define VXB_GPCLK_DEBUG
#ifdef VXB_GPCLK_DEBUG

LOCAL int vxbGpClkDebugLevel = 0;

#undef VXB_GPCLK_LOG
#define VXB_GPCLK_LOG(lvl,msg,a,b,c,d,e,f) \
    if ( vxbGpClkDebugLevel >= lvl ) printf(msg,a,b,c,d,e,f)
#else /* VXB_GPCLK_DEBUG */

#undef VXB_GPCLK_LOG
#define VXB_GPCLK_LOG(lvl,msg,a,b,c,d,e,f)

#endif /* VXB_GPCLK_DEBUG */

IMPORT struct  vxbTimerFunctionality * ftGptTimerFunctionalityGet(UINT32 timerId, VXB_DEVICE_ID *ppDev);
IMPORT struct  vxbTimerFunctionalityEx * ftGptTimerFunctionalityExGet(UINT32 timerId, VXB_DEVICE_ID *ppDev);

/* locals. only for cycle timer using it. */

LOCAL BOOL  vxbFtGpClkRunning[GPT_TOTAL_COUNT] = {FALSE,};


/*******************************************************************************
*
* vxbFtGptClkCycleConnect - connect a routine to the general purpose cycle clock interrupt
*
* This routine later specifies the interrupt service routine to be called at
* each general purpose cycle clock interrupt. It does not enable it.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
*/

STATUS vxbFtGptClkCycleConnect
    (
    UINT32 timerId,
    FUNCPTR routine,
    _Vx_usr_arg_t arg
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;

    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(1, "General Perpus Timer %d has no timer func!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }
    
    /* call the function to hook the ISR */

    if (pTimerFunc->timerISRSet (pDev,
                    (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_GPCLK_LOG(1, "%s,%d : not able to set ISR\n",__func__,__LINE__,3,4,5,6);
        return ERROR;
        }

    return (OK);

    }

/*******************************************************************************
*
* vxbFtGptClkCycleDisable - turn off general purpose cycle clock interrupts
*
* This routine disables general purpose cycle clock interrupts.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptClkCycleDisable (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;
    
    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(0, "General Perpus Timer %d is not initialized!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    /*
     * If the clock is enabled, then we can disable it.
     * If the clock is not enabled, then it means that is already disabled
     * and nothing needs to be done.
     */

    if (vxbFtGpClkRunning[timerId])
        {
        if ( pTimerFunc->timerDisable (pDev) != OK )
            {
            VXB_GPCLK_LOG(1, "%s : not able to disable timer\n",
                                                  __func__,2,3,4,5,6);
            return ERROR;
            }

        vxbFtGpClkRunning[timerId] = FALSE;
        }

    

    return OK;
    }

/*******************************************************************************
*
* vxbFtGptClkCycleEnable - turn on general purpose cycle clock interrupts
*
* This routine enables general purpose cycle clock interrupts.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptClkCycleEnable (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;
    
    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(1, "General Perpus Timer %d is not initialized!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    /* if Clock is not enabled, then enable it */

    if (!vxbFtGpClkRunning[timerId])
        {

        if (NULL != pTimerFunc->timerEnable)
            {
            if (pTimerFunc->timerEnable (pDev, 
                                           (pTimerFunc->clkFrequency / 
                                           pTimerFunc->ticksPerSecond)) != OK)
                {
                VXB_GPCLK_LOG(1, "%s,%d : not able to enable timer\n",
                              __func__,__LINE__,3,4,5,6); 
                return ERROR;
                }
            }

        if (NULL != pTimerFunc->timerEnable64)
            {

            if (pTimerFunc->timerEnable64 (pDev, 
                                           (pTimerFunc->clkFrequency / 
                                           pTimerFunc->ticksPerSecond)) != OK)
                {
                VXB_GPCLK_LOG(1, "%s,%d : not able to enable timer\n",
                              __func__,__LINE__,3,4,5,6); 
                return ERROR;
                }
            }
  
        vxbFtGpClkRunning[timerId] = TRUE;
        }

    

    return OK;
    }

/*******************************************************************************
*
* vxbFtGptClkCycleCountGet - get current timer counter value(32 or 64bit)
*
* This routine gets current timer counter value, 32 or 64bit.
*
* RETURNS: OK, or ERROR if fail
*
*/

UINT64 vxbFtGptClkCycleCountGet (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;
    UINT32 count32=0;
    UINT64 count64=0;
    
    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(1, "General Perpus Timer %d is not initialized!\r\n", timerId,2,3,4,5,6);
        return 0;
        }

    if (NULL != pTimerFunc->timerCountGet)
        {
        if ((pTimerFunc->timerCountGet (pDev,&count32)) != OK)
            {
            VXB_GPCLK_LOG(1, "%s,%d : not able to get count\n",
                          __func__,__LINE__,3,4,5,6); 
            return 0;
            }

            return (UINT64)count32;
        }

    if (NULL != pTimerFunc->timerCountGet64)
        {
        if ((pTimerFunc->timerCountGet64 (pDev,&count64)) != OK)
            {
            VXB_GPCLK_LOG(1, "%s,%d : not able to get count64\n",
                          __func__,__LINE__,3,4,5,6); 
            return 0;
            }

        return count64;
        }

    return 0;
    }

/*******************************************************************************
*
* ftGptClkRolloverGet - get timer rollover value(32 or 64bit)
*
* This routine gets timer rollover value, 32 or 64bit.
*
* RETURNS: OK, or ERROR if fail
*
*/

UINT64 vxbFtGptClkCycleRolloverGet(UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;
    UINT32 rollover32=0;
    UINT64 rollover64=0;
    
    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(1, "General Perpus Timer %d is not initialized!\r\n", timerId,2,3,4,5,6);
        return 0;
        }

    if (NULL != pTimerFunc->timerRolloverGet)
        {
        if ((pTimerFunc->timerRolloverGet (pDev,&rollover32)) != OK)
            {
            VXB_GPCLK_LOG(1, "%s,%d : not able to get rollover\n",
                          __func__,__LINE__,3,4,5,6); 
            return 0;
            }

            return (UINT64)rollover32;
        }

    if (NULL != pTimerFunc->timerRolloverGet64)
        {
        if ((pTimerFunc->timerRolloverGet64 (pDev,&rollover64)) != OK)
            {
            VXB_GPCLK_LOG(1, "%s,%d : not able to get rollover64\n",
                          __func__,__LINE__,3,4,5,6); 
            return 0;
            }

        return rollover64;
        }

    return 0;
    }

/*******************************************************************************
*
* vxbFtGptClkCycleRateGet - get the general purpose cycle clock rate
*
* This routine returns the interrupt rate of the general purpose cycle clock.
*
* RETURNS: The number of ticks per second of the general purpose cycle clock.
*
*/

int vxbFtGptClkCycleRateGet (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;

    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(1, "General Perpus Timer %d is not initialized!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    VXB_GPCLK_LOG(2, "%s : value %d\n",
                        __func__, pTimerFunc->ticksPerSecond,3,4,5,6);

    return (pTimerFunc->ticksPerSecond);
    }

/*******************************************************************************
*
* vxbFtGptClkCycleRateSet - set the general purpose cycle clock rate
*
* This routine sets the interrupt rate of the general purpose cycle clock.
* It does not enable general purpose cycle clock interrupts.
*
* RETURNS: OK, or ERROR if the tick rate is invalid or the timer cannot be set.
*
*/

STATUS vxbFtGptClkCycleRateSet
    (
    UINT32 timerId,
    int ticksPerSecond          /* number of clock interrupts per second */
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionality * pTimerFunc;

    pTimerFunc = ftGptTimerFunctionalityGet(timerId, &pDev);
    if (NULL == pTimerFunc)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no timer func!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    VXB_GPCLK_LOG(2, "%s : value %d\n",
                        __func__,ticksPerSecond,3,4,5,6);

    /* 
     * if the clk rate to be set does not fall within the limits,
     * return ERROR.
     */

    if ( ((UINT)ticksPerSecond < pTimerFunc->minFrequency) ||
         ((UINT)ticksPerSecond > pTimerFunc->maxFrequency) )
        {
        VXB_GPCLK_LOG(1, "%s : value %d not in [%d~%d]\r\n",__func__,ticksPerSecond,
            pTimerFunc->minFrequency,pTimerFunc->maxFrequency,5,6);

        return (ERROR);
        }

    /* if only the rate to be set is different, set the rate */

    if (pTimerFunc->ticksPerSecond != (UINT)ticksPerSecond)
        {
        pTimerFunc->ticksPerSecond = ticksPerSecond;

        if (vxbFtGpClkRunning[timerId])
            {
            vxbFtGptClkCycleDisable (timerId);
            vxbFtGptClkCycleEnable (timerId);
            }
        }

    return (OK);
    }

/*******************************************************************************
*
* vxbFtGptClkOnceConnect - connect a routine to the once time clock interrupt
*
* This routine later specifies the interrupt service routine to be called at
* once time clock interrupt. It does not enable it.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
*/

STATUS vxbFtGptClkOnceConnect
    (
    UINT32 timerId,
    FUNCPTR routine,
    _Vx_usr_arg_t arg
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    /* call the function to hook the ISR */

    if (pTimerFuncEx->timerOnceISRSet(pDev,
                   (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_GPCLK_LOG(1, "%s,%d : not able to set ISR\n",__func__,__LINE__,3,4,5,6);
        return ERROR;
        }

    return (OK);
    }

/*******************************************************************************
*
* vxbFtGptClkOnceTimeSet - set the general purpose clock once time
*
* This routine sets the interrupt once time of the general purpose clock.
* It does not enable general purpose clock interrupts.
*
* RETURNS: OK, or ERROR if the tick rate is invalid or the timer cannot be set.
*
*/

STATUS vxbFtGptClkOnceTimeSet
    (
    UINT32 timerId,
    int onceTimeMs          /* once time in milli-second */
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    pTimerFuncEx->cmpValOnceTimeMs = onceTimeMs;

    return (OK);
    }

/*******************************************************************************
*
* vxbFtGptClkOnceDisable - turn off general purpose clock interrupts
*
* This routine disables general purpose clock interrupts.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptClkOnceDisable (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    if ( pTimerFuncEx->timerOnceDisable(pDev) != OK )
        {
        VXB_GPCLK_LOG(1, "%s : not able to disable timer\n",
                                              __func__,2,3,4,5,6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* vxbFtGptClkOnceEnable - turn on general purpose clock interrupts
*
* This routine enables general purpose clock interrupts.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptClkOnceEnable (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    if (pTimerFuncEx->timerOnceEnable(pDev) != OK)
        {
            VXB_GPCLK_LOG(1, "%s,%d : not able to enable timer\n",
                          __func__,__LINE__,3,4,5,6); 
            return ERROR;
        }
  
    return OK;
    }

/*******************************************************************************
*
* vxbFtGptTachoOverConnect - connect a routine to the tacho over interrupt
*
* This routine later specifies the interrupt service routine to be called at
* tacho over interrupt. It does not enable it.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
*/

STATUS vxbFtGptTachoOverConnect
    (
    UINT32 timerId,
    FUNCPTR routine,
    _Vx_usr_arg_t arg
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    /* call the function to hook the ISR */

    if (pTimerFuncEx->timerTachoOverISRSet(pDev,
                   (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_GPCLK_LOG(1, "%s,%d : not able to set ISR\n",__func__,__LINE__,3,4,5,6);
        return ERROR;
        }

    return (OK);
    }


/*******************************************************************************
*
* vxbFtGptTachoUnderConnect - connect a routine to the tacho under interrupt
*
* This routine later specifies the interrupt service routine to be called at
* tacho under interrupt. It does not enable it.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
*/

STATUS vxbFtGptTachoUnderConnect
    (
    UINT32 timerId,
    FUNCPTR routine,
    _Vx_usr_arg_t arg
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
       {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
       }

    /* call the function to hook the ISR */

    if (pTimerFuncEx->timerTachoUnderISRSet(pDev,
                   (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_GPCLK_LOG(1, "%s,%d : not able to set ISR\n",__func__,__LINE__,3,4,5,6);
        return ERROR;
        }

    return (OK);
    }

/*******************************************************************************
*
* vxbFtGptTachoOverLimitSet - set tacho over limit
*
* This routine sets tacho over limit value.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptTachoOverLimitSet (UINT32 timerId, UINT32 overLimit)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    if ( pTimerFuncEx->timerTachoOverLimitSet(pDev, overLimit) != OK )
        {
        VXB_GPCLK_LOG(1, "%s : failed!\n",__func__,2,3,4,5,6);
        return ERROR;
        }

    return OK;
    }


/*******************************************************************************
*
* vxbFtGptTachoUnderLimitSet - set tacho under limit
*
* This routine sets tacho under limit value.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptTachoUnderLimitSet (UINT32 timerId, UINT32 underLimit)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    if ( pTimerFuncEx->timerTachoUnderLimitSet(pDev, underLimit) != OK )
        {
        VXB_GPCLK_LOG(1, "%s : failed!\n",__func__,2,3,4,5,6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* vxbFtGptTachoOverLimitGet - get tacho over limit
*
* This routine gets tacho over limit value.
*
* RETURNS: OK, or ERROR if fail
*
*/

UINT32 vxbFtGptTachoOverLimitGet (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    return pTimerFuncEx->timerTachoOverLimitGet(pDev);
    }

/*******************************************************************************
*
* vxbFtGptTachoUnderLimitGet - get tacho under limit
*
* This routine gets tacho under limit value.
*
* RETURNS: OK, or ERROR if fail
*
*/

UINT32 vxbFtGptTachoUnderLimitGet (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    return pTimerFuncEx->timerTachoUnderLimitGet(pDev);
    }

/*******************************************************************************
*
* vxbFtGptTachoDisable - turn off tacho
*
* This routine disables tacho.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptTachoDisable (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }


    if ( pTimerFuncEx->timerTachoDisable(pDev) != OK )
        {
        VXB_GPCLK_LOG(1, "%s : not able to disable timer\n",
                                              __func__,2,3,4,5,6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* vxbFtGptTachoEnable - turn on tacho
*
* This routine enables tacho.
*
* RETURNS: OK, or ERROR if fail
*
*/

STATUS vxbFtGptTachoEnable (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    if (pTimerFuncEx->timerTachoEnable(pDev) != OK)
        {
            VXB_GPCLK_LOG(1, "%s,%d : not able to enable timer\n",
                          __func__,__LINE__,3,4,5,6); 
            return ERROR;
        }
  
    return OK;
    }

/*******************************************************************************
*
* vxbFtGptTachoRPM - get tacho RPM
*
* This routine gets tacho RPM value.
*
* RETURNS: OK, or ERROR if fail
*
*/

UINT32 vxbFtGptTachoRPM (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    return pTimerFuncEx->timerTachoRPM(pDev);
    }
    

/*******************************************************************************
*
* vxbFtGptClkRollOverConnect - connect a routine to the roll over interrupt
*
* This routine later specifies the interrupt service routine to be called at
* roll over interrupt. It does not enable it.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
*/

STATUS vxbFtGptClkRollOverConnect
    (
    UINT32 timerId,
    FUNCPTR routine,
    _Vx_usr_arg_t arg
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    /* call the function to hook the ISR */

    if (pTimerFuncEx->timerRollOverISRSet(pDev,
                   (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_GPCLK_LOG(1, "%s,%d : not able to set ISR\n",__func__,__LINE__,3,4,5,6);
        return ERROR;
        }

    return (OK);
    }

/*******************************************************************************
*
* vxbFtGptCaptureConnect - connect a routine to the capture interrupt
*
* This routine later specifies the interrupt service routine to be called at
* capture interrupt. It does not enable it.
*
* RETURNS: OK, or ERROR if the routine cannot be connected to the interrupt.
*
*/

STATUS vxbFtGptCaptureConnect
    (
    UINT32 timerId,
    FUNCPTR routine,
    _Vx_usr_arg_t arg
    )
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    

    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    /* call the function to hook the ISR */

    if (pTimerFuncEx->timerCaptureISRSet(pDev,
                   (void(*)(_Vx_usr_arg_t))routine,
                   arg) != OK)
        {
        VXB_GPCLK_LOG(1, "%s,%d : not able to set ISR\n",__func__,__LINE__,3,4,5,6);
        return ERROR;
        }

    return (OK);
    }


/*******************************************************************************
*
* vxbFtGptCaptureCnt - get capture count value in capture mode
*
* This routine gets capture count value.
*
* RETURNS: OK, or ERROR if fail
*
*/

UINT32 vxbFtGptCaptureCnt (UINT32 timerId)
    {
    VXB_DEVICE_ID    pDev;
	struct  vxbTimerFunctionalityEx * pTimerFuncEx;
    
    pTimerFuncEx = ftGptTimerFunctionalityExGet(timerId, &pDev);
    if (NULL == pTimerFuncEx)
        {
        VXB_GPCLK_LOG(1,"General Perpus Timer %d has no funcEx!\r\n", timerId,2,3,4,5,6);
        return ERROR;
        }

    return pTimerFuncEx->timerCaptureCntGet(pDev);
    }


#define TEST_FTGP_CLK
#ifdef TEST_FTGP_CLK

UINT32 debugGptCycClkIntCount=0;
UINT32 debugGptCycClkRate=10;
UINT32 debugGptOnceClkIntCount=0;

LOCAL void testGptClkInt (UINT32 timerId)
    {
    if((debugGptCycClkIntCount % debugGptCycClkRate) == 0)
        {
         logMsg("##### Enter General Timer %d interrupt count %d......\r\n", timerId, debugGptCycClkIntCount,3,4,5,6);
        }
    debugGptCycClkIntCount++;
    }
STATUS testGptClkStart(UINT32 timerId, int rate)
    {
    STATUS ret;
    
    if (timerId>=GPT_TOTAL_COUNT)
        {
        printf("General Perpus Timer %d is invalid.\r\n", timerId);
        return ERROR;
        }


    if (vxbFtGptClkCycleConnect (timerId, (FUNCPTR)testGptClkInt, (_Vx_usr_arg_t)timerId) != OK)
        {

        printf ("Line %d  Failed!\r\n", __LINE__);
        return (ERROR);
        }

    if (0 != rate)
        {
        debugGptCycClkRate = rate;
        }
    
    ret = vxbFtGptClkCycleRateSet (timerId, debugGptCycClkRate);
    if (ret != OK)
        {
        printf ("Line %d  Failed to set clock rate %d!\r\n", __LINE__, debugGptCycClkRate);
        return (ERROR);
        }
      
    if (vxbFtGptClkCycleEnable (timerId) != OK)
        {
        printf ("Line %d  Failed!\r\n", __LINE__);
        return (ERROR);
        }
    else
        {
        printf ("start clock  successfully.\r\n");
        }

  return OK;
}

STATUS testGptClkStop(UINT32 timerId)
    {
  
    if (timerId>=GPT_TOTAL_COUNT)
        {
        printf("General Perpus Timer %d is invalid.\r\n", timerId);
        return ERROR;
        }
    if (vxbFtGptClkCycleDisable(timerId) != OK)
        {
        printf ("Line %d  Failed!\r\n", __LINE__);
        return (ERROR);
        }
    else
        {
        printf ("stop clock  successfully.\r\n");
        }
    
  return OK;
}

STATUS testTimerCycStart(UINT32 timerId)
    {
    return testGptClkStart(timerId, 0);
    }

STATUS testTimerCycStop(UINT32 timerId)
    {
    return testGptClkStop(timerId);
    }

LOCAL void testGptClkOnceInt (UINT32 timerId)
    {
    logMsg("##### Enter General Timer %d interrupt only once......\r\n", timerId,2,3,4,5,6);
    debugGptOnceClkIntCount++;
    }

UINT32 testTimerOnce(UINT32 timerId, UINT32 ms)
    {
    STATUS st;

    st = vxbFtGptClkOnceConnect(timerId, testGptClkOnceInt, timerId);
    if (ERROR == st)
        {
        printf("L%d:Error: clock once connect \r\n",__LINE__);
        return ERROR;
        }

    st = vxbFtGptClkOnceTimeSet(timerId, ms);
    if (ERROR == st)
        {
        printf("L%d:Error: clock once time set \r\n",__LINE__);
        return ERROR;
        }

    st = vxbFtGptClkOnceEnable(timerId);
    if (ERROR == st)
        {
        printf("L%d:Error: clock once enable \r\n",__LINE__);
        return ERROR;
        }

    taskDelay((ms/1000+1)*sysClkRateGet());

    st = vxbFtGptClkOnceDisable(timerId);
    if (ERROR == st)
        {
        printf("L%d:Error: clock once disable \r\n",__LINE__);
        return ERROR;
        }
    
    return OK;
}

LOCAL void testTachoOverISR(UINT32 timerId)
    {
    logMsg("##Tacho-%d over ISR handler...\r\n", timerId,2,3,4,5,6);
    }
LOCAL void testTachoUnderISR(UINT32 timerId)
    {
    logMsg("##Tacho-%d under ISR handler...\r\n", timerId,2,3,4,5,6);
    }

UINT32 testTachoOverUnderStart(UINT32 timerId)
{
    STATUS st;

    st = vxbFtGptTachoOverConnect(timerId, testTachoOverISR, (_Vx_usr_arg_t) timerId );
    if (ERROR == st)
        {
        printf("L%d:Error: tacho over connect \r\n",__LINE__);
        return ERROR;
        }

    st = vxbFtGptTachoUnderConnect(timerId, testTachoUnderISR, (_Vx_usr_arg_t) timerId );
    if (ERROR == st)
        {
        printf("L%d:Error: tacho under connect \r\n",__LINE__);
        return ERROR;
        }

    st = vxbFtGptTachoEnable (timerId);
    if (ERROR == st)
        {
        printf("L%d:Error: tacho enable \r\n",__LINE__);
        return ERROR;
        }

    return OK;
    
}

UINT32 testRpm(UINT32 timerId)
    {
    return vxbFtGptTachoRPM(timerId);
    }

UINT32 testRpm2(void)
{
    UINT32 rawDat;
    UINT32 Rpm;
    UINT32 cntNum;
    UINT64 tmp;
    int rate;

    cntNum = 0xf4240; /* tacho mode: CMPL is pulse_number */

    rawDat = 0x12e; 
    if (0 == rawDat)
        {
        Rpm = 0;
        }
    else
        {
        /* calculate rpm */
        tmp = ((UINT64)CLK_50MHZ * 60 * (UINT64)rawDat);
        Rpm = (UINT32)(tmp / (2 * (cntNum + 1)));
        }

    return Rpm;
}

/*
50,000,000 * 60 * 0x12e = 906,000,000,000 
                        =  0xD2 F1CE E400

(2 * (cntNum + 1)) = (2 * (0xf4240 + 1))= 0x1E8482 = 2 000 002;
*/


UINT32 testRpm3(void)
{
    UINT32 loopCnt;
    UINT32 rawDat;
    UINT32 Rpm;
    UINT32 cntNum;
    UINT64 tmp;
    int rate;

    cntNum = 0xf4240; /* tacho mode: CMPL is pulse_number */

    rawDat = 0x14; 
    if (0 == rawDat)
        {
        Rpm = 0;
        }
    else
        {
        /* calculate rpm */
        tmp = ((UINT64)CLK_50MHZ * 60 * (UINT64)rawDat);
        Rpm = (UINT32)(tmp / (2 * (cntNum + 1)));
        }

    return Rpm;
}


#endif /* TEST_FTGP_CLK */
