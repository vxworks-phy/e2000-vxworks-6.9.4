/* vxbFtGptLib.h - API for Phytium General Purpose Timer library */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCvxbFtGptLibh
#define __INCvxbFtGptLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <hwif/vxbus/vxBus.h>
#include <vxBusLib.h>

#define GPT_TOTAL_COUNT 38

/* Extension timer func struc*/
struct vxbTimerFunctionalityEx
    {

    /* --------------- Once Time ------------- */
    
    UINT32              cmpValOnceTimeMs; /* in milli-second */

    STATUS (*timerOnceISRSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        void   (*pIsr)(_Vx_usr_arg_t),	/* IN */
        _Vx_usr_arg_t	arg		/* IN */
        );
        /* disable the timer */
 
    STATUS (*timerOnceDisable)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        ); 

    /* enable the timer */

    STATUS (*timerOnceEnable)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        ); 

    /* --------------- tacho ------------- */
    
    STATUS (*timerTachoOverISRSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        void   (*pIsr)(_Vx_usr_arg_t),	/* IN */
        _Vx_usr_arg_t	arg		/* IN */
        );

    STATUS (*timerTachoUnderISRSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        void   (*pIsr)(_Vx_usr_arg_t),	/* IN */
        _Vx_usr_arg_t	arg		/* IN */
        );
        
    STATUS (*timerTachoOverLimitSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        UINT32 overLimit		/* IN */
        );
    STATUS (*timerTachoUnderLimitSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        UINT32 underLimit		/* IN */
        );
    UINT32 (*timerTachoOverLimitGet)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        );
    UINT32 (*timerTachoUnderLimitGet)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        );
    UINT32 (*timerTachoRPM)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        );
        
        
        /* disable the timer */
 
    STATUS (*timerTachoDisable)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        ); 

    /* enable the timer */

    STATUS (*timerTachoEnable)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        ); 
        
    /* --------------- rollover ------------- */
    
    STATUS (*timerRollOverISRSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        void   (*pIsr)(_Vx_usr_arg_t),	/* IN */
        _Vx_usr_arg_t	arg		/* IN */
        );

    /* --------------- capture ------------- */
    
    STATUS (*timerCaptureISRSet)
        (
        VXB_DEVICE_ID       pDev,		/* IN */
        void   (*pIsr)(_Vx_usr_arg_t),	/* IN */
        _Vx_usr_arg_t	arg		/* IN */
        );
    UINT32 (*timerCaptureCntGet)
        (
        VXB_DEVICE_ID       pDev		/* IN */
        );
 
    };

/* cycle clock APIs */

STATUS vxbFtGptClkCycleConnect(UINT32 timerId,FUNCPTR routine,_Vx_usr_arg_t arg);
STATUS vxbFtGptClkCycleDisable (UINT32 timerId);
STATUS vxbFtGptClkCycleEnable (UINT32 timerId);
int vxbFtGptClkCycleRateGet (UINT32 timerId);
STATUS vxbFtGptClkCycleRateSet(UINT32 timerId,int ticksPerSecond);
UINT64 vxbFtGptClkCycleCountGet (UINT32 timerId);
UINT64 vxbFtGptClkCycleRolloverGet(UINT32 timerId);

/* one time clock APIs */

STATUS vxbFtGptClkOnceConnect(UINT32 timerId,FUNCPTR routine,_Vx_usr_arg_t arg);
STATUS vxbFtGptClkOnceTimeSet(UINT32 timerId,int onceTimeMs);
STATUS vxbFtGptClkOnceDisable (UINT32 timerId);
STATUS vxbFtGptClkOnceEnable (UINT32 timerId);

/* tacho/capture clock APIs */

STATUS vxbFtGptTachoOverConnect(UINT32 timerId,FUNCPTR routine,_Vx_usr_arg_t arg);
STATUS vxbFtGptTachoUnderConnect(UINT32 timerId,FUNCPTR routine,_Vx_usr_arg_t arg);
STATUS vxbFtGptTachoOverLimitSet (UINT32 timerId, UINT32 overLimit);
STATUS vxbFtGptTachoUnderLimitSet (UINT32 timerId, UINT32 underLimit);
UINT32 vxbFtGptTachoOverLimitGet (UINT32 timerId);
UINT32 vxbFtGptTachoUnderLimitGet (UINT32 timerId);
STATUS vxbFtGptTachoDisable (UINT32 timerId);
STATUS vxbFtGptTachoEnable (UINT32 timerId);
UINT32 vxbFtGptTachoRPM (UINT32 timerId);

STATUS vxbFtGptClkRollOverConnect(UINT32 timerId,FUNCPTR routine,_Vx_usr_arg_t arg);
STATUS vxbFtGptCaptureConnect(UINT32 timerId,FUNCPTR routine, _Vx_usr_arg_t arg);
UINT32 vxbFtGptCaptureCnt (UINT32 timerId);

#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbFtGptLibh */
