/* config.h - ARM Phytium configuration header */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCconfigh
#define __INCconfigh

#ifdef __cplusplus
extern "C" {
#endif

/* BSP version/revision identification, before configAll.h */

#define BSP_VERSION     "1.0"
#define BSP_REV         "/0"

#include <vsbConfig.h>
#include <configAll.h>

#define SYS_MODEL "E2000Q BOARD"

#define INCLUDE_VXBUS
#ifdef  INCLUDE_VXBUS
#define INCLUDE_VXBUS_SHOW
#define INCLUDE_HWMEM_ALLOC
#define INCLUDE_PLB_BUS
#define DRV_ARM_GICV3
#define DRV_SIO_PRIMECELL /* UART */
#define DRV_ARM_GEN_SYS_TIMER
#define INCLUDE_SYSCLK_INIT
#define INCLUDE_AUX_CLK
#define DRV_ARM_GEN_AUX_TIMER
#define INCLUDE_SPY
#define INCLUDE_PARAM_SYS
#define DRV_PCIBUS_FT
#define DRV_FTI2C
#undef DRV_FTQSPI
#undef DRV_SPIFLASH_SP25
#undef DRV_TIMER_FT_GPT
#undef DRV_FTSPI
#undef DRV_FTWDT
#undef DRV_FTGPIO
#undef DRV_FTGDMA
#undef DRV_FTPWM

#undef DRV_FTSD
/*If a SD/MMC-0 device exist, define this macro, otherwise, undef it*/
# define CONFIG_FT_SD0
/*If a SD/MMC-1 device exist, define this macro, otherwise, undef it*/
# define CONFIG_FT_SD1
/*SD/MMC clock pin delay maybe adjust by different board*/
# define CONFIG_FT_SD0_CLKPIN_DELAY
# define CONFIG_FT_SD1_CLKPIN_DELAY 

#undef DRV_FTCAN
#undef DRV_FTSCMI

/* ARM PrimeCell SIO for VxBus */
#define INCLUDE_SIO_UTILS

#define DRV_VXBEND_FTGEM
/* 
 *  Because DEFAULT_BOOT_LINE only choose one NIC name initialized automatically,
 *  shell command: ifconfig, may not find other NIC names. If so, you can use ipAttach()
 *  to attach stack. For example:
 *  -> ipAttach(1, "gem")
 *  -> ipAttach(2, "gem")
 *  -> ipAttach(3, "gem")
 *  -> ifconfig("gem1 192.168.1.1  up")
 *  -> ifconfig("gem2 192.168.2.1  up")
 *  -> ifconfig("gem3 192.168.3.1  up")
 *
 *  If gemX (X:0,1,2,3) is used by WDB debugger, you must set correct DEFAULT_BOOT_LINE value
 *  for this WDB. Because WDB can not use IP configured/modified at running time.
 *  
 */
# define CONFIG_FT_GEM0
# undef CONFIG_FT_GEM1
# undef CONFIG_FT_GEM2
# undef CONFIG_FT_GEM3

#undef INCLUDE_USB
#ifdef INCLUDE_USB
#define INCLUDE_USB_INIT
#define INCLUDE_USB_XHCI_HCD
#define INCLUDE_USB_XHCI_HCD_INIT
#define INCLUDE_USB_GEN2_STORAGE
#define INCLUDE_USB_GEN2_STORAGE_INIT
#endif

#define  INCLUDE_DRV_STORAGE_AHCI
#if defined(INCLUDE_USB) || defined(INCLUDE_TFFS) || \
  defined(INCLUDE_DRV_STORAGE_PIIX) || defined(INCLUDE_DRV_STORAGE_AHCI)
    #define INCLUDE_DOSFS
    #define INCLUDE_DOSFS_MAIN
    #define INCLUDE_DOSFS_CHKDSK
    #define INCLUDE_DOSFS_FMT
    #define INCLUDE_DOSFS_FAT
    #define INCLUDE_DOSFS_SHOW
    #define INCLUDE_DOSFS_DIR_VFAT
    #define INCLUDE_DOSFS_DIR_FIXED
    #define INCLUDE_FS_MONITOR
    #define INCLUDE_FS_EVENT_UTIL
    #define INCLUDE_ERF
    #define INCLUDE_XBD
    #define INCLUDE_XBD_BLKDEV
    #define INCLUDE_XBD_TRANS
    #define INCLUDE_DEVICE_MANAGER
    #define INCLUDE_XBD_BLK_DEV
    #define INCLUDE_XBD_PART_LIB
    #define INCLUDE_DISK_UTIL
#endif


#ifdef DRV_PCIBUS_FT
#   define INCLUDE_PCI_BUS
#   define INCLUDE_PCI_BUS_AUTOCONF
#   define INCLUDE_PCI_BUS_SHOW
#   define INCLUDE_PCI_OLD_CONFIG_ROUTINES
#endif  /* DRV_PCIBUS_FT */

#if defined(INCLUDE_GEI825XX_VXB_END) || defined(DRV_VXBEND_FTGEM)
#   define INCLUDE_GENERICPHY
#   define INCLUDE_MII_BUS
#   define INCLUDE_IFCONFIG      /* old coreip stack ifconfig command line/API */
#   define INCLUDE_PING      /* old coreip stack ping client */
#   define INCLUDE_IPATTACH
#   define INCLUDE_ISR_SHOW
#   define INCLUDE_WATCHDOGS
#   define INCLUDE_NETWORK
#   define INCLUDE_NET_INIT
#   define INCLUDE_BOOT_LINE_INIT
#endif

#define INCLUDE_INTCTLR_LIB
#define INCLUDE_VXB_LEGACY_INTERRUPTS
#define INCLUDE_TIMER_SYS
#define INCLUDE_TIMESTAMP

#define HWMEM_POOL_SIZE 500000
#define INCLUDE_VXB_CMDLINE
#endif  /* INCLUDE_VXBUS */

#define FORCE_DEFAULT_BOOT_LINE

#define DEFAULT_BOOT_LINE \
    "gem(0,0) host:/vxWorks " \
    "h=192.168.4.162 e=192.168.4.127:ffffff00 u=target pw=target tn=target"


/* Memory configuration */

#define USER_RESERVED_MEM   0       /* see sysMemTop() */


/*
 * bootapp speed ups.
 */

#define BSP_COPY_LONGS bcopyLongs
#define BSP_FILL_LONGS bfillLongs
#define INCLUDE_BOOT_D_CACHE_ENABLE          /* Enable Data Cache for bootrom */

/*
 * Local-to-Bus memory address constants:
 * the local memory address always appears at 0 locally;
 * it is not dual ported.
 */

#define LOCAL_MEM_LOCAL_ADRS  0x80000000
#define LOCAL_MEM_BUS_ADRS    0x80000000
#define LOCAL_MEM_SIZE        0x70000000
#define LOCAL_MEM_END_ADRS    (LOCAL_MEM_LOCAL_ADRS + LOCAL_MEM_SIZE)

#define ROM_BASE_ADRS       0x40000000     /* base of NOR Flash/EPROM */
#define ROM_TEXT_ADRS       0x44040000     /* code start addr in ROM */
#define ROM_SIZE            0x00100000     /* size of ROM holding VxWorks*/

#define ROM_COPY_SIZE       ROM_SIZE
#define ROM_SIZE_TOTAL      0x04000000     /* total size of ROM */

/* Flash memory configuration */
#define SZ_4M                       (0x00400000)
#define SZ_8M                       (0x00800000)
#define SZ_32M                      (0x02000000)
#define SZ_64K                      (0x00010000)

#define QSPI_FLASH_DEVICE_NAME       "spiFlash_gd25lq32es"
#define QSPI_FLASH_SIZE              (SZ_4M)
#define QSPI_FLASH_SECTOR_SIZE       (SZ_64K)     
#define QSPI_FLASH_PAGE_SIZE         (0x100)     
#define QSPI_FLASH_BASE_ADRS         0
#define QSPI_FLASH_SECTOR_NUM        ((QSPI_FLASH_SIZE) / (QSPI_FLASH_SECTOR_SIZE))
#define QSPI_BOOTROM_SIZE            (0x300000)

#define SPI_FLASH_DEVICE_NAME       "spiFlash_gd25lq32es"
#define SPI_FLASH_SIZE              (SZ_4M)
#define SPI_FLASH_SECTOR_SIZE       (SZ_64K)     
#define SPI_FLASH_PAGE_SIZE         (0x100)     
#define SPI_FLASH_BASE_ADRS         0
#define SPI_FLASH_SECTOR_NUM        ((SPI_FLASH_SIZE) / (SPI_FLASH_SECTOR_SIZE))


#define INCLUDE_TFFS              

#ifdef INCLUDE_TFFS
#   define INCLUDE_TFFS_MOUNT
#   define INCLUDE_TFFS_SHOW

/* TFFS stub to vxBus Flash interface */

#   define INCLUDE_TFFS_STUB_VXBFLASH
#   define TFFS_FORMAT_PRINT            /* print the tffs format process */

/* TrueFFS partition 0 --- QSPI CS0*/

#   define TFFS_PART0_NAME              "RFA0"
#   define TFFS_PART0_FLASH_NAME        QSPI_FLASH_DEVICE_NAME
#   define TFFS_PART0_FLASH_UNIT        0
#   define TFFS_PART0_FLASH_BASE_ADRS   QSPI_FLASH_BASE_ADRS
#   define TFFS_PART0_FLASH_OFFSET      0               
#   define TFFS_PART0_FLASH_SIZE        QSPI_FLASH_SIZE      
#   define TFFS_PART0_FLASH_BOOT_SIZE   QSPI_BOOTROM_SIZE     

/* TrueFFS partition 1  --- SPI0 CS2*/

#   define TFFS_PART1_NAME              "RFA1"
#   define TFFS_PART1_FLASH_NAME        SPI_FLASH_DEVICE_NAME
#   define TFFS_PART1_FLASH_UNIT        1
#   define TFFS_PART1_FLASH_BASE_ADRS   SPI_FLASH_BASE_ADRS
#   define TFFS_PART1_FLASH_OFFSET      0               
#   define TFFS_PART1_FLASH_SIZE        SPI_FLASH_SIZE      
#   define TFFS_PART1_FLASH_BOOT_SIZE   0    

#endif /* INCLUDE_TFFS */

/* clock rate configuration*/

#define SYS_CLK_RATE_MIN    10
#define SYS_CLK_RATE_MAX    15000
#define AUX_CLK_RATE_MIN    10
#define AUX_CLK_RATE_MAX    15000


/* Serial port configuration */

/* If CONFIG_MIO_UART is defined, NUM_TTY should be set (max uart +1) */

#undef  CONFIG_MIO_UART

#undef  NUM_TTY
#define NUM_TTY           4

#define DEFAULT_BAUD      115200
#undef CONSOLE_BAUD_RATE
#define CONSOLE_BAUD_RATE 115200

#undef CONSOLE_TTY
#define CONSOLE_TTY 1
#undef INCLUDE_WDB

#undef  USER_I_CACHE_MODE
#define USER_I_CACHE_MODE   (CACHE_COPYBACK)

#undef  USER_D_CACHE_MODE
#define USER_D_CACHE_MODE   (CACHE_COPYBACK)


/*
 * Include MMU BASIC and CACHE support for command line and project builds
 */

#   define INCLUDE_MMU_BASIC
#   define INCLUDE_MMU_FULL
#   define INCLUDE_CACHE_SUPPORT


/*
 * Vector Floating Point Support
 */

#define INCLUDE_VFP


/*
 * miscellaneous definitions
 * Note: ISR_STACK_SIZE is defined here rather than in ../all/configAll.h
 * (as is more usual) because the stack size depends on the interrupt
 * structure of the BSP.
 */

#define ISR_STACK_SIZE  0x2000  /* size of ISR stack, in bytes */
#define INCLUDE_PCI_OLD_CONFIG_ROUTINES


#define INCLUDE_SYS_HW_INIT_0
#define SYS_HW_INIT_0()         (sysHwInit0())

#ifdef _WRS_CONFIG_SMP
#   define INCLUDE_VXIPI
#endif /* _WRS_CONFIG_SMP */

#define INCLUDE_SHELL
#define INCLUDE_SYM_TBL
#define INCLUDE_SYM_TBL_INIT
/*#define INCLUDE_STANDALONE_SYM_TBL*/

#define INCLUDE_ISR_OBJECTS
#define INCLUDE_ISR_SHOW

#ifdef __cplusplus
}
#endif
#endif  /* __INCconfigh */
#if defined(PRJ_BUILD)
#include "prjParams.h"
#endif /* PRJ_BUILD */

