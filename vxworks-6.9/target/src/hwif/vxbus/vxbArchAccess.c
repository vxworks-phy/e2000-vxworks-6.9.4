/* vxbArchAccess.c - vxBus access routines specific to an architecture  */

/*
 * Copyright (C) 2005-2009, 2012 Wind River Systems, Inc. 
 * 
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01t,15feb12,wyt  add special MMU attributes handling in vxbRegBarMap() 
                 for Intel 64. (WIND00266514)
01s,07dec09,h_k  added vxbRegUnmap() and vxbRegUnmapAll().
                 changed the default MMU attributes for pentium and PPC.
                 corrected a return value from vxbRegMap().
                 added more descriptions in the header for the vxbRegMap().
01r,10sep09,h_k  fix for BUS specific MMU attributes get.
01q,04sep09,h_k  removed ADR_SPACE_OPT_OPTIMIZED conditions.
01p,26aug09,h_k  added vxbRegBarMap().
01o,23feb09,h_k  updated for LP64 support.
01n,24oct08,h_k  fixed _archRegisterWrite64() for LE order. (CQ:89805)
01m,04sep07,pdg  corrected apigen errors
01l,12jun07,tor  replace VIRT_ADDR
01k,04may07,wap  Rearchitect and clean up
01j,06feb06,mdo  Change vxMemArchProbe call to vxMemProbe.
01i,16nov05,pdg  Fix for incorrect flag values in
                 registerReadXX/registerWriteXX
01h,07nov05,mdo  Fix several routine titles for apigen
01g,29sep05,mdo  Fix gnu warnings
01f,20sep05,pdg  Fix for vxbus access routines errors (SPR #112197)
01e,14sep05,jb  Allow IO space optimization
01d,02sep05,pdg  vxIOArchProbe () function added
01d,01sep05,mdo  Add vxb prefix
01c,10aug05,mdo  Phase in new access method
01b,01aug05,mdo Updating bus access methods
01a,21Jul05,gpd written
*/

/*
DESCRIPTION
This library contains the support routines for architecture
dependent access on the vxBus.

INCLUDE FILES: vxbAccess.h vxbArchAccess.h
*/

/* includes */
#include <vxWorks.h>
#include <cacheLib.h>
#include <logLib.h>
#include <vxLib.h>
#include <hwif/vxbus/vxBus.h>
#include "../h/vxbus/vxbAccess.h"
#include "../h/vxbus/vxbArchAccess.h"

#ifdef	_WRS_CONFIG_LP64
#include <private/adrSpaceLibP.h>
#include <private/vmLibP.h>

#ifndef	MMU_ATTR_IO_MASK
#define MMU_ATTR_IO_MASK	(MMU_ATTR_VALID_MSK	| \
				 MMU_ATTR_PROT_MSK	| \
				 MMU_ATTR_CACHE_MSK)
#endif	/* !MMU_ATTR_IO_MASK */

#ifndef	MMU_ATTR_IO_DEFAULT
# if (CPU_FAMILY == PPC)
# define MMU_ATTR_IO_DEFAULT	(MMU_ATTR_VALID		| \
				 MMU_ATTR_SUP_RW	| \
				 MMU_ATTR_CACHE_OFF	| \
				 MMU_ATTR_CACHE_GUARDED
# else
# define MMU_ATTR_IO_DEFAULT	(MMU_ATTR_VALID		| \
				 MMU_ATTR_SUP_RW	| \
				 MMU_ATTR_CACHE_OFF)
# endif	/* CPU_FAMILY == PPC */
#endif	/* !MMU_ATTR_IO_DEFAULT */

#endif	/* _WRS_CONFIG_LP64 */

#ifdef VXB_LEGACY_ACCESS

#ifdef  VXB_ACCESS_DEBUG

#define VXB_ACCESS_ARCH_LOG_ERROR(fmt,a,b,c,d,e,f)  logMsg(fmt,a,b,c,d,e,f)

#else

#define VXB_ACCESS_ARCH_LOG_ERROR(fmt,a,b,c,d,e,f)

#endif

/* defines */
/******************************************************************************
*
* _archRegProbe - probe a register on the device
*
* This routine is used by the driver to identify whether the device register
* is present and is available.
*
* RETURNS: OK on success, or ERROR if register is not present or unavailable
*
* ERRNO: N/A
*/

STATUS  _archRegProbe
    (
    VXB_DEVICE_ID   pDevInfo,        /* device info */
    void    *       pRegBase,        /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset,      /* offset, in bytes, of register */
    UINT32          transactionSize, /* register size */
    char *          pProbeDatum,     /* value to write */
    char *          pRetVal,         /* value read back */
    UINT32 *        pFlags           /* Flags used */
    )
    {

    /* check the validity of the parameters */

    if ((pDevInfo == NULL)    || (transactionSize == 0) ||
        (pProbeDatum == NULL) || (pRetVal == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archAddressProbe: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /* if the byte order is already known, do not perform any conversion */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0)
        {
        /* based on the transactionSize, perform the conversion */

        switch (transactionSize)
            {
#if  (_BYTE_ORDER == _BIG_ENDIAN)
            case    2:
                VXB_BYTE_ORDER_CONVERT_16(*pFlags,
                                          (*((UINT16 *)pProbeDatum)),
                                          VXBAF_BYTE_ORDER_BE_16);
                break;

            case    4:
                VXB_BYTE_ORDER_CONVERT_32(*pFlags,
                                          (*((UINT32 *)pProbeDatum)),
                                          VXBAF_BYTE_ORDER_BE_32);
                break;
            case    8:
                VXB_BYTE_ORDER_CONVERT_64(*pFlags,
                                          (*((UINT64 *) pProbeDatum)),
                                          VXBAF_BYTE_ORDER_BE_64);
                break;

#else
            case    2:
                VXB_BYTE_ORDER_CONVERT_16(*pFlags,
                                          (*((UINT16 *)pProbeDatum)),
                                          VXBAF_BYTE_ORDER_LE_16);
                break;

            case    4:
                VXB_BYTE_ORDER_CONVERT_32(*pFlags,
                                          (*((UINT32 *)pProbeDatum)),
                                          VXBAF_BYTE_ORDER_LE_32);
                break;
            case    8:
                VXB_BYTE_ORDER_CONVERT_64(*pFlags,
                                          (*((UINT64 *) pProbeDatum)),
                                          VXBAF_BYTE_ORDER_LE_64);
                break;
#endif
            default:
                return ERROR;

            }
        }

    /* Call the function to perform the register probe */
    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        /* call the probe function to write to the register */

        if (vxIOArchProbe ((char *)((UINT32)pRegBase + byteOffset),
                            VX_WRITE,
                            transactionSize,
                            (char *)pProbeDatum) != OK)
            {
            VXB_ACCESS_ARCH_LOG_ERROR("_archAddressProbe: vxIOArchProbe \
                 on VX_WRITE returned error\n", 0,0,0,0,0,0);
            return ERROR;
            }

        /* call the probe function to read from the register */

        if (vxIOArchProbe ((char *)((UINT32)pRegBase + byteOffset),
                            VX_READ,
                            transactionSize,
                            (char *)pRetVal) != OK)
            {
            VXB_ACCESS_ARCH_LOG_ERROR("_archAddressProbe: vxIOArchProbe \
                 on VX_READ returned error\n", 0,0,0,0,0,0);
            return ERROR;
            }

        }
    else
        {

        /* call the probe function to write to the register */

        if (vxMemProbe ((char *)((UINT32)pRegBase + byteOffset),
                            VX_WRITE,
                            transactionSize,
                            (char *)pProbeDatum) != OK)
            {
            VXB_ACCESS_ARCH_LOG_ERROR("_archAddressProbe: vxMemProbe \
                 on VX_WRITE returned error\n", 0,0,0,0,0,0);
            return ERROR;
            }

        /* call the probe function to read from the register */

        if (vxMemProbe ((char *)((UINT32)pRegBase + byteOffset),
                            VX_READ,
                            transactionSize,
                            (char *)pRetVal) != OK)
            {
            VXB_ACCESS_ARCH_LOG_ERROR("_archAddressProbe: vxMemProbe \
                 on VX_READ returned error\n", 0,0,0,0,0,0);
            return ERROR;
            }

        }

    /* store the IO operation flag & byte order known flag */
    *pFlags &= (VXBAF_BYTE_ORDER_KNOWN | VXBAF_IO_SPACE_OPERATION);

    /* if the byte order is already known, do not perform any conversion */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0)
        {
        /* based on the transactionSize, perform the conversion */

        switch (transactionSize)
            {
#if  (_BYTE_ORDER == _BIG_ENDIAN)
            case    2:
                *pFlags |= VXBAF_BYTE_ORDER_BE_16;
                break;

            case    4:
                *pFlags |= VXBAF_BYTE_ORDER_BE_32;
                break;
            case    8:
                *pFlags |= VXBAF_BYTE_ORDER_BE_64;
                break;
#else
            case    2:
                *pFlags |= VXBAF_BYTE_ORDER_LE_16;
                break;

            case    4:
                *pFlags |= VXBAF_BYTE_ORDER_LE_32;
                break;
            case    8:
                *pFlags |= VXBAF_BYTE_ORDER_LE_64;
                break;
#endif
            default:
                return ERROR;

            }
        }

    /* return the status */

    return OK;
    }

/******************************************************************************
*
* _archRegisterRead8 - read 8-bit value from a register
*
* This routine is used by the driver to read 8-bits from a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterRead8
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterRead8: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        *pDataBuf = _archIORead8((UINT32)pRegBase + byteOffset);
        }
    else
        {
        *pDataBuf = *((UINT8 *)((UINT32)pRegBase + byteOffset));
        }

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function.
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList *)pDevInfo->pAccess,
                               OPT_REG_READ8);
        }

    return OK;

    }

/******************************************************************************
*
* _archRegisterRead16 - read 16-bit value from a register
*
* This routine is used by the driver to read 16-bits from a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterRead16
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;
    UINT32  endianFlag;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterRead16: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* update the flags */

    if (((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0) &&
             ((*pFlags & VXBAF_ENDIAN_MASK) == 0))
        *pFlags |= VXBAF_BYTE_ORDER_LE_16;

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        *pDataBuf = _archIORead16((UINT32)pRegBase + byteOffset);

        }
    else
        {
        *pDataBuf = *((UINT16 *)((UINT32)pRegBase + byteOffset));
        }

    /* if the byte order is already known, return from the function */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) != 0)
        return OK;

    /* retrieve the expected endianness flag */

    endianFlag = *pFlags & VXBAF_ENDIAN_MASK;

    /* clear the expected flags */

    *pFlags &= ~VXBAF_ENDIAN_MASK;

    /* update the current flags value */

#if  (_BYTE_ORDER == _BIG_ENDIAN)

    *pFlags |= VXBAF_BYTE_ORDER_BE_16;
#else

    *pFlags |= VXBAF_BYTE_ORDER_LE_16;
#endif


    /* convert the data to the required format */

    VXB_BYTE_ORDER_CONVERT_16(*pFlags,
                              *pDataBuf,
                              endianFlag);
    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_READ16);

        }

    return OK;
    }

/******************************************************************************
*
* _archRegisterRead32 - read 32-bit value from a register
*
* This routine is used by the driver to read 32-bits from a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterRead32
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;
    UINT32  endianFlag;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterRead32: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* update the flags */

    if (((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0) &&
             ((*pFlags & VXBAF_ENDIAN_MASK) == 0))
        *pFlags |= VXBAF_BYTE_ORDER_LE_32;

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        *pDataBuf = _archIORead32((UINT32)pRegBase + byteOffset);

        }
    else
        {
        *pDataBuf = *((UINT32 *)((UINT32)pRegBase + byteOffset));
        }

    /* if the byte order is already known, return from the function */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) != 0)
        return OK;

    /* retrieve the expected endianness flag */

    endianFlag = *pFlags & VXBAF_ENDIAN_MASK;

    /* clear the expected flags */

    *pFlags &= ~VXBAF_ENDIAN_MASK;

    /* update the current flags value */

#if  (_BYTE_ORDER == _BIG_ENDIAN)

    *pFlags |= VXBAF_BYTE_ORDER_BE_32;
#else

    *pFlags |= VXBAF_BYTE_ORDER_LE_32;
#endif

    /* convert the data to the required format */

    VXB_BYTE_ORDER_CONVERT_32(*pFlags,
                              *pDataBuf,
                              endianFlag);

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_READ32);

        }

    return OK;
    }

/******************************************************************************
*
* _archRegisterRead64 - read 64-bit value from a register
*
* This routine is used by the driver to read 64-bits from a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterRead64
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;
    UINT32  endianFlag;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterRead64: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* update the flags */

    if (((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0) &&
             ((*pFlags & VXBAF_ENDIAN_MASK) == 0))
        *pFlags |= VXBAF_BYTE_ORDER_LE_64;


    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        /* FIXME :
         * This should read 64 bits at a time. The function name should
         * be changed.
         */

        *pDataBuf = _archIORead16((UINT32)pRegBase + byteOffset);

        }
    else
        {
        *pDataBuf = *((UINT64 *)((UINT32)pRegBase + byteOffset));
        }

    /* if the byte order is already known, return from the function */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) != 0)
        return OK;

    /* retrieve the expected endianness flag */

    endianFlag = *pFlags & VXBAF_ENDIAN_MASK;

    /* clear the expected flags */

    *pFlags &= ~VXBAF_ENDIAN_MASK;

    /* update the current flags value */

#if  (_BYTE_ORDER == _BIG_ENDIAN)

    *pFlags |= VXBAF_BYTE_ORDER_BE_64;
#else

    *pFlags |= VXBAF_BYTE_ORDER_LE_64;
#endif

    /* convert the data to the required format */

    VXB_BYTE_ORDER_CONVERT_64(*pFlags,
                              *pDataBuf,
                              endianFlag);

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function.
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_READ64);
        }

    return OK;

    }

/******************************************************************************
*
* _archRegisterWrite8 - write 8-bits to a register
*
* This routine is used by the driver to write 8-bits to a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterWrite8
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterWrite8: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        _archIOWrite8(((UINT32)pRegBase + byteOffset), (*pDataBuf));
        }
    else
        {
        *((UINT8 *)((UINT32)pRegBase + byteOffset)) = (*pDataBuf);
        }

    /* if the pipe has to be flushed, flush the pipe */

    if ((*pFlags & VXBAF_CPU_PIPE_FLUSH) != 0)
        CACHE_PIPE_FLUSH();

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function.
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_WRITE8);

        }

    return OK;
    }

/******************************************************************************
*
* _archRegisterWrite16 - write 16-bits to a register
*
* This routine is used by the driver to write 16-bits to a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterWrite16
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterWrite16: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* if the byte order is already known, do not perform conversion */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0)

        {
        /* If mask value is not set, set it */

        if ((*pFlags & VXBAF_ENDIAN_MASK) == 0)
            *pFlags |= VXBAF_BYTE_ORDER_LE_16;

    /* Perform the conversion and update the flags */

#if  (_BYTE_ORDER == _BIG_ENDIAN)

        VXB_BYTE_ORDER_CONVERT_16(*pFlags,
                              *pDataBuf,
                              VXBAF_BYTE_ORDER_BE_16)
#else

        VXB_BYTE_ORDER_CONVERT_16(*pFlags,
                              *pDataBuf,
                              VXBAF_BYTE_ORDER_LE_16)
#endif
        }

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);
        }
    else
        {
        *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;
        }

    /* if the pipe has to be flushed, flush the pipe */

    if ((*pFlags & VXBAF_CPU_PIPE_FLUSH) != 0)
        CACHE_PIPE_FLUSH();

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function.
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_WRITE16);

        }

    return OK;
    }


/******************************************************************************
*
* _archRegisterWrite32 - write 32-bits to a register
*
* This routine is used by the driver to write 32-bits to a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterWrite32
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterWrite32: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* if the byte order is already known, do not perform conversion */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0)
        {
        /* If mask value is not set, set it */

        if ((*pFlags & VXBAF_ENDIAN_MASK) == 0)
            *pFlags |= VXBAF_BYTE_ORDER_LE_32;

    /* Perform the conversion and update the flags */

#if  (_BYTE_ORDER == _BIG_ENDIAN)

        VXB_BYTE_ORDER_CONVERT_32(*pFlags,
                              *pDataBuf,
                              VXBAF_BYTE_ORDER_BE_32)
#else

        VXB_BYTE_ORDER_CONVERT_32(*pFlags,
                              *pDataBuf,
                              VXBAF_BYTE_ORDER_LE_32)
#endif
        }

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);
        }
    else
        {
        *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;
        }

    /* if the pipe has to be flushed, flush the pipe */

    if ((*pFlags & VXBAF_CPU_PIPE_FLUSH) != 0)
        CACHE_PIPE_FLUSH();

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function.
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_WRITE32);
        }

    return OK;

    }

/******************************************************************************
*
* _archRegisterWrite64 - write 64-bits to a register
*
* This routine is used by the driver to write 64-bits to a device register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archRegisterWrite64
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    )
    {
    BOOL        firstTime   = FALSE;

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archRegisterWrite64: Invalid parameter\n",
                                  0,0,0,0,0,0);
        return ERROR;
        }

    /*
     * if it is the first time, this function is called, set the flag
     * indicating that this is the first time.
     */

    if (*pFlags == 0)
        {
        firstTime = TRUE;
        *pFlags = VXBAF_PREVENT_OPTIMIZATION;
        }

    /* if the byte order is already known, do not perform conversion */

    if ((*pFlags & VXBAF_BYTE_ORDER_KNOWN) == 0)
        {
        /* If mask value is not set, set it */

        if ((*pFlags & VXBAF_ENDIAN_MASK) == 0)
            *pFlags |= VXBAF_BYTE_ORDER_LE_64;

    /* Perform the conversion and update the flags */

#if  (_BYTE_ORDER == _BIG_ENDIAN)

        VXB_BYTE_ORDER_CONVERT_64(*pFlags,
                              *pDataBuf,
                              VXBAF_BYTE_ORDER_BE_64);
#else
        VXB_BYTE_ORDER_CONVERT_64(*pFlags,
                              *pDataBuf,
                              VXBAF_BYTE_ORDER_LE_64);

#endif
        }

    /* If the registers are in IO space, perform an IO operation */

    if ((*pFlags & VXBAF_IO_SPACE_OPERATION) != 0)
        {
        _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);
        }
    else
        {
        *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;
        }

    /* if the pipe has to be flushed, flush the pipe */

    if ((*pFlags & VXBAF_CPU_PIPE_FLUSH) != 0)
        CACHE_PIPE_FLUSH();

    /*
     * If this is the first time, this function is called. If it
     * requires optimization, call the function to optimize
     * this function.
     */
    if (firstTime)
        {
        optimizeAccessFunction(*pFlags,
                               (struct vxbAccessList * )pDevInfo->pAccess,
                               OPT_REG_WRITE64);

        }

    return OK;
    }

/******************************************************************************
*
* _archVolatileRegisterWrite - write to a volatile register
*
* This routine is used to write to a volatile register and read back the data
* from the volatile register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archVolatileRegisterWrite
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32          transactionSize,/* transaction size, in bytes */
    char *          pDataBuf,   /* buffer to read-from/write-to */
    UINT32 *        pFlags      /* flags */
    )
    {

    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }

    /* switch based on the transaction size */

    switch (transactionSize)
        {
        case 1:

            /* call the function to perform an 8 bit write operation */

            if (_archRegisterWrite8(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT8 *)pDataBuf,
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         write 8 failed: \n", 0,0,0,0,0,0);
                return ERROR;
                }

            /*
             * If IO operation flag is set,
             * retain it while issuing a read request
             */

            *pFlags &= VXBAF_IO_SPACE_OPERATION;

            /* call the function to perform an 8 bit read operation */

            if (_archRegisterRead8(pDevInfo,
                                   pRegBase,
                                   byteOffset,
                                   (UINT8 *)pDataBuf,
                                   pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         read 8 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            break;
        case 2:

            /* call the function to perform a 16 bit write operation */

            if (_archRegisterWrite16(pDevInfo,
                                     pRegBase,
                                     byteOffset,
                                     (UINT16 *)pDataBuf,
                                     pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         write 16 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            /*
             * If IO operation flag is set,
             * retain it while issuing a read request
             */

            *pFlags &= VXBAF_IO_SPACE_OPERATION;

            /* call the function to perform a 16 bit read operation */

            if (_archRegisterRead16(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT16 *)(pDataBuf),
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         read 16 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            break;
        case 4:

            /* call the function to perform a 32 bit write operation */

            if (_archRegisterWrite32(pDevInfo,
                                     pRegBase,
                                     byteOffset,
                                     (UINT32 *)pDataBuf,
                                     pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         write 32 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            /*
             * If IO operation flag is set,
             * retain it while issuing a read request
             */

            *pFlags &= VXBAF_IO_SPACE_OPERATION;

            /* call the function to perform a 32 bit read operation */

            if (_archRegisterRead32(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT32 *)pDataBuf,
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         read 32 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }
            break;

        case 8:

            /* call the function to perform a 64 bit write operation */

            if (_archRegisterWrite64(pDevInfo,
                                     pRegBase,
                                     byteOffset,
                                     (UINT64 *)pDataBuf,
                                     pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         write 64 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            /*
             * If IO operation flag is set,
             * retain it while issuing a read request
             */

            *pFlags &= VXBAF_IO_SPACE_OPERATION;

            /* call the function to perform a 64 bit read operation */

            if (_archRegisterRead64(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT64 *)pDataBuf,
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterWrite : \
                                         read 64 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            break;

        default:
            return ERROR;

        }

    return OK;

    }


/******************************************************************************
*
* _archVolatileRegisterRead - read from a volatile register
*
* This routine is used to read from a volatile register. This function does
* not split the read into multiple transactions.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archVolatileRegisterRead
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32          transactionSize,/* transaction size, in bytes */
    char *          pDataBuf,   /* buffer to read-from */
    UINT32 *        pFlags      /* flags */
    )
    {
    /* check whether the parameters are valid */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL) ||
        (pFlags == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterRead: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }

    switch (transactionSize)
        {
        case 1:

            /* call the function to perform a 8 bit read operation */

            if (_archRegisterRead8(pDevInfo,
                                   pRegBase,
                                   byteOffset,
                                   (UINT8 *)pDataBuf,
                                   pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterRead : \
                                          read 8 failed: \n", 0,0,0,0,0,0);
                return ERROR;
                }

            break;
        case 2:

            /* call the function to perform a 16 bit read operation */

            if (_archRegisterRead16(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT16 *)pDataBuf,
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterRead : \
                                          read 16 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }

            break;
        case 4:

            /* call the function to perform a 32 bit read operation */

            if (_archRegisterRead32(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT32 *)pDataBuf,
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterRead : \
                                          read 32 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }
            break;

        case 8:

            /* call the function to perform a 64 bit read operation */

            if (_archRegisterRead64(pDevInfo,
                                    pRegBase,
                                    byteOffset,
                                    (UINT64 *)pDataBuf,
                                    pFlags) != OK)
                {
                VXB_ACCESS_ARCH_LOG_ERROR("_archVolatileRegisterRead : \
                                          read 64 failed: \n", 0,0,0,0,0,0);

                return ERROR;
                }
            break;

        default:
            return ERROR;

        }

    return OK;

    }

/******************************************************************************
*
* _archOptRegWr64_00 - write 64 bits to a mem space register
*
* This is an optimized function which writes 64 bit data to the mem
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_07 - swap 64 bit data and write to a mem space register
*
* This is an optimized function which swaps the 64 bit data and writes to the mem
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_07
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_07: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_20 - write 64 bits to an IO space register
*
* This is an optimized function which writes 64 bits to the IO
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_27 - swap and write 64 bits to an IO space register
*
* This is an optimized function which swaps the 64 bit data and writes to the IO
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_27
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_27: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_10 - write 64 bits to a mem space register and flush data
*
* This is an optimized function which writes 64 bit data to the mem
* space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* do a pipe flush */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_17 - swap, write 64 bits to a mem space register & flush data
*
* This is an optimized function which swaps the 64 bit data to be written,
* writes to the mem space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_17
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_17: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* perform a flush */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_30 - write 64 bits to a IO space register and flush data
*
* This is an optimized function which writes 64 bit data to the IO
* space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr64_37 - swap, write 64 bits to a IO space register & flush data
*
* This is an optimized function which swaps the 64 bit data to be written,
* writes to the IO space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr64_37
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr64_37: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_00 - write 32 bits to mem space register
*
* This is an optimized function which writes 32 bits to the memory space
* register, without any modifications to the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the register in memory space */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_03 - swap the data and write 32 bits to mem space register
*
* This is an optimized function which swaps the 32 bit data and then writes to
* the memory space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_03
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_03: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* write to the register in memory space */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_20 - write 32 bits to an IO space register
*
* This is an optimized function which writes 32 bits to the IO space register,
* without any modifications to the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_23 - swap and write 32 bits to an IO space register
*
* This is an optimized function which swaps the 32 bit data and then writes to
* the IO space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_23
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_23: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 32 bits */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* Write to the IO space register */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_10 - write 32 bits to an mem space register and flush
*
* This is an optimized function which writes to the mem space register and then
* flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the register */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_10 - swap & write 32 bits to an mem space register and flush
*
* This is an optimized function which swaps the 32 bit data, writes to the mem
* space register and then flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_13
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_13: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 32 bit data */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* write the 32 bits to the mem space */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_30 - write 32 bits to an IO space register and flush
*
* This is an optimized function which writes 32 bit data and then
* flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the register in IO space */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr32_33 - swap and write 32 bits to an IO space register & flush
*
* This is an optimized function which swaps the 32 bit data, writes to the IO
* space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr32_33
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_33: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* write to the IO space register */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_00 - write 16 bits to a mem space register
*
* This is an optimized function which writes 16 bit data to the mem
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write 16 bits to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_01 - swap and write 16 bits to a mem space register
*
* This is an optimized function which swaps the 16 bit data and writes to the mem
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_01
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_01: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 16 bit data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_20 - write 16 bits to an IO space register
*
* This is an optimized function which writes the 16 bit data to the IO
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_21 - swap and write 16 bits to an IO space register
*
* This is an optimized function which swaps and writes the 16 bit data to the IO
* space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_21
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_21: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_10 - write 16 bits to a mem space register and flush data
*
* This is an optimized function which writes the 16 bit data to the mem
* space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_11 - swap, write 16 bits to mem space register & flush data
*
* This is an optimized function which swaps the 16 bit data, writes to the mem
* space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_11
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_11: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_30 - write 16 bits to IO space register & flush data
*
* This is an optimized function which writes 16 bits to the IO
* space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr16_31 - swap & write 16 bits to IO space register & flush data
*
* This is an optimized function which swaps the 16 bit data and writes the
* data to the IO space register and flushes the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr16_31
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr16_31: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr8_00 - write 8 bits to mem space register
*
* This is an optimized function which writes 8 bits to the memory space
* register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr8_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr8_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT8 *)((UINT32)pRegBase + byteOffset)) = (*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr8_20 - write 8 bits to IO space register
*
* This is an optimized function which writes 8 bits to the IO space
* register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr8_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr8_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite8(((UINT32)pRegBase + byteOffset), *pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWr8_10 - write 8 bits to mem space register and flush data
*
* This is an optimized function which writes 8 bits to the mem space
* register and flush the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr8_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr8_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write 8 bits to the register */

    *((UINT8 *)((UINT32)pRegBase + byteOffset)) = (*pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }


/******************************************************************************
*
* _archOptRegWr8_30 - write 8 bits to IO space register and flush data
*
* This is an optimized function which writes 8 bits to the IO space
* register and flush the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWr8_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr8_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite8(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_00 - write 64 bits to a mem space register and read back
*
* This is an optimized function which writes 64 bit data to the mem
* space register and read back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* read back the contents of the register */

    dataRead = *((UINT64 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_07 - swap 64 bit data and write & read from mem space
*
* This is an optimized function which swaps the 64 bit data and writes to the mem
* space register and read back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_07
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_07: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* read back the contents of the register */

    dataRead = *((UINT64 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_20 - write 64 bits to an IO space register and read back
*
* This is an optimized function which writes 64 bits to the IO
* space register and read back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);


    /* FIXME : Should do a 64 bit read */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_27 - swap, write 64 bits & read from an IO space register
*
* This is an optimized function which swaps the 64 bit data and writes to the IO
* space register and also reads back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_27
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_27: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    /* FIXME : Should do a 64 bit read */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_10 - write 64 bits to mem space register, flush data & read
*
* This is an optimized function which writes 64 bit data to the mem
* space register, flushes the data and reads back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* do a pipe flush */

    CACHE_PIPE_FLUSH();

    /* read back the contents of the register */

    dataRead = *((UINT64 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_17 - swap, write 64 bits to memspace, flush data & read
*
* This is an optimized function which swaps the 64 bit data to be written,
* writes to the mem space register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_17
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_17: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* write to the mem space register */

    *((UINT64 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* perform a flush */

    CACHE_PIPE_FLUSH();

    /* read back the contents of the register */

    dataRead = *((UINT64 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_30 - write 64 bits to IO space register, flush and read
*
* This is an optimized function which writes 64 bit data to the IO
* space register, flushes the data and reads back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* FIXME : Should do a 64 bit read */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd64_37 - swap, write 64 bits to IO space, flush & read data
*
* This is an optimized function which swaps the 64 bit data to be written,
* writes to the IO space register, flushes the data and reads back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd64_37
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT64  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd64_37: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 64 bit data */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    /* FIXME : Should do a 64 bit write */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), (UINT32)*pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* FIXME : Should do a 64 bit read */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_00 - write 32 bits to mem space register & read back
*
* This is an optimized function which writes 32 bits to the memory space
* register, without any modifications to the data and reads back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the register in memory space */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* read back the contents of the memory */

    dataRead = *((UINT32 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_03 - swap, write 32 bits to mem space register & read
*
* This is an optimized function which swaps the 32 bit data, writes to
* the memory space register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_03
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {
    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWr32_03: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* write to the register in memory space */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* read back the contents of the memory */

    dataRead = *((UINT32 *)((UINT32)pRegBase + byteOffset));


    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_20 - write 32 bits to an IO space register & read back
*
* This is an optimized function which writes 32 bits to the IO space register,
* without any modifications to the data and reads back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* read from the IO space register */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_23 - swap, write 32 bits to an IO space register & read
*
* This is an optimized function which swaps the 32 bit data, writes to
* the IO space register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_23
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_23: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 32 bits */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* Write to the IO space register */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* read from the IO space register */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_10 - write 32 bits to memspace register, flush & read
*
* This is an optimized function which writes to the mem space register,
* flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {
    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the register */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents of the memory */

    dataRead = *((UINT32 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_13 - swap, write 32 bits to memspace, flush & read
*
* This is an optimized function which swaps the 32 bit data, writes to the mem
* space register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_13
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_13: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 32 bit data */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* write the 32 bits to the mem space */

    *((UINT32 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents of the memory */

    dataRead = *((UINT32 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_30 - write 32 bits to an IO space register, flush & read
*
* This is an optimized function which writes 32 bit data, flushes the data and
* reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {
    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the register in IO space */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read from the IO space register */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd32_33 - swap, write 32 bits to IO space, flush & read
*
* This is an optimized function which swaps the 32 bit data, writes to the IO
* space register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd32_33
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT32  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd32_33: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    /* write to the IO space register */

    _archIOWrite32(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read from the IO space register */

    dataRead = (UINT32) _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_00 - write 16 bits to a mem space register & read back
*
* This is an optimized function which writes 16 bit data to the mem
* space register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write 16 bits to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* read back the contents */

    dataRead = *((UINT16 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_01 - swap, write 16 bits to a mem space register & read
*
* This is an optimized function which swaps the 16 bit data, writes to the mem
* space register and read back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_01
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_01: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the 16 bit data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* read back the contents */

    dataRead = *((UINT16 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_20 - write 16 bits to an IO space register & read
*
* This is an optimized function which writes the 16 bit data to the IO
* space register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* read back the contents */

    dataRead = (UINT16)_archIORead16((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_21 - swap, write 16 bits to an IO space register & read
*
* This is an optimized function which swaps the data, writes the 16 bit data
* to the IO space register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_21
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_21: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* read back the contents to flush the data */

    dataRead = (UINT16)_archIORead16((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_10 - write 16 bits to memspace, flush data & read
*
* This is an optimized function which writes the 16 bit data to the mem
* space register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents */

    dataRead = *((UINT16 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_11 - swap, write 16 bits to memspace, flush & read data
*
* This is an optimized function which swaps the 16 bit data, writes to the mem
* space register, flushes the data & reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_11
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_11: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the mem space register */

    *((UINT16 *)((UINT32)pRegBase + byteOffset)) = *pDataBuf;

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents */

    dataRead = *((UINT16 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_30 - write 16 bits to IO space, flush & read back data
*
* This is an optimized function which writes 16 bits to the IO
* space register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents */

    dataRead = (UINT16)_archIORead16((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd16_31 - swap, write 16 bits to IO space, flush data & read
*
* This is an optimized function which swaps the 16 bit data, writes the
* data to the IO space register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd16_31
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags          /* flags */
    )
    {

    volatile UINT16  dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd16_31: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* swap the data */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    /* write to the IO space register */

    _archIOWrite16(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents */

    dataRead = (UINT16)_archIORead16((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd8_00 - write 8 bits to mem space register and read back data
*
* This is an optimized function which writes 8 bits to the memory space
* register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd8_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {
    volatile UINT8   dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd8_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the mem space register */

    *((UINT8 *)((UINT32)pRegBase + byteOffset)) = (*pDataBuf);

    /* read back the mem space register contents */

    dataRead = *((UINT8 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd8_20 - write 8 bits to IO space register and read back
*
* This is an optimized function which writes 8 bits to the IO space
* register and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd8_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {

    volatile UINT8   dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd8_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite8(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* read back the contents of IO space register */

    dataRead = (UINT8)_archIORead8((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegWrRd8_10 - write 8 bits to mem space register, flush data & read
*
* This is an optimized function which writes 8 bits to the mem space
* register, flushes the data and reads back the data.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd8_10
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {

    volatile UINT8   dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd8_10: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write 8 bits to the register */

    *((UINT8 *)((UINT32)pRegBase + byteOffset)) = (*pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the mem space register contents */

    dataRead = *((UINT8 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }


/******************************************************************************
*
* _archOptRegWrRd8_30 - write 8 bits to IO space register, flush data & read
*
* This is an optimized function which writes 8 bits to the IO space
* register, flush the data and read back the contents.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegWrRd8_30
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to copy data from */
    UINT32 *            pFlags  /* flags */
    )
    {

    volatile UINT8   dataRead;

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegWrRd8_30: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* write to the IO space register */

    _archIOWrite8(((UINT32)pRegBase + byteOffset), *pDataBuf);

    /* flush the data */

    CACHE_PIPE_FLUSH();

    /* read back the contents of IO space register */

    dataRead = (UINT8)_archIORead8((UINT32)pRegBase + byteOffset);

    return OK;
    }
/******************************************************************************
*
* _archOptRegRd64_00 - read 64 bits from mem space register
*
* This is an optimized function which reads 64 bits from a mem space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd64_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {
#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd64_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from mem space register */

    *pDataBuf = *((UINT64 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd64_07 - read 64 bits from mem space register and swap data
*
* This is an optimized function which reads 64 bits from a mem space register and
* swap the data read.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd64_07
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd64_07: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from mem space register */

    *pDataBuf = *((UINT64 *)((UINT32)pRegBase + byteOffset));

    /* swap the data read */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd64_20 - read 64 bits from IO space register
*
* This is an optimized function which reads 64 bits from a IO space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd64_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {
    /* check if the parameters are valid, the last parameter can be NULL */

#ifdef VXB_ACCESS_DEBUG
    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd64_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* FIXME : This should read 64 bits */

    *pDataBuf = _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd64_27 - read 64 bits from IO space register and swap data
*
* This is an optimized function which reads 64 bits from a IO space register and
* swap the data read.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd64_27
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT64 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {
    /* check if the parameters are valid, the last parameter can be NULL */

#ifdef VXB_ACCESS_DEBUG
    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd64_27: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* FIXME : This should read 64 bits */

    *pDataBuf = _archIORead32((UINT32)pRegBase + byteOffset);

    /* swap the data read */

    VXBAF_BYTE_SWAP_64(*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd32_00 - read 32 bits from mem space register
*
* This is an optimized function which reads 32 bits from a mem space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd32_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd32_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from mem space register */

    *pDataBuf = *((UINT32 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd32_03 - read 32 bits from mem space register and swap data
*
* This is an optimized function which reads 32 bits from a mem space register and
* swap the data read.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd32_03
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd32_03: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from a mem space register */

    *pDataBuf = *((UINT32 *)((UINT32)pRegBase + byteOffset));

    /* swap the bytes read */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd32_20 - read 32 bits from IO space register
*
* This is an optimized function which reads 32 bits from an IO space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd32_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd32_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from an IO space register */

    *pDataBuf = _archIORead32((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd32_23 - read 32 bits from IO space register and swap data
*
* This is an optimized function which reads 32 bits from an IO space register and
* swap the data read.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd32_23
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT32 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd32_23: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from an IO space register */

    *pDataBuf = _archIORead32((UINT32)pRegBase + byteOffset);

    /* swap the data read */

    VXBAF_BYTE_SWAP_32(*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd16_00 - read 16 bits from mem space register
*
* This is an optimized function which reads 16 bits from a mem space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd16_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd16_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from mem space register */

    *pDataBuf = *((UINT16 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd16_01 - read 16 bits from mem space register and swap data
*
* This is an optimized function which reads 16 bits from a mem space register and
* swap the data read.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd16_01
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

    /* check if the parameters are valid, the last parameter can be NULL */

#ifdef VXB_ACCESS_DEBUG
    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd16_01: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from mem space register */

    *pDataBuf = *((UINT16 *)((UINT32)pRegBase + byteOffset));

    /* swap the data read */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd16_20 - read 16 bits from IO space register
*
* This is an optimized function which reads 16 bits from an IO space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd16_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd16_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from IO space register */

    *pDataBuf = _archIORead16((UINT32)pRegBase + byteOffset);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd16_21 - read 16 bits from IO space register and swap data
*
* This is an optimized function which reads 16 bits from an IO space register and
* swap the data read.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd16_21
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT16 *            pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd16_21: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from an IO space register */

    *pDataBuf = _archIORead16((UINT32)pRegBase + byteOffset);

    /* swap the data read */

    VXBAF_BYTE_SWAP_16(*pDataBuf);

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd8_00 - read 8 bits from mem space register
*
* This is an optimized function which reads 8 bits from a mem space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd8_00
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd8_00: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from the mem space register */

    *pDataBuf = *((UINT8 *)((UINT32)pRegBase + byteOffset));

    return OK;
    }

/******************************************************************************
*
* _archOptRegRd8_20 - read 8 bits from IO space register
*
* This is an optimized function which reads 8 bits from an IO space register.
*
* RETURNS: OK on success, or ERROR otherwise
*
* ERRNO: N/A
*/

STATUS  _archOptRegRd8_20
    (
    VXB_DEVICE_ID       pDevInfo,       /* device info */
    void *              pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32              byteOffset,     /* offset, in bytes, of register */
    UINT8 *             pDataBuf,       /* buffer to put data in */
    UINT32 *            pFlags          /* flags */
    )
    {

#ifdef VXB_ACCESS_DEBUG
    /* check if the parameters are valid, the last parameter can be NULL */

    if ((pDevInfo == NULL) ||
        (pRegBase == NULL) ||
        (pDataBuf == NULL))
        {
        VXB_ACCESS_ARCH_LOG_ERROR("_archOptRegRd8_20: \
                                  Invalid parameter\n", 0,0,0,0,0,0);
        return ERROR;
        }
#endif

    /* read from an IO space register */

    *pDataBuf = _archIORead8((UINT32)pRegBase + byteOffset);

    return OK;
    }

#endif

#ifdef	_WRS_CONFIG_LP64
/*******************************************************************************
*
* vxbRegBarMap - obtain an virtual address for a given register mapping
*
* This function maps the virtual address given by the address space allocator
* to the BAR if the BAR is for memory mapped I/O registers or a PCI memory
* that requires accessing the region through the virtual space.
* The initial MMU attributes and the masks for the page tables can be
* configured per bus with VXB_MMU_ATTR_BUS and VXB_MMU_ATTR_BUS_MASK resource
* entries in the hcfResource structure for the bus controller device in the
* BSP. They also can be configured per device with VXB_MMU_ATTR_IO and
* VXB_MMU_ATTR_IO_MASK resource entry for the device, that will over write the
* initial MMU attributes provided by the bus controller device. The default MMU
* attributes and the masks,
* (MMU_ATTR_VALID     | MMU_ATTR_SUP_RW   | MMU_ATTR_CACHE_OFF),
* (MMU_ATTR_VALID_MSK | MMU_ATTR_PROT_MSK | MMU_ATTR_CACHE_MSK),
* will be used to set the initial MMU attributes if none of them is specified.
* The BAR will be updated to the virtual address from physical address if the
* virtual address is successfully assigned by the address space allocator.
*
* This function skips mapping if the 'regBaseSize[index]' is zero,
* the 'pRegBasePhys[index]' is NONE or NULL or the 'pRegBase[index]' and the
* 'pRegBasePhys[index]' are not identical in the VxBus device instance
* structure given to this function.
* The BAR for I/O access registers on Intel architecture or the BAR in K0
* or K1 region on MIPS architecture are also skipped the mapping.
*
* This routine should not be called from interrupt level.
*
* RETURNS: ERROR if the address space allocator fails to assign a virtual
* address or map to the BAR, otherwise OK.
*
* ERRNOS
* The routine may set the following errnos:
*
* \is
* \i 'S_adrSpaceLib_SIZE_IS_INVALID'
* The <numPages> parameter is 0, optimized option with unoptimal size
* \i 'S_adrSpaceLib_INSUFFICIENT_SPACE_IN_RGN'
* Not enough pages in rvirtual region.
* \i 'S_adrSpaceLib_REGION_NOT_AVAILABLE'
* Pages requested not available in virtual region.
* \ie
*/

LOCAL STATUS vxbRegBarMap
    (
    VXB_DEVICE_ID pDev,
    int index
    )
    {
    ULONG	regBase;
    size_t	regBaseSize;
    size_t	pageSize;
    STATUS	status = OK;

    /* save the physical address */

    regBase = (ULONG)pDev->pRegBasePhys[index];

    /* save the BAR size */

    regBaseSize = pDev->regBaseSize[index];

    /*
     * Perform address mapping to the IO space if needs to allocate
     * a virtual space to access the memory mapped IO registers which hasn't
     * be done. 
     */

    if ((regBaseSize != 0) &&
	(regBase != 0) &&
	(regBase != (ULONG) NONE) &&
# if (CPU_FAMILY == I80X86)
	(pDev->regBaseFlags[index] != VXB_REG_IO) &&
# elif (CPU_FAMILY == MIPS)
	((regBase < K0BASE) || (K2BASE <= regBase)) &&
# endif	/* CPU_FAMILY == I80X86 */
	(regBase == (ULONG)pDev->pRegBase[index]) &&
	((pageSize = VM_PAGE_SIZE_GET()) != (size_t)ERROR))
	{
	ULONG		regBaseEnd;
	size_t		length;
	HCF_DEVICE *	pHcf;
	VXB_DEVICE_ID	pParent;
	VIRT_ADDR	pVirtRegBase;	/* virtual address mapped for BAR */
	STATUS (* mmuAttrFunc) (VXB_DEVICE_ID, VXB_DEVICE_ID, int,
				    PHYS_MEM_DESC *) = NULL;
	PHYS_MEM_DESC iomMemDesc = {0,0,0,0,0};
					/* description for memory mapped IO */

	/* the end of the physical address of the BAR (+1) */

	regBaseEnd = regBase + regBaseSize;

	/* align on the MMU page */

	regBase = (ULONG) ROUND_DOWN (regBase, pageSize);

	/* align on the next MMU page */

	regBaseEnd = (ULONG) ROUND_UP (regBaseEnd, pageSize);

	/* length must be multiple of the pageSize */

	length = regBaseEnd - regBase;

	/* get the HCF if available */

	pHcf = hcfDeviceGet (pDev);

	/* 
	 * Get the VXB_MMU_ATTR_IO_MASK and the VXB_MMU_ATTR_IO
	 * if the HCF specifies.
	 */

	if ((pHcf == NULL) ||
	    (devResourceGet (pHcf, VXB_MMU_ATTR_IO_MASK, HCF_RES_INT,
			     (void *)&iomMemDesc.initialStateMask) != OK) ||
	    (devResourceGet (pHcf, VXB_MMU_ATTR_IO, HCF_RES_INT,
			     (void *)&iomMemDesc.initialState) != OK))
	    {
	    /* get them from parent bus if no device MMU attributes specifies */

	    for (pParent = vxbDevParent (pDev); pParent != NULL;
		 pParent = vxbDevParent (pParent))
		{
		if ((mmuAttrFunc = vxbDevMethodGet (pParent,
				DEVMETHOD_CALL(busCtlrMmuAttr))) != NULL)
		    break;
		}

	    if (mmuAttrFunc != NULL)
		{
		/* the parent device will set the MMU attributes */

		mmuAttrFunc (pParent, pDev, index,
			     (PHYS_MEM_DESC *)&iomMemDesc);
		}
	    }

	/*
	 * If none of the device and the parants bus specifies the MMU
	 * attributes, use the default.
	 */

	if ((iomMemDesc.initialStateMask == 0) ||
	    (iomMemDesc.initialState == 0))
	    {
	    iomMemDesc.initialStateMask = MMU_ATTR_IO_MASK;
	    iomMemDesc.initialState     = MMU_ATTR_IO_DEFAULT;
	    }

#if (CPU_FAMILY == I80X86)
        /*
         * For an Intel 64 or IA-32 processor, IA32_EFER.NXE = 0 indicates
         * XD flag (bit 63) of a paging-structure must be a reserved bit.
         * An attempt to access a memory page with the XD flag set causes
         * a page-fault exception for reserved bit checking failed. 
         */

#   define IA32_EFER       0xc0000080
#   define IA32_EFER_NX    0x00000800      /* Execute disable bit enable (bit 11) */
        {
        UINT64 msrValue = 0;
        IMPORT void	pentiumMsrGet ();
        pentiumMsrGet (IA32_EFER, &msrValue);

        if ((msrValue & IA32_EFER_NX) != IA32_EFER_NX)
            {
            iomMemDesc.initialState |= MMU_ATTR_PROT_SUP_EXE;
            }
        }
#endif  /* (CPU_FAMILY == I80X86) */

	/* map the MMU page */

	pVirtRegBase = adrSpacePageAlloc (NULL,
					  (VIRT_ADDR)NULL,
					  (PHYS_ADDR)regBase,
					  length / pageSize,
					  ADR_SPACE_OPT_RGN_KERNEL |
					  ADR_SPACE_OPT_MAPPED |
					  ADR_SPACE_OPT_OPTIMIZED |
					  VM_MAP_GLOBAL |
					  (iomMemDesc.initialState &
					   iomMemDesc.initialStateMask));

	if (pVirtRegBase != (VIRT_ADDR)NONE)
	    {
	    /*
	     * Over write the physical address of the BAR with the
	     * virtual address.
	     */

	    pDev->pRegBase[index] = (void *)((ULONG)pVirtRegBase +
				(ULONG)pDev->pRegBasePhys[index] - regBase);
	    }
	else
	    status = ERROR;
	}

    return (status);
    }
#endif	/* _WRS_CONFIG_LP64 */

/*******************************************************************************
*
* vxbRegMap - obtain an access handle for a given register mapping
*
* This function returns a handle suitable for accessing a given BAR
* with the vxbReadXX()/vxbWriteXX() register access API. The handle is
* opaque to the caller, but is used by the API to determine if the BAR
* is I/O mapped, memory mapped, or some special case so that it can choose
* which access method to use.
*
* The vxbRegMap() routine will first try to query the parent bus controller
* of a given device to see if it has a custom mapping routine. If not,
* an attempt is made to return a reasonable default handle value.
*
* This routine should not be called from interrupt level on VxWorks LP64
* support.
*
* 64-BIT CONSIDERATIONS
* This function maps the virtual address given by the address space allocator
* to the BAR if the BAR is for memory mapped I/O registers or a PCI memory
* that requires accessing the region through the virtual space.
* The initial MMU attributes and the masks for the page tables can be
* configured per bus with VXB_MMU_ATTR_BUS and VXB_MMU_ATTR_BUS_MASK resource
* entries in the hcfResource structure for the bus controller device in the
* BSP. They also can be configured per device with VXB_MMU_ATTR_IO and
* VXB_MMU_ATTR_IO_MASK resource entry for the device, that will over write the
* initial MMU attributes provided by the bus controller device. The default MMU
* attributes and the masks,
* (MMU_ATTR_VALID     | MMU_ATTR_SUP_RW   | MMU_ATTR_CACHE_OFF),
* (MMU_ATTR_VALID_MSK | MMU_ATTR_PROT_MSK | MMU_ATTR_CACHE_MSK),
* will be used to set the initial MMU attributes if none of them is specified.
* The BAR will be updated to the virtual address from physical address if the
* virtual address is successfully assigned by the address space allocator.
* The driver calls this function must use the BAR after this function is
* performed.
*
* This function skips mapping if the 'regBaseSize[index]' is zero,
* the 'pRegBasePhys[index]' is NONE or NULL or the 'pRegBase[index]' and the
* 'pRegBasePhys[index]' are not identical in the VxBus device instance
* structure given to this function.
* The BAR for I/O access registers on Intel architecture or the BAR in K0 
* or K1 region on MIPS architecture are also skipped the mapping.
*
* ERRNOS
* The routine may set the following errnos on 64-bit system:
*
* \is
* \i 'S_adrSpaceLib_SIZE_IS_INVALID'
* The <numPages> parameter is 0, optimized option with unoptimal size
* \i 'S_adrSpaceLib_INSUFFICIENT_SPACE_IN_RGN'
* Not enough pages in rvirtual region.
* \i 'S_adrSpaceLib_REGION_NOT_AVAILABLE'
* Pages requested not available in virtual region.
* \ie
*
* SEE ALSO: vxbRegUnmap()
*
* RETURNS: ERROR if the address space allocator fails to assign a virtual
* address or map to the BAR on 64-bit system or the BAR cannot be mapped to
* a reasonable handle, otherwise OK.
*/

STATUS vxbRegMap
    (
    VXB_DEVICE_ID pDev,
    int index,
    void ** ppHandle
    )
    {
    VXB_DEVICE_ID pParent;
    STATUS (* mapFunc) (VXB_DEVICE_ID, VXB_DEVICE_ID, int, void *) = NULL;
    ULONG handle = 0;

    if (index >= VXB_MAXBARS)
        return (ERROR);

    pParent = vxbDevParent (pDev);

    if (pParent == NULL)
        return (ERROR);

#ifdef	_WRS_CONFIG_LP64
    /* map the BAR if required */

    if (vxbRegBarMap (pDev, index) != OK)
	return (ERROR);
#endif	/* _WRS_CONFIG_LP64 */

    mapFunc = vxbDevMethodGet (pParent, DEVMETHOD_CALL(vxbDevRegMap));

    /*
     * If the parent bus driver has its own mapping routine, then
     * call it. Otherwise, we make an educated guess below about
     * what default handle to provide.
     */

    if (mapFunc != NULL)
        return (mapFunc (pParent, pDev, index, ppHandle));

    if (pDev->regBaseFlags[index] == VXB_REG_NONE ||
        pDev->regBaseFlags[index] == VXB_REG_SPEC)
        return (ERROR);

    /*
     * For PowerPC, use ordered accesses for memory mapped BARs.
     * The ordered methods on PPC include an eieio, which is needed
     * in most scenarios.
     */

    if (pDev->regBaseFlags[index] == VXB_REG_MEM)
#if (CPU_FAMILY == PPC)
        handle = VXB_HANDLE_ORDERED;
#elif (CPU_FAMILY == ARM)
        /* IO is necessary! If not, SATA/USB will be unstable on E2000. */
        
        handle = VXB_HANDLE_IO;
#else
        handle = VXB_HANDLE_MEM;
#endif

    if (pDev->regBaseFlags[index] == VXB_REG_IO)
        handle = VXB_HANDLE_IO;

#if  (_BYTE_ORDER == _BIG_ENDIAN)
    if (pDev->busID == VXB_BUSID_PCI || pDev->busID == VXB_BUSID_PCIX ||
        pDev->busID == VXB_BUSID_PCIEXPRESS)
        handle = VXB_HANDLE_SWAP(handle);
#endif

    *ppHandle = (void *)handle;

    return (OK);
    }

/*******************************************************************************
*
* vxbRegUnmap - unmap a specified base register address of virtual pages
*
* 64-BIT CONSIDERATIONS
* This function unmaps the virtual address given by the address space allocator
* to the BAR if the BAR is for memory mapped I/O registers or a PCI memory
* that requires accessing the region through the virtual space and frees
* the virtual address to the address space allocator.
*
* This function skips unmapping if the 'pRegBase[index]' or
* the 'pRegBasePhys[index]' is NULL, the 'pRegBase[index]' and
* the 'pRegBasePhys[index]' are identical,
* the 'pRegBasePhys[index]' is NONE or 'regBaseSize[index]' is zero in
* the VxBus device instance structure given to this function.
*
* This function does no operation on 32-bit system.
*
* RETURNS: ERROR if the address space allocator fails to unmap or free
* a virtual address on 64-bit system, otherwise OK.
*
* SEE ALSO: vxbRegMap()
*/

STATUS vxbRegUnmap
    (
    VXB_DEVICE_ID pDev,
    int index
    )
    {
    if (index >= VXB_MAXBARS)
	return (ERROR);

#ifdef	_WRS_CONFIG_LP64
    ULONG	regBase;
    ULONG	regBasePhys;
    size_t	regBaseSize;
    size_t	pageSize;
    STATUS	status = OK;

    /* save the virtual address */

    regBase = (ULONG) pDev->pRegBase[index];

    /* save the physical address */

    regBasePhys = (ULONG) pDev->pRegBasePhys[index];

    /* save the BAR size */

    regBaseSize = pDev->regBaseSize[index];

    /* Perform address unmapping from the IO space if necessary. */

    if ((regBase != 0) &&
	(regBasePhys != 0) &&
	(regBase != regBasePhys) &&
	(regBasePhys != (ULONG) NONE) &&
	(regBaseSize != 0) &&
	((pageSize = VM_PAGE_SIZE_GET()) != (size_t)ERROR))
	{
	ULONG		regBaseEnd;
	size_t		length;
	VIRT_ADDR	virtAddr;
	size_t		numPages;

	/* the end of the virtual address of the BAR (+1) */

	regBaseEnd = regBase + regBaseSize;

	/* align on the MMU page */

	regBase = (ULONG) ROUND_DOWN (regBase, pageSize);

	/* align on the next MMU page */

	regBaseEnd = (ULONG) ROUND_UP (regBaseEnd, pageSize);

	/* length must be multiple of the pageSize */

	length = regBaseEnd - regBase;

	/* unmap the MMU page */

	status = adrSpacePageFree (NULL,
				   (VIRT_ADDR)regBase,
				   length / pageSize,
				   (ADR_SPACE_OPT_RGN_KERNEL |
				    ADR_SPACE_OPT_OPTIMIZED  |
				    ADR_SPACE_OPT_PHYS_FREE));

	if (status == OK)
	    {
	    /* Restore the physical address to the pRegBase[]. */

	    pDev->pRegBase[index] = pDev->pRegBasePhys[index];
	    }
	}

    return (status);
#else
    return (OK);
#endif	/* _WRS_CONFIG_LP64 */
    }

#ifdef	_WRS_CONFIG_LP64
/*******************************************************************************
*
* vxbRegUnmapAll - unmap all the base register address of virtual pages
*
* This function unmaps all the virtual addresses given by the address space
* allocator to the BARs if the BARs are for memory mapped I/O registers or
* PCI memory that require accessing the region through the virtual space
* and frees the virtual addresses to the address space allocator.
*
* RETURNS: ERROR if the address space allocator fails to unmap or free one of
* the virtual addresses, otherwise OK.
*
* \NOMANUAL
*/

STATUS vxbRegUnmapAll
    (
    VXB_DEVICE_ID pDev
    )
    {
    int index;

    /* unmap all the BARs */

    for (index = 0; index < VXB_MAXBARS; index++)
	{
	/* Execute vxbRegUnmap() only when the regBaseSize[] is non-zero. */

	if (pDev->regBaseSize[index] != 0)
	    {
	    if (vxbRegUnmap (pDev, index) != OK)
		{
		/* failed to unmap */

		return (ERROR);
		}
	    }
	}

    return (OK);
    }
#endif	/* _WRS_CONFIG_LP64 */
