/* cacheAimArch7Identify.s - AIM ARM ARCH7 cache identify routines */

/*
 * Copyright (c) 2008, 2012, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
06jan14,c_l  Improve cache operation. (VXW6-27040)
01c,18jan12,rec  WIND00329076 - fix stack backtrace.  General cleanup
01b,12aug08,j_b  use SBZ data with ISB
01a,06jun08,j_b  Created from cacheAimArch6Identify.s, rev 01b
*/

/*
DESCRIPTION
This library contains a common cache identify routines for the ARM ARCH7 caches.

SEE ALSO: 
\tb ARM Architecture Reference Manual ARMv7-A and ARMv7-R edition (ARM DDI 0406)
*/

#define _ASMLANGUAGE
#include <vxWorks.h>
#include <asm.h>
#include <arch/arm/cacheArmLib.h>

	.data

	/* globals */

	FUNC_EXPORT(cacheArch7CLIDR)            /* Get CLIDR register */
	FUNC_EXPORT(cacheAimArch7Identify)      /* Get types of caches fitted */
	FUNC_EXPORT(cacheAimArch7SizeInfoGet)   /* Get size of caches fitted  */
	FUNC_EXPORT(cacheArchLocSizeGet)        /* Get cache size of PoC      */
	FUNC_EXPORT(cacheArchLouSizeGet)        /* Get cache size of PoU      */

	.text
	.balign	4

#if (ARM_THUMB2)
	.thumb
#else /*(ARM_THUMB2)*/
	.code	32
#endif /*(ARM_THUMB2)*/

/*******************************************************************************
*
* cacheArch7CLIDR - read CLIDR Register to find cache levels
*
* This routine reads the Cache Level ID register (MMU/MPU) 
*
* \NOMANUAL
*
* RETURNS: coded value indicating information about the cache type(s).
*
* UINT32 cacheArch7CLIDR (void)
*/

FUNC_BEGIN(cacheArch7CLIDR)

    MRC p15, 1, r0, c0, c0, 1   /* r0 = Cache Lvl ID register info */
                    
    /* Return, with value in R0 */

    MOV pc, lr

FUNC_END(cacheArch7CLIDR)
        

/*******************************************************************************
*
* cacheAimArch7Identify - identify type of cache(s) fitted at the given level
*
* This routine reads the Cache Level ID register (MMU/MPU) to determine the
* type(s) of cache(s) fitted at the given cache level.  
* It is assumed the cache level is valid (0-6).
*
* \NOMANUAL
*
* RETURNS: coded value indicating information about the cache type(s).
*
* UINT32 cacheAimArch7Identify (UINT32 level)
*/

FUNC_BEGIN(cacheAimArch7Identify)

	MRC	p15, 1, r1, c0, c0, 1	/* r1 = Cache Lvl ID register info */
					/* CtypeX = bits[(3x+2):3x], x=0 to 6 */

	MOV	r2, r0, LSL #1		/* r2 = level*2 */
	ADD	r0, r0, r2		/* r0 = lsb pos. of type = 3*level */
	
	MOV	r1, r1, LSR r0		/* rt-align type field of cache level */
	MOV	r2, #0x7		/* r2 = cache type mask (rt-aligned) */
	AND	r0, r1, r2		/* r0 = cache type code for level */

	/* Return, with value in R0 */

	MOV	pc, lr

        FUNC_END(cacheAimArch7Identify)

/*******************************************************************************
*
* cacheAimArch7SizeInfoGet - return size-related information of cache fitted
*
* This routine reads the cache size ID register (MMU/MPU) to return the 
* size-related information of the given type of cache fitted at the
* given cache level.
* It is assumed the cache level is valid (0-6) and a cache exists at that level.
* Valid cache type values are: 0 for data/unified cache, 1 for instruction cache
*
* \NOMANUAL
*
* RETURNS: coded value indicating information about the cache size
*
* NOTE: If level or type is set to indicate a cache that is not implemented,
* the result is UNPREDICTABLE.
*
* UINT32 cacheAimArch7SizeGet (UINT32 level, UINT32 type)
*/

FUNC_BEGIN(cacheAimArch7SizeInfoGet)

	MOV	r2, #0
	MOV	r0, r0, LSL #1		/* shift cache level into position */
	ADD	r0, r0, r1		/* set cache type (data/instr) */
	MCR	p15, 2, r0, c0, c0, 0	/* select the Cache Size ID register */
	/*MCR	p15, 0, r2, c7, c5, 4*/	 /* CP15ISB deprecated, ISB strongly recommended */
	ISB /* ISB: sync change to Cache Size ID */

	MRC	p15, 1, r0, c0, c0, 0	/* r0 = current Cache Size ID info */

	/* Return, with value read in R0 */

	MOV     pc, lr

FUNC_END(cacheAimArch7SizeInfoGet)

/*******************************************************************************
 *
 * cacheArchLocSizeGet - get the cache size of the level of PoC
 *
 * This routine get cache size of the last level of cache that must be cleaned
 * or invalidated when cleaning or invalidating to the point of coherency.
 *
 * \NOMANUAL
 *
 * size_t cacheArchLocSizeGet (void)
 */

FUNC_BEGIN (cacheArchLocSizeGet)
        MOV     r1, #LoC_MASK
        MOV     r2, #LoC_SHIFT
        B       1f
FUNC_END (cacheArchLocSizeGet)

/*******************************************************************************
 *
 * cacheArchLouSizeGet - get the cache size of the level of PoU
 *
 * This routine get the cache size of the last level of cache that must be cleaned
 * or invalidated when cleaning or invalidating to the point of unification for the
 * processor.
 *
 * \NOMANUAL
 *
 * size_t cacheArchLouSizeGet (void)
 */

FUNC_BEGIN (cacheArchLouSizeGet)
        MOV     r1, #LoUU_MASK
        MOV     r2, #LoUU_SHIFT
1:
        MRC     p15, 1, r0, c0, c0, 1   /* r0 = Cache Lvl ID register info */
        AND     r1, r0, r1              /* get PoU/PoC level */
        MOV     r1, r1, LSR r2          /* get LoU/PoU << 1 */
        SUB     r1, r1, #2
        MCR     p15, 2, r1, c0, c0, 0   /* select the Cache Size ID register */
        ISB
        MRC     p15, 1, r3, c0, c0, 0   /* get current Cache Size ID info */

        AND     r0, r3, #7              /* bits:0~2 */
        MOV     r1, #16
        MOV     r0, r1, LSL r0          /* r0 = cache line size */

        LDR     r1, =0x1ff8             /* waynum mask */
        AND     r1, r3, r1
        MOV     r1, r1, LSR #3          /* waynum shift */
        ADD     r1, r1, #1

        LDR     r2, =0xfffe000          /* setnum mask */
        AND     r2, r3, r2
        MOV     r2, r2, LSR #13         /* setnum shift */
        ADD     r2, r2, #1

        MUL     r0, r0, r1
        MUL     r0, r0, r2
        BX      lr
FUNC_END (cacheArchLouSizeGet)
